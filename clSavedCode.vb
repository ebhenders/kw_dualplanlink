Imports System.Runtime.InteropServices


Public Class clSavedCode

    'Public Sub SCHEDULE()
    '	Dim ThisPolySpatialMx As Single
    '	Dim bestKRegTic As Short
    '	Dim Newloss As Single
    '	Dim DPacceptedLoss As Single
    '	Dim SpatMnVal As Single
    '	Dim ktic As Short
    '	Dim kmtype As Short
    '	Dim AspatNPV As Single
    '	Dim kpres As Integer
    '	Dim ForceDP As Short
    '	Dim kbestdpspace As Short
    '	Dim jtic As Short
    '	Dim kMapArea As Short
    '	Dim jflow As Integer
    '	Dim AANPV As Single
    '	Dim jpresAA As Integer
    '	Dim BestAApres As Integer
    '	Dim mxNPV As Single
    '	Dim AAarea As Single
    '	Dim kmloc As Short
    '	Dim jMap As Short
    '	Dim KAA As Integer
    '	Dim jaa As Integer
    '	Dim kdpspaceAA As Integer
    '	Dim jaaset As Short
    '	Dim relabel As Integer
    '	Dim kaatotal As Integer

    '	If HaveReadDPfilenames <= 0 Then
    '		Call ReadDPdataFilenames()
    '	End If

    '       ReDim CondTypeFlow(Ncoversite, MxAgeClass, NMapArea, MxTic)
    '       ReDim MTypeFlow(NMType, NMLocation, MxTic)

    '	'  Evaluate existing stands one at a time

    '	kaatotal = 0
    '	relabel = 0

    '	' Loop through the set of input data.
    '	' Data is stored in blocks of AA's with multiple files for each block
    '       For jaaset = 1 To AAnDataSet

    '           'ebh: set DPSpace poly index at first shot through the dataset...increment
    '           '       at the end of the "If polyidDpspace = polyidDualplan Then" statement below
    '           polyidDpspace = AAdpSpace(1).polyid
    '           kdpspaceAA = 1

    '           'Open "debug.txt" For Output As #99
    '           For jaa = 1 To AASetNAA(kaaset) 'ebh: kaaset is initially set @ read main input and modified from there
    '               polyidDualplan = AA(jaa).polyid

    '               If relabel > 4999 Then
    '                   MainForm.TxtPolyDone.Text = Str(kaatotal)
    '                   MainForm.TxtPolyDone.Refresh()
    '                   relabel = 0
    '               End If

    '               kaatotal = kaatotal + 1
    '               relabel = relabel + 1
    '               KAA = jaa
    '               ReDim MapLayerColor(NMapLayer)
    '               For jMap = 1 To NMapLayer
    '                   'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(jMap). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                   MapLayerColor(jMap) = AA(jaa).MapLayerColor(jMap)
    '               Next jMap

    '               kmloc = AA(jaa).mloc
    '               AAarea = AA(jaa).area

    '               mxNPV = -99999
    '               BestAApres = -9

    '               'ebh: this for-next finds max NPV
    '               For jpresAA = AA(KAA).firstpres To AA(KAA).lastpres

    '                   AANPV = 0
    '                   '  first add values of all market flows
    '                   For jflow = AApres(jpresAA).FirstFlow To AApres(jpresAA + 1).FirstFlow - 1
    '                       AANPV = AANPV + AAflow(jflow).quan * MtypePr(AAflow(jflow).type, kmloc, AAflow(jflow).tic)
    '                   Next jflow

    '                   ' Then add values associated with biological conditions of each map layer
    '                   For jMap = 1 To NMapLayer
    '                       'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                       'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                       kMapArea = MapAreaID(jMap, MapLayerColor(jMap))
    '                       For jtic = 1 To NTic + 1
    '                           AANPV = AANPV + CondTypePr(AApres(jpresAA).Coversite(jtic), AApres(jpresAA).Age(jtic), kMapArea, jtic)
    '                       Next jtic
    '                   Next jMap

    '                   'ebh: search for the Pres with the hightest NPV
    '                   If AANPV > mxNPV Then
    '                       mxNPV = AANPV
    '                       BestAApres = jpresAA
    '                   End If

    '               Next jpresAA


    '               kbestdpspace = 0 'whether to use the DPSpace solution to tally flows or not...
    '               If polyidDpspace = polyidDualplan Then

    '                   '7/10/03 HMH Change rules on forcing DPspace solution

    '                   'If AAdpSpace(kdpspaceAA&).NPVallMx > mxNPV! * AAarea! Then
    '                   ' do not use DPspace solution if no spatial value possible
    '                   If AAdpSpace(kdpspaceAA).NPVallMx - AAdpSpace(kdpspaceAA).NPVAspat > 0.01 Then

    '                       ForceDP = 0

    '                       If AAarea > PolySizeMinForWindows Then

    '                           ' determine aspatial NPV of DPspace model 1 pres
    '                           kpres = AAdpSpace(kdpspaceAA).presAA
    '                           AspatNPV = 0

    '                           For jflow = AApres(kpres).FirstFlow To AApres(kpres + 1).FirstFlow - 1
    '                               kmtype = AAflow(jflow).type
    '                               ktic = AAflow(jflow).tic
    '                               AspatNPV = AspatNPV + MtypePr(kmtype, kmloc, ktic) * AAflow(jflow).quan
    '                           Next jflow

    '                           ' Add Info for biological conditions for each map for first rotation
    '                           For jMap = 1 To NMapLayer
    '                               'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                               'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                               kMapArea = MapAreaID(jMap, MapLayerColor(jMap))
    '                               For jtic = 1 To NTic + 1
    '                                   AspatNPV = AspatNPV + CondTypePr(AApres(kpres).Coversite(jtic), AApres(kpres).Age(jtic), kMapArea, jtic)
    '                               Next jtic
    '                           Next jMap

    '                           AspatNPV = AspatNPV * AAarea
    '                           SpatMnVal = AAdpSpace(kdpspaceAA).NPVallMn - AAdpSpace(kdpspaceAA).NPVAspat
    '                           DPacceptedLoss = AAdpSpace(kdpspaceAA).NPVAspatMx - AAdpSpace(kdpspaceAA).NPVAspat

    '                           'hmh 1/31/2004
    '                           'Newloss! = mxNPV! * AAarea! - AspatNPV!
    '                           Newloss = mxNPV * AAarea - AspatNPV - SpatMnVal

    '                           bestKRegTic = AApres(BestAApres).RgTic

    '                           If Newloss < DPacceptedLoss + MDisc(bestKRegTic) * (MxAspatialLoss * AAarea) Then ForceDP = 1
    '                           'If ForceDP = 0 Then Stop
    '                       Else 'EBH: What's the logic? Write it out in commentary
    '                           ThisPolySpatialMx = AAdpSpace(kdpspaceAA).NPVallMx - AAdpSpace(kdpspaceAA).NPVAspat
    '                           If ThisPolySpatialMx > DPspatlock Then ForceDP = 1
    '                       End If



    '                       If ForceDP > 0 Then
    '                           CountDPused = CountDPused + 1
    '                           kbestdpspace = 1
    '                           ' sum flows from dpspace model 1 pres
    '                           kpres = AAdpSpace(kdpspaceAA).presAA

    '                           For jflow = AApres(kpres).FirstFlow To AApres(kpres + 1).FirstFlow - 1
    '                               kmtype = AAflow(jflow).type
    '                               ktic = AAflow(jflow).tic
    '                               MTypeFlow(kmtype, kmloc, ktic) = MTypeFlow(kmtype, kmloc, ktic) + AAarea * AAflow(jflow).quan
    '                           Next jflow

    '                           ' Add Info for biological conditions for each map for first rotation
    '                           For jMap = 1 To NMapLayer
    '                               'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                               'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                               kMapArea = MapAreaID(jMap, MapLayerColor(jMap))
    '                               For jtic = 0 To NTic
    '                                   CondTypeFlow(AApres(kpres).Coversite(jtic), AApres(kpres).Age(jtic), kMapArea, jtic) = CondTypeFlow(AApres(kpres).Coversite(jtic), AApres(kpres).Age(jtic), kMapArea, jtic) + AAarea
    '                               Next jtic
    '                           Next jMap
    '                       End If
    '                       ' end of force DP

    '                   End If

    '                   If kdpspaceAA < AAdpFilesize(kaaset) Then kdpspaceAA = kdpspaceAA + 1
    '                   polyidDpspace = AAdpSpace(kdpspaceAA).polyid
    '               End If

    '               ' kw why two blocks now (above and below) -- just determine best alt (kpres&) and sum -- stored same now!
    '               If kbestdpspace = 0 Then

    '                   ' Add info for forest-wide totals
    '                   kpres = BestAApres

    '                   '  Add Market flows for first rotation

    '                   For jflow = AApres(kpres).FirstFlow To AApres(kpres + 1).FirstFlow - 1
    '                       kmtype = AAflow(jflow).type
    '                       ktic = AAflow(jflow).tic
    '                       MTypeFlow(kmtype, kmloc, ktic) = MTypeFlow(kmtype, kmloc, ktic) + AAarea * AAflow(jflow).quan
    '                   Next jflow

    '                   ' Add Info for biological conditions for each map for first rotation
    '                   For jMap = 1 To NMapLayer
    '                       'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                       'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                       kMapArea = MapAreaID(jMap, MapLayerColor(jMap))
    '                       For jtic = 0 To NTic
    '                           CondTypeFlow(AApres(kpres).Coversite(jtic), AApres(kpres).Age(jtic), kMapArea, jtic) = CondTypeFlow(AApres(kpres).Coversite(jtic), AApres(kpres).Age(jtic), kMapArea, jtic) + AAarea
    '                       Next jtic
    '                   Next jMap


    '                   ' sum flows from Future Rotations

    '               End If
    '           Next jaa

    '           If jaaset < AAnDataSet Then

    '               kaaset = kaaset + kaasetdirection

    '               ReDim AAflow(AASetNFlow(kaaset))
    '               ReDim AApres(AASetNPres(kaaset))
    '               ReDim AA(AASetNAA(kaaset))

    '               LoadSubForest(AAFlowFN(kaaset), AAPresFN(kaaset), AAFN(kaaset))

    '           End If

    '       Next jaaset
    '	kaasetdirection = kaasetdirection * (-1)

    '	MainForm.TxtPolyDone.Text = Str(kaatotal)
    '	MainForm.Label1.Text = "Finished this iteration"
    '	MainForm.Label1.Refresh()

    'End Sub









    ''ORIGINAL - WORKED OK
    ''Public Sub SpatialScheduleRefine(ByRef ProposedRx() As Long)
    ''    'This refines an aspatial schedule before you go to the tally routines and price adjustment routines...no subforest information
    ''    'called from this subroutine

    ''    'subroutine expands on the regular dualplan schedule subroutine and includes values for spatial interactions
    ''    'between prescriptions chosen from the previous dualplan iteration
    ''    'for each polygon, this considers how its neighbors are managed and chooses (and stores) the best schedule
    ''    'subsequently evaluated polygons may be evaluated relative to the chosen polygon schedule and may or may not
    ''    'keep their original schedule

    ''    'sub trims out spatial max prescriptions that are less than the lowest non-spatial NPV value
    ''    ' so...even if the stand got credit for ALL of the interior space it could create + outside its borders, it couldn't compete 
    ''    ' with the best "NPVLbnd" - 'spatial minimum' Rx, then trim it out...

    ''    'uses the AA, AAPres, AAFlow as well as the Spatial, Market, and Condition set shadow price data
    ''    'to calculate the NPV values for all DualPlan model 1 prescriptions...
    ''    'Dim kaa As Integer 'the index of the anchor stand
    ''    Dim NRx As Integer
    ''    Dim kMapArea As Integer
    ''    Dim kaa As Integer
    ''    Dim krx As Integer
    ''    Dim kind As Integer
    ''    Dim kspat As Integer
    ''    Dim kmtype As Integer
    ''    Dim jmap As Integer
    ''    Dim AAArea As Double 'the area of the AA
    ''    Dim InteriorISpaceAreaRatio As Double 'ratio of the 1-way Izone area in the center of stand to the area of the stand
    ''    Dim zoneID As Integer
    ''    'Dim InfStands() As Integer 'a list of the influenced stands to try to match schedules against.
    ''    'Dim NInfStands As Integer = 0 'number of influenced stands
    ''    Dim TicAreaVal() As Double 'for all stands influenced by the stand, what's the per-tic potential area in each time period?
    ''    Dim ZoneAreaVal() As Double 'for the evaluated zone, what's the potential ISpace by Tic independent of the stand being evaluated, but accounting for the chosen schedules of the other two stands
    ''    Dim TicTally() As Integer 'whether the izone produces core area in each period
    ''    Dim ktic As Short
    ''    'Dim kmtype As Short
    ''    'Dim kpres As Integer
    ''    Dim jflow As Integer
    ''    Dim AANPV As Single
    ''    Dim AANSNPV As Single 'non-spatial aanpv - includes value of 1-way Izone space in the interior of the stand
    ''    Dim BestAApres As Integer
    ''    Dim mxNPV As Single
    ''    Dim mxNSNPV As Single
    ''    Dim kmloc As Short
    ''    Dim kcoversite As Short
    ''    Dim kage As Integer
    ''    Dim relabel As Integer
    ''    Dim kaatotal As Integer
    ''    Dim ZoneArea As Double 'total area in the influence zone
    ''    'Dim TicMult As Integer
    ''    Dim NRxChange As Integer 'the number of prescriptions that this iteration changes...loop until it all 'shakes out'

    ''    Dim EntAdjFactor As Double 'adjustment factor for size-based entry cost
    ''    Dim AARxNPV(,) As Single 'by aa, prescription


    ''    '#############This added for debug; can compute efficiency another place
    ''    Dim ZoneEff(,) As Single 'efficiency of the zone in creating Interior space - used to modify/calculate assumed added value to the stand, by zone, tic
    ''    Dim Efficiency As Double = 1
    ''    ReDim ZoneEff(NZoneSubFor, MxTic)
    ''    For jzone As Integer = 1 To NZoneSubFor
    ''        For jtic As Integer = 1 To MxTic
    ''            ZoneEff(jzone, jtic) = Efficiency
    ''        Next
    ''    Next

    ''    '  Evaluate existing stands one at a time

    ''    'First, get the non-spatial value of each stand, each prescription, so you don't have to do this for every loop
    ''    ReDim AARxNPV(AA_NPolys, MxStandRx)
    ''    'First, find the value of the aspatial flows
    ''    For jaa As Integer = 1 To AA_NPolys
    ''        Do Until AA_polyid(jaa) > 0 'control for blank aa information
    ''            jaa = jaa + 1
    ''            If jaa > AA_NPolys Then Exit For
    ''        Loop

    ''        'set parameters at the AA level
    ''        ReDim MapLayerColor(NMapLayer)
    ''        For jmap = 1 To NMapLayer
    ''            MapLayerColor(jmap) = AA_MapLayerColor(jaa, jmap)
    ''        Next jmap

    ''        kmloc = AA_mloc(jaa)
    ''        AAArea = AA_area(jaa)
    ''        'InteriorISpaceAreaRatio = DualPlanAAPoly(jaa).ISpaceSelf / AAArea
    ''        'kaa = jaa
    ''        NRx = AA_NPres(jaa) 'AA(jaa).PresArray.Length - 1
    ''        kmloc = AA_mloc(jaa)
    ''        AAArea = AA_area(jaa)
    ''        InteriorISpaceAreaRatio = DualPlanAAPoly(jaa).ISpaceSelf / AAArea

    ''        For jrx As Integer = 1 To NRx 'jpresAA = AA(KAA).firstpres To AA(KAA).lastpres
    ''            krx = AA_PresArray(jaa, jrx) 'krx used to access line in prescription database
    ''            kcoversite = AApres_Coversite(krx, ktic)
    ''            kage = AApres_Age(krx, ktic)
    ''            kind = AApres_ISpaceInd(krx)
    ''            kspat = AApres_SpaceType(krx)


    ''            'jpresAA = AA(jaa).PresArray(j)
    ''            AANPV = 0
    ''            AANSNPV = 0
    ''            '  first add values of all market flows

    ''            For jflow = AApres_FirstFlow(krx) To AApres_FirstFlow(krx + 1) - 1
    ''                ktic = AAflow_tic(jflow)
    ''                kcoversite = AApres_Coversite(krx, ktic)
    ''                kage = AApres_Age(krx, ktic)
    ''                'DEBUG: Control for long rotations
    ''                If ktic > MxTic Then Exit For
    ''                kmtype = AAflow_type(jflow)
    ''                EntAdjFactor = 1
    ''                For jEnt As Integer = 1 To EntryAdjNEquation
    ''                    For jEntDef As Integer = 1 To EntryAdjNDef(jEnt)
    ''                        If kcoversite >= EntryAdjkCoverSiteBeg(jEnt, jEntDef) And kcoversite <= EntryAdjkCoverSiteEnd(jEnt, jEntDef) And _
    ''                           kage >= EntryAdjkagebeg(jEnt, jEntDef) And kage <= EntryAdjkageend(jEnt, jEntDef) And _
    ''                           AAflow_type(jflow) >= MTypeEntryCostBeg(jEnt, jEntDef) And AAflow_type(jflow) <= MTypeEntryCostEnd(jEnt, jEntDef) Then
    ''                            'If AAflow_type(jflow) = EntryAdjMType(jEnt) Then
    ''                            'have to figure out the area-based factor
    ''                            EntAdjFactor = AdjFactor(jEnt, AAArea)
    ''                            Exit For
    ''                        End If
    ''                    Next jEntDef
    ''                Next
    ''                AANPV = AANPV + AAflow_quan(jflow) * MtypePr(kmtype, kmloc, ktic) * EntAdjFactor
    ''            Next jflow
    ''            AANSNPV = AANPV

    ''            ' Then add values associated with biological conditions and spatial conditions of each map layer
    ''            ktic = Math.Max(AApres_RgTic(krx) + AApres_RgRotLen(krx), NTic) 'the regen rx is summed through the full rotation and a land val is added after that point
    ''            If ktic > MxTic Then ktic = MxTic 'DEBUG: Control for long rotations
    ''            For jtic As Integer = 1 To ktic 'the regen Rx will begin tally before the end of the planning horizon, but then adjust everything to NTIC +1
    ''                'For jtic As Integer = 1 To NTic 'the regen Rx will begin tally before the end of the planning horizon, but then adjust everything to NTIC +1
    ''                'first condition type prices
    ''                For jmap = 1 To NMapLayer
    ''                    kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
    ''                    AANPV = AANPV + CondTypePr(AApres_Coversite(krx, jtic), AApres_Age(krx, jtic), kMapArea, jtic)
    ''                    AANSNPV = AANSNPV + CondTypePr(AApres_Coversite(krx, jtic), AApres_Age(krx, jtic), kMapArea, jtic)

    ''                Next jmap
    ''                'then spatial type prices for the 1-way influence zone
    ''                AANPV = AANPV + SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * InteriorISpaceAreaRatio
    ''                AANSNPV = AANSNPV + SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * InteriorISpaceAreaRatio

    ''            Next jtic


    ''            '-----------------------------------------
    ''            'Add info. for future rotations...only for Rx other than grow-only
    ''            'DEBUG: - ADDED THE RGROTLEN+RGTIC < MXTIC FOR LONG ROTATIONS - IF YOU CAN'T GET IT IN, IT'S ESSENTIALLY A GROW-ONLY
    ''            If AApres_RgRotLen(krx) > 0 And AApres_RgTic(krx) + AApres_RgRotLen(krx) < MxTic Then
    ''                ktic = AApres_RgTic(krx) + AApres_RgRotLen(krx)
    ''                kcoversite = AApres_Coversite(krx, ktic)

    ''                If MktLandValue_Calculated(kaaset, kmloc, krx) = 0 Then CalcMktLandVal(kmloc, krx)
    ''                AANPV = AANPV + MktLandValue_LandVal(kaaset, kmloc, krx) * CondDisc(ktic - NTic - 1)
    ''                AANSNPV = AANSNPV + MktLandValue_LandVal(kaaset, kmloc, krx) * CondDisc(ktic - NTic - 1)

    ''                'add in value for condition type flows
    ''                For jmap = 1 To NMapLayer
    ''                    kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
    ''                    If CondLandValue_Calculated(kaaset, kMapArea, krx) = 0 Then CalcCondLandVal(kMapArea, krx)
    ''                    'discount the appropriate land value by the number of periods beyond NTic + 1 the regen occurs
    ''                    'add flows for condition types
    ''                    AANPV = AANPV + CondLandValue_LandVal(kaaset, kMapArea, krx) * CondDisc(ktic - NTic - 1)
    ''                    AANSNPV = AANSNPV + CondLandValue_LandVal(kaaset, kMapArea, krx) * CondDisc(ktic - NTic - 1)

    ''                Next

    ''                'add in any value for spatial flows - of the interior of the stand
    ''                If SpatLandValue_Calculated(kaaset, kspat, krx) = 0 Then CalcSpatLandVal(krx, kind, kspat)
    ''                AANPV = AANPV + SpatLandValue_LandVal(kaaset, kspat, krx) * CondDisc(ktic - NTic - 1) * InteriorISpaceAreaRatio
    ''                AANSNPV = AANSNPV + SpatLandValue_LandVal(kaaset, kspat, krx) * CondDisc(ktic - NTic - 1) * InteriorISpaceAreaRatio
    ''            End If
    ''            AARxNPV(jaa, jrx) = AANPV
    ''        Next jrx
    ''    Next jaa


    ''    kaatotal = 0
    ''    relabel = 0

    ''    ' Loop through the set of input data.
    ''    ' Data is stored in blocks of AA's with multiple files for each block

    ''    'For jaaset As Integer = 1 To AAnDataSet
    ''    NRxChange = AA_NPolys
    ''    Do Until NRxChange / AA_NPolys <= OneOptStop 'used to be until nrxchange = 0
    ''        NRxChange = 0

    ''        For jaa As Integer = 1 To AA_NPolys 'AASetNAA(kaaset)
    ''            Do Until AA_polyid(jaa) > 0 'control for blank aa information
    ''                jaa = jaa + 1
    ''                If jaa > AA_NPolys Then Exit For
    ''            Loop
    ''            'If jaa = 4204 Then Stop
    ''            If relabel > 4999 Then
    ''                MainForm.TxtPolyDone.Text = Str(kaatotal)
    ''                MainForm.TxtPolyDone.Refresh()
    ''                MainForm.Refresh()
    ''                relabel = 0
    ''            End If

    ''            kaatotal = kaatotal + 1
    ''            relabel = relabel + 1

    ''            'set parameters at the AA level
    ''            ReDim MapLayerColor(NMapLayer)
    ''            For jmap = 1 To NMapLayer
    ''                MapLayerColor(jmap) = AA_MapLayerColor(jaa, jmap)
    ''            Next jmap

    ''            kmloc = AA_mloc(jaa)
    ''            AAArea = AA_area(jaa)
    ''            'InteriorISpaceAreaRatio = DualPlanAAPoly(jaa).ISpaceSelf / AAArea
    ''            'kaa = jaa
    ''            NRx = AA_NPres(jaa) 'AA(jaa).PresArray.Length - 1
    ''            mxNPV = -99999
    ''            mxNSNPV = -99999
    ''            BestAApres = 0

    ''            'look at the schedule of the neighbors and figure out the maximum area in this stand that 
    ''            'would be in ISpace if the evaluated schedule matches with them

    ''            'figure out the influenced stands to evaluate prescriptions against...
    ''            'go through influence zones this stand is in...populate a list of influenced stands to consider

    ''            ReDim TicAreaVal(MxTic)
    ''            ReDim TicTally(MxTic)
    ''            ReDim ZoneAreaVal(MxTic)

    ''            For jiz As Integer = 1 To DualPlanAAPoly(jaa).IZones.Length - 1
    ''                zoneID = DualPlanAAPoly(jaa).IZones(jiz)
    ''                ReDim ZoneAreaVal(MxTic)
    ''                'If KeepRx = 1 Then Exit For

    ''                For jtic As Integer = 1 To MxTic
    ''                    ZoneAreaVal(jtic) = 1 'default to 1
    ''                Next
    ''                ZoneArea = 0
    ''                For jjaa As Integer = 1 To Zonedim(zoneID)
    ''                    kaa = ZoneAAlist(zoneID, jjaa)
    ''                    If kaa <> jaa Then
    ''                        'krx = PolySolRx(kaa).ChosenRxInd
    ''                        krx = ProposedRx(kaa)
    ''                        kind = AApres_ISpaceInd(krx)
    ''                        For jtic As Integer = 1 To MxTic
    ''                            ZoneAreaVal(jtic) = ZoneAreaVal(jtic) * SpatialSeries(kind).Flows(jtic)
    ''                        Next
    ''                    Else
    ''                        '    'tally the potential area 
    ''                        '    For jtic As Integer = 1 To MxTic
    ''                        '       ZoneAreaVal(jtic) = ZoneAreaVal(jtic) * ZoneAAlistAreaS(zoneID, jjaa) '* SpatialSeries(kind).Flows(jtic)
    ''                        '    Next

    ''                        '^^^^^^^^^^^^^
    ''                        '3/26/10 - for debug, see what happens when you give the stand credit only for the stand's share of the space...
    ''                        'ZoneArea = ZoneAAlistAreaS(zoneID, jjaa)
    ''                    End If
    ''                    '@@@@@@@@@@@@@
    ''                    ZoneArea = ZoneArea + ZoneAAlistAreaS(zoneID, jjaa) '^^^^^^^^^^
    ''                Next
    ''                '7/13 - for debug purposes, see what happens when you give the stand credit for the value of the entire influence zone - more consistent with how aspatial schedule works
    ''                '@@@@@@@@@@@@@
    ''                For jtic As Integer = 1 To MxTic
    ''                    TicAreaVal(jtic) = TicAreaVal(jtic) + ZoneAreaVal(jtic) * ZoneArea * ZoneEff(zoneID, jtic)
    ''                Next
    ''                ''End If
    ''            Next jiz

    ''            'find the prescription for this AA that has the highest value, as lined up with its neighbors.

    ''            ''First, find the value of the aspatial flows
    ''            For jrx As Integer = 1 To NRx 'jpresAA = AA(KAA).firstpres To AA(KAA).lastpres
    ''                krx = AA_PresArray(jaa, jrx) 'krx used to access line in prescription database
    ''                kind = AApres_ISpaceInd(krx)
    ''                kspat = AApres_SpaceType(krx)

    ''                '    'jpresAA = AA(jaa).PresArray(j)
    ''                AANPV = AARxNPV(jaa, jrx)
    ''                AANSNPV = AARxNPV(jaa, jrx)
    ''                '    '  first add values of all market flows

    ''                'only use the values within the range of looking to the MaxTIC...ignore any type of 
    ''                'perpetual series (I think)
    ''                kMapArea = MapAreaID(SpaceDefs_MapLayer(kspat), SpaceDefs_MapColor(kspat))

    ''                For jtic As Integer = SpatialSeries(kind).First1Tic To SpatialSeries(kind).Last1tic '#####NTic + 1
    ''                    AANPV = AANPV + SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * (TicAreaVal(jtic) / AAArea) 'adjusted to per-acre value
    ''                Next jtic
    ''                'AANPV = AANPV + SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * ExteriorISpaceAreaRatio * DualPlanAAPoly(jaa).EstISpacePct


    ''                If AANPV > mxNPV Then
    ''                    mxNPV = AANPV
    ''                    mxNSNPV = AANSNPV
    ''                    BestAApres = krx
    ''                End If
    ''            Next jrx

    ''            'catch the prescription with the highest npv - and track how many polys are changing Rx
    ''            If ProposedRx(jaa) <> BestAApres Then
    ''                NRxChange = NRxChange + 1
    ''                ProposedRx(jaa) = BestAApres
    ''            End If

    ''        Next jaa
    ''    Loop

    ''End Sub












    'Public Sub WriteAAPres()
    '	Dim intAreaColor As Integer
    '	Dim Rowtotal As Integer
    '	Dim jcolor As Short
    '	Dim header As String
    '	Dim file76 As String
    '	Dim Flow As Integer
    '	Dim AreaFlow As Integer
    '	Dim Sum As Double
    '	Dim jharvtype As Short
    '	Dim jprestype As Short
    '	Dim jcoversite As Short
    '	Dim file56 As String
    '	Dim intConvert As Integer
    '	Dim TotalConvert As Single
    '	Dim jfrom As Short
    '	Dim jto As Short
    '	Dim ktab As Short
    '	Dim kPolyFlow As Short
    '	Dim kcountwrite As Short
    '	Dim kticfirst As Short
    '	Dim jrot As Short
    '	Dim zero As Short
    '	Dim NRgRotation As Short
    '	Dim kHarvtype As Short
    '	Dim kcoversite As Short
    '	Dim thisflow As Single
    '	Dim kticfirstflow As Short
    '	Dim kfirstflow As Integer
    '	Dim kcolor As Short
    '	Dim kprestype As Short
    '	Dim kRgbioNext As Short
    '	Dim ThisPolySpatialMx As Single
    '	Dim bestKRegTic As Short
    '	Dim Newloss As Single
    '	Dim DPacceptedLoss As Single
    '	Dim SpatMnVal As Single
    '	Dim kregbiotypeDP As Short
    '	Dim kpresDpjreg As Short
    '	Dim kpresDPAa As Integer
    '	Dim jtime As Short
    '	Dim kTicRg As Short
    '	Dim kRgbioNextDP As Short
    '	Dim DPBestRgOption As Short
    '	Dim AspatNPV As Single
    '	Dim kregtic As Short
    '	Dim kpres As Integer
    '	Dim kRgbioNextDualplan As Short
    '	Dim ForceDPregen As Short
    '	Dim ForceDP As Short
    '	Dim kbestdpspace As Short
    '	Dim jlayer As Short
    '	Dim regval As Single
    '	Dim TVAL As Single
    '	Dim ktime As Short
    '	Dim FirstTic As Short
    '	Dim SEV As Single
    '	Dim jage As Short
    '	Dim ktic As Short
    '	Dim kmtype As Short
    '	Dim kage As Short
    '	Dim jitem As Integer
    '	Dim NetPV As Single
    '	Dim rotlen As Short
    '	Dim kpresRG As Integer
    '	Dim jpresRG As Integer
    '	Dim kRgBioType As Short
    '	Dim jreg As Short
    '	Dim PresRegTic As Short
    '	Dim kMapArea As Short
    '	Dim jflow As Integer
    '	Dim AANPV As Single
    '	Dim jpresAA As Integer
    '	Dim jtic As Short
    '	Dim EntryCostFactor As Single
    '	Dim BestRgOption As Short
    '	Dim BestAApres As Integer
    '	Dim mxNPV As Single
    '	Dim kcoversiteAA As Short
    '	Dim AAarea As Single
    '	Dim kmloc As Short
    '	Dim jMap As Short
    '	Dim KAA As Integer
    '	Dim jaa As Integer
    '	Dim kdpspaceAA As Integer
    '	Dim jaaset As Short
    '	Dim relabel As Integer
    '	Dim kaatotal As Integer
    '       Dim Nprestype As Short
    '       Dim j As Integer

    '	CountDPused = 0
    '	If HaveReadDPfilenames <= 0 Then
    '		Call ReadDPdataFilenames()
    '		'moved to block at start of schedule -- files may need to be created first
    '		' read input from dpspace

    '		' hmh 1/30/2004 -- must process subforests in same order as dualplan
    '		'Open AAdpSpaceFN$(1) For Binary Access Read As #4
    '		'ReDim AAdpSpace(AAdpFilesize&(1)) As Model1PresOut
    '		'Get #4, , AAdpSpace()
    '		'Close #4
    '	End If




    '	' Nprestype was not set on input.  Use 17 for now
    '	If Nprestype < 1 Then Nprestype = 17

    '	'UPGRADE_WARNING: Lower bound of array MapMtypeFlow was changed from 1,1,1,1,0 to 0,0,0,0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '	ReDim MapMtypeFlow(NMapArea, Ncoversite, 17, NTic, 1)
    '	'UPGRADE_WARNING: Lower bound of array MapAreaFlow was changed from 1,1,1,1,0 to 0,0,0,0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '	ReDim MapAreaFlow(NMapArea, Ncoversite, 17, NTic, 1)
    '	'UPGRADE_WARNING: Lower bound of array AreaColorPrestype was changed from 1,1 to 0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '	ReDim AreaColorPrestype(NMapArea, 17)


    '	MainForm.Label1.Text = "Writing AA Output File"""
    '	MainForm.TxtPolyDone.Text = "0"
    '	MainForm.Refresh()


    '	Call SetTypePrices()
    '	'Write #26, "file#    polyid    area   MapColor(1 to nMap)         BAadj,    mxnpv    HarvType  pres#  ConvOpt#  rgtic       3 Sets Regen: (Rotlen  Prestype  Pres#)              Ageclass(tic0 to Ntic)               Coversite(tic0 to Ntic)"
    '	'Write #26, "file#,   polyid,   area,  MC1, MC2, MC3, MC4, MC5, MC6,BAadj,    mxNPV,   HarvType, pres#, ConvOpt#, rgtic,   Rot1, Ptype1, P#1,  Rot2, Ptype2, P#2,  Rot3, Ptype3, P#3,      A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10,      C0, C1, C2, C3, C4, C5, C6, C7, C8, C9, C10"
    '	WriteLine(26, "file#", "polyid", "area", "MC1", "MC2", "MC3", "MC4", "MC5", "MC6", "BAadj", "mxNPV", "FirstHarvTic", "HarvType", "pres#", "ConvOpt#", "rgtic", "Rot1", "Ptype1", "Pres#1", "Rot2", "Ptype2", "Pres#2", "Rot3", "Ptype3", "Pres#3", "A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "C0", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "C10", "y1", "y2", "y3", "y4", "y5", "y6", "y7", "y8", "y9", "y10")

    '	'ReDim CondTypeFlow#(Ncoversite, MxAgeClass, NMapArea, 0 To MxTic)
    '	'ReDim MTypeFlow#(NMType, NMLocation, MxTic)

    '	Dim PolyAge() As Object
    '	Dim PolyCoversite() As Object

    '       Dim ConversionPoly(,) As Integer
    '       Dim ConversionArea(,) As Single


    '	'  Evaluate existing stands one at a time

    '	'UPGRADE_WARNING: Lower bound of array ConversionPoly was changed from 0,0 to 0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '	ReDim ConversionPoly(Ncoversite, Ncoversite)
    '	'UPGRADE_WARNING: Lower bound of array ConversionArea was changed from 0,0 to 0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '	ReDim ConversionArea(Ncoversite, Ncoversite)

    '	'UPGRADE_WARNING: Lower bound of array EntryCostTicThisAA was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '	ReDim EntryCostTicThisAA(MxTic)
    '	'UPGRADE_WARNING: Lower bound of array LandVal was changed from 1,1 to 0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '	ReDim LandVal(NRgBioType, MxTic)

    '	kaatotal = 0
    '	relabel = 0

    '	' Loop through the set of input data.
    '	' Data is stored in blocks of AA's with three files for each block
    '	For jaaset = 1 To AAnDataSet


    '		'kaaset = jaaset
    '		' load the three AA tables for this set

    '		'ReDim AAflow(AASetNFlow&(kaaset)) As FlowData
    '		'ReDim AApres(AASetNPres&(kaaset)) As AApresData
    '		'ReDim AA(AASetNAA&(kaaset)) As AAdata

    '		'Open AAFlowFN$(kaaset) For Binary Access Read As #4
    '		'Get #4, , AAflow()
    '		'Close #4

    '		'Open AAPresFN$(kaaset) For Binary Access Read As #4
    '		'Get #4, , AApres()
    '		'Close #4

    '		'Open AAFN$(kaaset) For Binary Access Read As #4
    '		'Get #4, , AA()
    '		'Close #4
    '		polyidDpspace = AAdpSpace(1).polyid
    '		kdpspaceAA = 1
    '		For jaa = 1 To AASetNAA(kaaset)

    '			polyidDualplan = AA(jaa).polyid


    '			If relabel > 999 Then
    '				MainForm.TxtPolyDone.Text = Str(kaatotal)
    '				MainForm.TxtPolyDone.Refresh()
    '				relabel = 0
    '			End If


    '			kaatotal = kaatotal + 1
    '			relabel = relabel + 1
    '			KAA = jaa
    '			'UPGRADE_WARNING: Lower bound of array MapLayerColor was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '			ReDim MapLayerColor(NMapLayer)
    '			For jMap = 1 To NMapLayer
    '				'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(jMap). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '				MapLayerColor(jMap) = AA(jaa).MapLayerColor(jMap)
    '			Next jMap

    '			kmloc = AA(jaa).mloc
    '			AAarea = AA(jaa).area
    '			'AAage = AA(jaa&).Ageclass
    '			kcoversiteAA = AA(jaa).Coversite

    '			mxNPV = -99999
    '			BestAApres = -9
    '			BestRgOption = -9

    '			' move LandVal! Redim outside polygon loop  2/2002
    '			'ReDim LandVal!(NRgBioType, MxTic)
    '			'UPGRADE_WARNING: Lower bound of array LandOpt was changed from 1,1 to 0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '			ReDim LandOpt(NRgBioType, MxTic)

    '			'set the fixed entry cost for this AA on a per land unit basis
    '			'ReDim EntryCostThisPoly!(MxTic)
    '			EntryCostFactor = 1# / AAarea
    '			For jtic = 1 To MxTic
    '				EntryCostTicThisAA(jtic) = EntryCostTic(kmloc, jtic) * EntryCostFactor
    '			Next jtic





    '               For j = 1 To AA(KAA).PresArray.Length - 1 'jpresAA = AA(KAA).firstpres To AA(KAA).lastpres
    '                   jpresAA = AA(jaa).PresArray(j)

    '                   AANPV = 0
    '                   '  first add values of all market flows

    '                   'change to eliminate aapres().lastflow
    '                   'For jflow& = AApres(jpresAA&).FirstFlow To AApres(jpresAA&).LastFlow
    '                   For jflow = AApres(jpresAA).FirstFlow To AApres(jpresAA + 1).FirstFlow - 1
    '                       AANPV = AANPV + AAflow(jflow).quan * MtypePr(AAflow(jflow).type, kmloc, AAflow(jflow).tic)
    '                   Next jflow

    '                   ' Then add values associated with biological conditions of each map layer
    '                   For jMap = 1 To NMapLayer
    '                       'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                       'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                       kMapArea = MapAreaID(jMap, MapLayerColor(jMap))
    '                       For jtic = 1 To AApres(jpresAA).RgTic - 1
    '                           AANPV = AANPV + CondTypePr(AApres(jpresAA).Coversite(jtic), AApres(jpresAA).Age(jtic), kMapArea, jtic)
    '                       Next jtic
    '                   Next jMap



    '                   PresRegTic = AApres(jpresAA).RgTic

    '                   'Add cost of entering AA at end of rotation
    '                   AANPV = AANPV + EntryCostTicThisAA(PresRegTic)


    '                   ' then consider regen options

    '                   For jreg = 1 To AApres(jpresAA).NRgAlt
    '                       kRgBioType = AApres(jpresAA).RgBioType(jreg)
    '                       ' if this bareland type has not been considered for this AA, then
    '                       ' analyze it.

    '                       'If kRgBioType = 13 Then Stop

    '                       If LandOpt(kRgBioType, NTic1) = 0 Then
    '                           '   initialize best option info assuming one option specified must
    '                           '   be selected, i.e., no assumed "do nothing"
    '                           For jtic = 1 To NTic1
    '                               LandVal(kRgBioType, jtic) = -9999
    '                           Next jtic

    '                           '   Determine SEVs (LandVal!())FOR THIS kRgBioType for periods beyond the end
    '                           '   of the planning horizon.

    '                           For jpresRG = RgBioType(kRgBioType).firstpres To RgBioType(kRgBioType).lastpres
    '                               kpresRG = jpresRG
    '                               rotlen = RgPres(kpresRG).Rotlength

    '                               NetPV = 0
    '                               ' Add NPV from market returns
    '                               For jitem = RgPres(kpresRG).FirstFlow To RgPres(kpresRG).LastFlow
    '                                   kage = RgFlow(jitem).tic
    '                                   kmtype = RgFlow(jitem).type
    '                                   ktic = NTic1 + kage
    '                                   NetPV = NetPV + MtypePr(kmtype, kmloc, ktic) * RgFlow(jitem).quan
    '                               Next jitem

    '                               ' Add NPV from conditions for all map layers
    '                               For jMap = 1 To NMapLayer
    '                                   ktic = NTic + 1
    '                                   'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                   'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                   kMapArea = MapAreaID(jMap, MapLayerColor(jMap))
    '                                   For jage = 1 To rotlen
    '                                       'NetPV! = NetPV! + CondTypePr!(kRgBioType, jage, kMapArea, ktic)
    '                                       NetPV = NetPV + CondTypePr(RgPres(kpresRG).Coversite(jage), RgPres(kpresRG).Age(jage), kMapArea, ktic)
    '                                       ktic = ktic + 1
    '                                   Next jage
    '                               Next jMap


    '                               'Add cost of entering AA at end of rotation
    '                               NetPV = NetPV + EntryCostTicThisAA(NTic1 + rotlen)


    '                               SEV = NetPV * SEVFactor(rotlen)
    '                               If SEV > LandVal(kRgBioType, NTic1) Then
    '                                   LandVal(kRgBioType, NTic1) = SEV
    '                                   LandOpt(kRgBioType, NTic1) = jpresRG
    '                               End If
    '                           Next jpresRG

    '                           ' Set LandVal!() for periods beyond NTIC1

    '                           For jtic = NTic1 + 1 To MxTic
    '                               LandVal(kRgBioType, jtic) = LandVal(kRgBioType, jtic - 1) * MDisc(2)
    '                           Next jtic

    '                           ' DETERMINE LandVal!() FOR EACH Possible TIC STARTING WITH LAST PERIOD

    '                           FirstTic = AA(KAA).FirstRgTic
    '                           For jtic = NTic To FirstTic Step -1
    '                               For jpresRG = RgBioType(kRgBioType).firstpres To RgBioType(kRgBioType).lastpres
    '                                   kpresRG = jpresRG
    '                                   rotlen = RgPres(kpresRG).Rotlength

    '                                   NetPV = 0

    '                                   ' add values for market flows
    '                                   For jitem = RgPres(kpresRG).FirstFlow To RgPres(kpresRG).LastFlow
    '                                       ktime = jtic + RgFlow(jitem).tic
    '                                       kmtype = RgFlow(jitem).type
    '                                       NetPV = NetPV + MtypePr(kmtype, kmloc, ktime) * RgFlow(jitem).quan
    '                                   Next jitem

    '                                   '  Add value for conditions for all map layers
    '                                   For jMap = 1 To NMapLayer
    '                                       'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                       'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                       kMapArea = MapAreaID(jMap, MapLayerColor(jMap))
    '                                       ktic = jtic

    '                                       For jage = 1 To rotlen
    '                                           'NetPV! = NetPV! + CondTypePr!(kRgBioType, jage, kMapArea, ktic)
    '                                           NetPV = NetPV + CondTypePr(RgPres(kpresRG).Coversite(jage), RgPres(kpresRG).Age(jage), kMapArea, ktic)
    '                                           ktic = ktic + 1
    '                                       Next jage
    '                                   Next jMap

    '                                   'Add cost of entering AA at end of rotation
    '                                   NetPV = NetPV + EntryCostTicThisAA(jtic + rotlen)


    '                                   TVAL = NetPV + LandVal(kRgBioType, jtic + rotlen)
    '                                   If TVAL > LandVal(kRgBioType, jtic) Then
    '                                       LandVal(kRgBioType, jtic) = TVAL
    '                                       LandOpt(kRgBioType, jtic) = kpresRG
    '                                   End If
    '                               Next jpresRG
    '                           Next jtic
    '                       End If

    '                       regval = AApres(jpresAA).RgCost(jreg) * MtypePr(MTypeSiteConvCost, kmloc, PresRegTic) + LandVal(kRgBioType, PresRegTic)
    '                       If AANPV + regval > mxNPV Then
    '                           mxNPV = AANPV + regval
    '                           BestAApres = jpresAA
    '                           BestRgOption = jreg
    '                       End If
    '                   Next jreg
    '               Next j


    '			Write(26, kaaset)
    '			Write(26, AA(jaa).polyid)
    '			Write(26, AA(jaa).area)
    '			For jlayer = 1 To 6
    '				Write(26, AA(jaa).MapLayerColor(jlayer))
    '			Next jlayer




    '			kbestdpspace = 0
    '			If polyidDpspace = polyidDualplan Then

    '				'7/10/03 HMH Change rules on forcing DPspace solution

    '				'If AAdpSpace(kdpspaceAA&).NPVallMx > mxNPV! * AAarea! Then
    '				' do not use DPspace solution if no spatial value possible
    '				If AAdpSpace(kdpspaceAA).NPVallMx - AAdpSpace(kdpspaceAA).NPVAspat > 0.01 Then

    '					ForceDP = 0
    '					ForceDPregen = 1

    '					kRgbioNextDualplan = AApres(BestAApres).RgBioType(BestRgOption)

    '					If AAarea > PolySizeMinForWindows Then

    '						' determine aspatial NPV of DPspace model 1 pres
    '						kpres = AAdpSpace(kdpspaceAA).presAA
    '						kregtic = AApres(kpres).RgTic
    '						AspatNPV = 0

    '						DPBestRgOption = AAdpSpace(kdpspaceAA).jreg
    '						kRgbioNextDP = AApres(kdpspaceAA).RgBioType(DPBestRgOption)



    '						For jflow = AApres(kpres).FirstFlow To AApres(kpres + 1).FirstFlow - 1
    '							kmtype = AAflow(jflow).type
    '							ktic = AAflow(jflow).tic
    '							AspatNPV = AspatNPV + MtypePr(kmtype, kmloc, ktic) * AAflow(jflow).quan
    '						Next jflow
    '						AspatNPV = AspatNPV + EntryCostTicThisAA(kregtic)
    '						'AspatNPV! = AspatNPV! + AApres(kpres&).RgCost(DPBestRgOption) * MDisc!(kregtic)
    '						AspatNPV = AspatNPV + AApres(kpres).RgCost(DPBestRgOption) * MtypePr(MTypeSiteConvCost, kmloc, kregtic)



    '						' Add Info for biological conditions for each map for first rotation
    '						For jMap = 1 To NMapLayer
    '							'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							kMapArea = MapAreaID(jMap, MapLayerColor(jMap))
    '							For jtic = 1 To kregtic - 1
    '								'CondTypeFlow#(kcoversiteAA, AAage + jtic, kMapArea, jtic) = CondTypeFlow#(kcoversiteAA, AAage + jtic, kMapArea, jtic) + AAArea!
    '								AspatNPV = AspatNPV + CondTypePr(AApres(kpres).Coversite(jtic), AApres(kpres).Age(jtic), kMapArea, jtic)
    '							Next jtic
    '						Next jMap


    '						If AAdpSpace(kdpspaceAA).pres1rg > 0 And kregtic < NTic1 Then

    '							kpres = AAdpSpace(kdpspaceAA).pres1rg
    '							' Add market flows
    '							For jitem = RgPres(kpres).FirstFlow To RgPres(kpres).LastFlow
    '								ktime = kregtic + RgFlow(jitem).tic
    '								kmtype = RgFlow(jitem).type
    '								AspatNPV = AspatNPV + MtypePr(kmtype, kmloc, ktime) * RgFlow(jitem).quan
    '							Next jitem

    '							' Add age info for each map
    '							For jMap = 1 To NMapLayer
    '								'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '								'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '								kMapArea = MapAreaID(jMap, MapLayerColor(jMap))
    '								kTicRg = 0
    '								For jtime = kregtic To kregtic + RgPres(kpres).Rotlength - 1
    '									kTicRg = kTicRg + 1
    '									AspatNPV = AspatNPV + CondTypePr(RgPres(kpres).Coversite(kTicRg), RgPres(kpres).Age(kTicRg), kMapArea, jtime)
    '								Next jtime
    '							Next jMap
    '							kregtic = kregtic + RgPres(kpres).Rotlength


    '							AspatNPV = AspatNPV + EntryCostTicThisAA(kregtic)

    '							If AAdpSpace(kdpspaceAA).npresrg > 1 And kregtic < NTic1 Then

    '								If kRgbioNextDualplan <> kRgbioNextDP Then ForceDPregen = 0

    '								kpres = AAdpSpace(kdpspaceAA).pres2rg
    '								' Add market flows
    '								For jitem = RgPres(kpres).FirstFlow To RgPres(kpres).LastFlow
    '									ktime = kregtic + RgFlow(jitem).tic
    '									kmtype = RgFlow(jitem).type
    '									AspatNPV = AspatNPV + MtypePr(kmtype, kmloc, ktime) * RgFlow(jitem).quan
    '								Next jitem

    '								' Add age info for each map
    '								For jMap = 1 To NMapLayer
    '									'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '									'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '									kMapArea = MapAreaID(jMap, MapLayerColor(jMap))
    '									kTicRg = 0
    '									For jtime = kregtic To kregtic + RgPres(kpres).Rotlength - 1
    '										kTicRg = kTicRg + 1
    '										AspatNPV = AspatNPV + CondTypePr(RgPres(kpres).Coversite(kTicRg), RgPres(kpres).Age(kTicRg), kMapArea, jtime)
    '									Next jtime
    '								Next jMap
    '								kregtic = kregtic + RgPres(kpres).Rotlength
    '								AspatNPV = AspatNPV + EntryCostTicThisAA(kregtic)
    '								If AAdpSpace(kdpspaceAA).npresrg > 2 And kregtic < NTic1 Then
    '									kpres = AAdpSpace(kdpspaceAA).pres3rg

    '									' Add market flows
    '									For jitem = RgPres(kpres).FirstFlow To RgPres(kpres).LastFlow
    '										ktime = kregtic + RgFlow(jitem).tic
    '										kmtype = RgFlow(jitem).type
    '										AspatNPV = AspatNPV + MtypePr(kmtype, kmloc, ktime) * RgFlow(jitem).quan
    '									Next jitem

    '									' Add age info for each map
    '									For jMap = 1 To NMapLayer
    '										'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '										'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '										kMapArea = MapAreaID(jMap, MapLayerColor(jMap))
    '										kTicRg = 0
    '										For jtime = kregtic To kregtic + RgPres(kpres).Rotlength - 1
    '											kTicRg = kTicRg + 1
    '											AspatNPV = AspatNPV + CondTypePr(RgPres(kpres).Coversite(kTicRg), RgPres(kpres).Age(kTicRg), kMapArea, jtime)
    '										Next jtime
    '									Next jMap
    '									kregtic = kregtic + RgPres(kpres).Rotlength
    '									AspatNPV = AspatNPV + EntryCostTicThisAA(kregtic)
    '								End If
    '								' end of pres3rg
    '							End If
    '							' end of pres2rg
    '						End If
    '						' end of pres1rg

    '						kpresDPAa = AAdpSpace(kdpspaceAA).presAA
    '						kpresDpjreg = AAdpSpace(kdpspaceAA).jreg
    '						kregbiotypeDP = AApres(kpresDPAa).RgBioType(kpresDpjreg)

    '						AspatNPV = AspatNPV + LandVal(kregbiotypeDP, kregtic)
    '						AspatNPV = AspatNPV * AAarea
    '						SpatMnVal = AAdpSpace(kdpspaceAA).NPVallMn - AAdpSpace(kdpspaceAA).NPVAspat
    '						DPacceptedLoss = AAdpSpace(kdpspaceAA).NPVAspatMx - AAdpSpace(kdpspaceAA).NPVAspat

    '						'hmh 1/31/2004
    '						'Newloss! = mxNPV! * AAarea! - AspatNPV!
    '						Newloss = mxNPV * AAarea - AspatNPV - SpatMnVal


    '						bestKRegTic = AApres(BestAApres).RgTic

    '						If Newloss < DPacceptedLoss + MDisc(bestKRegTic) * (MxAspatialLoss * AAarea) Then ForceDP = 1

    '					Else
    '						ThisPolySpatialMx = AAdpSpace(kdpspaceAA).NPVallMx - AAdpSpace(kdpspaceAA).NPVAspat
    '						If ThisPolySpatialMx > DPspatlock Then ForceDP = 1
    '					End If

    '					'6/9/2004 debug hmh
    '					'ForceDP = -99

    '					If ForceDP > 0 Then

    '						CountDPused = CountDPused + 1
    '						'If AAdpSpace(kdpspaceAA&).NPVallMx > mxNPV! * AAarea! Then
    '						kbestdpspace = 1
    '						' sum flows from dpspace model 1 pres
    '						kpres = AAdpSpace(kdpspaceAA).presAA
    '						kregtic = AApres(kpres).RgTic
    '						DPBestRgOption = AAdpSpace(kdpspaceAA).jreg
    '						kRgbioNext = AApres(kpres).RgBioType(DPBestRgOption)
    '						'add to totals for first rotation summary
    '						' do not include open land  KcoversiteAA = Ncoversite in summary
    '						' *****
    '						' 6/02/03,
    '						kprestype = AApres(kpres).PresType
    '						If kcoversiteAA < Ncoversite Then
    '							For jMap = 1 To NMapLayer
    '								'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '								'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '								kcolor = MapAreaID(jMap, MapLayerColor(jMap))
    '								AreaColorPrestype(kcolor, kprestype) = AreaColorPrestype(kcolor, kprestype) + AAarea
    '							Next jMap
    '						End If
    '						'UPGRADE_WARNING: Lower bound of array PolyFlow was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '						ReDim PolyFlow(MxTic)
    '						'UPGRADE_WARNING: Lower bound of array PolyHarvType was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '						ReDim PolyHarvType(MxTic)

    '						'UPGRADE_WARNING: Couldn't resolve default property of object PolyHarvType(kregtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '						PolyHarvType(kregtic) = 1
    '						If AApres(kpres).PresType < Nprestype Then
    '							kfirstflow = AApres(kpres).FirstFlow
    '							kticfirstflow = AAflow(kfirstflow).tic
    '						Else
    '							kticfirstflow = 999
    '						End If


    '						'  Add Market flows for first rotation
    '						For jflow = AApres(kpres).FirstFlow To AApres(kpres + 1).FirstFlow - 1
    '							kmtype = AAflow(jflow).type
    '							ktic = AAflow(jflow).tic
    '							thisflow = Timberfactor(kmtype) * AAflow(jflow).quan
    '							'PolyFlow! is on a per acre basis
    '							PolyFlow(ktic) = PolyFlow(ktic) + thisflow
    '							'MTypeFlow#(kmtype, kmloc, kTic) = MTypeFlow#(kmtype, kmloc, kTic) + AAArea! * AAflow(jflow&).quan
    '						Next jflow


    '						' add flows from first rotation to forest-wide totals
    '						kcoversite = AApres(kpres).Coversite(0)
    '						For jtic = 1 To NTic
    '							thisflow = PolyFlow(jtic) * AAarea
    '							If thisflow > 0.0001 Then
    '								'UPGRADE_WARNING: Couldn't resolve default property of object PolyHarvType(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '								kHarvtype = PolyHarvType(jtic)
    '								For jMap = 1 To NMapLayer
    '									'kcolor = MapLayerColor(jMap)
    '									'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '									'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '									kcolor = MapAreaID(jMap, MapLayerColor(jMap))
    '									MapMtypeFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) = MapMtypeFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) + thisflow
    '									MapAreaFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) = MapAreaFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) + AAarea
    '								Next jMap
    '							End If
    '						Next jtic







    '						' write polygon info
    '						Write(26, AApres(kpres).BAadjust)
    '						Write(26, mxNPV)
    '						Write(26, kticfirstflow)
    '						Write(26, AApres(kpres).PresType)
    '						Write(26, kpres)
    '						Write(26, DPBestRgOption)
    '						Write(26, AApres(kpres).RgTic)

    '						'UPGRADE_WARNING: Lower bound of array PolyAge was changed from 0 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '						ReDim PolyAge(MxTic)
    '						'UPGRADE_WARNING: Lower bound of array PolyCoversite was changed from 0 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '						ReDim PolyCoversite(MxTic)

    '						For jtic = 0 To kregtic - 1
    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							PolyCoversite(jtic) = AApres(kpres).Coversite(jtic)
    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyAge(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							PolyAge(jtic) = AApres(kpres).Age(jtic)
    '						Next jtic




    '						If ForceDPregen = 0 Then


    '							' set krgbiotype after AA regen
    '							kRgbioNext = AApres(kpres).RgBioType(BestRgOption)
    '							NRgRotation = 0
    '							Do Until kregtic > NTic
    '								kpres = LandOpt(kRgbioNext, kregtic)
    '								Write(26, RgPres(kpres).Rotlength)
    '								Write(26, RgPres(kpres).PresType)
    '								Write(26, kpres)
    '								NRgRotation = NRgRotation + 1
    '								' Add age/coversite info for this regen rotation

    '								kTicRg = 0
    '								For jtic = kregtic To kregtic + RgPres(kpres).Rotlength - 1
    '									kTicRg = kTicRg + 1
    '									'UPGRADE_WARNING: Couldn't resolve default property of object PolyAge(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '									PolyAge(jtic) = RgPres(kpres).Age(kTicRg)
    '									'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '									PolyCoversite(jtic) = RgPres(kpres).Coversite(kTicRg)
    '								Next jtic
    '								kregtic = kregtic + RgPres(kpres).Rotlength
    '							Loop 

    '							' add conversion info for forestwide summary

    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(NTic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							ConversionArea(PolyCoversite(0), PolyCoversite(NTic)) = ConversionArea(PolyCoversite(0), PolyCoversite(NTic)) + AA(jaa).area
    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(NTic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							ConversionPoly(PolyCoversite(0), PolyCoversite(NTic)) = ConversionPoly(PolyCoversite(0), PolyCoversite(NTic)) + 1

    '							' fixed format on output so fill regen fields
    '							zero = 0
    '							For jrot = NRgRotation + 1 To 3
    '								Write(26, zero)
    '								Write(26, zero)
    '								Write(26, zero)
    '							Next jrot

    '							' hmh 6/7/2004 fix -- reset kregtic to end of rotation for existing stand
    '							kpres = AAdpSpace(kdpspaceAA).presAA
    '							kregtic = AApres(kpres).RgTic


    '							' sum flows from Future Rotations

    '							Do Until kregtic > NTic
    '								kticfirst = kregtic
    '								kpres = LandOpt(kRgbioNext, kregtic)
    '								kprestype = RgPres(kpres).PresType
    '								kcoversite = RgPres(kpres).Coversite(1)
    '								'UPGRADE_WARNING: Lower bound of array PolyFlowRG was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '								ReDim PolyFlowRG(MxTic)
    '								' Add market flows
    '								For jitem = RgPres(kpres).FirstFlow To RgPres(kpres).LastFlow
    '									ktime = kregtic + RgFlow(jitem).tic
    '									kmtype = RgFlow(jitem).type
    '									thisflow = Timberfactor(kmtype) * RgFlow(jitem).quan
    '									PolyFlow(ktime) = PolyFlow(ktime) + thisflow
    '									PolyFlowRG(ktime) = PolyFlowRG(ktime) + thisflow
    '									'MTypeFlow#(kmtype, kmloc, ktime) = MTypeFlow#(kmtype, kmloc, ktime) + RgFlow(jitem&).quan * AAArea!
    '								Next jitem

    '								'UPGRADE_WARNING: Couldn't resolve default property of object PolyHarvType(kregtic + RgPres().Rotlength). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '								PolyHarvType(kregtic + RgPres(kpres).Rotlength) = 1

    '								' add flows from this regen rotation
    '								For jtic = kregtic To NTic
    '									thisflow = PolyFlowRG(jtic) * AAarea
    '									If thisflow > 0.001 Then
    '										'UPGRADE_WARNING: Couldn't resolve default property of object PolyHarvType(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '										kHarvtype = PolyHarvType(jtic)
    '										For jMap = 1 To NMapLayer
    '											'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '											'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '											kcolor = MapAreaID(jMap, MapLayerColor(jMap))
    '											MapMtypeFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) = MapMtypeFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) + thisflow
    '											MapAreaFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) = MapAreaFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) + AAarea
    '										Next jMap
    '									End If
    '								Next jtic
    '								kregtic = kregtic + RgPres(kpres).Rotlength
    '							Loop 


    '							' force DPregen

    '						Else
    '							' hmh 6/7/2004 fix -- reset kregtic to end of rotation for existing stand
    '							kpres = AAdpSpace(kdpspaceAA).presAA
    '							kregtic = AApres(kpres).RgTic



    '							kcountwrite = 0
    '							' change 5/29/03
    '							'If AAdpSpace(kdpspaceAA&).npresrg > 0 Then
    '							If AAdpSpace(kdpspaceAA).pres1rg > 0 And kregtic < NTic1 Then

    '								kpres = AAdpSpace(kdpspaceAA).pres1rg
    '								kcountwrite = kcountwrite + 1
    '								Write(26, RgPres(kpres).Rotlength)
    '								Write(26, RgPres(kpres).PresType)
    '								Write(26, kpres)
    '								NRgRotation = NRgRotation + 1
    '								' Add age/coversite info for this regen rotation
    '								kTicRg = 0
    '								For jtic = kregtic To kregtic + RgPres(kpres).Rotlength - 1
    '									kTicRg = kTicRg + 1
    '									'UPGRADE_WARNING: Couldn't resolve default property of object PolyAge(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '									PolyAge(jtic) = RgPres(kpres).Age(kTicRg)
    '									'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '									PolyCoversite(jtic) = RgPres(kpres).Coversite(kTicRg)
    '								Next jtic

    '								kprestype = RgPres(kpres).PresType
    '								kcoversite = RgPres(kpres).Coversite(1)
    '								'UPGRADE_WARNING: Lower bound of array PolyFlowRG was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '								ReDim PolyFlowRG(MxTic)

    '								' Add market flows
    '								For jitem = RgPres(kpres).FirstFlow To RgPres(kpres).LastFlow
    '									ktime = kregtic + RgFlow(jitem).tic
    '									kmtype = RgFlow(jitem).type
    '									thisflow = Timberfactor(kmtype) * RgFlow(jitem).quan
    '									PolyFlow(ktime) = PolyFlow(ktime) + thisflow
    '									PolyFlowRG(ktime) = PolyFlowRG(ktime) + thisflow
    '									'MTypeFlow#(kmtype, kmloc, ktime) = MTypeFlow#(kmtype, kmloc, ktime) + RgFlow(jitem&).quan * AAArea!
    '								Next jitem

    '								'UPGRADE_WARNING: Couldn't resolve default property of object PolyHarvType(kregtic + RgPres().Rotlength). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '								PolyHarvType(kregtic + RgPres(kpres).Rotlength) = 1

    '								' add flows from this regen rotation
    '								For jtic = kregtic To NTic
    '									thisflow = PolyFlowRG(jtic) * AAarea
    '									If thisflow > 0.001 Then
    '										'UPGRADE_WARNING: Couldn't resolve default property of object PolyHarvType(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '										kHarvtype = PolyHarvType(jtic)
    '										For jMap = 1 To NMapLayer
    '											'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '											'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '											kcolor = MapAreaID(jMap, MapLayerColor(jMap))
    '											MapMtypeFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) = MapMtypeFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) + thisflow
    '											MapAreaFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) = MapAreaFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) + AAarea
    '										Next jMap
    '									End If
    '								Next jtic
    '								kregtic = kregtic + RgPres(kpres).Rotlength
    '								' end for pres1rg
    '								' change 5/29/03
    '								If AAdpSpace(kdpspaceAA).npresrg > 1 And kregtic < NTic1 Then
    '									kcountwrite = kcountwrite + 1
    '									kpres = AAdpSpace(kdpspaceAA).pres2rg
    '									Write(26, RgPres(kpres).Rotlength)
    '									Write(26, RgPres(kpres).PresType)
    '									Write(26, kpres)
    '									NRgRotation = NRgRotation + 1
    '									' Add age/coversite info for this regen rotation
    '									kTicRg = 0
    '									For jtic = kregtic To kregtic + RgPres(kpres).Rotlength - 1
    '										kTicRg = kTicRg + 1
    '										'UPGRADE_WARNING: Couldn't resolve default property of object PolyAge(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '										PolyAge(jtic) = RgPres(kpres).Age(kTicRg)
    '										'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '										PolyCoversite(jtic) = RgPres(kpres).Coversite(kTicRg)
    '									Next jtic

    '									kprestype = RgPres(kpres).PresType
    '									kcoversite = RgPres(kpres).Coversite(1)
    '									'UPGRADE_WARNING: Lower bound of array PolyFlowRG was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '									ReDim PolyFlowRG(MxTic)

    '									' Add market flows
    '									For jitem = RgPres(kpres).FirstFlow To RgPres(kpres).LastFlow
    '										ktime = kregtic + RgFlow(jitem).tic
    '										kmtype = RgFlow(jitem).type
    '										thisflow = Timberfactor(kmtype) * RgFlow(jitem).quan
    '										PolyFlow(ktime) = PolyFlow(ktime) + thisflow
    '										PolyFlowRG(ktime) = PolyFlowRG(ktime) + thisflow
    '										'MTypeFlow#(kmtype, kmloc, ktime) = MTypeFlow#(kmtype, kmloc, ktime) + RgFlow(jitem&).quan * AAArea!
    '									Next jitem

    '									'UPGRADE_WARNING: Couldn't resolve default property of object PolyHarvType(kregtic + RgPres().Rotlength). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '									PolyHarvType(kregtic + RgPres(kpres).Rotlength) = 1

    '									' add flows from this regen rotation
    '									For jtic = kregtic To NTic
    '										thisflow = PolyFlowRG(jtic) * AAarea
    '										If thisflow > 0.001 Then
    '											'UPGRADE_WARNING: Couldn't resolve default property of object PolyHarvType(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '											kHarvtype = PolyHarvType(jtic)
    '											For jMap = 1 To NMapLayer
    '												'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '												'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '												kcolor = MapAreaID(jMap, MapLayerColor(jMap))
    '												MapMtypeFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) = MapMtypeFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) + thisflow
    '												MapAreaFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) = MapAreaFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) + AAarea
    '											Next jMap
    '										End If
    '									Next jtic
    '									kregtic = kregtic + RgPres(kpres).Rotlength
    '									' change 5/29/03
    '									If AAdpSpace(kdpspaceAA).npresrg > 2 And kregtic < NTic1 Then
    '										kcountwrite = kcountwrite + 1
    '										kpres = AAdpSpace(kdpspaceAA).pres3rg
    '										Write(26, RgPres(kpres).Rotlength)
    '										Write(26, RgPres(kpres).PresType)
    '										Write(26, kpres)
    '										NRgRotation = NRgRotation + 1
    '										' Add age/coversite info for this regen rotation
    '										kTicRg = 0
    '										For jtic = kregtic To kregtic + RgPres(kpres).Rotlength - 1
    '											kTicRg = kTicRg + 1
    '											'UPGRADE_WARNING: Couldn't resolve default property of object PolyAge(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '											PolyAge(jtic) = RgPres(kpres).Age(kTicRg)
    '											'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '											PolyCoversite(jtic) = RgPres(kpres).Coversite(kTicRg)
    '										Next jtic

    '										kprestype = RgPres(kpres).PresType
    '										kcoversite = RgPres(kpres).Coversite(1)
    '										'UPGRADE_WARNING: Lower bound of array PolyFlowRG was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '										ReDim PolyFlowRG(MxTic)

    '										' Add market flows
    '										For jitem = RgPres(kpres).FirstFlow To RgPres(kpres).LastFlow
    '											ktime = kregtic + RgFlow(jitem).tic
    '											kmtype = RgFlow(jitem).type
    '											thisflow = Timberfactor(kmtype) * RgFlow(jitem).quan
    '											PolyFlow(ktime) = PolyFlow(ktime) + thisflow
    '											PolyFlowRG(ktime) = PolyFlowRG(ktime) + thisflow
    '											'MTypeFlow#(kmtype, kmloc, ktime) = MTypeFlow#(kmtype, kmloc, ktime) + RgFlow(jitem&).quan * AAArea!
    '										Next jitem

    '										'UPGRADE_WARNING: Couldn't resolve default property of object PolyHarvType(kregtic + RgPres().Rotlength). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '										PolyHarvType(kregtic + RgPres(kpres).Rotlength) = 1

    '										' add flows from this regen rotation
    '										For jtic = kregtic To NTic
    '											thisflow = PolyFlowRG(jtic) * AAarea
    '											If thisflow > 0.001 Then
    '												'UPGRADE_WARNING: Couldn't resolve default property of object PolyHarvType(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '												kHarvtype = PolyHarvType(jtic)
    '												For jMap = 1 To NMapLayer
    '													'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '													'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '													kcolor = MapAreaID(jMap, MapLayerColor(jMap))
    '													MapMtypeFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) = MapMtypeFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) + thisflow
    '													MapAreaFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) = MapAreaFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) + AAarea
    '												Next jMap
    '											End If
    '										Next jtic
    '										kregtic = kregtic + RgPres(kpres).Rotlength


    '									End If
    '									' end of pres3rg

    '								End If
    '								' end of pres2rg


    '							End If
    '							' end of pres1rg


    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(NTic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							ConversionArea(PolyCoversite(0), PolyCoversite(NTic)) = ConversionArea(PolyCoversite(0), PolyCoversite(NTic)) + AA(jaa).area
    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(NTic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							ConversionPoly(PolyCoversite(0), PolyCoversite(NTic)) = ConversionPoly(PolyCoversite(0), PolyCoversite(NTic)) + 1

    '							' fixed format on output so fill regen fields
    '							zero = 0

    '							' fixed 6/02/03 Wei & HMH
    '							' fixed again HMH 1/2004
    '							'kloop = 4
    '							'If AAdpSpace(kdpspaceAA&).pres1rg = 0 Or kregtic >= NTic1 Then
    '							'kloop = 1
    '							'ElseIf AAdpSpace(kdpspaceAA&).pres2rg = 0 Then
    '							'kloop = 2
    '							'ElseIf AAdpSpace(kdpspaceAA&).pres3rg = 0 Then
    '							'kloop = 3
    '							'End If

    '							'For jrot = kloop To 3
    '							'For jrot = AAdpSpace(kdpspaceAA&).npresrg + 1 To 3
    '							For jrot = kcountwrite + 1 To 3
    '								Write(26, zero)
    '								Write(26, zero)
    '								Write(26, zero)
    '							Next jrot
    '						End If
    '						'end of force Dp regen
    '					End If
    '				End If

    '				'kdpspaceAA& = kdpspaceAA& + 1
    '				If kdpspaceAA < AAdpFilesize(kaaset) Then kdpspaceAA = kdpspaceAA + 1 'wei, 5/30/03

    '				polyidDpspace = AAdpSpace(kdpspaceAA).polyid
    '			End If

    '			'end of dpspace option


    '			If kbestdpspace = 0 Then

    '				' Add info for forest-wide totals

    '				kpres = BestAApres
    '				kregtic = AApres(kpres).RgTic
    '				kprestype = AApres(kpres).PresType

    '				'add to totals for first rotation summary
    '				' do not include open land  KcoversiteAA = Ncoversite in summary
    '				' *****

    '				If kcoversiteAA < Ncoversite Then
    '					For jMap = 1 To NMapLayer
    '						'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '						'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '						kcolor = MapAreaID(jMap, MapLayerColor(jMap))
    '						AreaColorPrestype(kcolor, kprestype) = AreaColorPrestype(kcolor, kprestype) + AAarea
    '					Next jMap
    '				End If




    '				'UPGRADE_WARNING: Lower bound of array PolyFlow was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '				ReDim PolyFlow(MxTic)
    '				'UPGRADE_WARNING: Lower bound of array PolyHarvType was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '				ReDim PolyHarvType(MxTic)

    '				'UPGRADE_WARNING: Couldn't resolve default property of object PolyHarvType(kregtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '				PolyHarvType(kregtic) = 1

    '				'For jmap = 1 To NMapLayer
    '				'MapLayerColor(jmap) = AA(jaa&).MapLayerColor(jmap)
    '				'Next jmap



    '				If AApres(kpres).PresType < Nprestype Then
    '					kfirstflow = AApres(kpres).FirstFlow
    '					kticfirstflow = AAflow(kfirstflow).tic
    '				Else
    '					kticfirstflow = 999
    '				End If



    '				'write #26, "file#, polyid, area, MapColor(1 to nMap)";
    '				'write #26, "mxnpv, aaprestype filepres# ConvOption# rgtic";
    '				'write #26, "3 sets Regen:(Rotlength, Prestype, Pres#)";
    '				'write #26, "Ageclass(tic0 to ntic), Coversite(tic0 to ntic)"

    '				' write polygon info
    '				Write(26, AApres(kpres).BAadjust)
    '				Write(26, mxNPV)
    '				Write(26, kticfirstflow)
    '				Write(26, AApres(kpres).PresType)
    '				Write(26, kpres)
    '				Write(26, BestRgOption)
    '				Write(26, AApres(kpres).RgTic)


    '				'UPGRADE_WARNING: Lower bound of array PolyAge was changed from 0 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '				ReDim PolyAge(MxTic)
    '				'UPGRADE_WARNING: Lower bound of array PolyCoversite was changed from 0 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '				ReDim PolyCoversite(MxTic)


    '				For jtic = 0 To kregtic - 1
    '					'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '					PolyCoversite(jtic) = AApres(kpres).Coversite(jtic)
    '					'UPGRADE_WARNING: Couldn't resolve default property of object PolyAge(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '					PolyAge(jtic) = AApres(kpres).Age(jtic)
    '				Next jtic


    '				' set krgbiotype after AA regen
    '				kRgbioNext = AApres(kpres).RgBioType(BestRgOption)
    '				NRgRotation = 0
    '				Do Until kregtic > NTic
    '					kpres = LandOpt(kRgbioNext, kregtic)
    '					Write(26, RgPres(kpres).Rotlength)
    '					Write(26, RgPres(kpres).PresType)
    '					Write(26, kpres)
    '					NRgRotation = NRgRotation + 1
    '					' Add age/coversite info for this regen rotation

    '					kTicRg = 0
    '					For jtic = kregtic To kregtic + RgPres(kpres).Rotlength - 1
    '						kTicRg = kTicRg + 1
    '						'UPGRADE_WARNING: Couldn't resolve default property of object PolyAge(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '						PolyAge(jtic) = RgPres(kpres).Age(kTicRg)
    '						'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '						PolyCoversite(jtic) = RgPres(kpres).Coversite(kTicRg)
    '					Next jtic
    '					kregtic = kregtic + RgPres(kpres).Rotlength
    '				Loop 

    '				' add conversion info for forestwide summary

    '				'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(NTic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '				'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '				'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '				ConversionArea(PolyCoversite(0), PolyCoversite(NTic)) = ConversionArea(PolyCoversite(0), PolyCoversite(NTic)) + AA(jaa).area
    '				'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(NTic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '				'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '				'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '				ConversionPoly(PolyCoversite(0), PolyCoversite(NTic)) = ConversionPoly(PolyCoversite(0), PolyCoversite(NTic)) + 1

    '				' fixed format on output so fill regen fields
    '				zero = 0
    '				For jrot = NRgRotation + 1 To 3
    '					Write(26, zero)
    '					Write(26, zero)
    '					Write(26, zero)
    '				Next jrot



    '				kpres = BestAApres
    '				kregtic = AApres(kpres).RgTic

    '				'  Add Market flows for first rotation
    '				For jflow = AApres(kpres).FirstFlow To AApres(kpres + 1).FirstFlow - 1
    '					kmtype = AAflow(jflow).type
    '					ktic = AAflow(jflow).tic
    '					thisflow = Timberfactor(kmtype) * AAflow(jflow).quan
    '					'PolyFlow! is on a per acre basis
    '					PolyFlow(ktic) = PolyFlow(ktic) + thisflow
    '					'MTypeFlow#(kmtype, kmloc, kTic) = MTypeFlow#(kmtype, kmloc, kTic) + AAArea! * AAflow(jflow&).quan
    '				Next jflow


    '				' add flows from first rotation to forest-wide totals
    '				kcoversite = AApres(kpres).Coversite(0)
    '				For jtic = 1 To NTic
    '					thisflow = PolyFlow(jtic) * AAarea
    '					If thisflow > 0.0001 Then
    '						'UPGRADE_WARNING: Couldn't resolve default property of object PolyHarvType(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '						kHarvtype = PolyHarvType(jtic)
    '						For jMap = 1 To NMapLayer
    '							'kcolor = MapLayerColor(jMap)
    '							'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							kcolor = MapAreaID(jMap, MapLayerColor(jMap))
    '							MapMtypeFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) = MapMtypeFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) + thisflow
    '							MapAreaFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) = MapAreaFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) + AAarea
    '						Next jMap
    '					End If
    '				Next jtic



    '				' sum flows from Future Rotations

    '				Do Until kregtic > NTic
    '					kticfirst = kregtic
    '					kpres = LandOpt(kRgbioNext, kregtic)
    '					kprestype = RgPres(kpres).PresType
    '					kcoversite = RgPres(kpres).Coversite(1)
    '					'UPGRADE_WARNING: Lower bound of array PolyFlowRG was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '					ReDim PolyFlowRG(MxTic)
    '					' Add market flows
    '					For jitem = RgPres(kpres).FirstFlow To RgPres(kpres).LastFlow
    '						ktime = kregtic + RgFlow(jitem).tic
    '						kmtype = RgFlow(jitem).type
    '						thisflow = Timberfactor(kmtype) * RgFlow(jitem).quan
    '						PolyFlow(ktime) = PolyFlow(ktime) + thisflow
    '						PolyFlowRG(ktime) = PolyFlowRG(ktime) + thisflow
    '						'MTypeFlow#(kmtype, kmloc, ktime) = MTypeFlow#(kmtype, kmloc, ktime) + RgFlow(jitem&).quan * AAArea!
    '					Next jitem

    '					'UPGRADE_WARNING: Couldn't resolve default property of object PolyHarvType(kregtic + RgPres().Rotlength). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '					PolyHarvType(kregtic + RgPres(kpres).Rotlength) = 1

    '					' add flows from this regen rotation
    '					For jtic = kregtic To NTic
    '						thisflow = PolyFlowRG(jtic) * AAarea
    '						If thisflow > 0.001 Then
    '							'UPGRADE_WARNING: Couldn't resolve default property of object PolyHarvType(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '							kHarvtype = PolyHarvType(jtic)
    '							For jMap = 1 To NMapLayer
    '								'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '								'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '								kcolor = MapAreaID(jMap, MapLayerColor(jMap))
    '								MapMtypeFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) = MapMtypeFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) + thisflow
    '								MapAreaFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) = MapAreaFlow(kcolor, kcoversite, kprestype, jtic, kHarvtype) + AAarea
    '							Next jMap
    '						End If
    '					Next jtic
    '					kregtic = kregtic + RgPres(kpres).Rotlength
    '				Loop 
    '			End If


    '			For jtic = 0 To NTic
    '				Write(26, PolyAge(jtic))
    '			Next jtic
    '			For jtic = 0 To NTic
    '				Write(26, PolyCoversite(jtic))
    '			Next jtic
    '			For jtic = 1 To NTic
    '				kPolyFlow = PolyFlow(jtic)
    '				Write(26, kPolyFlow)
    '			Next jtic
    '			WriteLine(26)
    '			'GoTo 366
    '		Next jaa


    '		If jaaset < AAnDataSet Then

    '			kaaset = kaaset + kaasetdirection

    '			'UPGRADE_WARNING: Lower bound of array AAflow was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '			ReDim AAflow(AASetNFlow(kaaset))
    '			'UPGRADE_WARNING: Array AApres may need to have individual elements initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="B97B714D-9338-48AC-B03F-345B617E2B02"'
    '			'UPGRADE_WARNING: Lower bound of array AApres was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '			ReDim AApres(AASetNPres(kaaset))
    '			'UPGRADE_WARNING: Array AA may need to have individual elements initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="B97B714D-9338-48AC-B03F-345B617E2B02"'
    '			'UPGRADE_WARNING: Lower bound of array AA was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '			ReDim AA(AASetNAA(kaaset))

    '			FileOpen(4, AAFlowFN(kaaset), OpenMode.Binary, OpenAccess.Read)
    '			'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '			FileGet(4, AAflow)
    '			FileClose(4)

    '			FileOpen(4, AAPresFN(kaaset), OpenMode.Binary, OpenAccess.Read)
    '			'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '			FileGet(4, AApres)
    '			FileClose(4)

    '			FileOpen(4, AAFN(kaaset), OpenMode.Binary, OpenAccess.Read)
    '			'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '			FileGet(4, AA)
    '			FileClose(4)

    '			' read input from dpspace
    '			FileOpen(4, AAdpSpaceFN(kaaset), OpenMode.Binary, OpenAccess.Read)
    '			'UPGRADE_WARNING: Lower bound of array AAdpSpace was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '			ReDim AAdpSpace(AAdpFilesize(kaaset))
    '			'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '			FileGet(4, AAdpSpace)
    '			FileClose(4)



    '		End If
    '	Next jaaset
    '	kaasetdirection = kaasetdirection * (-1)

    '	FileClose(26)

    '	'366: ktab = 35
    '	ktab = 35
    '	'Write #36, "Coversite"; Tab(ktab);
    '	Write(36, "Coversite")

    '	For jto = 0 To Ncoversite
    '		ktab = ktab + 6
    '		'Write #36, jto; Tab(ktab);
    '		Write(36, jto)
    '	Next jto
    '	WriteLine(36, "total")
    '	For jfrom = 1 To Ncoversite
    '		ktab = 35
    '		'Write #36, coversiteLabel$(jfrom); Tab(ktab);
    '		Write(36, coversiteLabel(jfrom))
    '		TotalConvert = 0
    '		For jto = 0 To Ncoversite
    '			ktab = ktab + 6
    '			TotalConvert = TotalConvert + ConversionPoly(jfrom, jto)
    '			'Write #36, ConversionPoly&(jfrom, jto); Tab(ktab);
    '			Write(36, ConversionPoly(jfrom, jto))
    '		Next jto
    '		WriteLine(36, TotalConvert)
    '	Next jfrom
    '	WriteLine(36)


    '	ktab = 35
    '	'Write #36, "Coversite"; Tab(ktab);
    '	Write(36, "Coversite")

    '	For jto = 0 To Ncoversite
    '		ktab = ktab + 6
    '		'Write #36, jto; Tab(ktab);
    '		Write(36, jto)
    '	Next jto
    '	WriteLine(36, "total")
    '	For jfrom = 1 To Ncoversite
    '		ktab = 35
    '		'Write #36, coversiteLabel$(jfrom); Tab(ktab);
    '		Write(36, coversiteLabel(jfrom))
    '		TotalConvert = 0
    '		For jto = 0 To Ncoversite
    '			ktab = ktab + 6
    '			intConvert = ConversionArea(jfrom, jto)
    '			TotalConvert = TotalConvert + intConvert
    '			'Write #36, intConvert&; Tab(ktab);
    '			Write(36, intConvert)
    '		Next jto
    '		WriteLine(36, TotalConvert)
    '	Next jfrom
    '	FileClose(36)

    '	file56 = FnBaseforOutputFiles & "TreatmentSum" & LTrim(Str(IterationNumber)) & ".csv"
    '	FileOpen(56, file56, OpenMode.Output)

    '	Write(56, "MapLabel$", "CoversiteLabel$", "TreatType", "HarvType")
    '	For jtic = 1 To NTic
    '		Write(56, "AreaTic" & CStr(jtic))
    '	Next jtic
    '	For jtic = 1 To NTic
    '		Write(56, "FlowTic" & CStr(jtic))
    '	Next jtic
    '	WriteLine(56)





















    '	'ReDim MapMtypeFlow#(NMapArea, Ncoversite, 17, NTic, 0 To 1)
    '	'ReDim MapAreaFlow#(NMapArea, Ncoversite, 17, NTic, 0 To 1)


    '	For jMap = 1 To NMapArea
    '		For jcoversite = 1 To Ncoversite
    '			For jprestype = 1 To 16
    '				For jharvtype = 0 To 1

    '					Sum = 0
    '					For jtic = 1 To NTic
    '						Sum = Sum + MapAreaFlow(jMap, jcoversite, jprestype, jtic, jharvtype)
    '					Next jtic
    '					If Sum > 1# Then
    '						Write(56, MapAreaLabel(jMap))
    '						Write(56, coversiteLabel(jcoversite))
    '						Write(56, jprestype, jharvtype)
    '						For jtic = 1 To NTic
    '							AreaFlow = MapAreaFlow(jMap, jcoversite, jprestype, jtic, jharvtype)
    '							Write(56, AreaFlow)
    '						Next jtic
    '						For jtic = 1 To NTic
    '							AreaFlow = MapAreaFlow(jMap, jcoversite, jprestype, jtic, jharvtype)
    '							If AreaFlow > 1 Then
    '								Flow = MapMtypeFlow(jMap, jcoversite, jprestype, jtic, jharvtype) / AreaFlow
    '								Write(56, Flow)
    '							Else
    '								Write(56, "0")
    '							End If
    '						Next jtic
    '						WriteLine(56)
    '					End If
    '				Next jharvtype
    '			Next jprestype
    '		Next jcoversite
    '	Next jMap
    '	FileClose(56)

    '	'UPGRADE_WARNING: Lower bound of array MapMtypeFlow was changed from 1,1,1,1,1 to 0,0,0,0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '	ReDim MapMtypeFlow(1, 1, 1, 1, 1)
    '	'UPGRADE_WARNING: Lower bound of array MapAreaFlow was changed from 1,1,1,1,1 to 0,0,0,0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '	ReDim MapAreaFlow(1, 1, 1, 1, 1)


    '	file76 = FnBaseforOutputFiles & "MapTreat" & LTrim(Str(IterationNumber)) & ".csv"
    '	FileOpen(76, file76, OpenMode.Output)

    '	Write(76, "MapColor")
    '	For jprestype = 1 To Nprestype
    '		header = "treat#" & Str(jprestype)
    '		Write(76, header)
    '	Next jprestype
    '	WriteLine(76, "total")

    '	For jcolor = 1 To NMapArea
    '		Write(76, MapAreaLabel(jcolor))
    '		Rowtotal = 0
    '		For jprestype = 1 To Nprestype
    '			intAreaColor = AreaColorPrestype(jcolor, jprestype)
    '			Write(76, intAreaColor)
    '			Rowtotal = Rowtotal + intAreaColor
    '		Next jprestype
    '		WriteLine(76, Rowtotal)
    '	Next jcolor
    '	FileClose(76)




    '	MainForm.TxtPolyDone.Text = Str(kaatotal)
    '	MainForm.Label1.Text = "Finished writing AApoly file"
    '	MainForm.Label1.Refresh()
    'End Sub

















    'Public Sub ShiftCycleTrimmer(ByVal ksubfor As Integer, ByVal CycleNum As Integer, ByVal WeightScheme As String)
    '    'Takes the current solution from the last DP iteration and allows only a few prescriptions per poly
    '    'on either side - spatial alignment must be within SpatialShift of the chosen prescription

    '    'also ADDS potential prescriptions from other stands not necessarily in current solution, but 
    '    '  that share part of an IZone with a stand in current solution and has the potential to add
    '    '  to the current patch structures.

    '    Dim jaa As Integer
    '    Dim NRx As Integer
    '    Dim jrx As Integer
    '    Dim mrx As Integer 'the index of the last chosen prescription
    '    Dim mind As Integer 'spatial index of the last chosen prescription
    '    Dim mspat As Integer 'spatial type of the last chosen prescription
    '    Dim kaa As Integer
    '    Dim krx As Integer
    '    Dim kind As Integer
    '    Dim kspat As Integer
    '    Dim trimiter As Integer

    '    Dim ZoneTicVal(,) As Double 'tally the total spatial value of each tic for the influence zone - by space type
    '    Dim TallySpatialTics() As Integer 'by space type
    '    Dim Weight As Double 'weight to multiply by spatial value

    '    Dim BestSpatRx() As Long 'chosen Rx if it's a kw rx, otherwise the one chosen that best aligns with fellow IZone stands
    '    Dim HighestSpatRx() As Long 'spatial rx with the highest level of spatial alignment with chosen prescriptions of the neighbors
    '    Dim HighestSpatRxVal() As Double
    '    'Dim HighestSpatRxInd() As Integer 'the "jrx" value of the prescription with the highest spatial index
    '    Dim RxAdded As Integer 'the number of prescriptions added to the minimum set each iteration

    '    ReDim AASpacePotential(AA_NPolys, NSpaceType, MxTic)
    '    ReDim RxIn(AA_NPolys, MxStandRx) ', SolutionRxIn(AA_NPolys, MxStandRx)
    '    ReDim MaxNSRx(AA_NPolys)

    '    ReDim SpatialAlignValue(AA_NPolys, MxStandRx)
    '    ReDim KeepZoneSpace(NSpaceType, NZoneSubFor)

    '    'default weight = 1
    '    Weight = 1

    '    'First, get the default economic/"True" trim values...
    '    If CycleNum = 1 Then 'don't have to do with every cycle
    '        'now get the economic info. for all Dual Plan prescriptions (similar to the SCHEDULE routine, just don't pick yet!)
    '        ReDim EconRxIn(AA_NPolys, MxStandRx) 'what was trimmed based just on econ - a "pure" trim
    '        ReDim NPValMxSpat(AA_NPolys, MxStandRx)
    '        DualPlanRxEcon(DualPlanAAPoly, EconRxIn, MaxNSRx)
    '        trimiter = 0
    '        GlobalTrim = 1
    '        Do Until GlobalTrim = 0
    '            trimiter = trimiter + 1
    '            If trimiter = 1 Then 'go through each trim routine at least once
    '                TrimEcon2(DualPlanAAPoly, EconRxIn, 1, 1) 'use a 1 trimtolerance to not artificially trim treatments
    '                TrimSpatialDom(DualPlanAAPoly, EconRxIn)
    '            Else
    '                'if your last trimspatialdom trimmed anything, re-evaluate with TrimEcon
    '                If GlobalTrim > 0 Then TrimEcon2(DualPlanAAPoly, EconRxIn, 1, 1) 'use a 1 trimtolerance to not artificially trim treatments
    '                'if your last TrimEcon trimmed anything, re-check spatial dominance
    '                If GlobalTrim > 0 Then TrimSpatialDom(DualPlanAAPoly, EconRxIn)
    '            End If
    '        Loop
    '    End If

    '    'First, figure the best KW Rx per stand and default to include the best non-spatial
    '    ReDim BestSpatRx(AA_NPolys)
    '    For jaa = 1 To AA_NPolys
    '        If AA_polyid(jaa) > 0 Then
    '            mrx = PolySolRx_ChosenRxInd(jaa) 'the DualPlan input file prescription index
    '            mind = AApres_ISpaceInd(mrx)
    '            If SpatialSeries(mind).NNonZero > 0 Then
    '                BestSpatRx(jaa) = mrx
    '            End If
    '            'find best non-spatial rx and keep it in
    '            NRx = AA_NPres(jaa)
    '            For jrx = 1 To NRx
    '                krx = AA_PresArray(jaa, jrx)
    '                If mrx = krx Then RxIn(jaa, jrx) = 1 'chosen prescription is always in
    '                kind = AApres_ISpaceInd(krx)
    '                If SpatialSeries(kind).NNonZero = 0 And EconRxIn(jaa, jrx) = 1 Then
    '                    RxIn(jaa, jrx) = 1
    '                    Exit For
    '                End If
    '            Next jrx
    '        End If
    '    Next jaa
    '    'SolutionRxIn = RxIn 'set this here to include solution + best non-spatial

    '    'Then add to the best spatial rx
    '    RxAdded = 1
    '    Do Until RxAdded = 0
    '        RxAdded = 0
    '        ReDim HighestSpatRx(AA_NPolys), HighestSpatRxVal(AA_NPolys) ', HighestSpatRxInd(AA_NPolys)
    '        For jzone As Integer = 1 To NZoneSubFor
    '            If Zonedim(jzone) > 1 Then
    '                ReDim ZoneTicVal(NSpaceDef, NTic)
    '                ReDim TallySpatialTics(NSpaceDef)
    '                If WeightScheme = "AREA" Then Weight = ZoneArea(jzone)
    '                For jdim As Integer = 1 To Zonedim(jzone)
    '                    kaa = ZoneAAlist(jzone, jdim)

    '                    If AA_polyid(kaa) > 0 Then
    '                        'NRx = AA_NPres(kaa)
    '                        mrx = BestSpatRx(kaa) 'PolySolRx_ChosenRxInd(kaa) 'the DualPlan input file prescription index
    '                        mind = AApres_ISpaceInd(mrx)
    '                        mspat = AApres_SpaceType(mrx)
    '                        For jtic As Integer = SpatialSeries(mind).First1Tic To NTic
    '                            ZoneTicVal(mspat, jtic) = ZoneTicVal(mspat, jtic) + SpatialSeries(mind).Flows(jtic) * Weight
    '                            TallySpatialTics(mspat) = TallySpatialTics(mspat) + ZoneTicVal(mspat, jtic)
    '                        Next
    '                    End If
    '                Next
    '                'NEXT, add to the RxIn array those prescription that fall within the SpatialShift tolerance and have made it through the econ. trimmer
    '                For jspat As Integer = 1 To NSpaceDef
    '                    If TallySpatialTics(jspat) > 0 Then 'means there is at least 1 spatial tic in the influence zone
    '                        For jdim As Integer = 1 To Zonedim(jzone)
    '                            kaa = ZoneAAlist(jzone, jdim)
    '                            If AA_polyid(kaa) > 0 Then
    '                                NRx = AA_NPres(kaa)
    '                                mrx = PolySolRx_ChosenRxInd(kaa) 'the DualPlan input file prescription index
    '                                mind = AApres_ISpaceInd(mrx)
    '                                mspat = AApres_SpaceType(mrx)

    '                                For jrx = 1 To NRx
    '                                    'has it made it through the econ filter?
    '                                    If EconRxIn(kaa, jrx) = 1 Then
    '                                        'AANPV = 0
    '                                        krx = AA_PresArray(kaa, jrx)
    '                                        kind = AApres_ISpaceInd(krx)
    '                                        kspat = AApres_SpaceType(krx)
    '                                        If kspat = mspat Or mspat = 0 Then 'adding same spatial type prescriptions to a chosen spatial Rx OR adding spatial prescriptions to a non-spatial chosen prescription
    '                                            For jtic As Integer = 1 To NTic
    '                                                SpatialAlignValue(kaa, jrx) = SpatialAlignValue(kaa, jrx) + SpatialSeries(kind).Flows(jtic) * ZoneTicVal(kspat, jtic)
    '                                            Next
    '                                            If SpatialAlignValue(kaa, jrx) > HighestSpatRxVal(kaa) Then
    '                                                HighestSpatRxVal(kaa) = SpatialAlignValue(kaa, jrx)
    '                                                HighestSpatRx(kaa) = krx 'rx value of the lookup table
    '                                                'HighestSpatRxInd(kaa) = jrx
    '                                            End If
    '                                        End If
    '                                        If SpatialAlignValue(kaa, jrx) > 0 And RxIn(kaa, jrx) = 0 Then
    '                                            RxIn(kaa, jrx) = 1
    '                                            RxAdded = RxAdded + 1
    '                                        End If
    '                                    End If
    '                                Next jrx
    '                            End If
    '                        Next jdim
    '                    End If
    '                Next jspat
    '            End If
    '        Next jzone
    '        'find the best spatial value for those newly-added stands...
    '        For jaa = 1 To AA_NPolys
    '            'If BestSpatRx(jaa) = 0 And HighestSpatRx(jaa) <> 0 Then
    '            BestSpatRx(jaa) = HighestSpatRx(jaa)
    '            'End If
    '        Next
    '    Loop



    '    'now, pare down to the max number of Rx per poly and see if this takes out any economic options
    '    trimiter = 0
    '    GlobalTrim = 0 'don't do another trim iteration if you're not trimming based on max # per poly
    '    If MxPresPerPolyC > 0 Then
    '        TrimMaxRxPerPoly(SpatialAlignValue, RxIn, MxPresPerPolyC, True)
    '        trimiter = 0
    '        GlobalTrim = 1 'DO another trim iteration if you have trimmed out based on max # per poly
    '    End If

    '    'Do this again to see whether you have pared down any more potential prescriptions- but only if you pared down the max # per poly
    '    Do Until GlobalTrim = 0
    '        trimiter = trimiter + 1
    '        If trimiter = 1 Then 'go through each trim routine at least once
    '            TrimEcon2(DualPlanAAPoly, RxIn, 1, 1)
    '            TrimSpatialDom(DualPlanAAPoly, RxIn)
    '        Else
    '            'if your last trimspatialdom trimmed anything, re-evaluate with TrimEcon
    '            If GlobalTrim > 0 Then TrimEcon2(DualPlanAAPoly, RxIn, 1, 1)
    '            'if your last TrimEcon trimmed anything, re-check spatial dominance
    '            If GlobalTrim > 0 Then TrimSpatialDom(DualPlanAAPoly, RxIn)
    '        End If
    '    Loop

    '    Dim tallyRx As Integer = 0
    '    For jaa = 1 To AA_NPolys
    '        NRx = AA_NPres(jaa)
    '        For jrx = 1 To NRx
    '            tallyRx = tallyRx + RxIn(jaa, jrx)
    '        Next
    '    Next

    '    WriteDPSpaceTrimInfo(ksubfor, RxIn)
    '    WriteTrimmedIZoneAndHexInfo(ksubfor)

    'End Sub
End Class


