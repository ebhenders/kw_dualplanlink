<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GridForTrimmer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.tbHexFile = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.tbHexAcres = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.tbPctGridOverlap = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.tbGridSize = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.tbMinFeasGrid = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Button2 = New System.Windows.Forms.Button
        Me.rbFullStandInGrid = New System.Windows.Forms.RadioButton
        Me.rbPartStandInGrid = New System.Windows.Forms.RadioButton
        Me.rbMajorityStandInGrid = New System.Windows.Forms.RadioButton
        Me.cbCircleGrid = New System.Windows.Forms.CheckBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.rbStandBasedGrids = New System.Windows.Forms.RadioButton
        Me.tbPctStandInGrid = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.cbRxFeas = New System.Windows.Forms.CheckBox
        Me.tbNPer = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(80, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(237, 26)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Make Grid For Trimmer"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Hex File to Load:"
        '
        'tbHexFile
        '
        Me.tbHexFile.Location = New System.Drawing.Point(113, 61)
        Me.tbHexFile.Name = "tbHexFile"
        Me.tbHexFile.Size = New System.Drawing.Size(382, 20)
        Me.tbHexFile.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(512, 57)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(71, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Browse"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'tbHexAcres
        '
        Me.tbHexAcres.Location = New System.Drawing.Point(137, 86)
        Me.tbHexAcres.Name = "tbHexAcres"
        Me.tbHexAcres.Size = New System.Drawing.Size(72, 20)
        Me.tbHexAcres.TabIndex = 5
        Me.tbHexAcres.Text = "2"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 89)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Hexagon Acres:"
        '
        'tbPctGridOverlap
        '
        Me.tbPctGridOverlap.Location = New System.Drawing.Point(137, 174)
        Me.tbPctGridOverlap.Name = "tbPctGridOverlap"
        Me.tbPctGridOverlap.Size = New System.Drawing.Size(72, 20)
        Me.tbPctGridOverlap.TabIndex = 7
        Me.tbPctGridOverlap.Text = "75"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 174)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "% Grid Overlap"
        '
        'tbGridSize
        '
        Me.tbGridSize.Location = New System.Drawing.Point(137, 114)
        Me.tbGridSize.Name = "tbGridSize"
        Me.tbGridSize.Size = New System.Drawing.Size(72, 20)
        Me.tbGridSize.TabIndex = 9
        Me.tbGridSize.Text = "1000"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 117)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(85, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Grid Size (Acres)"
        '
        'tbMinFeasGrid
        '
        Me.tbMinFeasGrid.Location = New System.Drawing.Point(137, 145)
        Me.tbMinFeasGrid.Name = "tbMinFeasGrid"
        Me.tbMinFeasGrid.Size = New System.Drawing.Size(72, 20)
        Me.tbMinFeasGrid.TabIndex = 11
        Me.tbMinFeasGrid.Text = "100"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 145)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(127, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Min Feasible Grid (Acres):"
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(226, 230)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(74, 40)
        Me.Button2.TabIndex = 12
        Me.Button2.Text = "GO"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'rbFullStandInGrid
        '
        Me.rbFullStandInGrid.AutoSize = True
        Me.rbFullStandInGrid.Location = New System.Drawing.Point(270, 86)
        Me.rbFullStandInGrid.Name = "rbFullStandInGrid"
        Me.rbFullStandInGrid.Size = New System.Drawing.Size(158, 17)
        Me.rbFullStandInGrid.TabIndex = 13
        Me.rbFullStandInGrid.Text = "Whole Stand must be in grid"
        Me.rbFullStandInGrid.UseVisualStyleBackColor = True
        '
        'rbPartStandInGrid
        '
        Me.rbPartStandInGrid.AutoSize = True
        Me.rbPartStandInGrid.Location = New System.Drawing.Point(270, 109)
        Me.rbPartStandInGrid.Name = "rbPartStandInGrid"
        Me.rbPartStandInGrid.Size = New System.Drawing.Size(172, 17)
        Me.rbPartStandInGrid.TabIndex = 14
        Me.rbPartStandInGrid.Text = "Any part of stand can be in grid"
        Me.rbPartStandInGrid.UseVisualStyleBackColor = True
        '
        'rbMajorityStandInGrid
        '
        Me.rbMajorityStandInGrid.AutoSize = True
        Me.rbMajorityStandInGrid.Checked = True
        Me.rbMajorityStandInGrid.Location = New System.Drawing.Point(270, 132)
        Me.rbMajorityStandInGrid.Name = "rbMajorityStandInGrid"
        Me.rbMajorityStandInGrid.Size = New System.Drawing.Size(178, 17)
        Me.rbMajorityStandInGrid.TabIndex = 15
        Me.rbMajorityStandInGrid.TabStop = True
        Me.rbMajorityStandInGrid.Text = "Most of the stand must be in grid"
        Me.rbMajorityStandInGrid.UseVisualStyleBackColor = True
        '
        'cbCircleGrid
        '
        Me.cbCircleGrid.AutoSize = True
        Me.cbCircleGrid.Checked = True
        Me.cbCircleGrid.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbCircleGrid.Location = New System.Drawing.Point(270, 187)
        Me.cbCircleGrid.Name = "cbCircleGrid"
        Me.cbCircleGrid.Size = New System.Drawing.Size(119, 17)
        Me.cbCircleGrid.TabIndex = 16
        Me.cbCircleGrid.Text = "Create circular grids"
        Me.cbCircleGrid.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(267, 207)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(310, 13)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "If using circular grids, 19% overlap is min. to ensure full coverage"
        '
        'rbStandBasedGrids
        '
        Me.rbStandBasedGrids.AutoSize = True
        Me.rbStandBasedGrids.Location = New System.Drawing.Point(270, 155)
        Me.rbStandBasedGrids.Name = "rbStandBasedGrids"
        Me.rbStandBasedGrids.Size = New System.Drawing.Size(147, 17)
        Me.rbStandBasedGrids.TabIndex = 18
        Me.rbStandBasedGrids.Text = "Create Stand Based Grids"
        Me.rbStandBasedGrids.UseVisualStyleBackColor = True
        '
        'tbPctStandInGrid
        '
        Me.tbPctStandInGrid.Location = New System.Drawing.Point(454, 131)
        Me.tbPctStandInGrid.Name = "tbPctStandInGrid"
        Me.tbPctStandInGrid.Size = New System.Drawing.Size(42, 20)
        Me.tbPctStandInGrid.TabIndex = 19
        Me.tbPctStandInGrid.Text = "60"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(500, 134)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(44, 13)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Percent"
        '
        'cbRxFeas
        '
        Me.cbRxFeas.AutoSize = True
        Me.cbRxFeas.Checked = True
        Me.cbRxFeas.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbRxFeas.Location = New System.Drawing.Point(12, 203)
        Me.cbRxFeas.Name = "cbRxFeas"
        Me.cbRxFeas.Size = New System.Drawing.Size(148, 17)
        Me.cbRxFeas.TabIndex = 21
        Me.cbRxFeas.Text = "Consider Rx for Feasibilitiy"
        Me.cbRxFeas.UseVisualStyleBackColor = True
        '
        'tbNPer
        '
        Me.tbNPer.Location = New System.Drawing.Point(137, 223)
        Me.tbNPer.Name = "tbNPer"
        Me.tbNPer.Size = New System.Drawing.Size(72, 20)
        Me.tbNPer.TabIndex = 23
        Me.tbNPer.Text = "30"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(9, 226)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(99, 13)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "Periods to Evaluate"
        '
        'GridForTrimmer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(609, 283)
        Me.Controls.Add(Me.tbNPer)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.cbRxFeas)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.tbPctStandInGrid)
        Me.Controls.Add(Me.rbStandBasedGrids)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cbCircleGrid)
        Me.Controls.Add(Me.rbMajorityStandInGrid)
        Me.Controls.Add(Me.rbPartStandInGrid)
        Me.Controls.Add(Me.rbFullStandInGrid)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.tbMinFeasGrid)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tbGridSize)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tbPctGridOverlap)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tbHexAcres)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbHexFile)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "GridForTrimmer"
        Me.Text = "GridForTrimmer"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbHexFile As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents tbHexAcres As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbPctGridOverlap As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbGridSize As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbMinFeasGrid As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents rbFullStandInGrid As System.Windows.Forms.RadioButton
    Friend WithEvents rbPartStandInGrid As System.Windows.Forms.RadioButton
    Friend WithEvents rbMajorityStandInGrid As System.Windows.Forms.RadioButton
    Friend WithEvents cbCircleGrid As System.Windows.Forms.CheckBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents rbStandBasedGrids As System.Windows.Forms.RadioButton
    Friend WithEvents tbPctStandInGrid As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cbRxFeas As System.Windows.Forms.CheckBox
    Friend WithEvents tbNPer As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
End Class
