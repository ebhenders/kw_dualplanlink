Imports System.Threading
Public Class clTrimmer_wGrid

    'Trimmer wGrid tries to implement the concept of trimming solutions (or adding them) based on the spatial
    ' value of a grid of stands analyzed together

    'The trimmer will read price, AA, and Rx information either from an input file, or from a Dual-plan
    'processed solution (with adjusted prices)
    'The trimmer will then determine the values (including an infinite series beyond the planning horizon),
    ' assume optimistic spatial alignment, and choose suite of best spatial solutions PLUS the best
    ' non-spatial solutions to send on to DPForm OR DPSpace

    'outputs will include the per-poly prescriptions, associated non-spatial NPV, and Space, Buffer, and Edge
    ' series pointer indices for each Rx. These are all inputs for DPSpace.

#Region "Declarations"
    Dim TThread() As Threading.Thread
    Dim TThread2 As Threading.Thread
    Dim objMyLock1 As New Object
    Dim objMyLock2 As New Object
    Dim objMyLock3 As New Object
    Dim NRunningThreads As Integer
    Dim sRunningThreads As Integer

    Dim DualPlanFileList As String 'aa and rx index file
    Dim CurrentDualPlanPricesFile As String 'current dual plan input file; mining the shadow price info.
    Dim ISpaceDefFile As String 'file with interior space and quality buffer definitions
    Dim IZonesFile As String
    Dim GlobalTrim As Integer

    'Stand-level prescriptions available.
    'Dim StandRx() As RxInfo 'by stand

    'Hexagon/stand information
    'Dim HexPolys() As PolyInfo

    'Dim NStand As Integer
    Dim StandIDByPolyID() As Integer 'displays a poly's stand ID...indexed by its polyID
    Dim PolyIndByPolyID() As Integer 'put in polyID and output hex ID for polygon array
    Dim StandMaxPatch(,,) As Single  'maximum patch size a stand can be a part of - by stand,spacetype, period (eventually should be by space type as well)
    Dim StandMaxID() As Long 'when reassigning stand IDs to the hex, keep track of the largest reassigned ID associated with any stand


    'for economic prescription trimming
    Dim CurrRxValue As Single 'value of the current prescription being evaluated
    Dim MaxRxValue As Single 'the maximum value of the suite of prescriptions being evaluated
    'Dim KeepRx As Integer '0/1 whether to keep or not
    Dim RxSpatialValue As Integer '0/1 whether or not the Rx can even have spatial value from neighboring stands

    'for trimming based on spatial information
    Dim DPSpaceStandID_LUT() As Double 'stores the stand id sent to DPSpace
    Dim NSpatialPolys As Integer = 0 'number of polygons with spatial prescriptions that have made it through the trimmer
    Dim AASpacePotential(,,) As Byte 'by AA, SpaceType, Tic
    Dim KeepZoneSpace(,) As Byte 'whether to keep the zone for printing or not - dimensioned by the number of SpaceTypes and IZones
    Dim SpatialSeriesNFirstTic() As Integer 'counts the number of 1st appearances of KW in the spatial series
    Dim SpatialSeriesFirstTic(,) As Integer 'the tic of first appearance of Kw

    'for tracking prescriptions
    Dim OrderedPres(,) As Integer 'by stand, prescription - ordered by value largest to smallest
    Dim RxIn(,) As Byte 'by stand, prescription

    Dim SpatialAlignValue(,) As Single 'Double 'by stand, prescription

    Dim GridRxHighestRank(,) As Integer 'highest grid rank that the Rx is associated with. 1 is highest
    Dim StandRxRank(,) As Integer 'rank of the Rx by value in the stand's list of Rxs
    Dim GridRxHighestVal(,) As Single 'per acre value of grid associated with the Rx - looks for the one closest to the highest identified for the grid
    Dim DefaultHighestGridVal As Single = 999999


#End Region

    Public Sub RunGridTrimmer(ByVal ksubfor As Integer, ByVal infile As String, ByVal kiter As Integer)
        'This trimmer looks at a predetermined spatial extent (grid) of stands and evaulates the spatial prescription
        '  for the whole area. The spatial values of the whole "grid" are then ranked, and the highest X number
        '  of prescriptions are chosen for each stand in that grid.

        Dim trimiter As Integer = 0
        Dim ktrialset As Integer
        Dim kbreakset As Integer
        Dim GridRxIn(,) As Byte
        Dim BestSpatRx() As Long 'DUMMY chosen Rx if it's a kw rx, otherwise the one chosen that best aligns with fellow IZone stands

        Dim TrimReset As Integer = 0
        Dim MxPresPerPoly As Integer = 999999

        ReDim BestSpatRx(AA_NPolys) 'dummy
        ReDim RxIn(AA_NPolys, MxStandRx)
        ReDim GridRxIn(AA_NPolys, MxStandRx), GridRxHighestRank(AA_NPolys, MxStandRx), GridRxHighestVal(AA_NPolys, MxStandRx), StandRxRank(AA_NPolys, MxStandRx)
        ReDim NPValMxSpat(AA_NPolys, MxStandRx)
        ReDim MaxNSRx(AA_NPolys)

        If kiter = 1 Then LoadGridInfo(infile)

        ' get the economic info. for all Dual Plan prescriptions (similar to the SCHEDULE routine, just don't pick yet!)
        DualPlanRxEcon(RxIn, MaxNSRx)

        'figure the MxPresPerPoly
        ktrialset = DetermineKTrialSet(0)
        kbreakset = TrialBreakSet(ktrialset)
        For jbreak As Integer = 1 To TrialBreakSetNBreaks(kbreakset)
            If TrialNSched(kbreakset, jbreak) < MxPresPerPoly And TrialNSched(kbreakset, jbreak) > 0 Then MxPresPerPoly = TrialNSched(kbreakset, jbreak)
            If TrialNUnsched(kbreakset, jbreak) < MxPresPerPoly And TrialNUnsched(kbreakset, jbreak) > 0 Then MxPresPerPoly = TrialNUnsched(kbreakset, jbreak)
        Next

        'back and forth between TrimEcon and SpatialDominance
        GlobalTrim = 1
        Do Until GlobalTrim = 0
            trimiter = trimiter + 1
            If trimiter = 1 Then 'go through each trim routine at least once
                GlobalTrim = 0
                TrimEcon(RxIn, NPValMxSpat, TrimTolerance, FootprintPct)
                If GlobalTrim > 0 Then TrimSpatialDom(RxIn)
            Else
                'if your last trimspatialdom trimmed anything, re-evaluate with TrimEcon
                If GlobalTrim > 0 Then
                    GlobalTrim = 0
                    TrimEcon(RxIn, NPValMxSpat, TrimTolerance, FootprintPct)
                End If

                'if your last TrimEcon trimmed anything, re-check spatial dominance
                If GlobalTrim > 0 Then
                    GlobalTrim = 0
                    TrimSpatialDom(RxIn)
                End If

            End If
        Loop


        For jaa As Integer = 1 To AA_NPolys
            For jrx As Integer = 1 To AA_NPres(jaa)
                GridRxHighestRank(jaa, jrx) = AA_NPres(jaa) + 1 'default
                GridRxHighestVal(jaa, jrx) = DefaultHighestGridVal 'default
            Next
        Next
        'if you are trimming based on grids to a desired # of prescriptions per poly then build the GridRxIn array 
        If MxPresPerPoly > 0 Then
            GridRxBuilderDouble1OptSubRegenTic(GridRxIn)
            'GridRxBuilderDouble1OptParentOnly(GridRxIn)
            'GridRxBuilderSingle1OptParentOnly(GridRxIn)
        Else 'otherwise, RxIn is the default - you are not trimming based on grids
            For jaa As Integer = 1 To AA_NPolys
                For jrx As Integer = 1 To AA_NPres(jaa)
                    GridRxIn(jaa, jrx) = RxIn(jaa, jrx)
                Next
            Next
        End If

        'finally, make sure that you include the best non-spatial
        For jaa As Integer = 1 To AA_NPolys
            GridRxIn(jaa, MaxNSRx(jaa)) = 1 'best non-spatial
        Next

        WriteDPSpaceTrimInfo(ksubfor, GridRxIn, True)
        WriteTrimmedIZoneAndHexInfo(ksubfor)


    End Sub

    Private Sub GridRxBuilderDouble1OptSubRegenTic(ByRef GridRxIn(,) As Byte)
        'looks at grid-wide financial information for each prescription timing choice in each grid and populates
        ' arrays of the value of those presecriptions as well as the associated stand Rx number associated with 
        ' the value. Also, populates a parent RxIn array with the top X number of prescription values for the grid

        Dim ValCom As Double
        Dim RxVal() As Double
        Dim NSpatSeries As Integer
        Dim jgrid As Integer
        Dim krx As Integer
        Dim MaxFirstTic As Integer = MxTic 'default
        Dim TrimStartTime As Double
        Dim TrimStopTime As Double




        TrimStartTime = Microsoft.VisualBasic.Timer

        'mine the spatial series information for data about first tic
        NSpatSeries = SpatialSeries.Length - 1
        ReDim SpatialSeriesNFirstTic(NSpatSeries), SpatialSeriesFirstTic(NSpatSeries, MaxFirstTic)
        For jspat As Integer = 1 To NSpatSeries
            'catch time 1
            If SpatialSeries(jspat).Flows(1) = 1 Then
                SpatialSeriesNFirstTic(jspat) = SpatialSeriesNFirstTic(jspat) + 1
                SpatialSeriesFirstTic(jspat, SpatialSeriesNFirstTic(jspat)) = 1
            End If
            For jtic As Integer = 2 To MxTic ' NTic
                If SpatialSeries(jspat).Flows(jtic) = 1 And SpatialSeries(jspat).Flows(jtic - 1) = 0 Then 'have a first flow tic
                    SpatialSeriesNFirstTic(jspat) = SpatialSeriesNFirstTic(jspat) + 1
                    SpatialSeriesFirstTic(jspat, SpatialSeriesNFirstTic(jspat)) = jtic
                End If
            Next
        Next
        'ReDim GridStartTime(NGrids), GridStopTime(NGrids), Grid1Opts(NGrids)

        'HERE IS WHERE THE MULTITHREADING MIGHT OCCUR
        NRunningThreads = NGrids
        sRunningThreads = 0
        jgrid = 0
        ReDim TThread(NGrids)
        Do Until jgrid >= NGrids
            If sRunningThreads < 6 Then
                SyncLock objMyLock3
                    sRunningThreads = sRunningThreads + 1
                End SyncLock
                jgrid = jgrid + 1
                'start a new thread
                TThread(jgrid) = New Thread(AddressOf Thread1Opt1)
                'NRunningThreads = NRunningThreads + 1
                TThread(jgrid).Start(jgrid)
                TThread(jgrid) = Nothing
            Else
                System.Threading.Thread.Sleep(1000)
            End If
            'Thread1Opt1(jgrid)
            'TThread(jgrid).Join()
        Loop
        Do While NRunningThreads > 0
            System.Threading.Thread.Sleep(1000)
        Loop
        'TThread2 = New Thread(AddressOf wait)
        'TThread2.Join()
        'TThread2.Start()

        'TThread = Nothing

        'for each prescription of each stand, evaluate whether to keep its prescriptions or not based on its best values in the grid
        For jaa As Integer = 1 To AA_NPolys
            ReDim RxVal(AA_NPres(jaa))
            RxVal(0) = DefaultHighestGridVal
            For jrx As Integer = 1 To AA_NPres(jaa)
                krx = AA_PresArray(jaa, jrx)
                RxVal(jrx) = GridRxHighestVal(jaa, jrx)
            Next
            'find the value of the nth value in the list
            Array.Sort(RxVal)
            ValCom = RxVal(AA_NPres(jaa)) 'default
            If MxPresPerPoly - 1 <= AA_NPres(jaa) Then
                ValCom = RxVal(MxPresPerPoly - 1)
            End If

            For jrx As Integer = 1 To AA_NPres(jaa)
                'for debug, find the rank of this Rx in the context of the stand
                For jjrx As Integer = 0 To AA_NPres(jaa)
                    If GridRxHighestVal(jaa, jrx) = RxVal(jjrx) Then
                        StandRxRank(jaa, jrx) = jjrx + 1 'rxval is 0-based, with the highest rank occupying spot 0
                        If RxVal(jjrx) = DefaultHighestGridVal Then
                            StandRxRank(jaa, jrx) = AA_NPres(jaa)
                        End If

                        Exit For 'found the match
                    End If
                Next
                If GridRxHighestVal(jaa, jrx) <= DollarPerAcreTol And GridRxHighestVal(jaa, jrx) <= ValCom Then
                    GridRxIn(jaa, jrx) = 1
                End If
            Next
        Next

        'debug stuff
        TrimStopTime = Microsoft.VisualBasic.Timer
        'SPATIAL INDEX BY RX DATA FILE
        Dim f86 As Integer
        f86 = FreeFile()
        FileOpen(f86, FnTrimmeroutBaseA & "_Iter" & IterationNumber & "_GridDebug.txt", OpenMode.Output)
        PrintLine(f86, "TotalTime")
        PrintLine(f86, Math.Round((TrimStopTime - TrimStartTime) / 60, 2))

        FileClose(f86)

    End Sub
    'Private Sub GridRxBuilderDouble1OptParentOnly(ByRef GridRxIn(,) As Byte)
    '    'looks at grid-wide financial information for each prescription timing choice in each grid and populates
    '    ' arrays of the value of those presecriptions as well as the associated stand Rx number associated with 
    '    ' the value. Also, populates a parent RxIn array with the top X number of prescription values for the grid

    '    Dim BestNSGridVal As Double 'best non-spatial grid value
    '    Dim RxChosen() As Integer 'the prescription Number chosen for each stand in the grid
    '    Dim MasterRxChosen() As Integer
    '    Dim NSRxChosen() As Integer
    '    Dim BestSpatialGridVal() As Double 'best spatial grid value per period
    '    Dim SpatialGridValTic As Double ' spatial grid value for the tic - evaluated over several tries within the same tic
    '    Dim BestSpatialGridValTic As Double
    '    Dim OrderedArray() As Double 'used to order the values and choose best X Rx
    '    Dim ValCom As Double
    '    Dim OrdRank As Integer
    '    Dim krank As Integer
    '    Dim ValueCompare As Double
    '    Dim ValueComparePerAcre As Double
    '    Dim StandRxPerGridVal(,) As Integer 'the stand's prescription associated with the BestSpatialGridVal
    '    'Dim GridRxIn(,) As Integer
    '    Dim NSpatSeries As Integer
    '    Dim NRxChosen As Integer
    '    Dim NLocked As Integer
    '    Dim NSubLocked As Integer

    '    Dim GridAcres As Single
    '    'Dim DollarPerAcreTol As Single = 10

    '    Dim MaxFirstTic As Integer = MxTic 'default
    '    Dim maxtic As Integer
    '    Dim kaa As Integer
    '    Dim krx As Integer
    '    Dim irx As Integer
    '    Dim kind As Integer 'spatial index
    '    Dim kspat As Integer 'spatial type
    '    Dim kstart As Integer
    '    Dim kregen As Integer
    '    Dim tregen As Integer
    '    Dim subtic As Integer
    '    Dim stic As Integer
    '    Dim StandTicAreaVal() As Double
    '    Dim FullTicAreaVal() As Double
    '    Dim Nrx As Integer
    '    Dim StandsInGrid() As Integer
    '    Dim bRxHit As Boolean
    '    Dim bUsingSubTic As Boolean
    '    Dim LockedRx() As Byte
    '    Dim SubLockedRx() As Byte
    '    Dim jticProposedRx() As Integer
    '    Dim StandInGrid() As Byte
    '    Dim bCalcSpace As Boolean

    '    Dim ParentTimingChoice() As Integer 'indicates whether the stand in question has a parent timing choice
    '    Dim NParentRegen As Integer
    '    Dim NSubRegen() As Integer
    '    Dim ParentRegenTic() As Integer
    '    Dim SubRegenTic(,) As Integer
    '    Dim ParentStandRx(,) As Long 'by jstand,maxparentregentics
    '    Dim SubStandRx(,,) As Long 'by jstand,subperiod, subperiodregen

    '    '########## this is artificially constrained to 5 to speed up the process!
    '    Dim MaxRegenTics As Integer = 5

    '    Dim bUnqRegen As Boolean
    '    Dim MatchInd As Integer

    '    'debug stuff
    '    'Dim GridStartTime() As Double
    '    'Dim GridStopTime() As Double
    '    'Dim Grid1Opts() As Integer
    '    Dim TrimStartTime As Double
    '    Dim TrimStopTime As Double

    '    TrimStartTime = Microsoft.VisualBasic.Timer

    '    'mine the spatial series information for data about first tic
    '    NSpatSeries = SpatialSeries.Length - 1
    '    ReDim SpatialSeriesNFirstTic(NSpatSeries), SpatialSeriesFirstTic(NSpatSeries, MaxFirstTic)
    '    For jspat As Integer = 1 To NSpatSeries
    '        'catch time 1
    '        If SpatialSeries(jspat).Flows(1) = 1 Then
    '            SpatialSeriesNFirstTic(jspat) = SpatialSeriesNFirstTic(jspat) + 1
    '            SpatialSeriesFirstTic(jspat, SpatialSeriesNFirstTic(jspat)) = 1
    '        End If
    '        For jtic As Integer = 2 To MxTic ' NTic
    '            If SpatialSeries(jspat).Flows(jtic) = 1 And SpatialSeries(jspat).Flows(jtic - 1) = 0 Then 'have a first flow tic
    '                SpatialSeriesNFirstTic(jspat) = SpatialSeriesNFirstTic(jspat) + 1
    '                SpatialSeriesFirstTic(jspat, SpatialSeriesNFirstTic(jspat)) = jtic
    '            End If
    '        Next
    '    Next
    '    ' ReDim GridStartTime(NGrids), GridStopTime(NGrids), Grid1Opts(NGrids)

    '    For jgrid As Integer = 1 To NGrids
    '        'GridStartTime(jgrid) = Microsoft.VisualBasic.Timer
    '        'If jgrid = 309 Then Stop
    '        ReDim BestSpatialGridVal(NTic), StandRxPerGridVal(NTic, NStandsInGrid(jgrid)), OrderedArray(NTic)
    '        BestNSGridVal = 0 'reset
    '        'use this unused index to store the best NS for comparison purposes
    '        BestSpatialGridVal(0) = -999999999 'BestNSGridVal 'not using this index, so make sure it's last!

    '        'figure the best non-spatial value of the polys in this grid. Should just be adding up the best non-spatial values...
    '        ReDim StandsInGrid(NStandsInGrid(jgrid)), NSRxChosen(NStandsInGrid(jgrid)), StandInGrid(AA_NPolys)
    '        bCalcSpace = False
    '        GridAcres = 0
    '        For jstand As Integer = 1 To NStandsInGrid(jgrid)
    '            kaa = GridStands(jgrid, jstand)
    '            GridAcres = GridAcres + AA_area(kaa)
    '            StandsInGrid(jstand) = kaa
    '            StandInGrid(kaa) = 1
    '            irx = MaxNSRx(kaa) 'not the base prescription index, but rather the prescription index of the total Rx for this stand...
    '            BestNSGridVal = BestNSGridVal + DualPlanAAPoly_NPVSelf(kaa, irx) * AA_area(kaa)
    '            krx = AA_PresArray(kaa, irx)
    '            kind = AApres_ISpaceInd(krx)
    '            kspat = AApres_SpaceType(krx)
    '            If SpatialSeries(kind).NNonZero > 0 Then
    '                bCalcSpace = True
    '            End If
    '            NSRxChosen(jstand) = krx 'default- prescription index; not stand-based prescription index
    '        Next
    '        If bCalcSpace Then
    '            For jstand As Integer = 1 To NStandsInGrid(jgrid)
    '                kaa = GridStands(jgrid, jstand)
    '                irx = MaxNSRx(kaa) 'not the base prescription index, but rather the prescription index of the total Rx for this stand...
    '                krx = AA_PresArray(kaa, irx)
    '                kind = AApres_ISpaceInd(krx)
    '                kspat = AApres_SpaceType(krx)
    '                If SpatialSeries(kind).NNonZero > 0 Then
    '                    'have to look for spatial interactions of the IZones
    '                    ReDim FullTicAreaVal(MxTic), StandTicAreaVal(MxTic)
    '                    CalcTicAreaVal(FullTicAreaVal, StandTicAreaVal, StandInGrid, StandsInGrid, NSRxChosen, kaa)
    '                    For jtic As Integer = SpatialSeries(kind).First1Tic To SpatialSeries(kind).Last1tic
    '                        BestNSGridVal = BestNSGridVal + SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * (StandTicAreaVal(jtic)) 'want it on the total - not a per-acre basis
    '                    Next jtic
    '                End If
    '                NSRxChosen(jstand) = krx 'default- prescription index; not stand-based prescription index
    '            Next
    '        End If

    '        'for each tic, figure the max financial value, including a 1-opt search, which rectifies multiple regen timing choices as well as catches stands with first KW corresponding to the regen KW of most of the patch. 
    '        '   Also, populate array of prescriptions chosen for the stand associated with the value
    '        '   This is the meat and potatoes of this whole procesing routine!
    '        BestSpatialGridVal(1) = -99999999
    '        For jtic As Integer = 1 To NTic
    '            'default is the best non-spatial solution
    '            '#####################
    '            BestSpatialGridVal(jtic) = -99999999 'BestNSGridVal 'default
    '            For jstand As Integer = 1 To NStandsInGrid(jgrid)
    '                StandRxPerGridVal(jtic, jstand) = NSRxChosen(jstand)
    '            Next

    '            'It is here that you want to construct the parameters for evaluating jjtic metrics
    '            ' So...if you are on Tic 12, and you want to evaluate those stands without a 12 Rx, what are they?
    '            ReDim ParentTimingChoice(NStandsInGrid(jgrid))
    '            maxtic = 4
    '            If jtic - 4 <= 0 Then maxtic = jtic - 1
    '            NParentRegen = 0
    '            ReDim ParentRegenTic(MaxRegenTics), ParentStandRx(NStandsInGrid(jgrid), MaxRegenTics), SubStandRx(NStandsInGrid(jgrid), maxtic, MaxRegenTics)
    '            For jstand As Integer = 1 To NStandsInGrid(jgrid)
    '                kaa = GridStands(jgrid, jstand)
    '                Nrx = AA_NPres(kaa)
    '                For jrx As Integer = 1 To Nrx
    '                    If RxIn(kaa, jrx) = 1 Then
    '                        krx = AA_PresArray(kaa, jrx) 'krx used to access line in prescription database
    '                        kind = AApres_ISpaceInd(krx)
    '                        For jstart As Integer = 1 To SpatialSeriesNFirstTic(kind)
    '                            If SpatialSeriesFirstTic(kind, jstart) = jtic And NParentRegen < MaxRegenTics Then
    '                                ParentTimingChoice(jstand) = 1
    '                                kregen = 1 'default
    '                                'look for regen tics
    '                                kstart = jstart + 1
    '                                If kstart <= SpatialSeriesNFirstTic(kind) Then
    '                                    'check for unique regen tic
    '                                    tregen = SpatialSeriesFirstTic(kind, kstart)
    '                                    bUnqRegen = True
    '                                    For jregen As Integer = 1 To NParentRegen
    '                                        If ParentRegenTic(jregen) = tregen Then
    '                                            kregen = jregen
    '                                            bUnqRegen = False
    '                                            Exit For
    '                                        End If
    '                                    Next
    '                                    If bUnqRegen Then
    '                                        NParentRegen = NParentRegen + 1
    '                                        kregen = NParentRegen
    '                                        ParentRegenTic(NParentRegen) = tregen
    '                                    End If
    '                                End If
    '                                ParentStandRx(jstand, kregen) = krx 'is this the right spot????
    '                            End If 'on same parent tic or a qualifying 
    '                        Next
    '                    End If
    '                Next
    '            Next jstand

    '            'now go ahead and seed the grid's stands with the different prescription combinations
    '            For jparentregentic As Integer = 1 To NParentRegen
    '                NRxChosen = 0
    '                '#####################
    '                SpatialGridValTic = -99999999 'BestNSGridVal
    '                ReDim MasterRxChosen(NStandsInGrid(jgrid)), RxChosen(NStandsInGrid(jgrid)), LockedRx(NStandsInGrid(jgrid)), SubLockedRx(NStandsInGrid(jgrid)), jticProposedRx(NStandsInGrid(jgrid))
    '                'add defaults for this parentregentic
    '                For jstand As Integer = 1 To NStandsInGrid(jgrid)
    '                    MasterRxChosen(jstand) = NSRxChosen(jstand) 'default
    '                    If ParentStandRx(jstand, jparentregentic) > 0 Then
    '                        MasterRxChosen(jstand) = ParentStandRx(jstand, jparentregentic)
    '                        LockedRx(jstand) = 1
    '                        NRxChosen = NRxChosen + 1
    '                    Else
    '                        For jjparentregentic As Integer = 1 To NParentRegen
    '                            If ParentStandRx(jstand, jjparentregentic) > 0 Then
    '                                MasterRxChosen(jstand) = ParentStandRx(jstand, jjparentregentic)
    '                                LockedRx(jstand) = 1
    '                                NRxChosen = NRxChosen + 1
    '                            End If
    '                        Next
    '                    End If
    '                    RxChosen(jstand) = MasterRxChosen(jstand)
    '                Next



    '                'Then try a double 1-opt with just the stands in the current jtic solution
    '                If NRxChosen > 0 Then
    '                    'Grid1Opts(jgrid) = Grid1Opts(jgrid) + 1
    '                    NLocked = 0
    '                    NSubLocked = 0
    '                    'first try to add
    '                    GridTrimOneOpt(StandsInGrid, StandInGrid, MasterRxChosen, LockedRx, SubLockedRx, SpatialGridValTic, True, True, NLocked, NSubLocked, jtic, 0)
    '                    'then try to pare down
    '                    GridTrimOneOpt(StandsInGrid, StandInGrid, MasterRxChosen, LockedRx, SubLockedRx, SpatialGridValTic, True, False, NLocked, NSubLocked, jtic, 0)
    '                End If

    '                If SpatialGridValTic > BestSpatialGridVal(jtic) And NLocked > 0 Then
    '                    BestSpatialGridVal(jtic) = SpatialGridValTic
    '                    For jstand As Integer = 1 To NStandsInGrid(jgrid)
    '                        StandRxPerGridVal(jtic, jstand) = MasterRxChosen(jstand)
    '                    Next
    '                End If

    '            Next jparentregentic
    '        Next jtic


    '        'Then, choose the highest X number of prescriptions for the grid - make sure these values clear the 
    '        '  best non-spatial grid value, though!
    '        For jtic As Integer = 0 To NTic
    '            OrderedArray(jtic) = BestSpatialGridVal(jtic)
    '            '##########################
    '            'If OrderedArray(jtic) = BestNSGridVal Then OrderedArray(jtic) = -999999999 'don't count this
    '        Next
    '        Array.Sort(OrderedArray)
    '        'If MaxNumPres > OrderedArray.Length Then
    '        '    ValueCompare = OrderedArray(1)
    '        'Else
    '        '    ValueCompare = OrderedArray(OrderedArray.Length - MaxNumPres) 'largest values are at the end - find the lowest value to keep
    '        'End If
    '        ValueComparePerAcre = Math.Max(OrderedArray(OrderedArray.Length - 1) / GridAcres, BestNSGridVal / GridAcres)
    '        For jtic As Integer = 1 To NTic
    '            'find the ordinal rank of the prescription
    '            For jrank As Integer = 1 To OrderedArray.Length - 1
    '                'krank = OrderedArray.Length - jrank
    '                If BestSpatialGridVal(jtic) = OrderedArray(OrderedArray.Length - jrank) Then
    '                    krank = jrank
    '                    Exit For
    '                End If
    '            Next
    '            ValCom = BestSpatialGridVal(jtic) / GridAcres
    '            If BestSpatialGridVal(jtic) = -99999999 Then ValCom = BestNSGridVal / GridAcres
    '            '########################
    '            If ValCom + DollarPerAcreTol > ValueComparePerAcre Then 'ValueCompare Then 'And BestSpatialGridVal(jtic) >= BestNSGridVal Then
    '                'keep the prescriptions that created this great value!
    '                For jstand As Integer = 1 To NStandsInGrid(jgrid)
    '                    kaa = GridStands(jgrid, jstand)

    '                    For jrx As Integer = 1 To AA_NPres(kaa)
    '                        krx = AA_PresArray(kaa, jrx)
    '                        'If kaa = 376 And jrx = 92 Then Stop
    '                        'If kaa = 376 And jrx = 221 Then Stop
    '                        If AA_PresArray(kaa, jrx) = StandRxPerGridVal(jtic, jstand) Then
    '                            'If krx = 47869 Then Stop
    '                            GridRxIn(kaa, jrx) = 1
    '                            Exit For 'quit searching for appropriate prescription to find - you got it! Move to next stand
    '                        End If
    '                    Next
    '                Next
    '            End If
    '            'debug - find the best ordinal and $/acre rank of each prescription
    '            For jstand As Integer = 1 To NStandsInGrid(jgrid)
    '                kaa = GridStands(jgrid, jstand)
    '                'If kaa = 376 Then Stop
    '                For jrx As Integer = 1 To AA_NPres(kaa)
    '                    krx = AA_PresArray(kaa, jrx)
    '                    'If kaa = 376 And jrx = 92 Then Stop
    '                    'If kaa = 376 And jrx = 221 Then Stop
    '                    If AA_PresArray(kaa, jrx) = StandRxPerGridVal(jtic, jstand) Then
    '                        If Math.Abs(ValueComparePerAcre - ValCom) < GridRxHighestVal(kaa, jrx) Then GridRxHighestVal(kaa, jrx) = Math.Abs(ValueComparePerAcre - ValCom)
    '                        If krank < GridRxHighestRank(kaa, jrx) Then GridRxHighestRank(kaa, jrx) = krank
    '                        'search for rank in this grid
    '                        Exit For 'quit searching for appropriate prescription to find - you got it! Move to next stand
    '                    End If
    '                Next
    '            Next
    '        Next
    '        'GridStopTime(jgrid) = Microsoft.VisualBasic.Timer
    '    Next jgrid

    '    'debug stuff
    '    TrimStopTime = Microsoft.VisualBasic.Timer
    '    'SPATIAL INDEX BY RX DATA FILE
    '    Dim f86 As Integer
    '    f86 = FreeFile()
    '    FileOpen(f86, FnTrimmeroutBaseA & "_Iter" & IterationNumber & "_GridDebug.txt", OpenMode.Output)
    '    PrintLine(f86, "TotalTime")
    '    PrintLine(f86, Math.Round(TrimStopTime - TrimStartTime, 2))
    '    'PrintLine(f86, "GridNum, Time, N1Opts")
    '    'For jgrid As Integer = 1 To NGrids
    '    '    PrintLine(f86, jgrid & "," & Math.Round(((GridStopTime(jgrid) - GridStartTime(jgrid))), 2) & "," & Grid1Opts(jgrid))
    '    'Next
    '    FileClose(f86)

    'End Sub

    Sub Thread1Opt1(ByVal obj As Object)
        Dim jgrid As Integer = CInt(obj)
        Dim BestNSGridVal As Double 'best non-spatial grid value
        Dim RxChosen() As Integer 'the prescription Number chosen for each stand in the grid
        Dim MasterRxChosen() As Integer
        Dim NSRxChosen() As Integer
        Dim BestSpatialGridVal() As Double 'best spatial grid value per period
        Dim SpatialGridValTic As Double ' spatial grid value for the tic - evaluated over several tries within the same tic
        Dim OrderedArray() As Double 'used to order the values and choose best X Rx
        Dim ValCom As Double
        Dim krank As Integer
        Dim StandRxPerGridVal(,) As Integer 'the stand's prescription associated with the BestSpatialGridVal
        Dim StandsInGrid() As Integer
        Dim StandInGrid() As Byte
        Dim StandTicAreaVal() As Double
        Dim FullTicAreaVal() As Double
        Dim GridAcres As Single

        Dim bCalcSpace As Boolean
        Dim Nrx As Integer

        Dim bUsingSubTic As Boolean
        Dim LockedRx() As Byte
        Dim SubLockedRx() As Byte
        Dim jticProposedRx() As Integer
        Dim ValueComparePerAcre As Double

        Dim ParentTimingChoice() As Integer 'indicates whether the stand in question has a parent timing choice
        Dim NParentRegen As Integer
        Dim NSubRegen() As Integer
        Dim ParentRegenTic() As Integer
        Dim SubRegenTic(,) As Integer
        Dim ParentStandRx(,) As Long 'by jstand,maxparentregentics
        Dim SubStandRx(,,) As Long 'by jstand,subperiod, subperiodregen


        Dim bUnqRegen As Boolean
        Dim OrderedRxVal() As Double
        Dim NRxChosen As Integer
        Dim NLocked As Integer
        Dim NSubLocked As Integer


        Dim maxtic As Integer
        Dim kaa As Integer
        Dim krx As Integer
        Dim irx As Integer
        Dim kind As Integer 'spatial index
        Dim kspat As Integer 'spatial type
        Dim kstart As Integer
        Dim kregen As Integer
        'Dim jgrid As Integer
        Dim tregen As Integer
        Dim subtic As Integer
        Dim stic As Integer
        '########## this is artificially constrained to 5 to speed up the process!
        Dim MaxRegenTics As Integer = 5



        'For jgrid As Integer = 1 To NGrids
        'GridStartTime(jgrid) = Microsoft.VisualBasic.Timer
        'If jgrid = 309 Then Stop
        ReDim BestSpatialGridVal(NTic), StandRxPerGridVal(NTic, NStandsInGrid(jgrid)), OrderedArray(NTic)
        BestNSGridVal = 0 'reset
        'use this unused index to store the best NS for comparison purposes
        BestSpatialGridVal(0) = -999999999 'BestNSGridVal 'not using this index, so make sure it's last!

        'figure the best non-spatial value of the polys in this grid. Should just be adding up the best non-spatial values...
        ReDim StandsInGrid(NStandsInGrid(jgrid)), NSRxChosen(NStandsInGrid(jgrid)), StandInGrid(AA_NPolys)
        bCalcSpace = False
        GridAcres = 0
        For jstand As Integer = 1 To NStandsInGrid(jgrid)
            kaa = GridStands(jgrid, jstand)
            GridAcres = GridAcres + AA_area(kaa)
            StandsInGrid(jstand) = kaa
            StandInGrid(kaa) = 1
            irx = MaxNSRx(kaa) 'not the base prescription index, but rather the prescription index of the total Rx for this stand...
            BestNSGridVal = BestNSGridVal + DualPlanAAPoly_NPVSelf(kaa, irx) * AA_area(kaa)
            krx = AA_PresArray(kaa, irx)
            kind = AApres_ISpaceInd(krx)
            kspat = AApres_SpaceType(krx)
            If SpatialSeries(kind).NNonZero > 0 Then
                bCalcSpace = True
            End If
            NSRxChosen(jstand) = krx 'default- prescription index; not stand-based prescription index
        Next
        If bCalcSpace Then
            For jstand As Integer = 1 To NStandsInGrid(jgrid)
                kaa = GridStands(jgrid, jstand)
                irx = MaxNSRx(kaa) 'not the base prescription index, but rather the prescription index of the total Rx for this stand...
                krx = AA_PresArray(kaa, irx)
                kind = AApres_ISpaceInd(krx)
                kspat = AApres_SpaceType(krx)
                If SpatialSeries(kind).NNonZero > 0 Then
                    'have to look for spatial interactions of the IZones
                    ReDim FullTicAreaVal(MxTic), StandTicAreaVal(MxTic)
                    CalcTicAreaVal(FullTicAreaVal, StandTicAreaVal, StandInGrid, StandsInGrid, NSRxChosen, kaa)
                    For jtic As Integer = SpatialSeries(kind).First1Tic To SpatialSeries(kind).Last1tic
                        BestNSGridVal = BestNSGridVal + SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * (StandTicAreaVal(jtic)) 'want it on the total - not a per-acre basis
                    Next jtic
                End If
                NSRxChosen(jstand) = krx 'default- prescription index; not stand-based prescription index
            Next
        End If

        'for each tic, figure the max financial value, including a 1-opt search, which rectifies multiple regen timing choices as well as catches stands with first KW corresponding to the regen KW of most of the patch. 
        '   Also, populate array of prescriptions chosen for the stand associated with the value
        '   This is the meat and potatoes of this whole procesing routine!
        BestSpatialGridVal(1) = -99999999
        For jtic As Integer = 2 To NTic
            'default is the best non-spatial solution
            '#####################
            BestSpatialGridVal(jtic) = -99999999 'BestNSGridVal 'default
            For jstand As Integer = 1 To NStandsInGrid(jgrid)
                StandRxPerGridVal(jtic, jstand) = NSRxChosen(jstand)
            Next

            'It is here that you want to construct the parameters for evaluating jjtic metrics
            ' So...if you are on Tic 12, and you want to evaluate those stands without a 12 Rx, what are they?
            ReDim ParentTimingChoice(NStandsInGrid(jgrid))
            maxtic = 4
            If jtic - 4 <= 0 Then maxtic = jtic - 1
            NParentRegen = 0
            ReDim ParentRegenTic(MaxRegenTics), ParentStandRx(NStandsInGrid(jgrid), MaxRegenTics), SubStandRx(NStandsInGrid(jgrid), maxtic, MaxRegenTics)
            For jstand As Integer = 1 To NStandsInGrid(jgrid)
                kaa = GridStands(jgrid, jstand)
                Nrx = AA_NPres(kaa)
                For jrx As Integer = 1 To Nrx
                    If RxIn(kaa, jrx) = 1 Then
                        krx = AA_PresArray(kaa, jrx) 'krx used to access line in prescription database
                        kind = AApres_ISpaceInd(krx)
                        For jstart As Integer = 1 To SpatialSeriesNFirstTic(kind)
                            If SpatialSeriesFirstTic(kind, jstart) = jtic And NParentRegen < MaxRegenTics Then
                                ParentTimingChoice(jstand) = 1
                                kregen = 1 'default
                                'look for regen tics
                                kstart = jstart + 1
                                If kstart <= SpatialSeriesNFirstTic(kind) Then
                                    'check for unique regen tic
                                    tregen = SpatialSeriesFirstTic(kind, kstart)
                                    bUnqRegen = True
                                    For jregen As Integer = 1 To NParentRegen
                                        If ParentRegenTic(jregen) = tregen Then
                                            kregen = jregen
                                            bUnqRegen = False
                                            Exit For
                                        End If
                                    Next
                                    If bUnqRegen Then
                                        NParentRegen = NParentRegen + 1
                                        kregen = NParentRegen
                                        ParentRegenTic(NParentRegen) = tregen
                                    End If
                                End If
                                ParentStandRx(jstand, kregen) = krx 'is this the right spot????
                            End If 'on same parent tic or a qualifying 
                        Next
                    End If
                Next
            Next jstand
            'now go through and collect information about timing choices for stands that don't have a parent timing choice
            ReDim NSubRegen(maxtic), SubRegenTic(maxtic, MaxRegenTics)
            bUsingSubTic = False 'default
            For jstand As Integer = 1 To NStandsInGrid(jgrid)
                If ParentTimingChoice(jstand) = 0 Then
                    kaa = GridStands(jgrid, jstand)
                    Nrx = AA_NPres(kaa)
                    For jrx As Integer = 1 To Nrx
                        If RxIn(kaa, jrx) = 1 Then
                            krx = AA_PresArray(kaa, jrx) 'krx used to access line in prescription database
                            kind = AApres_ISpaceInd(krx)
                            For jstart As Integer = 1 To SpatialSeriesNFirstTic(kind)
                                If SpatialSeriesFirstTic(kind, jstart) < jtic And SpatialSeriesFirstTic(kind, jstart) >= jtic - maxtic Then
                                    'which sub tic are you on? subtic = number of tics below jtic
                                    subtic = jtic - SpatialSeriesFirstTic(kind, jstart)
                                    'look for regen tics
                                    kstart = jstart + 1
                                    kregen = 1 'default
                                    If kstart <= SpatialSeriesNFirstTic(kind) And NSubRegen(subtic) < MaxRegenTics Then
                                        'yep, you sure are using one!
                                        bUsingSubTic = True
                                        'check for unique regen tic
                                        tregen = SpatialSeriesFirstTic(kind, kstart)
                                        bUnqRegen = True
                                        For jregen As Integer = 1 To NSubRegen(subtic)
                                            If SubRegenTic(subtic, jregen) = tregen Then
                                                kregen = jregen
                                                bUnqRegen = False
                                                Exit For
                                            End If
                                        Next
                                        If bUnqRegen Then
                                            NSubRegen(subtic) = NSubRegen(subtic) + 1
                                            SubRegenTic(subtic, NSubRegen(subtic)) = tregen
                                            kregen = NSubRegen(subtic)
                                        End If
                                    End If
                                    SubStandRx(jstand, subtic, kregen) = krx
                                End If
                            Next jstart
                        End If
                    Next jrx
                End If 'parent timing choice does not exist
            Next jstand

            'now go ahead and seed the grid's stands with the different prescription combinations
            For jparentregentic As Integer = 1 To NParentRegen
                NRxChosen = 0
                '#####################
                SpatialGridValTic = -99999999 'BestNSGridVal
                ReDim MasterRxChosen(NStandsInGrid(jgrid)), RxChosen(NStandsInGrid(jgrid)), LockedRx(NStandsInGrid(jgrid)), SubLockedRx(NStandsInGrid(jgrid)), jticProposedRx(NStandsInGrid(jgrid))
                'add defaults for this parentregentic
                For jstand As Integer = 1 To NStandsInGrid(jgrid)
                    MasterRxChosen(jstand) = NSRxChosen(jstand) 'default
                    If ParentStandRx(jstand, jparentregentic) > 0 Then
                        MasterRxChosen(jstand) = ParentStandRx(jstand, jparentregentic)
                        LockedRx(jstand) = 1
                        NRxChosen = NRxChosen + 1
                    Else
                        For jjparentregentic As Integer = 1 To NParentRegen
                            If ParentStandRx(jstand, jjparentregentic) > 0 Then
                                MasterRxChosen(jstand) = ParentStandRx(jstand, jjparentregentic)
                                LockedRx(jstand) = 1
                                NRxChosen = NRxChosen + 1
                            End If
                        Next
                    End If
                    RxChosen(jstand) = MasterRxChosen(jstand)
                Next

                'next cut is to add in potential regen tics to see if you can do better
                If bUsingSubTic = True Then 'find it
                    For jsubtic As Integer = 1 To maxtic
                        For jsubregentic As Integer = 1 To NSubRegen(jsubtic)
                            'need to reset RxChosen to parent-level default
                            '#####################
                            SpatialGridValTic = -99999999 'BestNSGridVal
                            stic = jtic - jsubtic
                            ReDim SubLockedRx(NStandsInGrid(jgrid)), RxChosen(NStandsInGrid(jgrid))
                            For jstand As Integer = 1 To NStandsInGrid(jgrid)
                                RxChosen(jstand) = MasterRxChosen(jstand) 'reset to default
                                If SubStandRx(jstand, jsubtic, jsubregentic) > 0 Then
                                    RxChosen(jstand) = SubStandRx(jstand, jsubtic, jsubregentic)
                                    SubLockedRx(jstand) = 1
                                    'NRxChosen = NRxChosen + 1
                                Else
                                    For jjsubregentic As Integer = 1 To NSubRegen(jsubtic)
                                        If SubStandRx(jstand, jsubtic, jjsubregentic) > 0 Then
                                            RxChosen(jstand) = SubStandRx(jstand, jsubtic, jjsubregentic)
                                            SubLockedRx(jstand) = 1
                                            'NRxChosen = NRxChosen + 1
                                        End If
                                    Next
                                End If
                            Next jstand

                            If NRxChosen > 0 Then
                                'Grid1Opts(jgrid) = Grid1Opts(jgrid) + 1
                                NLocked = 0
                                NSubLocked = 0
                                'first try to add
                                GridTrimOneOpt(StandsInGrid, StandInGrid, RxChosen, LockedRx, SubLockedRx, SpatialGridValTic, True, True, NLocked, NSubLocked, jtic, stic)
                                'then try to pare down
                                GridTrimOneOpt(StandsInGrid, StandInGrid, RxChosen, LockedRx, SubLockedRx, SpatialGridValTic, True, False, NLocked, NSubLocked, jtic, stic)
                            End If

                            If SpatialGridValTic > BestSpatialGridVal(jtic) And NLocked > 0 Then 'And NSubLocked > 0 Then
                                BestSpatialGridVal(jtic) = SpatialGridValTic
                                For jstand As Integer = 1 To NStandsInGrid(jgrid)
                                    StandRxPerGridVal(jtic, jstand) = RxChosen(jstand)
                                Next
                            End If
                        Next jsubregentic
                    Next jsubtic
                Else
                    'Then try a double 1-opt with just the stands in the current jtic solution
                    If NRxChosen > 0 Then
                        'Grid1Opts(jgrid) = Grid1Opts(jgrid) + 1
                        NLocked = 0
                        NSubLocked = 0
                        'first try to add
                        GridTrimOneOpt(StandsInGrid, StandInGrid, MasterRxChosen, LockedRx, SubLockedRx, SpatialGridValTic, True, True, NLocked, NSubLocked, jtic, 0)
                        'then try to pare down
                        GridTrimOneOpt(StandsInGrid, StandInGrid, MasterRxChosen, LockedRx, SubLockedRx, SpatialGridValTic, True, False, NLocked, NSubLocked, jtic, 0)
                    End If

                    If SpatialGridValTic > BestSpatialGridVal(jtic) And NLocked > 0 Then
                        BestSpatialGridVal(jtic) = SpatialGridValTic
                        For jstand As Integer = 1 To NStandsInGrid(jgrid)
                            StandRxPerGridVal(jtic, jstand) = MasterRxChosen(jstand)
                        Next
                    End If
                End If
            Next jparentregentic
        Next jtic


        'Then, choose the highest X number of prescriptions for the grid - make sure these values clear the 
        '  best non-spatial grid value, though!
        For jtic As Integer = 0 To NTic
            OrderedArray(jtic) = BestSpatialGridVal(jtic)
            '##########################
            'If OrderedArray(jtic) = BestNSGridVal Then OrderedArray(jtic) = -999999999 'don't count this
        Next
        Array.Sort(OrderedArray)

        ValueComparePerAcre = Math.Max(OrderedArray(OrderedArray.Length - 1) / GridAcres, BestNSGridVal / GridAcres)

        'THREAD lock the two global arrays

        For jtic As Integer = 1 To NTic
            'find the ordinal rank of the prescription
            For jrank As Integer = 1 To OrderedArray.Length - 1
                'krank = OrderedArray.Length - jrank
                If BestSpatialGridVal(jtic) = OrderedArray(OrderedArray.Length - jrank) Then
                    krank = jrank
                    Exit For
                End If
            Next
            ValCom = BestSpatialGridVal(jtic) / GridAcres
            If BestSpatialGridVal(jtic) = -99999999 Then ValCom = BestNSGridVal / GridAcres
            ''########################

            'debug - find the best ordinal and $/acre rank of each prescription
            For jstand As Integer = 1 To NStandsInGrid(jgrid)
                kaa = GridStands(jgrid, jstand)
                For jrx As Integer = 1 To AA_NPres(kaa)
                    krx = AA_PresArray(kaa, jrx)
                    If AA_PresArray(kaa, jrx) = StandRxPerGridVal(jtic, jstand) Then
                        SyncLock objMyLock1
                            If Math.Abs(ValueComparePerAcre - ValCom) < GridRxHighestVal(kaa, jrx) Then GridRxHighestVal(kaa, jrx) = Math.Abs(ValueComparePerAcre - ValCom)
                            If krank < GridRxHighestRank(kaa, jrx) Then GridRxHighestRank(kaa, jrx) = krank
                            'search for rank in this grid
                        End SyncLock
                        Exit For 'quit searching for appropriate prescription to find - you got it! Move to next stand

                    End If
                Next
            Next
        Next
        SyncLock objMyLock2
            NRunningThreads = NRunningThreads - 1
            sRunningThreads = sRunningThreads - 1
        End SyncLock
        'THREAD unlock the two global arrays


    End Sub

    Private Sub GridRxBuilderSingle1OptParentOnly(ByRef GridRxIn(,) As Byte)
        'looks at grid-wide financial information for each prescription timing choice in each grid and populates
        ' arrays of the value of those presecriptions as well as the associated stand Rx number associated with 
        ' the value. Also, populates a parent RxIn array with the top X number of prescription values for the grid

        Dim BestNSGridVal As Double 'best non-spatial grid value
        Dim RxChosen() As Integer 'the prescription Number chosen for each stand in the grid
        Dim MasterRxChosen() As Integer
        Dim NSRxChosen() As Integer
        Dim BestSpatialGridVal() As Double 'best spatial grid value per period
        Dim SpatialGridValTic As Double ' spatial grid value for the tic - evaluated over several tries within the same tic
        Dim BestSpatialGridValTic As Double
        Dim OrderedArray() As Double 'used to order the values and choose best X Rx
        Dim ValCom As Double
        Dim OrdRank As Integer
        Dim krank As Integer
        Dim ValueCompare As Double
        Dim ValueComparePerAcre As Double
        Dim StandRxPerGridVal(,) As Integer 'the stand's prescription associated with the BestSpatialGridVal
        'Dim GridRxIn(,) As Integer
        Dim NSpatSeries As Integer
        Dim NRxChosen As Integer
        Dim NLocked As Integer
        Dim NSubLocked As Integer

        Dim GridAcres As Single
        'Dim DollarPerAcreTol As Single = 10

        Dim MaxFirstTic As Integer = MxTic 'default
        Dim maxtic As Integer
        Dim kaa As Integer
        Dim krx As Integer
        Dim irx As Integer
        Dim kind As Integer 'spatial index
        Dim kspat As Integer 'spatial type
        Dim kstart As Integer
        Dim kregen As Integer
        Dim tregen As Integer
        Dim subtic As Integer
        Dim stic As Integer
        Dim StandTicAreaVal() As Double
        Dim FullTicAreaVal() As Double
        Dim Nrx As Integer
        Dim StandsInGrid() As Integer
        Dim bRxHit As Boolean
        Dim bUsingSubTic As Boolean
        Dim LockedRx() As Byte
        Dim SubLockedRx() As Byte
        Dim jticProposedRx() As Integer
        Dim StandInGrid() As Byte
        Dim bCalcSpace As Boolean

        Dim ParentTimingChoice() As Integer 'indicates whether the stand in question has a parent timing choice
        Dim NParentRegen As Integer
        Dim NSubRegen() As Integer
        Dim ParentRegenTic() As Integer
        Dim SubRegenTic(,) As Integer
        Dim ParentStandRx(,) As Long 'by jstand,maxparentregentics
        Dim SubStandRx(,,) As Long 'by jstand,subperiod, subperiodregen

        '########## this is artificially constrained to 5 to speed up the process!
        Dim MaxRegenTics As Integer = 5

        Dim bUnqRegen As Boolean
        Dim MatchInd As Integer

        'debug stuff
        Dim GridStartTime() As Double
        Dim GridStopTime() As Double
        Dim Grid1Opts() As Integer
        Dim TrimStartTime As Double
        Dim TrimStopTime As Double

        TrimStartTime = Microsoft.VisualBasic.Timer

        'mine the spatial series information for data about first tic
        NSpatSeries = SpatialSeries.Length - 1
        ReDim SpatialSeriesNFirstTic(NSpatSeries), SpatialSeriesFirstTic(NSpatSeries, MaxFirstTic)
        For jspat As Integer = 1 To NSpatSeries
            'catch time 1
            If SpatialSeries(jspat).Flows(1) = 1 Then
                SpatialSeriesNFirstTic(jspat) = SpatialSeriesNFirstTic(jspat) + 1
                SpatialSeriesFirstTic(jspat, SpatialSeriesNFirstTic(jspat)) = 1
            End If
            For jtic As Integer = 2 To MxTic ' NTic
                If SpatialSeries(jspat).Flows(jtic) = 1 And SpatialSeries(jspat).Flows(jtic - 1) = 0 Then 'have a first flow tic
                    SpatialSeriesNFirstTic(jspat) = SpatialSeriesNFirstTic(jspat) + 1
                    SpatialSeriesFirstTic(jspat, SpatialSeriesNFirstTic(jspat)) = jtic
                End If
            Next
        Next
        ReDim GridStartTime(NGrids), GridStopTime(NGrids), Grid1Opts(NGrids)

        For jgrid As Integer = 1 To NGrids
            GridStartTime(jgrid) = Microsoft.VisualBasic.Timer
            'If jgrid = 309 Then Stop
            ReDim BestSpatialGridVal(NTic), StandRxPerGridVal(NTic, NStandsInGrid(jgrid)), OrderedArray(NTic)
            BestNSGridVal = 0 'reset
            'use this unused index to store the best NS for comparison purposes
            BestSpatialGridVal(0) = -999999999 'BestNSGridVal 'not using this index, so make sure it's last!

            'figure the best non-spatial value of the polys in this grid. Should just be adding up the best non-spatial values...
            ReDim StandsInGrid(NStandsInGrid(jgrid)), NSRxChosen(NStandsInGrid(jgrid)), StandInGrid(AA_NPolys)
            bCalcSpace = False
            GridAcres = 0
            For jstand As Integer = 1 To NStandsInGrid(jgrid)
                kaa = GridStands(jgrid, jstand)
                GridAcres = GridAcres + AA_area(kaa)
                StandsInGrid(jstand) = kaa
                StandInGrid(kaa) = 1
                irx = MaxNSRx(kaa) 'not the base prescription index, but rather the prescription index of the total Rx for this stand...
                BestNSGridVal = BestNSGridVal + DualPlanAAPoly_NPVSelf(kaa, irx) * AA_area(kaa)
                krx = AA_PresArray(kaa, irx)
                kind = AApres_ISpaceInd(krx)
                kspat = AApres_SpaceType(krx)
                If SpatialSeries(kind).NNonZero > 0 Then
                    bCalcSpace = True
                End If
                NSRxChosen(jstand) = krx 'default- prescription index; not stand-based prescription index
            Next
            If bCalcSpace Then
                For jstand As Integer = 1 To NStandsInGrid(jgrid)
                    kaa = GridStands(jgrid, jstand)
                    irx = MaxNSRx(kaa) 'not the base prescription index, but rather the prescription index of the total Rx for this stand...
                    krx = AA_PresArray(kaa, irx)
                    kind = AApres_ISpaceInd(krx)
                    kspat = AApres_SpaceType(krx)
                    If SpatialSeries(kind).NNonZero > 0 Then
                        'have to look for spatial interactions of the IZones
                        ReDim FullTicAreaVal(MxTic), StandTicAreaVal(MxTic)
                        CalcTicAreaVal(FullTicAreaVal, StandTicAreaVal, StandInGrid, StandsInGrid, NSRxChosen, kaa)
                        For jtic As Integer = SpatialSeries(kind).First1Tic To SpatialSeries(kind).Last1tic
                            BestNSGridVal = BestNSGridVal + SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * (StandTicAreaVal(jtic)) 'want it on the total - not a per-acre basis
                        Next jtic
                    End If
                    NSRxChosen(jstand) = krx 'default- prescription index; not stand-based prescription index
                Next
            End If

            'for each tic, figure the max financial value, including a 1-opt search, which rectifies multiple regen timing choices as well as catches stands with first KW corresponding to the regen KW of most of the patch. 
            '   Also, populate array of prescriptions chosen for the stand associated with the value
            '   This is the meat and potatoes of this whole procesing routine!
            BestSpatialGridVal(1) = -99999999
            For jtic As Integer = 1 To NTic
                'default is the best non-spatial solution
                '#####################
                BestSpatialGridVal(jtic) = -99999999 'BestNSGridVal 'default
                For jstand As Integer = 1 To NStandsInGrid(jgrid)
                    StandRxPerGridVal(jtic, jstand) = NSRxChosen(jstand)
                Next

                'It is here that you want to construct the parameters for evaluating jjtic metrics
                ' So...if you are on Tic 12, and you want to evaluate those stands without a 12 Rx, what are they?
                ReDim ParentTimingChoice(NStandsInGrid(jgrid))
                maxtic = 4
                If jtic - 4 <= 0 Then maxtic = jtic - 1
                NParentRegen = 0
                ReDim ParentRegenTic(MaxRegenTics), ParentStandRx(NStandsInGrid(jgrid), MaxRegenTics), SubStandRx(NStandsInGrid(jgrid), maxtic, MaxRegenTics)
                For jstand As Integer = 1 To NStandsInGrid(jgrid)
                    kaa = GridStands(jgrid, jstand)
                    Nrx = AA_NPres(kaa)
                    For jrx As Integer = 1 To Nrx
                        If RxIn(kaa, jrx) = 1 Then
                            krx = AA_PresArray(kaa, jrx) 'krx used to access line in prescription database
                            kind = AApres_ISpaceInd(krx)
                            For jstart As Integer = 1 To SpatialSeriesNFirstTic(kind)
                                If SpatialSeriesFirstTic(kind, jstart) = jtic And NParentRegen < MaxRegenTics Then
                                    ParentTimingChoice(jstand) = 1
                                    kregen = 1 'default
                                    'look for regen tics
                                    kstart = jstart + 1
                                    If kstart <= SpatialSeriesNFirstTic(kind) Then
                                        'check for unique regen tic
                                        tregen = SpatialSeriesFirstTic(kind, kstart)
                                        bUnqRegen = True
                                        For jregen As Integer = 1 To NParentRegen
                                            If ParentRegenTic(jregen) = tregen Then
                                                kregen = jregen
                                                bUnqRegen = False
                                                Exit For
                                            End If
                                        Next
                                        If bUnqRegen Then
                                            NParentRegen = NParentRegen + 1
                                            kregen = NParentRegen
                                            ParentRegenTic(NParentRegen) = tregen
                                        End If
                                    End If
                                    ParentStandRx(jstand, kregen) = krx 'is this the right spot????
                                End If 'on same parent tic or a qualifying 
                            Next
                        End If
                    Next
                Next jstand

                'now go ahead and seed the grid's stands with the different prescription combinations
                For jparentregentic As Integer = 1 To NParentRegen
                    NRxChosen = 0
                    '#####################
                    SpatialGridValTic = -99999999 'BestNSGridVal
                    ReDim MasterRxChosen(NStandsInGrid(jgrid)), RxChosen(NStandsInGrid(jgrid)), LockedRx(NStandsInGrid(jgrid)), SubLockedRx(NStandsInGrid(jgrid)), jticProposedRx(NStandsInGrid(jgrid))
                    'add defaults for this parentregentic
                    For jstand As Integer = 1 To NStandsInGrid(jgrid)
                        MasterRxChosen(jstand) = NSRxChosen(jstand) 'default
                        If ParentStandRx(jstand, jparentregentic) > 0 Then
                            MasterRxChosen(jstand) = ParentStandRx(jstand, jparentregentic)
                            LockedRx(jstand) = 1
                            NRxChosen = NRxChosen + 1
                        Else
                            For jjparentregentic As Integer = 1 To NParentRegen
                                If ParentStandRx(jstand, jjparentregentic) > 0 Then
                                    MasterRxChosen(jstand) = ParentStandRx(jstand, jjparentregentic)
                                    LockedRx(jstand) = 1
                                    NRxChosen = NRxChosen + 1
                                End If
                            Next
                        End If
                        RxChosen(jstand) = MasterRxChosen(jstand)
                    Next



                    'Then try a single 1-opt with just the stands in the current jtic solution
                    If NRxChosen > 0 Then
                        Grid1Opts(jgrid) = Grid1Opts(jgrid) + 1
                        NLocked = 0
                        NSubLocked = 0
                        'first try to add
                        'GridTrimOneOpt(StandsInGrid, StandInGrid, MasterRxChosen, LockedRx, SubLockedRx, SpatialGridValTic, True, True, NLocked, NSubLocked, jtic, 0)
                        'then try to pare down
                        GridTrimOneOpt(StandsInGrid, StandInGrid, MasterRxChosen, LockedRx, SubLockedRx, SpatialGridValTic, True, False, NLocked, NSubLocked, jtic, 0)
                    End If

                    If SpatialGridValTic > BestSpatialGridVal(jtic) And NLocked > 0 Then
                        BestSpatialGridVal(jtic) = SpatialGridValTic
                        For jstand As Integer = 1 To NStandsInGrid(jgrid)
                            StandRxPerGridVal(jtic, jstand) = MasterRxChosen(jstand)
                        Next
                    End If

                Next jparentregentic
            Next jtic


            'Then, choose the highest X number of prescriptions for the grid - make sure these values clear the 
            '  best non-spatial grid value, though!
            For jtic As Integer = 0 To NTic
                OrderedArray(jtic) = BestSpatialGridVal(jtic)
                '##########################
                'If OrderedArray(jtic) = BestNSGridVal Then OrderedArray(jtic) = -999999999 'don't count this
            Next
            Array.Sort(OrderedArray)
            'If MaxNumPres > OrderedArray.Length Then
            '    ValueCompare = OrderedArray(1)
            'Else
            '    ValueCompare = OrderedArray(OrderedArray.Length - MaxNumPres) 'largest values are at the end - find the lowest value to keep
            'End If
            ValueComparePerAcre = Math.Max(OrderedArray(OrderedArray.Length - 1) / GridAcres, BestNSGridVal / GridAcres)
            For jtic As Integer = 1 To NTic
                'find the ordinal rank of the prescription
                For jrank As Integer = 1 To OrderedArray.Length - 1
                    'krank = OrderedArray.Length - jrank
                    If BestSpatialGridVal(jtic) = OrderedArray(OrderedArray.Length - jrank) Then
                        krank = jrank
                        Exit For
                    End If
                Next
                ValCom = BestSpatialGridVal(jtic) / GridAcres
                If BestSpatialGridVal(jtic) = -99999999 Then ValCom = BestNSGridVal / GridAcres
                '########################
                If ValCom + DollarPerAcreTol > ValueComparePerAcre Then 'ValueCompare Then 'And BestSpatialGridVal(jtic) >= BestNSGridVal Then
                    'keep the prescriptions that created this great value!
                    For jstand As Integer = 1 To NStandsInGrid(jgrid)
                        kaa = GridStands(jgrid, jstand)

                        For jrx As Integer = 1 To AA_NPres(kaa)
                            krx = AA_PresArray(kaa, jrx)
                            'If kaa = 376 And jrx = 92 Then Stop
                            'If kaa = 376 And jrx = 221 Then Stop
                            If AA_PresArray(kaa, jrx) = StandRxPerGridVal(jtic, jstand) Then
                                'If krx = 47869 Then Stop
                                GridRxIn(kaa, jrx) = 1
                                Exit For 'quit searching for appropriate prescription to find - you got it! Move to next stand
                            End If
                        Next
                    Next
                End If
                'debug - find the best ordinal and $/acre rank of each prescription
                For jstand As Integer = 1 To NStandsInGrid(jgrid)
                    kaa = GridStands(jgrid, jstand)
                    'If kaa = 376 Then Stop
                    For jrx As Integer = 1 To AA_NPres(kaa)
                        krx = AA_PresArray(kaa, jrx)
                        'If kaa = 376 And jrx = 92 Then Stop
                        'If kaa = 376 And jrx = 221 Then Stop
                        If AA_PresArray(kaa, jrx) = StandRxPerGridVal(jtic, jstand) Then
                            If Math.Abs(ValueComparePerAcre - ValCom) < GridRxHighestVal(kaa, jrx) Then GridRxHighestVal(kaa, jrx) = Math.Abs(ValueComparePerAcre - ValCom)
                            If krank < GridRxHighestRank(kaa, jrx) Then GridRxHighestRank(kaa, jrx) = krank
                            'search for rank in this grid
                            Exit For 'quit searching for appropriate prescription to find - you got it! Move to next stand
                        End If
                    Next
                Next
            Next
            GridStopTime(jgrid) = Microsoft.VisualBasic.Timer
        Next jgrid

        'debug stuff
        TrimStopTime = Microsoft.VisualBasic.Timer
        'SPATIAL INDEX BY RX DATA FILE
        Dim f86 As Integer
        f86 = FreeFile()
        FileOpen(f86, FnTrimmeroutBaseA & "_Iter" & IterationNumber & "_GridDebug.txt", OpenMode.Output)
        PrintLine(f86, "TotalTime")
        PrintLine(f86, Math.Round(TrimStopTime - TrimStartTime, 2))
        PrintLine(f86, "GridNum, Time, N1Opts")
        For jgrid As Integer = 1 To NGrids
            PrintLine(f86, jgrid & "," & Math.Round(((GridStopTime(jgrid) - GridStartTime(jgrid))), 2) & "," & Grid1Opts(jgrid))
        Next
        FileClose(f86)

    End Sub
    Private Sub GridTrimOneOpt(ByVal Stands() As Integer, ByVal StandInGrid() As Byte, ByRef ProposedRx() As Integer, ByVal LockedRx() As Byte, ByVal SubLockedRx() As Byte, ByRef GridValue As Double, ByVal bDoOneOpt As Boolean, ByVal bLockSpatialSol As Boolean, ByRef NLocked As Integer, ByRef NSubLocked As Integer, ByVal TTic As Integer, ByVal STic As Integer) ', ByVal AddSubtract As String)
        'searches for stands that don't have a prescription that starts KW in the given period and looks for whether it
        ' has a prescription that can increase the spatial grid value
        ' Catches stuff like Red Pine that doesn't regen and add to the pot until late in the planning horizon
        ' May also decide to exclude a prescription from KW if it is a poor addition to the grid
        'bLockSpatialSol indicates whether or not to allow the stand to switch to non-KW

        Dim NRx As Integer
        Dim taa As Integer
        Dim krx As Integer
        Dim kind As Integer
        Dim kspat As Integer
        Dim AAArea As Double 'the area of the AA
        Dim StandTicAreaVal() As Double 'for all stands influenced by the stand, what's the per-tic potential area in each time period?
        Dim FullTicAreaVal() As Double
        Dim AANPV As Single
        Dim StandAANPV As Single
        Dim BestAApres As Integer
        Dim mxNPV As Single
        Dim mxStandNPV As Single
        Dim NRxChange As Integer 'the number of prescriptions that this iteration changes...loop until it all 'shakes out'

        Dim NStands As Integer
        Dim bEvalRx As Boolean
        Dim CountLocked() As Integer
        Dim SubCountLocked() As Integer

        '#############This added for debug; can compute efficiency another place
        'Dim ZoneEff(,) As Single 'efficiency of the zone in creating Interior space - used to modify/calculate assumed added value to the stand, by zone, tic
        Dim Efficiency As Double = 1

        'figure out whether the stand is in the grid and whether it has a locked timing choice
        NStands = Stands.Length - 1
        NLocked = 0
        NSubLocked = 0
        'count locked and sublocked
        ReDim CountLocked(NStands), SubCountLocked(NStands)
        For jaa As Integer = 1 To NStands
            If LockedRx(jaa) = 1 Then NLocked = NLocked + 1
            If SubLockedRx(jaa) = 1 Then NSubLocked = NSubLocked + 1
            CountLocked(jaa) = LockedRx(jaa)
            SubCountLocked(jaa) = SubCountLocked(jaa)
        Next

        ' Loop through the set of input data.
        NRxChange = NStands
        Do Until NRxChange = 0 '/ AA_NPolys <= OneOptStop 'used to be until nrxchange = 0
            NRxChange = 0
            GridValue = 0
            'ReDim AAEvaluated(NStands)
            For jaa As Integer = 1 To NStands 'AASetNAA(kaaset)
                taa = Stands(jaa)
                AAArea = AA_area(taa)
                NRx = AA_NPres(taa)
                mxNPV = -9999999
                mxStandNPV = mxNPV

                BestAApres = 0

                'look at the schedule of the neighbors and figure out the maximum area in this stand that 
                'would be in ISpace if the evaluated schedule matches with them

                'figure out the influenced stands to evaluate prescriptions against...
                'go through influence zones this stand is in...find the potential spatial added value regardless of 
                ' the prescription chosen by the stand in question

                ReDim StandTicAreaVal(MxTic), FullTicAreaVal(MxTic)
                CalcTicAreaVal(FullTicAreaVal, StandTicAreaVal, StandInGrid, Stands, ProposedRx, taa)

                If bDoOneOpt = False Then
                    BestAApres = ProposedRx(jaa)
                    kind = AApres_ISpaceInd(BestAApres)
                    kspat = AApres_SpaceType(BestAApres)
                    krx = 0
                    If BestAApres > 0 Then
                        For jrx As Integer = 1 To NRx
                            If BestAApres = AA_PresArray(taa, jrx) Then
                                krx = jrx
                                Exit For
                            End If
                        Next
                    End If
                    mxNPV = DualPlanAAPoly_NPVSelf(taa, krx) * AAArea

                    'Then, add possible spatial value of the whole footprint - the only part of the overall NPV that will change is within the scope of the 
                    '   stands this AA/Rx influences
                    ' The Rx either aligns or it doesn't - if not, then the whole area and value of the zone is off the table for that TIC
                    For jtic As Integer = SpatialSeries(kind).First1Tic To SpatialSeries(kind).Last1tic
                        mxNPV = mxNPV + SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * (StandTicAreaVal(jtic)) 'want it on the total - not a per-acre basis
                    Next jtic
                    mxStandNPV = mxNPV
                End If

                If bDoOneOpt = True Then
                    'find the prescription for this AA that has the highest value, as lined up with its neighbors.
                    'If bLockSpatialSol = False Or (bLockSpatialSol = True And LockedRx(jaa) = 0 And SubLockedRx(jaa) = 0) Then
                    For jrx As Integer = 1 To NRx
                        If RxIn(taa, jrx) = 1 Then
                            'have to make sure if this is a locked AA that either you have a firsttic that matches Ttic or you have a non-spatial prescription
                            krx = AA_PresArray(taa, jrx) 'krx used to access line in prescription database
                            kind = AApres_ISpaceInd(krx)
                            kspat = AApres_SpaceType(krx)
                            bEvalRx = True
                            'check for matching MaxNSRx or having some sort of spatial value

                            If bLockSpatialSol = True And LockedRx(jaa) = 1 And krx <> ProposedRx(jaa) Then 'jrx <> MaxNSRx(taa) Then 'And krx <> MaxNSRx(taa) Then 'check to see if the rx is the best non-spatial
                                bEvalRx = False
                            ElseIf bLockSpatialSol = True And SubLockedRx(jaa) = 1 And krx <> ProposedRx(jaa) Then ' jrx <> MaxNSRx(taa) Then 'And krx <> MaxNSRx(taa) Then 'check to see if the rx is the best non-spatial
                                bEvalRx = False
                            End If

                            ''First, find the value of the aspatial flows
                            If bEvalRx = True Then 'only allow a change if it is unlocked, or it shares the same 
                                AANPV = DualPlanAAPoly_NPVSelf(taa, jrx) * AAArea
                                StandAANPV = AANPV
                                'Then, add possible spatial value of the whole footprint - the only part of the overall NPV that will change is within the scope of the 
                                '   stands this AA/Rx influences
                                ' The Rx either aligns or it doesn't - if not, then the whole area and value of the zone is off the table for that TIC
                                For jtic As Integer = SpatialSeries(kind).First1Tic To SpatialSeries(kind).Last1tic
                                    AANPV = AANPV + SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * FullTicAreaVal(jtic) 'FullTicAreaVal(jtic) 'want it on the total - not a per-acre basis
                                    StandAANPV = StandAANPV + SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * StandTicAreaVal(jtic)
                                Next jtic
                                'If bLockSpatialSol = True Then ' trying to add AddSubtract = "add" Then
                                If AANPV > mxNPV Then
                                    mxNPV = AANPV
                                    mxStandNPV = StandAANPV
                                    BestAApres = krx
                                End If
                             
                            End If
                        End If
                    Next jrx
                End If

                'catch the prescription with the highest npv - and track how many polys are changing Rx
                If ProposedRx(jaa) <> BestAApres Then
                    NRxChange = NRxChange + 1
                    If CountLocked(jaa) = 1 Then
                        NLocked = NLocked - 1
                        CountLocked(jaa) = 0
                    ElseIf SubCountLocked(jaa) = 1 Then
                        NSubLocked = NSubLocked - 1
                        SubCountLocked(jaa) = 0
                    End If
                    ProposedRx(jaa) = BestAApres
                End If
                'End If
                GridValue = GridValue + mxStandNPV 'summed over all aas - will 0 out if you have a changed Rx for a stand though
            Next jaa
        Loop


    End Sub

    Private Sub CalcTicAreaVal(ByRef FullTicAreaVal() As Double, ByRef StandTicAreaVal() As Double, ByVal StandInGrid() As Byte, ByVal StandsInGrid() As Integer, ByVal ProposedRx() As Integer, ByVal taa As Integer)
        'returns TicAreaVal populated by TIC
        'AreaType describes if you are counting the area of the full zone in the valuation or just the portion in "taa"
        Dim zoneID As Integer
        'Dim TicAreaVal() As Double 'for all stands influenced by the stand, what's the per-tic potential area in each time period?
        Dim ZoneAreaVal() As Double 'for the evaluated zone, what's the potential ISpace by Tic independent of the stand being evaluated, but accounting for the chosen schedules of the other two stands
        Dim kaa As Integer
        Dim bAllStandsInGrid As Boolean
        Dim krx As Integer
        Dim kind As Integer
        Dim FullThisZoneArea As Double 'total area in the influence zone
        Dim StandThisZoneArea As Double 'Area of influence zone in just the stand

        For jiz As Integer = 1 To DualPlanAAPoly_NIZones(taa)
            zoneID = DualPlanAAPoly_IZones(taa, jiz)
            'see if all stands in the zone are in the grid
            bAllStandsInGrid = True
            'find whether all stands in the zone are in the grid - don't have to evaluate the zone if it doesn't count in this grid
            For jdim As Integer = 1 To Zonedim(zoneID)
                kaa = ZoneAAlist(zoneID, jdim)
                If StandInGrid(kaa) = False Then
                    bAllStandsInGrid = False
                    Exit For
                End If
            Next
            If bAllStandsInGrid = True Then
                ReDim ZoneAreaVal(MxTic)
                For jtic As Integer = 1 To MxTic
                    '###########THIS PARAMETER IS SENSITIVE - IT CAUSES SIGNIFICANT CHANGES IN THE OUTCOMES OF THE TRIMMER!!!
                    ZoneAreaVal(jtic) = 1 'default to 1
                Next
                StandThisZoneArea = 0
                FullThisZoneArea = 0
                For jjaa As Integer = 1 To Zonedim(zoneID)
                    kaa = ZoneAAlist(zoneID, jjaa)
                    If kaa <> taa Then
                        'need to find what it's proposed Rx is - based on its order in the StandsInGrid
                        krx = 0
                        For jstand As Integer = 1 To StandsInGrid.Length - 1
                            If kaa = StandsInGrid(jstand) Then
                                krx = ProposedRx(jstand)
                                Exit For
                            End If
                        Next

                        kind = AApres_ISpaceInd(krx)
                        For jtic As Integer = 1 To MxTic
                            ZoneAreaVal(jtic) = ZoneAreaVal(jtic) * SpatialSeries(kind).Flows(jtic)
                        Next
                    End If

                    If Zonedim(zoneID) > 1 Then
                        FullThisZoneArea = FullThisZoneArea + ZoneAAlistAreaS(zoneID, jjaa)
                        If kaa = taa And Zonedim(zoneID) > 1 Then StandThisZoneArea = StandThisZoneArea + ZoneAAlistAreaS(zoneID, jjaa) 'only includes currently analyzed stand
                    End If

                Next

                For jtic As Integer = 1 To MxTic
                    FullTicAreaVal(jtic) = FullTicAreaVal(jtic) + ZoneAreaVal(jtic) * FullThisZoneArea * 1 'ZoneEff(zoneID, jtic)
                    StandTicAreaVal(jtic) = StandTicAreaVal(jtic) + ZoneAreaVal(jtic) * StandThisZoneArea * 1 'ZoneEff(zoneID, jtic)
                Next
            End If
        Next jiz

    End Sub
    Private Sub DualPlanRxEcon(ByRef iRxIn(,) As Byte, ByRef MxNSRx() As Integer)
        'all this does is filter out the non-spatial prescriptions except for the best non-spatial
        'does NOT account for spatial interactions of neighboring stands and prescriptions
        'also should identify the maximum non-ispace solution for inclusion in the DP
        Dim jaa As Integer
        Dim jrx As Integer
        Dim mrx As Integer 'the non-ispace rx with the maximum NPV
        Dim lrx As Integer 'the rx associated with the NPVLbnd
        Dim krx As Integer
        Dim ktic As Integer
        Dim kcoversite As Integer
        Dim kage As Integer
        Dim kspat As Integer
        Dim kind As Integer
        Dim NRx As Integer
        Dim MaxNSNPV As Double 'max non-spatial npv
        Dim NPVLbnd As Double
        Dim AAArea As Double 'the area of the AA
        Dim InteriorISpaceAreaRatio As Double
        Dim LastExTic As Integer = 0
        Dim FirstRgTic As Integer = 0
        Dim LastRgTic As Integer = 0


        For jaa = 1 To AA_NPolys
            'refresh the parameters
            'If jaa = 10217 Then Stop
            NPVLbnd = 0
            If AA_polyid(jaa) > 0 Then
                'If AA_polyid(jaa) = 5107 Then Stop
                AAArea = AA_area(jaa)
                InteriorISpaceAreaRatio = DualPlanAAPoly_ISpaceSelf(jaa) / AAArea

                NRx = AA_NPres(jaa)
                MaxNSNPV = -9999999999

                mrx = 0
                lrx = 0
                For jrx = 1 To NRx
                    krx = AA_PresArray(jaa, jrx)
                    kind = AApres_ISpaceInd(krx)
                    kspat = AApres_SpaceType(krx)
                    kcoversite = AApres_Coversite(krx, ktic)
                    kage = AApres_Age(krx, ktic)

                    'DEFAULT TO INCLUDE THE RX
                    iRxIn(jaa, jrx) = 1

                    'want to only keep in the ASPATIAL Rx with the greatest npv...so...
                    If SpatialSeries(kind).PersistentSpatial = False Then
                        iRxIn(jaa, jrx) = 0
                    End If
                    If SpatialSeries(kind).PersistentSpatial = False And DualPlanAAPoly_NPVSelf(jaa, jrx) > MaxNSNPV Then
                        mrx = jrx
                        MaxNSNPV = DualPlanAAPoly_NPVSelf(jaa, jrx)
                    End If

                    If jrx = 1 Then
                        NPVLbnd = DualPlanAAPoly_NPVSelf(jaa, jrx)
                    Else
                        If DualPlanAAPoly_NPVSelf(jaa, jrx) > NPVLbnd Then
                            NPVLbnd = DualPlanAAPoly_NPVSelf(jaa, jrx)
                            lrx = jrx
                        End If
                    End If

                Next jrx
                DualPlanAAPoly_NPValLbnd(jaa) = NPVLbnd
                'keep the non-spatial rx in the solution IF it is larger than NPVLbnd. mrx flags this as you are going through
                If DualPlanAAPoly_NPVSelf(jaa, mrx) >= NPVLbnd - 0.0001 Then
                    iRxIn(jaa, mrx) = 1
                    MxNSRx(jaa) = mrx 'flag this anyway
                Else
                    MxNSRx(jaa) = lrx
                End If

            End If
        Next

    End Sub

    Private Sub TrimEcon(ByRef iRxIn(,) As Byte, ByRef RxValueWithSpace(,) As Single, ByVal TrimTol As Double, ByVal ISpaceRatioVal As Single)
        'looks at the total ISpace potential of the stands in an influence
        ' zone and compares that to the current prescription of the stand being evaluated. Also tracks
        ' influence zones where the alignment potential is 0, thus eliminating the zone from need for 
        ' consideration

        'sub trims out spatial max prescriptions that are less than the lowest non-spatial NPV value
        ' so...even if the stand got credit for ALL of the interior space it could create + outside its borders, it couldn't compete 
        ' with the best "NPVLbnd" - 'spatial minimum' Rx, then trim it out...

        'uses the AA, AAPres, AAFlow as well as the Spatial, Market, and Condition set shadow price data
        'to calculate the NPV values for all DualPlan model 1 prescriptions...

        ' TrimTol used to control prescriptions that aren't at least this much higher than the best NPV without considering spatial interaction
        ' ISpaceRatioVal used to control how much "footprint" value the prescription gets when ranking...

        Dim jaa As Integer
        'Dim kaa As Integer 'the index of the anchor stand
        Dim zstand As Integer
        Dim jrx As Integer
        Dim NRx As Integer
        Dim NTrim As Integer = 1

        Dim krx As Integer
        Dim kind As Integer
        Dim kspat As Integer

        Dim jmap As Integer

        Dim AAArea As Double 'the area of the AA
        Dim zoneID As Integer

        Dim MaxTicAlign As Integer 'the maximum number of periods stuff can be in ispace and align...by spacetype

        Dim ValAdd As Double 'value to add to the prescription
        Dim ZoneArea As Double 'total area in the influence zone
        Dim TicMult As Integer
        Dim KeepRx As Integer 'whether to keep (1) or trim (0) the rx
        'Dim KeepZoneRx(,,,) As Byte 'by nZones, maxStandsinZone, NSpaceTypes, maxSpatialRx(any space type)

        'GlobalTrim = 0

        'find the value of the 'footprint'
        'loop through the relevant izones, and assume that the suite of prescriptions that create the maximum 
        'value are chosen are the ones picked by the solver. What's the value of those prescriptions?
        'Is it more than the Lbnd value for the AA? If not, then trim it!

        'continue looping through all AAs and prescriptions until none are trimmed. After all, once you trim
        ' one, it may affect the value of another stand in the influence zone...

        ReDim AASpacePotential(AA_NPolys, NSpaceType, MxTic)
        ReDim KeepZoneSpace(NSpaceType, NZoneSubFor)
        'ReDim TrimmedEcon(AA_NPolys, MxStandRx)
        'default influence zone "keep" as 1
        For jspace As Integer = 1 To NSpaceType
            For jiz As Integer = 1 To NZoneSubFor
                KeepZoneSpace(jspace, jiz) = 1
            Next
        Next

        For jaa = 1 To AA_NPolys
            'now tally all periods of possible spatial contribution per stand. If any RxIn contributes a spatial value
            ' in any period, it gets set to a 1.
            For jspace As Integer = 1 To NSpaceType
                If TallySpacePotential(AASpacePotential, iRxIn, jaa, jaa, jspace) = 0 Then 'trim out the zone for this space type
                    For jiz As Integer = 1 To DualPlanAAPoly_NIZones(jaa)
                        zoneID = DualPlanAAPoly_IZones(jaa, jiz)
                        KeepZoneSpace(jspace, zoneID) = 0 'Assumes all polys in the zone must have at least one period of space production
                    Next jiz
                End If
            Next
        Next jaa


        Do Until NTrim = 0
            NTrim = 0
            For jaa = 1 To AA_NPolys
                AAArea = AA_area(jaa)
                If AA_polyid(jaa) > 0 Then
                    'kaa = jaa
                    NRx = AA_NPres(jaa) 'AA(jaa).PresArray.Length - 1
                    AAArea = AA_area(jaa)
                    DualPlanAAPoly_NPVallMx(jaa) = DualPlanAAPoly_NPValLbnd(jaa) 'default
                    ReDim MapLayerColor(NMapLayer)
                    For jmap = 1 To NMapLayer
                        MapLayerColor(jmap) = AA_MapLayerColor(jaa, jmap)
                    Next jmap

                    'now go through and for each prescription of the stand, compare potential ISpace to the Zone's potential
                    For jrx = 1 To NRx
                        KeepRx = 0
                        krx = AA_PresArray(jaa, jrx) 'krx used to access line in prescription database
                        kind = AApres_ISpaceInd(krx)
                        kspat = AApres_SpaceType(krx)

                        MaxTicAlign = SpatialSeries(kind).NNonZero

                        'has the rx been trimmed (want the non-trimmed) and is it a <potential> spatial rx?
                        If iRxIn(jaa, jrx) = 1 And MaxTicAlign > 0 Then 'If TrimPoly(kaa).RxIn(jrx) = 1 And MaxTicAlign > 0 Then ' FIGURE THE MAX VALUE FOR EACH RX And DlPlM1Poly(kaa).NPVSelf(jrx) < DlPlM1Poly(kaa).NPValLbnd Then 'only need to evaluate the Rx if it's less than the LBnd
                            'up the feasible prescriptions of the other stands in all influence zones around the 
                            'current polygon

                            ''#########REQUIRED...IF YOU HAVE GOTTEN TO THIS POINT, NEED TO CHECK TO SEE IF YOU NEED TO trim OR NOT
                            ValAdd = 0
                            For jiz As Integer = 1 To DualPlanAAPoly_NIZones(jaa) 'ON INPUT, THE 1-WAY INTERACTIONS ARE FILTERED OUT
                                zoneID = DualPlanAAPoly_IZones(jaa, jiz)
                                'If KeepRx = 1 Then Exit For
                                ZoneArea = 0
                                If KeepZoneSpace(kspat, zoneID) > 0 Then
                                    For jstand As Integer = 1 To Zonedim(zoneID) 'get whether the izone lines up for this period
                                        ZoneArea = ZoneArea + ZoneAAlistAreaS(zoneID, jstand)
                                    Next jstand
                                    'If MaxTicAlign > 0 Then 'only need to check this spatial def if there are values

                                    For jtic As Integer = SpatialSeries(kind).First1Tic To SpatialSeries(kind).Last1tic 'COULD ITERATE FROM FIRST TIC FLOW WITH A '1' TO THE LAST
                                        TicMult = 1
                                        For jstand As Integer = 1 To Zonedim(zoneID) 'get whether the izone lines up for this period
                                            zstand = ZoneAAlist(zoneID, jstand)
                                            TicMult = TicMult * AASpacePotential(zstand, kspat, jtic)
                                            'TicMult = TicMult * SpatialSeries(kkind).Flows(jtic) 'DlPlM1Poly(zstand).ISpaceSeries(MaxMatchRx(jinf), jspat, jtic)
                                        Next jstand
                                        'now for this particular prescription!
                                        TicMult = TicMult * SpatialSeries(kind).Flows(jtic)
                                        ValAdd = ValAdd + TicMult * ISpaceRatioVal * (SpatTypePr(kspat, jtic) * (ZoneArea / AAArea)) 'divide by AAArea to translate this into a per-acre value

                                    Next jtic
                                    'End If
                                End If
                            Next jiz

                            'Tally the value of this prescription considering spatial interactions
                            RxValueWithSpace(jaa, jrx) = DualPlanAAPoly_NPVSelf(jaa, jrx) + ValAdd 'TrimPoly(kaa).NPValMxSpat(jrx) = TrimPoly(kaa).NPVSelf(jrx) + ValAdd

                            ''compare to npvaspatial because you have 1 way influence zones that come into account here - except make sure you keep at least the prescription that wins when it's a spatial prescription
                            If DualPlanAAPoly_NPVSelf(jaa, jrx) + ValAdd >= DualPlanAAPoly_NPValLbnd(jaa) * TrimTol Or DualPlanAAPoly_NPVSelf(jaa, jrx) = DualPlanAAPoly_NPValLbnd(jaa) Then
                                KeepRx = 1
                            End If

                            'if after you go through all this and you decide not to keep the Rx, then trim it
                            'only trim out spatial value prescriptions, though - still keep the best non-spatial
                            'If KeepRx = 0 And iRxIn(jaa, jrx) = 1 And SpatialSeries(kind).NNonZero > 0 Then 'If KeepRx = 0 And TrimPoly(kaa).RxIn(jrx) = 1 And SpatialSeries(kind).NNonZero > 0 Then 'DlPlM1Poly(kaa).RxISpace(jrx) = 1 Then
                            If KeepRx = 0 And iRxIn(jaa, jrx) = 1 And SpatialSeries(kind).PersistentSpatial = True Then
                                iRxIn(jaa, jrx) = 0
                                NTrim = NTrim + 1
                                GlobalTrim = GlobalTrim + 1

                                'refigure the potential of this stand in light of the trimmed prescription
                                If TallySpacePotential(AASpacePotential, iRxIn, jaa, jaa, kspat) = 0 Then 'trim out the zone for this space type AND reset AASpacePotential modified to trimmed prescription
                                    For jiz As Integer = 1 To DualPlanAAPoly_NIZones(jaa)
                                        zoneID = DualPlanAAPoly_IZones(jaa, jiz)
                                        KeepZoneSpace(kspat, zoneID) = 0
                                    Next jiz
                                End If
                            End If

                            'FIGURE THE NPVVALMAX
                            'If TrimPoly(kaa).NPValMxSpat(jrx) > TrimPoly(kaa).NPVallMx Then TrimPoly(kaa).NPVallMx = TrimPoly(kaa).NPValMxSpat(jrx)
                            If RxValueWithSpace(jaa, jrx) > DualPlanAAPoly_NPVallMx(jaa) Then DualPlanAAPoly_NPVallMx(jaa) = RxValueWithSpace(jaa, jrx)
                        End If 'has not been trimmed
                    Next jrx

                    'calculate the ratio of spatial max to spatial min (for this prescription(?)...to use in the aspatial schedule routine
                    'Looking to compute EstISpacePct or EstISpacePct()
                    If DualPlanAAPoly_NPVallMx(jaa) <> 0 Then
                        DualPlanAAPoly_EstISpacePct(jaa) = 1 - Math.Min(1, Math.Abs(DualPlanAAPoly_NPValLbnd(jaa) / DualPlanAAPoly_NPVallMx(jaa)))
                    Else
                        DualPlanAAPoly_EstISpacePct(jaa) = 0
                    End If

                End If 'polyID > 0
            Next jaa
        Loop

    End Sub

    Private Sub TrimSpatialDom(ByRef iRxIn(,) As Byte)
        'For each prescription of each stand that has not been trimmed...
        '1. compare to each other non-trimmed prescription
        '2. If you find a spatially equal or superior prescription, then
        '3. Trim current prescription if the PNV is lower than that other prescription
        ' Subroutine uses NPVSelf (which includes the value of the interior of the stand)
        ' and only modifies the RxIn(jrx) flag...
        Dim jaa As Integer
        Dim kaa As Integer 'the index of the anchor stand
        Dim SpatialTicMatch As Boolean 'whether you get same or greater spatial benefit from the evaluated prescription
        Dim jrx As Integer
        Dim jjrx As Integer
        Dim NRx As Integer
        Dim NTrim As Integer = 1

        Dim krx As Integer
        Dim kind As Integer
        Dim kkrx As Integer
        Dim kkind As Integer
        Dim kspat As Integer
        Dim kkspat As Integer

        Dim MaxTicAlign As Integer 'the maximum number of periods stuff can be in ispace and align...by spacetype
        Dim MaxMatchTicAlign As Integer 'the influenced stand number of tics that align wihth ispace value
        'ReDim TrimmedSpatialDom(AA_NPolys, MxStandRx)

        'GlobalTrim = 0

        For jaa = 1 To AA_NPolys
            'AAArea = AA_area(jaa)
            If AA_polyid(jaa) > 0 Then
                kaa = jaa
                NRx = AA_NPres(jaa) 'AA(jaa).PresArray.Length - 1
                For jrx = 1 To NRx
                    'KeepRx = 0
                    If iRxIn(jaa, jrx) = 1 Then 'If TrimPoly(jaa).RxIn(jrx) = 1 Then
                        krx = AA_PresArray(kaa, jrx) 'krx used to access line in prescription database
                        kind = AApres_ISpaceInd(krx)
                        kspat = AApres_SpaceType(krx)

                        MaxTicAlign = SpatialSeries(kind).NNonZero
                        For jjrx = 1 To NRx
                            kkrx = AA_PresArray(kaa, jjrx) 'krx used to access line in prescription database
                            kkind = AApres_ISpaceInd(kkrx)
                            kkspat = AApres_SpaceType(kkrx)
                            MaxMatchTicAlign = SpatialSeries(kkind).NNonZero
                            SpatialTicMatch = True
                            'potential better prescription is still in, 
                            ' has the same space type, 
                            ' has a higher NPV
                            ' and has at least as many periods of spatial value as the current prescription,
                            ' and you're dealing with a spatial prescription...

                            'If jrx <> jjrx And TrimPoly(kaa).RxIn(jjrx) = 1 And kspat = kkspat _
                            '   And MaxMatchTicAlign >= MaxTicAlign And _
                            '   TrimPoly(kaa).NPVSelf(jjrx) >= TrimPoly(kaa).NPVSelf(jrx) And MaxTicAlign > 0 Then
                            If jrx <> jjrx And iRxIn(kaa, jjrx) = 1 And kspat = kkspat _
                               And MaxMatchTicAlign >= MaxTicAlign And _
                               DualPlanAAPoly_NPVSelf(kaa, jjrx) >= DualPlanAAPoly_NPVSelf(kaa, jrx) And MaxTicAlign > 0 Then
                                For jtic As Integer = SpatialSeries(kind).First1Tic To SpatialSeries(kind).Last1tic
                                    'if the parent Rx has a spatial flow and the potential dominant does not, then there is no spatial dominance
                                    If SpatialSeries(kind).Flows(jtic) = 1 And SpatialSeries(kkind).Flows(jtic) = 0 Then
                                        SpatialTicMatch = False
                                        Exit For
                                    End If
                                Next jtic
                                'you found a prescription with higher PNV and at least as good of a spatial flow
                                If SpatialTicMatch = True Then
                                    'TrimPoly(kaa).RxIn(jrx) = 0
                                    iRxIn(kaa, jrx) = 0
                                    GlobalTrim = GlobalTrim + 1
                                    Exit For 'don't have to look for another spatially dominant series since we're trimmed!
                                End If

                            End If
                        Next jjrx

                    End If
                Next jrx
            End If 'polyID > 0
        Next jaa
    End Sub


    Private Sub LoadGridInfo(ByVal infile As String)
        'loads the grid input file

        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String

        fnum = FreeFile()

        FileOpen(fnum, infile, OpenMode.Input)
        dummy = LineInput(fnum)
        dummy = LineInput(fnum)
        arr = Split(dummy, ",")
        NGrids = CType(arr(0), Integer)
        MaxStandsInGrid = CType(arr(1), Integer)
        ReDim GridStands(NGrids, MaxStandsInGrid), NStandsInGrid(NGrids)
        dummy = LineInput(fnum)
        For jgrid As Integer = 1 To NGrids
            dummy = LineInput(fnum)
            arr = Split(dummy, ",")
            NStandsInGrid(jgrid) = arr(1)
            For jstand As Integer = 1 To NStandsInGrid(jgrid)
                GridStands(jgrid, jstand) = arr(1 + jstand)
            Next
        Next
        FileClose(fnum)
    End Sub

    Private Sub WriteDPSpaceTrimInfo(ByVal ksubfor As Integer, ByVal iRxIn(,) As Byte, ByVal bFirstDP As Boolean)

        Dim SpatialRxCount As Integer = 0
        Dim PolySpatialRxCount As Integer = 0 'how many 
        Dim NPolyRx As Integer = 0
        Dim CountSpatialPolys As Integer = 0 'used to formulate new StandID
        Dim Mxkind As Integer
        Dim parea As Double
        Dim krx As Integer
        Dim kind As Integer
        Dim kcount As Integer
        Dim SpatialRxPerAA() As Integer
        Dim SpatialRx As Integer
        Dim pDP As Integer 'signals whether you are printing to the DP Input files
        Dim f11 As Integer
        Dim f36 As Integer
        Dim f26 As Integer
        Dim f27 As Integer
        Dim f43 As Integer
        Dim f44 As Integer
        Dim f86 As Integer

        'HOW MANY PRESCRIPTIONS ARE PRINTED OUT IN THIS SUBFOREST?
        ReDim SpatialRxPerAA(AA_NPolys)
        ReDim DPSpaceStandID_LUT(AA_NPolys) 'indexed by the dualplan stand id
        For jaa As Integer = 1 To AA_NPolys
            DualPlanAAPoly_PolyRxSpace(jaa) = 0 'default
            NPolyRx = 0
            For jrx As Integer = 1 To AA_NPres(jaa) 'AA(jaa).PresArray.Length - 1
                krx = AA_PresArray(jaa, jrx) 'krx used to access line in prescription database
                kind = AApres_ISpaceInd(krx)
                If iRxIn(jaa, jrx) = 1 Then 'DualPlanAAPoly(jaa).RxIn(jrx) = 1 Then 'RxTally = RxTally + 1 'And DualPlanModel1Poly(jaa).RxISpace(jrx) = 1 Then RxTally = RxTally + 1
                    'if there is a prescription that's kept that makes ISpace then flag the poly as needing to be passed to DPSpace
                    If SpatialSeries(kind).NNonZero > 0 Then DualPlanAAPoly_PolyRxSpace(jaa) = 1
                    'RxCount = RxCount + 1
                    NPolyRx = NPolyRx + 1
                End If
            Next jrx
            'control for 'blank' aa's with no Rx identified...in this output file, it'll at least get 1 Rx
            'If NPolyRx = 0 Then RxCount = RxCount + 1
            SpatialRxPerAA(jaa) = NPolyRx * DualPlanAAPoly_PolyRxSpace(jaa) 'totals the spatial and non-spatial prescriptions for any poly that has at least 1 spatial Rx that is "in"
            SpatialRxCount = SpatialRxCount + SpatialRxPerAA(jaa)
            NSpatialPolys = NSpatialPolys + DualPlanAAPoly_PolyRxSpace(jaa) '1 used to flag wheter it produces ISpace, can also be used to tally total stands capable of producing ISpace
        Next jaa

        'SPATIAL INDEX BY RX DATA FILE
        f86 = FreeFile()
        FileOpen(f86, FnTrimmeroutBaseA & "_Iter" & IterationNumber & "_PresDataSpat_sf" & CStr(ksubfor) & ".csv", OpenMode.Output)
        PrintLine(f86, "NpresSubFor")
        PrintLine(f86, SpatialRxCount)
        PrintLine(f86, "ISpatSeriesAlt, IBufferSeriesAlt, IEdgeSeriesAlt")

        'RX DATA FILE 
        f36 = FreeFile()
        FileOpen(f36, FnDualPlanOutBaseA & "_Iter" & IterationNumber & "_PresDataNPV_sf" & CStr(ksubfor) & ".csv", OpenMode.Output)
        PrintLine(f36, "NpresSubFor")
        PrintLine(f36, SpatialRxCount)

        'POLY DATA FILE
        f26 = FreeFile()
        FileOpen(f26, FnTrimmeroutBaseA & "_Iter" & IterationNumber & "_PolyData_sf" & CStr(ksubfor) & ".csv", OpenMode.Output)
        PrintLine(f26, "NstandSubfor")
        PrintLine(f26, NSpatialPolys)
        PrintLine(f26, "NAltStand, StandSpaceType, StandArea")
        f27 = FreeFile()
        FileOpen(f27, BaseNameAAnaltAAareaInfilesA & "_sf" & ksubfor & ".csv", OpenMode.Output)
        PrintLine(f27, "kaa, NAlt, Area!")

        'PRESCRIPTION NUMBER LOOK UP TABLE
        f11 = FreeFile()
        FileOpen(f11, FnDualPlanOutBaseA & "_RxLUT." & CStr(ksubfor), OpenMode.Output)
        PrintLine(f11, "StandID, DPSpaceStandID, StandRx, RxNumber")


        'DEBUG TRIMMER DUMP
        f43 = FreeFile()
        FileOpen(f43, FnTrimmeroutBaseA & "_Iter" & IterationNumber & "_TrimmerDump_sf" & CStr(ksubfor) & ".csv", OpenMode.Output)
        PrintLine(f43, "jaa, AA_area, jrx, pDP, DP_StandID, NPolyRx, krx, NPVSelf(jrx)")

        'TRIMMER DUMP FILE WITH INFO ABOUT EVERY RX
        f44 = FreeFile()
        FileOpen(f44, FnTrimmeroutBaseA & "_Iter" & IterationNumber & "_TrimmerDump2_sf" & CStr(ksubfor) & ".csv", OpenMode.Output)
        PrintLine(f44, "jaa, AA_area, jrx, SpatialRx, pDp, NPolyRx, krx, BestValuePerAcre, BestOrdinalRank, RxRank")

        Dim spatprint As Integer
        'RxCount = 0
        CountSpatialPolys = 0
        For jaa As Integer = 1 To AA_NPolys

            Mxkind = 0
            NPolyRx = 0
            kcount = 0
            pDP = DualPlanAAPoly_PolyRxSpace(jaa)
            CountSpatialPolys = CountSpatialPolys + DualPlanAAPoly_PolyRxSpace(jaa)
            DPSpaceStandID_LUT(jaa) = 0
            If DualPlanAAPoly_PolyRxSpace(jaa) = 1 Then
                DPSpaceStandID_LUT(jaa) = CountSpatialPolys
                'PrintLine(12, jaa & "," & DPSpaceStandID_LUT(jaa))
            End If

            For jrx As Integer = 1 To AA_NPres(jaa) 'AA(jaa).PresArray.Length - 1
                krx = AA_PresArray(jaa, jrx)
                If GridRxHighestRank(jaa, jrx) <= AA_NPres(jaa) Or iRxIn(jaa, jrx) Then
                    kcount = kcount + 1
                    kind = AApres_ISpaceInd(krx)
                    SpatialRx = 0
                    If SpatialSeries(kind).NNonZero > 0 Then SpatialRx = 1
                    If jrx = MaxNSRx(jaa) Then 'best non-spatial
                        PrintLine(f44, jaa & "," & AA_area(jaa) & "," & jrx & "," & SpatialRx & "," & pDP & "," & kcount & "," & krx & ",0,1,1")
                    Else
                        PrintLine(f44, jaa & "," & AA_area(jaa) & "," & jrx & "," & SpatialRx & "," & pDP & "," & kcount & "," & krx & "," & Math.Round(GridRxHighestVal(jaa, jrx), 4) & "," & GridRxHighestRank(jaa, jrx) & "," & StandRxRank(jaa, jrx))
                    End If

                End If
                If iRxIn(jaa, jrx) = 1 Then 'DualPlanAAPoly(jaa).RxIn(jrx) = 1 Then 'RxTally = RxTally + 1 'And DualPlanModel1Poly(jaa).RxISpace(jrx) = 1 Then RxTally = RxTally + 1
                    'RxCount = RxCount + 1
                    NPolyRx = NPolyRx + 1

                    kind = AApres_SpaceType(krx)
                    If kind > Mxkind Then Mxkind = kind
                    spatprint = 0
                    If pDP Then
                        PrintLine(f36, DualPlanAAPoly_NPVSelf(jaa, jrx)) 'this includes the value of the 1-way influence zones, as calculated in the DualPlanRxEcon Sub
                        PrintLine(f86, AApres_ISpaceInd(krx) & "," & AApres_QBuffInd(krx) & "," & AApres_EdgeInd(krx))
                    End If
                    PrintLine(f11, jaa & "," & DPSpaceStandID_LUT(jaa) & "," & NPolyRx & "," & krx)
                    PrintLine(f43, jaa & "," & AA_area(jaa) & "," & jrx & "," & pDP & "," & DPSpaceStandID_LUT(jaa) & "," & NPolyRx & "," & krx & "," & Math.Round(DualPlanAAPoly_NPVSelf(jaa, jrx), 2))
                End If
            Next

            If NPolyRx = 0 Then
                NPolyRx = 1
                PrintLine(f11, jaa & ",0,1,0")
            End If
            'count the number of untrimmed prescriptions for this polygon
            DualPlanAAPoly_NRxIn(jaa) = NPolyRx

            parea = AA_area(jaa)
            If parea = 0 Then parea = 999 'Dummy for now #############
            If pDP Then
                PrintLine(f26, NPolyRx & "," & Mxkind & "," & parea)
                PrintLine(f27, DPSpaceStandID_LUT(jaa) & "," & NPolyRx & "," & parea)
            End If
        Next

        FileClose(f26)
        FileClose(f27)
        FileClose(f36)
        FileClose(f86)
        FileClose(f11)
        'dump/debug file
        FileClose(f43)
        FileClose(f44)

        'IF FIRST DP AND USING FIRST DP TRIMS FOR CYCLES, THEN WRITE OUT A LUT
        If bFirstDP = True Then _
            FileCopy(FnDualPlanOutBaseA & "_RxLUT." & CStr(ksubfor), FnDualPlanOutBaseA & "_RxLUTFirst." & CStr(ksubfor))

    End Sub
    Private Sub WriteTrimmedIZoneAndHexInfo(ByVal ksubfor As Integer)
        'This subroutine filters out hexagons that are not in a spatial stand
        'also filters out influence zones that cannot produce core based on their available prescriptions

        'subroutine uses DualPlanSubForests().HexStandsFile and DualPlanSubforests().IZonesFile
        ' Also uses DPSpaceStandID_LUT() to filter hexagons and stands

        Dim NHex As Integer
        Dim SubForNHex As Long = 0
        Dim NSpatialHex As Long 'count the number of hexagons to print out
        Dim minx As Double
        Dim maxx As Double
        Dim miny As Double
        Dim maxy As Double
        Dim MxDistanceBetweenAdjHexCenters As Double
        Dim HexPolys_PolyID() As Integer
        Dim HexPolys_StandID() As Integer
        Dim HexPolys_xVal() As Double
        Dim HexPolys_yVal() As Double
        Dim HexPolys_AdjPolyInd(,) As Integer
        Dim dummy As String
        Dim dummy1 As String
        Dim DPHexID() As Long 'reassigned hexagon ID of hexagon going into DPSpace
        Dim f11 As Integer
        Dim f22 As Integer

        'define the field numbers
        Dim HexIDField As Integer = 0
        Dim StandIDField As Integer = 1
        Dim XField As Integer = 2
        Dim YField As Integer = 3
        Dim StartAdjField As Integer = 4

        Dim tind As Integer = 0
        f11 = FreeFile()
        FileOpen(f11, DualPlanSubForests(ksubfor).HexStandsFile, OpenMode.Input)
        dummy = LineInput(f11)
        dummy = LineInput(f11)
        Atts = Split(dummy, ",")
        NHex = Atts(0)
        MxDistanceBetweenAdjHexCenters = Atts(6)

        ReDim HexPolys_PolyID(NHex)
        ReDim HexPolys_StandID(NHex)
        ReDim HexPolys_xVal(NHex)
        ReDim HexPolys_yVal(NHex)
        ReDim HexPolys_AdjPolyInd(NHex, 5)
        ReDim DPHexID(NHex)
        dummy = LineInput(f11)

        Do Until EOF(f11)
            dummy = Trim(LineInput(f11))
            If Len(dummy) > 0 Then
                Atts = Split(dummy, ",")
                'test to see if this hexagon is in a defined stand
                If DPSpaceStandID_LUT(Atts(StandIDField)) > 0 Then 'means the stand was sent to dpspace
                    tind = tind + 1
                    DPHexID(Atts(HexIDField)) = tind
                    HexPolys_PolyID(tind) = tind 'Atts(HexIDField)
                    HexPolys_StandID(tind) = DPSpaceStandID_LUT(Atts(StandIDField)) 'the mapped stand id
                    HexPolys_xVal(tind) = Atts(XField)
                    HexPolys_yVal(tind) = Atts(YField)
                    'Poly indexes of adjacent polygons...
                    For j As Integer = 0 To 5
                        HexPolys_AdjPolyInd(tind, j) = Atts(j + StartAdjField) 'this will load the original hex id - have to print out the mapped hex id
                    Next

                    'determine min and max of that which is left in the forest
                    If tind = 1 Then
                        minx = HexPolys_xVal(tind)
                        maxx = HexPolys_xVal(tind)
                        miny = HexPolys_yVal(tind)
                        maxy = HexPolys_yVal(tind)
                    Else
                        If HexPolys_xVal(tind) < minx Then minx = HexPolys_xVal(tind)
                        If HexPolys_xVal(tind) > maxx Then maxx = HexPolys_xVal(tind)
                        If HexPolys_yVal(tind) < miny Then miny = HexPolys_yVal(tind)
                        If HexPolys_yVal(tind) > maxy Then maxy = HexPolys_yVal(tind)
                    End If
                End If
            End If
        Loop
        NSpatialHex = tind 'tally how many hexagons made it through
        FileClose(f11)

        f22 = FreeFile()
        FileOpen(f22, BaseNameHexDataOutfilesA & "_Hex_sf" & ksubfor & ".csv", OpenMode.Output)
        PrintLine(f22, "SubForNHex,SubForNAA,MnXlocS,MxXlocS,MnYlocS,MxYlocS,MxDistanceBetweenAdjHexCenters")
        PrintLine(f22, NSpatialHex & "," & NSpatialPolys & "," & minx & "," & maxx & "," & miny & "," & maxy & "," & MxDistanceBetweenAdjHexCenters)
        PrintLine(f22, "jHex,HexSubForAA,HexXloc,HexYloc,AdjHx(0),AdjHx(1),AdjHx(2),AdjHx(3),AdjHx(4),AdjHx(5)")
        For jpoly As Integer = 1 To NSpatialHex

            dummy1 = ""
            For jside As Integer = 0 To 5
                dummy1 = dummy1 & "," & DPHexID(HexPolys_AdjPolyInd(jpoly, jside))
            Next

            PrintLine(f22, HexPolys_PolyID(jpoly) & "," & HexPolys_StandID(jpoly) & "," & HexPolys_xVal(jpoly) & "," & _
                     HexPolys_yVal(jpoly) & dummy1)

        Next
        FileClose(f22)

        'Now process the influence zone file
        'ASSUMES THAT THE INFLUENCE ZONE FILE HAS ALREADY BEEN LOADED

        Dim NSpatialZones As Integer = 0 'counts how many influence zones made it through the trimmer
        Dim KeepZone() As Byte 'whether to keep the zone for printing or not - dimensioned by the number of IZones
        Dim dumstring As String
        Dim pZoneCount As Integer = 0 'counting the izones you are printing
        Dim TotalZoneRxCombos As Long 'prescription dimension of the influence zone
        Dim kaa As Integer

        ReDim KeepZone(NZoneSubFor)

        For jzone As Integer = 1 To NZoneSubFor
            KeepZone(jzone) = 0
            'default is initially set to whether any space types keep the zone, determined in TrimEcon2
            For jspace As Integer = 1 To NSpaceType
                If KeepZoneSpace(jspace, jzone) > 0 Then
                    KeepZone(jzone) = 1
                    Exit For
                End If
            Next

            If Zonedim(jzone) > 1 And KeepZone(jzone) = 1 Then 'filter out 1-way influence zones. Their value is already included in the stand's NPV anyway
                'go through the stands in the zone -if any one is NOT passed to the DP, the zone should be omitted
                'now check for whether the total number of prescription combos in the zone is greater than the defined threshhold
                TotalZoneRxCombos = 1 'reset
                For jdim As Integer = 1 To Zonedim(jzone)
                    kaa = ZoneAAlist(jzone, jdim)
                    If DPSpaceStandID_LUT(kaa) = 0 Then KeepZone(jzone) = 0

                    'TEST FOR TOTAL NUMBER OF PRESCRIPTIONS TO BE GREATER THAN A FILTER
                    TotalZoneRxCombos = TotalZoneRxCombos * DualPlanAAPoly_NRxIn(kaa)
                    If TotalZoneRxCombos > ZoneRxFilter And ZoneRxFilter > 0 Then
                        KeepZone(jzone) = 0
                        Exit For
                    End If
                Next
                If KeepZone(jzone) = 1 Then NSpatialZones = NSpatialZones + 1
            Else
                KeepZone(jzone) = 0
            End If
        Next jzone

        f22 = FreeFile()
        FileOpen(f22, BaseNameHexDataOutfilesA & "_Zones_sf" & ksubfor & ".csv", OpenMode.Output) 'for DPForm format
        PrintLine(f22, "NZoneSubFor, MxZoneDimInList")
        PrintLine(f22, NSpatialZones & "," & MXZoneDim)
        Dim dprint As String = ""
        dprint = "Zone_#,Dimension"
        For jzone As Integer = 1 To MXZoneDim
            dprint = dprint & ",AA" & jzone
        Next
        For jzone As Integer = 1 To MXZoneDim
            dprint = dprint & ",AA" & jzone & "area"
        Next
        PrintLine(f22, dprint)

        pZoneCount = 0
        For jzone As Integer = 1 To NZoneSubFor
            If KeepZone(jzone) = 1 Then
                pZoneCount = pZoneCount + 1
                dumstring = ""

                'dpspace stand IDs
                For jdim As Integer = 1 To MXZoneDim
                    dumstring = dumstring & "," & DPSpaceStandID_LUT(ZoneAAlist(jzone, jdim))
                Next jdim

                'areas in each stand within the izone
                For jdim As Integer = 1 To MXZoneDim
                    dumstring = dumstring & "," & ZoneAAlistAreaS(jzone, jdim)
                Next jdim

                PrintLine(f22, pZoneCount & "," & Zonedim(jzone) & dumstring)
            End If
        Next
        FileClose(f22)


    End Sub

    Private Function TallySpacePotential(ByRef btAASpacePotential(,,) As Byte, ByVal iRxIn(,) As Byte, ByVal kaa As Integer, ByVal SpatPotIndex As Integer, ByVal kspacetype As Integer) As Integer '(ByVal trimpoly() As TrimPolyData, ByVal kaa As Integer, ByVal kspacetype As Integer) As Integer
        'populates the periods of when the non-trimmed prescriptions of a stand can produce ISpace
        'also returns the number of periods of potential ISpace creation
        Dim NRx As Integer
        Dim krx As Integer
        Dim kind As Integer
        Dim kspat As Integer
        Dim MaxTicAlign As Integer
        Dim TicValue() As Integer
        ReDim TicValue(MxTic)
        For jtic As Integer = 0 To MxTic
            TicValue(jtic) = 0
        Next

        TallySpacePotential = 0

        NRx = AA_NPres(kaa)
        For jrx As Integer = 1 To NRx
            krx = AA_PresArray(kaa, jrx) 'krx used to access line in prescription database
            kind = AApres_ISpaceInd(krx)
            kspat = AApres_SpaceType(krx)
            MaxTicAlign = SpatialSeries(kind).NNonZero

            'has the rx been trimmed (want the non-trimmed) and is it a <potential> spatial rx?
            'If trimpoly(kaa).RxIn(jrx) = 1 And MaxTicAlign > 0 And kspat = kspacetype Then ' FIGURE THE MAX VALUE FOR EACH RX And DlPlM1Poly(kaa).NPVSelf(jrx) < DlPlM1Poly(kaa).NPValLbnd Then 'only need to evaluate the Rx if it's less than the LBnd
            If iRxIn(kaa, jrx) = 1 And MaxTicAlign > 0 And kspat = kspacetype Then
                For jtic As Integer = SpatialSeries(kind).First1Tic To SpatialSeries(kind).Last1tic
                    TicValue(jtic) = TicValue(jtic) + SpatialSeries(kind).Flows(jtic)
                Next
            End If
        Next jrx

        'now set the values
        For jtic As Integer = 1 To MxTic
            btAASpacePotential(SpatPotIndex, kspacetype, jtic) = 0
            If TicValue(jtic) > 0 Then
                btAASpacePotential(SpatPotIndex, kspacetype, jtic) = 1 'by AA, SpaceType, Tic
                TallySpacePotential = TallySpacePotential + 1
            End If
        Next

    End Function

    Private Function DetermineKTrialSet(ByVal CycleNum As Integer) As Integer
        DetermineKTrialSet = 0
        For ktrial As Integer = 1 To NTrialSets
            If CycleNum >= TrialSetBegTrial(ktrial) And CycleNum <= TrialSetEndTrial(ktrial) Then
                DetermineKTrialSet = ktrial
                Exit Function
            End If
        Next
    End Function


End Class


