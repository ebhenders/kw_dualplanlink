Option Strict Off
Option Explicit On
Friend Class MainForm
	Inherits System.Windows.Forms.Form
	
	Private Sub MainForm_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		'TxtIter.Text = IterationNumber&
		'Txtmore.Text = IterationNumber& + IterHowmanyMore& - HowMany
		'MainForm.TxtIter.Visible = False
		'MainForm.Txtmore.Visible = False
		'MainForm.Lbliter.Visible = False
		'MainForm.Lblmore.Visible = False
		ComboBoxIteration.Visible = False
		OK.Visible = False

        'See if your link file variable is set...

        Dim returnValue As IDictionary
        Dim bEnvSet As Boolean = False

        returnValue = Environment.GetEnvironmentVariables
        Dim de As DictionaryEntry
        For Each de In returnValue
            If de.Key = "LinkFile" Then
                bEnvSet = True
                Exit For
            End If
        Next

        If bEnvSet = True Then
            LinkFileA = Environment.GetEnvironmentVariable("LinkFile")
        Else
            MsgBox("Please Set the Environmental Variable 'LinkFile' to the full path and filename for the link file for this model run")
        End If
		
	End Sub
	
	Private Sub Iteration_Change()
		
	End Sub
	
	Public Sub mnuAdjParameters_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuAdjParameters.Click
		'Edit1.Show 1
        'ShadPrCoef.ShowDialog()
	End Sub
	
	Public Sub mnuCondSets_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuCondSets.Click
        'ConditionSet.ShowDialog()
	End Sub
	
	Public Sub mnuCondTypes_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuCondTypes.Click
        'CondType.ShowDialog()
	End Sub
	
	Private Sub mnuIterSteps_Click()
		IterSteps.ShowDialog()
	End Sub
	
	Public Sub mnuReadSpread_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuReadSpread.Click
		Call ReadSpread()
	End Sub
	
	Public Sub mnuRgLand_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuRgLand.Click
        'LandRg.ShowDialog()
	End Sub
	
	Public Sub mnuMSets_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMSets.Click
        'MKTset.ShowDialog()
	End Sub
	Public Sub mnuQuit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuQuit.Click
		'Close #6
		FileClose(57)
		End
	End Sub
	
	Public Sub mnuRunContinue_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuRunContinue.Click
        IterSteps.ShowDialog()
        Iterate()

	End Sub
	
	Public Sub mnuRunFloat_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuRunFloat.Click
		IterHowmanyMore = 1
		IterMethod = "Float"
        Iterate()
	End Sub
	
	Public Sub mnuRunShape_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuRunShape.Click
		IterHowmanyMore = 1
        IterMethod = "Shape"
        Iterate()
	End Sub
	Public Sub mnuEditPr_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuEditPr.Click
        'EditPR.ShowDialog()
	End Sub
	
	Public Sub mnuMtypes_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuMtypes.Click
        'MType.ShowDialog()
	End Sub
	
	Public Sub mnuRunSmooth_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuRunSmooth.Click
		IterHowmanyMore = 1
		IterMethod = "Smooth"
        Iterate()
	End Sub
	
	Public Sub mnuWriteIn_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuWriteIn.Click
		'ttt = 3
		Label1.Visible = True
		Label1.Text = "Name of input file to write"
        CommonDialog1Open.FileName = FnDualPlanOutBaseA & "NewIn." & LTrim(Str(IterationNumber))
		CommonDialog1Open.Title = " Name of New Input File"
		CommonDialog1Open.ShowDialog()
		Label1.Visible = False

        Call WriteNewInput(CommonDialog1Open.FileName)

	End Sub
	Public Sub mnuWriteForestwide_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuWriteForestwide.Click
		'ttt = 3
		'Label1.Visible = True
		'Label1.Caption = "BaseName of Forest-wide output files to write"
        'CommonDialog1.FileName = FnDualPlanOutBaseA$ + "ForestWideOut." + LTrim$(Str$(IterationNumber&))
        'CommonDialog1.DialogTitle = " Name of New output File"
        'CommonDialog1.ShowOpen
        'Label1.Visible = False
        'Open CommonDialog1.FileName For Output As #6
        Call WriteReport()
        'Close #6
    End Sub
    Public Sub mnuWritePolySched_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuWritePolySched.Click
        'Dim file36 As String
        'Label1.Visible = True
        'Label1.Text = "Name of output file to write"
        'CommonDialog1Open.FileName = FnDualPlanOutBaseA & "PolyStats" & LTrim(Str(IterationNumber)) & ".csv"
        'CommonDialog1Open.Title = " Name of New output File"
        'CommonDialog1Open.ShowDialog()
        'Label1.Visible = False
        'FileOpen(26, CommonDialog1Open.FileName, OpenMode.Output)
        'file36 = FnDualPlanOutBaseA & "ConvSummary" & LTrim(Str(IterationNumber)) & ".csv"
        'FileOpen(36, file36, OpenMode.Output)
        '      ''########### Call WriteAAPres(aa, aapres, aaflow)
        'WriteLine(57, "countWriteAApres", CountDPused)
        'CountDPused = 0
    End Sub

    Public Sub mnuWriteSpread_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuWriteSpread.Click
        Dim jiter As Object
        Dim iterhigh As Object
        Dim iterlow As Object
        Dim kiterspread As Short
        ComboBoxIteration.Visible = True
        OK.Visible = True
        iterlow = IterationNumber - NIterToSave + 1
        iterhigh = IterationNumber
        If iterhigh <= 0 Then iterhigh = 1
        If iterlow < 1 Then iterlow = 1
        For jiter = iterlow To iterhigh
            ComboBoxIteration.Items.Add(jiter)
        Next jiter
        Label1.Visible = True
        Label1.Text = "Choose Iteration Number to Write to Spreadsheet"
        kEventRun = ""
        Do Until kEventRun <> ""
            System.Windows.Forms.Application.DoEvents()
        Loop
        Label1.Text = ""
        Label1.Visible = False
        kiterspread = CShort(ComboBoxIteration.Text)
        ComboBoxIteration.Items.Clear()
        ComboBoxIteration.Visible = False
        OK.Visible = False

        Label1.Visible = True
        Label1.Text = "Name of spreadsheet file to write"
        CommonDialog1Open.FileName = FnDualPlanOutBaseA & "SpreadOut" & LTrim(Str(kiterspread)) & ".csv"
        CommonDialog1Open.Title = " Name of New output File"
        CommonDialog1Open.ShowDialog()
        Label1.Visible = False
        FileOpen(28, CommonDialog1Open.FileName, OpenMode.Output)
        Call WriteSpread(kiterspread)
    End Sub
	
	Private Sub OK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles OK.Click
		kEventRun = ComboBoxIteration.Text
	End Sub


    Private Sub LoadMainInputFileToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoadMainInputFileToolStripMenuItem.Click

        Dim OpenFileDialog1 As New OpenFileDialog
        'Dim TrimFile As String
        IterMethod = "Input"
        With OpenFileDialog1
            .Title = "Select the main DualPlan input file"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        DualPlan_Space.ReadLinkfile()
        DualPlanInputFileName = OpenFileDialog1.FileName
        DualPlan_Space.LoadDualPlanInput(OpenFileDialog1.FileName)
        'Default: set the output folder to that which the input file is in:
        'FnDualPlanOutBaseA = DirName(OpenFileDialog1.FileName)

    End Sub



    Private Sub WritePolygonSpatialFlowsToFileToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WritePolygonSpatialFlowsToFileToolStripMenuItem.Click
        '##############THIS SUBROUTINE WOULD NEED TO LOAD THE SUBFOREST INFORMATION IF YOU'RE DOING IT 'COLD' - 
        'WON'T WORK RIGHT NOW, BECAUSE THERE NO SUBFOREST INFORMATION LOADED

        Dim file36 As String
        'Label1.Visible = True
        'Label1.Text = "Name of output file to write"
        'CommonDialog1Open.FileName = FnDualPlanOutBaseA & "PolyStats" & LTrim(Str(IterationNumber)) & ".csv"
        'CommonDialog1Open.Title = " Name of New output File"
        'CommonDialog1Open.ShowDialog()
        'Label1.Visible = False
        'FileOpen(26, CommonDialog1Open.FileName, OpenMode.Output)
        For kaaset = 1 To AAnDataSet
            'If IterationNumber > 0 Then
            LoadPolySolFile(DualPlanSubForests(kaaset).PolySchedFile)
            'Else
            '    LoadPolySolFile(DualPlanSubForests(kaaset).PolySchedFileLoad)
            'End If
            file36 = FnDualPlanOutBaseA & "ConvSummary" & LTrim(Str(IterationNumber)) & ".csv"
            'FileOpen(36, file36, OpenMode.Output)
            Call WriteSpatialOutput(file36, kaaset, PolySolRx_ChosenRxInd)
            'WriteLine(57, "countWriteAApres", CountDPused)
            'CountDPused = 0
        Next kaaset
    End Sub


    Private Sub CreateGridForTrimmerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CreateGridForTrimmerToolStripMenuItem.Click
        Dim gridtrim As New GridForTrimmer
        gridtrim.ShowDialog()
        gridtrim = Nothing
    End Sub

End Class