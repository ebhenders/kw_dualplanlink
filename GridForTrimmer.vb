Friend Class GridForTrimmer
    Inherits System.Windows.Forms.Form

    Dim HexAcres As Single
    Dim GridSize As Single
    Dim MinFeasGrid As Single
    Dim PctGridOverlap As Single
    Dim PctStandInGrid As Single
    Dim InputHexFile As String
    Dim InputZoneFile As String
    Dim OFBaseName As String

    Dim bFullStandInGrid As Boolean 'only include if the full stand is in the grid
    Dim bPartStandInGrid As Boolean 'include in a grid if any part is within
    Dim bPctStandInGrid As Boolean 'include a stand in the grid if a certain percentage of it is in the grid
    Dim bStandBasedGrids As Boolean 'if you are using stand-based grids, all influence zones from all stands are included
    Dim bCircle As Boolean 'whether you are making a circle grid or not
    Dim bRxFeas As Boolean ' whehter you are loading prescription information to consider feasible grids

    Dim NStands As Integer
    Dim NHex As Long
    Dim HexX() As Double
    Dim HexY() As Double
    Dim MinX As Double
    Dim MinY As Double
    Dim MaxX As Double
    Dim MaxY As Double
    Dim StandSize() As Single
    Dim StandCentroidX() As Double
    Dim StandCentroidY() As Double
    Dim HexStandID() As Integer

    Dim GridSideXYUnits As Double 'the side length of the grid expressed in x/y units
    Dim GridAreaXYUnits As Double 'the area of the grid in x/y units

    'IZone stuff for stand-based grid
    Dim gMXZoneDim As Integer
    Dim gNZoneSubFor As Integer
    Dim gMXZonesForAA As Integer 'max zones for any one AA
    Dim gZoneDim() As Integer
    Dim gZoneAAList(,) As Integer
    Dim gStandIZones(,) As Integer 'by stand, max zones for any stand
    Dim gNIZonesStand() As Integer
    Dim gZoneArea() As Single 'by NZonesubfor - records the area of the IZone

    'Prescription information
    Dim PolyRxFile As String     'polyrxfile is the index of available polys per stand
    Dim RxSpatialIndFile As String 'tells the spatial index of each prescription
    Dim SpatialFlowFile As String 'tells the spatial flow sof each prescription
    Dim RxSpatialInd() As Integer
    Dim NSpatialInd As Integer
    Dim NSpatialFlows() As Integer 'number of spatial flows for the spatial index
    Dim SpatialFlows(,) As Integer 'flow value by period
    Dim PolyRx(,) As Long 'by nstands, maxrx
    Dim NPolyRx() As Integer 'by nstands
    Dim MaxRxPerPoly As Integer 'the max number of prescriptions per any one polygon
    Dim NRx As Integer 'the maximum prescription number index - also the MaxPresIndex
    Dim bSpatialRx() As Boolean 'whether a stand has a spatial Rx available or not
    Dim NPer As Integer 'number of periods in the RxCondFile
    'Dim RxCondPer(,) As Integer 'the condition/covertype of the prescrption in each time period - by NRx, NPer
    'Dim SpatialSeriesNFirstTic() As Integer 'counts the number of 1st appearances of KW in the spatial series
    Dim SpatialSeriesFirstTic(,) As Integer 'the tic of first appearance of Kw

    'other feasible grid stuff
    Dim OthFeasGridCount As Integer
    Dim OthFeasGridStands(,) As Integer 'catch inferior feasible patches in a grid > min patch size but not distinct in the grid
    Dim OthFeasNStandsInGrid() As Integer 'associated with the CatchGrid
    Dim UseOthFeasGrid() As Integer
    Dim OrigStandIncludedAnyGrid() As Byte
    Dim MaxStandsInGrid As Integer 'actual max stands in a grid


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim OpenFileDialog1 As New OpenFileDialog


        With OpenFileDialog1
            .Title = "Select the Polygon File to Process"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        tbHexFile.Text = OpenFileDialog1.FileName

    End Sub

    Private Sub GoButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim dummyprint As String
        Dim gridshape As String
        HexAcres = CType(tbHexAcres.Text, Single)
        GridSize = CType(tbGridSize.Text, Single)
        MinFeasGrid = CType(tbMinFeasGrid.Text, Single)
        PctGridOverlap = CType(tbPctGridOverlap.Text, Single) / 100
        NPer = CType(tbNPer.Text, Integer)
        InputHexFile = Trim(tbHexFile.Text)
        bFullStandInGrid = rbFullStandInGrid.Checked
        bPartStandInGrid = rbPartStandInGrid.Checked
        bPctStandInGrid = rbMajorityStandInGrid.Checked
        bRxFeas = cbRxFeas.Checked
        'NPer = CType(tbNPer.Text, Integer)
        If bPctStandInGrid = True Then PctStandInGrid = CType(tbPctStandInGrid.Text, Single) / 100
        bStandBasedGrids = rbStandBasedGrids.Checked
        dummyprint = "not defined"
        If bFullStandInGrid = True Then dummyprint = "Full Stand Inclusion"
        If bPartStandInGrid = True Then dummyprint = "Partial Stand Inclusion"
        If bPctStandInGrid = True Then dummyprint = "Percent Stand Inclusion: " & tbPctStandInGrid.Text
        If bStandBasedGrids = True Then dummyprint = "Stand Based Grids"
        bCircle = cbCircleGrid.Checked
        gridshape = "Square"
        If bCircle = True Then gridshape = "Circular"

        'If bStandBasedGrids = True Then
        'need to open the influence zone file
        Dim OpenFileDialog1 As New OpenFileDialog

        With OpenFileDialog1
            .Title = "Select the Influence Zone file to read"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        InputZoneFile = OpenFileDialog1.FileName
        'End If

        If bRxFeas = True Then
            With OpenFileDialog1
                .Title = "Select PolyRx File to read"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With
            PolyRxFile = OpenFileDialog1.FileName

            With OpenFileDialog1
                .Title = "Select File for Spatial Index of each Rx"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With
            RxSpatialIndFile = OpenFileDialog1.FileName

            With OpenFileDialog1
                .Title = "Select Spatial Flow file"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With
            SpatialFlowFile = OpenFileDialog1.FileName
        End If

        Dim SaveFileDialog1 As New SaveFileDialog

        With SaveFileDialog1
            .Title = "Save the Grid File - choose a name base"
            .FileName = FnDualPlanOutBaseA & "GridFile"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        OFBaseName = SaveFileDialog1.FileName

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, OFBaseName & "_gridparms.txt", OpenMode.Output)
        PrintLine(fnum, "Grid parameters for " & OFBaseName & ".txt")
        PrintLine(fnum, "Hexagon size (acres):", TAB(35), HexAcres)
        PrintLine(fnum, "Grid size (acres):", TAB(35), GridSize)
        PrintLine(fnum, "Min Feasible Grid:", TAB(35), MinFeasGrid)
        PrintLine(fnum, "Percentage Grid Overlap:", TAB(35), PctGridOverlap)
        PrintLine(fnum, "Consider Rx Info For Grids:", TAB(35), bRxFeas)
        If bRxFeas Then
            PrintLine(fnum, "Poly Prescription File:", TAB(35), PolyRxFile)
            PrintLine(fnum, "Rx Spatial Index File", TAB(35), RxSpatialIndFile)
            PrintLine(fnum, "Spatial Flow File", TAB(35), SpatialFlowFile)
            PrintLine(fnum, "Rx Periods to Evaluate", TAB(35), NPer)
        End If
        'PrintLine(fnum, "Number Time Periods for Rx:", TAB(35), NPer)
        PrintLine(fnum, "Grid Design for Stands:", TAB(35), dummyprint)
        PrintLine(fnum, "Grid Shape:", TAB(35), gridshape)
        FileClose(fnum)

        Readfile(InputHexFile)
        LoadIZones(InputZoneFile)
        'If bStandBasedGrids = True Then 
        If bRxFeas = True Then LoadRxInfo()
        If bStandBasedGrids = False Then CreateGrid()
        If bStandBasedGrids = True Then StandBasedGrid()

        MsgBox("Done Creating Grids")

    End Sub

    Private Sub Readfile(ByVal filename As String)
        'sub reads the hexagon information to get stand size, min/max x/y and other information about the hexagons in the forest
        Dim MaxStandID As Integer = -99
        Dim StandID As Integer
        Dim HexID As Integer
        Dim AdjFound As Boolean = False
        Dim Hex1 As Integer
        Dim Hex2 As Integer
        Dim HexAreaUnits As Double
        Dim DistBetweenCenters As Double
        Dim StandMaxX() As Double
        Dim StandMinX() As Double
        Dim StandMaxY() As Double
        Dim StandMinY() As Double

        MinY = 99999999
        MinX = 99999999
        MaxY = -99999999
        MaxX = -99999999

        Dim fnum As Integer = FreeFile()
        Dim dummy As String
        Dim arr() As String
        FileOpen(fnum, filename, OpenMode.Input)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If Trim(dummy).Length = 0 Then Exit Do
            arr = Split(dummy, " ")
            If arr(1) > MaxStandID Then MaxStandID = arr(1)
            NHex = NHex + 1
            If arr(15) < MinX Then MinX = arr(15)
            If arr(15) > MaxX Then MaxX = arr(15)
            If arr(16) < MinY Then MinY = arr(16)
            If arr(16) > MaxY Then MaxY = arr(16)
            If AdjFound = False Then
                For jarr As Integer = 3 To 14
                    If arr(jarr) > 0 Then 'found an adjacent hex id
                        Hex1 = arr(0)
                        Hex2 = arr(jarr)
                        AdjFound = True
                        Exit For
                    End If
                Next
            End If
        Loop
        FileClose(fnum)
        NStands = MaxStandID
        ReDim HexX(NHex), HexY(NHex), HexStandID(NHex), StandSize(NStands), StandMinX(NStands), StandMaxX(NStands), StandMinY(NStands), StandMaxY(NStands), StandCentroidX(NStands), StandCentroidY(NStands)
        'set initial parameters
        For jstand As Integer = 1 To NStands
            StandMinX(jstand) = 99999999
            StandMinY(jstand) = 99999999
            StandMaxX(jstand) = -99999999
            StandMaxY(jstand) = -99999999
        Next
        'read file again to squeeze out x/y coords, stand sizes, and stand ids
        FileOpen(fnum, filename, OpenMode.Input)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If Trim(dummy).Length = 0 Then Exit Do
            arr = Split(dummy, " ")
            HexID = Trim(arr(0))
            StandID = Trim(arr(1))
            StandSize(StandID) = StandSize(StandID) + HexAcres
            HexX(HexID) = Trim(arr(15))
            If HexX(HexID) > StandMaxX(StandID) Then StandMaxX(StandID) = HexX(HexID)
            If HexX(HexID) < StandMinX(StandID) Then StandMinX(StandID) = HexX(HexID)
            HexY(HexID) = Trim(arr(16))
            If HexY(HexID) > StandMaxY(StandID) Then StandMaxY(StandID) = HexY(HexID)
            If HexY(HexID) < StandMinY(StandID) Then StandMinY(StandID) = HexY(HexID)
            HexStandID(HexID) = StandID
        Loop

        'find the centroids of the stand - point need not be in the stand polygon and that's OK
        For jstand As Integer = 1 To NStands
            StandCentroidX(jstand) = StandMinX(jstand) + (StandMaxX(jstand) - StandMinX(jstand)) / 2
            StandCentroidY(jstand) = StandMinY(jstand) + (StandMaxY(jstand) - StandMinY(jstand)) / 2
        Next

        'find the size of the side of a grid expressed in x/y units; assume x and y are the same units in each direction
        DistBetweenCenters = Math.Sqrt((HexX(Hex1) - HexX(Hex2)) ^ 2 + (HexY(Hex1) - HexY(Hex2)) ^ 2)
        HexAreaUnits = DistBetweenCenters * DistBetweenCenters * Math.Sqrt(3) / 2
        GridSideXYUnits = Math.Sqrt(GridSize * HexAreaUnits / HexAcres)
        GridAreaXYUnits = GridSideXYUnits ^ 2
        FileClose(fnum)
    End Sub

    Private Sub CreateGrid()
        'sub mocks up the x/y coords of the parent grid and puts in the appropriate stands

        Dim fnum As Integer

        Dim BaseX As Double 'anchor point for grid you are working on
        Dim BaseY As Double 'anchor point Y for grid you are working on
        Dim BigX As Double 'extent in X direction of grid you are working on
        Dim BigY As Double 'extent in Y direction of the grid you are working on

        Dim NGridsYDir As Integer
        Dim NGridsXDir As Integer
        Dim GridShift As Double 'number of units the grid shifts with each movement
        Dim GridHeight As Double ' length of the side of the grid in x/y units for square, or the diameter for circle
        Dim NGrids As Integer
        Dim OrigNGrids As Integer
        Dim NStandBasedGrids As Integer
        Dim kgrid As Integer
        Dim GridBaseX() As Double
        Dim GridBaseY() As Double
        Dim UseGrid() As Byte 'set of switches as to whether to use the grid or not
        Dim ParentGrid() As Integer 'index of the printed grid
        Dim bDuplicate As Boolean
        'Dim bAlreadyIn As Boolean
        Dim nmatches As Integer
        Dim GridStands(,) As Integer 'array of the stands in each grid - dimmed by XRows, YRows and a guess at the max number of stands that can be in any given grid
        Dim OrigGridStands(,) As Integer
        Dim NStandsInGrid() As Integer
     
        Dim StandBin() As Integer 'collection of stands to dump into GridStands. Can be ordered before dumped into GridStands
        Dim InfFeasGridStandBin() As Integer
        Dim StandSizeInGrid() As Single 'the area of the portion of the stand within the grid
        Dim InfFeasGridStandSizeInGrid() As Single
        Dim StandsInGrid() As Integer 'the stands in this actual grid
        Dim InfFeasGridStandsInGrid() As Integer
        Dim StandCount As Integer
        Dim StandIncluded() As Byte 'whether or not a particular stand has been included in at least 1 grid

        Dim testid As Integer 'test stand id of the hexagon
        Dim tstand As Integer
        Dim kstand As Integer
        Dim kzone As Integer
        Dim AreaStandsIncludedInGrid As Single 'area of the stands included in the grid

        Dim GuessMaxStands As Integer
        Dim GuessInfFeasGridCount As Integer


        Dim GridCounter As Integer
        Dim NewGridCounter As Integer
        Dim dummy As String

        'circle metrics
        Dim r As Double 'radius of the circle
        Dim theta As Double 'angle at the center that creates a segment with area K
        Dim K As Double 'the area of the segment - equal to half the overlap
        Dim centerdist As Double 'distance from the center of the circle to the center of the hex

        'First, create all potential grids
        BaseX = MinX
        BaseY = MinY

        If bCircle = False Then
            GridShift = GridSideXYUnits * (1 - PctGridOverlap)
            GridHeight = GridSideXYUnits
        Else
            'determine the radius of the circle with the area you have
            r = Math.Sqrt(GridAreaXYUnits / Math.PI)
            K = GridAreaXYUnits * PctGridOverlap / 2 'the are in a circle segment is 1/2 the full overlap
            'use formula (2K/r^2) = theta - sin(theta) to solve for theta with iteration
            theta = SolveTheta((2 * K) / (r ^ 2), 0.00001)
            GridShift = 2 * r * Math.Cos(theta / 2) 'just using the formula, man! GridSizeXYUnits * (1 - PctGridOverlap)
            GridHeight = 2 * r 'the diameter
        End If

        'find how many grids tall this is  
        'begin working by adding to the y until you get past max y
        Do
            If BaseY + GridHeight < MaxY Then NGridsYDir = NGridsYDir + 1
            If BaseY + GridHeight >= MaxY Then
                NGridsYDir = NGridsYDir + 1
                Exit Do
            End If
            BaseY = BaseY + GridShift
        Loop
        'find how many grids wide
        Do
            If BaseX + GridHeight < MaxX Then NGridsXDir = NGridsXDir + 1
            If BaseX + GridHeight >= MaxX Then
                NGridsXDir = NGridsXDir + 1
                Exit Do
            End If
            BaseX = BaseX + GridShift
        Loop

        'populate the x/y coords of each grid center
        NGrids = NGridsXDir * NGridsYDir
        ReDim GridBaseX(NGrids), GridBaseY(NGrids)
        kgrid = 0
        BaseX = MinX  'start
        BaseY = MinY
        For jx As Integer = 1 To NGridsXDir
            For jy As Integer = 1 To NGridsYDir
                kgrid = kgrid + 1
                GridBaseX(kgrid) = BaseX
                GridBaseY(kgrid) = BaseY
                BaseY = BaseY + GridShift 'do this after, since first basey is miny
            Next
            BaseY = MinY
            BaseX = BaseX + GridShift 'do this after, since first basex is minx
        Next

        'DETERMINE WHICH STANDS ARE IN THE GRID
        GuessMaxStands = CType(Math.Round(GridSize / HexAcres, 0), Integer) + 10
        MaxStandsInGrid = 0
        ReDim UseGrid(NGrids), OrigGridStands(NGrids, GuessMaxStands), NStandsInGrid(NGrids), OrigStandIncludedAnyGrid(NStands) ', StandInGridAlready(NStands)

        'enable counting of feasible inferior grids
        Dim iMinFG As Integer = MinFeasGrid
        If iMinFG < HexAcres Then
            iMinFG = Math.Round(HexAcres, 0) + 1
        End If
        GuessInfFeasGridCount = NGrids '* Math.Round(GridSize / iMinFG, 0) 'number of grids * max number of unique patches in each grid
        ReDim OthFeasGridStands(GuessInfFeasGridCount, GuessMaxStands), OthFeasNStandsInGrid(GuessInfFeasGridCount), UseOthFeasGrid(GuessInfFeasGridCount)
        OthFeasGridCount = 0

        'scroll through all the grids - bottom to top, then left to right
        For jgrid As Integer = 1 To NGrids
            'If jgrid = 1022 Then Stop
            BaseX = GridBaseX(jgrid)
            BaseY = GridBaseY(jgrid)
            BigX = BaseX + GridHeight
            BigY = BaseY + GridHeight
            AreaStandsIncludedInGrid = 0
            ReDim StandBin(NStands), StandSizeInGrid(NStands), InfFeasGridStandSizeInGrid(NStands), InfFeasGridStandBin(NStands) ', StandInGridAlready(NStands)
            'go through all hexagons and tally the areas of each stand in the grid

            For jhex As Integer = 1 To NHex
                If bCircle = False Then 'squares use Big and Base
                    If HexY(jhex) >= BaseY And HexY(jhex) <= BigY And HexX(jhex) >= BaseX And HexX(jhex) <= BigX Then
                        'hexagon is in this grid
                        testid = HexStandID(jhex)
                        StandSizeInGrid(testid) = StandSizeInGrid(testid) + HexAcres
                        InfFeasGridStandSizeInGrid(testid) = StandSizeInGrid(testid)
                    End If
                ElseIf bCircle = True Then 'circles only use Base
                    'have to determine whether the distance from center of circle to center of hex <= radius of the circle
                    centerdist = Math.Sqrt((BaseX - HexX(jhex)) ^ 2 + (BaseY - HexY(jhex)) ^ 2)
                    If centerdist <= r Then
                        'hexagon is in this grid
                        testid = HexStandID(jhex)
                        StandSizeInGrid(testid) = StandSizeInGrid(testid) + HexAcres
                        InfFeasGridStandSizeInGrid(testid) = StandSizeInGrid(testid)
                    End If
                End If
            Next

            'see whether or not to include the stand in the grid or not
            StandCount = 0
            For jstand As Integer = 1 To NStands
                If (bRxFeas = True And bSpatialRx(jstand) = True) Or bRxFeas = False Then
                    If bFullStandInGrid = True Then
                        If StandSizeInGrid(jstand) / StandSize(jstand) = 1 Then 'special case if the whole grid is only 1 stand!
                            AreaStandsIncludedInGrid = AreaStandsIncludedInGrid + StandSize(jstand)
                            StandBin(jstand) = 1
                            InfFeasGridStandBin(jstand) = StandBin(jstand)
                            StandCount = StandCount + 1
                        End If
                    End If
                    If bPartStandInGrid = True Then
                        If StandSizeInGrid(jstand) > 0 Then
                            AreaStandsIncludedInGrid = AreaStandsIncludedInGrid + StandSize(jstand)
                            StandBin(jstand) = 1
                            InfFeasGridStandBin(jstand) = StandBin(jstand)
                            StandCount = StandCount + 1
                        End If
                    End If
                    If bPctStandInGrid = True Then
                        If StandSizeInGrid(jstand) / StandSize(jstand) >= PctStandInGrid Then
                            AreaStandsIncludedInGrid = AreaStandsIncludedInGrid + StandSize(jstand)
                            StandBin(jstand) = 1
                            InfFeasGridStandBin(jstand) = StandBin(jstand)
                            StandCount = StandCount + 1
                        End If
                    End If
                End If
            Next

            'collect subset of stands to loop through for patch stats
            ReDim StandsInGrid(StandCount), InfFeasGridStandsInGrid(StandCount)
            tstand = 0
            For jstand As Integer = 1 To NStands
                If StandBin(jstand) = 1 Then
                    tstand = tstand + 1
                    StandsInGrid(tstand) = jstand
                    InfFeasGridStandsInGrid(tstand) = jstand
                End If
            Next


            If AreaStandsIncludedInGrid > MinFeasGrid Then
                'lop off isoloated portions of grids (islands and such)
                FindPatch(StandSizeInGrid, StandBin, StandsInGrid, -999)


                'see if the area of the stands in the grid is larger than the min. acres to evaluate with the grid
                'populate the stands in the grid and count how many there are - regardless of any filtering or duplication
                AreaStandsIncludedInGrid = 0 're-figure this number after separated patches have been disconnected
                For jstand As Integer = 1 To NStands
                    If StandBin(jstand) = 1 Then
                        NStandsInGrid(jgrid) = NStandsInGrid(jgrid) + 1
                        OrigGridStands(jgrid, NStandsInGrid(jgrid)) = jstand
                        AreaStandsIncludedInGrid = AreaStandsIncludedInGrid + StandSize(jstand)
                        InfFeasGridStandBin(jstand) = 0 'filter it out!
                        InfFeasGridStandSizeInGrid(jstand) = 0 'filter it out
                        For jjstand As Integer = 1 To StandCount
                            If StandsInGrid(jjstand) = jstand Then
                                InfFeasGridStandsInGrid(jjstand) = 0 'take the stand out of the grid if you have included it in a patch already
                                Exit For
                            End If
                        Next
                    End If
                Next

                If AreaStandsIncludedInGrid > MinFeasGrid Then
                    If NStandsInGrid(jgrid) > 0 And UseGrid(jgrid) = 0 Then
                        UseGrid(jgrid) = 1
                        For jstand As Integer = 1 To NStandsInGrid(jgrid)
                            kstand = OrigGridStands(jgrid, jstand)
                            OrigStandIncludedAnyGrid(kstand) = 1
                        Next
                        If NStandsInGrid(jgrid) > MaxStandsInGrid Then MaxStandsInGrid = NStandsInGrid(jgrid)
                    End If
                    'now, have to loop through the other patches to see if you have more than 1 that exceeds the min. threshhold
                    ' so, take the biggest patch out and look for the second biggest patch. Repeat until you find no more feasible patches in the grid
                    ' add these inferior feasible patches to a bin to be collected after this routine

                    OtherFeasibleGrids(InfFeasGridStandSizeInGrid, InfFeasGridStandBin, InfFeasGridStandsInGrid, MinFeasGrid, OthFeasGridCount)


                End If
            End If
        Next

        ''MAKE SURE EACH STAND IS IN AT LEAST ONE GRID
        'use stand-based grids for the leftovers. Only enumerate grids that can make interior space

        'first collect the number of potential extra grids to use
        OrigNGrids = NGrids
        NStandBasedGrids = 0
        For jstand As Integer = 1 To NStands
            If OrigStandIncludedAnyGrid(jstand) = 0 Then
                NStandBasedGrids = NStandBasedGrids + 1
            End If
        Next

        NGrids = OrigNGrids + OthFeasGridCount + NStandBasedGrids
        ReDim Preserve UseGrid(NGrids)
        ReDim GridStands(NGrids, MaxStandsInGrid), standincluded(NStands)
        ReDim Preserve NStandsInGrid(NGrids)

        'populate GridStands to OrigNGrids
        For jgrid As Integer = 1 To OrigNGrids
            For jstand As Integer = 1 To MaxStandsInGrid
                GridStands(jgrid, jstand) = OrigGridStands(jgrid, jstand)
            Next
        Next
        For jgrid As Integer = OrigNGrids + 1 To OrigNGrids + OthFeasGridCount
            kgrid = jgrid - OrigNGrids
            For jstand As Integer = 1 To MaxStandsInGrid
                GridStands(jgrid, jstand) = OthFeasGridStands(kgrid, jstand)
            Next
            UseGrid(jgrid) = UseOthFeasGrid(kgrid)
            NStandsInGrid(jgrid) = OthFeasNStandsInGrid(kgrid)
        Next
        For jstand As Integer = 1 To NStands
            StandIncluded(jstand) = OrigStandIncludedAnyGrid(jstand)
        Next

        'COLLECT THE STANDS THAT AREN'T IN ANY GRID TO DATE. USE THE SEARCH RADIUS (OR SQUARE WIDTH) FROM THE CENTROID OF NON-INCLUDED STANDS
        NewGridCounter = OrigNGrids + OthFeasGridCount
        For jnewgrid As Integer = 1 To NStands
            'If jnewgrid = 134 Then Stop
            If OrigStandIncludedAnyGrid(jnewgrid) = 0 And ((bRxFeas = True And bSpatialRx(jnewgrid) = True) Or bRxFeas = False) Then 'if the stand was filtered because it never had any spatial Rx, then don't evaluate it now!
                NewGridCounter = NewGridCounter + 1
                ReDim StandBin(NStands), StandSizeInGrid(NStands)
                'make sure at least the non-included stand is considered
                StandBin(jnewgrid) = 1
                StandSizeInGrid(jnewgrid) = StandSize(jnewgrid)

                'set the new parameters based on the centroid of the stand
                If bCircle = True Then 'only use the base
                    BaseX = StandCentroidX(jnewgrid)
                    BaseY = StandCentroidY(jnewgrid)
                Else
                    BaseX = StandCentroidX(jnewgrid) - (GridHeight / 2)
                    BigX = StandCentroidX(jnewgrid) + (GridHeight / 2)
                    BaseY = StandCentroidY(jnewgrid) - (GridHeight / 2)
                    BigY = StandCentroidY(jnewgrid) + (GridHeight / 2)
                End If

                For jhex As Integer = 1 To NHex
                    If bCircle = False Then 'squares use Big and Base
                        If HexY(jhex) >= BaseY And HexY(jhex) <= BigY And HexX(jhex) >= BaseX And HexX(jhex) <= BigX Then
                            'hexagon is in this grid
                            testid = HexStandID(jhex)
                            StandSizeInGrid(testid) = StandSizeInGrid(testid) + HexAcres
                        End If
                    ElseIf bCircle = True Then 'circles only use Base
                        'have to determine whether the distance from center of circle to center of hex <= radius of the circle
                        centerdist = Math.Sqrt((BaseX - HexX(jhex)) ^ 2 + (BaseY - HexY(jhex)) ^ 2)
                        If centerdist <= r Then
                            'hexagon is in this grid
                            testid = HexStandID(jhex)
                            StandSizeInGrid(testid) = StandSizeInGrid(testid) + HexAcres
                        End If
                    End If
                Next

                'see whether or not to include the stand in the grid or not
                StandCount = 0
                For jstand As Integer = 1 To NStands
                    If (bRxFeas = True And bSpatialRx(jstand) = True) Or bRxFeas = False Then
                        If bFullStandInGrid = True And StandSizeInGrid(jstand) / StandSize(jstand) = 1 Then StandBin(jstand) = 1
                        If bPartStandInGrid = True And StandSizeInGrid(jstand) > 0 Then StandBin(jstand) = 1
                        If bPctStandInGrid = True And StandSizeInGrid(jstand) / StandSize(jstand) >= PctStandInGrid Then StandBin(jstand) = 1
                    End If
                    If StandBin(jstand) = 1 Then StandCount = StandCount + 1
                Next

                'collect subset of stands to loop through for patch stats
                ReDim StandsInGrid(StandCount)
                tstand = 0
                For jstand As Integer = 1 To NStands
                    If StandBin(jstand) = 1 Then
                        tstand = tstand + 1
                        StandsInGrid(tstand) = jstand
                    End If
                Next

                'still want to identify the patch that the parent stand is in
                FindPatch(StandSizeInGrid, StandBin, StandsInGrid, jnewgrid)
                For jjstand As Integer = 1 To NStands
                    If StandBin(jjstand) = 1 Then
                        NStandsInGrid(NewGridCounter) = NStandsInGrid(NewGridCounter) + 1
                        If NStandsInGrid(NewGridCounter) > MaxStandsInGrid Then
                            MaxStandsInGrid = NStandsInGrid(NewGridCounter)
                            ReDim Preserve GridStands(NGrids, MaxStandsInGrid)
                        End If
                        GridStands(NewGridCounter, NStandsInGrid(NewGridCounter)) = jjstand
                        StandIncluded(jjstand) = 1
                    End If
                Next
                If NStandsInGrid(NewGridCounter) > 0 Then UseGrid(NewGridCounter) = 1
            End If
        Next

        'FILTER OUT INFERIOR AND DUPLICATE GRIDS (e.g., don't include a grid with just stand 1 if a grid with 1 and 2 exists)
        For jgrid As Integer = 1 To NGrids
            'For jy As Integer = 1 To NGridsYDir
            'filter out duplicate grids
            bDuplicate = False 'prove it true
            If NStandsInGrid(jgrid) > 0 And UseGrid(jgrid) = 1 Then
                For jjgrid As Integer = 1 To NGrids
                    'For jjy As Integer = 1 To NGridsYDir
                    nmatches = 0
                    If NStandsInGrid(jgrid) <= NStandsInGrid(jjgrid) And UseGrid(jjgrid) = 1 And jgrid <> jjgrid Then
                        For jstand As Integer = 1 To NStandsInGrid(jgrid)
                            For jjstand As Integer = 1 To NStandsInGrid(jjgrid)
                                If GridStands(jgrid, jstand) = GridStands(jjgrid, jjstand) Then nmatches = nmatches + 1
                            Next jjstand
                        Next jstand
                    End If
                    If nmatches = NStandsInGrid(jgrid) Then
                        bDuplicate = True
                        Exit For
                    End If
                    'Next jjy
                    If bDuplicate = True Then Exit For
                Next jjgrid
            End If
            If bDuplicate = True And NStandsInGrid(jgrid) > 0 And UseGrid(jgrid) = 1 Then
                UseGrid(jgrid) = 0
            End If
            'Next
        Next

        'PRINT OUT THE GRID INFORMATION
        'first print a debug to check for missed stands - if there are any missed stands
        Dim bPrintDebug As Boolean = False
        For jstand As Integer = 1 To NStands
            If StandIncluded(jstand) = 0 Then
                bPrintDebug = True
                Exit For
            End If
        Next
        If bPrintDebug = True Then
            fnum = FreeFile()
            FileOpen(fnum, OFBaseName & "_debug.txt", OpenMode.Output)
            PrintLine(fnum, "Stands not included in any grid, SpatialRx?")
            For jstand As Integer = 1 To NStands
                If StandIncluded(jstand) = 0 Then
                    dummy = jstand
                    If bRxFeas = True Then
                        dummy = dummy & "," & bSpatialRx(jstand)
                    End If
                    PrintLine(fnum, dummy)
                End If
            Next
            FileClose(fnum)
        End If

        'count grids to print
        For jgrid As Integer = 1 To NGrids
            'For jy As Integer = 1 To NGridsYDir
            If UseGrid(jgrid) = 1 Then 'print the grid
                GridCounter = GridCounter + 1
            End If
            ' jy
        Next jgrid

        'then print out the actual grid information
        ReDim ParentGrid(NGrids)
        fnum = FreeFile()
        FileOpen(fnum, OFBaseName & ".txt", OpenMode.Output)
        PrintLine(fnum, "NGridsPrinted, MaxStandsInGrid")
        PrintLine(fnum, GridCounter & "," & MaxStandsInGrid)
        PrintLine(fnum, "GridID, NStandsInGrid, StandsInGrid...")
        GridCounter = 0
        For jgrid As Integer = 1 To NGrids
            'For jy As Integer = 1 To NGridsYDir
            If UseGrid(jgrid) = 1 Then 'print the grid
                dummy = ""
                GridCounter = GridCounter + 1
                ParentGrid(jgrid) = GridCounter
                dummy = GridCounter & "," & NStandsInGrid(jgrid) & ","
                For jstand As Integer = 1 To NStandsInGrid(jgrid) - 1
                    dummy = dummy & GridStands(jgrid, jstand) & ","
                Next
                dummy = dummy & GridStands(jgrid, NStandsInGrid(jgrid)) 'last stand is not followed by ","
                PrintLine(fnum, dummy)
            End If
            'Next
        Next
        FileClose(fnum)
        'If bRxFeas = True Then TimeDependentGrids(NGrids, UseGrid, ParentGrid, GridStands, NStandsInGrid)

    End Sub

    Private Sub StandBasedGrid()
        'pre-process this, so you have stand-based grid information to load into the Dual Plan run
        'requires:
        ' 1. MxZonesForAA
        ' 2. ZoneAAList
        ' 3. DualPlanAAPoly().IZones()

        Dim kzone As Integer
        Dim bDuplicate As Boolean
        Dim nmatches As Integer
        Dim UseGrid() As Integer
        Dim CountUsedGrids As Integer
        Dim StandsInGrid(,) As Integer 'array of the stands in a given grid
        Dim AANStandsInGrid() As Integer
        Dim StandBin() As Integer 'collect the stands in the bin of influenced stands for the parent stand...
        'Dim tgrid As Integer
        Dim fnum As Integer

        'Grid stuff
        Dim NGrids As Integer
        Dim MaxStandsInGrid As Integer
        Dim GridStands(,) As Integer 'by ngrids, maxstandsingrid
        Dim NStandsInGrid() As Integer 'number of stands in this grid - by NGrids
        Dim GridCounter As Integer
        Dim dummy As String

        'have to use the influence zone information to tease out influenced stands
        ReDim UseGrid(NStands), StandsInGrid(NStands, NStands), AANStandsInGrid(NStands)

        MaxStandsInGrid = 0
        CountUsedGrids = NStands
        For jstand As Integer = 1 To NStands
            ReDim StandBin(NStands)
            For jzone As Integer = 1 To gMXZonesForAA
                kzone = gStandIZones(jstand, jzone) 'DualPlanAAPoly(jstand).IZones(jzone)
                If kzone > 0 Then
                    StandBin(jstand) = 1 'only use this if the stand is capable of producing interior space either by itself or with another stand(s)
                    For jdim As Integer = 1 To gMXZoneDim
                        If gZoneAAList(kzone, jdim) > 0 Then StandBin(gZoneAAList(kzone, jdim)) = 1
                    Next jdim
                End If
            Next

            For jjstand As Integer = 1 To NStands
                If StandBin(jjstand) = 1 Then
                    AANStandsInGrid(jstand) = AANStandsInGrid(jstand) + 1
                    StandsInGrid(jstand, AANStandsInGrid(jstand)) = jjstand
                End If
            Next
            If AANStandsInGrid(jstand) > MaxStandsInGrid Then MaxStandsInGrid = AANStandsInGrid(jstand)
            If AANStandsInGrid(jstand) > 0 Then UseGrid(jstand) = 1
        Next

        'NOW, CHECK FOR INFERIOR GRIDS - e.g., don't evaluate grid (2,3) if (2,3,4) exists
        For jgrid As Integer = 1 To NStands
            'filter out duplicate grids
            bDuplicate = False 'prove it true
            If AANStandsInGrid(jgrid) > 0 And UseGrid(jgrid) = 1 Then
                For jjgrid As Integer = 1 To NStands
                    nmatches = 0
                    If AANStandsInGrid(jgrid) < AANStandsInGrid(jjgrid) And UseGrid(jjgrid) = 1 Then
                        For jstand As Integer = 1 To AANStandsInGrid(jgrid)
                            For jjstand As Integer = 1 To AANStandsInGrid(jjgrid)
                                If StandsInGrid(jgrid, jstand) = StandsInGrid(jjgrid, jjstand) Then nmatches = nmatches + 1
                            Next jjstand
                        Next jstand
                    End If
                    If nmatches = AANStandsInGrid(jgrid) Then
                        bDuplicate = True
                        Exit For
                    End If
                Next jjgrid
            End If
            If bDuplicate = True And AANStandsInGrid(jgrid) > 0 And UseGrid(jgrid) = 1 Then
                UseGrid(jgrid) = 0
                CountUsedGrids = CountUsedGrids - 1
            End If
        Next

        'now, populate standard information
        ReDim GridStands(NStands, MaxStandsInGrid), NStandsInGrid(NStands)
        NGrids = CountUsedGrids
        'tgrid = 0
        GridCounter = 0
        For jgrid As Integer = 1 To NStands
            If UseGrid(jgrid) = 1 Then
                'tgrid = tgrid + 1
                GridCounter = GridCounter + 1
                NStandsInGrid(jgrid) = AANStandsInGrid(jgrid)
                For jjstand As Integer = 1 To NStandsInGrid(jgrid)
                    GridStands(jgrid, jjstand) = StandsInGrid(jgrid, jjstand)
                Next
            End If
        Next

        'PRINT OUT THE GRID INFORMATION

        'then print out the actual grid information
        fnum = FreeFile()
        FileOpen(fnum, OFBaseName & ".txt", OpenMode.Output)
        PrintLine(fnum, "NGridsPrinted, MaxStandsInGrid")
        PrintLine(fnum, GridCounter & "," & MaxStandsInGrid)
        PrintLine(fnum, "GridID, NStandsInGrid, StandsInGrid...")
        GridCounter = 0
        For jgrid As Integer = 1 To NStands
            If UseGrid(jgrid) = 1 Then 'print the grid
                dummy = ""
                GridCounter = GridCounter + 1
                dummy = GridCounter & "," & NStandsInGrid(jgrid) & ","
                For jstand As Integer = 1 To NStandsInGrid(jgrid) - 1
                    dummy = dummy & GridStands(jgrid, jstand) & ","
                Next
                dummy = dummy & GridStands(jgrid, NStandsInGrid(jgrid)) 'last stand is not followed by ","
                PrintLine(fnum, dummy)
            End If
        Next
        FileClose(fnum)

    End Sub
    Public Sub LoadIZones(ByVal infile As String)
        Dim dummy1A As String
        Dim dummy2A As String

        Dim kcount As Integer
        Dim dlen As Integer
        Dim kstand As Integer 'stand id of the stand in the izone
        Dim arr() As String
        'Dim MxZones As Integer = 0
        gMxZonesForAA = 0

        'FileNameA = BaseNameIzoneInFilesA & CStr(jsf) & ".csv"
        FileOpen(45, infile, OpenMode.Input)
        dummy1A = LineInput(45)
        Input(45, gNZoneSubFor)
        Input(45, gMXZoneDim)

        ReDim gZonedim(gNZoneSubFor)
        ReDim gZoneAAList(gNZoneSubFor, gMXZoneDim)
        ReDim gNIZonesStand(NStands)
        ReDim gStandIZones(NStands, gMXZonesForAA)
        'ReDim gZoneAAlistAreaS(gNZoneSubFor, gMXZoneDim)
        ReDim gZoneArea(gNZoneSubFor)
        dummy2A = LineInput(45)

        For jzone As Integer = 1 To gNZoneSubFor
            arr = Split(LineInput(45), ",")
            kcount = arr(0) 'Input(45, kcount) 'let kcount also be the ID for the izone
            If jzone <> kcount Then
                MsgBox("Influence zones must be labeled and indexed sequentially from 1 to the total.")
                End
            End If
            gZoneDim(jzone) = arr(1) 'Input(45, gZoneDim(jzone))
            For jdim As Integer = 1 To gMXZoneDim
                gZoneAAList(jzone, jdim) = arr(jdim + 1) 'Input(45, gZoneAAList(jzone, jdim))
                kstand = gZoneAAList(jzone, jdim)
                If kstand > 0 Then 'gZoneDim(jzone) > 1 And 
                    gNIZonesStand(kstand) = gNIZonesStand(kstand) + 1
                    dlen = gNIZonesStand(kstand) 'DlPlM1Poly(ZoneAAlist(jzone, jdim)).IZones.Length
                    If dlen > gMXZonesForAA Then
                        gMXZonesForAA = dlen
                        ReDim Preserve gStandIZones(NStands, gMXZonesForAA)
                    End If
                    gStandIZones(kstand, dlen) = jzone 'or kzone
                End If
                'add up the area of the zone
                gZoneArea(jzone) = gZoneArea(jzone) + arr(jdim + 4) 'add areas in each stand
            Next jdim

        Next jzone
        FileClose(45)

    End Sub
    Public Sub LoadRxInfo()
        'loads the prescriptions per poly and the flows per prescription
        Dim arr() As String
        Dim fnum As Integer
        Dim tind As Integer
        Dim sind As Integer
        Dim nrxpoly As Integer
        Dim krx As Integer
        Dim dummy As String

        ReDim NPolyRx(NStands)
        MaxRxPerPoly = 0
        fnum = FreeFile()
        FileOpen(fnum, PolyRxFile, OpenMode.Input)
        'first find the max NRx
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, " ")
            nrxpoly = arr.Length - 1
            tind = arr(0) 'stand number
            NPolyRx(tind) = nrxpoly
            If nrxpoly > MaxRxPerPoly Then MaxRxPerPoly = nrxpoly
        Loop
        FileClose(fnum)

        NRx = 0
        fnum = FreeFile()
        FileOpen(fnum, PolyRxFile, OpenMode.Input)
        'then populate the Rx per poly
        ReDim PolyRx(NStands, MaxRxPerPoly)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, " ")
            'NRx = arr.Length - 1
            tind = arr(0) 'stand number
            For jrx As Integer = 1 To arr.Length - 1
                krx = Trim(arr(jrx))
                If krx > 0 Then PolyRx(tind, jrx) = krx
                If krx > NRx Then NRx = krx
            Next
        Loop
        FileClose(fnum)
        NRx = NRx + 1 'the last index is a dummy

        'read the RxSpatialInd file'figure out the spatial index per prescription
        'NPer = 0
        ReDim RxSpatialInd(NRx)
        fnum = FreeFile()
        FileOpen(fnum, RxSpatialIndFile, OpenMode.Input)
        dummy = Trim(LineInput(fnum))
        NSpatialInd = 0
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, " ")
            tind = arr(0) 'Rx number
            RxSpatialInd(tind) = arr(2)
            If RxSpatialInd(tind) > NSpatialInd Then NSpatialInd = RxSpatialInd(tind)
        Loop
        FileClose(fnum)

        'find out the number of spatial flows per prescription
        ReDim NSpatialFlows(NSpatialInd), SpatialFlows(NSpatialInd, NPer)
        fnum = FreeFile()
        FileOpen(fnum, SpatialFlowFile, OpenMode.Input)
        'figure out the spatial index per prescription
        dummy = Trim(LineInput(fnum))
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, " ")
            tind = arr(0) 'Spatial Index number
            NSpatialFlows(tind) = arr(1)
            For jper As Integer = 1 To NPer
                SpatialFlows(tind, jper) = arr(jper + 1)
            Next
        Loop
        FileClose(fnum)

        'NOW, populate the bSpatialRx() whether a stand has a spatial prescription avaialable or not!
        ReDim bSpatialRx(NStands)
        For jstand As Integer = 1 To NStands
            bSpatialRx(jstand) = False
            For jrx As Integer = 1 To NPolyRx(jstand)
                tind = PolyRx(jstand, jrx)
                sind = RxSpatialInd(tind)
                If NSpatialFlows(sind) > 0 Then
                    bSpatialRx(jstand) = True
                    Exit For 'go to next stand
                End If
            Next
        Next

    End Sub
    Private Function SolveTheta(ByVal k As Double, ByVal tolerance As Double) As Double
        'finds the area at the center of a circle given k, which is 2K/r^2. K= area of segment, r = radius
        Dim xatn As Double
        Dim lastxatn As Double

        xatn = (6 * k) ^ (1 / 3)
        Do Until tolerance > Math.Abs(xatn - Math.Sin(xatn) - k)
            lastxatn = xatn
            xatn = lastxatn - (lastxatn - Math.Sin(lastxatn) - k) / (1 - Math.Cos(lastxatn))
        Loop
        SolveTheta = xatn

    End Function

    Private Sub rbStandBasedGrids_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbStandBasedGrids.CheckedChanged
        If rbStandBasedGrids.Checked = True Then
            tbGridSize.Enabled = False
            tbMinFeasGrid.Enabled = False
            tbPctGridOverlap.Enabled = False
            cbCircleGrid.Enabled = False
            cbCircleGrid.Checked = False
        ElseIf rbStandBasedGrids.Checked = False Then
            tbGridSize.Enabled = True
            tbMinFeasGrid.Enabled = True
            tbPctGridOverlap.Enabled = True
            cbCircleGrid.Enabled = True
        End If
    End Sub


    Private Sub FindPatch(ByRef StandSizeInGrid() As Single, ByRef StandInGrid() As Integer, ByVal StandsInGrid() As Integer, ByVal StandIDToKeep As Integer)

        'subroutine goes through the latest solution and tallies patches included in the grid and filters out stands not in the largest one
        ' EXCEPT - if you use a positive StandIDToKeep, the routine will keep the patch with that standid

        'logic - go through each Influence zone (another way of measuring adjacency; already set up
        'search for the max stand ID associated with each influence zone/patch

        'ultimately trying to populate the following three sets:
        Dim PatchSize() As Double 'by GuessMaxStands (max possible patches) - the acres of the patch
        Dim PatchNStands() As Integer 'by GuessMaxStands (max possible patches) - number of stands in hte patch
        Dim ISpaceInPatch() As Single 'the ispace in the patch 
        Dim ZoneHit() As Byte 'whether the zone area has been counted yet or not
        Dim KeeperPatchSize As Double 'patch size of the patch to keep
        Dim NStandsToCount As Integer

        'reassign a stand to the max ID associated with each patch
        Dim StandMaxID() As Long 'by period, stand...
        Dim mxid As Integer

        'for evaluating adjacent stands and such
        Dim kaa As Integer
        Dim taa As Integer
        Dim xaa As Integer
        Dim zoneID As Integer
        Dim bAllStandsInGrid As Boolean

        'for evaluating when the patches have been fully defined
        Dim matchper As Integer = 1
        Dim matchstand As Integer = 1
        Dim dStandIDToKeep As Integer 'dummy

        NStandsToCount = StandsInGrid.Length - 1
        ReDim StandMaxID(NStands), ISpaceInPatch(NStands) ', ISpaceInGrid(NStands)

        'default values for max stand ID is itself
        For jaa As Integer = 1 To NStands 'NStandsToCount
            StandMaxID(jaa) = jaa
        Next

        ReDim PatchSize(NStands)
        ReDim PatchNStands(NStands)
        matchper = 1
        Do Until matchper = 0
            matchper = 0

            For jaa As Integer = 1 To NStandsToCount
                'filter out stands in grid without interior space!
                taa = StandsInGrid(jaa)
                If gNIZonesStand(taa) = 0 Then StandInGrid(taa) = 0
                If StandInGrid(taa) > 0 Then
                    For jiz As Integer = 1 To gNIZonesStand(taa) 'DualPlanAAPoly(jaa).IZones.Length - 1
                        zoneID = gStandIZones(taa, jiz) 'DualPlanAAPoly(jaa).IZones(jiz)
                        bAllStandsInGrid = True
                        'find whether all stands in the zone are in the grid
                        For jdim As Integer = 1 To gZoneDim(zoneID)
                            kaa = gZoneAAList(zoneID, jdim)
                            If StandInGrid(kaa) = False Then 'And ZoneAAlistAreaS(zoneID, jdim) > 0 Then
                                bAllStandsInGrid = False
                                Exit For
                            End If
                        Next
                        If bAllStandsInGrid = True Then
                            For jjaa As Integer = 1 To gZoneDim(zoneID)
                                kaa = gZoneAAList(zoneID, jjaa)
                                'largest of the two
                                If StandMaxID(taa) <> StandMaxID(kaa) Then
                                    mxid = Math.Max(StandMaxID(taa), StandMaxID(kaa))
                                    StandMaxID(taa) = mxid
                                    StandMaxID(kaa) = mxid
                                    matchper = matchper + 1
                                End If
                            Next
                        End If
                    Next jiz
                End If
            Next jaa
        Loop
        'record the max patch stats for this period and collect the ispace of this patch
        KeeperPatchSize = 0
        dStandIDToKeep = StandIDToKeep
        For jaa As Integer = 1 To NStandsToCount
            taa = StandsInGrid(jaa)
            kaa = StandMaxID(taa)
            PatchSize(kaa) = PatchSize(kaa) + StandSizeInGrid(taa)
            'ISpaceInPatch(kaa) = ISpaceInPatch(kaa) + ISpaceInGrid(jaa)
            If StandIDToKeep < 0 Then 'find the largest one if you don't care about including a particular stand
                If PatchSize(kaa) > KeeperPatchSize Then
                    KeeperPatchSize = PatchSize(kaa)
                    dStandIDToKeep = kaa
                End If
            End If
            PatchNStands(kaa) = PatchNStands(kaa) + 1
        Next

        If StandIDToKeep < 0 Then
            StandIDToKeep = dStandIDToKeep
        ElseIf StandIDToKeep > 0 Then
            dStandIDToKeep = StandIDToKeep
            StandIDToKeep = StandMaxID(dStandIDToKeep)
        End If

        'tally the ispace of the keeper patch
        ReDim ZoneHit(gNZoneSubFor)
        For jaa As Integer = 1 To NStandsToCount
            'filter out stands in grid without interior space!
            taa = StandsInGrid(jaa)
            kaa = StandMaxID(taa)
            If kaa = StandIDToKeep Then
                For jiz As Integer = 1 To gNIZonesStand(taa) 'DualPlanAAPoly(jaa).IZones.Length - 1
                    zoneID = gStandIZones(taa, jiz) 'DualPlanAAPoly(jaa).IZones(jiz)
                    bAllStandsInGrid = True
                    'find whether all stands in the zone are in the grid

                    For jdim As Integer = 1 To gZoneDim(zoneID)
                        xaa = gZoneAAList(zoneID, jdim)
                        If StandInGrid(xaa) = False Then 'And ZoneAAlistAreaS(zoneID, jdim) > 0 Then
                            bAllStandsInGrid = False
                            Exit For
                        End If
                    Next
                    If bAllStandsInGrid = True Then
                        If ZoneHit(zoneID) = 0 Then
                            ISpaceInPatch(kaa) = ISpaceInPatch(kaa) + gZoneArea(zoneID)
                            ZoneHit(zoneID) = 1
                        End If
                    End If
                Next jiz
            End If
        Next

        'if stand belongs to the largest patch (or keeper patch) then
        For jaa As Integer = 1 To NStandsToCount
            taa = StandsInGrid(jaa)
            kaa = StandMaxID(taa)
            If kaa <> StandIDToKeep Or ISpaceInPatch(kaa) <= 0 Then 'PatchSize(kaa) <> KeeperPatchSize Then
                StandSizeInGrid(taa) = 0 'filter this patch out of the grid!
                StandInGrid(taa) = 0
            End If
        Next jaa

    End Sub

    Private Sub OtherFeasibleGrids(ByVal StandSizeInGrid() As Single, ByVal StandBin() As Integer, ByVal OrigStandsInGrid() As Integer, ByVal MnFeasGrid As Single, ByVal kgrid As Integer)

        Dim AreaStandsIncludedInGrid As Single 'area of the stands included in the grid
        Dim OrigStandCount As Integer = OrigStandsInGrid.Length - 1
        Dim StandCount As Integer
        Dim StandsInGrid() As Integer
        Dim OthFeasGridStandBin() As Integer
        Dim OthFeasGridStandSizeInGrid() As Single
        Dim OthFeasGridStandsInGrid() As Integer
        Dim kstand As Integer
        Dim tstand As Integer

        kgrid = kgrid + 1
        OthFeasGridCount = OthFeasGridCount + 1
        For jstand As Integer = 1 To OrigStandCount
            If OrigStandsInGrid(jstand) > 0 Then
                StandCount = StandCount + 1
            End If
        Next
        ReDim StandsInGrid(StandCount), OthFeasGridStandsInGrid(StandCount)
        tstand = 0
        For jstand As Integer = 1 To OrigStandCount
            If OrigStandsInGrid(jstand) > 0 Then
                tstand = tstand + 1
                StandsInGrid(tstand) = OrigStandsInGrid(jstand)
                OthFeasGridStandsInGrid(tstand) = OrigStandsInGrid(jstand)
                AreaStandsIncludedInGrid = AreaStandsIncludedInGrid + StandSizeInGrid(OrigStandsInGrid(jstand))
            End If
        Next
        ReDim OthFeasGridStandSizeInGrid(NStands), OthFeasGridStandBin(NStands)
        For jstand As Integer = 1 To NStands
            OthFeasGridStandSizeInGrid(jstand) = StandSizeInGrid(jstand)
            OthFeasGridStandBin(jstand) = StandBin(jstand)
        Next

        If AreaStandsIncludedInGrid > MnFeasGrid Then
            'lop off isoloated portions of grids (islands and such)
            FindPatch(StandSizeInGrid, StandBin, StandsInGrid, -999)


            'see if the area of the stands in the grid is larger than the min. acres to evaluate with the grid
            'populate the stands in the grid and count how many there are - regardless of any filtering or duplication
            AreaStandsIncludedInGrid = 0 're-figure this number after separated patches have been disconnected
            For jstand As Integer = 1 To NStands
                'If jstand = 8804 Then Stop
                If StandBin(jstand) = 1 Then
                    OthFeasNStandsInGrid(kgrid) = OthFeasNStandsInGrid(kgrid) + 1
                    OthFeasGridStands(kgrid, OthFeasNStandsInGrid(kgrid)) = jstand
                    'OrigGridStands(jgrid, NStandsInGrid(jgrid)) = jstand
                    AreaStandsIncludedInGrid = AreaStandsIncludedInGrid + StandSize(jstand)
                    OthFeasGridStandBin(jstand) = 0 'filter it out!
                    OthFeasGridStandSizeInGrid(jstand) = 0 'filter it out
                    For jjstand As Integer = 1 To StandCount
                        If StandsInGrid(jjstand) = jstand Then
                            OthFeasGridStandsInGrid(jjstand) = 0 'take the stand out of the grid if you have included it in a patch already
                            Exit For
                        End If
                    Next
                End If
            Next

            If AreaStandsIncludedInGrid > MnFeasGrid Then
                If OthFeasNStandsInGrid(kgrid) > 0 And UseOthFeasGrid(kgrid) = 0 Then
                    UseOthFeasGrid(kgrid) = 1
                    'OthFeasGridCount = OthFeasGridCount + 1
                    For jstand As Integer = 1 To OthFeasNStandsInGrid(kgrid)
                        kstand = OthFeasGridStands(kgrid, jstand)
                        'If kstand = 9715 Then Stop
                        'InfFeasGridStands()
                        OrigStandIncludedAnyGrid(kstand) = 1
                    Next
                    If OthFeasNStandsInGrid(kgrid) > MaxStandsInGrid Then MaxStandsInGrid = OthFeasNStandsInGrid(kgrid)
                End If
                'now, have to loop through the other patches to see if you have more than 1 that exceeds the min. threshhold
                ' so, take the biggest patch out and look for the second biggest patch. Repeat until you find no more feasible patches in the grid
                ' add these inferior feasible patches to a bin to be collected after this routine

                OtherFeasibleGrids(OthFeasGridStandSizeInGrid, OthFeasGridStandBin, OthFeasGridStandsInGrid, MnFeasGrid, kgrid)


            End If
        End If

    End Sub

    Private Sub TimeDependentGrids(ByVal NGrids As Integer, ByVal UseGrid() As Byte, ByVal ParentGrid() As Integer, ByVal GridStands(,) As Integer, ByVal NStandsInGrid() As Integer)
        'goes through each grid by time period to search for feasible patches to evaluate at each time period
        'intent is that a large patch in a grid does not drown out an inferior-size patch that may have legitimate spatial value

        Dim StandCount As Integer
        Dim kstand As Integer
        Dim tstand As Integer
        Dim tind As Integer
        Dim sind As Integer
        Dim StandBin() As Integer 'collection of stands to dump into GridStands. Can be ordered before dumped into GridStands
        Dim StandSizeInGrid() As Single 'the area of the portion of the stand within the grid
        Dim StandsInGrid() As Integer 'the stands in this actual grid
        Dim fnum As Integer
        Dim dummy As String

        'mine the spatial series information for data about first tic
        ReDim SpatialSeriesFirstTic(NSpatialInd, NPer) 'MaxFirstTic)SpatialSeriesNFirstTic(NSpatialInd), 
        For jspat As Integer = 1 To NSpatialInd
            'catch time 1
            If SpatialFlows(jspat, 1) = 1 Then
                SpatialSeriesFirstTic(jspat, 1) = 1
            End If
            For jtic As Integer = 2 To NPer
                If SpatialFlows(jspat, jtic) = 1 And SpatialFlows(jspat, jtic - 1) = 0 Then 'have a first flow tic
                    SpatialSeriesFirstTic(jspat, jtic) = 1
                End If
            Next
        Next

        'evaluate these one period at a time...
        fnum = FreeFile()
        FileOpen(fnum, OFBaseName & "TimeGrids.txt", OpenMode.Output)
        PrintLine(fnum, "Tic, ParentGrid, NStandsInGrid, StandsInGrid...")
        For jtic As Integer = 1 To NPer
            For jgrid As Integer = 1 To NGrids
                'redim all this stuff...
                ReDim OthFeasGridStands(NGrids, MaxStandsInGrid), OthFeasNStandsInGrid(NGrids), UseOthFeasGrid(NGrids)
                OthFeasGridCount = 0
                ReDim StandBin(NStands), StandSizeInGrid(NStands)
                If UseGrid(jgrid) = 1 Then 'print the grid
                    For jstand As Integer = 1 To NStandsInGrid(jgrid)
                        kstand = GridStands(jgrid, jstand)
                        For jrx As Integer = 1 To NPolyRx(kstand)
                            tind = PolyRx(kstand, jrx)
                            sind = RxSpatialInd(tind)
                            If SpatialSeriesFirstTic(sind, jtic) = 1 Then
                                'include this stand in the bin
                                StandBin(kstand) = 1
                                StandSizeInGrid(kstand) = StandSize(kstand)
                                Exit For
                            End If
                        Next jrx
                    Next jstand
                    'count up the stands included
                    StandCount = 0
                    For jstand As Integer = 1 To NStands
                        If StandBin(jstand) = 1 Then StandCount = StandCount + 1
                    Next jstand

                    'collect subset of stands to loop through for patch stats
                    ReDim StandsInGrid(StandCount)
                    tstand = 0
                    For jstand As Integer = 1 To NStands
                        If StandBin(jstand) = 1 Then
                            tstand = tstand + 1
                            StandsInGrid(tstand) = jstand
                        End If
                    Next
                    If StandCount > 0 Then
                        OtherFeasibleGrids(StandSizeInGrid, StandBin, StandsInGrid, 0, 0)
                    End If
                End If 'if usegrid

                'then print out the actual grid information

                For jpgrid As Integer = 1 To OthFeasGridCount 'NGrids
                    If UseOthFeasGrid(jpgrid) = 1 Then 'print the grid
                        dummy = ""
                        dummy = jtic & "," & ParentGrid(jgrid) & "," & OthFeasNStandsInGrid(jpgrid) & ","
                        For jstand As Integer = 1 To OthFeasNStandsInGrid(jpgrid) - 1
                            dummy = dummy & OthFeasGridStands(jpgrid, jstand) & ","
                        Next
                        dummy = dummy & OthFeasGridStands(jpgrid, OthFeasNStandsInGrid(jpgrid)) 'last stand is not followed by ","
                        PrintLine(fnum, dummy)
                    End If
                Next
            Next jgrid
        Next jtic

        FileClose(fnum)
    End Sub


    Private Sub cbRxFeas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbRxFeas.CheckedChanged
        tbNPer.Enabled = cbRxFeas.Checked
    End Sub


End Class