Public Class Curve
    Dim PaintBitMap As System.Drawing.Bitmap
    Private Sub Curve_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim i As Integer
        Dim x As Integer
        Dim y As Integer
        Dim y1 As Integer
        Dim x1 As Integer
        Dim lastX As Integer
        Dim lastY As Integer
        Dim NewBitMap As System.Drawing.Bitmap
        Dim g As System.Drawing.Graphics

        x = Panel1.Width - 1
        y = Panel1.Height - 1
        NewBitMap = New Bitmap(Panel1.Width, Panel1.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
        g = Graphics.FromImage(NewBitMap)
        g.DrawLine(Pens.Black, 0, y, x, y)
        g.DrawLine(Pens.Black, 0, y, 0, 0)
        'FlopFactorArray(MaxFlopIndex)
        'MaxFlopAdjustFactor
        For i = 1 To MaxFlopAdjustFactor
            y1 = Math.Round(y - i * y / MaxFlopAdjustFactor, 0)
            g.DrawLine(Pens.DarkRed, 0, y1, 5, y1)
        Next
        For i = 1 To FlopNBreaks
            x1 = Math.Round(0 + i * x / FlopNBreaks, 0)
            g.DrawLine(Pens.DarkRed, x1, y, x1, y - 5)
        Next
        'draw the points
        lastX = 0
        lastY = Math.Round(y - y * FlopFactorArray(0) / MaxFlopAdjustFactor, 0)
        For i = 1 To FlopNBreaks
            x1 = Math.Round(0 + i * x / FlopNBreaks, 0)
            'tricky
            y1 = Math.Round(y - y * FlopFactorArray(i) / MaxFlopAdjustFactor, 0)
            g.DrawLine(Pens.Blue, lastX, lastY, x1, y1)
            lastX = x1
            lastY = y1
        Next
        PaintBitMap = NewBitMap

    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint
        e.Graphics.Clear(Color.LightGray)
        e.Graphics.DrawImage(PaintBitMap, 0, 0)
    End Sub
End Class