Option Strict Off
Option Explicit On
Friend Class IterSteps
	Inherits System.Windows.Forms.Form

    Dim WindowFile As String = ""
    Dim TrimFile As String = ""
    Dim bHighestValue As Boolean
    Dim bClosestValue As Boolean
    Dim bOnlyFirstDPRx As Boolean
    'Dim MxPresPerPoly As Integer


    Private Sub cmdItrPrcApply_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdItrPrcApply.Click
        Dim dirarray() As String
        Dim dirstring As String

        NShape = CShort(CStr(txtNShape.Text))
        NSmooth = CShort(CStr(txtNSmooth.Text))

        bSmoothBackFlopControl = cbSmoothBackSFlopControl.Checked
        bSmoothSideFlopControl = cbSmoothSideSFlopControl.Checked
        BackFlopFactor = CType(CStr(tbBackFlopFactor.Text), Single)
        FlopNBreaks = CType(CStr(tbMaxBreaks.Text), Single)
        MaxFlopAdjustFactor = CType(CStr(tbFlopConMaxMult.Text), Single)
        BackFlopFactor = CType(CStr(tbBackFlopFactor.Text), Single)

        NFloat = CShort(CStr(txtNFloat.Text))
        NAdjSetIters = NShape + (NShape * NSmooth) + NFloat + (NFloat * NSmooth)
        HalfLife = CShort(CStr(txtHalfLife.Text))
        ShadowPriceDecayfactor = 1 'default if the input is '0'
        If HalfLife > 0 Then ShadowPriceDecayfactor = System.Math.Exp(System.Math.Log(0.5) / HalfLife) 'calculate it if the decay factor is not 0

        ZoneRxFilter = tbTrimIzoneControls.Text
        TrimTolerance = CType(tbTrimTol.Text, Single)
        TrimTolerance = 1 + TrimTolerance
        FootprintPct = CType(tbFootprintPct.Text, Single)
        bOneOptAfterDP = cbOneOptAfterDP.Checked
        IterationNumber = CType(tbIterationNum.Text, Integer) - 1 'each iteration increments by 1, so start at 1 less than this to accommodate
        TrimSpeed = CType(tbTrimSpeed.Text, Integer)
        bGridRxBuilder = cbGridBuilder.Checked
        DollarPerAcreTol = CType(tbDollarPerAcreTol.Text, Single)
        ItersBetweenTrims = CType(tbItersBetweenTrims.Text, Integer)


        CycleHalfLife = CType(tbCycleHalfLife.Text, Integer)
        kSchedLockCycles = CType(tbSchedLockCycles.Text, Integer)
        kUnschedLockCycles = CType(tbUnschedLockCycles.Text, Integer)
        CycleDecayFactor = 1
        If CycleHalfLife > 0 Then CycleDecayFactor = System.Math.Exp(System.Math.Log(0.5) / CycleHalfLife)

        bDPCycles = False 'default
        NumDPCycles = CType(tbNumDPCycles.Text, Integer)
        If NumDPCycles > 0 Then bDPCycles = True
        'DPRefresh = CType(tbDPRefresh.Text, Integer)
        bLockExactSDSol = cbLockExactSDSol.Checked
        bReportByOrigSD = cbReportByOrigSD.Checked 'now, we're just tracking whether to report first DP subdivs, or some predefined list (needs to be loaded)
        bReportByPredefSD = cbReportByPredefSD.Checked
        bReportBySD = True 'default
        'ItersToRefresh = DPRefresh
        'If rbAreaWeight.Checked = True Then WeightScheme = "AREA"
        KeepBestNS = cbKeepBestNS.Checked
        NDPDirections = 0
        dirstring = tbDirections.Text
        dirarray = Split(tbDirections.Text, ",")
        If dirarray.Length > 0 Then
            If dirarray(0) > 0 Then
                NDPDirections = dirarray.Length
                ReDim DirectionsToSearch(NDPDirections)
                For jdir As Integer = 1 To NDPDirections
                    DirectionsToSearch(jdir) = CType(Trim(dirarray(jdir - 1)), Integer)
                Next
            End If
        End If

        If CType(tbWindowSizeFactor.Text, Single) > 0 Then bIncreaseWindowSize = True
        If CType(tbWindowMaxSize.Text, Long) > 0 Then WindowMaxSize = CType(tbWindowMaxSize.Text, Long)
        If bIncreaseWindowSize = True Then
            WindowSizeFactor = CType(tbWindowSizeFactor.Text, Single)
            If WindowSizeFactor < 10 Then WindowSizeFactor = WindowSizeFactor + 1
        Else
            WindowSizeFactor = 1
        End If

        '*
        bHighestValue = rbHighestVal.Checked
        bClosestValue = rbClosestVal.Checked
        bOnlyFirstDPRx = rbOnlyFirstDPRx.Checked

        MxPresPerPoly = CType(tbMxPresPerPoly.Text, Single)
        MxPresUnchosen = MxPresPerPoly 'default
        MaxPresChosen = MxPresPerPoly 'default
        If bOnlyFirstDPRx = False Then MxPresUnchosen = CType(tbMxPresUnchosen.Text, Single)
        If bOnlyFirstDPRx = False Then MaxPresChosen = CType(tbMaxPresChosen.Text, Single)

        'catch the default stuff if you haven't loaded a trim parameter file
        If bFileDefinedTrimParms = False Then
            NTrialSets = 2
            NBreakSets = 2
            MaxNStandSizeBreaks = 1
            ReDim TrialSetBegTrial(NTrialSets), TrialSetEndTrial(NTrialSets), TrialTrimType(NTrialSets), TrialBreakSet(NTrialSets), TrialBreakSetNBreaks(NBreakSets)
            TrialSetBegTrial(1) = 0
            TrialSetEndTrial(1) = 0
            TrialTrimType(1) = "Highest"
            TrialBreakSet(1) = 1

            TrialSetBegTrial(2) = 1
            TrialSetEndTrial(2) = NumDPCycles
            If bHighestValue = True Then TrialTrimType(2) = "Highest"
            If bClosestValue = True Then TrialTrimType(2) = "Closest"
            If bOnlyFirstDPRx = True Then TrialTrimType(2) = "First"
            TrialBreakSet(2) = 2

            'break set stuff...
            TrialBreakSetNBreaks(1) = 1
            ReDim TrialStandMinSize(NBreakSets, MaxNStandSizeBreaks), TrialStandMaxSize(NBreakSets, MaxNStandSizeBreaks), TrialNSched(NBreakSets, MaxNStandSizeBreaks), TrialNUnsched(NBreakSets, MaxNStandSizeBreaks)
            TrialStandMinSize(1, 1) = 0
            TrialStandMaxSize(1, 1) = 99999
            TrialNSched(1, 1) = MxPresPerPoly
            TrialNUnsched(1, 1) = MxPresPerPoly

            TrialBreakSetNBreaks(2) = 1
            TrialStandMinSize(2, 1) = 0
            TrialStandMaxSize(2, 1) = 99999
            TrialNSched(2, 1) = MaxPresChosen
            TrialNUnsched(2, 1) = MxPresUnchosen

        End If


        'populate the iteration set parameters
        ReDim AdjSetIters(NAdjSetIters)
        Dim k As Integer = 0
        k = 0
        For jiter As Integer = 1 To NFloat
            k = k + 1
            AdjSetIters(k) = "Float"
            For jsm As Integer = 1 To NSmooth
                k = k + 1
                AdjSetIters(k) = "Smooth"
            Next
        Next

        For jiter As Integer = 1 To NShape
            k = k + 1
            AdjSetIters(k) = "Shape"
            For jsm As Integer = 1 To NSmooth
                k = k + 1
                AdjSetIters(k) = "Smooth"
            Next
        Next

        IterHowmanyMore = CInt(CStr(txthowmanymore.Text))
        NiterToRewrite = CShort(CStr(txtrewrite.Text))
        DeviationWriteFile = CType(tbDeviationWriteFile.Text, Single)
        UseMSetViolations = 0 '
        If cbUseMSetViolations.Checked = True Then UseMSetViolations = 1 'CInt(cbUseMarketDev.Checked)
        UseCSetViolations = 0
        If cbUseCSetViolations.Checked = True Then UseCSetViolations = 1 'CInt(cbUseConditionDev.Checked)
        UseSSetViolations = 0
        If cbUseSSetViolations.Checked = True Then UseSSetViolations = 1 'CInt(cbUseSpatialDev.Checked)
        UseMSetExcesses = 0 '
        If cbUseMSetExcesses.Checked = True Then UseMSetExcesses = 1 'CInt(cbUseMarketDev.Checked)
        UseCSetExcesses = 0
        If cbUseCSetExcesses.Checked = True Then UseCSetExcesses = 1 'CInt(cbUseConditionDev.Checked)
        UseSSetExcesses = 0
        If cbUseSSetExcesses.Checked = True Then UseSSetExcesses = 1 'CInt(cbUseSpatialDev.Checked)
        bDeviationAbsValue = rbAbsDev.Checked

        'MxAspatialLoss = CSng(CStr(txtDploss.Text)) 'Max Additional Loss ($/acre) from Spatial


        'DlPlItersWithTrim = CInt(CStr(tbDlPlItersWithTrim.Text))

        DlPlItersNOTrim = CInt(CStr(tbDlPlItersNoTrim.Text))
        DPSpaceAfterDlPlIters = CInt(CStr(tbDPSpaceAfterDlPlIters.Text))
        NSchSetIters = DlPlItersNOTrim + DPSpaceAfterDlPlIters
        OneOptStop = CType(tb1optStop.Text, Single)
        bRandomOneOpt = cbRandomOneOpt.Checked
        ReDim SchSetIters(NSchSetIters)
        k = 0

        For jset As Integer = 1 To DlPlItersNOTrim
            k = k + 1
            SchSetIters(k) = "DlPlNoTrim"
        Next
        For jset As Integer = 1 To DPSpaceAfterDlPlIters
            k = k + 1
            SchSetIters(k) = "DPSpace"
        Next
        'if there are no spatial constraints, reset the SchSetIters
        If bNeedSpatialIterations = False Then
            NSchSetIters = 1
            ReDim SchSetIters(NSchSetIters)
            SchSetIters(NSchSetIters) = "NoSpace"
        End If

        If bReportByPredefSD = True And bDPCycles = True Then
            'need to define the files to look up
            Dim openFileDialog1 As New OpenFileDialog
            With openFileDialog1
                .Title = "Choose a Subdivisions Information File"
                .FileName = FnDualPlanOutBaseA & "_subdivAA_info_sf1.csv"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With
            BaseSDInfoFileName = DirName(openFileDialog1.FileName) & FName(openFileDialog1.FileName)
            BaseSDInfoFileName = BaseSDInfoFileName.Substring(0, BaseSDInfoFileName.Length - 1)
            With openFileDialog1
                .Title = "Choose an Rx_LUT to correspond to the Subdivision AA Information"
                .FileName = FnDualPlanOutBaseA & "_RxLUT.1"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With
            BaseSDRXLUTFileName = DirName(openFileDialog1.FileName) & FName(openFileDialog1.FileName)
            openFileDialog1 = Nothing
        End If
        If bGridRxBuilder = True Then
            'open the grid builder file
            Dim openFileDialog1 As New OpenFileDialog
            With openFileDialog1
                .Title = "Choose the Grid File for the Grid Rx Builder Routine"
                '.FileName = FnDualPlanOutBaseA & "_RxLUT.1"
                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With
            GridFile = openFileDialog1.FileName
            openFileDialog1 = Nothing
        End If

        Dim SaveFileDialog1 As New SaveFileDialog
        'Dim TrimFile As String
        IterMethod = "Input"
        With SaveFileDialog1
            .Title = "Save the Run Parameter File"
            .FileName = FnDualPlanOutBaseA & "ParamsFile.txt"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, SaveFileDialog1.FileName, OpenMode.Output)
        PrintLine(fnum, "Dual Plan Input File: ", TAB(45), DualPlanInputFileName)
        PrintLine(fnum, "Output Directory ", TAB(45), FnDualPlanOutBaseA)
        PrintLine(fnum, "")
        PrintLine(fnum, "ITERATION CONTROLS")
        PrintLine(fnum, "Number of Iterations ", TAB(45), IterHowmanyMore)
        PrintLine(fnum, "Number of Shape Iters ", TAB(45), NShape)
        PrintLine(fnum, "Number of Smooth Iters ", TAB(45), NSmooth)
        PrintLine(fnum, "Number of Float Iters ", TAB(45), NFloat)
        PrintLine(fnum, "Use Back S Smooth Flop Control ", TAB(45), bSmoothBackFlopControl)
        PrintLine(fnum, "Back S Flop Control Factor ", TAB(45), BackFlopFactor)
        PrintLine(fnum, "Use Side S Smooth Flop Control ", TAB(45), bSmoothSideFlopControl)
        PrintLine(fnum, "N Breaks for Flop Control ", TAB(45), FlopNBreaks)
        PrintLine(fnum, "Max Flop Adjustment Factor ", TAB(45), MaxFlopAdjustFactor)
        'PrintLine(fnum, "Use Smooth Flop Control ", TAB(45), bSmoothFlopControl)
        'PrintLine(fnum, "Use One-way Flop Control ", TAB(45), bOneWayFlopControl)
        'PrintLine(fnum, "Flop Control Factor ", TAB(45), FlopFactor)
        PrintLine(fnum, "Iter Half Life ", TAB(45), HalfLife)
        PrintLine(fnum, "Iterations Before Write ", TAB(45), NiterToRewrite)
        PrintLine(fnum, "Write Control Parameters")
        PrintLine(fnum, "Total Deviations to trigger Write: ", TAB(45), DeviationWriteFile)
        If DeviationWriteFile > 0 Then
            Dim dumstr As String = ""
            PrintLine(fnum, "Includes:")
            If UseMSetViolations = 1 Then dumstr = dumstr & "MSet Dev"
            If UseMSetExcesses = 1 Then dumstr = dumstr & ", MSet Excess"
            If UseCSetViolations = 1 Then dumstr = dumstr & ", CSet Dev"
            If UseCSetExcesses = 1 Then dumstr = dumstr & ", CSet Excess"
            If UseSSetViolations = 1 Then dumstr = dumstr & ", SSet Dev"
            If UseSSetExcesses = 1 Then dumstr = dumstr & ", SSet Excess"
            PrintLine(fnum, dumstr)
            PrintLine(fnum, "Using Absolute Deviations?", TAB(45), bDeviationAbsValue)
        End If
        PrintLine(fnum, "")
        PrintLine(fnum, "SCHEDULING CONTROLS")
        PrintLine(fnum, "1-Opts Per Iter Set ", TAB(45), DlPlItersNOTrim)
        PrintLine(fnum, "1-Opt Stop Percent ", TAB(45), OneOptStop)
        PrintLine(fnum, "Random One Opt Iterations", TAB(45), bRandomOneOpt)
        PrintLine(fnum, "DP Iters After DualPlan Iters ", TAB(45), DPSpaceAfterDlPlIters)
        'PrintLine(fnum, "Refresh Full DP Every X Iters ", TAB(45), DPRefresh)
        PrintLine(fnum, "Do One Opt after DP?", TAB(45), bOneOptAfterDP)
        PrintLine(fnum, "")
        PrintLine(fnum, "TRIMMER CONTROLS FOR DP")
        PrintLine(fnum, "IZone Rx Trim ", TAB(45), ZoneRxFilter)
        PrintLine(fnum, "$ Trim Tolerance ", TAB(45), DollarPerAcreTol)
        PrintLine(fnum, "Max Rx Per Poly ", TAB(45), MxPresPerPoly)
        PrintLine(fnum, "Iterations Between Trims", TAB(45), ItersBetweenTrims)
        PrintLine(fnum, "Footprint Percentage for ISpace Calculation ", TAB(45), FootprintPct)
        PrintLine(fnum, "Trim Speed", TAB(45), TrimSpeed)
        If bGridRxBuilder = True Then
            PrintLine(fnum, "Grid Definition File ", TAB(45), GridFile)
        End If
        PrintLine(fnum, "")
        PrintLine(fnum, "CYCLE CONTROLS")
        PrintLine(fnum, "Number DP Cycles ", TAB(45), NumDPCycles)
        PrintLine(fnum, "Lock Exact SD Solutions ", TAB(45), bLockExactSDSol)
        PrintLine(fnum, "Report by Original SD", TAB(45), bReportByOrigSD)
        PrintLine(fnum, "Max Rx for Polys In Sol: ", TAB(45), MaxPresChosen)
        PrintLine(fnum, "Keep Best Non-Spatial? ", TAB(45), KeepBestNS)
        PrintLine(fnum, "Use Highest Rx Filter? ", TAB(45), bHighestValue)
        PrintLine(fnum, "Use Closest Rx Filter? ", TAB(45), bClosestValue)
        PrintLine(fnum, "Use Only Rx From 1st DP?", TAB(45), bOnlyFirstDPRx)
        PrintLine(fnum, "Half Life of NRx In Scheduled ", TAB(45), CycleHalfLife)
        PrintLine(fnum, "Lock Sched Rx After N Cycles ", TAB(45), kSchedLockCycles)
        PrintLine(fnum, "Max RX for Unscheduled Stands ", TAB(45), MxPresUnchosen)
        PrintLine(fnum, "Lock Unsched Rx After N Cycles ", TAB(45), kUnschedLockCycles)
        PrintLine(fnum, "Directional Search Series ", TAB(45), dirstring)
        PrintLine(fnum, "Factor/Number States to add per cycle ", TAB(45), WindowSizeFactor)
        PrintLine(fnum, "Max window size ", TAB(45), WindowMaxSize)
        PrintLine(fnum, "")
        PrintLine(fnum, "DEFINITION FILES (if used)")
        PrintLine(fnum, "Window Size Def File", TAB(45), WindowFile)
        PrintLine(fnum, "Trim Parameter File", TAB(45), TrimFile)
        FileClose(fnum)

        If bSmoothBackFlopControl = True Or bSmoothSideFlopControl = True Then
            bSmoothFlopControl = True
            If bSmoothSideFlopControl = True Then PopulateFlopIndex("Side")
            If bSmoothBackFlopControl = True Then PopulateFlopIndex("Back")
        End If

        SaveFileDialog1 = Nothing
        'Default: set the output folder to that which the input file is in:
        'FnDualPlanOutBaseA = DirName(OpenFileDialog1.FileName)
        IterStop = 0
        Me.Close()
        MainForm.Refresh()
    End Sub

    Private Sub cmdItrPrcCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdItrPrcCancel.Click
        Me.Close()
        IterStop = 1
        MainForm.Refresh()
    End Sub

    Private Sub IterSteps_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        txtNShape.Text = Str(NShape)
        txtNSmooth.Text = Str(NSmooth)
        txtNFloat.Text = Str(NFloat)
        txthowmanymore.Text = Str(IterHowmanyMore)
        txtrewrite.Text = Str(NiterToRewrite)
        txtHalfLife.Text = Str(HalfLife)
        IterStop = 1
        bFileDefinedTrimParms = False 'default
    End Sub

    Private Sub cbShiftDPCycles_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'If cbShiftDPCycles.Checked = True Then cbExpandDPCycles.Checked = False
    End Sub


    Private Sub btnSchedShadPrice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSchedShadPrice.Click
        Dim OpenFileDialog1 As New OpenFileDialog
        Dim SolFile As String
        Dim OrigSolFileName As String
        Dim ShadPrFile As String

        With OpenFileDialog1
            .Title = "Select the Polygon Schedule File to Read"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        OrigSolFileName = OpenFileDialog1.FileName
        SolFile = DirName(OpenFileDialog1.FileName)
        '####Does not work for multiple subforests
        System.IO.File.Copy(OrigSolFileName, SolFile & "PolySched1.txt", True)

        For jsubfor As Integer = 1 To NSubFor
            DualPlanSubForests(jsubfor).PolySchedFile = SolFile & "PolySched" & jsubfor & ".txt"
        Next jsubfor
        DualPlan_Space.LoadPolySolFile(DualPlanSubForests(kaaset).PolySchedFile)

        With OpenFileDialog1
            .Title = "Select the Shadow Price File to Read"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        ShadPrFile = (OpenFileDialog1.FileName)
        DualPlan_Space.LoadShadowPriceFile(ShadPrFile)

    End Sub



    Private Sub cbReportByOrigSD_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbReportByOrigSD.CheckedChanged
        If cbReportByOrigSD.Checked = False Then cbReportByPredefSD.Checked = True
        If cbReportByOrigSD.Checked = True Then cbReportByPredefSD.Checked = False
    End Sub

    Private Sub cbReportByPredefSD_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbReportByPredefSD.CheckedChanged
        If cbReportByPredefSD.Checked = True Then cbReportByOrigSD.Checked = False
        If cbReportByPredefSD.Checked = False Then cbReportByOrigSD.Checked = True
    End Sub
    Public Sub LoadWindowBreakInfo(ByVal filename As String)
        Dim dummy As String
        Dim arr() As String
        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, filename, OpenMode.Input)
        NWindowBreaks = 0
        ReDim WindowSizeAtBreak(NWindowBreaks), WindowSizeFactorAtBreak(NWindowBreaks), WindowBreakStands(NWindowBreaks)
        WindowBreakStands(0) = 999999999 'really large number
        dummy = LineInput(fnum) 'header line
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Trim(dummy).Length = 0 Then Exit Do
            arr = Split(dummy, ",")
            NWindowBreaks = NWindowBreaks + 1
            ReDim Preserve WindowSizeAtBreak(NWindowBreaks), WindowSizeFactorAtBreak(NWindowBreaks), WindowBreakStands(NWindowBreaks)
            WindowBreakStands(NWindowBreaks) = CType(arr(0), Integer)
            WindowSizeAtBreak(NWindowBreaks) = CType(arr(1), Long)
            WindowSizeFactorAtBreak(NWindowBreaks) = CType(arr(2), Long)
            bIncreaseWindowSize = True
            bWindowBreaksFromFile = True
        Loop

        FileClose(fnum)
    End Sub

    Private Sub LoadTrimParameters(ByVal FileName As String)


        Dim dummy As String
        Dim arr() As String
        Dim fnum As Integer = FreeFile()

        'says you are using variable trim parameters
        bFileDefinedTrimParms = True

        FileOpen(fnum, FileName, OpenMode.Input)
        dummy = LineInput(fnum) 'header line
        dummy = LineInput(fnum)
        arr = Split(dummy, ",")
        NTrialSets = CType(Trim(arr(0)), Integer)
        NBreakSets = CType(Trim(arr(1)), Integer)
        MaxNStandSizeBreaks = CType(Trim(arr(2)), Integer)
        ReDim TrialSetBegTrial(NTrialSets), TrialSetEndTrial(NTrialSets), TrialTrimType(NTrialSets), TrialBreakSet(NTrialSets)

        dummy = LineInput(fnum) 'header
        For jts As Integer = 1 To NTrialSets
            dummy = LineInput(fnum)
            arr = Split(dummy, ",")
            TrialSetBegTrial(jts) = CType(Trim(arr(1)), Integer)
            TrialSetEndTrial(jts) = CType(Trim(arr(2)), Integer)
            TrialTrimType(jts) = Trim(arr(3))
            TrialBreakSet(jts) = CType(Trim(arr(4)), Integer)
        Next

        'break set information
        ReDim TrialBreakSetNBreaks(NBreakSets), TrialStandMinSize(NBreakSets, MaxNStandSizeBreaks), TrialStandMaxSize(NBreakSets, MaxNStandSizeBreaks), TrialNSched(NBreakSets, MaxNStandSizeBreaks), TrialNUnsched(NBreakSets, MaxNStandSizeBreaks)
        For jbs As Integer = 1 To NBreakSets
            dummy = LineInput(fnum) 'header
            dummy = LineInput(fnum)
            arr = Split(dummy, ",")
            TrialBreakSetNBreaks(jbs) = CType(Trim(arr(1)), Integer)
            dummy = LineInput(fnum)
            For jbreak As Integer = 1 To TrialBreakSetNBreaks(jbs)
                dummy = LineInput(fnum)
                arr = Split(dummy, ",")
                TrialStandMinSize(jbs, jbreak) = CType(Trim(arr(1)), Integer)
                TrialStandMaxSize(jbs, jbreak) = CType(Trim(arr(2)), Integer)
                TrialNSched(jbs, jbreak) = CType(Trim(arr(3)), Integer)
                TrialNUnsched(jbs, jbreak) = CType(Trim(arr(4)), Integer)
            Next
        Next

        FileClose(fnum)

        Label11.Enabled = False
        tbMxPresPerPoly.Enabled = False
        Label16.Enabled = False
        tbMaxPresChosen.Enabled = False
        rbOnlyFirstDPRx.Enabled = False
        rbHighestVal.Enabled = False
        rbClosestVal.Enabled = False
        tbMxPresUnchosen.Enabled = False
        Label13.Enabled = False


    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim OpenFileDialog1 As New OpenFileDialog


        With OpenFileDialog1
            .Title = "Select the Window Size Definition File"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        WindowFile = OpenFileDialog1.FileName
        LoadWindowBreakInfo(WindowFile)

    End Sub

    Private Sub rbOnlyFirstDPRx_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbOnlyFirstDPRx.CheckedChanged
        If rbOnlyFirstDPRx.Checked = False Then
            Label16.Enabled = True
            tbMaxPresChosen.Enabled = True
            cbKeepBestNS.Enabled = True
            Label13.Enabled = True
            tbMxPresUnchosen.Enabled = True
        End If
        If rbOnlyFirstDPRx.Checked = True Then
            Label16.Enabled = False
            tbMaxPresChosen.Enabled = False
            cbKeepBestNS.Enabled = False
            Label13.Enabled = False
            tbMxPresUnchosen.Enabled = False
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim OpenFileDialog1 As New OpenFileDialog


        With OpenFileDialog1
            .Title = "Select the Trim and Cycle Definition File"
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With

        TrimFile = OpenFileDialog1.FileName
        LoadTrimParameters(TrimFile)
    End Sub

    Private Sub cbGridBuilder_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbGridBuilder.CheckedChanged
        tbTrimSpeed.Enabled = cbGridBuilder.Checked
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If cbSmoothSideSFlopControl.Checked = True Or cbSmoothBackSFlopControl.Checked = True Then
            BackFlopFactor = CType(CStr(tbBackFlopFactor.Text), Single)
            FlopNBreaks = CType(CStr(tbMaxBreaks.Text), Single)
            MaxFlopAdjustFactor = CType(CStr(tbFlopConMaxMult.Text), Single)
            If cbSmoothSideSFlopControl.Checked = True Then PopulateFlopIndex("Side")
            If cbSmoothBackSFlopControl.Checked = True Then PopulateFlopIndex("Back")
        Else
            MsgBox("Choose The type of curve to use")
            Exit Sub
        End If
        Dim crv As New Curve
        crv.Show()
        crv = Nothing
    End Sub
    Private Sub PopulateFlopIndex(ByVal SType As String)
        'MaxFlopIndex = 40
        'If SType = "Side" Then MaxFlopIndex = SideFlopFactor
        'populate these sets 
        ReDim SpatSetAdjModIndex(NSpatSet, NTic)
        ReDim CondSetAdjModIndex(NCondSet, NTic)
        ReDim MSetAdjModIndex(NMSet, NTic)

        ReDim FlopFactorArray(FlopNBreaks)
        FlopFactorArray(FlopNBreaks / 2) = 1

        If SType = "Back" Then
            For jflop As Integer = FlopNBreaks / 2 + 1 To FlopNBreaks
                FlopFactorArray(jflop) = FlopFactorArray(jflop - 1) * BackFlopFactor
            Next
            For jflop As Integer = 0 To FlopNBreaks / 2 - 1
                FlopFactorArray(jflop) = 1 + (MaxFlopAdjustFactor - 1) * (1 - FlopFactorArray(FlopNBreaks - jflop))
            Next
        ElseIf SType = "Side" Then

            For jflop As Integer = FlopNBreaks / 2 + 1 To FlopNBreaks
                FlopFactorArray(jflop) = Math.Abs(((jflop - FlopNBreaks / 2) / (FlopNBreaks / 2)) ^ 2 - 1)
            Next
            For jflop As Integer = 0 To FlopNBreaks / 2 - 1
                FlopFactorArray(jflop) = 1 + (MaxFlopAdjustFactor - 1) * (1 - FlopFactorArray(FlopNBreaks - jflop))
            Next
        End If

        For jmset As Integer = 1 To NMSet
            For jtic As Integer = 1 To NTic
                MSetAdjModIndex(jmset, jtic) = FlopNBreaks / 2
            Next
        Next
        For jCondset As Integer = 1 To NCondSet
            For jtic As Integer = 1 To NTic
                CondSetAdjModIndex(jCondset, jtic) = FlopNBreaks / 2
            Next
        Next
        For jspatset As Integer = 1 To NSpatSet
            For jtic As Integer = 1 To NTic
                SpatSetAdjModIndex(jspatset, jtic) = FlopNBreaks / 2
            Next
        Next

    End Sub

    Private Sub cbSmoothBackSFlopControl_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSmoothBackSFlopControl.CheckedChanged
        If cbSmoothBackSFlopControl.Checked = True Then cbSmoothSideSFlopControl.Checked = False
    End Sub

    Private Sub cbSmoothSideSFlopControl_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSmoothSideSFlopControl.CheckedChanged
        If cbSmoothSideSFlopControl.Checked = True Then cbSmoothBackSFlopControl.Checked = False
    End Sub

End Class