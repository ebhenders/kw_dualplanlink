DualLinkKW_wGridTrim program is specific to the KW problem.

This is the master DualPlan portion of the KW problem. It will include solution techniques other than 
the Dynamic programming - including the regular DualPlan solution technique as well as options for one-opt 
and perhapse also 2-opt + moves. It should also include the Model 1 structure 
of the Kw problem in the DualPlan SCHEDULE routime.

This project is designed to eliminate the dynamic arrays (structures) with the hope of generating faster more 
efficient solutions.

This particular version was developed for Paper 2 in which various trimming strategies are employed to
try to make sure the global-optimal stand-level prescription is included for all stands. It works off the 
grid concept whereby stand-level prescriptions are added by evaluating all stands within a predefined grid - 
and includes the prescription for all stands in that grid if it meets a minimum trim criterion.

EBH 11/1/2012