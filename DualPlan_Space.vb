Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module DualPlan_Space
    'dual plan iterations plus valuation of the spatial value of the chosen prescriptions

    '7/16/2003 used to create batch process for linking to trimmer and Dpspace

    'Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Integer, ByVal bInheritHandle As Integer, ByVal dwProcessId As Integer) As Integer

    'Private Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Integer, ByRef lpExitCode As Integer) As Integer

    'Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Integer) As Integer

    'Private Const PROCESS_QUERY_INFORMATION As Short = &H400S
    'Private Const STATUS_PENDING As Integer = &H103


    'Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Integer)
    'Public Class clDualPlan
    'When integrated with DPSpace, this program will:
    '1. Read the DPSpace solution
    '2. Tally the Market Set, Condition Set and Spatial Set metrics associated with that solution
    '3. Adjust Set Prices (shadow prices) accordingly (in the usual fashion)
    '4. Send the new prices on to trimmer

#Region "Declarations"
    Public DualPlanInputFileName As String

    Public TOT_AASELF_NPV() As Double 'track the actual market PNV + value added from shadow price valuation of spatial and condition set constraints; store it by saved iterations
    Public TOT_AA_SPAT_BUFFER_NPV() As Double 'just the value of the core area in buffer areas (2+Izones); not 1-way zones within stands
    Public TOT_SPAT_NPV() As Double 'track total spatial NPV across the forest - includes all core area, in-stand in buffers
    Public TOT_MARKET_NPV() As Double 'track just the market values of the NPV - this is the true $ figure of interest
    Public TOT_EXCESSDEVVAL As Double 'financial value of excesses - added to financial NPV to estimate the true primal OF Value
    Public TOT_VIOLDEVVAL As Double 'financial value of violations - subtracted from financial NPV to estimate the true primal OF value
    'Public TOT_DUAL_NPV() As Double
    Public VALUE_DEVIATION() As Double
    Public SubForAspatNPV() As Double 'for each subforest, sum the total NPV w/o spatial values
    Public AASubForSpatNPV() As Double 'spatial NPV calculated from AA scheduling routine
    Public SubForSpatNPV() As Double 'spatial NPV calculated from tallying resulting schedule actual influence zone area and values (in SpatialTally)
    Public SubForMarketNPV() As Double 'for each subforest, track the market values of NPV only
    Public TicISpaceLevel() As Double 'by period, the 2+ Izone area in interior space
    Public TicISpaceValue() As Double 'by period, the 2+ Izone value
    Public SubForSDAspatNPV(,) As Double 'for each subforest, sum the total NPV w/o spatial values
    'Public SubForSDSpatNPV(,) As Double 'spatial NPV calculated from tallying resulting schedule actual influence zone area and values (in SpatialTally)
    'Public SubForSDMarketNPV(,) As Double 'for each subforest, track the market values of NPV only
    Public SubForSDSpatNPV(,) As Double
    Public StaticSubForSDAspatNPV(,) As Double
    Public StaticAASubForSDSpatNPV(,) As Double

    Public pnvfilnum As Integer
    Public subdivpnvfilnum As Integer
    Public Staticsubdivpnvfilnum As Integer
    'Public StaticsubdivINFOfilnum As Integer
    Public subdivinfofilnum As Integer
    Public setdevfilnum As Integer 'to track deviations by iteration
    Public totdevfilnum As Integer 'to track deviations by iteration - totals by set type

    'NOTE: MxAgeFlowRg is used to calculate MxTIC, which should be 3X NTIC
    'Public Const MxTicRegenAA As Short = 59
    'Public Const MxAgeRegenRg As Short = 59
    'Public Const MxTicFlowAA As Short = 59
    Public Const MxAgeFlowRg As Short = 59
    Public Const NMapColors As Integer = 6

    Public PolySchedFileExt As String 'file extension of the most current poly schedule files
    Public Atts() As String 'for the Stand Atts form

    Public LinkFileA As String
    Public NSubFor As Short

    'SUBDIVISION INFORMATION
    Public NSubDiv As Integer 'number of subdivisions in the subforest
    Public MaxSubForSD As Integer 'maximum number of subdivisions in any subforest
    Public MaxStaticSubForSD As Integer
    Public NSubForSD() As Integer 'number of subdivisions in the subforest
    Public NStaticSubForSD() As Integer
    Public AASubDiv() As Integer 'dim by  SubDiv - indicates which subdivision the AA is in
    Public SubForStaticAASubDiv(,) As Integer 'which subdiv AA is in- can be used to track consistent subdivisions in evolving problem
    Public SubForSubDivWindowCount(,) As Integer 'by subforest, subdivision - actually a window count
    Public bSubForSubDivSinglePass(,) As Boolean
    Public SubForSubDivNStands(,) As Integer 'by subforest, subdivision
    'Public SubDivWindowCount() As Integer 'track whether your DP cycle resulted in an exact solution
    Public StaticSubForSubDivNStands(,) As Integer
    Public BaseSDInfoFileName As String
    Public BaseSDRXLUTFileName As String

    Public FnDualPlanInBaseA As String
    Public FnDualPlanOutBaseA As String
    Public LastDualPlanIterNum As Integer

    Public FnDPspaceInBaseA As String
    Public FnDPspaceOutBaseA As String
    Public LastDPspaceIterNum As Short

    Public FnDPformInBaseA As String
    Public FnDPformOutBaseA As String

    Public FnTrimmerInBaseA As String
    Public FnTrimmeroutBaseA As String
    Public LastTrimmerIterNum As Long

    Public FnBinSpatSeriesYesNoA As String

    Public SubTimeStartS As Single
    Public SubTimeEndS As Single
    Public DPStartTime As Single
    Public DPEndTime As Single

    'subdivisions stuff in the link file
    Public FnSubdivisionsInputFile As String 'subdivisions input file
    Public BaseNameHexDataOutfilesA As String '        BaseName_Hexdata_OutfilesA()
    Public BaseNameAAnaltAAareaInfilesA As String '        BaseName_AAnaltArea_InfilesA()
    Public BaseNameOutFilesA As String '        BaseName_OutFilesA()

    Public isFnBinSpatSeriesYesNoBin As Byte
    Public arePolyDataFilesBin As Byte
    Public arePresDataNPVFilesBin As Byte
    Public arePresDataSpatFilesBin As Byte


    'ITERATION CONTROLS
    Public HowMany As Integer
    Public HowmanytoRewrite As Short
    Public InterestRate As Single
    Public IterationBase As Integer
    Public IterNPrAdjust As Short
    Public IterationNumber As Integer
    Public IterHowmanyMore As Integer
    Public IterMethod As String
    Public bNeedSpatialIterations As Boolean 'if there are no spatial constraints, don't need to do spatial iterations!
    Public OneOptStop As Single = 0 'percentage of reschedules before loop stops - prevents full SpatialScheduleRefine iterations to correct very few schedules
    'Public IterPerFileSave As Short
    Public IterStop As Short
    Public IterTimeToStop As Short
    Public NAdjSetIters As Integer 'number of adjustment iterations in each adjustment set
    Public NSchSetIters As Integer 'number of schedule iterations in each schedule set
    Public HowmanyBeforeRunDP As Integer
    'Public DlPlItersWithTrim As Integer 'ebh
    Public DlPlItersNOTrim As Integer 'ebh
    Public DPSpaceAfterDlPlIters As Integer 'ebh
    Public AdjSetIters() As String 'track adjustment type for each iteration in the set
    Public AdjSetCounter As Integer 'counting the index of the iteration for the set
    Public SchSetIters() As String 'track schedule type for each iteration in the set
    Public SchSetCounter As Integer 'count the index of the schedule iteration of the set
    Public bOneOptAfterDP As Boolean 'whether to do the one-opt after a DP 
    Public bRandomOneOpt As Boolean
    Public bGridRxBuilder As Boolean 'whether you are using pre-defined grids to build prescriptions
    Public GridFile As String
    Public DollarPerAcreTol As Single
    Public ItersBetweenTrims As Integer
    Public kitersbetweentrims As Integer

    Public DeviationWriteFile As Single 'if total deviations is less than X, write the file outputs. May be a good solution.
    Public bDeviationAbsValue As Boolean 'if true, you are adding the ordinal level of the absolute value devaiations; if false, you are adding the percentage of the deviation values
    Public UseMSetViolations As Integer
    Public UseCSetViolations As Integer
    Public UseSSetViolations As Integer 'controls file-writes below a certain deviation threshhold - if checked, spatial deviations will be included in the check
    Public UseMSetExcesses As Integer
    Public UseCSetExcesses As Integer
    Public UseSSetExcesses As Integer
    Public TotalViolationDev As Double 'deviations in violation of constraints (infeasibilties)
    Public TotalExcessDev As Double 'deviations in excess of constriants (suboptimalities?)
    Public TotalDeviations As Double 'sum of excess and violation

    'iteration controls for DP cycles
    Public NumDPCycles As Integer 'number of DP Cycles per DP iteration to conduct
    'Public SpatialShift As Integer 'number of periods to allow prescriptions to shift spatial value from chosen Rx - multiple of 2
    Public NumMultiWindowPasses As Integer 'collect the number of DP passes that include more than 1 window - used to stop the cycler
    Public NumMultiScheduleStands As Integer 'see how many stands have >1 schedule available in the DP
    Public bDPCycles As Boolean 'whether you are using DP cycles or not
    Public sCycleType As String 'the type of DP Cycle you are doing - EITHER shift or expand
    'Public bCycling As Boolean 'whether you are calling the DPSpace scheduler in iteration or cycling mode

    Public KeepBestNS As Boolean
    'Public bHighestValue As Boolean 'when filtering prescriptions for chosen stands in cycling, are you using higest or closest values?
    'Public bClosestValue As Boolean
    'Public bOnlyFirstDPRx As Boolean
    Public TrimTolerance As Double 'if the max possible prescription value is lower than (1+TrimTolerance)*NPVLbnd, then trim it
    'Public MxPresPerPoly As Integer 'used as a trimmer filter - only sends this number of prescriptions to the DP
    'Public DPRefresh As Integer 'how many DP iterations between a full DP refresh - otherwise can use last schedule to send right to the cycler
    'Public ItersToRefresh As Integer 'how many iterations left before a DP refresh
    Public MxPresPerPoly As Integer
    Public MxPresUnchosen As Integer 'used as trimmer filter for DP Cycles
    Public MaxPresChosen As Integer 'for polys with a spatial schedule, can add back in a few if you want
    Public dMaxPresChosen As Double 'for using a decay factor
    Public CycleHalfLife As Integer 'pares down prescriptions allocated to scheduled stands
    Public kSchedLockCycles As Integer 'if a stand's Rx does not change after X cycles, lock it to this chosen prescription
    Public kUnschedLockCycles As Integer
    Public bLockExactSDSol As Boolean 'lock the stand's Rx if it solves the subdivision on 1 pass - pertains to cycles
    Public bReportByOrigSD As Boolean 'whether to write out subdivision reports by the original subdivision in the first DP (not the cycle)
    Public bReportBySD As Boolean '
    Public bReportByPredefSD As Boolean
    'Public klock As Integer 'index of the PolySolution cycle
    Public SchedPolySolution(,) As Long 'solution for the polygon - by polygon (AA), LockCycles
    Public UnschedPolySolution(,) As Long
    Public AllSchedPolySolution(,) As Long
    Public AllUnschedPolySolution(,) As Long
    Public CycleDecayFactor As Double
    Public WeightScheme As String 'used for trimmer DP Cycles
    Public NZoneSubFor As Integer
    Public MXZoneDim As Integer
    Public FootprintPct As Single 'percentage of the footprint to count in valuing prescriptions - <1
    Public bSmoothFlopControl As Boolean
    Public bSmoothBackFlopControl As Boolean
    Public bSmoothSideFlopControl As Boolean
    'Public bOneWayFlopControl As Boolean 'whether to use one-way flop control - "True" means you get stuck once you define small adjustments
    Public LastIterType As String
    Public SpatSetLastIterDev(,) As Single 'Signed magnitude of last deviation - Spatial set - by set, tic
    Public CondSetLastIterDev(,) As Single
    Public MSetLastIterDev(,) As Single
    'Public SpatSetLastIterAdj(,) As Single 'value of the last adjustment ($) spatial set - by set, tic
    'Public CondSetLastIterAdj(,) As Single
    'Public MSetLastIterAdj(,) As Single
    Public SpatSetAdjModIndex(,) As Single 'if you are continually flopping, the modification factor gets smaller and smaller, if you are consistently under or over, it gets larger and larger
    Public CondSetAdjModIndex(,) As Single
    Public MSetAdjModIndex(,) As Single
    Public SpatSetFlopped(,) As Integer 'if you've flopped, then true- used to break ties when you have exactly the same deviation for an iteration
    Public CondSetFlopped(,) As Integer
    Public MSetFlopped(,) As Integer
    'Public FlopFactor As Single 'if you are continually flopping, reduce the magnitude of the price adjustment
    'Public FlopFactorArray() As Single
    'Public MaxFlopIndex As Integer = 40
    Public BackFlopFactor As Single 'if you are continually flopping, reduce the magnitude of the price adjustment
    'Public SideFlopFactor As Single
    Public FlopFactorArray() As Single
    Public FlopNBreaks As Integer '= 40
    Public MaxFlopAdjustFactor As Single

    'for Directional Search in DP
    Public bDirectionSearch As Boolean
    Public NDPDirections As Integer
    Public OriginalDirection As Integer 'defined in the input file
    'Public DirectionsSearched() As Integer 'if searching less than 8 directions, store randomly selected directions so as not to repeat
    Public DirectionsToSearch() As Integer
    Public MaxPresPerPolyDirection As Integer
    'FOR WINDOW SIZE MANIPULATIONS IN DP
    Public bIncreaseWindowSize As Boolean
    Public OriginalWindowSize As Long
    Public WindowSizeFactor As Single
    Public WindowMaxSize As Long
    Public bWindowBreaksFromFile As Boolean = False
    Public NWindowBreaks As Integer
    Public WindowSizeAtBreak() As Long
    Public WindowSizeFactorAtBreak() As Long
    Public WindowBreakStands() As Integer


    Public DualPlanSubForests() As SubForestData
    Public MaxSubforAAs As Long 'most number of analysis areas in a subforest

    Public SubForestNPV As Double

    'TRIMMER STUFF
    Dim TrimRx() As TrimFileData
    Public EconRxIn(,) As Byte 'by stand, prescription
    Public NPValMxSpat(,) As Single 'by stand, prescription
    Public MaxNSRx() As Integer 'prescription index of best non-spatial prescription
    Public gLockedRxSD(,) As Long 'locked Rx due to Subdivision exact scheduling - global
    Public bgLockedAASD(,) As Boolean
    '  - Trimmer stuff used for Trial/Cycle parameters
    Public bFileDefinedTrimParms As Boolean
    Public NTrialSets As Integer
    Public NBreakSets As Integer
    Public MaxNStandSizeBreaks As Integer
    Public TrialSetBegTrial() As Integer
    Public TrialSetEndTrial() As Integer
    Public TrialTrimType() As String
    Public TrialBreakSet() As Integer
    Public TrialBreakSetNBreaks() As Integer
    Public TrialStandMinSize(,) As Integer
    Public TrialStandMaxSize(,) As Integer
    Public TrialNSched(,) As Integer
    Public TrialNUnsched(,) As Integer
    Public TrimSpeed As Integer 'number of Rx's to trim off per poly before re-evaluating economics. Default = 1

    Public DualPlanAAPoly_polyid() As Integer
    Public DualPlanAAPoly_PassToSpace(,) As Boolean  'true means keep, false means it gets filtered...
    Public DualPlanAAPoly_ISpaceSelf() As Single ' the area of the 1-way influence zone within the borders of this stand
    Public DualPlanAAPoly_NPVallMx() As Double 'The maximum NPVValMxSpat - dimensioned by stand
    Public DualPlanAAPoly_NPVSelf(,) As Double 'by prescription, sum of NPVAspatial and the guaranteed interior value of the stand - Includes infinite series of those spatial flows...PER ACRE VALUE calculated
    Public DualPlanAAPoly_NPValLbnd() As Double 'aka NPVMin, or largest NPVSelf 
    Public DualPlanAAPoly_EstISpacePct() As Double 'estimated percentage of the 'edge' area that is in ispace for the poly - a function of npvlbnd and npvmax
    'Public DualPlanAAPoly_StandID() As Long 'Stand ID of the stand in question
    Public DualPlanAAPoly_NRxIn() As Integer 'number of non-trimmed prescriptions of the polygon
    Public DualPlanAAPoly_PolyRxSpace() As Byte '0/1 based on whether the stand has a non-trimmed spatial prescription
    Public DualPlanAAPoly_NIZones() As Integer 'number of IZones this polygon is included in
    Public DualPlanAAPoly_IZones(,) As Integer 'Izone IDs of what influence zones the stand is a part of

    Public CondSet() As SetData
    Public MSet() As SetData
    Public SpatSet() As SetData

    'INFLUENCE ZONE STUFF
    Public MxZonesForAA As Integer 'the maximum number of influence zones any AA is a part of

    'SPATIAL TYPES AND DEFINITIONS
    Public MinSpatSize() As Single 'minimum patch size by spatial definition
    Public NSpaceDef As Integer 'number of spatial definitions described
    Public SpatialSeries() As SpatialSeriesData
    Public ZoneAAlist(,) As Integer
    Public ZoneAAlistAreaS(,) As Single 'area of the zone within each AA. Indexed by zone, max AA. Look at ZoneAAList for AA ID
    Public Zonedim() As Integer
    Public ZoneArea() As Single 'area of the influence zone
    Public ZoneRxFilter As Long 'if the total evaluated prescriptions of an influence zone is larger than this, trim the zone when passing info. to DPSPace

    Public AAnDataSet As Short
    Public CondTypeNDef As Short
    Public SpatTypeNDef As Short

    Public DemandMxBreakPoint As Short
    Public DemandNEquation As Short

    'if you want to vary the costs based on size of the unit 
    Public EntryAdjMxBreakPoint As Short
    Public EntryAdjNEquation As Short

    Public EntryCost As Single

    Public EndPriceNWghtSet As Short
    Public FnMainInput As String
    Public FnRgData As String
    Public FnLabels As String
    Public FnAAFileList As String
    Public FnPolySchedFileDefName As String
    Public FnPolySchedFileBase As String 'if there is a poly schedule file on-hand to load with the DualPlan input file...

    Dim ISpaceDefFile As String
    Dim SpatialSeriesFile As String

    Public kaaset As Short
    Public kaasetdirection As Short
    Dim kDPdirection As Integer
    Public kadjset As Short
    Public knext As Short
    Public knextbase As Short
    Public ksave As Short
    Public ksaveBase As Short
    Public kEventRun As String

    Public MTypeNDef As Short
    Public MTypeSiteConvCost As Short
    Public MTypeEntryCost As Short
    Public MxAgeClass As Short
    Public MxStandRx As Integer = 0 'count the maximum number of Rx assigned to any one AA

    '  7/10/03 added to change DPlink rules
    Public MxTic As Short

    Public NCondSet As Short
    Public NFloat As Short
    Public NiterToRewrite As Short
    Public NIterToSave As Short

    'Spatial constraints
    Public NSpatSet As Short
    Public NSpaceType As Short

    Public Ncoversite As Short
    Public NMapLayer As Short
    Public NMapArea As Short
    Public NMLocation As Short
    Public NMSet As Short
    Public NMType As Short
    Public NShape As Short
    Public NSmooth As Short
    Public NTic As Short
    Public NTic1 As Short
    'Public MxTic As Integer
    Public NYearPerTic As Short

    Public NRgBioType As Short
    Public NRgPres As Integer
    Public NRgFlow As Integer

    Public OneTicDisc As Single

    Public PriceAdjMxBreakPoint As Short
    Public PriceAdjNSet As Short
    Public TotalMSetDef As Short
    Public TotalCondSetDef As Short
    Public WriteCondSet As Short
    Public WritecoversiteAge As Short
    Public WriteMSet As Short
    Public WriteMType As Short
    Public WriteSpatSet As Short
    Public WriteSpatType As Short

    ''Condition set stuff...
    Public CondDisc() As Single

    'CONDITION TYPE METRICS
    Public coversiteLabel() As String
    Public CondTypeFlow(,,,) As Single 'by coversite, age, map layer, tic
    Public CondTypePr(,,,) As Single 'by coversite, age, map layer, tic

    ''Spatial set stuff....
    Public SpatTypeLabel() As String 'label of the spatial type
    Public SpatTypeFlow(,) As Double 'by type, tic  - coversite, age, and map layer are inherent in the type definition by coversite, age, map layer, tic
    Public OneWaySpatTypeFlow(,) As Double '1-way influence zone spatial type flow
    Public SpatTypePr(,) As Single 'by type, tic

    '***END SPATIAL STUFF...

    Public DemandNPoint() As Integer
    Public DemandPrice(,) As Single
    Public DemandQuan(,) As Single

    'for entry cost adjustment equations
    Public EntryAdjNPoint() As Integer
    Public EntryAdjNDef() As Integer 'number of coversite/age/cost definitions
    'Public EntryAdjMType() As Integer 'the market type the adjustment parameters apply to
    Public EntryAdjAcres(,) As Single
    Public EntryAdjFactor(,) As Single
    Public EntryAdjkCoverSiteBeg(,) As Integer
    Public EntryAdjkCoverSiteEnd(,) As Integer
    Public EntryAdjkagebeg(,) As Integer
    Public EntryAdjkageend(,) As Integer
    Public MTypeEntryCostBeg(,) As Integer
    Public MTypeEntryCostEnd(,) As Integer

    Public EndPriceWght(,) As Single
    Public EntryCostTicThisAA() As Single
    Public EntryCostTic(,) As Single

    Public FloatNBreak() As Integer
    Public FloatPercentDev(,) As Single
    Public FloatPriceAdj(,) As Single
    Public FloatStopPercent() As Single

    Public AdjIterType() As String 'price adjustment type associated with the iteration
    Public SchIterType() As String 'schedule routine type for the iteration - DP, vs. schedule with trim, vs. schedule w/o trim

    Public LandOpt(,) As Integer
    Public LandRgArea() As Single
    Public LandVal(,) As Single

    Public MapLayerNColor() As Integer
    Public MapLayerColor() As Integer

    ''Market set stuff...
    Public MDisc() As Single
    Public MapAreaID(,) As Integer
    Public MapAreaLabel() As String
    Public MLocationLabel() As String

    Public MTypeFlow(,,) As Double
    Public MTypeLabel() As String
    Public MtypePr(,,) As Single

    'Regen prescription stuff...
    Public CondLandValue_Calculated(,,) As Byte 'As LandValData 'condition set price flows - by maparea, coversite
    Public CondLandValue_LandVal(,,) As Double
    Public SpatLandValue_Calculated(,,) As Byte
    Public SpatLandValue_LandVal(,,) As Double 'As LandValData 'spatial type flows - by spatial type, coversite
    Public MktLandValue_Calculated(,,) As Byte
    Public MktLandValue_LandVal(,,) As Double ' As LandValData 'market type condition flows - by market location, coversite
    'Public RgFlow() As FlowData

    Public RgBioType() As RgLandData
    'Public RgPres() As RgPresData
    Public RgBioTypeLabel() As String

    Public SEVFactor() As Single

    Public ShapeNBreak() As Integer
    Public ShapePercentDev(,) As Single
    Public ShapePriceAdj(,) As Single
    Public ShapeWght(,) As Single
    Public ShapeWghtTot() As Single
    Public SmoothNBreak() As Integer
    Public SmoothPercentDev(,) As Single
    Public SmoothPriceAdj(,) As Single

    Dim Timberfactor() As Single

    'MxTicRegenAA is the maximum period of first harvest for existing stands
    'MxAgeRegenRg is the maximum rotation length considered for regenerated stands

    ' data from DPspace
    Public AAdpSpaceFN() As String
    Public AAdpFilesize() As Integer

    Public NAADpSpace() As Integer

    Public EXEDirectory As String 'where you are storing Subdivisions, DPForm, DPSpace executables called by the Shell command

    Public polyidDpspace As Integer 'wei
    Public polyidDualplan As Integer 'wei
    Public HalfLife As Short

    Public ShadowPriceDecayfactor As Single

    Dim SPrUpLim() As Single
    Dim SPrLowLim() As Single


    'Dim PolySolRx_PolyInd() As Short 'store DPSpace solution polygon index
    Public DPSpace_PolyInd() As Short 'store DPSpace solution polygon index
    Public PolySolRx_ChosenRxInd() As Long 'the DualPlan prescription index - according to the DualPlan input file
    Public PolySolRx_ChosenRxIndSave(,) As Long 'DualPlan prescription index from the last iteration - by kaaset, aa
    Public PolySolRx_ChosenRxIndBest(,) As Long 'if doing cycling, save the best for reporting as chosen
    Public PolySolRx_BestSDSol(,) As Double 'best NPV per subdivision (static) - by subfor, subdivision
    Dim PolySolRx_AspatNPV() As Double 'the aspatial NPV of the chosen prescription for that stand...
    Dim PolySolRx_SpatNPV() As Double ' the NPV of the spatial shadow prices * acreage
    Dim PolySolRx_MarketNPV() As Double 'just the market portion of the stand's NPV
    'Dim PolySolRx_DualExcessNPV() As Double 'beyond the plan horizon, the shadow-price added value of the objective function

    Public AA_polyid() As Integer
    Public AA_area() As Single
    Public AA_Coversite() As Byte
    Public AA_Ageclass() As Byte
    Public AA_mloc() As Byte
    Public AA_PresArray(,) As Integer
    Public AA_FirstRgTic() As Byte
    Public AA_MapLayerColor(,) As Byte
    Public AA_NPres() As Integer 'number of prescriptions for each AA
    Public AA_NPolys As Integer 'number of aas/polygons

    'Grid stuff
    Public NGrids As Integer
    Public MaxStandsInGrid As Integer
    Public GridStands(,) As Integer 'by ngrids, maxstandsingrid
    Public NStandsInGrid() As Integer 'number of stands in this grid - by NGrids


#Region "Structures"

    'EBH 1/29/2010: THIS SHOULD BE OK - ONLY REFERENCED ONCE; SMALL ARRAYS - NOT CHANGED FROM STRUCTURE FORMAT

    Structure SubForestData
        Dim NPolys As Integer
        Dim NRx As Integer
        Dim NFlow As Integer
        Dim PolyFile As String
        Dim RxFile As String
        Dim RxSpatialIndFile As String
        Dim FlowFile As String
        Dim PolyRxFile As String
        Dim RgDataFile As String
        Dim HexStandsFile As String
        Dim IZonesFile As String
        Dim LatestTrimDataFile As String 'stored dynamically after the trimmer is done writing...
        'Dim PolySchedOutFile As String 'listing of polygon ID and Rx Index by subforest
        'Dim PolySchedInFile As String 'when first loading the polysched file, may have a different name...
        'Dim PolySchedFileLoad As String 'there's a working one and there's a load one
        Dim PolySchedFileWrite As String  'when writing out after X iterations, include a polysched file associated with that iteration
        Dim PolySchedFileWriteLast As String 'name of previous polyschedfilewrite
        Dim PolySchedFile As String 'the working polysched file that is overwritten with each iteration
    End Structure

    'Structure TrimPolyData
    '    Dim polyid As Integer
    '    Dim PassToSpace() As Boolean  'true means keep, false means it gets filtered...
    '    Dim ISpaceSelf As Single ' the area of the 1-way influence zone within the borders of this stand
    '    'Dim NPVAspat() As Double 'aspatial NPV
    '    Dim NPVallMx As Double 'The maximum NPVValMxSpat - dimensioned by stand
    '    'Dim NPValMxSpat() As Double 'tallies up the value of NPVSelf + the additional value if all stands are managed to max ISpace in the current stand
    '    Dim NPVSelf() As Double 'by prescription, sum of NPVAspatial and the guaranteed interior value of the stand - Includes infinite series of those spatial flows...
    '    Dim NPValLbnd As Double 'aka NPVMin, or largest NPVSelf 
    '    Dim EstISpacePct As Double 'estimated percentage of the 'edge' area that is in ispace for the poly - a function of npvlbnd and npvmax
    '    'Dim EdgeSpaceArea As Double 'area of the stand that is in more than 1-way influence zones....around the edge of the core
    '    Dim StandID As Long 'Stand ID of the stand in question
    '    'Dim RxIn() As Byte '0/1 based on whether the Rx for this poly has been trimmed or not
    '    Dim NRxIn As Integer 'number of non-trimmed prescriptions of the polygon
    '    Dim PolyRxSpace As Byte '0/1 based on whether the stand has a non-trimmed spatial prescription
    '    Dim IZones() As Integer 'Izone IDs of what influence zones the stand is a part of
    '    'Dim SpatNPVZone() As Integer 'the spatial NPV of each specific stand-side influence zone in the polygon

    '    Sub Initialize(ByVal NPres As Integer, ByVal NSpaceDefs As Integer)

    '        ReDim PassToSpace(NPres) 'default appears to be false...
    '        For j As Integer = 1 To NPres
    '            PassToSpace(j) = True
    '        Next
    '        'ReDim NPVAspat(NPres)
    '        ReDim NPVSelf(NPres)
    '        'ReDim NPValMxSpat(NPres)
    '        ISpaceSelf = 0
    '        'EdgeSpaceArea = 0
    '        'ReDim RxIn(NPres)  '0/1 based on whether the Rx for this poly has been trimmed or not
    '        ReDim IZones(0) 'dummy holder for now
    '        'ReDim SpatNPVZone(0)
    '        EstISpacePct = 0

    '    End Sub
    '    ' Dim npres As Short
    'End Structure

    Public AApres_FirstFlow() As Integer
    'Dim LastFlow As Integer
    Public AApres_PresType() As Integer
    Public AApres_RgTic() As Integer 'first period to tally flows of the regen Rx
    Public AApres_RgRotLen() As Integer 'rotation length of the regen Rx
    Public AApres_RgFirstFlow() As Integer 'regen first flow
    Public AApres_RgLastFlow() As Integer 'regen last flow
    'Dim NRgAlt As Integer
    'Dim BAadjust As Short
    'Public AApres_RgBioType(,) As Short
    'Public AApres_RgCost(,) As Short
    Public AApres_Age(,) As Integer
    Public AApres_Coversite(,) As Integer
    Public AApres_SpaceType() As Integer 'the type of space the prescription creates
    Public AApres_ISpaceInd() As Short 'ispace index to array of spatial flows
    Public AApres_QBuffInd() As Short
    Public AApres_EdgeInd() As Short
    Public AApres_NPres As Integer 'dimension of the arrays - number of prescriptions


    Public AAflow_tic() As Byte
    Public AAflow_type() As Byte
    Public AAflow_quan() As Double
    Public AAflow_Nflow As Integer

    Public Rgflow_tic() As Byte
    Public Rgflow_type() As Byte
    Public Rgflow_quan() As Double
    Public Rgflow_Nflow As Integer

    '#####WARNING - THESE ARRAYS ARE NOT DIMENSIONED (1/29/2010)
    Public RgPres_PresType() As Byte
    Public RgPres_FirstFlow() As Integer
    Public RgPres_LastFlow() As Integer
    Public RgPres_Rotlength() As Byte
    Public RgPres_Age(,) As Byte
    Public RgPres_Coversite(,) As Byte
    Public RgPres_NRgPres As Integer
    '    'UPGRADE_TODO: "Initialize" must be called to initialize instances of this structure. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
    '    Public Sub Initialize()
    '        ReDim Age(MxAgeRegenRg)
    '        ReDim Coversite(MxAgeRegenRg)
    '    End Sub


    Structure RgLandData
        Dim firstpres As Integer
        Dim lastpres As Integer
    End Structure

    Public CondTypePrDef_TypeInd() As Short
    Public CondTypePrDef_CoverSiteBeg() As Short
    Public CondTypePrDef_CoverSiteEnd() As Short
    Public CondTypePrDef_AgeBeg() As Short
    Public CondTypePrDef_AgeEnd() As Short
    Public CondTypePrDef_MapLayer() As Short
    Public CondTypePrDef_LayerColorBeg() As Short
    Public CondTypePrDef_LayerColorEnd() As Short
    Public CondTypePrDef_TicBeg() As Short
    Public CondTypePrDef_TicEnd() As Short
    Public CondTypePrDef_price() As Single
    Public CondTypePrDef_Nunits() As Single


    Public SpatTypePrDef_TypeBeg() As Short
    Public SpatTypePrDef_TypeEnd() As Short
    'Public SpatTypePrDef_MLocBeg() As Short
    'Public SpatTypePrDef_MLocEnd() As Short
    Public SpatTypePrDef_TicBeg() As Short
    Public SpatTypePrDef_TicEnd() As Short
    Public SpatTypePrDef_price() As Single
    Public SpatTypePrDef_Nunits() As Single

    Public MTypePrDef_TypeBeg() As Short
    Public MTypePrDef_TypeEnd() As Short
    Public MTypePrDef_MLocBeg() As Short
    Public MTypePrDef_MLocEnd() As Short
    Public MTypePrDef_TicBeg() As Short
    Public MTypePrDef_TicEnd() As Short
    Public MTypePrDef_price() As Single
    Public MTypePrDef_Nunits() As Single

    Structure SetData
        Dim SetType As String 'can be market, condition, spatial
        Dim nSetDefs As Integer ' set definition lines

        '------------------------------
        'the next few lines replace the  CondSet data structure
        Dim kMapLayer() As Integer
        '  Dim kset() As Short
        Dim kTypeBeg() As Integer
        Dim kTypeEnd() As Integer
        Dim kLocBeg() As Integer 'may only be used for market type sets
        Dim kLocEnd() As Integer 'may only be used for market type sets
        Dim kagebeg() As Integer
        Dim kageend() As Integer
        Dim kLayerColorBeg() As Integer
        Dim kLayerColorEnd() As Integer
        Dim convfact() As Single
        Dim convprob() As Single
        '------------------------------------

        'Dim Disc() As Single 'discount rate is just one array...same for every set
        Dim SetBaseQuan As Single
        Dim SetConstraintOption As Integer
        Dim SetDemandTableID() As Object
        Dim SetDev(,) As Single
        Dim SetDevMx As Single
        Dim SetDevMn As Single
        Dim SetEndPrWghtSet As Single
        Dim SetFlow(,) As Double
        Dim SetFlowMn() As Single
        Dim SetFlowMx() As Single
        Dim SetFlowMn1 As Single
        Dim SetFlowMx1 As Single
        Dim SetLabel As String
        Dim SetPr(,) As Single 'by iterations to save, tic
        Dim setPrAdjSet As Integer
        Dim SetTarget() As Single
        Sub Initialize(ByVal NDef As Integer, ByVal IterToSave As Integer, ByVal NTic As Integer)
            ReDim kMapLayer(NDef)
            ReDim kTypeBeg(NDef)
            ReDim kTypeEnd(NDef)
            ReDim kLocBeg(NDef)
            ReDim kLocEnd(NDef)
            ReDim kagebeg(NDef)
            ReDim kageend(NDef)
            ReDim kLayerColorBeg(NDef)
            ReDim kLayerColorEnd(NDef)
            'ReDim SetType(NDef)
            ReDim convfact(NDef)
            ReDim convprob(NDef)

            ReDim SetFlowMn(NTic)
            ReDim SetFlowMx(NTic)
            ReDim SetDemandTableID(NTic)
            ReDim SetTarget(NTic)
            ReDim SetPr(IterToSave, NTic) 'the price may have to go to MxTic...have to check
            ReDim SetDev(IterToSave, NTic)
            ReDim SetFlow(IterToSave, NTic)

        End Sub

    End Structure

    Dim CondSetMxNDefs As Integer
    Public CondSet_Type() As String 'can be market, condition, spatial
    Public CondSet_nSetDefs() As Integer ' set definition lines
    '------------------------------
    'the next few lines replace the  CondSet data structure
    Public CondSet_kMapLayer(,) As Integer
    '  Dim kset() As Short
    Public CondSet_kTypeBeg(,) As Integer
    Public CondSet_kTypeEnd(,) As Integer
    Public CondSet_kLocBeg(,) As Integer 'may only be used for market type sets
    Public CondSet_kLocEnd(,) As Integer 'may only be used for market type sets
    Public CondSet_kagebeg(,) As Integer
    Public CondSet_kageend(,) As Integer
    Public CondSet_kLayerColorBeg(,) As Integer
    Public CondSet_kLayerColorEnd(,) As Integer
    Public CondSet_convfact(,) As Single
    Public CondSet_convprob(,) As Single
    '------------------------------------
    'Dim Disc() As Single 'discount rate is just one array...same for every set
    Public CondSet_BaseQuan() As Single
    Public CondSet_ConstraintOption() As Integer
    Public CondSet_DemandTableID(,) As Object
    Public CondSet_Dev(,,) As Single
    Public CondSet_DevMx() As Single
    Public CondSet_DevMn() As Single
    Public CondSet_EndPrWghtSet() As Single
    Public CondSet_Flow(,,) As Single
    Public CondSet_FlowMn(,) As Single
    Public CondSet_FlowMx(,) As Single
    Public CondSet_FlowMn1() As Single
    Public CondSet_FlowMx1() As Single
    Public CondSet_Label() As String
    Public CondSet_Pr(,,) As Single 'by iterations to save, tic
    Public CondSet_PrAdjSet() As Integer
    Public CondSet_Target(,) As Single

    Dim SpatSetMxNDefs As Integer
    Public SpatSet_Type() As String 'can be market, condition, spatial
    Public SpatSet_nSetDefs() As Integer ' set definition lines
    '------------------------------
    'the next few lines replace the  SpatSet data structure
    Public SpatSet_kMapLayer(,) As Integer
    Public SpatSet_kTypeBeg(,) As Integer
    Public SpatSet_kTypeEnd(,) As Integer
    Public SpatSet_kLocBeg(,) As Integer 'may only be used for market type sets
    Public SpatSet_kLocEnd(,) As Integer 'may only be used for market type sets
    'Public SpatSet_kagebeg(,) As Integer
    'Public SpatSet_kageend(,) As Integer
    'Public SpatSet_kLayerColorBeg(,) As Integer
    'Public SpatSet_kLayerColorEnd(,) As Integer
    Public SpatSet_convfact(,) As Single
    'Public SpatSet_convprob(,) As Single
    '------------------------------------
    'Dim Disc() As Single 'discount rate is just one array...same for every set
    Public SpatSet_BaseQuan() As Single
    Public SpatSet_ConstraintOption() As Integer
    Public SpatSet_DemandTableID(,) As Object
    Public SpatSet_Dev(,,) As Single
    Public SpatSet_DevMx() As Single
    Public SpatSet_DevMn() As Single
    Public SpatSet_EndPrWghtSet() As Single
    Public SpatSet_Flow(,,) As Double
    Public SpatSet_FlowMn(,) As Single
    Public SpatSet_FlowMx(,) As Single
    Public SpatSet_FlowMn1() As Single
    Public SpatSet_FlowMx1() As Single
    Public SpatSet_Label() As String
    Public SpatSet_Pr(,,) As Single 'by iterations to save, tic
    Public SpatSet_PrAdjSet() As Integer
    Public SpatSet_Target(,) As Single

    Dim MSetMxNDefs As Integer
    Public MSet_Type() As String 'can be market, condition, spatial
    Public MSet_nSetDefs() As Integer ' set definition lines
    '------------------------------
    'the next few lines replace the  MSet_ data structure
    Public MSet_kMapLayer(,) As Integer
    Public MSet_kTypeBeg(,) As Integer
    Public MSet_kTypeEnd(,) As Integer
    Public MSet_kLocBeg(,) As Integer 'may only be used for market type sets
    Public MSet_kLocEnd(,) As Integer 'may only be used for market type sets
    'Public MSet_kagebeg(,) As Integer
    'Public MSet_kageend(,) As Integer
    'Public MSet_kLayerColorBeg(,) As Integer
    'Public MSet_kLayerColorEnd(,) As Integer
    Public MSet_convfact(,) As Single
    'Public MSet_convprob(,) As Single
    '------------------------------------
    'Dim Disc() As Single 'discount rate is just one array...same for every set
    Public MSet_BaseQuan() As Single
    Public MSet_ConstraintOption() As Integer
    Public MSet_DemandTableID(,) As Integer
    Public MSet_Dev(,,) As Single
    Public MSet_DevMx() As Single
    Public MSet_DevMn() As Single
    Public MSet_EndPrWghtSet() As Single
    Public MSet_Flow(,,) As Double
    Public MSet_FlowMn(,) As Single
    Public MSet_FlowMx(,) As Single
    Public MSet_FlowMn1() As Single
    Public MSet_FlowMx1() As Single
    Public MSet_Label() As String
    Public MSet_Pr(,,) As Single 'by iterations to save, tic
    Public MSet_PrAdjSet() As Integer
    Public MSet_Target(,) As Single

    Public SpaceDefs_SpaceType() As Integer
    Public SpaceDefs_MapLayer() As Integer
    Public SpaceDefs_MapColor() As Integer
    Public SpaceDefs_CoverSite() As Integer
    Public SpaceDefs_AgeBeg() As Integer
    Public SpaceDefs_AgeEnd() As Integer
    Public SpaceDefs_ISpaceFactor() As Single
    Public SpaceDefs_QBufferFactor() As Single
    Public SpaceDefs_EdgeFactor() As Single

    Structure SpatialSeriesData
        Dim SeriesIndex As Integer
        Dim NNonZero As Integer
        Dim Flows() As Byte
        'Dim BeginHabTic() As Byte 'track all periods when habitat first comes on line - tracks multiple locations
        'Dim HabStartTics() As Byte 'actual period values of starts are stored in first spots of the array
        Dim First1Tic As Integer
        Dim Last1tic As Integer
        Dim PersistentSpatial As Boolean 'true if KW is on indefinite rotation, false if existing KW is planted to something else
        Sub Initialize()
            ReDim Flows(MxTic)
            'ReDim BeginHabTic(MxTic)
            'ReDim HabStartTics(MxTic)
        End Sub
    End Structure

    Structure TrimFileData
        Dim NRx As Integer
        Dim RxInd() As Integer
        Dim NPV() As Single
        Dim BufferSeries() As Integer
        Dim EdgeSeries() As Integer
        Dim ISpaceSeries() As Integer
        Dim NPValLbnd As Double
        Dim NPVallMx As Double
    End Structure

    'Structure LandValData
    '    Dim Calculated As Byte 'whether the land val has been calculated or not
    '    Dim LandVal As Double 'the calculated value of flows
    '    'Dim SpatLandVal As Double 'calculated value of spatial flows (per land unit)
    'End Structure
#End Region
   


#End Region


    Public Sub Iterate()
        Dim INKEY As String
        Dim filetoOpen As String
        TotalViolationDev = DeviationWriteFile + 1 'default is to not write this....
        'Dim DirectionsLeft As Integer
        Dim kschedlock As Integer
        Dim kunschedlock As Integer
        Dim jcycle As Integer
        Dim OriginalDirection As Integer
        'Dim bDPRefreshed As Boolean
        Dim FileLines() As String
        ReDim FileLines(0)
        Dim dummy1 As String
        Dim dummy2 As String
        Dim bLastCycle As Boolean
        Dim WriteDir As Integer
        Dim WriteWindSize As Long
        Dim WriteWindSizeFromFile As Long
        Dim MxAllowableState As Long
        Dim jwdef As Integer
        Dim Usedef As Integer
        Dim LastNStandsDef As Integer
        Dim LastWindBreak As Integer
        'Dim bNPVSelfLoaded As Boolean

        ksave = 1
        IterationBase = 1
        TotalDeviations = 10000000 'really large number to begin with until deviations is called

        ReDim AdjIterType(NIterToSave)
        ReDim TOT_AASELF_NPV(NIterToSave)
        ReDim TOT_AA_SPAT_BUFFER_NPV(NIterToSave)
        ReDim TOT_SPAT_NPV(NIterToSave)
        ReDim TOT_MARKET_NPV(NIterToSave)
        ReDim VALUE_DEVIATION(NIterToSave)

        ReDim PolySolRx_ChosenRxIndSave(NSubFor, MaxSubforAAs), PolySolRx_ChosenRxIndBest(NSubFor, MaxSubforAAs)

        'ReadLinkfile() 'gives you FnDualPlanOutBaseA
        pnvfilnum = FreeFile()
        FileOpen(pnvfilnum, FnDualPlanOutBaseA & "IterationPNV.csv", OpenMode.Output)
        PrintLine(pnvfilnum, "IterationNumber, Total_NPV, NPV_AASelf,Total_Core_NPV,Buffer_Core_NPV,Financial_NPV, Estimated_Primal_NPV, AbsVal_Deviation, Net_Adjustment, DevAsPctOfPrimal, TotalDeviations, IterationTime, DPTime")
        subdivpnvfilnum = FreeFile()
        FileOpen(subdivpnvfilnum, FnDualPlanOutBaseA & "SDIterationPNV.csv", OpenMode.Output)
        setdevfilnum = FreeFile()
        FileOpen(setdevfilnum, FnDualPlanOutBaseA & "DeviationsBySet.csv", OpenMode.Output)
        PrintLine(setdevfilnum, "IterationNumber, SetType, SetNumber, Violation Deviation, Excess Deviation")
        totdevfilnum = FreeFile()
        FileOpen(totdevfilnum, FnDualPlanOutBaseA & "Deviations.csv", OpenMode.Output)
        PrintLine(totdevfilnum, "IterationNumber, SetType, Violation Deviation, Excess Deviation")
        subdivinfofilnum = FreeFile()
        FileOpen(subdivinfofilnum, FnDualPlanOutBaseA & "SDIterationINFO.csv", OpenMode.Output)
        If bReportBySD Then
            Staticsubdivpnvfilnum = FreeFile()
            FileOpen(Staticsubdivpnvfilnum, FnDualPlanOutBaseA & "StaticSDIterationPNV.csv", OpenMode.Output)

            If bReportByOrigSD Then
                BaseSDInfoFileName = BaseNameOutFilesA & "_subdivAA_info_sf"
                BaseSDRXLUTFileName = FnDualPlanOutBaseA & "_RxLUT"
            ElseIf bReportByPredefSD = True Then 'defined at IterationProcess screen
                BaseSDInfoFileName = BaseSDInfoFileName
                BaseSDRXLUTFileName = BaseSDRXLUTFileName
            End If

        End If

        IterTimeToStop = 0
        HowMany = 0
        If IterStop > 0 Then IterTimeToStop = 1 'IterStop means that you didn't choose to do the iteration - cancelled the input screen
        MainForm.Txtmore.Text = Str(IterationNumber + IterHowmanyMore)
        ReDim SpatSetLastIterDev(NSpatSet, NTic) 'Signed magnitude of last deviation - Spatial set - by set, tic
        ReDim CondSetLastIterDev(NCondSet, NTic)
        ReDim MSetLastIterDev(NMSet, NTic)
        ReDim SpatSetFlopped(NSpatSet, NTic)
        ReDim CondSetFlopped(NCondSet, NTic)
        ReDim MSetFlopped(NMSet, NTic)


        RedimFlowArrays() 'just the first time
        RedimPriceArrays() 'just the first time
        RedimLandValArrays() 'just the first time

        'Public ItersBetweenTrims As Integer
        kitersbetweentrims = ItersBetweenTrims 'make sure the first DP does a trim!

        Do Until IterTimeToStop > 0
            SubTimeStartS = VB.Timer()
            DPStartTime = 0
            DPEndTime = 0

            AdjSetCounter = AdjSetCounter + 1
            If AdjSetCounter > NAdjSetIters Then AdjSetCounter = 1
            SchSetCounter = SchSetCounter + 1
            If SchSetCounter > NSchSetIters Then SchSetCounter = 1

            MainForm.Label1.Text = "Scheduling"
            IterationNumber = IterationNumber + 1

            MainForm.TxtIter.Text = Str(IterationNumber)
            MainForm.TxtPolyDone.Text = "0"
            MainForm.Refresh()
            HowMany = HowMany + 1
            ksave = ((IterationNumber - 1) Mod NIterToSave) + 1
            ksaveBase = ((IterationBase - 1) Mod NIterToSave) + 1
            'bNPVSelfLoaded = False

            LastIterType = AdjSetIters(AdjSetCounter - 1)
            If IterMethod = "Auto" Then
                AdjIterType(ksave) = AdjSetIters(AdjSetCounter)
            Else
                AdjIterType(ksave) = IterMethod
            End If
            If AdjIterType(ksave) <> "Manual" And AdjIterType(ksave) <> "Input" Then
                Call FloatShapeSmooth() 'Adjusts Set prices based on latest calculated deviations
                Call DualityCheck() 'fixes set prices if there are logic inconsistencies
            End If
            Call SetTypePrices() 'uses updated set prices to adjust(set) type prices
            'need to load NPVSelf calculations
            CalcNPVSelfValue(DualPlanAAPoly_NPVSelf)
            HowmanytoRewrite = HowmanytoRewrite + 1

            ClearLandValArrays() 'once per iteration

            'Call the appropriate SCHEDULE subroutine
            If SchSetIters(SchSetCounter) = "DPSpace" Then
                'get original direction - regardless

                ReadFile(FnDPformInBaseA & "_DPform_MainIN.txt", FileLines)
                'Nsubfor   MovingWindowDesign NstandForLocPriority
                '  1              1                   18 
                dummy1 = Trim(FileLines(2))
                dummy2 = ParseString(dummy1, " ")
                dummy2 = ParseString(dummy1, " ")
                OriginalDirection = CType(dummy2, Integer)
                dummy1 = Trim(FileLines(4))
                dummy2 = ParseString(dummy1, " ")
                OriginalWindowSize = CType(dummy2, Long)
                MxAllowableState = OriginalWindowSize

                'bDPRefreshed = False
                'ItersToRefresh = ItersToRefresh - 1
                NumMultiWindowPasses = 0
                NumMultiScheduleStands = 0
                'If ItersToRefresh <= 0 Then
                If bDPCycles = True Then
                    kschedlock = 0
                    kunschedlock = 0
                    If kSchedLockCycles > 0 Then kschedlock = 1
                    If kUnschedLockCycles > 0 Then kunschedlock = 1
                    ReDim SchedPolySolution(AA_NPolys, kSchedLockCycles), AllSchedPolySolution(AA_NPolys, NumDPCycles)
                    ReDim UnschedPolySolution(AA_NPolys, kUnschedLockCycles), AllUnschedPolySolution(AA_NPolys, NumDPCycles)

                    If bOneOptAfterDP = False Then SCHEDULE_DPSpace(True, True, kschedlock, kunschedlock, False, 0, 0, 0, True, False, bOneOptAfterDP, False) 'check last iteration for good solution; how many comes on the last DP Cycle
                    If bOneOptAfterDP Then
                        SCHEDULE_DPSpace(True, True, kschedlock, kunschedlock, False, 0, 0, 0, False, False, bOneOptAfterDP, False)
                        SCHEDULE_OneOpt(True, True, kschedlock, kunschedlock, True, False, True, False, 0) 'want to consider writing files if last 1-opt was a good solution
                    End If
                Else
                    If bOneOptAfterDP = False Then SCHEDULE_DPSpace(False, False, 0, 0, False, 0, 0, 0, True, True, bOneOptAfterDP, False)
                    If bOneOptAfterDP Then SCHEDULE_DPSpace(False, False, 0, 0, False, 0, 0, 0, False, False, bOneOptAfterDP, False)
                    'one opt follow-up considered at the end of next section If bDPCycles = True
                End If
                'ItersToRefresh = DPRefresh
                'bDPRefreshed = True
                'End If

                If bDPCycles = True Then
                    'assum you've already solved the first DP and/or 1-opt and are tallying the results
                    Call SumVol()
                    ForestNPVTally()
                    'get the direction control file information and record the original direction

                    SubTimeEndS = VB.Timer()
                    'WriteForestPNVIterLines(IterationNumber & "_0", False)
                    'WriteSubDivPNVIterLines(IterationNumber, OriginalDirection, False)
                    SubTimeStartS = VB.Timer

                    'set original window direction and size
                    If bWindowBreaksFromFile = True Then
                        LastWindBreak = 0
                        WriteWindSizeFromFile = WindowSizeAtBreak(1)
                    End If

                    dMaxPresChosen = MaxPresChosen
                    bLastCycle = False
                    For jcycle = 1 To NumDPCycles

                        If jcycle = NumDPCycles Then bLastCycle = True
                        'what is the klock?
                        If kSchedLockCycles > 0 Then kschedlock = ((jcycle) Mod kSchedLockCycles) + 1 'so, cycle 1 is klock 2, etc.
                        If kUnschedLockCycles > 0 Then kunschedlock = ((jcycle) Mod kUnschedLockCycles) + 1
                        If jcycle = 1 Then
                            dMaxPresChosen = MaxPresChosen
                        Else
                            dMaxPresChosen = dMaxPresChosen * CycleDecayFactor
                        End If
                        'what is the direction?
                        WriteDir = OriginalDirection
                        If NDPDirections > 0 Then
                            kDPdirection = ((jcycle - 1) Mod NDPDirections) + 1 'so, cycle 1 is direction 1
                            WriteDir = DirectionsToSearch(kDPdirection)
                        Else
                            ReDim DirectionsToSearch(1)
                            DirectionsToSearch(1) = OriginalDirection
                        End If
                        WriteWindSize = OriginalWindowSize
                        If bIncreaseWindowSize = True Then
                            If bWindowBreaksFromFile = False Then
                                If WindowSizeFactor <= 10 Then
                                    MxAllowableState = Math.Round(MxAllowableState * WindowSizeFactor, 0)
                                Else
                                    MxAllowableState = MxAllowableState + WindowSizeFactor
                                End If
                                If MxAllowableState > WindowMaxSize Then MxAllowableState = WindowMaxSize
                                WriteWindSize = MxAllowableState
                            Else 'you loaded in predetermined breaks from a file
                                jwdef = 0
                                Usedef = jwdef
                                'LastWindBreak = 0
                                Do Until NumMultiScheduleStands > WindowBreakStands(jwdef)
                                    Usedef = jwdef
                                    jwdef = jwdef + 1
                                    If jwdef > NWindowBreaks Then Exit Do
                                Loop

                                If Usedef = LastWindBreak Then
                                    WriteWindSizeFromFile = WriteWindSizeFromFile + WindowSizeFactorAtBreak(Usedef)
                                Else
                                    WriteWindSizeFromFile = WindowSizeAtBreak(Usedef)
                                    LastWindBreak = Usedef
                                End If

                                WriteWindSize = WriteWindSizeFromFile
                                If WriteWindSize > WindowMaxSize Then WriteWindSize = WindowMaxSize 'still need to check for overall feasibility
                            End If
                        End If
                        WriteDPFormInputFile(FnDPformInBaseA & "_DPform_MainIN.txt", WriteDir, WriteWindSize, FileLines)

                        NumMultiWindowPasses = 0
                        NumMultiScheduleStands = 0
                        'for each cycle
                        If bOneOptAfterDP = False And jcycle < NumDPCycles Then SCHEDULE_DPSpace(True, True, kschedlock, kunschedlock, True, jcycle, dMaxPresChosen, MxPresUnchosen, False, False, bOneOptAfterDP, bLastCycle)
                        If bOneOptAfterDP = False And jcycle = NumDPCycles Then SCHEDULE_DPSpace(True, True, kschedlock, kunschedlock, True, jcycle, dMaxPresChosen, MxPresUnchosen, False, True, bOneOptAfterDP, bLastCycle)
                        If bOneOptAfterDP = True Then SCHEDULE_DPSpace(True, True, kschedlock, kunschedlock, True, jcycle, dMaxPresChosen, MxPresUnchosen, False, False, bOneOptAfterDP, bLastCycle)
                        If bOneOptAfterDP = True And jcycle < NumDPCycles Then SCHEDULE_OneOpt(True, True, kschedlock, kunschedlock, False, False, True, bLastCycle, jcycle)
                        If bOneOptAfterDP = True And jcycle = NumDPCycles Then SCHEDULE_OneOpt(True, True, kschedlock, kunschedlock, False, True, True, bLastCycle, jcycle)

                        ForestNPVTally()
                        SubTimeEndS = VB.Timer
                        If jcycle = NumDPCycles Then DPEndTime = VB.Timer
                        'WriteForestPNVIterLines(IterationNumber & "_" & jcycle, False)
                        'WriteSubDivPNVIterLines(IterationNumber & "_" & jcycle, DirectionsToSearch(kDPdirection), False)
                        SubTimeStartS = VB.Timer
                        If NumMultiWindowPasses <= 0 And jcycle < NumDPCycles Then
                            'you've exactly solved the problem; don't need to cycle anymore! - but, still collect the best prescription information
                            jcycle = NumDPCycles - 1 'so, next cycle is last
                        End If
                    Next

                    If NumDPCycles > 0 Then
                        'WriteForestPNVIterLines(IterationNumber & "_" & jcycle)
                        WriteSubDivPNVIterLines(IterationNumber & "_" & NumDPCycles & "H", DirectionsToSearch(kDPdirection), True)
                    End If

                    'what is the original direction and window size?
                    WriteDPFormInputFile(FnDPformInBaseA & "_DPform_MainIN.txt", OriginalDirection, OriginalWindowSize, FileLines)

                    Call SumVol()

                Else 'no cycles
                    If bOneOptAfterDP = True Then SCHEDULE_OneOpt(False, False, 0, 0, True, True, False, False, 0) 'default to this because it gives you better answer rather quickly
                    Call SumVol() 'Sumvol only gets called here if you are not cycling - otherwise the last cycle should catch latest SumVol
                    ForestNPVTally()
                    SubTimeEndS = VB.Timer
                    DPEndTime = VB.Timer
                    WriteForestPNVIterLines(IterationNumber, True)
                    WriteSubDivPNVIterLines(IterationNumber, OriginalDirection, False)
                    SubTimeStartS = VB.Timer
                End If
                Call Deviations(TotalViolationDev, TotalExcessDev)
                If bDeviationAbsValue = False Then TotalDeviations = TotalDeviations / (TOT_MARKET_NPV(ksave) + TOT_EXCESSDEVVAL - TOT_VIOLDEVVAL) 'TOT_MARKET_NPV(ksave)


            ElseIf SchSetIters(SchSetCounter) = "DlPlNoTrim" Then
                SCHEDULE_OneOpt(False, False, 0, 0, True, True, False, False, 0) 'one-opt only consider file write
                Call SumVol()
                ForestNPVTally()
                Call Deviations(TotalViolationDev, TotalExcessDev)
                If bDeviationAbsValue = False Then TotalDeviations = TotalDeviations / (TOT_MARKET_NPV(ksave) + TOT_EXCESSDEVVAL - TOT_VIOLDEVVAL) 'TOT_MARKET_NPV(ksave)
            ElseIf SchSetIters(SchSetCounter) = "NoSpace" Then
                'don't have to bother with spatial iterations
                Call NoSpaceIteration()
                Call SumVol()
                ForestNPVTally()
                Call Deviations(TotalViolationDev, TotalExcessDev)
                If bDeviationAbsValue = False Then TotalDeviations = TotalDeviations / (TOT_MARKET_NPV(ksave) + TOT_EXCESSDEVVAL - TOT_VIOLDEVVAL) 'TOT_MARKET_NPV(ksave)
            End If

            IterMethod = "Auto"
            IterationBase = IterationNumber
            If HowMany >= IterHowmanyMore Then IterTimeToStop = 1
            If INKEY = "8" Then IterTimeToStop = 8
            'option yet to stop if all constraints are satisfied

            If HowmanytoRewrite >= NiterToRewrite Or (TotalDeviations < DeviationWriteFile And TotalDeviations >= 0) Then
                If HowmanytoRewrite >= NiterToRewrite Then HowmanytoRewrite = 0
                PolySchedFileExt = LTrim(Str(IterationNumber)) 'reset
                filetoOpen = FnDualPlanOutBaseA & "NewIn." & LTrim(Str(IterationNumber))
                Call WriteNewInput(filetoOpen)
                Call WriteReport()
            End If
            SubTimeEndS = VB.Timer
            WriteForestPNVIterLines(IterationNumber, True)
            WriteShadowPriceFile(FnDualPlanOutBaseA & "ShadowPrices.txt")
        Loop

        If bReportBySD Then
            FileClose(Staticsubdivpnvfilnum)
        End If
        FileClose(subdivinfofilnum)
        FileClose(subdivpnvfilnum)
        FileClose(pnvfilnum)
        FileClose(setdevfilnum)
        FileClose(totdevfilnum)

    End Sub
    Private Sub RedimPNVArrays()
        ReDim SubForAspatNPV(NSubFor)
        ReDim SubForMarketNPV(NSubFor)
        ReDim AASubForSpatNPV(NSubFor)
        ReDim SubForSpatNPV(NSubFor)
    End Sub

    Private Sub RedimFlowArrays()
        ReDim CondTypeFlow(Ncoversite, MxAgeClass, NMapArea, MxTic)
        ReDim SpatTypeFlow(NSpaceType, MxTic)
        ReDim OneWaySpatTypeFlow(NSpaceType, MxTic)
        ReDim MTypeFlow(NMType, NMLocation, MxTic)
    End Sub
    Private Sub RedimPriceArrays()
        ReDim MtypePr(NMType, NMLocation, MxTic)
        ReDim SpatTypePr(NSpaceType, MxTic)
        ReDim CondTypePr(Ncoversite, MxAgeClass, NMapArea, MxTic)
    End Sub
    Public Sub RedimLandValArrays()
        'only once per iteration
        ReDim CondLandValue_Calculated(NSubFor, NMapArea, AApres_NPres)
        ReDim CondLandValue_LandVal(NSubFor, NMapArea, AApres_NPres)
        ReDim MktLandValue_Calculated(NSubFor, NMLocation, AApres_NPres)
        ReDim MktLandValue_LandVal(NSubFor, NMLocation, AApres_NPres)
        ReDim SpatLandValue_Calculated(NSubFor, NSpaceType, AApres_NPres)
        ReDim SpatLandValue_LandVal(NSubFor, NSpaceType, AApres_NPres)
    End Sub
    Public Sub ClearLandValArrays()
        For jsubfor As Integer = 1 To NSubFor
            For jpres As Integer = 1 To AApres_NPres
                For jma As Integer = 1 To NMapArea
                    CondLandValue_Calculated(jsubfor, jma, jpres) = 0
                    CondLandValue_LandVal(jsubfor, jma, jpres) = 0
                Next
                For jloc As Integer = 1 To NMLocation
                    MktLandValue_Calculated(jsubfor, jloc, jpres) = 0
                    MktLandValue_LandVal(jsubfor, jloc, jpres) = 0
                Next
                For jspat As Integer = 1 To NSpaceType
                    SpatLandValue_Calculated(jsubfor, jspat, jpres) = 0
                    SpatLandValue_LandVal(jsubfor, jspat, jpres) = 0
                Next
            Next
        Next

    End Sub
    Public Sub ClearFlowArrays()
        'happens whenever you recalculate PNV or tally the final for the iteration
        For jtic As Integer = 0 To MxTic
            'condition type arrays
            For jcoversite As Integer = 1 To Ncoversite
                For jage As Integer = 1 To MxAgeClass
                    For jmap As Integer = 1 To NMapArea
                        CondTypeFlow(jcoversite, jage, jmap, jtic) = 0
                    Next
                Next
            Next
            'spatial stuff
            For jspat As Integer = 1 To NSpaceType
                SpatTypeFlow(jspat, jtic) = 0
                OneWaySpatTypeFlow(jspat, jtic) = 0
            Next
            'mtype stuff
            For jmtype As Integer = 1 To NMType
                For jloc As Integer = 1 To NMLocation
                    MTypeFlow(jmtype, jloc, jtic) = 0
                Next
            Next
        Next
    End Sub
    Public Sub ClearPriceArrays()
        For jtic As Integer = 0 To MxTic
            'condition type arrays
            For jcoversite As Integer = 1 To Ncoversite
                For jage As Integer = 1 To MxAgeClass
                    For jmap As Integer = 1 To NMapArea
                        CondTypePr(jcoversite, jage, jmap, jtic) = 0
                    Next
                Next
            Next
            'spatial stuff
            For jspat As Integer = 1 To NSpaceType
                SpatTypePr(jspat, jtic) = 0
            Next
            'mtype stuff
            For jmtype As Integer = 1 To NMType
                For jloc As Integer = 1 To NMLocation
                    MtypePr(jmtype, jloc, jtic) = 0
                Next
            Next
        Next
    End Sub
    Private Sub WriteForestPNVIterLines(ByVal IterNum As String, ByVal bWriteAdjStats As Boolean)
        Dim EstPrimalVal As Double = TOT_MARKET_NPV(ksave) + TOT_EXCESSDEVVAL - TOT_VIOLDEVVAL
        Dim AbsValDeviation As Double = TOT_EXCESSDEVVAL + TOT_VIOLDEVVAL
        Dim TotalAdjustment As Double = TOT_EXCESSDEVVAL - TOT_VIOLDEVVAL
        Dim AdjustmentPercent As Double = AbsValDeviation / EstPrimalVal 'TOT_MARKET_NPV(ksave)
        Dim DPTime As Single = 0
        Dim pDPTime As String = ""
        If DPStartTime <> 0 And DPEndTime <> 0 Then
            DPTime = (DPEndTime - DPStartTime) / 60
        End If
        If DPTime <> 0 Then pDPTime = DPTime
        If bWriteAdjStats Then
            PrintLine(pnvfilnum, IterNum & ", " & TOT_AASELF_NPV(ksave) + TOT_AA_SPAT_BUFFER_NPV(ksave) & _
                       ", " & TOT_AASELF_NPV(ksave) & ", " & TOT_SPAT_NPV(ksave) & ", " & TOT_AA_SPAT_BUFFER_NPV(ksave) & _
                       ", " & TOT_MARKET_NPV(ksave) & ", " & EstPrimalVal & ", " & AbsValDeviation & ", " & TotalAdjustment & ", " & AdjustmentPercent & ", " & TotalDeviations & ", " & (SubTimeEndS - SubTimeStartS) / 60 & ", " & pDPTime)
        Else
            PrintLine(pnvfilnum, IterNum & ", " & TOT_AASELF_NPV(ksave) + TOT_AA_SPAT_BUFFER_NPV(ksave) & _
                       ", " & TOT_AASELF_NPV(ksave) & ", " & TOT_SPAT_NPV(ksave) & ", " & TOT_AA_SPAT_BUFFER_NPV(ksave) & _
                       ", " & TOT_MARKET_NPV(ksave) & ",,,,,,,,," & (SubTimeEndS - SubTimeStartS) / 60 & ", " & pDPTime)
        End If
    End Sub

    Private Sub WriteSubDivPNVIterLines(ByVal IterNum As String, ByVal Direction As Integer, ByVal bBestSol As Boolean)
        Dim str As String
        Dim str2 As String
        Dim str3 As String
        Dim str4 As String


        'first write the PNV information
        str = "IterNumber"
        For jsubfor As Integer = 1 To NSubFor
            For jsd As Integer = 1 To NSubForSD(jsubfor)
                'flag for exact solution
                str2 = ""
                If bSubForSubDivSinglePass(jsubfor, jsd) = True Then
                    str2 = "*"
                End If
                str = str & ", SD" & jsubfor & "." & jsd & str2
            Next
            PrintLine(subdivpnvfilnum, str)
            PrintLine(subdivinfofilnum, str)
            str = IterNum
            str3 = "Stands"
            str4 = "Passes(" & Direction & ")"
            For jsd As Integer = 1 To NSubForSD(jsubfor)
                str = str & "," & SubForSDAspatNPV(jsubfor, jsd) + SubForSDSpatNPV(jsubfor, jsd)
                str3 = str3 & "," & SubForSubDivNStands(jsubfor, jsd)
                str4 = str4 & "," & SubForSubDivWindowCount(jsubfor, jsd)
            Next
            PrintLine(subdivpnvfilnum, str)
            PrintLine(subdivinfofilnum, str3)
            PrintLine(subdivinfofilnum, str4)
        Next jsubfor

        If bReportBySD = True Then
            'first write the PNV information
            str = "IterNumber"
            For jsubfor As Integer = 1 To NSubFor
                For jsd As Integer = 1 To NStaticSubForSD(jsubfor)
                    str = str & ", SD" & jsubfor & "." & jsd
                Next
                PrintLine(Staticsubdivpnvfilnum, str)
                str = IterNum
                str3 = "Stands"
                If bBestSol = False Then
                    For jsd As Integer = 1 To NStaticSubForSD(jsubfor)
                        str = str & "," & StaticSubForSDAspatNPV(jsubfor, jsd) + StaticAASubForSDSpatNPV(jsubfor, jsd)
                        str3 = str3 & "," & StaticSubForSubDivNStands(jsubfor, jsd)
                    Next
                ElseIf bBestSol = True Then
                    For jsd As Integer = 1 To NStaticSubForSD(jsubfor)
                        str = str & "," & PolySolRx_BestSDSol(jsubfor, jsd)
                        str3 = str3 & "," & StaticSubForSubDivNStands(jsubfor, jsd)
                    Next
                End If
                PrintLine(Staticsubdivpnvfilnum, str)
                PrintLine(Staticsubdivpnvfilnum, str3)
                'also track best solution for this static subdivision - for reporting at the end of cycling

            Next jsubfor
        End If


        'then write the subdivision information

    End Sub
    Private Sub AspatialTally()
        'ULTIMATELY, THIS WILL HAVE TO TALLY THE SCHEDULES PICKED FROM THE DP (Or from the spatial scheduling routine from DualPlan)
        'WHICH MEANS IT WON'T HAVE TO GO THROUGH EVERY AA'S RX LIST...JUST REFERENCE THE ONE PICKED

        'Assume there may be some mapping for prescription names

        'Assume you've read some definitions file that gives you these parameters as well as the 
        'information on subforests processed and output by DPSpace...


        ' Loop through the set of input data.
        ' Data is stored in blocks of AA's with multiple files for each block


        'assume you've read the translator file and have the number of aas in this jaaset
        Dim ktic As Short
        Dim kmtype As Short
        Dim krx As Integer
        Dim jtic As Short
        Dim kMapArea As Short
        Dim jflow As Integer
        Dim AAarea As Single
        Dim kmloc As Short
        Dim jMap As Short
        Dim jaa As Integer

        For jaa = 1 To AA_NPolys ' AA_polyid.Length - 1 'AASetNAA(jaaset) 'AA.Length - 1 'You've read in the subforest information   AASetNAA(jaaset)

            polyidDualplan = AA_polyid(jaa)
            krx = PolySolRx_ChosenRxInd(jaa) 'PolySolRx(jaa).ChosenRxInd

            AAarea = AA_area(jaa)
            kmloc = AA_mloc(jaa)

            For jMap = 1 To NMapLayer
                MapLayerColor(jMap) = AA_MapLayerColor(jaa, jMap)
            Next jMap

            For krx = krx To krx 'filter to only forested polys
                For jflow = AApres_FirstFlow(krx) To AApres_FirstFlow(krx + 1) - 1
                    kmtype = AAflow_type(jflow)
                    ktic = AAflow_tic(jflow)
                    MTypeFlow(kmtype, kmloc, ktic) = MTypeFlow(kmtype, kmloc, ktic) + AAarea * AAflow_quan(jflow)
                Next jflow

                ' Add Info for biological conditions for each map for first rotation
                For jMap = 1 To NMapLayer
                    kMapArea = MapAreaID(jMap, MapLayerColor(jMap))
                    For jtic = 0 To MxTic
                        CondTypeFlow(AApres_Coversite(krx, jtic), AApres_Age(krx, jtic), kMapArea, jtic) = CondTypeFlow(AApres_Coversite(krx, jtic), AApres_Age(krx, jtic), kMapArea, jtic) + AAarea
                    Next jtic
                Next jMap
            Next krx
        Next jaa

    End Sub

    Private Sub SpatialTally()
        'goes through each stand in each IZone to tally the amount of ISpace in each zone...
        'used to populate the SpatialTypeFlow() information...

        'loop through all influence zones
        'check to see if their spatial type matches
        'check each time period to see whether they qualify as ISpace and sum the flows

        Dim FirstTic As Integer = 0
        Dim LastTic As Integer = 0
        Dim kaa As Integer
        Dim krx As Integer
        Dim kkaa As Integer
        Dim kkrx As Integer
        Dim kkind As Integer
        Dim kkspat As Integer
        Dim kind As Integer
        Dim kspat As Integer
        Dim SpatValue() As Double 'tally of the spatial value to add to the flow set each tic
        Dim OneWaySpatValue() As Double
        Dim IZoneArea As Double

        For jiz As Integer = 1 To NZoneSubFor
            'get looping and matching parameters from the first aa in the zone
            kaa = ZoneAAlist(jiz, 1)
            krx = PolySolRx_ChosenRxInd(kaa) 'PolySolRx(kaa).ChosenRxInd
            kind = AApres_ISpaceInd(krx)
            kspat = AApres_SpaceType(krx)
            'just take the first and last of the first one...range can't be any larger than the narrowest, so there could be some inefficiencies
            FirstTic = SpatialSeries(kind).First1Tic
            LastTic = SpatialSeries(kind).Last1tic 'Math.Min(SpatialSeries(kind).Last1tic, NTic) '########MAY BE USEFUL AT SOME POINT TO TALLY BEYOND THE PLAN HORIZON...
            ReDim SpatValue(MxTic)
            ReDim OneWaySpatValue(MxTic)
            For jtic As Integer = FirstTic To LastTic
                SpatValue(jtic) = 1
                OneWaySpatValue(jtic) = 1
            Next
            IZoneArea = 0
            'first, add the feasibility of spatial value in each tic
            For jaa As Integer = 1 To Zonedim(jiz)
                kkaa = ZoneAAlist(jiz, jaa)
                kkrx = PolySolRx_ChosenRxInd(kkaa) '(PolySolRx(kkaa).ChosenRxInd)
                kkind = AApres_ISpaceInd(kkrx)
                kkspat = AApres_SpaceType(kkrx)
                If kkspat <> kspat Then
                    For jtic As Integer = FirstTic To LastTic
                        SpatValue(jtic) = 0
                        OneWaySpatValue(jtic) = 0
                    Next
                    Exit For 'don't have to evaluate the rest of the izone...it's not good
                Else
                    For jtic As Integer = FirstTic To LastTic
                        SpatValue(jtic) = SpatValue(jtic) * SpatialSeries(kkind).Flows(jtic)
                    Next
                    If Zonedim(jiz) = 1 Then
                        For jtic As Integer = FirstTic To LastTic
                            OneWaySpatValue(jtic) = OneWaySpatValue(jtic) * SpatialSeries(kkind).Flows(jtic)
                        Next
                    Else
                        For jtic As Integer = FirstTic To LastTic
                            OneWaySpatValue(jtic) = 0
                        Next
                    End If
                End If
                IZoneArea = IZoneArea + ZoneAAlistAreaS(jiz, jaa)
            Next
            'then add to the spatial flows
            For jtic As Integer = FirstTic To LastTic
                SpatTypeFlow(kspat, jtic) = SpatTypeFlow(kspat, jtic) + SpatValue(jtic) * IZoneArea
                OneWaySpatTypeFlow(kspat, jtic) = OneWaySpatTypeFlow(kspat, jtic) + OneWaySpatValue(jtic) * IZoneArea
            Next
        Next
        'then tally the value of these flows
        For jstype As Integer = 1 To NSpaceType
            'kspat = SpaceDefs_SpaceType(jspat)
            For jtic As Integer = 1 To MxTic
                SubForSpatNPV(kaaset) = SubForSpatNPV(kaaset) + SpatTypeFlow(jstype, jtic) * SpatTypePr(jstype, jtic)
            Next
        Next jstype
    End Sub

    Private Sub ForestNPVTally()
        TOT_AASELF_NPV(ksave) = 0
        TOT_MARKET_NPV(ksave) = 0
        TOT_SPAT_NPV(ksave) = 0
        TOT_AA_SPAT_BUFFER_NPV(ksave) = 0
        'Now, calculate the total NPV for this saved iteration
        For jsubfor As Integer = 1 To NSubFor
            TOT_AASELF_NPV(ksave) = TOT_AASELF_NPV(ksave) + SubForAspatNPV(jsubfor)
            TOT_AA_SPAT_BUFFER_NPV(ksave) = TOT_AA_SPAT_BUFFER_NPV(ksave) + AASubForSpatNPV(jsubfor)
            TOT_SPAT_NPV(ksave) = TOT_SPAT_NPV(ksave) + SubForSpatNPV(jsubfor)
            TOT_MARKET_NPV(ksave) = TOT_MARKET_NPV(ksave) + SubForMarketNPV(jsubfor)
        Next

    End Sub

    Private Sub SubforNPVTally(ByVal subforind As Integer, ByVal bTrackSubDivisions As Boolean, ByVal bTrackStaticSubDivisions As Boolean, ByVal bAfterDP As Boolean)
        'go through this after schedules are locked for each subforest in the iteration.
        'tallies the total NPV for the entire subforest

        Dim krx As Integer
        Dim jaa As Integer
        Dim ksd As Integer
        Dim kosd As Integer



        For jaa = 1 To AA_NPolys 'AASetNAA(jaaset) 'AA.Length - 1 'You've read in the subforest information   AASetNAA(jaaset)
            'polyidDualplan = AA_polyid(jaa)
            krx = PolySolRx_ChosenRxInd(jaa) 'PolySolRx(jaa).ChosenRxInd
            If krx > 0 Then 'filter to only forested polys
                SubForAspatNPV(subforind) = SubForAspatNPV(subforind) + PolySolRx_AspatNPV(jaa)
                AASubForSpatNPV(subforind) = AASubForSpatNPV(subforind) + PolySolRx_SpatNPV(jaa)
                SubForMarketNPV(subforind) = SubForMarketNPV(subforind) + PolySolRx_MarketNPV(jaa)
                If bTrackSubDivisions Then
                    ksd = AASubDiv(DPSpace_PolyInd(jaa))
                    SubForSDAspatNPV(subforind, ksd) = SubForSDAspatNPV(subforind, ksd) + PolySolRx_AspatNPV(jaa)
                    SubForSDSpatNPV(subforind, ksd) = SubForSDSpatNPV(subforind, ksd) + PolySolRx_SpatNPV(jaa)
                    'SubForSDMarketNPV(subforind, ksd) = SubForSDMarketNPV(subforind, ksd) + PolySolRx_MarketNPV(jaa)
                    If bTrackStaticSubDivisions = True And SubForStaticAASubDiv(subforind, jaa) > 0 Then
                        kosd = SubForStaticAASubDiv(subforind, jaa)
                        StaticSubForSDAspatNPV(subforind, kosd) = StaticSubForSDAspatNPV(subforind, kosd) + PolySolRx_AspatNPV(jaa)
                        StaticAASubForSDSpatNPV(subforind, kosd) = StaticAASubForSDSpatNPV(subforind, kosd) + PolySolRx_SpatNPV(jaa)
                    End If
                End If
            End If
        Next jaa
        'track best solution 
        If bReportBySD = True And bAfterDP = True Then
            For jsd As Integer = 1 To NStaticSubForSD(subforind)
                If StaticSubForSDAspatNPV(subforind, jsd) + StaticAASubForSDSpatNPV(subforind, jsd) > PolySolRx_BestSDSol(subforind, jsd) Then
                    PolySolRx_BestSDSol(subforind, jsd) = StaticSubForSDAspatNPV(subforind, jsd) + StaticAASubForSDSpatNPV(subforind, jsd)
                    For jaa = 1 To AA_NPolys
                        kosd = SubForStaticAASubDiv(subforind, jaa)
                        If kosd = jsd Then
                            PolySolRx_ChosenRxIndBest(subforind, jaa) = PolySolRx_ChosenRxInd(jaa)
                        End If
                    Next
                End If
            Next jsd
        End If

    End Sub

    Private Sub AANPVTally(ByVal ProposedRx() As Long, ByRef AspatNPV() As Double, ByRef SpatNPV() As Double, ByRef MarketNPV() As Double) ', ByRef DualExcessNPV() As Double)
        Dim kMapArea As Integer
        Dim krx As Integer
        Dim kind As Integer
        Dim kspat As Integer
        Dim ktic As Integer
        Dim kcoversite As Integer
        Dim kage As Integer
        Dim kmtype As Integer
        Dim jmap As Integer
        Dim AAArea As Double 'the area of the AA

        Dim jflow As Integer
        Dim AANPV As Single
        Dim AANSNPV As Single 'non-spatial aanpv - includes value of 1-way Izone space in the interior of the stand
        Dim AAMarketNPV As Single 'market NPV

        Dim InteriorISpaceAreaRatio As Double 'ratio of the 1-way Izone area in the center of stand to the area of the stand

        Dim TicAreaVal() As Double 'for all stands influenced by the stand, what's the per-tic potential area in each time period?
        Dim ZoneAreaVal() As Byte 'for the evaluated zone, what's the potential ISpace by Tic? If all schedules line up to produce ISpace in a period, this value is 1 for that period
        Dim TicTally() As Integer 'whether the izone produces core area in each period
        Dim ZoneAreaThisStand As Double 'Area in the influence zone for the stand being evaluated
        Dim ZoneID As Integer
        Dim kaa As Integer

        Dim kmloc As Short

        Dim EntAdjFactor As Double 'adjustment factor for size-based entry cost

        'take the actual chosen schedules of the polys and attribute the NPV to each poly
        'DETERMINE THE VALUES OF THESE SCHEDULES
        ReDim TicISpaceLevel(MxTic), TicISpaceValue(MxTic)
        For jaa As Integer = 1 To AA_NPolys 'AASetNAA(kaaset)
            Do Until AA_polyid(jaa) > 0 Or jaa > AA_NPolys  'control for blank aa information
                jaa = jaa + 1
            Loop
            'If jaa = 512 Then Stop
            'set parameters at the AA level
            ReDim MapLayerColor(NMapLayer)
            For jmap = 1 To NMapLayer
                MapLayerColor(jmap) = AA_MapLayerColor(jaa, jmap)
            Next jmap

            kmloc = AA_mloc(jaa)
            AAArea = AA_area(jaa)
            InteriorISpaceAreaRatio = DualPlanAAPoly_ISpaceSelf(jaa) / AAArea
            'kaa = jaa

            'look at the schedule of the neighbors and figure out the maximum area in this stand that 
            'would be in ISpace if the evaluated schedule matches with them

            'figure out the influenced stands to evaluate prescriptions against...
            'go through influence zones this stand is in...populate a list of influenced stands to consider

            ReDim TicAreaVal(MxTic)
            ReDim TicTally(MxTic)
            ReDim ZoneAreaVal(MxTic)

            If bNeedSpatialIterations = True Then

                For jiz As Integer = 1 To DualPlanAAPoly_NIZones(jaa)
                    ZoneID = DualPlanAAPoly_IZones(jaa, jiz)
                    ReDim ZoneAreaVal(MxTic)
                    'If KeepRx = 1 Then Exit For

                    For jtic As Integer = 1 To MxTic
                        ZoneAreaVal(jtic) = 1 'default to 1
                    Next
                    ZoneAreaThisStand = 0
                    For jjaa As Integer = 1 To Zonedim(ZoneID)
                        kaa = ZoneAAlist(ZoneID, jjaa)
                        krx = ProposedRx(kaa)
                        kind = AApres_ISpaceInd(krx)
                        For jtic As Integer = 1 To MxTic
                            ZoneAreaVal(jtic) = ZoneAreaVal(jtic) * SpatialSeries(kind).Flows(jtic)
                        Next

                        If kaa = jaa And Zonedim(ZoneID) > 1 Then ZoneAreaThisStand = ZoneAAlistAreaS(ZoneID, jjaa) 'area of this poly in the zone

                    Next
                    For jtic As Integer = 1 To MxTic
                        TicAreaVal(jtic) = TicAreaVal(jtic) + ZoneAreaVal(jtic) * ZoneAreaThisStand
                    Next
                Next jiz
            End If

            'First, find the value of the aspatial flows
            'For jrx As Integer = 1 To NRx 'jpresAA = AA(KAA).firstpres To AA(KAA).lastpres
            krx = ProposedRx(jaa) 'AA_PresArray(jaa, jrx) 'krx used to access line in prescription database
            kind = AApres_ISpaceInd(krx)
            kspat = AApres_SpaceType(krx)

            'jpresAA = AA(jaa).PresArray(j)
            AANPV = 0
            AAMarketNPV = 0
            'AANSNPV = 0
            '  first add values of all market flows

            For jflow = AApres_FirstFlow(krx) To AApres_FirstFlow(krx + 1) - 1
                ktic = AAflow_tic(jflow)
                'DEBUG: Control for long rotations
                If ktic > MxTic Then Exit For
                kmtype = AAflow_type(jflow)
                kcoversite = AApres_Coversite(krx, ktic)
                kage = AApres_Age(krx, ktic)
                EntAdjFactor = 1
                For jEnt As Integer = 1 To EntryAdjNEquation
                    For jEntDef As Integer = 1 To EntryAdjNDef(jEnt)
                        If kcoversite >= EntryAdjkCoverSiteBeg(jEnt, jEntDef) And kcoversite <= EntryAdjkCoverSiteEnd(jEnt, jEntDef) And _
                           kage >= EntryAdjkagebeg(jEnt, jEntDef) And kage <= EntryAdjkageend(jEnt, jEntDef) And _
                           AAflow_type(jflow) >= MTypeEntryCostBeg(jEnt, jEntDef) And AAflow_type(jflow) <= MTypeEntryCostEnd(jEnt, jEntDef) Then
                            'If AAflow_type(jflow) = EntryAdjMType(jEnt) Then
                            'have to figure out the area-based factor
                            EntAdjFactor = AdjFactor(jEnt, AAArea)
                            Exit For
                        End If
                    Next jEntDef
                Next
                AANPV = AANPV + AAflow_quan(jflow) * MtypePr(kmtype, kmloc, ktic) * EntAdjFactor
            Next
            AAMarketNPV = AANPV 'only tally so far is for market values through the defined number of periods

            ' Then add values associated with biological conditions and spatial conditions of each map layer
            ktic = Math.Max(AApres_RgTic(krx) + AApres_RgRotLen(krx), NTic) 'the regen rx is summed through the full rotation and a land val is added after that point

            'DEBUG: Control for long rotations
            If ktic > MxTic Then ktic = MxTic
            For jtic As Integer = 1 To ktic 'the regen Rx will begin tally before the end of the planning horizon, but then adjust everything to NTIC +1
                'first condition type prices
                For jmap = 1 To NMapLayer
                    kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
                    AANPV = AANPV + CondTypePr(AApres_Coversite(krx, jtic), AApres_Age(krx, jtic), kMapArea, jtic)
                Next jmap
                'then spatial type prices for the 1-way influence zone
                AANPV = AANPV + SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * InteriorISpaceAreaRatio
            Next jtic


            '-----------------------------------------
            'Add info. for future rotations...only for Rx other than grow-only
            'DEBUG: - ADDED THE RGROTLEN+RGTIC < MXTIC FOR LONG ROTATIONS - IF YOU CAN'T GET IT IN, IT'S ESSENTIALLY A GROW-ONLY
            If AApres_RgRotLen(krx) > 0 And AApres_RgTic(krx) + AApres_RgRotLen(krx) < MxTic Then
                ktic = AApres_RgTic(krx) + AApres_RgRotLen(krx)
                kcoversite = AApres_Coversite(krx, ktic)


                If MktLandValue_Calculated(kaaset, kmloc, krx) = 0 Then CalcMktLandVal(kmloc, krx)
                AANPV = AANPV + MktLandValue_LandVal(kaaset, kmloc, krx) * CondDisc(ktic - NTic - 1)
                AAMarketNPV = AAMarketNPV + MktLandValue_LandVal(kaaset, kmloc, krx) * CondDisc(ktic - NTic - 1) 'NOW add in the market value of the infinite series

                'add in value for condition type flows
                For jmap = 1 To NMapLayer
                    kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
                    If CondLandValue_Calculated(kaaset, kMapArea, krx) = 0 Then CalcCondLandVal(kMapArea, krx)
                    'discount the appropriate land value by the number of periods beyond NTic + 1 the regen occurs
                    'add flows for condition types
                    AANPV = AANPV + CondLandValue_LandVal(kaaset, kMapArea, krx) * CondDisc(ktic - NTic - 1)
                Next

                'add in any value for spatial flows - of the interior of the stand
                If SpatLandValue_Calculated(kaaset, kspat, krx) = 0 Then CalcSpatLandVal(krx, kind, kspat)
                AANPV = AANPV + SpatLandValue_LandVal(kaaset, kspat, krx) * CondDisc(ktic - NTic - 1) * InteriorISpaceAreaRatio

            End If

            'add value for spatial alignment with the neighbors

            'only use the values within the range of looking to the MaxTIC...ignore any type of 
            'perpetual series (I think)
            kMapArea = MapAreaID(SpaceDefs_MapLayer(kspat), SpaceDefs_MapColor(kspat))
            AANSNPV = AANPV
            For jtic As Integer = SpatialSeries(kind).First1Tic To SpatialSeries(kind).Last1tic '#####NTic + 1
                'modified this code to be cleaner - spatialseries().flows is part of TicAreaVal, as calculated above
                AANPV = AANPV + SpatTypePr(kspat, jtic) * (TicAreaVal(jtic) / AAArea) 'SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * (TicAreaVal(jtic) / AAArea) 'adjusted to per-acre value
                TicISpaceLevel(jtic) = TicISpaceLevel(jtic) + TicAreaVal(jtic)
                TicISpaceValue(jtic) = TicISpaceValue(jtic) + TicAreaVal(jtic) * SpatTypePr(kspat, jtic)
            Next jtic

            AspatNPV(jaa) = AANSNPV * AAArea 'includes the spatial value of the interior of the stand
            SpatNPV(jaa) = (AANPV - AANSNPV) * AAArea 'is just the value of the area in 2+ influence zones
            MarketNPV(jaa) = AAMarketNPV * AAArea 'should include just the market NPV values....

        Next jaa

        ''----------------------------------------------
        ''debug
        'Dim fnum As Integer
        'fnum = FreeFile()
        'FileOpen(fnum, FnDualPlanOutBaseA & "DEBUG_PNV.csv", OpenMode.Output)
        'PrintLine(fnum, "jaa, DPSpacePolyID, subdiv, aa_aspatnpv, spat_npv, market_npv, chosenRx")
        'For jaa As Integer = 1 To DualPlanSubForests(kaaset).NPolys 'AASetNAA(kaaset)
        '    PrintLine(fnum, jaa & "," & DPSpace_PolyInd(jaa) & "," & AASubDiv(DPSpace_PolyInd(jaa)) & "," & PolySolRx_AspatNPV(jaa) & "," & PolySolRx_SpatNPV(jaa) & "," & PolySolRx_MarketNPV(jaa) & "," & PolySolRx_ChosenRxInd(jaa))
        'Next
        'FileClose(fnum)
        ''----------------------------------------------

    End Sub

    Private Sub CollectBestSubdivRx(ByVal ksubfor As Integer)
        'tracks the best solution found with cycle iterations - may not be the last one
        Dim kosd As Integer
        For jaa As Integer = 1 To AA_NPolys
            kosd = SubForStaticAASubDiv(kaaset, jaa)
            If kosd > 0 And PolySolRx_ChosenRxIndBest(ksubfor, jaa) <> 0 Then
                PolySolRx_ChosenRxInd(jaa) = PolySolRx_ChosenRxIndBest(ksubfor, jaa)
            End If
        Next
    End Sub

    Public Sub NoSpaceIteration()
        Dim relabel As Integer
        Dim kaatotal As Integer
        'Dim PolySolArray() As Long
        Dim ProposedRx() As Long

        '  Evaluate existing stands one at a time

        kaatotal = 0
        relabel = 0

        ' Loop through the set of input data.
        ' Data is stored in blocks of AA's with multiple files for each block

        ClearFlowArrays()
        RedimPNVArrays()
        For jaaset As Integer = 1 To AAnDataSet
            ReDim PolySolRx_AspatNPV(AA_NPolys)
            ReDim PolySolRx_SpatNPV(AA_NPolys)
            ReDim PolySolRx_MarketNPV(AA_NPolys)
            ReDim ProposedRx(AA_NPolys)

            'what is the schedule?
            Call SCHEDULE_NoSpace(PolySolRx_ChosenRxInd)
            'what do all these schedules mean?
            Call AANPVTally(PolySolRx_ChosenRxInd, PolySolRx_AspatNPV, PolySolRx_SpatNPV, PolySolRx_MarketNPV) ', PolySolRx_DualExcessNPV)
            Call AspatialTally()
            Call SpatialTally()

            If HowmanytoRewrite >= NiterToRewrite Or (TotalDeviations < DeviationWriteFile And TotalDeviations >= 0) Then
                WriteIterOutputFiles(LTrim(Str(IterationNumber)), PolySolRx_ChosenRxInd)
                GC.Collect()
            End If

            'now, tally the total pnv for this subforest
            SubforNPVTally(kaaset, False, False, False) 'bAfterDP, bReportBySD, bAfterDP)

            If jaaset < AAnDataSet Then
                DualPlanSubForests(kaaset).PolySchedFile = FnDualPlanOutBaseA & "PolySched" & kaaset & ".txt"
                WritePolySolFile(DualPlanSubForests(kaaset).PolySchedFile, kaaset, PolySolRx_ChosenRxInd) 'record the latest scheduled stuff for this stand

                kaaset = kaaset + kaasetdirection

                'ReDim DualPlanAAPoly(AA_NPolys)
                ReDim DualPlanAAPoly_polyid(AA_NPolys)

                ReDim PolySolRx_ChosenRxInd(AA_NPolys)

                LoadSubForest(DualPlanSubForests(kaaset))

                ''need to load NPVSelf calculations
                CalcNPVSelfValue(DualPlanAAPoly_NPVSelf)

            End If

        Next jaaset
        kaasetdirection = kaasetdirection * (-1)

        MainForm.TxtPolyDone.Text = Str(kaatotal)
        MainForm.Label1.Text = "Finished this iteration"
        MainForm.Label1.Refresh()


    End Sub
    Public Sub SCHEDULE_DPSpace(ByVal bSchedSaveLock As Boolean, ByVal bUnschedSaveLock As Boolean, ByVal ikschedlock As Integer, ByVal ikunschedlock As Integer, ByVal DoCycles As Boolean, ByVal cyclenum As Integer, ByVal iMaxPresScheduled As Integer, ByVal iMaxPresUnscheduled As Integer, ByVal bConsiderDepFileWrite As Boolean, ByVal bConsiderHowManyFileWrite As Boolean, ByVal bOneOptFollowUp As Boolean, ByVal bLastCycle As Boolean)
        'Calls DPSpace to do the scheduling with the current prices
        'bOneOptFollowUp means you're doing a one-opt refine after this; so don't need to tally and all that stuff
        Dim bLastIterRxTrim As Boolean
        Dim PolySolArray() As Long

        ClearFlowArrays()
        RedimPNVArrays()
        If bLockExactSDSol = True And DoCycles = False Then
            ReDim bgLockedAASD(NSubFor, 0)
            ReDim gLockedRxSD(NSubFor, 0)
        End If

        'If DoCycles = False Then ReadLinkfile()
        If DoCycles = False Then WriteDPSpaceFiles()
        bLastIterRxTrim = True
        If DoCycles = False Then kitersbetweentrims = kitersbetweentrims + 1 'ItersBetweenTrims
        If kitersbetweentrims >= ItersBetweenTrims Then
            kitersbetweentrims = 0 'reset
            bLastIterRxTrim = False 'make sure you do a trim this time
        End If

        'Use the best current soluion to trim treatments for DPSpace input...
        For jaaset As Integer = 1 To AAnDataSet
            ReDim PolySolRx_AspatNPV(AA_NPolys)
            ReDim PolySolRx_SpatNPV(AA_NPolys)
            ReDim PolySolRx_MarketNPV(AA_NPolys)
            'ReDim PolySolRx_DualExcessNPV(AA_NPolys)
            If bLockExactSDSol = True And DoCycles = False Then
                ReDim bgLockedAASD(kaaset, AA_NPolys)
                ReDim gLockedRxSD(kaaset, AA_NPolys)
            End If
            'RedimLandValArrays()
            'ReDim PolySolRx_ChosenRxInd(DualPlanSubForests(kaaset).NPolys) 'this is redimmed and populated in the DPSpace solution translator routine

            If jaaset < AAnDataSet Then

                kaaset = kaaset + kaasetdirection

                'ReDim DualPlanAAPoly(AA_NPolys)
                ReDim DualPlanAAPoly_polyid(AA_NPolys)
                'ReDim DualPlanAAPoly_PassToSpace(, )  'true means keep, false means it gets filtered...
                ReDim DualPlanAAPoly_ISpaceSelf(AA_NPolys)  ' the area of the 1-way influence zone within the borders of this stand
                ReDim DualPlanAAPoly_NPVallMx(AA_NPolys)  'The maximum NPVValMxSpat - dimensioned by stand
                'ReDim DualPlanAAPoly_NPVSelf(, )  'by prescription, sum of NPVAspatial and the guaranteed interior value of the stand - Includes infinite series of those spatial flows...
                ReDim DualPlanAAPoly_NPValLbnd(AA_NPolys)  'aka NPVMin, or largest NPVSelf 
                ReDim DualPlanAAPoly_EstISpacePct(AA_NPolys)  'estimated percentage of the 'edge' area that is in ispace for the poly - a function of npvlbnd and npvmax
                'ReDim DualPlanAAPoly_StandID(AA_NPolys)  'Stand ID of the stand in question
                ReDim DualPlanAAPoly_NRxIn(AA_NPolys)  'number of non-trimmed prescriptions of the polygon
                ReDim DualPlanAAPoly_PolyRxSpace(AA_NPolys)  '0/1 based on whether the stand has a non-trimmed spatial prescription
                ReDim DualPlanAAPoly_NIZones(AA_NPolys)
                'ReDim DualPlanAAPoly_IZones(, )
                'ReDim PolySolRx_PolyInd(DualPlanSubForests(kaaset).NPolys)

                LoadSubForest(DualPlanSubForests(kaaset))
                'NEED TO LOAD IZONES
                LoadIZones(DualPlanSubForests(kaaset).IZonesFile)
                'need to load NPVSelf calculations
                CalcNPVSelfValue(DualPlanAAPoly_NPVSelf)
            End If


            If DoCycles = False Then
                If bLastIterRxTrim = False Then
                    If bGridRxBuilder = False Then
                        Dim Trimmer As New clTrimmer1
                        Trimmer.RunTrimmer(kaaset, True) 'will spit out DPSpace trimmed poly/Rx file(s)
                        Trimmer = Nothing
                    ElseIf bGridRxBuilder = True Then
                        Dim Trimmer As New clTrimmer_wGrid
                        Trimmer.RunGridTrimmer(kaaset, GridFile, IterationNumber)
                        Trimmer = Nothing
                    End If
                Else
                    Dim Trimmer As New clTrimmer1
                    Trimmer.UseLastIterRxTrimmer(kaaset) 'will spit out DPSpace trimmed poly/Rx file(s)
                    Trimmer = Nothing
                End If
            Else
                Dim Trimmer As New clTrimmer1
                Trimmer.CycleTrimmer(kaaset, cyclenum)
                Trimmer = Nothing
            End If

        Next jaaset


        If DoCycles = False Then WriteLinkfile()
        If DoCycles = False Then DPStartTime = VB.Timer 'start the timer after the trimming is done

        'RUN THE DPFORM AND DPSPACE PROGRAMS!
        System.Windows.Forms.Application.DoEvents()
        ChDir(EXEDirectory)
        'first do the subdivisions
        MainForm.Label1.Text = "Called HexSubdivisions"
        MainForm.Label1.Refresh()
        Shell("HexSubdiv.exe", AppWinStyle.NormalFocus, True)

        'then DPForm
        MainForm.Label1.Text = "Called DPform"
        MainForm.Label1.Refresh()
        System.Windows.Forms.Application.DoEvents()
        Shell("DPform.exe", AppWinStyle.NormalFocus, True)

        'finally DPSpace
        MainForm.Label1.Text = "Called DPspace"
        MainForm.Label1.Refresh()
        Shell("FSDualSpaceKW.exe", AppWinStyle.NormalFocus, True)

        MainForm.Label1.Text = "Summarizing"
        MainForm.Label1.Refresh()



        'TRANSLATE THE DP SOLUTION BACK INTO DUAL PLAN POLYGON SOLUTION FILE FORMAT
        'C:\Analysis\KWVB\TestRuns\TestRun1\DPSpaceFiles\Output\Design2_Lim900000A_AAschedule_sf1.csv

        ReDim NSubForSD(AAnDataSet)

        MaxSubForSD = 0
        ReDim SubForSubDivWindowCount(NSubFor, MaxSubForSD), SubForSubDivNStands(NSubFor, MaxSubForSD), bSubForSubDivSinglePass(NSubFor, MaxSubForSD)
        ReDim SubForSDAspatNPV(NSubFor, MaxSubForSD), SubForSDSpatNPV(NSubFor, MaxSubForSD) ', SubForSDMarketNPV(NSubFor, MaxSubForSD)
        If bReportBySD = True Then
            If DoCycles = False Then
                MaxStaticSubForSD = 0
                ReDim StaticSubForSubDivNStands(NSubFor, MaxStaticSubForSD)
            End If
            ReDim StaticSubForSDAspatNPV(NSubFor, MaxStaticSubForSD), StaticAASubForSDSpatNPV(NSubFor, MaxStaticSubForSD)
        End If


        For jaaset As Integer = 1 To AAnDataSet
            'RedimLandValArrays()
            If jaaset < AAnDataSet Then
                kaaset = kaaset + kaasetdirection
            End If

            If DoCycles = False And bReportBySD = True Then SubDivisionStaticInformation(BaseSDInfoFileName & CStr(kaaset) & ".csv", BaseSDRXLUTFileName & "." & CStr(kaaset))
            'do the translation
            TranslateDPSpaceSol(FnDPspaceOutBaseA & "_AAschedule_sf" & CStr(kaaset) & ".csv", FnDualPlanOutBaseA & "_RxLUT." & CStr(kaaset), IterationNumber & cyclenum)
            'find information on subdivisions - from AA LUT to whether you are scheduled in one pass or not
            SubDivisionInformation(BaseNameOutFilesA & "_subdivAA_info_sf" & CStr(kaaset) & ".csv", FnDPformOutBaseA & "_Iter" & IterationNumber & "_summary_sf" & CStr(kaaset) & ".csv")

            'write the new solution file
            DualPlanSubForests(kaaset).PolySchedFile = FnDualPlanOutBaseA & "PolySched" & kaaset & ".txt"
            WritePolySolFile(DualPlanSubForests(kaaset).PolySchedFile, kaaset, PolySolRx_ChosenRxInd)


            'tally the AA NPV
            AANPVTally(PolySolRx_ChosenRxInd, PolySolRx_AspatNPV, PolySolRx_SpatNPV, PolySolRx_MarketNPV) ', PolySolRx_DualExcessNPV)
            AspatialTally()
            SpatialTally()

            If (bConsiderHowManyFileWrite And HowmanytoRewrite >= NiterToRewrite) Or (bConsiderDepFileWrite And TotalDeviations < DeviationWriteFile) Then
                If HowmanytoRewrite >= NiterToRewrite Then
                    WriteIterOutputFiles(LTrim(Str(IterationNumber)), PolySolRx_ChosenRxInd)
                Else 'LOGIC: TotalDeviations is from last iteration, therefore, you should use last iteration's schedule to write the outputs...
                    ReDim PolySolArray(AA_NPolys)
                    PolySolFromLastIteration(kaaset, PolySolArray)
                    WriteIterOutputFiles(LTrim(Str(IterationNumber - 1)), PolySolArray)
                End If
                GC.Collect()
            End If

            'now, tally the total pnv for this subforest
            SubforNPVTally(kaaset, True, bReportBySD, True) 'also identififes whether you have a better subdivision-level solution

            If bOneOptFollowUp = False Then
                If bLastCycle = True Then
                    If jaaset = 1 Then
                        ClearFlowArrays()
                        RedimPNVArrays()
                    End If
                    ReDim PolySolRx_AspatNPV(AA_NPolys)
                    ReDim PolySolRx_SpatNPV(AA_NPolys)
                    ReDim PolySolRx_MarketNPV(AA_NPolys)
                    'ReDim PolySolRx_DualExcessNPV(AA_NPolys)
                    'clear out values
                    For jsd As Integer = 1 To MaxStaticSubForSD
                        StaticSubForSDAspatNPV(kaaset, jsd) = 0
                        StaticAASubForSDSpatNPV(kaaset, jsd) = 0
                    Next

                    SubForAspatNPV(kaaset) = 0
                    AASubForSpatNPV(kaaset) = 0
                    SubForMarketNPV(kaaset) = 0
                    SubForSpatNPV(kaaset) = 0

                    CollectBestSubdivRx(kaaset) 'use best solution for last iteration
                    'tally the AA NPV
                    AANPVTally(PolySolRx_ChosenRxInd, PolySolRx_AspatNPV, PolySolRx_SpatNPV, PolySolRx_MarketNPV) ', PolySolRx_DualExcessNPV)
                    AspatialTally()
                    SpatialTally()


                    If bConsiderHowManyFileWrite And HowmanytoRewrite >= NiterToRewrite Then
                        WriteIterOutputFiles(LTrim(Str(IterationNumber)), PolySolRx_ChosenRxInd)
                        GC.Collect()
                    End If

                    'now, tally the total pnv for this subforest
                    SubforNPVTally(kaaset, True, bReportBySD, True)
                End If
                'save spatial stuff if you need to...

                SavePolySol(kaaset, bSchedSaveLock, bUnschedSaveLock, ikschedlock, ikunschedlock, cyclenum)

            End If

        Next jaaset
        MainForm.Label1.Text = "Iteration Finished"
        MainForm.Label1.Refresh()

        'Read updated filenames and sizes for linking to DPspace polygon schedules
        'Call ReadDPdataFilenames()

    End Sub

    Public Sub SCHEDULE_OneOpt(ByVal bSchedSaveLock As Boolean, ByVal bUnschedSaveLock As Boolean, ByVal ikschedlock As Integer, ByVal ikunschedlock As Integer, ByVal bConsiderDepFileWrite As Boolean, ByVal bConsiderHowManyFileWrite As Boolean, ByVal bAfterDP As Boolean, ByVal bLastCycle As Boolean, ByVal cyclenum As Integer)
        'subroutine expands on the regular dualplan schedule subroutine and includes values for spatial interactions
        'between prescriptions chosen from the previous dualplan iteration
        'for each polygon, this considers how its neighbors are managed and chooses (and stores) the best schedule
        'subsequently evaluated polygons may be evaluated relative to the chosen polygon schedule and may or may not
        'keep their original schedule

        '' '' ''sub trims out spatial max prescriptions that are less than the lowest non-spatial NPV value
        '' '' '' so...even if the stand got credit for ALL of the interior space it could create + outside its borders, it couldn't compete 
        '' '' '' with the best "NPVLbnd" - 'spatial minimum' Rx, then trim it out...

        'uses the AA, AAPres, AAFlow as well as the Spatial, Market, and Condition set shadow price data
        'to calculate the NPV values for all DualPlan model 1 prescriptions...

        Dim relabel As Integer
        Dim kaatotal As Integer
        Dim PolySolArray() As Long
        Dim ProposedRx() As Long

        '  Evaluate existing stands one at a time

        kaatotal = 0
        relabel = 0

        ' Loop through the set of input data.
        ' Data is stored in blocks of AA's with multiple files for each block

        ClearFlowArrays()
        RedimPNVArrays()
        If bReportBySD = True Then
            ReDim StaticSubForSDAspatNPV(NSubFor, MaxStaticSubForSD), StaticAASubForSDSpatNPV(NSubFor, MaxStaticSubForSD)
        End If

        For jaaset As Integer = 1 To AAnDataSet
            ReDim PolySolRx_AspatNPV(AA_NPolys)
            ReDim PolySolRx_SpatNPV(AA_NPolys)
            ReDim PolySolRx_MarketNPV(AA_NPolys)
            ReDim ProposedRx(AA_NPolys)
            'ReDim PolySolRx_DualExcessNPV(AA_NPolys)
            'populate the proposedrx array
            'CHEATING - wouldn't really have this loaded if there were more than one subforest!
            For jaa As Integer = 1 To AA_NPolys
                ProposedRx(jaa) = PolySolRx_ChosenRxInd(jaa)
            Next

            SpatialScheduleRefine(ProposedRx)
            'check to see if you want to accept the solution..but wait, choosing a 1-opt guarantees better global optimum, so don't really have to check
            CheckSolution(ProposedRx, PolySolRx_ChosenRxInd, bAfterDP)

            'what do all these schedules mean?
            Call AANPVTally(PolySolRx_ChosenRxInd, PolySolRx_AspatNPV, PolySolRx_SpatNPV, PolySolRx_MarketNPV) ', PolySolRx_DualExcessNPV)
            Call AspatialTally()
            Call SpatialTally()

            If (bConsiderHowManyFileWrite And HowmanytoRewrite >= NiterToRewrite) Or (bConsiderDepFileWrite And TotalDeviations < DeviationWriteFile) Then
                If HowmanytoRewrite >= NiterToRewrite Then
                    WriteIterOutputFiles(LTrim(Str(IterationNumber)), PolySolRx_ChosenRxInd)
                Else 'LOGIC: TotalDeviations is from last iteration, therefore, you should use last iteration's schedule to write the outputs...
                    ReDim PolySolArray(AA_NPolys)
                    PolySolFromLastIteration(kaaset, PolySolArray)
                    WriteIterOutputFiles(LTrim(Str(IterationNumber - 1)), PolySolArray)
                End If
                GC.Collect()
            End If

            'now, tally the total pnv for this subforest
            SubforNPVTally(kaaset, bAfterDP, bReportBySD, bAfterDP)


            If bLastCycle = True Then
                If jaaset = 1 Then
                    ClearFlowArrays()
                    RedimPNVArrays()
                End If

                ReDim PolySolRx_AspatNPV(AA_NPolys)
                ReDim PolySolRx_SpatNPV(AA_NPolys)
                ReDim PolySolRx_MarketNPV(AA_NPolys)
                'ReDim PolySolRx_DualExcessNPV(AA_NPolys)
                'clear out values
                For jsd As Integer = 1 To MaxStaticSubForSD
                    StaticSubForSDAspatNPV(kaaset, jsd) = 0
                    StaticAASubForSDSpatNPV(kaaset, jsd) = 0
                Next

                SubForAspatNPV(kaaset) = 0
                AASubForSpatNPV(kaaset) = 0
                SubForMarketNPV(kaaset) = 0
                SubForSpatNPV(kaaset) = 0

                CollectBestSubdivRx(kaaset) 'use best solution for last iteration
                'tally the AA NPV
                AANPVTally(PolySolRx_ChosenRxInd, PolySolRx_AspatNPV, PolySolRx_SpatNPV, PolySolRx_MarketNPV) ', PolySolRx_DualExcessNPV)
                AspatialTally()
                SpatialTally()
                'now, tally the total pnv for this subforest

                If bConsiderHowManyFileWrite And HowmanytoRewrite >= NiterToRewrite Then
                    WriteIterOutputFiles(LTrim(Str(IterationNumber)), PolySolRx_ChosenRxInd)
                    GC.Collect()
                End If
                SubforNPVTally(kaaset, True, bReportBySD, bAfterDP)
            End If

            SavePolySol(kaaset, bSchedSaveLock, bUnschedSaveLock, ikschedlock, ikunschedlock, cyclenum) 'save the solution to this kaaset/iteration


            If jaaset < AAnDataSet Then
                DualPlanSubForests(kaaset).PolySchedFile = FnDualPlanOutBaseA & "PolySched" & kaaset & ".txt"
                WritePolySolFile(DualPlanSubForests(kaaset).PolySchedFile, kaaset, PolySolRx_ChosenRxInd) 'record the latest scheduled stuff for this stand

                kaaset = kaaset + kaasetdirection

                'ReDim DualPlanAAPoly(AA_NPolys)
                ReDim DualPlanAAPoly_polyid(AA_NPolys)
                'ReDim DualPlanAAPoly_PassToSpace(, )  'true means keep, false means it gets filtered...
                ReDim DualPlanAAPoly_ISpaceSelf(AA_NPolys)  ' the area of the 1-way influence zone within the borders of this stand
                ReDim DualPlanAAPoly_NPVallMx(AA_NPolys)  'The maximum NPVValMxSpat - dimensioned by stand
                'ReDim DualPlanAAPoly_NPVSelf(, )  'by prescription, sum of NPVAspatial and the guaranteed interior value of the stand - Includes infinite series of those spatial flows...
                ReDim DualPlanAAPoly_NPValLbnd(AA_NPolys)  'aka NPVMin, or largest NPVSelf 
                ReDim DualPlanAAPoly_EstISpacePct(AA_NPolys)  'estimated percentage of the 'edge' area that is in ispace for the poly - a function of npvlbnd and npvmax
                'ReDim DualPlanAAPoly_StandID(AA_NPolys)  'Stand ID of the stand in question
                ReDim DualPlanAAPoly_NRxIn(AA_NPolys)  'number of non-trimmed prescriptions of the polygon
                ReDim DualPlanAAPoly_PolyRxSpace(AA_NPolys)  '0/1 based on whether the stand has a non-trimmed spatial prescription
                ReDim DualPlanAAPoly_NIZones(AA_NPolys)
                'ReDim DualPlanAAPoly_IZones(, )
                'ReDim PolySolRx_PolyInd(DualPlanSubForests(kaaset).NPolys)
                'ReDim PolySolRx_PolyInd(DualPlanSubForests(kaaset).NPolys)
                ReDim PolySolRx_ChosenRxInd(AA_NPolys)

                LoadSubForest(DualPlanSubForests(kaaset))

                'NEED TO LOAD IZONES
                LoadIZones(DualPlanSubForests(kaaset).IZonesFile)
                'need to load NPVSelf calculations
                CalcNPVSelfValue(DualPlanAAPoly_NPVSelf)
                'also need to load latest polyschedules - first iteration will not have previously recorded schedules!
                LoadPolySolFile(DualPlanSubForests(kaaset).PolySchedFile)

            End If

        Next jaaset
        kaasetdirection = kaasetdirection * (-1)

        MainForm.TxtPolyDone.Text = Str(kaatotal)
        MainForm.Label1.Text = "Finished this iteration"
        MainForm.Label1.Refresh()

    End Sub
    Public Sub SCHEDULE_NoSpace(ByRef ProposedRx() As Long)
        Dim NRx As Integer
        Dim jaa As Integer
        Dim krx As Integer
        Dim kind As Integer
        Dim kspat As Integer
        Dim AAArea As Double 'the area of the AA
        Dim AANPV As Single
        Dim BestAApres As Integer
        Dim mxNPV As Single
        Dim relabel As Integer
        Dim kaatotal As Integer
        Dim NRxChange As Integer 'the number of prescriptions that this iteration changes...loop until it all 'shakes out'


        '  Evaluate existing stands one at a time

        kaatotal = 0
        relabel = 0

        ' Loop through the set of input data.
        ' Data is stored in blocks of AA's with multiple files for each block

        'For jaaset As Integer = 1 To AAnDataSet
        NRxChange = AA_NPolys
        For iaa As Integer = 1 To AA_NPolys 'AASetNAA(kaaset)
     
            jaa = iaa
            Do Until AA_polyid(jaa) > 0 'control for blank aa information
                jaa = jaa + 1
                If jaa > AA_NPolys Then Exit For
            Loop

            If relabel > 4999 Then
                MainForm.TxtPolyDone.Text = Str(kaatotal)
                MainForm.TxtPolyDone.Refresh()
                MainForm.Refresh()
                relabel = 0
            End If

            kaatotal = kaatotal + 1
            relabel = relabel + 1

            AAArea = AA_area(jaa)
            NRx = AA_NPres(jaa) 'AA(jaa).PresArray.Length - 1
            mxNPV = -9999999
            BestAApres = 0

            'find the prescription for this AA that has the highest value, as lined up with its neighbors.
            For jrx As Integer = 1 To NRx 'jpresAA = AA(KAA).firstpres To AA(KAA).lastpres
                krx = AA_PresArray(jaa, jrx) 'krx used to access line in prescription database
                kind = AApres_ISpaceInd(krx)
                kspat = AApres_SpaceType(krx)

                ''First, find the value of the aspatial flows
                AANPV = DualPlanAAPoly_NPVSelf(jaa, jrx) * AAArea

                If AANPV > mxNPV Then
                    mxNPV = AANPV
                    BestAApres = krx
                End If
            Next jrx

            'catch the prescription with the highest npv - and track how many polys are changing Rx
            If ProposedRx(jaa) <> BestAApres Then
                'NRxChange = NRxChange + 1
                ProposedRx(jaa) = BestAApres
            End If

        Next iaa

    End Sub

    Public Sub SpatialScheduleRefine(ByRef ProposedRx() As Long)
        'This refines an aspatial schedule before you go to the tally routines and price adjustment routines...no subforest information
        'called from this subroutine

        'subroutine expands on the regular dualplan schedule subroutine and includes values for spatial interactions
        'between prescriptions chosen from the previous dualplan iteration
        'for each polygon, this considers how its neighbors are managed and chooses (and stores) the best schedule
        'subsequently evaluated polygons may be evaluated relative to the chosen polygon schedule and may or may not
        'keep their original schedule

        'sub trims out spatial max prescriptions that are less than the lowest non-spatial NPV value
        ' so...even if the stand got credit for ALL of the interior space it could create + outside its borders, it couldn't compete 
        ' with the best "NPVLbnd" - 'spatial minimum' Rx, then trim it out...

        'uses the AA, AAPres, AAFlow as well as the Spatial, Market, and Condition set shadow price data
        'to calculate the NPV values for all DualPlan model 1 prescriptions...
        Dim NRx As Integer
        Dim jaa As Integer
        Dim kaa As Integer
        Dim krx As Integer
        Dim kind As Integer
        Dim kspat As Integer
        Dim AAArea As Double 'the area of the AA
        Dim zoneID As Integer
        Dim TicAreaVal() As Double 'for all stands influenced by the stand, what's the per-tic potential area in each time period?
        Dim ZoneAreaVal() As Double 'for the evaluated zone, what's the potential ISpace by Tic independent of the stand being evaluated, but accounting for the chosen schedules of the other two stands
        Dim AANPV As Single
        Dim BestAApres As Integer
        Dim mxNPV As Single
        Dim relabel As Integer
        Dim kaatotal As Integer
        Dim ZoneArea As Double 'total area in the influence zone
        Dim NRxChange As Integer 'the number of prescriptions that this iteration changes...loop until it all 'shakes out'

        Dim EntAdjFactor As Double 'adjustment factor for size-based entry cost
        Dim AAEvaluated() As Byte


        '#############This added for debug; can compute efficiency another place
        ' Dim ZoneEff(,) As Single 'efficiency of the zone in creating Interior space - used to modify/calculate assumed added value to the stand, by zone, tic
        Dim Efficiency As Double = 1
        'ReDim ZoneEff(NZoneSubFor, MxTic)
        'For jzone As Integer = 1 To NZoneSubFor
        '    For jtic As Integer = 1 To MxTic
        '        ZoneEff(jzone, jtic) = Efficiency
        '    Next
        'Next

        '  Evaluate existing stands one at a time

        
        kaatotal = 0
        relabel = 0

        ' Loop through the set of input data.
        ' Data is stored in blocks of AA's with multiple files for each block

        'For jaaset As Integer = 1 To AAnDataSet
        NRxChange = AA_NPolys

        Randomize()
        
        Do Until NRxChange / AA_NPolys <= OneOptStop 'used to be until nrxchange = 0
            NRxChange = 0
            ReDim AAEvaluated(AA_NPolys)
            'NAAEvaluated = 0

            For iaa As Integer = 1 To AA_NPolys 'AASetNAA(kaaset)
                If bRandomOneOpt Then
                    Dim i As Integer = CInt(Int((AA_NPolys - 1) * Rnd()) + 1)
                    jaa = i
                    'AACounter = 0
                    Do Until AAEvaluated(jaa) = 0
                        jaa = jaa + 1
                        'AACounter = AACounter + 1
                        'If AACounter > AA_NPolys Then Exit Do
                        If jaa > AA_NPolys Then jaa = 1
                        If AA_polyid(jaa) = 0 Then
                            AAEvaluated(jaa) = 1
                            'NAAEvaluated = NAAEvaluated + 1
                            'If NAAEvaluated > AA_NPolys Then Exit Do
                            iaa = iaa + 1
                            If iaa > AA_NPolys Then Exit Do
                        End If
                    Loop
                    If iaa > AA_NPolys Then Exit For
                    'you should come out with the next poly that hasn't been evaluated yet. Now mark it as evaluated
                    AAEvaluated(jaa) = 1
                Else
                    jaa = iaa
                    Do Until AA_polyid(jaa) > 0 'control for blank aa information
                        jaa = jaa + 1
                        If jaa > AA_NPolys Then Exit For
                    Loop
                End If
                'If jaa = 4204 Then Stop
                If relabel > 4999 Then
                    MainForm.TxtPolyDone.Text = Str(kaatotal)
                    MainForm.TxtPolyDone.Refresh()
                    MainForm.Refresh()
                    relabel = 0
                End If

                kaatotal = kaatotal + 1
                relabel = relabel + 1

                AAArea = AA_area(jaa)
                NRx = AA_NPres(jaa) 'AA(jaa).PresArray.Length - 1
                mxNPV = -9999999
                BestAApres = 0

                'If iaa = 2075 Then Stop

                'look at the schedule of the neighbors and figure out the maximum area in this stand that 
                'would be in ISpace if the evaluated schedule matches with them

                'figure out the influenced stands to evaluate prescriptions against...
                'go through influence zones this stand is in...find the potential spatial added value regardless of 
                ' the prescription chosen by the stand in question

                ReDim TicAreaVal(MxTic)
                ReDim ZoneAreaVal(MxTic)

                For jiz As Integer = 1 To DualPlanAAPoly_NIZones(jaa)
                    zoneID = DualPlanAAPoly_IZones(jaa, jiz)
                    ReDim ZoneAreaVal(MxTic)
                    'If KeepRx = 1 Then Exit For

                    For jtic As Integer = 1 To MxTic
                        ZoneAreaVal(jtic) = 1 'default to 1
                    Next
                    ZoneArea = 0
                    For jjaa As Integer = 1 To Zonedim(zoneID)
                        kaa = ZoneAAlist(zoneID, jjaa)
                        If kaa <> jaa Then
                            'krx = PolySolRx(kaa).ChosenRxInd
                            krx = ProposedRx(kaa)
                            kind = AApres_ISpaceInd(krx)
                            For jtic As Integer = 1 To MxTic
                                ZoneAreaVal(jtic) = ZoneAreaVal(jtic) * SpatialSeries(kind).Flows(jtic)
                            Next
                        End If
                        'If kaa = jaa And Zonedim(zoneID) > 1 Then ZoneArea = ZoneArea + ZoneAAlistAreaS(zoneID, jjaa) 'includes currently analyzed stand
                        ZoneArea = ZoneArea + ZoneAAlistAreaS(zoneID, jjaa) 'includes currently analyzed stand
                    Next

                    For jtic As Integer = 1 To MxTic
                        TicAreaVal(jtic) = TicAreaVal(jtic) + ZoneAreaVal(jtic) * ZoneArea * Efficiency 'ZoneEff(zoneID, jtic)
                    Next
                    ''End If
                Next jiz

                'find the prescription for this AA that has the highest value, as lined up with its neighbors.
                For jrx As Integer = 1 To NRx 'jpresAA = AA(KAA).firstpres To AA(KAA).lastpres
                    krx = AA_PresArray(jaa, jrx) 'krx used to access line in prescription database
                    kind = AApres_ISpaceInd(krx)
                    kspat = AApres_SpaceType(krx)

                    ''First, find the value of the aspatial flows
                    AANPV = DualPlanAAPoly_NPVSelf(jaa, jrx) * AAArea
                    'AANPV = AARxNPV(jaa, jrx) * AAArea

                    'Then, add possible spatial value of the whole footprint - the only part of the overall NPV that will change is within the scope of the 
                    '   stands this AA/Rx influences
                    ' The Rx either aligns or it doesn't - if not, then the whole area and value of the zone is off the table for that TIC
                    For jtic As Integer = SpatialSeries(kind).First1Tic To SpatialSeries(kind).Last1tic '#####NTic + 1
                        AANPV = AANPV + SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * (TicAreaVal(jtic)) 'want it on the total - not a per-acre basis
                    Next jtic

                    If AANPV > mxNPV Then
                        mxNPV = AANPV
                        BestAApres = krx
                    End If
                Next jrx

                'catch the prescription with the highest npv - and track how many polys are changing Rx
                If ProposedRx(jaa) <> BestAApres Then
                    NRxChange = NRxChange + 1
                    ProposedRx(jaa) = BestAApres
                End If

            Next iaa
        Loop

    End Sub

    Public Sub Deviations(ByRef TotViolationDev As Double, ByRef TotExcessDev As Double)
        'Calculates the market and condition set % deviations from desired constraint levels.
        'Deviations are used in the price adjustment routines...a rule of thumb is the higher the
        'deviation, the higher the price adjustment.
        'Dim jCondSet As Short

        '##############DO WE WANT TO CONSIDER AN ABSOLUTE DEVIATION THRESHOLD -- LIKE 1 ACRE -- TO PREVENT SMALL (0) TARGETS FROM 
        'GETTING A SHADOW PRICE AT ALL???

        Dim quanprior As Single
        Dim MnLimit As Single
        Dim MxLimit As Single
        Dim quan As Single
        Dim wlower As Single
        Dim ktry As Short
        Dim klower As Short
        Dim kupper As Short
        Dim dprice As Single
        Dim ktable As Short
        Dim jtic As Short
        'Dim jmset As Short
        'track total deviations - constraint violation
        Dim MSetViolationDev As Double = 0
        Dim SSetViolationDev As Double = 0
        Dim CSetViolationDev As Double = 0
        Dim MSetViolationDevVal As Double = 0
        Dim SSetViolationDevVal As Double = 0
        Dim CSetViolationDevVal As Double = 0
        Dim MSetValueDev As Double = 0
        Dim SSetValueDev As Double = 0
        Dim CSetValueDev As Double = 0

        TOT_EXCESSDEVVAL = 0 'financial value of excesses - added to financial NPV to estimate the true primal OF Value
        TOT_VIOLDEVVAL = 0

        Dim MSetViolationDevBySet() As Double
        Dim SSetViolationDevBySet() As Double
        Dim CSetViolationDevBySet() As Double
        'track total deviations - in excess of constraints
        Dim MSetExcessDev As Double = 0
        Dim SSetExcessDev As Double = 0
        Dim CSetExcessDev As Double = 0
        Dim MSetExcessDevVal As Double = 0
        Dim SSetExcessDevVal As Double = 0
        Dim CSetExcessDevVal As Double = 0
        Dim MSetExcessDevBySet() As Double
        Dim SSetExcessDevBySet() As Double
        Dim CSetExcessDevBySet() As Double

        Dim ViolationDev As Double '
        Dim ExcessDev As Double
        Dim ConOpt As Integer

        ReDim MSetViolationDevBySet(NMSet)
        ReDim SSetViolationDevBySet(NSpatSet)
        ReDim CSetViolationDevBySet(NCondSet)
        ReDim MSetExcessDevBySet(NMSet)
        ReDim SSetExcessDevBySet(NSpatSet)
        ReDim CSetExcessDevBySet(NCondSet)


        'ReDim CondSetDev!(NIterToSave, NCondSet, NTic)
        'ReDim MSetDev!(NIterToSave, NMSet, NTic)
        '0. Unconstrained
        '1. Target Levels
        '2. Demand = f(price)
        '3. Lower Bounds Only -- Absolute levels, >= constraints, nonnegative prices
        '4. Upper Bounds Only -- Absolute levels, <= constraints, nonpositive shadow prices
        '5. Both Upper and Lower Bounds -- Absolute levels
        '6. Limited % Changes over time with first period limits
        '7. Limited % Changes over time without first period limits

        For jmset As Integer = 1 To NMSet

            ' If Demand = f(price) then set targets to demand for current price
            'UPGRADE_WARNING: Couldn't resolve default property of object MSetConstraintOption(jmset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If MSet_ConstraintOption(jmset) = 2 Then
                For jtic = 1 To NTic
                    'UPGRADE_WARNING: Couldn't resolve default property of object MSetDemandTableID(jmset, jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    ktable = MSet_DemandTableID(jmset, jtic)
                    dprice = MSet_Pr(jmset, ksave, jtic)
                    If dprice < DemandPrice(ktable, 1) Then
                        MSet_Target(jmset, jtic) = DemandQuan(ktable, 1)
                    ElseIf dprice > DemandPrice(ktable, DemandNPoint(ktable)) Then
                        MSet_Target(jmset, jtic) = DemandQuan(ktable, DemandNPoint(ktable))
                    Else
                        kupper = DemandNPoint(ktable)
                        klower = 1
                        Do Until kupper - klower = 1
                            ktry = klower + (kupper - klower) / 2
                            If dprice > DemandPrice(ktable, ktry) Then
                                klower = ktry
                            Else
                                kupper = ktry
                            End If
                        Loop
                        wlower = (DemandPrice(ktable, kupper) - dprice) / (DemandPrice(ktable, kupper) - DemandPrice(ktable, klower))
                        MSet_Target(jmset, jtic) = wlower * DemandQuan(ktable, klower) + (1.0! - wlower) * DemandQuan(ktable, kupper)
                    End If
                Next jtic
                'UPGRADE_WARNING: Couldn't resolve default property of object MSetConstraintOption(jmset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ElseIf MSet_ConstraintOption(jmset) = 5 Then
                For jtic = 1 To NTic
                    If MSet_Flow(jmset, ksave, jtic) > MSet_FlowMx(jmset, jtic) Then
                        MSet_Target(jmset, jtic) = MSet_FlowMx(jmset, jtic)
                    ElseIf MSet_Flow(jmset, ksave, jtic) < MSet_FlowMn(jmset, jtic) Then
                        MSet_Target(jmset, jtic) = MSet_FlowMn(jmset, jtic)
                    Else
                        MSet_Target(jmset, jtic) = MSet_Flow(jmset, ksave, jtic)
                    End If
                Next jtic
            End If

            ConOpt = MSet_ConstraintOption(jmset)
            Select Case MSet_ConstraintOption(jmset)
                Case 1, 2, 3, 4, 5
                    For jtic = 1 To NTic
                        MSet_Dev(jmset, ksave, jtic) = 0
                        ViolationDev = 0
                        ExcessDev = 0
                        If MSet_Target(jmset, jtic) > 0.001 Then
                            MSet_Dev(jmset, ksave, jtic) = 100 * (-1 + MSet_Flow(jmset, ksave, jtic) / MSet_Target(jmset, jtic))
                            If ConOpt = 1 Or ConOpt = 5 Or ConOpt = 2 Then
                                ViolationDev = Math.Abs(MSet_Target(jmset, jtic) - MSet_Flow(jmset, ksave, jtic))
                            ElseIf ConOpt = 3 Then 'lower
                                If MSet_Flow(jmset, ksave, jtic) < MSet_Target(jmset, jtic) Then ViolationDev = MSet_Target(jmset, jtic) - MSet_Flow(jmset, ksave, jtic)
                                If MSet_Flow(jmset, ksave, jtic) > MSet_Target(jmset, jtic) And MSet_Pr(jmset, ksave, jtic) <> 0 Then ExcessDev = MSet_Flow(jmset, ksave, jtic) - MSet_Target(jmset, jtic)
                            ElseIf ConOpt = 4 Then 'upper
                                If MSet_Flow(jmset, ksave, jtic) > MSet_Target(jmset, jtic) Then ViolationDev = MSet_Flow(jmset, ksave, jtic) - MSet_Target(jmset, jtic)
                                If MSet_Flow(jmset, ksave, jtic) < MSet_Target(jmset, jtic) And MSet_Pr(jmset, ksave, jtic) <> 0 Then ExcessDev = MSet_Target(jmset, jtic) - MSet_Flow(jmset, ksave, jtic)
                            Else
                                ViolationDev = 0
                                ExcessDev = 0
                            End If
                            MSetViolationDevBySet(jmset) = MSetViolationDevBySet(jmset) + ViolationDev
                            MSetViolationDev = MSetViolationDev + ViolationDev
                            MSetExcessDevBySet(jmset) = MSetExcessDevBySet(jmset) + ExcessDev
                            MSetExcessDev = MSetExcessDev + ExcessDev

                            TOT_EXCESSDEVVAL = TOT_EXCESSDEVVAL + Math.Abs(ExcessDev * MSet_Pr(jmset, ksave, jtic) * MDisc(jtic))
                            TOT_VIOLDEVVAL = TOT_VIOLDEVVAL + Math.Abs(ViolationDev * MSet_Pr(jmset, ksave, jtic) * MDisc(jtic))
                            MSetExcessDevVal = MSetExcessDevVal + Math.Abs(ExcessDev * MSet_Pr(jmset, ksave, jtic) * MDisc(jtic))
                            MSetViolationDevVal = MSetViolationDevVal + Math.Abs(ViolationDev * MSet_Pr(jmset, ksave, jtic) * MDisc(jtic))

                            MSetValueDev = MSetValueDev + Math.Abs((ViolationDev + ExcessDev) * MSet_Pr(jmset, ksave, jtic) * MDisc(jtic))
                        End If
                    Next jtic

                Case 6, 7
                    For jtic = 1 To NTic
                        quan = MSet_Flow(jmset, ksave, jtic)
                        If jtic = 1 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object MSetConstraintOption(jmset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If MSet_ConstraintOption(jmset) = 7 Then
                                ' Just set dummy Mx and min limits such that neither limit is violated by quan!
                                MxLimit = quan + 1000
                                MnLimit = quan - 1000
                            Else
                                quanprior = MSet_BaseQuan(jmset)
                                If quanprior < 0.0001 Then quanprior = 0.0001
                                MxLimit = quanprior * (1 + MSet_DevMx(jmset))
                                MnLimit = quanprior * (1 - MSet_DevMn(jmset))
                            End If
                        Else
                            quanprior = MSet_Flow(jmset, ksave, jtic - 1)
                            If quanprior < 0.0001 Then quanprior = 0.0001
                            MxLimit = quanprior * (1 + MSet_DevMx(jmset))
                            MnLimit = quanprior * (1 - MSet_DevMn(jmset))
                        End If
                        If quan < MnLimit Then
                            MSet_Dev(jmset, ksave, jtic) = (-1 + quan / MnLimit) * 100
                        ElseIf quan > MxLimit Then
                            MSet_Dev(jmset, ksave, jtic) = (-1 + quan / MxLimit) * 100
                        Else
                            MSet_Dev(jmset, ksave, jtic) = 0
                        End If
                    Next jtic
            End Select

            ' set deviations to no more than 100%
            For jtic = 1 To NTic
                If MSet_Dev(jmset, ksave, jtic) > 100 Then MSet_Dev(jmset, ksave, jtic) = 100
            Next jtic
            PrintLine(setdevfilnum, IterationNumber & ", mset, " & jmset & ", " & MSetViolationDevBySet(jmset) & ", " & MSetExcessDevBySet(jmset))
        Next jmset
        'PrintLine(totdevfilnum, IterationNumber & ", mset, " & MSetAbsDeviation)

        For jCondSet As Short = 1 To NCondSet
            ' If Demand = f(price) then set targets to demand for current price
            If CondSet_ConstraintOption(jCondSet) = 2 Then
                For jtic = 1 To NTic
                    ktable = CondSet_DemandTableID(jCondSet, jtic)
                    dprice = CondSet_Pr(jCondSet, ksave, jtic)
                    If dprice < DemandPrice(ktable, 1) Then
                        CondSet_Target(jCondSet, jtic) = DemandQuan(ktable, 1)
                    ElseIf dprice > DemandPrice(ktable, DemandNPoint(ktable)) Then
                        CondSet_Target(jCondSet, jtic) = DemandQuan(ktable, DemandNPoint(ktable))
                    Else
                        kupper = DemandNPoint(ktable)
                        klower = 1
                        Do Until kupper - klower = 1
                            ktry = klower + (kupper - klower) / 2
                            If dprice > DemandPrice(ktable, ktry) Then
                                klower = ktry
                            Else
                                kupper = ktry
                            End If
                        Loop
                        wlower = (DemandPrice(ktable, kupper) - dprice) / (DemandPrice(ktable, kupper) - DemandPrice(ktable, klower))
                        CondSet_Target(jCondSet, jtic) = wlower * DemandQuan(ktable, klower) + (1.0! - wlower) * DemandQuan(ktable, kupper)
                    End If
                Next jtic
            ElseIf CondSet_ConstraintOption(jCondSet) = 5 Then
                For jtic = 1 To NTic
                    If CondSet_Flow(jCondSet, ksave, jtic) > CondSet_FlowMx(jCondSet, jtic) Then
                        CondSet_Target(jCondSet, jtic) = CondSet_FlowMx(jCondSet, jtic)
                    ElseIf CondSet_Flow(jCondSet, ksave, jtic) < CondSet_FlowMn(jCondSet, jtic) Then
                        CondSet_Target(jCondSet, jtic) = CondSet_FlowMn(jCondSet, jtic)
                    Else
                        CondSet_Target(jCondSet, jtic) = CondSet_Flow(jCondSet, ksave, jtic)
                    End If
                Next jtic
            End If

            'End If
            ConOpt = CondSet_ConstraintOption(jCondSet)
            Select Case CondSet_ConstraintOption(jCondSet)
                Case 1, 2, 3, 4, 5
                    For jtic = 1 To NTic
                        CondSet_Dev(jCondSet, ksave, jtic) = 0
                        ViolationDev = 0
                        ExcessDev = 0
                        If CondSet_Target(jCondSet, jtic) > 0.001 Then
                            CondSet_Dev(jCondSet, ksave, jtic) = 100 * (-1 + CondSet_Flow(jCondSet, ksave, jtic) / CondSet_Target(jCondSet, jtic))
                            If ConOpt = 1 Or ConOpt = 5 Or ConOpt = 2 Then
                                ViolationDev = Math.Abs(CondSet_Target(jCondSet, jtic) - CondSet_Flow(jCondSet, ksave, jtic))
                            ElseIf ConOpt = 3 Then 'lower
                                If CondSet_Flow(jCondSet, ksave, jtic) < CondSet_Target(jCondSet, jtic) Then ViolationDev = CondSet_Target(jCondSet, jtic) - CondSet_Flow(jCondSet, ksave, jtic)
                                If CondSet_Flow(jCondSet, ksave, jtic) > CondSet_Target(jCondSet, jtic) And CondSet_Pr(jCondSet, ksave, jtic) <> 0 Then ExcessDev = CondSet_Flow(jCondSet, ksave, jtic) - CondSet_Target(jCondSet, jtic)
                            ElseIf ConOpt = 4 Then 'upper
                                If CondSet_Flow(jCondSet, ksave, jtic) > CondSet_Target(jCondSet, jtic) Then ViolationDev = CondSet_Flow(jCondSet, ksave, jtic) - CondSet_Target(jCondSet, jtic)
                                If CondSet_Flow(jCondSet, ksave, jtic) < CondSet_Target(jCondSet, jtic) And CondSet_Pr(jCondSet, ksave, jtic) <> 0 Then ExcessDev = CondSet_Target(jCondSet, jtic) - CondSet_Flow(jCondSet, ksave, jtic)
                            Else
                                ViolationDev = 0
                                ExcessDev = 0
                            End If
                            CSetViolationDevBySet(jCondSet) = CSetViolationDevBySet(jCondSet) + ViolationDev
                            CSetViolationDev = CSetViolationDev + ViolationDev
                            CSetExcessDevBySet(jCondSet) = CSetExcessDevBySet(jCondSet) + ExcessDev
                            CSetExcessDev = CSetExcessDev + ExcessDev

                            TOT_EXCESSDEVVAL = TOT_EXCESSDEVVAL + Math.Abs(ExcessDev * CondSet_Pr(jCondSet, ksave, jtic) * CondDisc(jtic))
                            TOT_VIOLDEVVAL = TOT_VIOLDEVVAL + Math.Abs(ViolationDev * CondSet_Pr(jCondSet, ksave, jtic) * CondDisc(jtic))
                            CSetExcessDevVal = CSetExcessDevVal + Math.Abs(ExcessDev * CondSet_Pr(jCondSet, ksave, jtic) * CondDisc(jtic))
                            CSetViolationDevVal = CSetViolationDevVal + Math.Abs(ViolationDev * CondSet_Pr(jCondSet, ksave, jtic) * CondDisc(jtic))

                            CSetValueDev = CSetValueDev + Math.Abs((ViolationDev + ExcessDev) * CondSet_Pr(jCondSet, ksave, jtic) * CondDisc(jtic))
                        End If
                    Next jtic

                Case 6, 7
                    For jtic = 1 To NTic
                        quan = CondSet_Flow(jCondSet, ksave, jtic)
                        If jtic = 1 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object CondSetConstraintOption(jCondSet). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If CondSet_ConstraintOption(jCondSet) = 7 Then
                                ' Just set dummy Mx and Mn limits such that neither limit is violated by quan!
                                MxLimit = quan + 1000
                                MnLimit = quan - 1000
                            Else
                                quanprior = CondSet_BaseQuan(jCondSet)
                                If quanprior < 0.0001 Then quanprior = 0.0001
                                MxLimit = quanprior * (1 + CondSet_DevMx(jCondSet))
                                MnLimit = quanprior * (1 - CondSet_DevMn(jCondSet))
                            End If
                        Else
                            quanprior = CondSet_Flow(jCondSet, ksave, jtic - 1)
                            If quanprior < 0.0001 Then quanprior = 0.0001
                            MxLimit = quanprior * (1 + CondSet_DevMx(jCondSet))
                            MnLimit = quanprior * (1 - CondSet_DevMn(jCondSet))
                        End If
                        If quan < MnLimit Then
                            CondSet_Dev(jCondSet, ksave, jtic) = 100 * (-1 + quan / MnLimit)
                        ElseIf quan > MxLimit Then
                            CondSet_Dev(jCondSet, ksave, jtic) = 100 * (-1 + quan / MxLimit)
                        Else
                            CondSet_Dev(jCondSet, ksave, jtic) = 0
                        End If
                    Next jtic
            End Select

            ' set deviations to more than 100%
            For jtic = 1 To NTic
                If CondSet_Dev(jCondSet, ksave, jtic) > 100 Then CondSet_Dev(jCondSet, ksave, jtic) = 100
            Next jtic
            PrintLine(setdevfilnum, IterationNumber & ", cset, " & jCondSet & ", " & CSetViolationDevBySet(jCondSet) & ", " & CSetExcessDevBySet(jCondSet))
        Next jCondSet
        'PrintLine(totdevfilnum, IterationNumber & ", cset, " & CSetAbsDeviation)

        For jSpatSet As Short = 1 To NSpatSet
            ' If Demand = f(price) then set targets to demand for current price
            If SpatSet_ConstraintOption(jSpatSet) = 2 Then
                For jtic = 1 To NTic
                    ktable = SpatSet_DemandTableID(jSpatSet, jtic)
                    dprice = SpatSet_Pr(jSpatSet, ksave, jtic)
                    If dprice < DemandPrice(ktable, 1) Then
                        SpatSet_Target(jSpatSet, jtic) = DemandQuan(ktable, 1)
                    ElseIf dprice > DemandPrice(ktable, DemandNPoint(ktable)) Then
                        SpatSet_Target(jSpatSet, jtic) = DemandQuan(ktable, DemandNPoint(ktable))
                    Else
                        kupper = DemandNPoint(ktable)
                        klower = 1
                        Do Until kupper - klower = 1
                            ktry = klower + (kupper - klower) / 2
                            If dprice > DemandPrice(ktable, ktry) Then
                                klower = ktry
                            Else
                                kupper = ktry
                            End If
                        Loop
                        wlower = (DemandPrice(ktable, kupper) - dprice) / (DemandPrice(ktable, kupper) - DemandPrice(ktable, klower))
                        SpatSet_Target(jSpatSet, jtic) = wlower * DemandQuan(ktable, klower) + (1.0! - wlower) * DemandQuan(ktable, kupper)
                    End If
                Next jtic
            ElseIf SpatSet_ConstraintOption(jSpatSet) = 5 Then
                For jtic = 1 To NTic
                    If SpatSet_Flow(jSpatSet, ksave, jtic) > SpatSet_FlowMx(jSpatSet, jtic) Then
                        SpatSet_Target(jSpatSet, jtic) = SpatSet_FlowMx(jSpatSet, jtic)
                    ElseIf SpatSet_Flow(jSpatSet, ksave, jtic) < SpatSet_FlowMn(jSpatSet, jtic) Then
                        SpatSet_Target(jSpatSet, jtic) = SpatSet_FlowMn(jSpatSet, jtic)
                    Else
                        SpatSet_Target(jSpatSet, jtic) = SpatSet_Flow(jSpatSet, ksave, jtic)
                    End If
                Next jtic
            End If

            'End If
            ConOpt = SpatSet_ConstraintOption(jSpatSet)
            Select Case SpatSet_ConstraintOption(jSpatSet)
                Case 1, 2, 3, 4, 5
                    For jtic = 1 To NTic
                        SpatSet_Dev(jSpatSet, ksave, jtic) = 0
                        ViolationDev = 0
                        ExcessDev = 0
                        If SpatSet_Target(jSpatSet, jtic) > 0.001 Then
                            SpatSet_Dev(jSpatSet, ksave, jtic) = 100 * (-1 + SpatSet_Flow(jSpatSet, ksave, jtic) / SpatSet_Target(jSpatSet, jtic))
                            If ConOpt = 1 Or ConOpt = 5 Or ConOpt = 2 Then
                                ViolationDev = Math.Abs(SpatSet_Target(jSpatSet, jtic) - SpatSet_Flow(jSpatSet, ksave, jtic))
                            ElseIf ConOpt = 3 Then 'lower
                                If SpatSet_Flow(jSpatSet, ksave, jtic) < SpatSet_Target(jSpatSet, jtic) Then ViolationDev = SpatSet_Target(jSpatSet, jtic) - SpatSet_Flow(jSpatSet, ksave, jtic)
                                If SpatSet_Flow(jSpatSet, ksave, jtic) > SpatSet_Target(jSpatSet, jtic) And SpatSet_Pr(jSpatSet, ksave, jtic) <> 0 Then ExcessDev = SpatSet_Flow(jSpatSet, ksave, jtic) - SpatSet_Target(jSpatSet, jtic)
                            ElseIf ConOpt = 4 Then 'upper
                                If SpatSet_Flow(jSpatSet, ksave, jtic) > SpatSet_Target(jSpatSet, jtic) Then ViolationDev = SpatSet_Flow(jSpatSet, ksave, jtic) - SpatSet_Target(jSpatSet, jtic)
                                If SpatSet_Flow(jSpatSet, ksave, jtic) < SpatSet_Target(jSpatSet, jtic) And SpatSet_Pr(jSpatSet, ksave, jtic) <> 0 Then ExcessDev = SpatSet_Target(jSpatSet, jtic) - SpatSet_Flow(jSpatSet, ksave, jtic)
                            Else
                                ViolationDev = 0
                            End If
                            SSetViolationDevBySet(jSpatSet) = SSetViolationDevBySet(jSpatSet) + ViolationDev
                            SSetViolationDev = SSetViolationDev + ViolationDev
                            SSetExcessDevBySet(jSpatSet) = SSetExcessDevBySet(jSpatSet) + ExcessDev
                            SSetExcessDev = SSetExcessDev + ExcessDev

                            TOT_EXCESSDEVVAL = TOT_EXCESSDEVVAL + Math.Abs(ExcessDev * SpatSet_Pr(jSpatSet, ksave, jtic) * CondDisc(jtic))
                            TOT_VIOLDEVVAL = TOT_VIOLDEVVAL + Math.Abs(ViolationDev * SpatSet_Pr(jSpatSet, ksave, jtic) * CondDisc(jtic))
                            SSetExcessDevVal = SSetExcessDevVal + Math.Abs(ExcessDev * SpatSet_Pr(jSpatSet, ksave, jtic) * CondDisc(jtic))
                            SSetViolationDevVal = SSetViolationDevVal + Math.Abs(ViolationDev * SpatSet_Pr(jSpatSet, ksave, jtic) * CondDisc(jtic))

                            SSetValueDev = SSetValueDev + Math.Abs((ViolationDev + ExcessDev) * SpatSet_Pr(jSpatSet, ksave, jtic) * CondDisc(jtic))
                        End If
                    Next jtic
                Case 6, 7
                    For jtic = 1 To NTic
                        quan = SpatSet_Flow(jSpatSet, ksave, jtic)
                        If jtic = 1 Then
                            If SpatSet_ConstraintOption(jSpatSet) = 7 Then
                                ' Just set dummy Mx and Mn limits such that neither limit is violated by quan!
                                MxLimit = quan + 1000
                                MnLimit = quan - 1000
                            Else
                                quanprior = SpatSet_BaseQuan(jSpatSet)
                                If quanprior < 0.0001 Then quanprior = 0.0001
                                MxLimit = quanprior * (1 + SpatSet_DevMx(jSpatSet))
                                MnLimit = quanprior * (1 - SpatSet_DevMn(jSpatSet))
                            End If
                        Else
                            quanprior = SpatSet_Flow(jSpatSet, ksave, jtic - 1)
                            If quanprior < 0.0001 Then quanprior = 0.0001
                            MxLimit = quanprior * (1 + SpatSet_DevMx(jSpatSet))
                            MnLimit = quanprior * (1 - SpatSet_DevMn(jSpatSet))
                        End If
                        If quan < MnLimit Then
                            SpatSet_Dev(jSpatSet, ksave, jtic) = 100 * (-1 + quan / MnLimit)
                        ElseIf quan > MxLimit Then
                            SpatSet_Dev(jSpatSet, ksave, jtic) = 100 * (-1 + quan / MxLimit)
                        Else
                            SpatSet_Dev(jSpatSet, ksave, jtic) = 0
                        End If
                    Next jtic
            End Select

            ' set deviations to more than 100%
            For jtic = 1 To NTic
                If SpatSet_Dev(jSpatSet, ksave, jtic) > 100 Then SpatSet_Dev(jSpatSet, ksave, jtic) = 100
            Next jtic
            PrintLine(setdevfilnum, IterationNumber & ", sset, " & jSpatSet & ", " & SSetViolationDevBySet(jSpatSet) & ", " & SSetExcessDevBySet(jSpatSet))
        Next jSpatSet
        PrintLine(totdevfilnum, IterationNumber & ", msetVI, " & MSetViolationDev & ", csetVI, " & CSetViolationDev & ", ssetVI, " & SSetViolationDev & ", msetEX, " & MSetExcessDev & ", csetXE, " & CSetExcessDev & ", ssetXE, " & SSetExcessDev & ", msetVAL, " & MSetValueDev & ", csetVAL, " & CSetValueDev & ", ssetVAL, " & SSetValueDev)
        If bDeviationAbsValue = True Then
            TotViolationDev = MSetViolationDev * UseMSetViolations + CSetViolationDev * UseCSetViolations + SSetViolationDev * UseSSetViolations
            TotExcessDev = MSetExcessDev * UseMSetExcesses + CSetExcessDev * UseCSetExcesses + SSetExcessDev * UseSSetExcesses
            TotalDeviations = TotViolationDev + TotExcessDev
        Else 'find the value
            TotViolationDev = MSetViolationDevVal * UseMSetViolations + CSetViolationDevVal * UseCSetViolations + SSetViolationDevVal * UseSSetViolations
            TotExcessDev = MSetExcessDevVal * UseMSetExcesses + CSetExcessDevVal * UseCSetExcesses + SSetExcessDevVal * UseSSetExcesses
            TotalDeviations = TotViolationDev + TotExcessDev
        End If

    End Sub

    Public Sub SumVol()
        'Adds up the type flows associated with the optimal polygon schedules (SCHEDULE routine) 
        '   into the appropriate condition set "SetFlow" value for each set
        'Type flows have been summed over all subforests; these are now used to determine the 
        ' total flow for each "Set" used to constrain the model. Set flows have the additional
        ' dimension for saving iterations (ksave)

        Dim jmloc As Short
        Dim jmtype As Short
        Dim kmset As Short
        Dim kMapArea As Short
        Dim jcolor As Short
        Dim jage As Short
        Dim jcs As Short
        Dim kcondset As Short
        Dim kMap As Short
        Dim jflow As Short
        Dim jtic As Short

        ' Need to dimension these arrays at start of ieration 1
        'ReDim CondSetFlow#(NiterToSave, NCondSet, Ntic)
        'ReDim MSetFlow#(NiterToSave, NMSet, Ntic)

        ' check to see if any reinitializing is needed
        'If IterationNumber > NIterToSave Then
        For jtic = 0 To NTic
            For jCondSet As Short = 1 To NCondSet
                CondSet_Flow(jCondSet, ksave, jtic) = 0
            Next jCondSet

            For jmset As Integer = 1 To NMSet
                MSet_Flow(jmset, ksave, jtic) = 0
            Next jmset
            For jSpatset As Integer = 1 To NSpatSet
                SpatSet_Flow(jSpatset, ksave, jtic) = 0
            Next jSpatset
        Next jtic

        'End If

        For jset As Integer = 1 To NCondSet
            For jflow = 1 To CondSet_nSetDefs(jset)
                kMap = CondSet_kMapLayer(jset, jflow)
                kcondset = jset 'CondSetDef(jflow).kcondset
                For jcs = CondSet_kTypeBeg(jset, jflow) To CondSet_kTypeEnd(jset, jflow) 'CondSetDef(jflow).kCoverSiteBeg To CondSetDef(jflow).kCoverSiteEnd
                    For jage = CondSet_kagebeg(jset, jflow) To Math.Min(CondSet_kageend(jset, jflow), MxAgeClass) 'CondSetDef(jflow).kagebeg To CondSetDef(jflow).kageend
                        For jcolor = CondSet_kLayerColorBeg(jset, jflow) To CondSet_kLayerColorEnd(jset, jflow) 'CondSetDef(jflow).kLayerColorBeg To CondSetDef(jflow).kLayerColorEnd
                            kMapArea = MapAreaID(kMap, jcolor)
                            For jtic = 0 To NTic
                                CondSet_Flow(kcondset, ksave, jtic) = CondSet_Flow(kcondset, ksave, jtic) + CondTypeFlow(jcs, jage, kMapArea, jtic) * CondSet_convfact(jset, jflow) 'CondSetDef(jflow).convfact
                            Next jtic
                        Next jcolor
                    Next jage
                Next jcs
            Next jflow
        Next jset

        For jsset As Integer = 1 To NSpatSet
            For jflow = 1 To SpatSet_nSetDefs(jsset)
                For jtype As Integer = SpatSet_kTypeBeg(jflow, jsset) To SpatSet_kTypeEnd(jsset, jflow)

                    For jtic = 0 To NTic
                        SpatSet_Flow(jsset, ksave, jtic) = SpatSet_Flow(jsset, ksave, jtic) + SpatTypeFlow(jtype, jtic) * SpatSet_convfact(jsset, jflow) 'CondSetDef(jflow).convfact
                    Next jtic

                Next jtype
            Next jflow
        Next jsset

        For jmset As Integer = 1 To NMSet
            For jflow = 1 To MSet_nSetDefs(jmset)
                kmset = jmset 'MSetDef(jflow).kmset
                For jmtype = MSet_kTypeBeg(jmset, jflow) To MSet_kTypeEnd(jmset, jflow) 'MSetDef(jflow).kMTypeBeg To MSetDef(jflow).kMTypeEnd
                    For jmloc = MSet_kLocBeg(jmset, jflow) To MSet_kLocEnd(jmset, jflow) ' MSetDef(jflow).kMLocBeg To MSetDef(jflow).kMLocEnd
                        For jtic = 1 To NTic
                            MSet_Flow(kmset, ksave, jtic) = MSet_Flow(kmset, ksave, jtic) + MTypeFlow(jmtype, jmloc, jtic) / MSet_convfact(jmset, jflow) 'MSetDef(jflow).convfact
                        Next jtic
                    Next jmloc
                Next jmtype
            Next jflow
        Next jmset



    End Sub

    Public Sub SetTypePrices()
        'Uses the condition set prices to add back into the condition Type prices (above the base prices from the DualPlan input file)
        'Shadow prices are adjusted at the "set" level
        'Set prices drive type prices. AA prescriptions are evaluated based on the type prices

        Dim kmset As Short
        Dim jmloc As Short
        Dim jmtype As Short
        Dim Adjust As Single
        'Dim kcondset As Short
        Dim jflow As Short
        Dim jArea As Short
        Dim kMapArea As Short
        Dim jcolor As Short
        Dim jage As Short
        Dim jcs As Short
        Dim UnitPrice As Single
        Dim kMap As Short
        Dim jdef As Short
        Dim jmset As Short
        Dim jtic As Short
        Dim kEndSet As Short
        'Dim jCondSet As Short
        ' Set MType and BioType Prices based on unconstrained prices and current shadow price estimates

        'first clear out arrays
        ClearPriceArrays()

        'First set MSet and CondSet and SpatSet prices for period NTic1
        For jCondSet As Integer = 1 To NCondSet
            CondSet_Pr(jCondSet, ksave, NTic1) = 0
            kEndSet = CondSet_EndPrWghtSet(jCondSet)
            For jtic = 1 To NTic
                CondSet_Pr(jCondSet, ksave, NTic1) = CondSet_Pr(jCondSet, ksave, NTic1) + CondSet_Pr(jCondSet, ksave, jtic) * EndPriceWght(kEndSet, jtic)
            Next jtic
        Next jCondSet

        For jSpatSet As Integer = 1 To NSpatSet
            SpatSet_Pr(jSpatSet, ksave, NTic1) = 0
            kEndSet = SpatSet_EndPrWghtSet(jSpatSet)
            For jtic = 1 To NTic
                SpatSet_Pr(jSpatSet, ksave, NTic1) = SpatSet_Pr(jSpatSet, ksave, NTic1) + SpatSet_Pr(jSpatSet, ksave, jtic) * EndPriceWght(kEndSet, jtic)
            Next jtic
        Next jSpatSet


        For jmset = 1 To NMSet
            MSet_Pr(jmset, ksave, NTic1) = 0
            kEndSet = MSet_EndPrWghtSet(jmset)
            For jtic = 1 To NTic
                MSet_Pr(jmset, ksave, NTic1) = MSet_Pr(jmset, ksave, NTic1) + MSet_Pr(jmset, ksave, jtic) * EndPriceWght(kEndSet, jtic)
            Next jtic
        Next jmset


        ' For Condition Type Flows, First set base prices without constraint impacts
        ' ReDim CondTypePr(Ncoversite, MxAgeClass, NMapArea, MxTic)
        For jdef = 1 To CondTypeNDef
            kMap = CondTypePrDef_MapLayer(jdef)
            For jtic = CondTypePrDef_TicBeg(jdef) To CondTypePrDef_TicEnd(jdef)
                UnitPrice = CondTypePrDef_price(jdef) / CondTypePrDef_Nunits(jdef) * CondDisc(jtic)
                For jcs = CondTypePrDef_CoverSiteBeg(jdef) To CondTypePrDef_CoverSiteEnd(jdef)
                    For jage = CondTypePrDef_AgeBeg(jdef) To CondTypePrDef_AgeEnd(jdef)
                        For jcolor = CondTypePrDef_LayerColorBeg(jdef) To CondTypePrDef_LayerColorEnd(jdef)
                            kMapArea = MapAreaID(kMap, jcolor)
                            CondTypePr(jcs, jage, kMapArea, jtic) = UnitPrice
                        Next jcolor
                    Next jage
                Next jcs
            Next jtic
        Next jdef
        For jcs = 1 To Ncoversite
            For jage = 1 To MxAgeClass
                For jArea = 1 To NMapArea
                    For jtic = NTic1 To MxTic
                        CondTypePr(jcs, jage, jArea, jtic) = CondTypePr(jcs, jage, jArea, jtic - 1) * OneTicDisc
                    Next jtic
                Next jArea
            Next jage
        Next jcs

        ' For Condition Type Flows, adjust prices for constraint impacts
        For jCondSet As Integer = 1 To NCondSet
            For jflow = 1 To CondSet_nSetDefs(jCondSet)
                kMap = CondSet_kMapLayer(jCondSet, jflow)
                Dim kcondset As Short = jCondSet 'CondSet(jCondSet).kcondset
                For jcs = CondSet_kTypeBeg(kcondset, jflow) To CondSet_kTypeEnd(kcondset, jflow)
                    For jage = CondSet_kagebeg(kcondset, jflow) To Math.Min(CondSet_kageend(kcondset, jflow), MxAgeClass)
                        For jcolor = CondSet_kLayerColorBeg(kcondset, jflow) To CondSet_kLayerColorEnd(kcondset, jflow)
                            kMapArea = MapAreaID(kMap, jcolor)
                            For jtic = 1 To NTic1
                                Adjust = CondSet_Pr(kcondset, ksave, jtic) * CondDisc(jtic) / CondSet_convfact(kcondset, jflow)
                                CondTypePr(jcs, jage, kMapArea, jtic) = CondTypePr(jcs, jage, kMapArea, jtic) + Adjust
                            Next jtic
                            For jtic = NTic1 + 1 To MxTic
                                Adjust = CondSet_Pr(kcondset, ksave, NTic1) * CondDisc(jtic) / CondSet_convfact(kcondset, jflow)
                                CondTypePr(jcs, jage, kMapArea, jtic) = CondTypePr(jcs, jage, kMapArea, jtic) + Adjust
                            Next jtic
                        Next jcolor
                    Next jage
                Next jcs
            Next jflow
        Next jCondSet

        ' For Spatial Type Flows, First set base prices without constraint impacts
        ' ReDim SpatTypePr(NSpaceType, MxTic)
        For jdef = 1 To SpatTypeNDef
            'kMap = SpatTypePrDef(jdef).MapLayer
            For jtic = SpatTypePrDef_TicBeg(jdef) To SpatTypePrDef_TicEnd(jdef)
                UnitPrice = SpatTypePrDef_price(jdef) / SpatTypePrDef_Nunits(jdef) * CondDisc(jtic)

                SpatTypePr(jdef, jtic) = UnitPrice

            Next jtic


            For jtic = NTic1 + 1 To MxTic
                SpatTypePr(jdef, jtic) = SpatTypePr(jdef, jtic - 1) * OneTicDisc
            Next jtic

        Next jdef
        ' For Spatial Type Flows, adjust prices for constraint impacts
        For jSpatSet As Integer = 1 To NSpatSet
            For jflow = 1 To SpatSet_nSetDefs(jSpatSet)
                kMap = SpatSet_kMapLayer(jSpatSet, jflow)
                'Dim kspatset As Integer = jSpatSet 'CondSet(jCondSet).kcondset
                For jspat As Integer = SpatSet_kTypeBeg(jSpatSet, jflow) To SpatSet_kTypeEnd(jSpatSet, jflow)

                    For jtic = 1 To NTic1
                        Adjust = SpatSet_Pr(jSpatSet, ksave, jtic) * CondDisc(jtic) / SpatSet_convfact(jSpatSet, jflow)
                        SpatTypePr(jspat, jtic) = SpatTypePr(jspat, jtic) + Adjust
                    Next jtic
                    For jtic = NTic1 + 1 To MxTic
                        Adjust = SpatSet_Pr(jSpatSet, ksave, NTic1) * CondDisc(jtic) / SpatSet_convfact(jSpatSet, jflow)
                        SpatTypePr(jspat, jtic) = SpatTypePr(jspat, jtic) + Adjust
                    Next jtic

                Next jspat
            Next jflow
        Next jSpatSet

        ' For Market Type Flows, first set base prices without constraint impacts
        'UPGRADE_WARNING: Lower bound of array MtypePr was changed from 1,1,1 to 0,0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
        ' ReDim MtypePr(NMType, NMLocation, MxTic)
        For jdef = 1 To MTypeNDef
            For jtic = MTypePrDef_TicBeg(jdef) To MTypePrDef_TicEnd(jdef)
                UnitPrice = MTypePrDef_price(jdef) / MTypePrDef_Nunits(jdef) * MDisc(jtic)
                For jmtype = MTypePrDef_TypeBeg(jdef) To MTypePrDef_TypeEnd(jdef)
                    For jmloc = MTypePrDef_MLocBeg(jdef) To MTypePrDef_MLocEnd(jdef)
                        MtypePr(jmtype, jmloc, jtic) = UnitPrice
                    Next jmloc
                Next jmtype
            Next jtic
        Next jdef
        For jmtype = 1 To NMType
            For jmloc = 1 To NMLocation
                For jtic = NTic1 + 1 To MxTic
                    MtypePr(jmtype, jmloc, jtic) = MtypePr(jmtype, jmloc, jtic - 1) * OneTicDisc
                Next jtic
            Next jmloc
        Next jmtype

        ' For Market Type Flows, adjust prices for constraint impacts
        For jmset = 1 To NMSet
            For jflow = 1 To MSet_nSetDefs(jmset)
                kmset = jmset 'MSetDef(jflow).kmset()
                For jmtype = MSet_kTypeBeg(kmset, jflow) To MSet_kTypeEnd(kmset, jflow)
                    For jmloc = MSet_kLocBeg(kmset, jflow) To MSet_kLocEnd(kmset, jflow)
                        For jtic = 1 To NTic1
                            Adjust = MSet_Pr(kmset, ksave, jtic) * MDisc(jtic) / MSet_convfact(kmset, jflow)
                            MtypePr(jmtype, jmloc, jtic) = MtypePr(jmtype, jmloc, jtic) + Adjust
                        Next jtic
                        For jtic = NTic1 + 1 To MxTic
                            Adjust = MSet_Pr(kmset, ksave, NTic1) * MDisc(jtic) / MSet_convfact(kmset, jflow)
                            MtypePr(jmtype, jmloc, jtic) = MtypePr(jmtype, jmloc, jtic) + Adjust
                        Next jtic
                    Next jmloc
                Next jmtype
            Next jflow
        Next jmset
        ' set EntryCosts considering current type prices
        'UPGRADE_WARNING: Lower bound of array EntryCostTic was changed from 1,1 to 0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
        ReDim EntryCostTic(NMLocation, MxTic)
        For jmloc = 1 To NMLocation
            For jtic = 1 To MxTic
                EntryCostTic(jmloc, jtic) = MtypePr(MTypeEntryCost, jmloc, jtic) * EntryCost
            Next jtic
        Next jmloc



    End Sub

    Private Sub FloatShapeSmooth()
        ' note that FloatShapeSmoothOLD() has been saved -- old uses differences for constraint option > 5

        'Here is where the prices are adjusted based on the deviation from the desired condition
        'i.e., if the problem is infeasible, constraint shadow prices are adjusted accordingly

        ' This routine adjusts CondSetPr!() and MSetPr!() for the type of adjustment specified

        ' Dim jCondSet As Short
        Dim TotDev As Single
        Dim PrPart As Single
        Dim Break As Short
        Dim jbreak As Short
        Dim LastBreak As Short
        Dim ABSDEV As Single
        Dim jjTic As Short
        Dim WghtDev As Single
        Dim PrAdj As Single
        'Dim CompPrAdj As Single 'used for flop control
        'Dim CalledForPrAdj As Single 'flop control - modify the adjustment based on the magnitude of the flop
        Dim devpct As Single
        Dim AVEDEV As Single
        Dim jtic As Short
        'Dim jmset As Short
        Dim FlopInd As Integer

        IterNPrAdjust = 0
        For jmset As Integer = 1 To NMSet
            'UPGRADE_WARNING: Couldn't resolve default property of object MsetPrAdjSet(jmset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            kadjset = MSet_PrAdjSet(jmset)
            'UPGRADE_WARNING: Couldn't resolve default property of object MSetConstraintOption(jmset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If MSet_ConstraintOption(jmset) > 0 Then
                If AdjIterType(ksave) = "Smooth" Then
                    IterNPrAdjust = 1
                    For jtic = 1 To NTic

                        ' constraint option 7 will never adjust prices in tic 1 as MSetDev!(ksaveBase, jmset,1) is always 0
                        'UPGRADE_WARNING: Couldn't resolve default property of object MSetConstraintOption(jmset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If MSet_ConstraintOption(jmset) = 7 And jtic = 1 Then
                            AVEDEV = (MSet_Dev(jmset, ksaveBase, jtic) - MSet_Dev(jmset, ksaveBase, jtic + 1)) / 2
                        Else
                            AVEDEV = MSet_Dev(jmset, ksaveBase, jtic)
                        End If
                        devpct = System.Math.Abs(AVEDEV)
                        PrAdj = Smooth(kadjset, devpct)
                        'If AVEDEV > 0 Then PrAdj = -PrAdj

                        If bSmoothFlopControl = True And LastIterType = "Smooth" Then
                            'first, check for flop - implies you had an overadjustment
                            FlopInd = MSetAdjModIndex(jmset, jtic) 'default
                            If MSetLastIterDev(jmset, jtic) * AVEDEV < 0 Then 'had a flop
                                FlopInd = Math.Min(MSetAdjModIndex(jmset, jtic) + 1, FlopNBreaks) 'ramp down adjustment parameter - highest index holds lowest value
                                MSetFlopped(jmset, jtic) = 1
                                '92911 - get a "free pass" to the center?? - if you all of a sudden find the flop price ??
                                If FlopInd < FlopNBreaks / 2 Then FlopInd = FlopNBreaks / 2
                            ElseIf MSetLastIterDev(jmset, jtic) * AVEDEV > 0 Then
                                'If bOneWayFlopControl = True Then
                                '    'if you have not flopped and the deviation is now more or equal to the last deviation, increase the magnitude of the price adjustment
                                '    If Math.Abs(AVEDEV) >= Math.Abs(MSetLastIterDev(jmset, jtic)) And MSetFlopped(jmset, jtic) = 0 Then FlopInd = Math.Max(MSetAdjModIndex(jmset, jtic) - 1, 0)
                                'Else
                                'if you simply have not flopped since the last iteration, then increase the magnitude of the adjustment
                                If Math.Abs(AVEDEV) >= Math.Abs(MSetLastIterDev(jmset, jtic)) Then FlopInd = Math.Max(MSetAdjModIndex(jmset, jtic) - 1, 0)
                                'End If
                            End If
                            MSetAdjModIndex(jmset, jtic) = FlopInd
                            PrAdj = PrAdj * FlopFactorArray(FlopInd)
                        End If

                        If AVEDEV > 0 Then PrAdj = -PrAdj
                        MSet_Pr(jmset, ksave, jtic) = MSet_Pr(jmset, ksaveBase, jtic) + PrAdj
                        If bSmoothFlopControl = True Then
                            'MSetLastIterAdj(jmset, jtic) = PrAdj
                            MSetLastIterDev(jmset, jtic) = AVEDEV
                        End If
                    Next jtic
                ElseIf AdjIterType(ksave) = "Shape" Then
                    IterNPrAdjust = 1
                    For jtic = 1 To NTic
                        WghtDev = 0.0!
                        For jjTic = 1 To NTic
                            'If MSetConstraintOption(jmset) = 7 And jtic = 1 Then
                            'WghtDev! = WghtDev! + ShapeWght!(jtic, jjTic) * (MSetDev!(ksaveBase, jmset, jjTic) - MSetDev!(ksaveBase, jmset, jjTic) / 2)
                            'Else
                            WghtDev = WghtDev + ShapeWght(jtic, jjTic) * MSet_Dev(jmset, ksaveBase, jjTic)
                            'End If
                        Next jjTic
                        AVEDEV = WghtDev / ShapeWghtTot(jtic)
                        ABSDEV = System.Math.Abs(AVEDEV)
                        LastBreak = 0
                        PrAdj = 0
                        'UPGRADE_WARNING: Couldn't resolve default property of object ShapeNBreak(kadjset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        For jbreak = 1 To ShapeNBreak(kadjset)
                            'UPGRADE_WARNING: Couldn't resolve default property of object ShapePercentDev(kadjset, jbreak). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Break = ShapePercentDev(kadjset, jbreak)
                            If ABSDEV > Break Then
                                PrAdj = ShapePriceAdj(kadjset, jbreak)
                                LastBreak = Break
                            Else
                                PrPart = (ShapePriceAdj(kadjset, jbreak) - PrAdj) * (ABSDEV - LastBreak) / (Break - LastBreak)
                                PrAdj = PrAdj + PrPart
                                Exit For
                            End If
                        Next jbreak
                        If AVEDEV > 0 Then PrAdj = -PrAdj
                        MSet_Pr(jmset, ksave, jtic) = MSet_Pr(jmset, ksaveBase, jtic) + PrAdj
                    Next jtic
                ElseIf AdjIterType(ksave) = "Float" Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object MSetConstraintOption(jmset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If MSet_ConstraintOption(jmset) > 5 Then
                        ' for option 6 this means shifting to help fit tic 1
                        ' for option 7 this means no shift since MsetDev!() = 0

                        AVEDEV = MSet_Dev(jmset, ksaveBase, 1)

                    Else
                        TotDev = 0
                        For jtic = 1 To NTic
                            TotDev = TotDev + MSet_Dev(jmset, ksaveBase, jtic)
                        Next jtic
                        AVEDEV = TotDev / NTic
                    End If
                    ABSDEV = System.Math.Abs(AVEDEV)
                    'UPGRADE_WARNING: Couldn't resolve default property of object FloatStopPercent(kadjset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If ABSDEV > FloatStopPercent(kadjset) Then
                        IterNPrAdjust = IterNPrAdjust + 1
                        LastBreak = 0
                        PrAdj = 0
                        'UPGRADE_WARNING: Couldn't resolve default property of object FloatNBreak(kadjset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        For jbreak = 1 To FloatNBreak(kadjset)
                            'UPGRADE_WARNING: Couldn't resolve default property of object FloatPercentDev(kadjset, jbreak). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Break = FloatPercentDev(kadjset, jbreak)
                            If ABSDEV > Break Then
                                PrAdj = FloatPriceAdj(kadjset, jbreak)
                                LastBreak = Break
                            Else
                                PrPart = (FloatPriceAdj(kadjset, jbreak) - PrAdj) * (ABSDEV - LastBreak) / (Break - LastBreak)
                                PrAdj = PrAdj + PrPart
                                Exit For
                            End If
                        Next jbreak
                        If AVEDEV > 0 Then PrAdj = -PrAdj
                        For jtic = 1 To NTic
                            MSet_Pr(jmset, ksave, jtic) = MSet_Pr(jmset, ksaveBase, jtic) + PrAdj
                        Next jtic
                    Else
                        For jtic = 1 To NTic
                            MSet_Pr(jmset, ksave, jtic) = MSet_Pr(jmset, ksaveBase, jtic)
                        Next jtic
                    End If
                Else
                    IterNPrAdjust = 1
                End If
            End If
        Next jmset

        For jCondSet As Short = 1 To NCondSet
            'UPGRADE_WARNING: Couldn't resolve default property of object CondsetPrAdjSet(jCondSet). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            kadjset = CondSet_PrAdjSet(jCondSet)
            'UPGRADE_WARNING: Couldn't resolve default property of object CondSetConstraintOption(jCondSet). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If CondSet_ConstraintOption(jCondSet) > 0 Then
                If AdjIterType(ksave) = "Smooth" Then
                    IterNPrAdjust = 1
                    For jtic = 1 To NTic
                        ' constraint option 7 will never adjust prices in tic 1 as CondSetDev!(ksaveBase, jmset,1) is always 0
                        'UPGRADE_WARNING: Couldn't resolve default property of object CondSetConstraintOption(jCondSet). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If CondSet_ConstraintOption(jCondSet) = 7 And jtic = 1 Then
                            AVEDEV = (CondSet_Dev(jCondSet, ksaveBase, jtic) - CondSet_Dev(jCondSet, ksaveBase, jtic + 1)) / 2
                        Else
                            AVEDEV = CondSet_Dev(jCondSet, ksaveBase, jtic)
                        End If
                        devpct = System.Math.Abs(AVEDEV)
                        PrAdj = Smooth(kadjset, devpct)
                        'If AVEDEV > 0 Then PrAdj = -PrAdj
                        If bSmoothFlopControl = True And LastIterType = "Smooth" Then
                            FlopInd = CondSetAdjModIndex(jCondSet, jtic) 'default
                            'first, check for flop - implies you had an overadjustment
                            If CondSetLastIterDev(jCondSet, jtic) * AVEDEV < 0 Then 'had a flop
                                FlopInd = Math.Min(CondSetAdjModIndex(jCondSet, jtic) + 1, FlopNBreaks) 'ramp down adjustment parameter - highest index holds lowest value
                                CondSetFlopped(jCondSet, jtic) = 1
                                '92911 - get a "free pass" to the center?? - if you all of a sudden find the flop price ??
                                If FlopInd < FlopNBreaks / 2 Then FlopInd = FlopNBreaks / 2
                            ElseIf CondSetLastIterDev(jCondSet, jtic) * AVEDEV > 0 Then
                                'If bOneWayFlopControl = True Then
                                '    If Math.Abs(AVEDEV) >= Math.Abs(CondSetLastIterDev(jCondSet, jtic)) And CondSetFlopped(jCondSet, jtic) = 0 Then FlopInd = Math.Max(CondSetAdjModIndex(jCondSet, jtic) - 1, 0)
                                'Else
                                If Math.Abs(AVEDEV) >= Math.Abs(CondSetLastIterDev(jCondSet, jtic)) Then FlopInd = Math.Max(CondSetAdjModIndex(jCondSet, jtic) - 1, 0)
                                'End If
                            End If
                                CondSetAdjModIndex(jCondSet, jtic) = FlopInd
                                PrAdj = PrAdj * FlopFactorArray(FlopInd)
                        End If
                        If AVEDEV > 0 Then PrAdj = -PrAdj
                        CondSet_Pr(jCondSet, ksave, jtic) = CondSet_Pr(jCondSet, ksaveBase, jtic) + PrAdj
                        If bSmoothFlopControl = True Then
                            'CondSetLastIterAdj(jCondSet, jtic) = PrAdj
                            CondSetLastIterDev(jCondSet, jtic) = AVEDEV
                        End If
                    Next jtic

                ElseIf AdjIterType(ksave) = "Shape" Then
                    IterNPrAdjust = 1
                    For jtic = 1 To NTic
                        WghtDev = 0.0!
                        For jjTic = 1 To NTic
                            'If CondSetConstraintOption(jCondSet) > 5 And jtic < NTic Then
                            'WghtDev! = WghtDev! + ShapeWght!(jtic, jjTic) * (CondSetDev!(ksaveBase, jCondSet, jjTic) - CondSetDev!(ksaveBase, jCondSet, jjTic) / 2)
                            'Else
                            WghtDev = WghtDev + ShapeWght(jtic, jjTic) * CondSet_Dev(jCondSet, ksaveBase, jjTic)
                            'End If
                        Next jjTic

                        AVEDEV = WghtDev / ShapeWghtTot(jtic)
                        ABSDEV = System.Math.Abs(AVEDEV)
                        LastBreak = 0
                        PrAdj = 0
                        'UPGRADE_WARNING: Couldn't resolve default property of object ShapeNBreak(kadjset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        For jbreak = 1 To ShapeNBreak(kadjset)
                            'UPGRADE_WARNING: Couldn't resolve default property of object ShapePercentDev(kadjset, jbreak). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Break = ShapePercentDev(kadjset, jbreak)
                            If ABSDEV > Break Then
                                PrAdj = ShapePriceAdj(kadjset, jbreak)
                                LastBreak = Break
                            Else
                                PrPart = (ShapePriceAdj(kadjset, jbreak) - PrAdj) * (ABSDEV - LastBreak) / (Break - LastBreak)
                                PrAdj = PrAdj + PrPart
                                Exit For
                            End If
                        Next jbreak
                        If AVEDEV > 0 Then PrAdj = -PrAdj
                        CondSet_Pr(jCondSet, ksave, jtic) = CondSet_Pr(jCondSet, ksaveBase, jtic) + PrAdj
                    Next jtic
                ElseIf AdjIterType(ksave) = "Float" Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object CondSetConstraintOption(jCondSet). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If CondSet_ConstraintOption(jCondSet) > 5 Then
                        AVEDEV = CondSet_Dev(jCondSet, ksaveBase, 1)
                    Else
                        TotDev = 0
                        For jtic = 1 To NTic
                            TotDev = TotDev + CondSet_Dev(jCondSet, ksaveBase, jtic)
                        Next jtic
                        AVEDEV = TotDev / NTic
                    End If
                    ABSDEV = System.Math.Abs(AVEDEV)
                    'UPGRADE_WARNING: Couldn't resolve default property of object FloatStopPercent(kadjset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If ABSDEV > FloatStopPercent(kadjset) Then
                        IterNPrAdjust = IterNPrAdjust + 1
                        LastBreak = 0
                        PrAdj = 0
                        'UPGRADE_WARNING: Couldn't resolve default property of object FloatNBreak(kadjset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        For jbreak = 1 To FloatNBreak(kadjset)
                            'UPGRADE_WARNING: Couldn't resolve default property of object FloatPercentDev(kadjset, jbreak). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Break = FloatPercentDev(kadjset, jbreak)
                            If ABSDEV > Break Then
                                PrAdj = FloatPriceAdj(kadjset, jbreak)
                                LastBreak = Break
                            Else
                                PrPart = (FloatPriceAdj(kadjset, jbreak) - PrAdj) * (ABSDEV - LastBreak) / (Break - LastBreak)
                                PrAdj = PrAdj + PrPart
                                Exit For
                            End If
                        Next jbreak
                        If AVEDEV > 0 Then PrAdj = -PrAdj
                        For jtic = 1 To NTic
                            CondSet_Pr(jCondSet, ksave, jtic) = CondSet_Pr(jCondSet, ksaveBase, jtic) + PrAdj
                        Next jtic
                    Else
                        For jtic = 1 To NTic
                            CondSet_Pr(jCondSet, ksave, jtic) = CondSet_Pr(jCondSet, ksaveBase, jtic)
                        Next jtic
                    End If
                Else
                    IterNPrAdjust = 1
                End If
            End If
        Next jCondSet


        For jSpatSet As Short = 1 To NSpatSet
            kadjset = SpatSet_PrAdjSet(jSpatSet)
            If SpatSet_ConstraintOption(jSpatSet) > 0 Then
                If AdjIterType(ksave) = "Smooth" Then
                    IterNPrAdjust = 1
                    For jtic = 1 To NTic
                        ' constraint option 7 will never adjust prices in tic 1 as CondSetDev!(ksaveBase, jmset,1) is always 0
                        If SpatSet_ConstraintOption(jSpatSet) = 7 And jtic = 1 Then
                            AVEDEV = (SpatSet_Dev(jSpatSet, ksaveBase, jtic) - SpatSet_Dev(jSpatSet, ksaveBase, jtic + 1)) / 2
                        Else
                            AVEDEV = SpatSet_Dev(jSpatSet, ksaveBase, jtic)
                        End If
                        devpct = System.Math.Abs(AVEDEV)
                        PrAdj = Smooth(kadjset, devpct)
                        'If AVEDEV > 0 Then PrAdj = -PrAdj
                        If bSmoothFlopControl = True And LastIterType = "Smooth" Then
                            'first, check for flop - implies you had an overadjustment
                            FlopInd = SpatSetAdjModIndex(jSpatSet, jtic) 'default
                            If SpatSetLastIterDev(jSpatSet, jtic) * AVEDEV < 0 Then 'had a flop
                                FlopInd = Math.Min(SpatSetAdjModIndex(jSpatSet, jtic) + 1, FlopNBreaks) 'ramp down adjustment parameter - highest index holds lowest value
                                '92911 - get a "free pass" to the center?? - if you all of a sudden find the flop price ??
                                If FlopInd < FlopNBreaks / 2 Then FlopInd = FlopNBreaks / 2
                                SpatSetFlopped(jSpatSet, jtic) = 1
                            ElseIf SpatSetLastIterDev(jSpatSet, jtic) * AVEDEV > 0 Then
                                'If bOneWayFlopControl = True Then
                                '    'if you have not flopped and the deviation is now more or equal to the last deviation, increase the magnitude of the price adjustment
                                '    If Math.Abs(AVEDEV) >= Math.Abs(SpatSetLastIterDev(jSpatSet, jtic)) And SpatSetFlopped(jSpatSet, jtic) = 0 Then FlopInd = Math.Max(SpatSetAdjModIndex(jSpatSet, jtic) - 1, 0)
                                'Else
                                'if you simply have not flopped since the last iteration, then increase the magnitude of the adjustment
                                If Math.Abs(AVEDEV) >= Math.Abs(SpatSetLastIterDev(jSpatSet, jtic)) Then FlopInd = Math.Max(SpatSetAdjModIndex(jSpatSet, jtic) - 1, 0)
                                'End If
                            End If
                            PrAdj = PrAdj * FlopFactorArray(FlopInd)
                            SpatSetAdjModIndex(jSpatSet, jtic) = FlopInd
                        End If

                        If AVEDEV > 0 Then PrAdj = -PrAdj
                        SpatSet_Pr(jSpatSet, ksave, jtic) = SpatSet_Pr(jSpatSet, ksaveBase, jtic) + PrAdj
                        If bSmoothFlopControl = True Then
                            'SpatSetLastIterAdj(jSpatSet, jtic) = PrAdj
                            SpatSetLastIterDev(jSpatSet, jtic) = AVEDEV
                        End If
                    Next jtic

                ElseIf AdjIterType(ksave) = "Shape" Then
                    IterNPrAdjust = 1
                    For jtic = 1 To NTic
                        WghtDev = 0.0!
                        For jjTic = 1 To NTic
                            'If CondSetConstraintOption(jCondSet) > 5 And jtic < NTic Then
                            'WghtDev! = WghtDev! + ShapeWght!(jtic, jjTic) * (CondSetDev!(ksaveBase, jCondSet, jjTic) - CondSetDev!(ksaveBase, jCondSet, jjTic) / 2)
                            'Else
                            WghtDev = WghtDev + ShapeWght(jtic, jjTic) * SpatSet_Dev(jSpatSet, ksaveBase, jjTic)
                            'End If
                        Next jjTic

                        AVEDEV = WghtDev / ShapeWghtTot(jtic)
                        ABSDEV = System.Math.Abs(AVEDEV)
                        LastBreak = 0
                        PrAdj = 0
                        For jbreak = 1 To ShapeNBreak(kadjset)
                            Break = ShapePercentDev(kadjset, jbreak)
                            If ABSDEV > Break Then
                                PrAdj = ShapePriceAdj(kadjset, jbreak)
                                LastBreak = Break
                            Else
                                PrPart = (ShapePriceAdj(kadjset, jbreak) - PrAdj) * (ABSDEV - LastBreak) / (Break - LastBreak)
                                PrAdj = PrAdj + PrPart
                                Exit For
                            End If
                        Next jbreak
                        If AVEDEV > 0 Then PrAdj = -PrAdj
                        SpatSet_Pr(jSpatSet, ksave, jtic) = SpatSet_Pr(jSpatSet, ksaveBase, jtic) + PrAdj
                    Next jtic
                ElseIf AdjIterType(ksave) = "Float" Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object CondSetConstraintOption(jCondSet). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If SpatSet_ConstraintOption(jSpatSet) > 5 Then
                        AVEDEV = SpatSet_Dev(jSpatSet, ksaveBase, 1)
                    Else
                        TotDev = 0
                        For jtic = 1 To NTic
                            TotDev = TotDev + SpatSet_Dev(jSpatSet, ksaveBase, jtic)
                        Next jtic
                        AVEDEV = TotDev / NTic
                    End If
                    ABSDEV = System.Math.Abs(AVEDEV)
                    'UPGRADE_WARNING: Couldn't resolve default property of object FloatStopPercent(kadjset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If ABSDEV > FloatStopPercent(kadjset) Then
                        IterNPrAdjust = IterNPrAdjust + 1
                        LastBreak = 0
                        PrAdj = 0
                        'UPGRADE_WARNING: Couldn't resolve default property of object FloatNBreak(kadjset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        For jbreak = 1 To FloatNBreak(kadjset)
                            'UPGRADE_WARNING: Couldn't resolve default property of object FloatPercentDev(kadjset, jbreak). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Break = FloatPercentDev(kadjset, jbreak)
                            If ABSDEV > Break Then
                                PrAdj = FloatPriceAdj(kadjset, jbreak)
                                LastBreak = Break
                            Else
                                PrPart = (FloatPriceAdj(kadjset, jbreak) - PrAdj) * (ABSDEV - LastBreak) / (Break - LastBreak)
                                PrAdj = PrAdj + PrPart
                                Exit For
                            End If
                        Next jbreak
                        If AVEDEV > 0 Then PrAdj = -PrAdj
                        For jtic = 1 To NTic
                            SpatSet_Pr(jSpatSet, ksave, jtic) = SpatSet_Pr(jSpatSet, ksaveBase, jtic) + PrAdj
                        Next jtic
                    Else
                        For jtic = 1 To NTic
                            SpatSet_Pr(jSpatSet, ksave, jtic) = SpatSet_Pr(jSpatSet, ksaveBase, jtic)
                        Next jtic
                    End If
                Else
                    IterNPrAdjust = 1
                End If
            End If
        Next jSpatSet
    End Sub

    Private Sub DualityCheck()
        '  Values for shadow prices are potentially limited based on the constraint type used.
        '  Shadow Prices must also be near-zero for constraints which are nonbinding.
        '  This routine checks for violations and makes adjustments

        Dim jbreak As Short
        Dim jadj As Short
        'Dim jCondSet As Short
        Dim MnLimit As Single
        Dim MxLimit As Single
        Dim quanprior As Single
        Dim NetPr As Single
        Dim sptic As Short
        Dim Allowdown As Single
        Dim Allowup As Single
        Dim PrAdj As Single
        Dim devpct As Single
        Dim dev As Single
        Dim dprice As Single
        Dim ktable As Short
        Dim jtic As Short
        Dim jmset As Short


        For jmset = 1 To NMSet
            kadjset = MSet_PrAdjSet(jmset)

            If MSet_ConstraintOption(jmset) = 2 Then
                ' For Demand Constraints, stay within range of demand table
                ' hmh 7/13/2003
                For jtic = 1 To NTic
                    ktable = MSet_DemandTableID(jmset, jtic)
                    dprice = MSet_Pr(jmset, ksave, jtic)
                    If dprice < DemandPrice(ktable, 1) Then
                        MSet_Pr(jmset, ksave, jtic) = DemandPrice(ktable, 1)
                    ElseIf dprice > DemandPrice(ktable, DemandNPoint(ktable)) Then
                        MSet_Pr(jmset, ksave, jtic) = DemandPrice(ktable, DemandNPoint(ktable))
                    End If
                Next jtic

            ElseIf MSet_ConstraintOption(jmset) = 3 Then
                ' For lower bound constraints, shadow prices cannot be negative
                For jtic = 1 To NTic
                    If MSet_Pr(jmset, ksave, jtic) < 0 Then
                        MSet_Pr(jmset, ksave, jtic) = 0
                    End If
                Next jtic
            ElseIf MSet_ConstraintOption(jmset) = 4 Then
                ' For Upper Bound constraints, shadow prices cannot be positive
                For jtic = 1 To NTic
                    If MSet_Pr(jmset, ksave, jtic) > 0 Then
                        MSet_Pr(jmset, ksave, jtic) = 0
                    End If
                Next jtic

            ElseIf MSet_ConstraintOption(jmset) = 5 Then
                ' If flows are within both upper and lower bounds, move price towards zero using smooth approach
                'kadjset = MsetPrAdjSet(jMSet)
                For jtic = 1 To NTic
                    dev = 0
                    If System.Math.Abs(MSet_Dev(jmset, ksaveBase, jtic)) < 0.01 Then
                        If MSet_Pr(jmset, ksave, jtic) > 0.01 Then
                            dev = (MSet_Flow(jmset, ksaveBase, jtic) - MSet_FlowMn(jmset, jtic)) / MSet_FlowMn(jmset, jtic)
                        ElseIf MSet_Pr(jmset, ksave, jtic) < 0.01 Then
                            dev = (MSet_Flow(jmset, ksaveBase, jtic) - MSet_FlowMx(jmset, jtic)) / MSet_FlowMx(jmset, jtic)
                        End If
                        devpct = System.Math.Abs(dev)
                        PrAdj = System.Math.Abs(Smooth(kadjset, devpct))

                        If PrAdj > System.Math.Abs(MSet_Pr(jmset, ksave, jtic)) Then
                            MSet_Pr(jmset, ksave, jtic) = 0
                        Else
                            If MSet_Pr(jmset, ksave, jtic) > 0 Then
                                'If dev! < 0 Then PrAdj! = -PrAdj
                                MSet_Pr(jmset, ksave, jtic) = MSet_Pr(jmset, ksave, jtic) - PrAdj
                            Else
                                MSet_Pr(jmset, ksave, jtic) = MSet_Pr(jmset, ksave, jtic) + PrAdj
                            End If
                        End If
                    End If
                Next jtic


            ElseIf MSet_ConstraintOption(jmset) > 5 Then
                ' For limited percentage change in flows between periods, the problem is more complicated.
                ' Our periodic shadow prices are actually "combined prices" reflecting difference
                ' constraints for two periods.  With constraints that tie production in the base period
                ' (option 6), one can solve for all the individual shadow prices by starting with the
                ' shadow prices for the last period.  The problem is more complicated when flows for
                ' the first tic are not limited by an earlier period.  Here, we solve for the implied
                ' shadow prices for the individual constraints starting with the Tic1 and Tic2 differences
                ' and then reestimate the combined shadow price for the last tic so that duality relationships
                ' hold.
                ' SPrUpLim!(jtic) is the shadow price associated with the upper limit on harvesting in decade
                ' jtic that depends on harvest level in period jtic-1
                ' SPrLowLim!(jtic) is the shadow price associated with the lower limit on harvesting in decade
                ' jtic that depends on harvest level in period jtic-1
                ReDim SPrUpLim(NTic)
                ReDim SPrLowLim(NTic)
                Allowup = 1 + MSet_DevMx(jmset)
                Allowdown = 1 - MSet_DevMn(jmset)


                ' code used initially that changed shadow price in last period for option 7.
                ' Last period shadow price changes were large because of discounting.
                ' Modified to adjust shadow prices in first period



                If MSet_ConstraintOption(jmset) = 7 Then
                    sptic = 2
                Else
                    sptic = 1
                End If

                If MSet_Pr(jmset, ksave, NTic) < 0 Then
                    SPrUpLim(NTic) = -MSet_Pr(jmset, ksave, NTic) * MDisc(NTic)
                Else
                    SPrLowLim(NTic) = MSet_Pr(jmset, ksave, NTic) * MDisc(NTic)
                End If
                For jtic = NTic - 1 To sptic Step -1
                    'For jtic = NTic - 1 To 1 Step -1
                    NetPr = MSet_Pr(jmset, ksave, NTic) * MDisc(jtic) - Allowup * SPrUpLim(jtic + 1) + Allowdown * SPrLowLim(jtic + 1)
                    If NetPr < 0 Then
                        SPrUpLim(jtic) = -NetPr
                    Else
                        SPrLowLim(jtic) = NetPr
                    End If
                Next jtic


                'check to see if complementary slackness conditions hold.
                'If not, move shadow prices towards zero using smooth process

                For jtic = 1 To NTic
                    If jtic > 1 Or MSet_ConstraintOption(jmset) = 6 Then

                        If jtic > 1 Then
                            quanprior = MSet_Flow(jmset, ksaveBase, jtic - 1)
                        Else
                            quanprior = MSet_BaseQuan(jmset)
                        End If
                        If quanprior < 0.0001 Then quanprior = 0.0001


                        'If Abs(MSetDev!(ksaveBase, jmset, jtic)) < 0.001 Then
                        If SPrUpLim(jtic) > 0.001 Then

                            MxLimit = quanprior * (1 + MSet_DevMx(jmset))
                            'devpct! = 100 * Abs((MxLimit! - MSetFlow#(ksaveBase, jmset, jtic)) / quanprior!)
                            devpct = 100 * ((MxLimit - MSet_Flow(jmset, ksaveBase, jtic)) / quanprior)
                            If devpct > 0 Then
                                PrAdj = Smooth(kadjset, devpct)
                                SPrUpLim(jtic) = SPrUpLim(jtic) - PrAdj * MDisc(jtic)
                                If SPrUpLim(jtic) < 0 Then SPrUpLim(jtic) = 0
                            End If
                        ElseIf SPrLowLim(jtic) > 0.001 Then

                            MnLimit = quanprior * (1 - MSet_DevMn(jmset))
                            'devpct! = 100 * Abs((MSetFlow#(ksaveBase, jmset, jtic) - MnLimit!) / quanprior!)
                            devpct = 100 * ((MSet_Flow(jmset, ksaveBase, jtic) - MnLimit) / quanprior)
                            If devpct > 0 Then
                                PrAdj = Smooth(kadjset, devpct)
                                SPrLowLim(jtic) = SPrLowLim(jtic) - PrAdj * MDisc(jtic)
                                If SPrLowLim(jtic) < 0 Then SPrLowLim(jtic) = 0
                            End If
                        End If
                        'End If
                    End If
                Next jtic

                ' reset shadow prices based on combined shadow price values

                MSet_Pr(jmset, ksave, NTic) = (SPrLowLim(NTic) - SPrUpLim(NTic)) / MDisc(NTic)
                For jtic = NTic - 1 To 1 Step -1
                    MSet_Pr(jmset, ksave, jtic) = (SPrLowLim(jtic) - SPrUpLim(jtic) + Allowup * SPrUpLim(jtic + 1) - Allowdown * SPrLowLim(jtic + 1)) / MDisc(jtic)
                Next jtic
            End If
        Next jmset

        For jCondSet As Integer = 1 To NCondSet
            If CondSet_ConstraintOption(jCondSet) = 2 Then
                ' For Demand Constraints, stay within range of demand table
                ' hmh 7/13/2003
                For jtic = 1 To NTic
                    ktable = CondSet_DemandTableID(jCondSet, jtic)
                    dprice = CondSet_Pr(jCondSet, ksave, jtic)
                    If dprice < DemandPrice(ktable, 1) Then
                        CondSet_Pr(jCondSet, ksave, jtic) = DemandPrice(ktable, 1)
                    ElseIf dprice > DemandPrice(ktable, DemandNPoint(ktable)) Then
                        CondSet_Pr(jCondSet, ksave, jtic) = DemandPrice(ktable, DemandNPoint(ktable))
                    End If
                Next jtic


            ElseIf CondSet_ConstraintOption(jCondSet) = 3 Then
                ' For lower bound constraints, shadow prices cannot be negative
                For jtic = 1 To NTic
                    If CondSet_Pr(jCondSet, ksave, jtic) < 0 Then
                        CondSet_Pr(jCondSet, ksave, jtic) = 0
                    End If
                Next jtic

            ElseIf CondSet_ConstraintOption(jCondSet) = 4 Then
                ' For upper bound constraints, shadow prices cannot be positive
                For jtic = 1 To NTic
                    If CondSet_Pr(jCondSet, ksave, jtic) > 0 Then
                        CondSet_Pr(jCondSet, ksave, jtic) = 0
                    End If
                Next jtic

            ElseIf CondSet_ConstraintOption(jCondSet) = 5 Then
                ' If flows are within both upper and lower bounds, move price towards zero using smooth approach
                kadjset = CondSet_PrAdjSet(jCondSet)
                For jtic = 1 To NTic
                    dev = 0
                    If System.Math.Abs(CondSet_Dev(jCondSet, ksaveBase, jtic)) < 0.01 Then
                        If CondSet_Pr(jCondSet, ksave, jtic) > 0.01 Then
                            dev = (CondSet_Flow(jCondSet, ksaveBase, jtic) - CondSet_FlowMn(jCondSet, jtic)) / CondSet_FlowMn(jCondSet, jtic)
                        ElseIf CondSet_Pr(jCondSet, ksave, jtic) < 0.01 Then
                            dev = (CondSet_Flow(jCondSet, ksaveBase, jtic) - CondSet_FlowMx(jCondSet, jtic)) / CondSet_FlowMx(jCondSet, jtic)
                        End If
                        devpct = System.Math.Abs(dev)
                        PrAdj = System.Math.Abs(Smooth(kadjset, devpct))

                        If PrAdj > System.Math.Abs(CondSet_Pr(jCondSet, ksave, jtic)) Then
                            CondSet_Pr(jCondSet, ksave, jtic) = 0
                        Else
                            'If dev! < 0 Then PrAdj! = -PrAdj
                            If CondSet_Pr(jCondSet, ksave, jtic) > 0 Then
                                CondSet_Pr(jCondSet, ksave, jtic) = CondSet_Pr(jCondSet, ksave, jtic) - PrAdj
                            Else
                                CondSet_Pr(jCondSet, ksave, jtic) = CondSet_Pr(jCondSet, ksave, jtic) + PrAdj
                            End If
                        End If
                    End If
                Next jtic

            ElseIf CondSet_ConstraintOption(jCondSet) > 5 Then
                ReDim SPrUpLim(NTic)
                ReDim SPrLowLim(NTic)
                Allowup = 1 + CondSet_DevMx(jCondSet)
                Allowdown = 1 - CondSet_DevMn(jCondSet)


                ' code used initially that changed shadow price in last period for option 7.
                ' Last period shadow price changes were large because of discounting.
                ' Modified to adjust shadow prices in first period


                If CondSet_ConstraintOption(jCondSet) = 7 Then
                    sptic = 2
                Else
                    sptic = 1
                End If
                If CondSet_Pr(jCondSet, ksave, NTic) < 0 Then
                    SPrUpLim(NTic) = -CondSet_Pr(jCondSet, ksave, NTic) * CondDisc(NTic)
                Else
                    SPrLowLim(NTic) = CondSet_Pr(jCondSet, ksave, NTic) * CondDisc(NTic)
                End If
                'For jtic = NTic - 1 To 1 Step -1
                For jtic = NTic - 1 To sptic Step -1
                    NetPr = CondSet_Pr(jCondSet, ksave, jtic) * CondDisc(jtic) - Allowup * SPrUpLim(jtic + 1) + Allowdown * SPrLowLim(jtic + 1)
                    If NetPr < 0 Then
                        SPrUpLim(jtic) = -NetPr
                    Else
                        SPrLowLim(jtic) = NetPr
                    End If
                Next jtic


                'check to see if complementary slackness conditions hold.
                'If not, move shadow prices towards zero using smooth process

                For jtic = 1 To NTic
                    If jtic > 1 Or CondSet_ConstraintOption(jCondSet) = 6 Then

                        If jtic > 1 Then
                            quanprior = CondSet_Flow(jCondSet, ksaveBase, jtic - 1)
                        Else
                            quanprior = CondSet_BaseQuan(jCondSet)
                        End If
                        If quanprior < 0.0001 Then quanprior = 0.0001


                        'If Abs(CondSetDev!(ksaveBase, jCondSet, jtic)) < 0.001 Then
                        If SPrUpLim(jtic) > 0.001 Then


                            MxLimit = quanprior * (1 + CondSet_DevMx(jCondSet))
                            'devpct! = 100 * Abs((MxLimit! - CondSetFlow#(ksaveBase, jCondSet, jtic)) / quanprior!)
                            devpct = 100 * ((MxLimit - CondSet_Flow(jCondSet, ksaveBase, jtic)) / quanprior)
                            If devpct > 0 Then
                                PrAdj = Smooth(kadjset, devpct)
                                SPrUpLim(jtic) = SPrUpLim(jtic) - PrAdj * CondDisc(jtic)
                                If SPrUpLim(jtic) < 0 Then SPrUpLim(jtic) = 0
                            End If
                        ElseIf SPrLowLim(jtic) > 0.001 Then

                            MnLimit = quanprior * (1 - CondSet_DevMn(jCondSet))
                            'devpct! = 100 * Abs((CondSetFlow#(ksave, jCondSet, jtic) - MnLimit!) / quanprior!)
                            devpct = 100 * ((CondSet_Flow(jCondSet, ksave, jtic) - MnLimit) / quanprior)
                            If devpct > 0 Then
                                PrAdj = Smooth(kadjset, devpct)
                                SPrLowLim(jtic) = SPrLowLim(jtic) - PrAdj * CondDisc(jtic)
                                If SPrLowLim(jtic) < 0 Then SPrLowLim(jtic) = 0
                            End If
                        End If
                        'End If
                    End If
                Next jtic

                ' reset shadow prices based on combined shadow price values

                CondSet_Pr(jCondSet, ksave, NTic) = (SPrLowLim(NTic) - SPrUpLim(NTic)) / CondDisc(NTic)
                For jtic = NTic - 1 To 1 Step -1
                    CondSet_Pr(jCondSet, ksave, jtic) = (SPrLowLim(jtic) - SPrUpLim(jtic) + Allowup * SPrUpLim(jtic + 1) - Allowdown * SPrLowLim(jtic + 1)) / CondDisc(jtic)
                Next jtic
            End If
        Next jCondSet

        For jSpatSet As Integer = 1 To NSpatSet
            If SpatSet_ConstraintOption(jSpatSet) = 2 Then
                ' For Demand Constraints, stay within range of demand table
                ' hmh 7/13/2003
                For jtic = 1 To NTic
                    ktable = SpatSet_DemandTableID(jSpatSet, jtic)
                    dprice = SpatSet_Pr(jSpatSet, ksave, jtic)
                    If dprice < DemandPrice(ktable, 1) Then
                        SpatSet_Pr(jSpatSet, ksave, jtic) = DemandPrice(ktable, 1)
                    ElseIf dprice > DemandPrice(ktable, DemandNPoint(ktable)) Then
                        SpatSet_Pr(jSpatSet, ksave, jtic) = DemandPrice(ktable, DemandNPoint(ktable))
                    End If
                Next jtic


            ElseIf SpatSet_ConstraintOption(jSpatSet) = 3 Then
                ' For lower bound constraints, shadow prices cannot be negative
                For jtic = 1 To NTic
                    If SpatSet_Pr(jSpatSet, ksave, jtic) < 0 Then
                        SpatSet_Pr(jSpatSet, ksave, jtic) = 0
                    End If
                Next jtic

            ElseIf SpatSet_ConstraintOption(jSpatSet) = 4 Then
                ' For upper bound constraints, shadow prices cannot be positive
                For jtic = 1 To NTic
                    If SpatSet_Pr(jSpatSet, ksave, jtic) > 0 Then
                        SpatSet_Pr(jSpatSet, ksave, jtic) = 0
                    End If
                Next jtic

            ElseIf SpatSet_ConstraintOption(jSpatSet) = 5 Then
                ' If flows are within both upper and lower bounds, move price towards zero using smooth approach
                kadjset = SpatSet_PrAdjSet(jSpatSet)
                For jtic = 1 To NTic
                    dev = 0
                    If System.Math.Abs(SpatSet_Dev(jSpatSet, ksaveBase, jtic)) < 0.01 Then
                        If SpatSet_Pr(jSpatSet, ksave, jtic) > 0.01 Then
                            dev = (SpatSet_Flow(jSpatSet, ksaveBase, jtic) - SpatSet_FlowMn(jSpatSet, jtic)) / SpatSet_FlowMn(jSpatSet, jtic)
                        ElseIf SpatSet_Pr(jSpatSet, ksave, jtic) < 0.01 Then
                            dev = (SpatSet_Flow(jSpatSet, ksaveBase, jtic) - SpatSet_FlowMx(jSpatSet, jtic)) / SpatSet_FlowMx(jSpatSet, jtic)
                        End If
                        devpct = System.Math.Abs(dev)
                        PrAdj = System.Math.Abs(Smooth(kadjset, devpct))

                        If PrAdj > System.Math.Abs(SpatSet_Pr(jSpatSet, ksave, jtic)) Then
                            SpatSet_Pr(jSpatSet, ksave, jtic) = 0
                        Else
                            'If dev! < 0 Then PrAdj! = -PrAdj
                            If SpatSet_Pr(jSpatSet, ksave, jtic) > 0 Then
                                SpatSet_Pr(jSpatSet, ksave, jtic) = SpatSet_Pr(jSpatSet, ksave, jtic) - PrAdj
                            Else
                                SpatSet_Pr(jSpatSet, ksave, jtic) = SpatSet_Pr(jSpatSet, ksave, jtic) + PrAdj
                            End If
                        End If
                    End If
                Next jtic

            ElseIf SpatSet_ConstraintOption(jSpatSet) > 5 Then
                ReDim SPrUpLim(NTic)
                ReDim SPrLowLim(NTic)
                Allowup = 1 + SpatSet_DevMx(jSpatSet)
                Allowdown = 1 - SpatSet_DevMn(jSpatSet)


                ' code used initially that changed shadow price in last period for option 7.
                ' Last period shadow price changes were large because of discounting.
                ' Modified to adjust shadow prices in first period


                If SpatSet_ConstraintOption(jSpatSet) = 7 Then
                    sptic = 2
                Else
                    sptic = 1
                End If
                If SpatSet_Pr(jSpatSet, ksave, NTic) < 0 Then
                    SPrUpLim(NTic) = -SpatSet_Pr(jSpatSet, ksave, NTic) * CondDisc(NTic)
                Else
                    SPrLowLim(NTic) = SpatSet_Pr(jSpatSet, ksave, NTic) * CondDisc(NTic)
                End If
                'For jtic = NTic - 1 To 1 Step -1
                For jtic = NTic - 1 To sptic Step -1
                    NetPr = SpatSet_Pr(jSpatSet, ksave, jtic) * CondDisc(jtic) - Allowup * SPrUpLim(jtic + 1) + Allowdown * SPrLowLim(jtic + 1)
                    If NetPr < 0 Then
                        SPrUpLim(jtic) = -NetPr
                    Else
                        SPrLowLim(jtic) = NetPr
                    End If
                Next jtic


                'check to see if complementary slackness conditions hold.
                'If not, move shadow prices towards zero using smooth process

                For jtic = 1 To NTic
                    If jtic > 1 Or SpatSet_ConstraintOption(jSpatSet) = 6 Then

                        If jtic > 1 Then
                            quanprior = SpatSet_Flow(jSpatSet, ksaveBase, jtic - 1)
                        Else
                            quanprior = SpatSet_BaseQuan(jSpatSet)
                        End If
                        If quanprior < 0.0001 Then quanprior = 0.0001


                        'If Abs(CondSetDev!(ksaveBase, jCondSet, jtic)) < 0.001 Then
                        If SPrUpLim(jtic) > 0.001 Then


                            MxLimit = quanprior * (1 + SpatSet_DevMx(jSpatSet))
                            'devpct! = 100 * Abs((MxLimit! - CondSetFlow#(ksaveBase, jCondSet, jtic)) / quanprior!)
                            devpct = 100 * ((MxLimit - SpatSet_Flow(jSpatSet, ksaveBase, jtic)) / quanprior)
                            If devpct > 0 Then
                                PrAdj = Smooth(kadjset, devpct)
                                SPrUpLim(jtic) = SPrUpLim(jtic) - PrAdj * CondDisc(jtic)
                                If SPrUpLim(jtic) < 0 Then SPrUpLim(jtic) = 0
                            End If
                        ElseIf SPrLowLim(jtic) > 0.001 Then

                            MnLimit = quanprior * (1 - SpatSet_DevMn(jSpatSet))
                            'devpct! = 100 * Abs((CondSetFlow#(ksave, jCondSet, jtic) - MnLimit!) / quanprior!)
                            devpct = 100 * ((SpatSet_Flow(jSpatSet, ksave, jtic) - MnLimit) / quanprior)
                            If devpct > 0 Then
                                PrAdj = Smooth(kadjset, devpct)
                                SPrLowLim(jtic) = SPrLowLim(jtic) - PrAdj * CondDisc(jtic)
                                If SPrLowLim(jtic) < 0 Then SPrLowLim(jtic) = 0
                            End If
                        End If
                        'End If
                    End If
                Next jtic

                ' reset shadow prices based on combined shadow price values

                SpatSet_Pr(jSpatSet, ksave, NTic) = (SPrLowLim(NTic) - SPrUpLim(NTic)) / CondDisc(NTic)
                For jtic = NTic - 1 To 1 Step -1
                    SpatSet_Pr(jSpatSet, ksave, jtic) = (SPrLowLim(jtic) - SPrUpLim(jtic) + Allowup * SPrUpLim(jtic + 1) - Allowdown * SPrLowLim(jtic + 1)) / CondDisc(jtic)
                Next jtic
            End If
        Next jSpatSet

        '7/14/2003 add decay factor to price adjustment factors

        For jadj = 1 To PriceAdjNSet
            For jbreak = 1 To FloatNBreak(jadj)
                FloatPriceAdj(jadj, jbreak) = FloatPriceAdj(jadj, jbreak) * ShadowPriceDecayfactor
            Next jbreak

            For jbreak = 1 To SmoothNBreak(jadj)
                SmoothPriceAdj(jadj, jbreak) = SmoothPriceAdj(jadj, jbreak) * ShadowPriceDecayfactor
            Next jbreak

            For jbreak = 1 To ShapeNBreak(jadj)
                ShapePriceAdj(jadj, jbreak) = ShapePriceAdj(jadj, jbreak) * ShadowPriceDecayfactor
            Next jbreak

        Next jadj


    End Sub

    Function Smooth(ByRef kadjset As Short, ByRef devpct As Single) As Single
        Dim PrPart As Single
        Dim Break As Short
        Dim jbreak As Short
        Dim LastBreak As Short
        LastBreak = 0
        Smooth = 0
        'UPGRADE_WARNING: Couldn't resolve default property of object SmoothNBreak(kadjset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        For jbreak = 1 To SmoothNBreak(kadjset)
            'UPGRADE_WARNING: Couldn't resolve default property of object ShapePercentDev(kadjset, jbreak). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Break = ShapePercentDev(kadjset, jbreak)
            If devpct > Break Then
                Smooth = SmoothPriceAdj(kadjset, jbreak)
                LastBreak = Break
            Else
                PrPart = (SmoothPriceAdj(kadjset, jbreak) - Smooth) * (devpct - LastBreak) / (Break - LastBreak)
                Smooth = Smooth + PrPart
                Exit For
            End If
        Next jbreak
    End Function

    Public Sub LoadDualPlanInput(ByVal infile As String)


        Dim kkLayerColorEnd As Short
        Dim kkLayerColorBeg As Short
        Dim kkAgeEnd As Short
        Dim kkAgeBeg As Short
        Dim kkCoverSiteEnd As Short
        Dim kkCoverSiteBeg As Short
        Dim kkMapLayer As Short
        Dim kkNunits As Single
        Dim kkprice As Single
        Dim kkTicEnd As Short
        Dim kkTicBeg As Short
        Dim kkMLocEnd As Short
        Dim kkMLocBeg As Short
        Dim kkTypeEnd As Short
        Dim kkTypeBeg As Short
        Dim jEq As Short
        Dim MxEqNDef As Integer

        Dim kdumtic As Short

        Dim jjTic As Short
        Dim jbreak As Short
        Dim jadj As Short
        Dim jset As Short
        Dim jcolor As Short
        Dim jMap As Short
        Dim MxColor As Short
        Dim jtic As Short
        'Dim DummyFnDualPlanOutBaseA As String
        'Dim ISpaceDefFile As String
        Dim dummy As String
        Dim dumline As String


        Dim DumSet() As SetData
        Dim NSet As Integer
        ReDim MSet(0)
        ReDim SpatSet(0)
        ReDim CondSet(0)
        CondSetMxNDefs = 0
        SpatSetMxNDefs = 0
        MSetMxNDefs = 0

        'default - assume no spatial sets are active
        bNeedSpatialIterations = False

        'Dim SpatialSeriesFile As String

        FileOpen(5, infile, OpenMode.Input)

        dummy = LineInput(5)
        FnAAFileList = Trim(LineInput(5))
        'FIleListPolyOptions = FnAAFileList
        dummy = LineInput(5)
        dummy = Trim(LineInput(5)) 'FnDualPlanOutBaseA = Trim(LineInput(5)) 'now set in the link file
        dummy = LineInput(5) 'Generic File Name for Schedule File to read (optional)
        FnPolySchedFileDefName = Trim(LineInput(5)) 'DetermineBaseIFName(infile)
        FnPolySchedFileBase = DirName(FnPolySchedFileDefName) & FName(FnPolySchedFileDefName)
        PolySchedFileExt = FileExtension(FnPolySchedFileDefName)
        'FnDualPlanOutBaseA = DualPlanLink
        dummy = LineInput(5)
        FnLabels = Trim(LineInput(5))
        dummy = LineInput(5)
        ISpaceDefFile = Trim(LineInput(5))
        dummy = LineInput(5)
        'FnRgData = Trim(LineInput(5))
        SpatialSeriesFile = Trim(LineInput(5)) 'use this spot in the input file to load the spatial series information
        dummy = LineInput(5)
        dummy = Trim(LineInput(5))
        NIterToSave = ParseString(dummy, " ")
        NiterToRewrite = ParseString(dummy, " ")
        'IterPerFileSave = ParseString(dummy, " ")
        'Input(5, NIterToSave)
        'Input(5, IterPerFileSave)
        dummy = LineInput(5)
        dummy = Trim(LineInput(5))
        NFloat = ParseString(dummy, " ")
        NSmooth = ParseString(dummy, " ")
        NShape = ParseString(dummy, " ")
        dummy = LineInput(5)
        dummy = Trim(LineInput(5))
        NCondSet = ParseString(dummy, " ")
        NMSet = ParseString(dummy, " ")
        NSpatSet = ParseString(dummy, " ")
        InterestRate = ParseString(dummy, " ")
        NTic = ParseString(dummy, " ")
        NYearPerTic = ParseString(dummy, " ")
        NTic1 = NTic + 1
        dummy = LineInput(5)
        dummy = Trim(LineInput(5))
        WriteMSet = ParseString(dummy, " ")
        WriteMType = ParseString(dummy, " ")
        WriteCondSet = ParseString(dummy, " ")
        WritecoversiteAge = ParseString(dummy, " ")
        WriteSpatSet = ParseString(dummy, " ")
        WriteSpatType = ParseString(dummy, " ")
        dummy = LineInput(5)
        dummy = Trim(LineInput(5))
        Ncoversite = ParseString(dummy, " ")
        NMType = ParseString(dummy, " ")
        NSpaceType = ParseString(dummy, " ")
        NMLocation = ParseString(dummy, " ")
        MTypeSiteConvCost = ParseString(dummy, " ")
        MTypeEntryCost = ParseString(dummy, " ")
        EntryCost = ParseString(dummy, " ")
        dummy = LineInput(5)
        dummy = Trim(LineInput(5))
        '!!! MxAgeClass now set in LoadSubForest subroutine
        'MxAgeClass = ParseString(dummy, " ")
        'Public Const MxTicRegenAA = 15
        'Public Const MxAgeRegenRg = 15
        MxTic = NTic * 3 'MxAgeFlowRg + NTic1
        'If MxTicFlowAA > MxTic Then MxTic = MxTicFlowAA

        OneTicDisc = 1 / (1 + InterestRate)
        'HalfTicDisc! = Sqr(OneTicDisc!)
        'DEBUG: Make larger to handle long rotations
        ReDim MDisc(MxTic + 100)
        ReDim CondDisc(MxTic + 100)
        ReDim SEVFactor(MxTic + 100)
        'EBH: WANT TO DISCOUNT MARKET FLOWS FROM CENTERS OF PERIODS AND 
        '  CONDITION FLOWS FROM THE END
        '  

        MDisc(1) = OneTicDisc ^ 0.5 '1.0#
        CondDisc(0) = 1
        CondDisc(1) = OneTicDisc 'OneTicDisc ^ 0.5
        SEVFactor(1) = 1 + (1 / InterestRate)
        For jtic = 2 To MxTic + 100
            MDisc(jtic) = MDisc(jtic - 1) * OneTicDisc
            CondDisc(jtic) = CondDisc(jtic - 1) * OneTicDisc
            SEVFactor(jtic) = 1 + (1 / (((1 + InterestRate) ^ jtic) - 1))
        Next jtic

        dummy = LineInput(5)
        dummy = Trim(LineInput(5))
        NMapLayer = ParseString(dummy, " ")
        ReDim MapLayerNColor(NMapLayer)
        MxColor = 1
        dummy = LineInput(5)
        dummy = Trim(LineInput(5))
        For jMap = 1 To NMapLayer
            MapLayerNColor(jMap) = ParseString(dummy, " ")
            If MapLayerNColor(jMap) > MxColor Then MxColor = MapLayerNColor(jMap)
        Next jMap

        ReDim MapAreaID(NMapLayer, MxColor)
        NMapArea = 0
        'Line Input #5, Dummy$
        For jMap = 1 To NMapLayer
            'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerNColor(jMap). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For jcolor = 1 To MapLayerNColor(jMap)
                NMapArea = NMapArea + 1
                'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, jcolor). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                MapAreaID(jMap, jcolor) = NMapArea
                'Input #5, jjmap, jjcolor, MapAreaID(jmap, jcolor)
            Next jcolor
        Next jMap

        dummy = LineInput(5)
        dummy = Trim(LineInput(5))
        EndPriceNWghtSet = ParseString(dummy, " ")
        ReDim EndPriceWght(EndPriceNWghtSet, NTic)
        dummy = LineInput(5)
        For jset = 1 To EndPriceNWghtSet
            dummy = Trim(LineInput(5))
            For jtic = 1 To NTic
                EndPriceWght(jset, jtic) = ParseString(dummy, " ")
            Next jtic
        Next jset

        dummy = LineInput(5)
        dummy = Trim(LineInput(5))
        EntryAdjNEquation = ParseString(dummy, " ")
        EntryAdjMxBreakPoint = ParseString(dummy, " ")
        MxEqNDef = 0


        ReDim EntryAdjNPoint(EntryAdjNEquation), EntryAdjNDef(EntryAdjNEquation)
        'ReDim EntryAdjMType(EntryAdjNEquation)
        ReDim EntryAdjAcres(EntryAdjNEquation, EntryAdjMxBreakPoint)
        ReDim EntryAdjFactor(EntryAdjNEquation, EntryAdjMxBreakPoint)

        'variable "entry cost" adjustments
        For jEq = 1 To EntryAdjNEquation
            dummy = LineInput(5)
            dummy = LineInput(5)
            dummy = Trim(LineInput(5))
            EntryAdjNDef(jEq) = ParseString(dummy, " ")
            EntryAdjNPoint(jEq) = ParseString(dummy, " ")
            If EntryAdjNDef(jEq) > MxEqNDef Then
                MxEqNDef = EntryAdjNDef(jEq)
                ReDim Preserve EntryAdjkCoverSiteBeg(EntryAdjNEquation, MxEqNDef), EntryAdjkCoverSiteEnd(EntryAdjNEquation, MxEqNDef), _
                EntryAdjkagebeg(EntryAdjNEquation, MxEqNDef), EntryAdjkageend(EntryAdjNEquation, MxEqNDef), _
                MTypeEntryCostBeg(EntryAdjNEquation, MxEqNDef), MTypeEntryCostEnd(EntryAdjNEquation, MxEqNDef)
            End If
            dummy = LineInput(5) 'kCoverSiteBeg  kCoverSiteEnd  kagebeg  kageend  MTypeEntryCostBeg  MTypeEntryCostEnd  
            For jEqdef As Integer = 1 To EntryAdjNDef(jEq)
                dummy = Trim(LineInput(5))
                EntryAdjkCoverSiteBeg(jEq, jEqdef) = ParseString(dummy, " ")
                EntryAdjkCoverSiteEnd(jEq, jEqdef) = ParseString(dummy, " ")
                EntryAdjkagebeg(jEq, jEqdef) = ParseString(dummy, " ")
                EntryAdjkageend(jEq, jEqdef) = ParseString(dummy, " ")
                MTypeEntryCostBeg(jEq, jEqdef) = ParseString(dummy, " ")
                MTypeEntryCostEnd(jEq, jEqdef) = ParseString(dummy, " ")
            Next
            dummy = LineInput(5) 'EntryAdjAcres            AdjFactor 
            For jbreak = 1 To EntryAdjNPoint(jEq)
                dummy = Trim(LineInput(5))
                EntryAdjAcres(jEq, jbreak) = ParseString(dummy, " ")
                EntryAdjFactor(jEq, jbreak) = ParseString(dummy, " ")
            Next jbreak
        Next jEq


        dummy = LineInput(5)
        dummy = Trim(LineInput(5))
        PriceAdjNSet = ParseString(dummy, " ")
        PriceAdjMxBreakPoint = ParseString(dummy, " ")

        ReDim FloatStopPercent(PriceAdjNSet)
        ReDim FloatNBreak(PriceAdjNSet)
        ReDim SmoothNBreak(PriceAdjNSet)
        ReDim ShapeNBreak(PriceAdjNSet)
        ReDim FloatPercentDev(PriceAdjNSet, PriceAdjMxBreakPoint)
        ReDim FloatPriceAdj(PriceAdjNSet, PriceAdjMxBreakPoint)
        ReDim SmoothPercentDev(PriceAdjNSet, PriceAdjMxBreakPoint)
        ReDim SmoothPriceAdj(PriceAdjNSet, PriceAdjMxBreakPoint)
        ReDim ShapePercentDev(PriceAdjNSet, PriceAdjMxBreakPoint)
        ReDim ShapePriceAdj(PriceAdjNSet, PriceAdjMxBreakPoint)

        For jadj = 1 To PriceAdjNSet
            dummy = LineInput(5)
            dummy = Trim(LineInput(5))
            FloatStopPercent(jadj) = ParseString(dummy, " ")
            FloatNBreak(jadj) = ParseString(dummy, " ")
            SmoothNBreak(jadj) = ParseString(dummy, " ")
            ShapeNBreak(jadj) = ParseString(dummy, " ")
            dummy = LineInput(5)
            'UPGRADE_WARNING: Couldn't resolve default property of object FloatNBreak(jadj). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For jbreak = 1 To FloatNBreak(jadj)
                dummy = Trim(LineInput(5))
                FloatPercentDev(jadj, jbreak) = ParseString(dummy, " ")
                FloatPriceAdj(jadj, jbreak) = ParseString(dummy, " ")
            Next jbreak
            dummy = LineInput(5)
            'UPGRADE_WARNING: Couldn't resolve default property of object SmoothNBreak(jadj). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For jbreak = 1 To SmoothNBreak(jadj)
                dummy = Trim(LineInput(5))
                SmoothPercentDev(jadj, jbreak) = ParseString(dummy, " ")
                SmoothPriceAdj(jadj, jbreak) = ParseString(dummy, " ")
            Next jbreak
            dummy = LineInput(5)
            'UPGRADE_WARNING: Couldn't resolve default property of object ShapeNBreak(jadj). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For jbreak = 1 To ShapeNBreak(jadj)
                dummy = Trim(LineInput(5))
                ShapePercentDev(jadj, jbreak) = ParseString(dummy, " ")
                ShapePriceAdj(jadj, jbreak) = ParseString(dummy, " ")
            Next jbreak
        Next jadj

        ReDim ShapeWght(NTic, NTic)
        ReDim ShapeWghtTot(NTic)
        dummy = LineInput(5)
        For jtic = 1 To NTic
            dummy = Trim(LineInput(5))
            For jjTic = 1 To NTic
                ShapeWght(jtic, jjTic) = ParseString(dummy, " ")
                ShapeWghtTot(jtic) = ShapeWghtTot(jtic) + ShapeWght(jtic, jjTic)
            Next jjTic
        Next jtic

        Do Until dumline = "DemandNEquation  DemandMxBreakPoint"
            dumline = Trim(LineInput(5))
            If dumline = "Market Set" Or dumline = "Condition Set" Or dumline = "Spatial Set" Then
                'load dummy set information
                ReDim DumSet(1)
                DumSet(1).SetType = dumline
                dummy = Trim(LineInput(5))
                DumSet(1).SetLabel = Trim(LineInput(5))
                dummy = Trim(LineInput(5))
                dummy = Trim(LineInput(5))
                DumSet(1).nSetDefs = ParseString(dummy, " ")
                DumSet(1).SetConstraintOption = ParseString(dummy, " ")
                DumSet(1).setPrAdjSet = ParseString(dummy, " ")
                DumSet(1).SetEndPrWghtSet = ParseString(dummy, " ")

                dummy = Trim(LineInput(5))
                DumSet(1).Initialize(DumSet(1).nSetDefs, NIterToSave, NTic1)
                Select Case DumSet(1).SetType

                    Case "Market Set"

                        'mset '' MTypeBeg      kMTypeEnd     LocBeg        LocEnd        convfact
                        For j As Integer = 1 To DumSet(1).nSetDefs
                            If DumSet(1).nSetDefs > MSetMxNDefs Then MSetMxNDefs = DumSet(1).nSetDefs
                            'mset '' MTypeBeg      kMTypeEnd     LocBeg        LocEnd        convfact
                            dummy = Trim(LineInput(5))
                            DumSet(1).kTypeBeg(j) = ParseString(dummy, " ")
                            DumSet(1).kTypeEnd(j) = ParseString(dummy, " ")
                            DumSet(1).kLocBeg(j) = ParseString(dummy, " ")
                            DumSet(1).kLocEnd(j) = ParseString(dummy, " ")
                            DumSet(1).convfact(j) = ParseString(dummy, " ")
                        Next j

                    Case "Condition Set"
                        'cset    kmaplayer  kCoverSiteBeg  kCoverSiteEnd  kagebeg  kageend  kLayerColorBeg  kLayerColorEnd  convfact  convprob

                        For j As Integer = 1 To DumSet(1).nSetDefs
                            If DumSet(1).nSetDefs > CondSetMxNDefs Then CondSetMxNDefs = DumSet(1).nSetDefs
                            dummy = Trim(LineInput(5))
                            DumSet(1).kMapLayer(j) = ParseString(dummy, " ")
                            DumSet(1).kTypeBeg(j) = ParseString(dummy, " ")
                            DumSet(1).kTypeEnd(j) = ParseString(dummy, " ")
                            DumSet(1).kagebeg(j) = ParseString(dummy, " ")
                            DumSet(1).kageend(j) = ParseString(dummy, " ")
                            DumSet(1).kLayerColorBeg(j) = ParseString(dummy, " ")
                            DumSet(1).kLayerColorEnd(j) = ParseString(dummy, " ")
                            DumSet(1).convfact(j) = ParseString(dummy, " ")
                            DumSet(1).convprob(j) = ParseString(dummy, " ")
                        Next j

                    Case "Spatial Set"
                        If DumSet(1).SetConstraintOption <> 0 Then bNeedSpatialIterations = True 'you have to consider spatial iterations
                        For j As Integer = 1 To DumSet(1).nSetDefs
                            If DumSet(1).nSetDefs > SpatSetMxNDefs Then SpatSetMxNDefs = DumSet(1).nSetDefs
                            dummy = Trim(LineInput(5))
                            DumSet(1).kTypeBeg(j) = ParseString(dummy, " ")
                            DumSet(1).kTypeEnd(j) = ParseString(dummy, " ")
                            'All this stuff is stored in the spatial type definition file (for now)
                            DumSet(1).convfact(j) = ParseString(dummy, " ")
                        Next j

                End Select
                dummy = LineInput(5) 'BaseQuantic0! used only for constraint option 7
                dummy = LineInput(5) 'BaseQuantic0!
                dummy = Trim(LineInput(5))
                DumSet(1).SetBaseQuan = ParseString(dummy, " ")
                DumSet(1).SetBaseQuan = DumSet(1).SetBaseQuan * NYearPerTic
                dummy = LineInput(5) 'MSetDevMn!, MSetDevMx!used only for constraint option 6 and 7
                dummy = LineInput(5) ' MSetDevMn! MSetDevMx!
                dummy = Trim(LineInput(5))
                DumSet(1).SetDevMn = ParseString(dummy, " ")
                DumSet(1).SetDevMx = ParseString(dummy, " ")
                dummy = LineInput(5) 'MSetDemandTableID() used only for constraint option 2
                dummy = LineInput(5) 'kdumtic, MSetDemandTableID(kdumtic)

                For jtic = 1 To NTic
                    dummy = Trim(LineInput(5))
                    kdumtic = ParseString(dummy, " ")
                    DumSet(1).SetDemandTableID(jtic) = ParseString(dummy, " ")
                Next jtic
                dummy = LineInput(5) ' options 1, 3, and 4 use MSetTarget!() option 5 uses FlowMN() and Flowmx()
                dummy = LineInput(5) 'jtic MSetFlowMn! MSetTarget! MSetFlowMx!

                For jtic = 1 To NTic
                    dummy = Trim(LineInput(5))
                    kdumtic = ParseString(dummy, " ")
                    DumSet(1).SetFlowMn(jtic) = ParseString(dummy, " ")
                    DumSet(1).SetTarget(jtic) = ParseString(dummy, " ")
                    DumSet(1).SetFlowMx(jtic) = ParseString(dummy, " ")
                    'PUT IN A REALLY SMALL DUMMY VALUE TO CONTROL FOR PRICE ADJUSTMENTS...
                    If DumSet(1).SetConstraintOption > 0 Then
                        If DumSet(1).SetFlowMn(jtic) = 0 Then DumSet(1).SetFlowMn(jtic) = 0.0001
                        If DumSet(1).SetFlowMx(jtic) = 0 Then DumSet(1).SetFlowMx(jtic) = 0.0001
                        If DumSet(1).SetTarget(jtic) = 0 Then DumSet(1).SetTarget(jtic) = 0.0001
                    End If
                    If DumSet(1).SetType = "Market Set" Then
                        DumSet(1).SetFlowMn(jtic) = DumSet(1).SetFlowMn(jtic) * NYearPerTic
                        DumSet(1).SetTarget(jtic) = DumSet(1).SetTarget(jtic) * NYearPerTic
                        DumSet(1).SetFlowMx(jtic) = DumSet(1).SetFlowMx(jtic) * NYearPerTic
                    End If
                Next jtic
                dummy = LineInput(5) 'ShadowPriceEstimates used for all options except option 0
                dummy = LineInput(5) 'kdumtic,     ShadowPriceEstimate
                For jtic = 1 To NTic
                    dummy = Trim(LineInput(5))
                    kdumtic = ParseString(dummy, " ")
                    DumSet(1).SetPr(1, jtic) = ParseString(dummy, " ")
                Next jtic

                ' make sure any unconstrained set has shadow prices = 0
                If DumSet(1).SetConstraintOption = 0 Then
                    For jtic = 1 To NTic
                        DumSet(1).SetPr(1, jtic) = 0
                    Next jtic
                End If


                Select Case DumSet(1).SetType

                    Case "Market Set"
                        NSet = MSet.Length
                        ReDim Preserve MSet(NSet)
                        MSet(NSet) = DumSet(1)


                    Case "Condition Set"
                        NSet = CondSet.Length
                        ReDim Preserve CondSet(NSet)
                        CondSet(NSet) = DumSet(1)

                    Case "Spatial Set"
                        NSet = SpatSet.Length
                        ReDim Preserve SpatSet(NSet)
                        SpatSet(NSet) = DumSet(1)

                End Select
            End If
        Loop


        dummy = Trim(LineInput(5))
        DemandNEquation = ParseString(dummy, " ")
        DemandMxBreakPoint = ParseString(dummy, " ")

        ReDim DemandNPoint(DemandNEquation)
        ReDim DemandPrice(DemandNEquation, DemandMxBreakPoint)
        ReDim DemandQuan(DemandNEquation, DemandMxBreakPoint)


        For jEq = 1 To DemandNEquation
            dummy = LineInput(5)
            dummy = Trim(LineInput(5))
            DemandNPoint(jEq) = ParseString(dummy, " ")
            'UPGRADE_WARNING: Couldn't resolve default property of object DemandNPoint(jEq). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For jbreak = 1 To DemandNPoint(jEq)
                dummy = Trim(LineInput(5))
                DemandPrice(jEq, jbreak) = ParseString(dummy, " ")
                DemandQuan(jEq, jbreak) = ParseString(dummy, " ")
                DemandQuan(jEq, jbreak) = DemandQuan(jEq, jbreak) * NYearPerTic
            Next jbreak
        Next jEq

        ' Base price info for Market Types
        ' ReDim MtypePr!(NMType, NMLocation, MxTic)
        dummy = LineInput(5)
        dummy = LineInput(5)
        MTypeNDef = 0
        dummy = Trim(LineInput(5))
        kkTypeBeg = ParseString(dummy, " ")
        kkTypeEnd = ParseString(dummy, " ")
        kkMLocBeg = ParseString(dummy, " ")
        kkMLocEnd = ParseString(dummy, " ")
        kkTicBeg = ParseString(dummy, " ")
        kkTicEnd = ParseString(dummy, " ")
        kkprice = ParseString(dummy, " ")
        kkNunits = ParseString(dummy, " ")
        Do Until kkTypeBeg < 0
            MTypeNDef = MTypeNDef + 1
            ReDim Preserve MTypePrDef_TypeBeg(MTypeNDef), MTypePrDef_TypeEnd(MTypeNDef), MTypePrDef_MLocBeg(MTypeNDef), MTypePrDef_MLocEnd(MTypeNDef), _
            MTypePrDef_TicBeg(MTypeNDef), MTypePrDef_TicEnd(MTypeNDef), MTypePrDef_price(MTypeNDef), MTypePrDef_Nunits(MTypeNDef)
            MTypePrDef_TypeBeg(MTypeNDef) = kkTypeBeg
            MTypePrDef_TypeEnd(MTypeNDef) = kkTypeEnd
            MTypePrDef_MLocBeg(MTypeNDef) = kkMLocBeg
            MTypePrDef_MLocEnd(MTypeNDef) = kkMLocEnd
            MTypePrDef_TicBeg(MTypeNDef) = kkTicBeg
            MTypePrDef_TicEnd(MTypeNDef) = kkTicEnd
            MTypePrDef_price(MTypeNDef) = kkprice
            MTypePrDef_Nunits(MTypeNDef) = kkNunits
            dummy = Trim(LineInput(5))
            kkTypeBeg = ParseString(dummy, " ")
            kkTypeEnd = ParseString(dummy, " ")
            kkMLocBeg = ParseString(dummy, " ")
            kkMLocEnd = ParseString(dummy, " ")
            kkTicBeg = ParseString(dummy, " ")
            kkTicEnd = ParseString(dummy, " ")
            kkprice = ParseString(dummy, " ")
            kkNunits = ParseString(dummy, " ")
        Loop

        ' Base Price info for Condition Types
        dummy = LineInput(5)
        dummy = LineInput(5)
        CondTypeNDef = 0
        dummy = Trim(LineInput(5))
        kkMapLayer = ParseString(dummy, " ")
        kkCoverSiteBeg = ParseString(dummy, " ")
        kkCoverSiteEnd = ParseString(dummy, " ")
        kkAgeBeg = ParseString(dummy, " ")
        kkAgeEnd = ParseString(dummy, " ")
        kkLayerColorBeg = ParseString(dummy, " ")
        kkLayerColorEnd = ParseString(dummy, " ")
        kkTicBeg = ParseString(dummy, " ")
        kkTicEnd = ParseString(dummy, " ")
        kkprice = ParseString(dummy, " ")
        kkNunits = ParseString(dummy, " ")
        Do Until kkMapLayer < 0
            CondTypeNDef = CondTypeNDef + 1
            ReDim Preserve CondTypePrDef_MapLayer(CondTypeNDef), CondTypePrDef_CoverSiteBeg(CondTypeNDef), _
            CondTypePrDef_CoverSiteEnd(CondTypeNDef), CondTypePrDef_AgeBeg(CondTypeNDef), CondTypePrDef_AgeEnd(CondTypeNDef), _
            CondTypePrDef_LayerColorBeg(CondTypeNDef), CondTypePrDef_LayerColorEnd(CondTypeNDef), CondTypePrDef_TicBeg(CondTypeNDef), _
            CondTypePrDef_TicEnd(CondTypeNDef), CondTypePrDef_price(CondTypeNDef), CondTypePrDef_Nunits(CondTypeNDef)
            CondTypePrDef_MapLayer(CondTypeNDef) = kkMapLayer
            CondTypePrDef_CoverSiteBeg(CondTypeNDef) = kkCoverSiteBeg
            CondTypePrDef_CoverSiteEnd(CondTypeNDef) = kkCoverSiteEnd
            CondTypePrDef_AgeBeg(CondTypeNDef) = kkAgeBeg
            CondTypePrDef_AgeEnd(CondTypeNDef) = kkAgeEnd
            CondTypePrDef_LayerColorBeg(CondTypeNDef) = kkLayerColorBeg
            CondTypePrDef_LayerColorEnd(CondTypeNDef) = kkLayerColorEnd
            CondTypePrDef_TicBeg(CondTypeNDef) = kkTicBeg
            CondTypePrDef_TicEnd(CondTypeNDef) = kkTicEnd
            CondTypePrDef_price(CondTypeNDef) = kkprice
            CondTypePrDef_Nunits(CondTypeNDef) = kkNunits
            dummy = Trim(LineInput(5))
            kkMapLayer = ParseString(dummy, " ")
            kkCoverSiteBeg = ParseString(dummy, " ")
            kkCoverSiteEnd = ParseString(dummy, " ")
            kkAgeBeg = ParseString(dummy, " ")
            kkAgeEnd = ParseString(dummy, " ")
            kkLayerColorBeg = ParseString(dummy, " ")
            kkLayerColorEnd = ParseString(dummy, " ")
            kkTicBeg = ParseString(dummy, " ")
            kkTicEnd = ParseString(dummy, " ")
            kkprice = ParseString(dummy, " ")
            kkNunits = ParseString(dummy, " ")
        Loop

        ' Base Price info for Spatial Types
        dummy = LineInput(5)
        dummy = LineInput(5)
        SpatTypeNDef = 0
        dummy = Trim(LineInput(5))
        kkTypeBeg = ParseString(dummy, " ")
        kkTypeEnd = ParseString(dummy, " ")
        kkTicBeg = ParseString(dummy, " ")
        kkTicEnd = ParseString(dummy, " ")
        kkprice = ParseString(dummy, " ")
        If kkTypeBeg > 0 And kkprice <> 0 Then bNeedSpatialIterations = True 'even if the constaint option for spatial sets is 0, a non-0 price triggers a DPSpace iteration
        kkNunits = ParseString(dummy, " ")
        'may want to revise this to just include spatial types from the associated input file
        Do Until kkTypeBeg < 0
            SpatTypeNDef = SpatTypeNDef + 1
            ReDim Preserve SpatTypePrDef_TypeBeg(SpatTypeNDef), SpatTypePrDef_TypeEnd(SpatTypeNDef), _
            SpatTypePrDef_TicBeg(SpatTypeNDef), SpatTypePrDef_TicEnd(SpatTypeNDef), _
            SpatTypePrDef_price(SpatTypeNDef), SpatTypePrDef_Nunits(SpatTypeNDef)
            SpatTypePrDef_TypeBeg(SpatTypeNDef) = kkTypeBeg
            SpatTypePrDef_TypeEnd(SpatTypeNDef) = kkTypeEnd
            SpatTypePrDef_TicBeg(SpatTypeNDef) = kkTicBeg
            SpatTypePrDef_TicEnd(SpatTypeNDef) = kkTicEnd
            SpatTypePrDef_price(SpatTypeNDef) = kkprice
            SpatTypePrDef_Nunits(SpatTypeNDef) = kkNunits
            dummy = Trim(LineInput(5))
            kkTypeBeg = ParseString(dummy, " ")
            kkTypeEnd = ParseString(dummy, " ")
            kkTicBeg = ParseString(dummy, " ")
            kkTicEnd = ParseString(dummy, " ")
            kkprice = ParseString(dummy, " ")
            kkNunits = ParseString(dummy, " ")
        Loop

        FileClose(5)

        'OPEN THE LABELS FILE
        FileOpen(25, FnLabels, OpenMode.Input)

        ReDim MTypeLabel(NMType)
        ReDim MapAreaLabel(NMapArea)
        ReDim coversiteLabel(Ncoversite)
        ReDim SpatTypeLabel(NSpaceType)
        ReDim MLocationLabel(NMLocation)

        dummy = LineInput(25)
        For jmtype As Integer = 1 To NMType
            MTypeLabel(jmtype) = Trim(LineInput(25))
        Next jmtype

        dummy = LineInput(25)
        For jArea As Integer = 1 To NMapArea
            dummy = Trim(LineInput(25))
            Dim kkmap As Integer = ParseString(dummy, ",")
            Dim kkcolor As Integer = ParseString(dummy, ",")
            MapAreaLabel(jArea) = ParseString(dummy, ",")
        Next jArea

        dummy = LineInput(25)
        For jcs As Integer = 1 To Ncoversite
            coversiteLabel(jcs) = LineInput(25)
        Next jcs

        'spatial types labels
        dummy = LineInput(25)
        For jst As Integer = 1 To NSpaceType
            SpatTypeLabel(jst) = LineInput(25)
        Next jst

        dummy = LineInput(25)
        For jloc As Integer = 1 To NMLocation
            MLocationLabel(jloc) = LineInput(25)
        Next jloc

        FileClose(25)

        'LOAD SPATIAL TYPE DEFINITIONS
        LoadSpaceTypeDefs(ISpaceDefFile)

        'LOAD THE SPATIAL SERIES TIC FILE
        LoadSpatialSeriesTics(SpatialSeriesFile)

        'LOAD THE SUBFOREST FILE INFORMATION

        LoadSubForestInfo(FnAAFileList, DualPlanSubForests)

        'LOAD THE FIRST SUBFOREST
        kaaset = 1
        kaasetdirection = 1

        AA_NPolys = DualPlanSubForests(kaaset).NPolys
        ReDim DualPlanAAPoly_polyid(AA_NPolys)
        'ReDim DualPlanAAPoly_PassToSpace(, )  'true means keep, false means it gets filtered...
        ReDim DualPlanAAPoly_ISpaceSelf(AA_NPolys)  ' the area of the 1-way influence zone within the borders of this stand
        ReDim DualPlanAAPoly_NPVallMx(AA_NPolys)  'The maximum NPVValMxSpat - dimensioned by stand
        'ReDim DualPlanAAPoly_NPVSelf(, )  'by prescription, sum of NPVAspatial and the guaranteed interior value of the stand - Includes infinite series of those spatial flows...
        ReDim DualPlanAAPoly_NPValLbnd(AA_NPolys)  'aka NPVMin, or largest NPVSelf 
        ReDim DualPlanAAPoly_EstISpacePct(AA_NPolys)  'estimated percentage of the 'edge' area that is in ispace for the poly - a function of npvlbnd and npvmax
        'ReDim DualPlanAAPoly_StandID(AA_NPolys)  'Stand ID of the stand in question
        ReDim DualPlanAAPoly_NRxIn(AA_NPolys)  'number of non-trimmed prescriptions of the polygon
        ReDim DualPlanAAPoly_PolyRxSpace(AA_NPolys)  '0/1 based on whether the stand has a non-trimmed spatial prescription
        ReDim DualPlanAAPoly_NIZones(AA_NPolys)

        ReDim PolySolRx_ChosenRxInd(AA_NPolys)
        ReDim PolySolRx_AspatNPV(AA_NPolys)
        ReDim PolySolRx_SpatNPV(AA_NPolys)
        ReDim PolySolRx_MarketNPV(AA_NPolys)
 
        LoadSubForest(DualPlanSubForests(kaaset))
        LoadPolySolFile(DualPlanSubForests(kaaset).PolySchedFile)
        'NEED TO LOAD IZONES
        LoadIZones(DualPlanSubForests(kaaset).IZonesFile)
        PopulateSets()

        'set the adjustment set counters = 0
        AdjSetCounter = 0
        SchSetCounter = 0


    End Sub

    Private Sub PopulateSets()
        'uses the read in structure sets to populate straight arrays - hopefully makes runs faster

        Dim NSets As Integer

        NSets = CondSet.Length - 1

        ReDim CondSet_Type(NSets)  'can be market, condition, spatial
        ReDim CondSet_nSetDefs(NSets)  ' set definition lines
        '------------------------------
        'the next few lines replace the  CondSet data structure
        ReDim CondSet_kMapLayer(NSets, CondSetMxNDefs)
        '  Dim kset() As Short
        ReDim CondSet_kTypeBeg(NSets, CondSetMxNDefs)
        ReDim CondSet_kTypeEnd(NSets, CondSetMxNDefs)
        ReDim CondSet_kLocBeg(NSets, CondSetMxNDefs)  'may only be used for market type sets
        ReDim CondSet_kLocEnd(NSets, CondSetMxNDefs)  'may only be used for market type sets
        ReDim CondSet_kagebeg(NSets, CondSetMxNDefs)
        ReDim CondSet_kageend(NSets, CondSetMxNDefs)
        ReDim CondSet_kLayerColorBeg(NSets, CondSetMxNDefs)
        ReDim CondSet_kLayerColorEnd(NSets, CondSetMxNDefs)
        ReDim CondSet_convfact(NSets, CondSetMxNDefs)
        ReDim CondSet_convprob(NSets, CondSetMxNDefs)
        '------------------------------------
        'Dim Disc() As Single 'discount rate is just one array...same for every set
        ReDim CondSet_BaseQuan(NSets)
        ReDim CondSet_ConstraintOption(NSets)
        ReDim CondSet_DemandTableID(NSets, NTic1)
        ReDim CondSet_Dev(NSets, NIterToSave, NTic1)
        ReDim CondSet_DevMx(NSets)
        ReDim CondSet_DevMn(NSets)
        ReDim CondSet_EndPrWghtSet(NSets)
        'ReDim CondSet_FlowNSets(NSets, NIterToSave, NTic1)
        ReDim CondSet_Flow(NSets, NIterToSave, NTic1)
        ReDim CondSet_FlowMn(NSets, NTic1)
        ReDim CondSet_FlowMx(NSets, NTic1)
        ReDim CondSet_FlowMn1(NSets)
        ReDim CondSet_FlowMx1(NSets)
        ReDim CondSet_Label(NSets)
        ReDim CondSet_Pr(NSets, NIterToSave, NTic1)  'by iterations to save, tic
        ReDim CondSet_PrAdjSet(NSets)
        ReDim CondSet_Target(NSets, NTic1)

        For jCondset As Integer = 1 To CondSet.Length - 1
            CondSet_Type(jCondset) = CondSet(jCondset).SetType  'can be market, condition, spatial
            CondSet_nSetDefs(jCondset) = CondSet(jCondset).nSetDefs ' set definition lines
            '------------------------------
            For jSetDef As Integer = 1 To CondSet_nSetDefs(jCondset)
                CondSet_kMapLayer(jCondset, jSetDef) = CondSet(jCondset).kMapLayer(jSetDef)
                CondSet_kTypeBeg(jCondset, jSetDef) = CondSet(jCondset).kTypeBeg(jSetDef)
                CondSet_kTypeEnd(jCondset, jSetDef) = CondSet(jCondset).kTypeEnd(jSetDef)
                CondSet_kLocBeg(jCondset, jSetDef) = CondSet(jCondset).kLocBeg(jSetDef) 'may only be used for market type sets
                CondSet_kLocEnd(jCondset, jSetDef) = CondSet(jCondset).kLocEnd(jSetDef)  'may only be used for market type sets
                CondSet_kagebeg(jCondset, jSetDef) = CondSet(jCondset).kagebeg(jSetDef)
                CondSet_kageend(jCondset, jSetDef) = CondSet(jCondset).kageend(jSetDef)
                CondSet_kLayerColorBeg(jCondset, jSetDef) = CondSet(jCondset).kLayerColorBeg(jSetDef)
                CondSet_kLayerColorEnd(jCondset, jSetDef) = CondSet(jCondset).kLayerColorEnd(jSetDef)
                CondSet_convfact(jCondset, jSetDef) = CondSet(jCondset).convfact(jSetDef)
                CondSet_convprob(jCondset, jSetDef) = CondSet(jCondset).convprob(jSetDef)
            Next jSetDef
            '------------------------------------
            'Dim Disc() As Single 'discount rate is just one array...same for every set
            CondSet_BaseQuan(jCondset) = CondSet(jCondset).SetBaseQuan
            CondSet_PrAdjSet(jCondset) = CondSet(jCondset).setPrAdjSet
            CondSet_ConstraintOption(jCondset) = CondSet(jCondset).SetConstraintOption
            CondSet_DevMx(jCondset) = CondSet(jCondset).SetDevMx
            CondSet_DevMn(jCondset) = CondSet(jCondset).SetDevMx
            CondSet_EndPrWghtSet(jCondset) = CondSet(jCondset).SetEndPrWghtSet
            CondSet_FlowMn1(jCondset) = CondSet(jCondset).SetFlowMn1
            CondSet_FlowMx1(jCondset) = CondSet(jCondset).SetFlowMx1
            CondSet_Label(jCondset) = CondSet(jCondset).SetLabel

            For jtic As Integer = 1 To NTic1
                CondSet_DemandTableID(jCondset, jtic) = CondSet(jCondset).SetDemandTableID(jtic)
                'ReDim CondSet_FlowNSets(NSets, NIterToSave, NTic1)
                CondSet_FlowMn(jCondset, jtic) = CondSet(jCondset).SetFlowMn(jtic)
                CondSet_FlowMx(jCondset, jtic) = CondSet(jCondset).SetFlowMx(jtic)
                CondSet_Target(jCondset, jtic) = CondSet(jCondset).SetTarget(jtic)
                For jiter As Integer = 1 To NIterToSave
                    CondSet_Dev(jCondset, jiter, jtic) = CondSet(jCondset).SetDev(jiter, jtic)
                    CondSet_Pr(jCondset, jiter, jtic) = CondSet(jCondset).SetPr(jiter, jtic)  'by iterations to save, tic
                Next jiter
            Next jtic
        Next jCondset



        '---------------------------------------------------------------
        'MARKET SETS
        NSets = MSet.Length - 1

        ReDim MSet_Type(NSets)  'can be market, condition, spatial
        ReDim MSet_nSetDefs(NSets)  ' set definition lines
        '------------------------------
        'the next few lines replace the  MSet_ data structure
        ReDim MSet_kMapLayer(NSets, MSetMxNDefs)
        ReDim MSet_kTypeBeg(NSets, MSetMxNDefs)
        ReDim MSet_kTypeEnd(NSets, MSetMxNDefs)
        ReDim MSet_kLocBeg(NSets, MSetMxNDefs)  'may only be used for market type sets
        ReDim MSet_kLocEnd(NSets, MSetMxNDefs) 'may only be used for market type sets
        ReDim MSet_convfact(NSets, MSetMxNDefs)

        ReDim MSet_BaseQuan(NSets)
        ReDim MSet_ConstraintOption(NSets)
        ReDim MSet_DemandTableID(NSets, NTic1)
        ReDim MSet_Dev(NSets, NIterToSave, NTic1)
        ReDim MSet_DevMx(NSets)
        ReDim MSet_DevMn(NSets)
        ReDim MSet_EndPrWghtSet(NSets)
        ReDim MSet_Flow(NSets, NIterToSave, NTic1)
        ReDim MSet_FlowMn(NSets, NTic1)
        ReDim MSet_FlowMx(NSets, NTic1)
        ReDim MSet_FlowMn1(NSets)
        ReDim MSet_FlowMx1(NSets)
        ReDim MSet_Label(NSets)
        ReDim MSet_Pr(NSets, NIterToSave, NTic1)  'by iterations to save, tic
        ReDim MSet_PrAdjSet(NSets)
        ReDim MSet_Target(NSets, NTic1)

        For jMset As Integer = 1 To MSet.Length - 1
            MSet_Type(jMset) = MSet(jMset).SetType  'can be market, Spatition, spatial
            MSet_nSetDefs(jMset) = MSet(jMset).nSetDefs ' set definition lines
            '------------------------------
            For jSetDef As Integer = 1 To MSet_nSetDefs(jMset)
                MSet_kMapLayer(jMset, jSetDef) = MSet(jMset).kMapLayer(jSetDef)
                MSet_kTypeBeg(jMset, jSetDef) = MSet(jMset).kTypeBeg(jSetDef)
                MSet_kTypeEnd(jMset, jSetDef) = MSet(jMset).kTypeEnd(jSetDef)
                MSet_kLocBeg(jMset, jSetDef) = MSet(jMset).kLocBeg(jSetDef) 'may only be used for market type sets
                MSet_kLocEnd(jMset, jSetDef) = MSet(jMset).kLocEnd(jSetDef)  'may only be used for market type sets
                'SpatSet_kagebeg(jSpatset, jSetDef) = SpatSet(jSpatset).kagebeg(jSetDef)
                'SpatSet_kageend(jSpatset, jSetDef) = SpatSet(jSpatset).kageend(jSetDef)
                'SpatSet_kLayerColorBeg(jSpatset, jSetDef) = SpatSet(jSpatset).kLayerColorBeg(jSetDef)
                'SpatSet_kLayerColorEnd(jSpatset, jSetDef) = SpatSet(jSpatset).kLayerColorEnd(jSetDef)
                MSet_convfact(jMset, jSetDef) = MSet(jMset).convfact(jSetDef)
                'SpatSet_convprob(jSpatset, jSetDef) = SpatSet(jSpatset).convprob(jSetDef)
            Next jSetDef
            '------------------------------------
            'Dim Disc() As Single 'discount rate is just one array...same for every set
            MSet_BaseQuan(jMset) = MSet(jMset).SetBaseQuan
            MSet_PrAdjSet(jMset) = MSet(jMset).setPrAdjSet
            MSet_ConstraintOption(jMset) = MSet(jMset).SetConstraintOption
            MSet_DevMx(jMset) = MSet(jMset).SetDevMx
            MSet_DevMn(jMset) = MSet(jMset).SetDevMx
            MSet_EndPrWghtSet(jMset) = MSet(jMset).SetEndPrWghtSet
            MSet_FlowMn1(jMset) = MSet(jMset).SetFlowMn1
            MSet_FlowMx1(jMset) = MSet(jMset).SetFlowMx1
            MSet_Label(jMset) = MSet(jMset).SetLabel

            For jtic As Integer = 1 To NTic1
                MSet_DemandTableID(jMset, jtic) = MSet(jMset).SetDemandTableID(jtic)
                'ReDim SpatSet_FlowNSets(NSets, NIterToSave, NTic1)
                MSet_FlowMn(jMset, jtic) = MSet(jMset).SetFlowMn(jtic)
                MSet_FlowMx(jMset, jtic) = MSet(jMset).SetFlowMx(jtic)
                MSet_Target(jMset, jtic) = MSet(jMset).SetTarget(jtic)
                For jiter As Integer = 1 To NIterToSave
                    MSet_Dev(jMset, jiter, jtic) = MSet(jMset).SetDev(jiter, jtic)
                    MSet_Pr(jMset, jiter, jtic) = MSet(jMset).SetPr(jiter, jtic)  'by iterations to save, tic
                Next jiter
            Next jtic
        Next jMset


        '---------------------------------------------------------------
        'SPATIAL SETS
        NSets = SpatSet.Length - 1

        ReDim SpatSet_Type(NSets)  'can be market, Spatition, spatial
        ReDim SpatSet_nSetDefs(NSets)  ' set definition lines
        '------------------------------
        'the next few lines replace the  SpatSet data structure
        ReDim SpatSet_kMapLayer(NSets, SpatSetMxNDefs)
        '  Dim kset() As Short
        ReDim SpatSet_kTypeBeg(NSets, SpatSetMxNDefs)
        ReDim SpatSet_kTypeEnd(NSets, SpatSetMxNDefs)
        ReDim SpatSet_kLocBeg(NSets, SpatSetMxNDefs)  'may only be used for market type sets
        ReDim SpatSet_kLocEnd(NSets, SpatSetMxNDefs)  'may only be used for market type sets
        'ReDim SpatSet_kagebeg(NSets, SpatSetMxNDefs)
        'ReDim SpatSet_kageend(NSets, SpatSetMxNDefs)
        'ReDim SpatSet_kLayerColorBeg(NSets, SpatSetMxNDefs)
        'ReDim SpatSet_kLayerColorEnd(NSets, SpatSetMxNDefs)
        ReDim SpatSet_convfact(NSets, SpatSetMxNDefs)
        'ReDim SpatSet_convprob(NSets, SpatSetMxNDefs)
        '------------------------------------
        'Dim Disc() As Single 'discount rate is just one array...same for every set
        ReDim SpatSet_BaseQuan(NSets)
        ReDim SpatSet_ConstraintOption(NSets)
        ReDim SpatSet_DemandTableID(NSets, NTic1)
        ReDim SpatSet_Dev(NSets, NIterToSave, NTic1)
        ReDim SpatSet_DevMx(NSets)
        ReDim SpatSet_DevMn(NSets)
        ReDim SpatSet_EndPrWghtSet(NSets)
        'ReDim SpatSet_FlowNSets(NSets, NIterToSave, NTic1)
        ReDim SpatSet_Flow(NSets, NIterToSave, NTic1)
        ReDim SpatSet_FlowMn(NSets, NTic1)
        ReDim SpatSet_FlowMx(NSets, NTic1)
        ReDim SpatSet_FlowMn1(NSets)
        ReDim SpatSet_FlowMx1(NSets)
        ReDim SpatSet_Label(NSets)
        ReDim SpatSet_Pr(NSets, NIterToSave, NTic1)  'by iterations to save, tic
        ReDim SpatSet_PrAdjSet(NSets)
        ReDim SpatSet_Target(NSets, NTic1)

        For jSpatset As Integer = 1 To SpatSet.Length - 1
            SpatSet_Type(jSpatset) = SpatSet(jSpatset).SetType  'can be market, Spatition, spatial
            SpatSet_nSetDefs(jSpatset) = SpatSet(jSpatset).nSetDefs ' set definition lines
            '------------------------------
            For jSetDef As Integer = 1 To SpatSet_nSetDefs(jSpatset)
                SpatSet_kMapLayer(jSpatset, jSetDef) = SpatSet(jSpatset).kMapLayer(jSetDef)
                SpatSet_kTypeBeg(jSpatset, jSetDef) = SpatSet(jSpatset).kTypeBeg(jSetDef)
                SpatSet_kTypeEnd(jSpatset, jSetDef) = SpatSet(jSpatset).kTypeEnd(jSetDef)
                SpatSet_kLocBeg(jSpatset, jSetDef) = SpatSet(jSpatset).kLocBeg(jSetDef) 'may only be used for market type sets
                SpatSet_kLocEnd(jSpatset, jSetDef) = SpatSet(jSpatset).kLocEnd(jSetDef)  'may only be used for market type sets
                'SpatSet_kagebeg(jSpatset, jSetDef) = SpatSet(jSpatset).kagebeg(jSetDef)
                'SpatSet_kageend(jSpatset, jSetDef) = SpatSet(jSpatset).kageend(jSetDef)
                'SpatSet_kLayerColorBeg(jSpatset, jSetDef) = SpatSet(jSpatset).kLayerColorBeg(jSetDef)
                'SpatSet_kLayerColorEnd(jSpatset, jSetDef) = SpatSet(jSpatset).kLayerColorEnd(jSetDef)
                SpatSet_convfact(jSpatset, jSetDef) = SpatSet(jSpatset).convfact(jSetDef)
                'SpatSet_convprob(jSpatset, jSetDef) = SpatSet(jSpatset).convprob(jSetDef)
            Next jSetDef
            '------------------------------------
            'Dim Disc() As Single 'discount rate is just one array...same for every set
            SpatSet_BaseQuan(jSpatset) = SpatSet(jSpatset).SetBaseQuan
            SpatSet_PrAdjSet(jSpatset) = SpatSet(jSpatset).setPrAdjSet
            SpatSet_ConstraintOption(jSpatset) = SpatSet(jSpatset).SetConstraintOption
            SpatSet_DevMx(jSpatset) = SpatSet(jSpatset).SetDevMx
            SpatSet_DevMn(jSpatset) = SpatSet(jSpatset).SetDevMx
            SpatSet_EndPrWghtSet(jSpatset) = SpatSet(jSpatset).SetEndPrWghtSet
            SpatSet_FlowMn1(jSpatset) = SpatSet(jSpatset).SetFlowMn1
            SpatSet_FlowMx1(jSpatset) = SpatSet(jSpatset).SetFlowMx1
            SpatSet_Label(jSpatset) = SpatSet(jSpatset).SetLabel

            For jtic As Integer = 1 To NTic1
                SpatSet_DemandTableID(jSpatset, jtic) = SpatSet(jSpatset).SetDemandTableID(jtic)
                'ReDim SpatSet_FlowNSets(NSets, NIterToSave, NTic1)
                SpatSet_FlowMn(jSpatset, jtic) = SpatSet(jSpatset).SetFlowMn(jtic)
                SpatSet_FlowMx(jSpatset, jtic) = SpatSet(jSpatset).SetFlowMx(jtic)
                SpatSet_Target(jSpatset, jtic) = SpatSet(jSpatset).SetTarget(jtic)
                For jiter As Integer = 1 To NIterToSave
                    SpatSet_Dev(jSpatset, jiter, jtic) = SpatSet(jSpatset).SetDev(jiter, jtic)
                    SpatSet_Pr(jSpatset, jiter, jtic) = SpatSet(jSpatset).SetPr(jiter, jtic)  'by iterations to save, tic
                Next jiter
            Next jtic
        Next jSpatset

    End Sub

    Private Sub PatchStats(ByVal Aggoutfile As String, ByVal Patchoutfile As String, ByVal kaaset As Integer, ByVal PolySol() As Long)
        GC.Collect()
        'subroutine goes through the latest solution and tallies patches by coversite in each time period

        'logic - go through each Influence zone (another way of measuring adjacency; already set up
        'search for the max stand ID associated with each influence zone/patch

        'ultimately trying to populate the following two sets:
        Dim PatchSize(,) As Double 'by period, coversite, NStands (max possible patches) - the acres of the patch
        Dim PatchNStands(,) As Integer 'by period,coversite , NStands (max possible patches) - number of stands in hte patch
        Dim NPatches(,) As Integer
        Dim TicPatchSize(,) As Double


        'reassign a stand to the max ID associated with each patch
        Dim StandMaxID(,) As Long 'by period, stand...
        Dim mxid As Integer

        'for evaluating adjacent stands and such
        Dim kaa As Integer
        Dim kcoversite As Integer
        Dim zoneID As Integer
        Dim bStandInZone As Boolean

        'for evaluating when the patches have been fully defined
        Dim matchper As Integer = 1
        Dim matchstand As Integer = 1


        ReDim NPatches(MxTic, Ncoversite)
        ReDim TicPatchSize(MxTic, Ncoversite)
        ReDim StandMaxID(MxTic, AA_NPolys)


        'default values for max stand ID is itself
        For jaa As Integer = 1 To AA_NPolys
            For jtic As Integer = 1 To MxTic
                StandMaxID(jtic, jaa) = jaa
            Next jtic
        Next

        Dim fnum As Integer = FreeFile()
        FileOpen(fnum, Patchoutfile, OpenMode.Output)
        PrintLine(fnum, "Tic, Coversite, NStands, Size")
        For jtic As Integer = 1 To MxTic
            ReDim PatchSize(Ncoversite, AA_NPolys)
            ReDim PatchNStands(Ncoversite, AA_NPolys)
            matchper = 1
            Do Until matchper = 0
                matchper = 0
                For jaa As Integer = 1 To AA_NPolys 'AASetNAA(kaaset)
                    Do Until AA_polyid(jaa) > 0 'control for blank aa information
                        jaa = jaa + 1
                    Loop
                    'kcoversite = AApres_Coversite(PolySol(jaa), jtic)
                    For jiz As Integer = 1 To DualPlanAAPoly_NIZones(jaa)
                        zoneID = DualPlanAAPoly_IZones(jaa, jiz)
                        bStandInZone = False
                        'find whether the parent AA has area in this influence zone
                        'if ZoneAAlistAreaS(zoneid,
                        For jdim As Integer = 1 To Zonedim(zoneID)
                            kaa = ZoneAAlist(zoneID, jdim)
                            If AA_polyid(jaa) = AA_polyid(kaa) And ZoneAAlistAreaS(zoneID, jdim) > 0 Then
                                bStandInZone = True
                                Exit For
                            End If
                        Next
                        If bStandInZone = True Then
                            For jjaa As Integer = 1 To Zonedim(zoneID)
                                kaa = ZoneAAlist(zoneID, jjaa)
                                'If kaa <> jaa Then
                                'check to see if their covertype in this period matches
                                If AApres_Coversite(PolySol(jaa), jtic) = AApres_Coversite(PolySol(kaa), jtic) Then
                                    'if covertype matches, then assign the largest StandID of both stands to the 
                                    'largest of the two
                                    If StandMaxID(jtic, jaa) <> StandMaxID(jtic, kaa) Then
                                        mxid = Math.Max(StandMaxID(jtic, jaa), StandMaxID(jtic, kaa))
                                        StandMaxID(jtic, jaa) = mxid
                                        StandMaxID(jtic, kaa) = mxid
                                        matchper = matchper + 1
                                    End If
                                End If
                                'End If
                            Next
                        End If
                    Next jiz
                Next jaa
            Loop
            'record the max patch stats for this period
            For jaa As Integer = 1 To AA_NPolys 'AASetNAA(kaaset)
                kaa = StandMaxID(jtic, jaa)
                kcoversite = AApres_Coversite(PolySol(kaa), jtic)
                PatchSize(kcoversite, kaa) = PatchSize(kcoversite, kaa) + AA_area(jaa)
                PatchNStands(kcoversite, kaa) = PatchNStands(kcoversite, kaa) + 1

            Next
            For jcoversite As Integer = 1 To Ncoversite
                For jaa As Integer = 1 To AA_NPolys
                    If PatchNStands(jcoversite, jaa) > 0 Then
                        NPatches(jtic, jcoversite) = NPatches(jtic, jcoversite) + 1
                        TicPatchSize(jtic, jcoversite) = TicPatchSize(jtic, jcoversite) + PatchSize(jcoversite, jaa)
                        PrintLine(fnum, jtic & "," & jcoversite & "," & PatchNStands(jcoversite, jaa) & "," & Math.Round(PatchSize(jcoversite, jaa), 1))
                    End If
                Next jaa
            Next jcoversite
        Next jtic
        FileClose(fnum)

        'Dim fnum As Integer = FreeFile()
        FileOpen(fnum, Aggoutfile, OpenMode.Output)
        'PrintLine(fnum, "Aggregated Patch Statistics")
        PrintLine(fnum, "Tic, Coversite, NPatches, Total_Area, Average_Size")
        For jtic As Integer = 1 To MxTic
            For jcoversite As Integer = 1 To Ncoversite
                If NPatches(jtic, jcoversite) > 0 Then PrintLine(fnum, jtic & "," & jcoversite & "," & NPatches(jtic, jcoversite) & "," & _
                   Math.Round(TicPatchSize(jtic, jcoversite), 1) & "," & Math.Round(TicPatchSize(jtic, jcoversite) / NPatches(jtic, jcoversite), 1))
            Next
        Next
        FileClose(fnum)

        'get rid of these large arrays
        ReDim PatchSize(0, 0)
        ReDim PatchNStands(0, 0)
        ReDim NPatches(0, 0)
        ReDim TicPatchSize(0, 0)
        ReDim StandMaxID(0, 0)

    End Sub

    Public Sub WriteIterOutputFiles(ByVal FileExt As String, ByVal SchedArray() As Long)

        DualPlanSubForests(kaaset).PolySchedFileWrite = FnDualPlanOutBaseA & "PolySched" & kaaset & "." & FileExt
        WritePolySolFile(DualPlanSubForests(kaaset).PolySchedFileWrite, kaaset, SchedArray)
        WriteSpatialOutput(FnDualPlanOutBaseA & "SpatialFlowsByPolySubfor" & kaaset & "_" & FileExt & ".csv", kaaset, SchedArray)
        WriteConditionOutput(FnDualPlanOutBaseA & "CondFlowsByPolySubfor" & kaaset & "_" & FileExt & ".csv", kaaset, SchedArray)
        PatchStats(FnDualPlanOutBaseA & "AggPatchStats" & kaaset & "_" & FileExt & ".csv", FnDualPlanOutBaseA & "PatchStats" & kaaset & "_" & FileExt & ".csv", kaaset, SchedArray)

    End Sub

    Public Sub WriteNewInput(ByVal outfile As String)
        'assume all written variables are public and defined...
        Dim jEq As Short
        Dim jCondSet As Short
        Dim jSpatSet As Short
        Dim jdef As Short
        Dim jmset As Short
        'Dim FirstDef As Short
        Dim jjTic As Short
        Dim jbreak As Short
        Dim jadj As Short
        Dim jtic As Short
        Dim jset As Short
        Dim jlayer As Short
        FileOpen(66, outfile, OpenMode.Output)
        PrintLine(66, "Name of File listing Binary files for AA's")
        PrintLine(66, FnAAFileList)

        PrintLine(66, "Basefile name for new input files")
        PrintLine(66, FnDualPlanOutBaseA & LTrim(Str(IterationNumber)))

        PrintLine(66, "Generic File Name for Schedule File to read (optional)")
        PrintLine(66, FnDualPlanOutBaseA & "PolySched." & PolySchedFileExt) 'FnDualPlanOutBaseA & LTrim(Str(IterationNumber)))

        PrintLine(66, "Name of input file with labels")
        PrintLine(66, FnLabels)

        PrintLine(66, "Name of file with Spatial Type Definitions")
        PrintLine(66, ISpaceDefFile)

        PrintLine(66, "Name of file with Spatial Series Information")
        PrintLine(66, SpatialSeriesFile)

        'PrintLine(66, "NIterToSave", "IterPerFileSave")
        'PrintLine(66, NIterToSave, IterPerFileSave)
        PrintLine(66, "NIterToSave", "NiterToRewrite")
        PrintLine(66, NIterToSave, NiterToRewrite)

        PrintLine(66, "NFloat", "NSmooth", "NShape")
        PrintLine(66, NFloat, NSmooth, NShape)

        '######################NEED TO WRITE SPATIAL DEFS # IN HERE...
        PrintLine(66, "NCondSet", "NMSet", "NSpatSet", "InterestRate!", "NTic", "NYearPerTic")
        PrintLine(66, NCondSet, NMSet, NSpatSet, InterestRate, NTic, NYearPerTic)

        PrintLine(66, "WriteMSet", "WriteMType", "WriteCondSet", "WritecoversiteAge", "WriteSpatSet", "WriteSpatType")
        PrintLine(66, WriteMSet, WriteMType, WriteCondSet, WritecoversiteAge, WriteSpatSet, WriteSpatType)

        PrintLine(66, "Ncoversite", "NMType", "NSpatType", "NMLocation", "MTypeSiteConvCost  MTypeEntryCost  EntryCost!")
        PrintLine(66, Ncoversite, NMType, NSpaceType, NMLocation, MTypeSiteConvCost, MTypeEntryCost, EntryCost)

        PrintLine(66, "MxAgeClass")
        PrintLine(66, MxAgeClass)

        PrintLine(66, "NMapLayer")
        PrintLine(66, NMapLayer)
        PrintLine(66, "Number of colors per layer")
        For jlayer = 1 To NMapLayer
            Print(66, MapLayerNColor(jlayer))
        Next jlayer
        PrintLine(66)
        PrintLine(66, "EndPriceNWghtSet")
        PrintLine(66, EndPriceNWghtSet)
        PrintLine(66, "EndPriceWght!(jset, jtic)")

        For jset = 1 To EndPriceNWghtSet
            For jtic = 1 To NTic
                Print(66, TAB(jtic * 7), EndPriceWght(jset, jtic))
            Next jtic
            PrintLine(66)
        Next jset



        '        Equation 1 Prescribed Burns for Openings
        '        EquationNDef(NBreakPoints)
        '    1                5     
        'kCoverSiteBeg  kCoverSiteEnd  kagebeg  kageend  MTypeEntryCostBeg  MTypeEntryCostEnd  
        '     25               25            1        150        12               12 
        '        EntryAdjAcres(AdjFactor)
        '               0             1.2 
        '               6             1.15 
        '               50            1 
        '               200           0.78 
        '               999999        0.75 

        'entry cost adjustment factors
        PrintLine(66, "EntryAdjustNEquation", "EntryAdjustMxBreakPoint")
        PrintLine(66, EntryAdjNEquation, EntryAdjMxBreakPoint)
        For jEq = 1 To EntryAdjNEquation
            PrintLine(66, "Equation " & jEq)
            PrintLine(66, "EquationNDef", "NBreakPoints")
            PrintLine(66, EntryAdjNDef(jEq), EntryAdjNPoint(jEq))
            PrintLine(66, "kCoverSiteBeg  kCoverSiteEnd  kagebeg  kageend  MTypeEntryCostBeg  MTypeEntryCostEnd")
            For jbreak = 1 To EntryAdjNDef(jEq)
                PrintLine(66, TAB, EntryAdjkCoverSiteBeg(jEq, jbreak), EntryAdjkCoverSiteEnd(jEq, jbreak), EntryAdjkagebeg(jEq, jbreak), EntryAdjkageend(jEq, jbreak), MTypeEntryCostBeg(jEq, jbreak), MTypeEntryCostEnd(jEq, jbreak))
            Next
            PrintLine(66, TAB, "EntryAdjAcres", "AdjFactor")
            For jbreak = 1 To EntryAdjNPoint(jEq)
                PrintLine(66, TAB, EntryAdjAcres(jEq, jbreak), EntryAdjFactor(jEq, jbreak))
            Next jbreak
        Next jEq


        'price adjustment sets
        PrintLine(66, "PriceAdjNSet", "PriceAdjMxBreakPoint")
        PrintLine(66, PriceAdjNSet, PriceAdjMxBreakPoint)

        For jadj = 1 To PriceAdjNSet

            Print(66, "Adj set" & jadj)
            PrintLine(66, " FloatStopPercent", "FloatNBreak", "SmoothNBreak", "ShapeNBreak")
            PrintLine(66, TAB, FloatStopPercent(jadj), FloatNBreak(jadj), SmoothNBreak(jadj), ShapeNBreak(jadj))

            PrintLine(66, "       FloatPercentDev   FloatPriceAdj!")
            'UPGRADE_WARNING: Couldn't resolve default property of object FloatNBreak(jadj). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For jbreak = 1 To FloatNBreak(jadj)
                PrintLine(66, TAB, FloatPercentDev(jadj, jbreak), FloatPriceAdj(jadj, jbreak))
            Next jbreak

            PrintLine(66, "       SmoothPercentDev  SmoothPriceAdj!")
            'UPGRADE_WARNING: Couldn't resolve default property of object SmoothNBreak(jadj). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For jbreak = 1 To SmoothNBreak(jadj)
                PrintLine(66, TAB, SmoothPercentDev(jadj, jbreak), SmoothPriceAdj(jadj, jbreak))
            Next jbreak

            PrintLine(66, "       ShapePercentDev   ShapePriceAdj!")
            'UPGRADE_WARNING: Couldn't resolve default property of object ShapeNBreak(jadj). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For jbreak = 1 To ShapeNBreak(jadj)
                PrintLine(66, TAB, ShapePercentDev(jadj, jbreak), ShapePriceAdj(jadj, jbreak))
            Next jbreak
        Next jadj

        PrintLine(66, "Shape Weights")
        For jtic = 1 To NTic
            For jjTic = 1 To NTic
                Print(66, TAB(jjTic * 7), ShapeWght(jtic, jjTic))
                ShapeWghtTot(jtic) = ShapeWghtTot(jtic) + ShapeWght(jtic, jjTic)
            Next jjTic
            PrintLine(66)
        Next jtic
        PrintLine(66, "Options for MSetConstraintOption and CondSetConstraintOption")
        PrintLine(66, "0. Unconstrained")
        PrintLine(66, "1. Target Levels")
        PrintLine(66, "2. Demand = f(price)")
        PrintLine(66, "3. Lower Bounds Only, >= constraints, nonnegative shadow prices ")
        PrintLine(66, "4. Upper Bounds Only, <= constraints, nonpositive shadow prices ")
        PrintLine(66, "5. Both Upper and Lower Bounds -- Absolute levels")
        PrintLine(66, "6. Limited % Changes over time with first period limits")
        PrintLine(66, "7. Limited % Changes over time without first period limits")

        'MSet Info

        'FirstDef = 1
        For jmset = 1 To NMSet
            PrintLine(66, "Market Set")
            PrintLine(66, jmset)
            PrintLine(66, MSet_Label(jmset))
            PrintLine(66, "           MSetNDef   MSetConstraintOption        MsetPrAdjSet   MsetEndPrWghtSet")
            PrintLine(66, TAB, MSet_nSetDefs(jmset), MSet_ConstraintOption(jmset), TAB, TAB, MSet_PrAdjSet(jmset), MSet_EndPrWghtSet(jmset))
            PrintLine(66, "              MTypeBeg   ", "kMTypeEnd", "LocBeg", "LocEnd", "convfact")

            For jdef = 1 To MSet_nSetDefs(jmset)
                PrintLine(66, TAB, MSet_kTypeBeg(jmset, jdef), MSet_kTypeEnd(jmset, jdef), MSet_kLocBeg(jmset, jdef), MSet_kLocEnd(jmset, jdef), MSet_convfact(jmset, jdef))
            Next jdef
            'FirstDef = FirstDef + MSet(jmset).SetNDef




            PrintLine(66, "BaseQuantic0! used only for constraint option 7")
            PrintLine(66, "  BaseQuantic0!")
            PrintLine(66, TAB(4), MSet_BaseQuan(jmset) / NYearPerTic)
            PrintLine(66, "MSetDevMn!, MSetDevMx!used only for constraint option 6 and 7")
            PrintLine(66, "  MSetDevMn!   MSetDevMx!")
            PrintLine(66, TAB(4), MSet_DevMn(jmset), MSet_DevMx(jmset))
            PrintLine(66, "MSetDemandTableID() used only for constraint option 2")
            PrintLine(66, "  tic, MSetDemandTableID(tic)")
            For jtic = 1 To NTic
                PrintLine(66, TAB(4), jtic, TAB(12), MSet_DemandTableID(jmset, jtic))
            Next jtic
            PrintLine(66, "Options 1, 3, and 4 use MSetTarget!() option 5 uses FlowMN() and Flowmx()")
            PrintLine(66, "  tic  MSetFlowMn!   MSetTarget!   MSetFlowMx!")
            For jtic = 1 To NTic
                PrintLine(66, TAB(4), jtic, TAB(10), MSet_FlowMn(jmset, jtic) / NYearPerTic, TAB(24), MSet_Target(jmset, jtic) / NYearPerTic, TAB(38), MSet_FlowMx(jmset, jtic) / NYearPerTic)
            Next jtic
            PrintLine(66, "ShadowPriceEstimates used for all options except option 0")
            PrintLine(66, "  tic     ShadowPriceEstimate")
            For jtic = 1 To NTic
                PrintLine(66, TAB(4), jtic, TAB(18), MSet_Pr(jmset, ksave, jtic))
            Next jtic

        Next jmset


        'Spatial Set Defs
        'FirstDef = 1
        For jSpatSet = 1 To NSpatSet
            PrintLine(66, "Spatial Set")
            PrintLine(66, jSpatSet)
            PrintLine(66, SpatSet_Label(jSpatSet))
            PrintLine(66, TAB, "SpatSetNDef", "SpatSetConstraintOption", "SpatsetPrAdjSet", "SpatSetEndPrWghtSet")
            PrintLine(66, TAB, SpatSet_nSetDefs(jSpatSet), SpatSet_ConstraintOption(jSpatSet), TAB, TAB, SpatSet_PrAdjSet(jSpatSet), TAB, TAB, SpatSet_EndPrWghtSet(jSpatSet))

            'ReDim Preserve SpatSetDef(TotalSpatSetDef)

            PrintLine(66, "    ktypeBeg ktypeEnd convfact")

            For jdef = 1 To SpatSet_nSetDefs(jSpatSet)
                PrintLine(66, TAB(7), SpatSet_kTypeBeg(jSpatSet, jdef), TAB(18), SpatSet_kTypeEnd(jSpatSet, jdef), SpatSet_convfact(jSpatSet, jdef))
            Next jdef
            'FirstDef = FirstDef + SpatSet(jSpatSet).SetNDef

            PrintLine(66, "BaseQuantic0! used only for constraint option 7")
            PrintLine(66, "  BaseQuantic0!")
            PrintLine(66, TAB(4), SpatSet_BaseQuan(jSpatSet))
            PrintLine(66, "SpatSetDevMn!, SpatSetDevMx!used only for constraint option 6 and 7")
            PrintLine(66, "  SpatSetDevMn!  SpatSetDevMx!")
            PrintLine(66, TAB(7), SpatSet_DevMn(jSpatSet), TAB(23), SpatSet_DevMx(jSpatSet))
            PrintLine(66, "SpatSetDemandTableID() used only for constraint option 2")
            PrintLine(66, "   tic, SpatSetDemandTableID(tic)")
            For jtic = 1 To NTic
                PrintLine(66, TAB(4), jtic, TAB(12), SpatSet_DemandTableID(jSpatSet, jtic))
            Next jtic
            PrintLine(66, "Options 1, 3, and 4 use SpatSetTarget!() option 5 uses FlowMN() and Flowmx()")
            PrintLine(66, "  jtic   SpatSetFlowMn!    SpatSetTarget!    SpatSetFlowMx!")
            For jtic = 1 To NTic
                PrintLine(66, TAB(4), jtic, TAB(11), SpatSet_FlowMn(jSpatSet, jtic), TAB(28), SpatSet_Target(jSpatSet, jtic), TAB(48), SpatSet_FlowMx(jSpatSet, jtic))
            Next jtic
            PrintLine(66, "ShadowPriceEstimates used for all options except option 0")
            PrintLine(66, "  kdumtic  ShadowPriceEstimate")
            For jtic = 1 To NTic
                PrintLine(66, TAB(6), jtic, TAB(15), SpatSet_Pr(jSpatSet, ksave, jtic))
            Next jtic
        Next jSpatSet

        'Condition set info.
        'FirstDef = 1
        For jCondSet = 1 To NCondSet
            PrintLine(66, "Condition Set")
            PrintLine(66, jCondSet)
            PrintLine(66, CondSet_Label(jCondSet))
            PrintLine(66, TAB, "CondSetNDef", "CondSetConstraintOption", "CondsetPrAdjSet", "CondSetEndPrWghtSet")
            PrintLine(66, TAB, CondSet_nSetDefs(jCondSet), CondSet_ConstraintOption(jCondSet), TAB, TAB, CondSet_PrAdjSet(jCondSet), TAB, TAB, CondSet_EndPrWghtSet(jCondSet))

            'ReDim Preserve CondSetDef(TotalCondSetDef)

            PrintLine(66, "    kMapLayer  kCoverSiteBeg  kCoverSiteEnd  kagebeg  kageend  kLayerColorBeg  kLayerColorEnd  convfact convprob")

            For jdef = 1 To CondSet_nSetDefs(jCondSet) 'FirstDef To FirstDef + CondSet(jCondSet).SetNDef - 1
                PrintLine(66, TAB(7), CondSet_kMapLayer(jCondSet, jdef), TAB(18), CondSet_kTypeBeg(jCondSet, jdef), TAB(34), CondSet_kTypeEnd(jCondSet, jdef), TAB(48), CondSet_kagebeg(jCondSet, jdef), TAB(57), CondSet_kageend(jCondSet, jdef), TAB(68), CondSet_kLayerColorBeg(jCondSet, jdef), TAB(84), CondSet_kLayerColorEnd(jCondSet, jdef), TAB(99), CondSet_convfact(jCondSet, jdef), TAB(108), CondSet_convprob(jCondSet, jdef))
            Next jdef
            'FirstDef = FirstDef + CondSet(jCondSet).SetNDef

            PrintLine(66, "BaseQuantic0! used only for constraint option 7")
            PrintLine(66, "  BaseQuantic0!")
            PrintLine(66, TAB(4), CondSet_BaseQuan(jCondSet))
            PrintLine(66, "CondSetDevMn!, CondSetDevMx!used only for constraint option 6 and 7")
            PrintLine(66, "  CondSetDevMn!  CondSetDevMx!")
            PrintLine(66, TAB(7), CondSet_DevMn(jCondSet), TAB(23), CondSet_DevMx(jCondSet))
            PrintLine(66, "CondSetDemandTableID() used only for constraint option 2")
            PrintLine(66, "   tic, CondSetDemandTableID(tic)")
            For jtic = 1 To NTic
                PrintLine(66, TAB(4), jtic, TAB(12), CondSet_DemandTableID(jCondSet, jtic))
            Next jtic
            PrintLine(66, "Options 1, 3, and 4 use CondSetTarget!() option 5 uses FlowMN() and Flowmx()")
            PrintLine(66, "  jtic   CondSetFlowMn!    CondSetTarget!    CondSetFlowMx!")
            For jtic = 1 To NTic
                PrintLine(66, TAB(4), jtic, TAB(11), CondSet_FlowMn(jCondSet, jtic), TAB(28), CondSet_Target(jCondSet, jtic), TAB(48), CondSet_FlowMx(jCondSet, jtic))
            Next jtic
            PrintLine(66, "ShadowPriceEstimates used for all options except option 0")
            PrintLine(66, "  kdumtic  ShadowPriceEstimate")
            For jtic = 1 To NTic
                PrintLine(66, TAB(6), jtic, TAB(15), CondSet_Pr(jCondSet, ksave, jtic))
            Next jtic

        Next jCondSet


        PrintLine(66, "DemandNEquation  DemandMxBreakPoint")
        PrintLine(66, DemandNEquation, TAB(17), DemandMxBreakPoint)


        For jEq = 1 To DemandNEquation
            PrintLine(66, "Number of break points for demand equation " & jEq)
            PrintLine(66, DemandNPoint(jEq))
            'UPGRADE_WARNING: Couldn't resolve default property of object DemandNPoint(jEq). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For jbreak = 1 To DemandNPoint(jEq)
                PrintLine(66, TAB, DemandPrice(jEq, jbreak), DemandQuan(jEq, jbreak) / NYearPerTic)
            Next jbreak
        Next jEq

        ' Base price info for Market Types
        PrintLine(66, "Nonzero Prices for Market Types")
        PrintLine(66, " MTypeBeg  MTypeEnd    MLocBeg MLocEnd   TicBeg TicEnd     price!    Nunits!")
        '234567890123456789012345678901234567890123456789012345678901234567890
        For jdef = 1 To MTypeNDef
            Print(66, TAB(4), MTypePrDef_TypeBeg(jdef))
            Print(66, TAB(14), MTypePrDef_TypeEnd(jdef))
            Print(66, TAB(26), MTypePrDef_MLocBeg(jdef))
            Print(66, TAB(34), MTypePrDef_MLocEnd(jdef))
            Print(66, TAB(44), MTypePrDef_TicBeg(jdef))
            Print(66, TAB(51), MTypePrDef_TicEnd(jdef))
            Print(66, TAB(61), MTypePrDef_price(jdef))
            PrintLine(66, TAB(71), MTypePrDef_Nunits(jdef))
        Next jdef
        PrintLine(66, "   -99       -99         -99     -99       -99   -99         0         0")


        ' Base Price info for Condition Types
        PrintLine(66, "Nonzero Prices for Condition Types")
        PrintLine(66, "kmaplayer  CoverSiteBeg CoverSiteEnd   AgeBeg AgeEnd    LayerColorBeg LayerColorEnd   TicBeg TicEnd   price!   Nunits!")
        '2345678901234567890123456789012345678901234567890123456789012345678901234567890
        For jdef = 1 To CondTypeNDef
            Print(66, TAB(4), CondTypePrDef_MapLayer(jdef))
            Print(66, TAB(14), CondTypePrDef_CoverSiteBeg(jdef))
            Print(66, TAB(22), CondTypePrDef_CoverSiteEnd(jdef))
            Print(66, TAB(31), CondTypePrDef_AgeBeg(jdef))
            Print(66, TAB(38), CondTypePrDef_AgeEnd(jdef))
            Print(66, TAB(49), CondTypePrDef_LayerColorBeg(jdef))
            Print(66, TAB(57), CondTypePrDef_LayerColorEnd(jdef))
            Print(66, TAB(66), CondTypePrDef_TicBeg(jdef))
            Print(66, TAB(73), CondTypePrDef_TicEnd(jdef))
            Print(66, TAB(81), CondTypePrDef_price(jdef))
            PrintLine(66, TAB(90), CondTypePrDef_Nunits(jdef))
        Next jdef
        PrintLine(66, "  -99         -99            -99         -99    -99        -99              -99        -99     -99     -9         1")


        ' Base Price info for Spatial Types
        PrintLine(66, "Nonzero Prices for Spatial Types")
        PrintLine(66, "SpatialTypeBeg SpatialTypeEnd  TicBeg TicEnd   price!   Nunits!")
        '2345678901234567890123456789012345678901234567890123456789012345678901234567890
        For jdef = 1 To SpatTypeNDef
            Print(66, TAB(4), SpatTypePrDef_TypeBeg(jdef))
            Print(66, TAB(14), SpatTypePrDef_TypeEnd(jdef))
            Print(66, TAB(22), SpatTypePrDef_TicBeg(jdef))
            Print(66, TAB(31), SpatTypePrDef_TicEnd(jdef))
            Print(66, TAB(38), SpatTypePrDef_price(jdef))
            PrintLine(66, TAB(49), SpatTypePrDef_Nunits(jdef))
        Next jdef
        PrintLine(66, "  -99       -99        -99     -99     -9         1")

        FileClose(66)
    End Sub

    Public Sub WriteReport()
        Dim jage As Short
        Dim jcs As Short
        Dim jArea As Short
        Dim jmloc As Short
        'Dim jmtype As Short
        Dim jset As Short
        Dim jtic As Short
        Dim File6 As String
        Dim File7 As String
        Dim Fnum7 As Integer

        'FIRST, SAVE PNV AND DEVIATIONS FILES
        FileClose(pnvfilnum)
        If System.IO.File.Exists(FnDualPlanOutBaseA & "IterationPNV" & IterationNumber & ".csv") = True Then System.IO.File.Delete(FnDualPlanOutBaseA & "IterationPNV" & IterationNumber & ".csv")
        System.IO.File.Copy(FnDualPlanOutBaseA & "IterationPNV.csv", FnDualPlanOutBaseA & "IterationPNV" & IterationNumber & ".csv")
        FileOpen(pnvfilnum, FnDualPlanOutBaseA & "IterationPNV.csv", OpenMode.Append)
        FileClose(subdivpnvfilnum)
        If System.IO.File.Exists(FnDualPlanOutBaseA & "SDIterationPNV" & IterationNumber & ".csv") = True Then System.IO.File.Delete(FnDualPlanOutBaseA & "SDIterationPNV" & IterationNumber & ".csv")
        System.IO.File.Copy(FnDualPlanOutBaseA & "SDIterationPNV.csv", FnDualPlanOutBaseA & "SDIterationPNV" & IterationNumber & ".csv")
        FileOpen(subdivpnvfilnum, FnDualPlanOutBaseA & "SDIterationPNV.csv", OpenMode.Append)
        FileClose(setdevfilnum)
        If System.IO.File.Exists(FnDualPlanOutBaseA & "DeviationsBySet" & IterationNumber & ".csv") = True Then System.IO.File.Delete(FnDualPlanOutBaseA & "DeviationsBySet" & IterationNumber & ".csv")
        System.IO.File.Copy(FnDualPlanOutBaseA & "DeviationsBySet.csv", FnDualPlanOutBaseA & "DeviationsBySet" & IterationNumber & ".csv")
        FileOpen(setdevfilnum, FnDualPlanOutBaseA & "DeviationsBySet.csv", OpenMode.Append)
        FileClose(totdevfilnum)
        If System.IO.File.Exists(FnDualPlanOutBaseA & "Deviations" & IterationNumber & ".csv") = True Then System.IO.File.Delete(FnDualPlanOutBaseA & "Deviations" & IterationNumber & ".csv")
        System.IO.File.Copy(FnDualPlanOutBaseA & "Deviations.csv", FnDualPlanOutBaseA & "Deviations" & IterationNumber & ".csv")
        FileOpen(totdevfilnum, FnDualPlanOutBaseA & "Deviations.csv", OpenMode.Append)

        ' WRITE DESIRED SUMMARY REPORT TO FILE 6
        Dim FNum6 As Integer = FreeFile()
        If WriteMSet > 0 Then

            File6 = FnDualPlanOutBaseA & "MSet" & LTrim(Str(IterationNumber)) & ".csv"
            FileOpen(6, File6, OpenMode.Output)
            Write(FNum6, "set#", "SetLabel")
            For jtic = 1 To NTic
                Write(FNum6, "SPrice" & Str(jtic))
            Next jtic
            For jtic = 1 To NTic
                Write(FNum6, "Flow" & Str(jtic))
            Next jtic
            WriteLine(FNum6)
            For jset = 1 To NMSet
                Write(FNum6, jset, MSet_Label(jset))
                For jtic = 1 To NTic
                    Write(FNum6, MSet_Pr(jset, ksave, jtic)) ' * MDisc(jtic))
                Next jtic
                For jtic = 1 To NTic
                    Write(FNum6, MSet_Flow(jset, ksave, jtic) / NYearPerTic)
                Next jtic
                WriteLine(FNum6)
            Next jset

            FileClose(FNum6)
        End If
        If WriteCondSet > 0 Then
            File6 = FnDualPlanOutBaseA & "CondSet" & LTrim(Str(IterationNumber)) & ".csv"
            FileOpen(FNum6, File6, OpenMode.Output)
            Write(FNum6, "CondSet#", "SetLabel")
            For jtic = 1 To NTic
                Write(FNum6, "SPrice" & Str(jtic))
            Next jtic
            For jtic = 0 To NTic
                Write(FNum6, "Area" & Str(jtic))
            Next jtic
            WriteLine(FNum6)

            For jset = 1 To NCondSet
                Write(FNum6, jset, CondSet_Label(jset))
                For jtic = 1 To NTic
                    Write(FNum6, CondSet_Pr(jset, ksave, jtic)) ' * CondDisc(jtic))
                Next jtic
                For jtic = 0 To NTic
                    Write(FNum6, CondSet_Flow(jset, ksave, jtic))
                Next jtic
                WriteLine(FNum6)
            Next jset
            FileClose(FNum6)
        End If

        If WriteSpatSet > 0 Then
            File6 = FnDualPlanOutBaseA & "SpatSet" & LTrim(Str(IterationNumber)) & ".csv"

            FileOpen(FNum6, File6, OpenMode.Output)
            Write(FNum6, "SpatSet#", "SetLabel")
            For jtic = 1 To NTic
                Write(FNum6, "SPrice" & Str(jtic))
            Next jtic
            For jtic = 0 To NTic
                Write(FNum6, "Area" & Str(jtic))
            Next jtic
            WriteLine(FNum6)

            For jset = 1 To NSpatSet
                Write(FNum6, jset, SpatSet_Label(jset))
                For jtic = 1 To NTic
                    Write(FNum6, SpatSet_Pr(jset, ksave, jtic)) ' * CondDisc(jtic))
                Next jtic
                For jtic = 0 To NTic
                    Write(FNum6, SpatSet_Flow(jset, ksave, jtic))
                Next jtic
                WriteLine(FNum6)
            Next jset
            FileClose(FNum6)
        End If

        If WriteMType > 0 Then
            File6 = FnDualPlanOutBaseA & "MType" & LTrim(Str(IterationNumber)) & ".csv"
            FileOpen(FNum6, File6, OpenMode.Output)
            Write(FNum6, "MLoc", "Mtype", "MTypeLabel$")

            For jtic = 1 To MxTic
                Write(FNum6, "AdjPrice" & Str(jtic))
            Next jtic
            For jtic = 1 To MxTic
                Write(FNum6, "Flow" & Str(jtic))
            Next jtic
            WriteLine(FNum6)

            For jmtype As Integer = 1 To NMType
                For jmloc = 1 To NMLocation
                    Write(FNum6, jmloc, jmtype, MTypeLabel(jmtype))
                    For jtic = 1 To MxTic
                        Write(FNum6, MtypePr(jmtype, jmloc, jtic) / MDisc(jtic))
                    Next jtic
                    For jtic = 1 To MxTic
                        Write(FNum6, MTypeFlow(jmtype, jmloc, jtic) / NYearPerTic)
                    Next jtic
                    WriteLine(FNum6)
                Next jmloc
            Next jmtype
            FileClose(FNum6)
        End If

        If WritecoversiteAge > 0 Then
            File6 = FnDualPlanOutBaseA & "CoverSite" & LTrim(Str(IterationNumber)) & ".csv"
            FileOpen(FNum6, File6, OpenMode.Output)
            Write(FNum6, "MapArea", "CoverSite", "Age")

            For jtic = 0 To NTic
                Write(FNum6, "Tic" & Str(jtic))
            Next jtic
            WriteLine(FNum6)

            For jArea = 1 To NMapArea
                For jcs = 1 To Ncoversite
                    For jage = 1 To MxAgeClass
                        Write(FNum6, MapAreaLabel(jArea), coversiteLabel(jcs), jage)
                        For jtic = 0 To NTic
                            Write(FNum6, CondTypeFlow(jcs, jage, jArea, jtic))
                        Next jtic
                        WriteLine(FNum6)
                    Next jage
                Next jcs
            Next jArea
            FileClose(FNum6)
        End If

        If WriteSpatType > 0 Then
            File6 = FnDualPlanOutBaseA & "SpatType" & LTrim(Str(IterationNumber)) & ".csv"
            FileOpen(FNum6, File6, OpenMode.Output)
            Write(FNum6, "Spattype", "SpatTypeLabel$")

            File7 = FnDualPlanOutBaseA & "Summary" & LTrim(Str(IterationNumber)) & ".csv"
            Fnum7 = FreeFile()
            FileOpen(Fnum7, File7, OpenMode.Output)

            For jtic = 1 To MxTic
                Write(FNum6, "Price" & Str(jtic))
            Next jtic
            For jtic = 1 To MxTic
                Write(FNum6, "Flow" & Str(jtic))
            Next jtic
            WriteLine(FNum6)


            PrintLine(Fnum7, "SpaceType, period, Price, EdgeProd, OneWayProd, TotalProd")

            For jstype As Integer = 1 To NSpaceType

                Write(FNum6, jstype, SpatTypeLabel(jstype))
                For jtic = 1 To MxTic
                    Write(FNum6, SpatTypePr(jstype, jtic) / CondDisc(jtic))
                Next jtic
                For jtic = 1 To MxTic
                    Write(FNum6, SpatTypeFlow(jstype, jtic))
                Next jtic
                WriteLine(FNum6)

                For jtic = 0 To MxTic
                    PrintLine(Fnum7, jstype & "," & jtic & "," & SpatTypePr(jstype, jtic) & "," & SpatTypeFlow(jstype, jtic) - OneWaySpatTypeFlow(jstype, jtic) & "," & _
                               OneWaySpatTypeFlow(jstype, jtic) & "," & SpatTypeFlow(jstype, jtic))
                Next jtic
            Next jstype
            FileClose(FNum6)
            FileClose(Fnum7)
        End If
    End Sub

    Public Sub WriteSpatialOutput(ByVal OutFile As String, ByVal kaaset As Integer, ByVal SolArray() As Long)
        'sub writes the streams of spatial types that each stand is in through MxTic
        Dim fn36 As Integer = FreeFile()
        Dim pline As String
        Dim krx As Integer
        Dim kind As Integer
        Dim kspat As Integer
        FileOpen(fn36, OutFile, OpenMode.Output)
        PrintLine(fn36, "NPolys, NTIC")
        PrintLine(fn36, AA_NPolys & "," & MxTic)
        PrintLine(fn36, "PolyID, TicSpatialSeries...")
        For jaa As Integer = 1 To AA_NPolys
            krx = SolArray(jaa)
            kind = AApres_ISpaceInd(krx)
            kspat = AApres_SpaceType(krx)
            pline = jaa
            For jtic As Integer = 0 To MxTic
                pline = pline & "," & SpatialSeries(kind).Flows(jtic) * kspat
            Next
            PrintLine(fn36, pline)
        Next

        FileClose(fn36)

    End Sub

    Public Sub WriteConditionOutput(ByVal OutFile As String, ByVal kaaset As Integer, ByVal SolArray() As Long)
        'sub writes the streams of spatial types that each stand is in through MxTic
        Dim fn36 As Integer = FreeFile()
        Dim pline As String
        Dim krx As Integer

        FileOpen(fn36, OutFile, OpenMode.Output)
        PrintLine(fn36, "NPolys, NTIC")
        PrintLine(fn36, AA_NPolys & "," & MxTic)
        PrintLine(fn36, "PolyID, TicCovertype...")
        For jaa As Integer = 1 To AA_NPolys
            krx = SolArray(jaa)
            pline = jaa
            For jtic As Integer = 0 To MxTic
                pline = pline & "," & AApres_Coversite(krx, jtic) 'SpatialSeries(kind).Flows(jtic) * kspat
            Next
            PrintLine(fn36, pline)
        Next

        FileClose(fn36)

    End Sub

    Public Sub WriteSpread(ByRef kiterspread As Short)
        Dim jCondSet As Short
        Dim jtic As Short
        Dim ksaveSpread As Short
        Dim file57 As String

        FileClose(57)
        file57 = FnDualPlanOutBaseA & "DPused.csv"
        FileOpen(57, file57, OpenMode.Append)

        ksaveSpread = (kiterspread - 1) Mod (NIterToSave) + 1

        Write(28, "Label", "Set#", "ConstrOpt", "AdjSet", "DemandTable")
        For jtic = 1 To NTic
            Write(28, "SPr" & CStr(jtic))
        Next jtic
        For jtic = 1 To NTic
            Write(28, "Targ" & CStr(jtic))
        Next jtic
        For jtic = 0 To NTic
            Write(28, "Flow" & CStr(jtic))
        Next jtic
        WriteLine(28)
        For jCondSet = 1 To NCondSet
            Write(28, CondSet_Label(jCondSet))
            Write(28, jCondSet)
            Write(28, CondSet_ConstraintOption(jCondSet))
            Write(28, CondSet_PrAdjSet(jCondSet))
            Write(28, CondSet_DemandTableID(jCondSet, 1))

            For jtic = 1 To NTic
                Write(28, CondSet_Pr(jCondSet, ksaveSpread, jtic))
            Next jtic
            For jtic = 1 To NTic
                Write(28, CondSet_Target(jCondSet, jtic))
            Next jtic
            For jtic = 0 To NTic - 1
                Write(28, CondSet_Flow(jCondSet, ksaveSpread, jtic))
            Next jtic
            WriteLine(28, CondSet_Flow(jCondSet, ksaveSpread, NTic))
        Next jCondSet

        FileClose(28)

    End Sub

    Public Sub ReadSpread()
        Dim jtic As Short
        Dim dummynum As Integer
        Dim dummy As String = ""
        Dim jCondSet As Short
        Dim dummy1 As String
        Dim ksaveSpread As Short
        MainForm.Show()
        MainForm.Label1.Text = "Name of Spreadsheet Input File to Read"
        MainForm.Label1.Refresh()
        MainForm.CommonDialog1Open.Title = "Select spreadsheet File to Read"
        MainForm.CommonDialog1Open.ShowDialog()

        FileOpen(27, MainForm.CommonDialog1Open.FileName, OpenMode.Input)

        ksaveSpread = ksave
        dummy1 = LineInput(27)
        For jCondSet = 1 To NCondSet
            Input(27, dummy)
            Input(27, dummynum)
            Input(27, CondSet_ConstraintOption(jCondSet))
            Input(27, CondSet_PrAdjSet(jCondSet))
            Input(27, CondSet_DemandTableID(jCondSet, 1))
            For jtic = 2 To NTic
                CondSet_DemandTableID(jCondSet, jtic) = CondSet_DemandTableID(jCondSet, 1)
            Next jtic

            For jtic = 1 To NTic
                Input(27, CondSet_Pr(jCondSet, ksaveSpread, jtic))
            Next jtic
            For jtic = 1 To NTic
                Input(27, CondSet_Target(jCondSet, jtic))
            Next jtic
            For jtic = 0 To NTic
                Input(27, dummynum)
            Next jtic
        Next jCondSet

        FileClose(27)
    End Sub

    Public Sub ReadLinkfile()

        Dim Blank1 As String
        Dim dummyA As String
        Dim dummy1 As String
        Dim dummy2 As String
        Dim dummy3 As String
        Dim dummy4 As String
        Dim dummy5 As String
        Dim dummy6 As String
        Dim dummy7 As String
        Dim dummy8 As String
        Dim dummy9 As String
        Dim dummy10 As String
        Dim dummy11 As String
        Dim dummy12 As String
        Dim dummy14 As String

        ' Public declarations for linkfile

        'Public FnDualPlanInBaseA As String
        'Public FnDualPlanOutBaseA As String
        'Public LastDualPlanIterNum As Integer

        'Public FnDPspaceInBaseA As String
        'Public FnDPspaceOutBaseA As String
        'Public LastDPspaceIterNum As Short

        'Public FnDPformInBaseA As String
        'Public FnDPformOutBaseA As String

        'Public FnTrimmerInBaseA As String
        'Public FnTrimmeroutBaseA As String

        'Public FnBinSpatSeriesYesNoA As String

        FileOpen(14, LinkFileA, OpenMode.Input)
        dummy1 = LineInput(14)
        Input(14, FnDualPlanInBaseA)
        Blank1 = LineInput(14)
        dummy2 = LineInput(14)
        Input(14, FnDualPlanOutBaseA)
        Blank1 = LineInput(14)
        dummy3 = LineInput(14)
        Input(14, LastDualPlanIterNum)
        Blank1 = LineInput(14)

        dummy4 = LineInput(14)
        Input(14, FnDPspaceInBaseA)
        Blank1 = LineInput(14)
        dummy5 = LineInput(14)
        Input(14, FnDPspaceOutBaseA)
        Blank1 = LineInput(14)
        dummy6 = LineInput(14)
        Input(14, LastDPspaceIterNum)
        Blank1 = LineInput(14)

        dummy7 = LineInput(14)
        Input(14, FnDPformInBaseA)
        Blank1 = LineInput(14)

        dummy8 = LineInput(14)
        Input(14, FnDPformOutBaseA)
        Blank1 = LineInput(14)

        dummy9 = LineInput(14)
        Input(14, FnTrimmerInBaseA)
        Blank1 = LineInput(14)

        dummy10 = LineInput(14)
        Input(14, FnTrimmeroutBaseA)
        Blank1 = LineInput(14)

        dummy11 = LineInput(14)
        Input(14, LastTrimmerIterNum)
        Blank1 = LineInput(14)

        'subdivisions stuff
        dummyA = LineInput(14)
        Input(14, FnSubdivisionsInputFile) 'Subdivisions Input File
        dummyA = LineInput(14) '        BaseName_Hexdata_OutfilesA()
        Input(14, BaseNameHexDataOutfilesA)
        dummyA = LineInput(14) '        BaseName_AAnaltArea_InfilesA()
        Input(14, BaseNameAAnaltAAareaInfilesA)
        dummyA = LineInput(14) '        BaseName_OutFilesA()
        Input(14, BaseNameOutFilesA)
        Blank1 = LineInput(14)

        'file where you are storing the executables
        dummyA = LineInput(14) 'Executables Directory
        Input(14, EXEDirectory)
        Blank1 = LineInput(14)

        dummy12 = LineInput(14)
        Input(14, FnBinSpatSeriesYesNoA)
        Blank1 = LineInput(14)

        dummyA = LineInput(14)
        Input(14, isFnBinSpatSeriesYesNoBin)
        Input(14, arePolyDataFilesBin)
        Blank1 = LineInput(14)

        dummy14 = LineInput(14)
        Input(14, arePresDataNPVFilesBin)
        Input(14, arePresDataSpatFilesBin)


        FileClose(14)


    End Sub

    Public Sub WriteLinkfile()

        ' Public declarations for linkfile

        'Public FnDualPlanInBaseA As String
        'Public FnDualPlanOutBaseA As String
        'Public LastDualPlanIterNum As Integer

        'Public FnDPspaceInBaseA As String
        'Public FnDPspaceOutBaseA As String
        'Public LastDPspaceIterNum As Short

        'Public FnDPformInBaseA As String
        'Public FnDPformOutBaseA As String

        'Public FnTrimmerInBaseA As String
        'Public FnTrimmeroutBaseA As String

        'Public FnBinSpatSeriesYesNoA As String

        FileOpen(17, LinkFileA, OpenMode.Output)
        PrintLine(17, "FnDualPlanInBaseA")
        PrintLine(17, FnDualPlanInBaseA)
        PrintLine(17, " ")
        PrintLine(17, "FnDualPlanOutBaseA")
        PrintLine(17, FnDualPlanOutBaseA)
        PrintLine(17, " ")
        PrintLine(17, "LastDualPlanIterNum")
        PrintLine(17, IterationNumber) 'LastDualPlanIterNum)
        PrintLine(17, " ")
        PrintLine(17, "FnDPspaceInBaseA")
        PrintLine(17, FnDPspaceInBaseA)
        PrintLine(17, " ")
        PrintLine(17, "FnDPspaceOutBaseA")
        PrintLine(17, FnDPspaceOutBaseA)
        PrintLine(17, " ")
        PrintLine(17, "LastDPspaceIterNum")
        PrintLine(17, IterationNumber - 1) 'DPSpace increments this by 1 in the code
        PrintLine(17, " ")
        PrintLine(17, "FnDPformInBaseA")
        PrintLine(17, FnDPformInBaseA)
        PrintLine(17, " ")
        PrintLine(17, "FnDPformOutBaseA")
        PrintLine(17, FnDPformOutBaseA)
        PrintLine(17, " ")
        PrintLine(17, "FnTrimmerInBaseA")
        PrintLine(17, FnTrimmerInBaseA)
        PrintLine(17, " ")
        PrintLine(17, "FnTrimmeroutBaseA")
        PrintLine(17, FnTrimmeroutBaseA)
        PrintLine(17, " ")
        PrintLine(17, "LastTrimmerIterNum")
        PrintLine(17, IterationNumber) 'LastTrimmerIterNum)
        PrintLine(17, " ")

        'subdivisions stuff
        PrintLine(17, "Subdivisions Input File")
        PrintLine(17, FnSubdivisionsInputFile)
        PrintLine(17, "BaseName_Hexdata_OutfilesA()")
        PrintLine(17, BaseNameHexDataOutfilesA)
        PrintLine(17, "BaseName_AAnaltArea_InfilesA()")
        PrintLine(17, BaseNameAAnaltAAareaInfilesA)
        PrintLine(17, "BaseName_OutFilesA()")
        PrintLine(17, BaseNameOutFilesA)
        PrintLine(17, " ")

        'executables directory
        PrintLine(17, "Executables directory")
        PrintLine(17, EXEDirectory)
        PrintLine(17, " ")

        PrintLine(17, "FnBinSpatSeriesYesNoA")
        FnBinSpatSeriesYesNoA = FnDualPlanOutBaseA & "SpatialSeries.txt"
        PrintLine(17, FnBinSpatSeriesYesNoA)
        PrintLine(17, " ")

        PrintLine(17, "isFnBinSpatSeriesYesNoBin  arePolyDataFilesBin")
        Print(17, isFnBinSpatSeriesYesNoBin, "                       ")
        PrintLine(17, arePolyDataFilesBin)
        PrintLine(17, " ")

        PrintLine(17, "arePresDataNPVFilesBin  arePresDataSpatFilesBin")
        Print(17, arePresDataNPVFilesBin, "                       ")
        PrintLine(17, arePresDataSpatFilesBin)
        FileClose(17)

    End Sub

    Public Sub LoadSubForest(ByVal SubForestFileInfo As SubForestData)
        'ALSO SETS MXAGECLASS!!
        Dim dummy As String = ""
        Dim PolyRx() As String
        Dim Rx() As String
        Dim Flow() As String
        Dim AA1() As String
        Dim k As Integer
        Dim i As Integer

        Dim NFlow As Integer = SubForestFileInfo.NFlow
        Dim NRx As Integer = SubForestFileInfo.NRx
        Dim NPolys As Integer = SubForestFileInfo.NPolys

        ReDim AAflow_tic(NFlow)
        ReDim AAflow_type(NFlow)
        ReDim AAflow_quan(NFlow)
        AAflow_Nflow = NFlow

        ReDim AApres_FirstFlow(NRx)
        ReDim AApres_PresType(NRx)
        ReDim AApres_RgTic(NRx) 'first period to tally flows of the regen Rx
        ReDim AApres_RgRotLen(NRx) 'rotation length of the regen Rx
        ReDim AApres_RgFirstFlow(NRx) 'regen first flow
        ReDim AApres_RgLastFlow(NRx) 'regen last flow

        ReDim AApres_Age(NRx, MxTic)
        ReDim AApres_Coversite(NRx, MxTic)
        ReDim AApres_SpaceType(NRx) 'the type of space the prescription creates
        ReDim AApres_ISpaceInd(NRx) 'ispace index to array of spatial flows
        ReDim AApres_QBuffInd(NRx)
        ReDim AApres_EdgeInd(NRx)
        AApres_NPres = NRx


        ReDim AA_polyid(NPolys)
        ReDim AA_area(NPolys)
        ReDim AA_Coversite(NPolys)
        ReDim AA_Ageclass(NPolys)
        ReDim AA_mloc(NPolys)
        ReDim AA_PresArray(NPolys, 0)
        ReDim AA_FirstRgTic(NPolys)
        ReDim AA_MapLayerColor(NPolys, NMapColors)
        ReDim AA_NPres(NPolys)
        AA_NPolys = NPolys

        'FLOW FILE
        FileOpen(11, SubForestFileInfo.FlowFile, OpenMode.Input)
        'AAflow.Initialize()
        dummy = LineInput(11)
        For i = 1 To AAflow_Nflow
            Flow = Split(LineInput(11))
            AAflow_type(i) = Flow(2)
            AAflow_tic(i) = Flow(3)
            AAflow_quan(i) = Flow(4)
        Next
        FileClose(11)

        'PRES FILE
        FileOpen(11, SubForestFileInfo.RxFile, OpenMode.Input)
        dummy = LineInput(11)
        For i = 1 To AApres_NPres
            Rx = Split(LineInput(11), " ")
            k = Rx(0)

            AApres_FirstFlow(k) = Rx(1)
            AApres_PresType(k) = Rx(2)
            For j As Integer = 0 To MxTic
                AApres_Coversite(k, j) = Rx(j + 5) 'Input(11, )
            Next
            For j As Integer = 0 To MxTic
                AApres_Age(k, j) = Rx(j + MxTic + 6) ' Input(11, )
            Next
        Next
        FileClose(11)

        'REGEN PRESCRIPTION FILE

        FileOpen(11, SubForestFileInfo.RgDataFile, OpenMode.Input)

        dummy = LineInput(11)
        For i = 1 To AApres_NPres

            Rx = Split(LineInput(11), " ")
            'RxIndex
            k = CType(Rx(0), Integer)
            AApres_RgFirstFlow(k) = Rx(1)
            AApres_RgLastFlow(k) = Rx(2)
            AApres_RgTic(k) = Rx(3)
            If AApres_RgTic(k) < 0 Then AApres_RgTic(k) = MxTic
            AApres_RgRotLen(k) = Rx(4)

        Next
        FileClose(11)


        'SPATIAL INDEX FILE...
        FileOpen(11, SubForestFileInfo.RxSpatialIndFile, OpenMode.Input)
        dummy = LineInput(11)
        Do Until EOF(11)
            dummy = Trim(LineInput(11))
            If dummy.Length < 1 Then Exit Do
            Rx = Split(dummy, " ")
            k = CType(Rx(0), Integer)
            'AApres(i).Initialize()
            AApres_SpaceType(k) = Rx(1)
            AApres_ISpaceInd(k) = Rx(2)
            AApres_QBuffInd(k) = Rx(3)
            AApres_EdgeInd(k) = Rx(4)
        Loop
        FileClose(11)



        'AA RX INDEX FILE
        'find the max stand RX
        FileOpen(11, SubForestFileInfo.PolyRxFile, OpenMode.Input)
        Do Until EOF(11)
            PolyRx = Split(LineInput(11), " ")
            If PolyRx.Length - 1 > MxStandRx Then MxStandRx = PolyRx.Length - 1
        Loop
        FileClose(11)
        ReDim AA_PresArray(NPolys, MxStandRx)
        ReDim DualPlanAAPoly_PassToSpace(NPolys, MxStandRx)
        ReDim DualPlanAAPoly_NPVSelf(NPolys, MxStandRx)

        'read in parameters
        FileOpen(11, SubForestFileInfo.PolyRxFile, OpenMode.Input)
        Do Until EOF(11)
            PolyRx = Split(LineInput(11), " ")
            If PolyRx.Length = 0 Then Exit Do
            i = PolyRx(0) '0 index is the poly id
            AA_polyid(i) = i 'PolyRx(0) '0 index is the poly id
            'TrimPoly(i).Initialize(PolyRx.Length - 1, NSpaceDef)
            DualPlanAAPoly_polyid(i) = i 'AA(i).polyid
            AA_NPres(i) = PolyRx.Length - 1 'first index is the poly ID

            For k = 1 To PolyRx.Length - 1 '0 index is the poly id
                AA_PresArray(i, k) = PolyRx(k)
                DualPlanAAPoly_PassToSpace(i, k) = True
            Next
        Loop
        FileClose(11)

        'What to do for polys that aren't defined?
        For j As Integer = 1 To NPolys
            If DualPlanAAPoly_polyid(j) = 0 Then
                DualPlanAAPoly_polyid(j) = j
                'TrimPoly(j).Initialize(0, NSpaceDef)
                'AA(j).Initialize(0)
            End If
        Next

        'POLY FILE
        FileOpen(11, SubForestFileInfo.PolyFile, OpenMode.Input)
        'set MxAgeClass here!
        MxAgeClass = 0
        'AA.Initialize()
        dummy = LineInput(11)
        Do Until EOF(11)
            AA1 = Split(LineInput(11))
            If AA1.Length = 0 Then Exit Do
            i = AA1(0) '0 index is the poly id
            ' DualPlanAAPoly_StandID(AA_polyid(i)) = AA_polyid(i)
            AA_area(i) = AA1(1) 'Input(11, AA(i).area)
            AA_Coversite(i) = AA1(2) 'Input(11, AA(i).Coversite)
            AA_Ageclass(i) = AA1(3) 'Input(11, AA(i).Ageclass)
            MxAgeClass = Math.Max(MxAgeClass, AA_Ageclass(i))
            AA_mloc(i) = AA1(4) 'Input(11, AA(i).mloc)
            For j As Integer = 1 To 6
                AA_MapLayerColor(i, j) = AA1(j + 5) 'Input(11, AA(i).MapLayerColor(j))
            Next
        Loop
        FileClose(11)
        MxAgeClass = MxAgeClass + MxTic

    End Sub

    Public Sub LoadSubForestInfo(ByVal filename As String, ByRef SubFor() As SubForestData)
        'FileOpen(11, filename, OpenMode.Input)
        'Dim nSubFor As Integer
        Dim dummy As String
        Dim dint As Short
        MaxSubforAAs = 0
        FileOpen(35, filename, OpenMode.Input)

        dummy = LineInput(35)

        dummy = Trim(LineInput(35))

        Do
            dint = ParseString(dummy, ",") 'dummy index of subforest
            If dint < 0 Then Exit Do
            NSubFor = NSubFor + 1
            ReDim Preserve SubFor(NSubFor)
            SubFor(NSubFor).NPolys = ParseString(dummy, ",")
            If SubFor(NSubFor).NPolys > MaxSubforAAs Then MaxSubforAAs = SubFor(NSubFor).NPolys
            SubFor(NSubFor).NRx = ParseString(dummy, ",")
            SubFor(NSubFor).NFlow = ParseString(dummy, ",")
            SubFor(NSubFor).PolyFile = Trim(LineInput(35))
            SubFor(NSubFor).RxFile = Trim(LineInput(35))
            SubFor(NSubFor).RxSpatialIndFile = Trim(LineInput(35))
            SubFor(NSubFor).FlowFile = Trim(LineInput(35))
            SubFor(NSubFor).PolyRxFile = Trim(LineInput(35))
            SubFor(NSubFor).RgDataFile = Trim(LineInput(35))
            SubFor(NSubFor).HexStandsFile = Trim(LineInput(35))
            SubFor(NSubFor).IZonesFile = Trim(LineInput(35))
            SubFor(NSubFor).LatestTrimDataFile = FnDualPlanOutBaseA & "DlPlTrimData" & dint & ".txt"
            SubFor(NSubFor).PolySchedFile = FnPolySchedFileBase & dint & "." & PolySchedFileExt 'FnDualPlanOutBaseA & "PolySched" & dint & ".txt"
            'SubFor(NSubFor).PolySchedFileLoad = FnPolySchedFileBase & dint & "." & PolySchedFileExt

            dummy = Trim(LineInput(35))
        Loop
        FileClose(35)
        AAnDataSet = NSubFor

    End Sub

    Public Sub LoadSpatialSeriesTics(ByVal SpatialSeriesTicFile As String)
        Dim fnumSS As Integer = FreeFile()
        Dim dummy As String
        Dim arr() As String
        Dim Ind As Integer
        Dim MxInd As Integer = 0
        'Dim TrackFirstHab As Integer = 0
        Dim TicVal As Integer
        'Dim NStartTics As Integer = 0
        'ReDim SpatialSeries(MxInd)
        'find the MxInd
        FileOpen(fnumSS, SpatialSeriesTicFile, OpenMode.Input)
        dummy = LineInput(fnumSS)
        Do Until EOF(fnumSS)
            dummy = LineInput(fnumSS)
            If Trim(dummy).Length <= 1 Then Exit Do
            arr = Split(dummy, " ")
            Ind = arr(0)
            If Ind > MxInd Then
                MxInd = Ind
            End If
        Loop
        FileClose(fnumSS)
        ReDim SpatialSeries(MxInd)

        fnumSS = FreeFile()
        FileOpen(fnumSS, SpatialSeriesTicFile, OpenMode.Input)
        dummy = LineInput(fnumSS)
        Do Until EOF(fnumSS)
            dummy = LineInput(fnumSS)
            If Trim(dummy).Length <= 1 Then Exit Do
            arr = Split(dummy, " ")
            Ind = arr(0)
            'If Ind > MxInd Then
            '    MxInd = Ind
            '    ReDim Preserve SpatialSeries(MxInd)
            'End If
            SpatialSeries(Ind).Initialize()
            SpatialSeries(Ind).SeriesIndex = Ind 'double counting...probably not necessary
            SpatialSeries(Ind).NNonZero = arr(1)
            SpatialSeries(Ind).First1Tic = -1
            SpatialSeries(Ind).Last1tic = -1
            SpatialSeries(Ind).PersistentSpatial = False 'default
            'NStartTics = 0
            For jper As Integer = 0 To MxTic
                TicVal = arr(jper + 2)
                SpatialSeries(Ind).Flows(jper) = TicVal
                If TicVal = 1 Then
                    SpatialSeries(Ind).Last1tic = jper
                    If SpatialSeries(Ind).First1Tic = -1 Then
                        SpatialSeries(Ind).First1Tic = jper
                    End If
                End If
            Next
            If SpatialSeries(Ind).First1Tic = -1 Then SpatialSeries(Ind).First1Tic = 0
            If SpatialSeries(Ind).Last1tic = -1 Then SpatialSeries(Ind).Last1tic = 0
            'is this REALLY a KW prescription
            If SpatialSeries(Ind).NNonZero > 0 And (SpatialSeries(Ind).Last1tic - SpatialSeries(Ind).First1Tic + 1) > _
               SpatialSeries(Ind).NNonZero Then SpatialSeries(Ind).PersistentSpatial = True

        Loop
        FileClose(fnumSS)


    End Sub

    Public Sub LoadSpaceTypeDefs(ByVal infile As String)
        Dim dummy As String
        'Dim NDefs As Integer = 0
        NSpaceDef = 0
        FileOpen(1, infile, OpenMode.Input)
        dummy = LineInput(1)
        Do Until EOF(1)
            dummy = Trim(LineInput(1))
            If Len(dummy) > 0 Then
                NSpaceDef = NSpaceDef + 1
                ReDim Preserve SpaceDefs_SpaceType(NSpaceDef), SpaceDefs_MapLayer(NSpaceDef), SpaceDefs_MapColor(NSpaceDef), _
                SpaceDefs_CoverSite(NSpaceDef), SpaceDefs_AgeBeg(NSpaceDef), SpaceDefs_AgeEnd(NSpaceDef), _
                SpaceDefs_ISpaceFactor(NSpaceDef), SpaceDefs_QBufferFactor(NSpaceDef), SpaceDefs_EdgeFactor(NSpaceDef)
                ReDim Preserve MinSpatSize(NSpaceDef)
                'MapLayer MapColor Coversite AgeBegYrs AgeEndYrs SpaceType ISpaceFactor QBufferFactor
                SpaceDefs_SpaceType(NSpaceDef) = ParseString(dummy, " ")
                SpaceDefs_MapLayer(NSpaceDef) = ParseString(dummy, " ")
                SpaceDefs_MapColor(NSpaceDef) = ParseString(dummy, " ")
                SpaceDefs_CoverSite(NSpaceDef) = ParseString(dummy, " ")
                SpaceDefs_AgeBeg(NSpaceDef) = ParseString(dummy, " ")
                SpaceDefs_AgeEnd(NSpaceDef) = ParseString(dummy, " ")

                SpaceDefs_ISpaceFactor(NSpaceDef) = ParseString(dummy, " ")
                SpaceDefs_QBufferFactor(NSpaceDef) = ParseString(dummy, " ")
                SpaceDefs_EdgeFactor(NSpaceDef) = ParseString(dummy, " ")
                '##############DEFAULT FOR DEBUG
                MinSpatSize(NSpaceDef) = 40
            End If
        Loop

        FileClose(1)
    End Sub

    Public Sub LoadIZones(ByVal infile As String)
        Dim dummy1A As String
        Dim dummy2A As String

        Dim kcount As Integer
        Dim kstand As Integer
        Dim dlen As Integer
        Dim f45 As Integer
        'Dim MxZones As Integer = 0
        MxZonesForAA = 0

        'FileNameA = BaseNameIzoneInFilesA & CStr(jsf) & ".csv"
        f45 = FreeFile()
        FileOpen(f45, infile, OpenMode.Input)
        dummy1A = LineInput(f45)
        Input(f45, NZoneSubFor)
        Input(f45, MXZoneDim)

        ReDim Zonedim(NZoneSubFor)
        ReDim ZoneAAlist(NZoneSubFor, MXZoneDim)
        ReDim ZoneAAlistAreaS(NZoneSubFor, MXZoneDim), DualPlanAAPoly_IZones(AA_NPolys, MXZoneDim)
        ReDim ZoneArea(NZoneSubFor)
        dummy2A = LineInput(f45)

        For jzone As Integer = 1 To NZoneSubFor
            Input(f45, kcount) 'let kcount also be the ID for the izone
            If jzone <> kcount Then
                MsgBox("Influence zones must be labeled and indexed sequentially from 1 to the total.")
                End
            End If
            Input(f45, Zonedim(jzone))
            For jdim As Integer = 1 To MXZoneDim
                Input(f45, ZoneAAlist(jzone, jdim))
                If Zonedim(jzone) > 1 And ZoneAAlist(jzone, jdim) > 0 Then 'DON'T RECORD THE 1-WAY INFLUENCE ZONES AS PART OF THE AA
                    kstand = ZoneAAlist(jzone, jdim)
                    DualPlanAAPoly_NIZones(kstand) = DualPlanAAPoly_NIZones(kstand) + 1
                    dlen = DualPlanAAPoly_NIZones(kstand)
                    If dlen > MxZonesForAA Then
                        MxZonesForAA = dlen
                        ReDim Preserve DualPlanAAPoly_IZones(AA_NPolys, dlen)
                    End If
                    DualPlanAAPoly_IZones(kstand, dlen) = jzone 'or kzone
                End If
            Next jdim
            For jdim As Integer = 1 To MXZoneDim
                Input(f45, ZoneAAlistAreaS(jzone, jdim))
                ZoneArea(jzone) = ZoneArea(jzone) + ZoneAAlistAreaS(jzone, jdim)
            Next jdim
            'record the area of the 1-way influence zone
            If Zonedim(jzone) = 1 Then DualPlanAAPoly_ISpaceSelf(ZoneAAlist(jzone, 1)) = ZoneAAlistAreaS(jzone, 1)

        Next jzone
        FileClose(f45)

    End Sub

    Public Sub LoadDlPlTrimData(ByVal infile As String)
        'used for aspatial dual plan iterations
        'loads the latest trimmmer info. on ratio of max npv to min npv of any Rx for the polygon
        Dim fnum As Integer = FreeFile()
        Dim dumline As String
        Dim polynum As Integer
        FileOpen(fnum, infile, OpenMode.Input)
        Do Until EOF(fnum)
            dumline = LineInput(fnum)
            polynum = ParseString(dumline, " ")
            DualPlanAAPoly_EstISpacePct(polynum) = ParseString(dumline, " ")
        Loop

        FileClose(fnum)
    End Sub

    Public Sub LoadPolySolFile(ByVal SolFil As String)
        'this can be used to read several subforests if necessary

        'all this does is read the chosen prescription for each stand

        'there is no financial information associated with reading this file

        'need either a dualplan input file for latest $$ information, or perhaps it's still store directly in
        'memory from before the DPSpace call

        'Dim linecounter As Integer = 0
        Dim Ind As Integer
        Dim dumline As String = ""

        Dim arr() As String
        If System.IO.File.Exists(SolFil) = False Then Exit Sub
        Dim sfile As Integer = FreeFile()
        FileOpen(sfile, SolFil, OpenMode.Input)
        dumline = LineInput(sfile)
        Do Until EOF(sfile)
            dumline = LineInput(sfile)
            arr = Split(dumline, ",")
            If Len(Trim(dumline)) > 2 Then
                'linecounter = linecounter + 1
                Ind = CType(arr(0), Integer)
                'If Ind = 4204 Then Stop
                'PolySolRx_PolyInd(Ind) = arr(0)
                PolySolRx_ChosenRxInd(Ind) = arr(1)
            End If

        Loop
        FileClose(sfile)
    End Sub

    Private Sub WritePolySolFile(ByVal outfile As String, ByVal kaaset As Integer, ByVal SolArray() As Long)
        'writes the polygon schedules to a file...
        Dim sfile As Integer = FreeFile()
        FileOpen(sfile, outfile, OpenMode.Output)
        PrintLine(sfile, "polyID, ChosenRx, Aspat NPV, Spatial NPV")
        For jaa As Integer = 1 To AA_NPolys
            'PrintLine(sfile, jaa & "," & PolySolRx_ChosenRxInd(jaa) & "," & PolySolRx_AspatNPV(jaa) & "," & PolySolRx_SpatNPV(jaa))
            PrintLine(sfile, jaa & "," & SolArray(jaa)) '& "," & PolySolRx_AspatNPV(jaa) & "," & PolySolRx_SpatNPV(jaa))
        Next
        FileClose(sfile)
    End Sub

    Private Sub WriteShadowPriceFile(ByVal outfile As String)


        Dim fnum As Integer = FreeFile()
        Dim dumline As String
        FileOpen(fnum, outfile, OpenMode.Output)
        'market set prices
        For jmset As Integer = 1 To NMSet
            dumline = ""
            For jtic As Integer = 1 To NTic - 1
                dumline = dumline & MSet_Pr(jmset, ksave, jtic) & ","
            Next
            dumline = dumline & MSet_Pr(jmset, ksave, NTic)
            PrintLine(fnum, dumline)
        Next jmset
        'condset prices
        For jCondset As Integer = 1 To NCondSet
            dumline = ""
            For jtic As Integer = 1 To NTic - 1
                dumline = dumline & CondSet_Pr(jCondset, ksave, jtic) & ","
            Next
            dumline = dumline & CondSet_Pr(jCondset, ksave, NTic)
            PrintLine(fnum, dumline)
        Next jCondset
        'spatial set prices
        For jSpatSet As Integer = 1 To NSpatSet
            dumline = ""
            For jtic As Integer = 1 To NTic - 1
                dumline = dumline & SpatSet_Pr(jSpatSet, ksave, jtic) & ","
            Next
            dumline = dumline & SpatSet_Pr(jSpatSet, ksave, NTic)
            PrintLine(fnum, dumline)
        Next jSpatSet

        FileClose(fnum)
        System.IO.File.Copy(outfile, outfile & "_" & IterationNumber, True)
    End Sub
    Public Sub LoadShadowPriceFile(ByVal infile As String)

        Dim fnum As Integer = FreeFile()

        FileOpen(fnum, infile, OpenMode.Input)
        For jmset As Integer = 1 To NMSet
            For jtic As Integer = 1 To NTic
                Input(fnum, MSet_Pr(jmset, 1, jtic))
            Next
        Next jmset
        'condset prices
        For jCondset As Integer = 1 To NCondSet
            For jtic As Integer = 1 To NTic
                Input(fnum, CondSet_Pr(jCondset, 1, jtic))
            Next
        Next jCondset
        'spatial set prices
        For jSpatSet As Integer = 1 To NSpatSet
            For jtic As Integer = 1 To NTic
                Input(fnum, SpatSet_Pr(jSpatSet, 1, jtic))
            Next
        Next jSpatSet
        FileClose(fnum)

    End Sub

    Private Sub SavePolySol(ByVal kaaset As Integer, ByVal SchedSaveLock As Boolean, ByVal UnschedSaveLock As Boolean, ByVal kSchedlock As Integer, ByVal kUnschedlock As Integer, ByVal CycleNum As Integer)
        For jaa As Integer = 1 To PolySolRx_ChosenRxInd.Length - 1
            PolySolRx_ChosenRxIndSave(kaaset, jaa) = PolySolRx_ChosenRxInd(jaa)
            If SchedSaveLock Then
                SchedPolySolution(jaa, kSchedlock) = PolySolRx_ChosenRxInd(jaa)
                AllSchedPolySolution(jaa, CycleNum) = PolySolRx_ChosenRxInd(jaa)
            End If

            If UnschedSaveLock Then
                UnschedPolySolution(jaa, kUnschedlock) = PolySolRx_ChosenRxInd(jaa)
                AllUnschedPolySolution(jaa, CycleNum) = PolySolRx_ChosenRxInd(jaa)
            End If

        Next
    End Sub

    Private Sub CheckSolution(ByVal ProposedSol() As Long, ByRef ExistingSol() As Long, ByVal bAfterDP As Boolean)
        'if after a DP, compares the value of each subdivision after a 1-opt iteration to see if it beats the DPSpace solution
        Dim Proposed_AspatNPV() As Double
        Dim Proposed_SpatNPV() As Double
        Dim Dummy_MarketNPV() As Double
        'Dim Dummy_DualExcessNPV() As Double
        Dim Existing_AspatNPV() As Double
        Dim Existing_SpatNPV() As Double
        'Dim Existing_MarketNPV() As Double
        ' MaxSubForSD()
        Dim PropSubForSDValue() As Double
        Dim ExistSubForSDValue() As Double
        Dim ksd As Integer
     
        If bAfterDP = True Then
            ReDim Proposed_AspatNPV(AA_NPolys)
            ReDim Proposed_SpatNPV(AA_NPolys)
            ReDim Existing_AspatNPV(AA_NPolys)
            ReDim Existing_SpatNPV(AA_NPolys)
            'ReDim Existing_MarketNPV(AA_NPolys)
            ReDim PropSubForSDValue(NSubDiv) '(NStaticSubForSD(kaaset))
            ReDim ExistSubForSDValue(NSubDiv) '(NStaticSubForSD(kaaset))

            ReDim Dummy_MarketNPV(AA_NPolys) ', Dummy_DualExcessNPV(AA_NPolys)
            AANPVTally(ProposedSol, Proposed_AspatNPV, Proposed_SpatNPV, Dummy_MarketNPV) ', Dummy_DualExcessNPV)
            ReDim Dummy_MarketNPV(AA_NPolys) ', Dummy_DualExcessNPV(AA_NPolys)
            AANPVTally(ExistingSol, Existing_AspatNPV, Existing_SpatNPV, Dummy_MarketNPV) ', Dummy_DualExcessNPV)

            For jaa As Integer = 1 To AA_NPolys

                ksd = AASubDiv(DPSpace_PolyInd(jaa)) 'SubForStaticAASubDiv(kaaset, jaa)
                PropSubForSDValue(ksd) = PropSubForSDValue(ksd) + Proposed_AspatNPV(jaa) + Proposed_SpatNPV(jaa)
                ExistSubForSDValue(ksd) = ExistSubForSDValue(ksd) + Existing_AspatNPV(jaa) + Existing_SpatNPV(jaa)
            Next jaa
            'see if you have better proposed solutions in any subdivisions
            'NChangedSD = 0
            For jsd As Integer = 1 To NSubDiv 'NStaticSubForSD(kaaset)
                'ChangeSD(jsd) = 0 'default
                'proposed value may be greater OR less since it may identifify subdivisions larger than what we first expected and actually lower the value within a particular subdivision
                If PropSubForSDValue(jsd) <> ExistSubForSDValue(jsd) Then
                    'ChangeSD(jsd) = 1
                    'NChangedSD = NChangedSD + 1
                    bSubForSubDivSinglePass(kaaset, jsd) = False
                    NumMultiWindowPasses = NumMultiWindowPasses + 1 'make sure if you are still finding better solutions with 1-opt that it is verified through the DP
                End If
            Next jsd
        End If

        'for any iteration type, catch the assigned prescription
        For jaa As Integer = 1 To AA_NPolys
            ExistingSol(jaa) = ProposedSol(jaa)
        Next

    End Sub

    Private Sub PolySolFromLastIteration(ByVal kaaset As Integer, ByRef SolArray() As Long)
        For jaa As Integer = 1 To AA_NPolys
            SolArray(jaa) = PolySolRx_ChosenRxIndSave(kaaset, jaa)
        Next
    End Sub

    Private Sub SubDivisionInformation(ByVal SubDivOutputFile As String, ByVal DPFormOutputFile As String)
        ' looks up subdivision information to see whether you have achieved exact solution anywhere, and which AAs are part of that subdivision
        Dim fnum1 As Integer = FreeFile()
        Dim dummy As String
        Dim arr() As String
        Dim SubDivNum As Integer
        Dim AANum As Integer
        Dim NAAsInDP As Integer
        Dim ksd As Integer
        Dim Maxksd As Integer = 0

        'load subforest information - which AAs are in which subdivision, and whether or not you had an exact solution
        NSubDiv = 0

        ReDim AASubDiv(AA_NPolys)

        FileOpen(fnum1, SubDivOutputFile, OpenMode.Input)
        dummy = LineInput(fnum1)
        NAAsInDP = 0
        Do Until EOF(fnum1)
            dummy = Trim(LineInput(fnum1))
            If dummy.Length < 1 Then Exit Do
            NAAsInDP = NAAsInDP + 1
            arr = Split(dummy, ",")
            SubDivNum = arr(0)
            AANum = arr(2)
            If SubDivNum > NSubDiv Then NSubDiv = SubDivNum
            AASubDiv(AANum) = SubDivNum
        Loop
        FileClose(fnum1)
        NSubForSD(kaaset) = NSubDiv
        If NSubDiv > MaxSubForSD Then
            MaxSubForSD = NSubDiv
            ReDim Preserve SubForSubDivWindowCount(NSubFor, MaxSubForSD), SubForSubDivNStands(NSubFor, MaxSubForSD), bSubForSubDivSinglePass(NSubFor, MaxSubForSD)
            ReDim Preserve SubForSDAspatNPV(NSubFor, MaxSubForSD), SubForSDSpatNPV(NSubFor, MaxSubForSD) ', SubForSDMarketNPV(NSubFor, MaxSubForSD)
        End If
        'clear arrays
        For jsd As Integer = 1 To MaxSubForSD
            SubForSubDivWindowCount(kaaset, jsd) = 0
            SubForSubDivNStands(kaaset, jsd) = 0
            SubForSDAspatNPV(kaaset, jsd) = 0
            SubForSDSpatNPV(kaaset, jsd) = 0
            bSubForSubDivSinglePass(kaaset, jsd) = True
        Next
        'Tally stands in each subdivision here - to catch single schedule stands that should be included
        For jaa As Integer = 1 To NAAsInDP
            ksd = AASubDiv(jaa)
            SubForSubDivNStands(kaaset, ksd) = SubForSubDivNStands(kaaset, ksd) + 1
        Next


        'Now, deal with how many passes per subdivisions there are - do you have an exact solution?
        'ReDim SubDivExactSol(NSubDiv)
        FileOpen(fnum1, DPFormOutputFile, OpenMode.Input)
        For j As Integer = 1 To 13
            dummy = LineInput(fnum1)
            If dummy.Length < 1 Then Exit For
        Next j
        Do Until EOF(fnum1)
            dummy = Trim(LineInput(fnum1))
            If dummy.Length < 1 Then Exit Do
            arr = Split(dummy, ",")
            ksd = arr(0)
            If ksd > Maxksd Then Maxksd = ksd
            SubForSubDivWindowCount(kaaset, ksd) = SubForSubDivWindowCount(kaaset, ksd) + 1
            NumMultiScheduleStands = NumMultiScheduleStands + CType(arr(7), Integer)
        Loop

        For jsd As Integer = 1 To Maxksd
            'multi-passes are flagged by window counts greater than 1
            If SubForSubDivWindowCount(kaaset, jsd) > 1 Then
                NumMultiWindowPasses = NumMultiWindowPasses + SubForSubDivWindowCount(kaaset, jsd)
                bSubForSubDivSinglePass(kaaset, jsd) = False
            End If

        Next
        FileClose(fnum1)

    End Sub
    Private Sub SubDivisionStaticInformation(ByVal SubDivOutputFile As String, ByVal RxLUT As String)
        ' looks up predefined subdivision and which AAs are part of that subdivision
        Dim fnum1 As Integer = FreeFile()
        Dim dummy As String
        Dim arr() As String
        Dim SubDivNum As Integer
        Dim AANum As Integer
        Dim DlPlkaa As Integer
        Dim NAAsInDP As Integer
        Dim kaa As Integer
        Dim daa As Integer
        Dim krx As Integer
        Dim ksd As Integer
        Dim DlPl_PolyInd() As Integer
        Dim NStaticSubDiv As Integer
        Dim StaticAASubDiv() As Integer

        ReDim DlPl_PolyInd(AA_NPolys)
        ReDim StaticAASubDiv(AA_NPolys)
        ReDim NStaticSubForSD(AAnDataSet)

        'figure out an AA's DP number
        Dim fnum2 As Integer = FreeFile()
        FileOpen(fnum2, RxLUT, OpenMode.Input)
        dummy = LineInput(fnum2)
        Do Until EOF(fnum2)
            dummy = Trim(LineInput(fnum1))
            If dummy.Length < 1 Then Exit Do
            arr = Split(dummy, ",")
            kaa = arr(0)
            daa = arr(1)
            krx = arr(2)
            DlPl_PolyInd(daa) = kaa 'will be dimensioned multiple times for multiple Rx sent to DPSpace
        Loop
        FileClose(fnum2)

        'load subforest information - which AAs are in which subdivision, and whether or not you had an exact solution
        NStaticSubDiv = 0

        ReDim Preserve SubForStaticAASubDiv(NSubFor, AA_NPolys)
        'zero out
        For jaa As Integer = 1 To AA_NPolys
            SubForStaticAASubDiv(kaaset, jaa) = 0
        Next

        FileOpen(fnum1, SubDivOutputFile, OpenMode.Input)
        dummy = LineInput(fnum1)
        NAAsInDP = 0
        Do Until EOF(fnum1)
            dummy = Trim(LineInput(fnum1))
            If dummy.Length < 1 Then Exit Do
            NAAsInDP = NAAsInDP + 1
            arr = Split(dummy, ",")
            SubDivNum = arr(0)
            AANum = arr(2)
            DlPlkaa = DlPl_PolyInd(AANum)
            StaticAASubDiv(AANum) = SubDivNum
            SubForStaticAASubDiv(kaaset, DlPlkaa) = SubDivNum 'AANum is the DP AA number
            If SubDivNum > NStaticSubDiv Then NStaticSubDiv = SubDivNum
        Loop

        NStaticSubForSD(kaaset) = NStaticSubDiv
        If NStaticSubDiv > MaxStaticSubForSD Then
            MaxStaticSubForSD = NStaticSubDiv
            ReDim Preserve StaticSubForSubDivNStands(NSubFor, MaxStaticSubForSD)
            ReDim Preserve StaticSubForSDAspatNPV(NSubFor, MaxStaticSubForSD), StaticAASubForSDSpatNPV(NSubFor, MaxStaticSubForSD), PolySolRx_BestSDSol(NSubFor, MaxStaticSubForSD)
        End If

        'Tally stands in each subdivision here - to catch single schedule stands that should be included
        For jaa As Integer = 1 To NAAsInDP
            ksd = StaticAASubDiv(jaa)
            StaticSubForSubDivNStands(kaaset, ksd) = StaticSubForSubDivNStands(kaaset, ksd) + 1
            PolySolRx_BestSDSol(kaaset, ksd) = -999999999999 'really low number
        Next
        FileClose(fnum1)

    End Sub
    Private Sub TranslateDPSpaceSol(ByVal DPSpaceSolFil As String, ByVal RxLUT As String, ByVal IterNum As String)
        'Sub reads the latest DPSpace solution - prescription by polygon and 
        'populates PolySolRx so that the solution file can be written for further DualPlan runs

        Dim DPSpaceSolRxInd() As Integer 'stores the selected prescription index by stand
        'Dim DPSpaceAAInd() As Integer 'stores the stand's DPSpace StandID
        Dim fnum1 As Integer = FreeFile()

        Dim dummy As String
        Dim arr() As String
        Dim StandCount As Integer = 0
        Dim kaa As Integer
        Dim daa As Integer 'dpspace id
        Dim krx As Integer

        FileOpen(fnum1, DPSpaceSolFil, OpenMode.Input)
        'use this to get index of the chosen prescription per stand
        dummy = LineInput(fnum1)
        ReDim DPSpaceSolRxInd(AA_NPolys)
        Do Until EOF(fnum1)
            dummy = Trim(LineInput(fnum1))
            If dummy.Length < 1 Then Exit Do
            StandCount = StandCount + 1
            arr = Split(dummy, ",")
            DPSpaceSolRxInd(StandCount) = arr(4)
        Loop
        FileClose(fnum1)

        'this has to be redimmed to the number of stands in the subforest!
        ReDim PolySolRx_ChosenRxInd(AA_NPolys), DPSpace_PolyInd(AA_NPolys)

        'figure out which solution was chosen per polygon
        Dim fnum2 As Integer = FreeFile()
        FileOpen(fnum2, RxLUT, OpenMode.Input)
        dummy = LineInput(fnum2)
        Do Until EOF(fnum2)
            dummy = Trim(LineInput(fnum2))
            If dummy.Length < 1 Then Exit Do
            arr = Split(dummy, ",")
            kaa = arr(0)
            daa = arr(1)
            krx = arr(2)
            DPSpace_PolyInd(kaa) = arr(1) 'will be dimensioned multiple times for multiple Rx sent to DPSpace
            'ASSUMPTION - those stands not passed to DPSpace have only 1 option, the best one
            If daa = 0 Then 'daa of 0 means not passed to DPSpace
                PolySolRx_ChosenRxInd(kaa) = arr(3) 'not passed to DPSpace
            Else
                If krx = DPSpaceSolRxInd(daa) Then
                    PolySolRx_ChosenRxInd(kaa) = arr(3)
                End If
            End If


        Loop
        FileClose(fnum2)

        ''-------------------------------------------------------
        ''debug
        'fnum2 = FreeFile()
        'FileOpen(fnum2, FnDualPlanOutBaseA & "_Iter" & IterNum & "_Solution_sf" & CStr(kaaset) & ".csv", OpenMode.Output)
        'PrintLine(fnum2, "jaa, SubDivID, ChosenRx")
        'For jaa As Integer = 1 To AA_NPolys
        '    PrintLine(fnum2, jaa & "," & SubForStaticAASubDiv(kaaset, jaa) & "," & PolySolRx_ChosenRxInd(jaa))
        'Next
        'FileClose(fnum2)
        ''-------------------------------------------------------

    End Sub

    Private Sub WriteDPSpaceFiles()
        'writes the input files read by DPSpace..

        'Write the spatial series input file
        FileOpen(11, FnDualPlanOutBaseA & "SpatialSeries.txt", OpenMode.Output)
        PrintLine(11, "NSpatSeriesYesNo NperiodSpatial")
        PrintLine(11, SpatialSeries.Length - 1 & "," & MxTic)
        PrintLine(11, "ISpace yes/no by tic....")
        For jsseries As Integer = 1 To SpatialSeries.Length - 1
            For jtic As Integer = 0 To MxTic - 1
                Print(11, SpatialSeries(jsseries).Flows(jtic) & ",")
            Next
            PrintLine(11, SpatialSeries(jsseries).Flows(MxTic))
        Next
        FileClose(11)


        Dim fnum As Integer = FreeFile()
        'write the latest spatial prices...
        '300100_Iter101_SpatPrices.csv
        FileOpen(fnum, FnDualPlanOutBaseA & "_Iter" & IterationNumber & "_SpatPrices.csv", OpenMode.Output)
        PrintLine(fnum, "Nspacetype, Nedgetype")
        PrintLine(fnum, NSpaceType & ", 0")
        PrintLine(fnum, "NperiodSpatial")
        PrintLine(fnum, MxTic)
        PrintLine(fnum, "Spatial Prices by period, type")
        PrintLine(fnum, "Dummy4")

        For jper As Integer = 1 To MxTic
            For jspace As Integer = 1 To NSpaceType
                PrintLine(fnum, jper & "," & SpatTypePr(jspace, jper) / CondDisc(jper))
            Next
        Next

        FileClose(fnum)

    End Sub

    Public Sub ReadFile(ByVal FileName As String, ByRef Lines() As String)
        'reads the DPForm file input for direction search
        Dim fnum As Integer = FreeFile()
        Dim nlines As Integer = 0
        FileOpen(fnum, FileName, OpenMode.Input)
        Do Until EOF(fnum)
            nlines = nlines + 1
            ReDim Preserve Lines(nlines)
            Lines(nlines) = LineInput(fnum)
        Loop
        FileClose(fnum)

    End Sub

    Public Sub WriteDPFormInputFile(ByVal DPFormFile As String, ByVal Direction As Integer, ByVal WindowSize As Long, ByVal Lines() As String)
        Dim fnum As Integer = FreeFile()
        Dim Nsubfor As Integer
        Dim MovingWindowDesign As Integer
        Dim NstandForLocPriority As Integer
        Dim MxAllowableState As Integer
        Dim Mxstage As Integer
        Dim MxZoneForOneStage As Integer
        Dim dummy1 As String
        Dim dummy2 As String

        'Nsubfor   MovingWindowDesign NstandForLocPriority
        '  1              1                   18 
        dummy1 = Trim(Lines(2))
        Nsubfor = CType(ParseString(dummy1, " "), Integer)
        MovingWindowDesign = CType(ParseString(dummy1, " "), Integer)
        NstandForLocPriority = CType(ParseString(dummy1, " "), Integer)

        dummy2 = Trim(Lines(4))
        MxAllowableState = CType(ParseString(dummy2, " "), Integer)
        Mxstage = CType(ParseString(dummy2, " "), Integer)
        MxZoneForOneStage = CType(ParseString(dummy2, " "), Integer)
        'If bIncreaseWindowSize = True Then
        '    MxAllowableState = MxAllowableState * WindowSizeFactor
        '    If MxAllowableState > WindowMaxSize Then
        '        MxAllowableState = WindowMaxSize
        '    End If
        'End If

        If System.IO.File.Exists(DPFormFile) = True Then System.IO.File.Delete(DPFormFile)
        FileOpen(fnum, DPFormFile, OpenMode.Output)
        PrintLine(fnum, Lines(1)) 'first line is headings
        PrintLine(fnum, "  " & Nsubfor & "              " & Direction & "              " & NstandForLocPriority)
        PrintLine(fnum, Lines(3))
        PrintLine(fnum, "  " & WindowSize & "              " & Mxstage & "              " & MxZoneForOneStage)
        For jline As Integer = 5 To Lines.Length - 1
            PrintLine(fnum, Lines(jline))
        Next

        FileClose(fnum)

    End Sub


    Public Function ParseString(ByRef Str As String, ByVal StopChar As String) As String
        Dim i As Integer
        Dim dummy1 As String
        Dim dummy As String
        dummy = ""
        For i = 0 To Str.Length - 1
            dummy1 = Str.Substring(i, 1)
            If dummy1 = StopChar Then Exit For
            dummy = dummy & dummy1
        Next
        ParseString = dummy
        If i < Str.Length - 1 Then
            Str = Trim(Str.Substring(i + 1, Str.Length - i - 1))
        Else
            Str = ""
        End If

    End Function

    Public Sub CalcCondLandVal(ByVal kmaparea As Integer, ByVal krx As Integer)
        'calculates the land value of a stream of flows adjusted to NTIC1


        Dim ktic As Integer 'translated from actual period of flow to appropriate past-planning horizon flow
        Dim kage As Integer
        Dim acoversite As Integer 'coversite adjusted through time for condition set valuation
        Dim begtic As Integer
        Dim adjtic As Integer 'number of periods that the flows need to be adjusted (positive or negative)
        Dim NTic1SEV As Double 'the SEV of the stream of flows doscounted to NTic1
        'Dim NTic1SpatSEV As Double 'the per-acre spatial SEV of the Rx - needs to be adjusted by the interior space percentage
        'identify the beginning of the rotation, and adjust flows by the number of periods to get to NTic1
        begtic = AApres_RgTic(krx) '- AApres(krx).RgRotLen
        adjtic = NTic + 1 - begtic
        NTic1SEV = 0
        'NTic1SpatSEV = 0


        If AApres_RgRotLen(krx) > 0 Then 'grow only prescriptions don't have a rotation length
            'catch condition set flows
            'DEBUG: This may be erroneous; there were some regen ages beyond MxTic that were causing this to bomb...
            For jtic As Integer = begtic To begtic + AApres_RgRotLen(krx) 'Go through the regen rotation
                ktic = jtic + adjtic
                If ktic > MxTic Then Exit For 'ktic = MxTic
                acoversite = AApres_Coversite(krx, jtic) 'actual tic associated with the flow
                kage = AApres_Age(krx, jtic) 'age from the actual age of the flow
                NTic1SEV = NTic1SEV + CondTypePr(acoversite, kage, kmaparea, ktic)

            Next jtic

            NTic1SEV = NTic1SEV * SEVFactor(AApres_RgRotLen(krx))
        End If


        CondLandValue_Calculated(kaaset, kmaparea, krx) = 1
        CondLandValue_LandVal(kaaset, kmaparea, krx) = NTic1SEV
        'LandValue(kmaparea, kcoversite).SpatLandVal = NTic1SpatSEV
    End Sub
    Public Sub CalcMktLandVal(ByVal kmloc As Integer, ByVal krx As Integer)
        'calculates the land value of a stream of flows adjusted to NTIC1
        'start adding market values

        Dim ktic As Integer 'translated from actual period of flow to appropriate past-planning horizon flow
        'Dim kmloc As Integer
        Dim kmtype As Integer
        Dim begtic As Integer
        Dim adjtic As Integer 'number of periods that the flows need to be adjusted (positive or negative)
        Dim NTic1SEV As Double 'the SEV of the stream of flows doscounted to NTic1
        'identify the beginning of the rotation, and adjust flows by the number of periods to get to NTic1
        begtic = AApres_RgTic(krx) '- AApres(krx).RgRotLen
        adjtic = NTic + 1 - begtic
        NTic1SEV = 0

        If AApres_RgRotLen(krx) > 0 Then 'grow only prescriptions don't have a rotation length

            'catch market flows
            For jflow As Integer = AApres_RgFirstFlow(krx) To AApres_RgLastFlow(krx)
                kmtype = AAflow_type(jflow)
                ktic = AAflow_tic(jflow) + adjtic
                '###debug - default to last recognized calculated value for very long rotations
                If ktic < MxTic Then NTic1SEV = NTic1SEV + AAflow_quan(jflow) * MtypePr(kmtype, kmloc, ktic)
            Next
            NTic1SEV = NTic1SEV * SEVFactor(AApres_RgRotLen(krx))

        End If


        MktLandValue_Calculated(kaaset, kmloc, krx) = 1
        MktLandValue_LandVal(kaaset, kmloc, krx) = NTic1SEV

    End Sub
    Public Sub CalcSpatLandVal(ByVal krx As Integer, ByVal kind As Integer, ByVal kspat As Integer)
        'calculates the land value of a stream of flows adjusted to NTIC1

        Dim ktic As Integer 'translated from actual period of flow to appropriate past-planning horizon flow

        Dim kage As Integer
        Dim acoversite As Integer 'coversite adjusted through time for condition set valuation
        Dim begtic As Integer
        Dim adjtic As Integer 'number of periods that the flows need to be adjusted (positive or negative)
        Dim NTic1SpatSEV As Double 'the per-acre spatial SEV of the Rx - needs to be adjusted by the interior space percentage
        'identify the beginning of the rotation, and adjust flows by the number of periods to get to NTic1
        begtic = AApres_RgTic(krx) '- AApres(krx).RgRotLen
        adjtic = NTic + 1 - begtic

        NTic1SpatSEV = 0

        If AApres_RgRotLen(krx) > 0 Then 'grow only prescriptions don't have a rotation length

            For jtic As Integer = begtic To begtic + AApres_RgRotLen(krx) 'Go through the regen rotation
                acoversite = AApres_Coversite(krx, jtic) 'actual tic associated with the flow
                kage = AApres_Age(krx, jtic) 'age from the actual age of the flow
                ktic = jtic + adjtic
                If ktic > MxTic Then Exit For '###debug - don't measure beyond the MaxTic recognized
                'NEED TO CAPTURE THE SPATIAL VALUES OF THE CENTER OF THE STAND (IF IT EXISTS)
                NTic1SpatSEV = NTic1SpatSEV + SpatTypePr(kspat, ktic) * SpatialSeries(kind).Flows(jtic)
            Next jtic

            NTic1SpatSEV = NTic1SpatSEV * SEVFactor(AApres_RgRotLen(krx))

        End If

        SpatLandValue_Calculated(kaaset, kspat, krx) = 1
        SpatLandValue_LandVal(kaaset, kspat, krx) = NTic1SpatSEV

    End Sub

    Public Sub CalcNPVSelfValue(ByRef AARxNPV(,) As Double)

        Dim AAArea As Double
        Dim AANPV As Double
        Dim EntAdjFactor As Single
        Dim InteriorISpaceAreaRatio As Single
        Dim kage As Integer
        Dim kcoversite As Integer
        Dim kind As Integer
        Dim kMapArea As Integer
        Dim kmloc As Integer
        Dim kmtype As Integer
        Dim krx As Integer
        Dim kspat As Integer
        Dim ktic As Integer
        Dim NRx As Integer

        'calculates the value of the each prescription per polygon without looking at spatial interactions with the neighbors

        'First, get the non-spatial value of each stand, each prescription, so you don't have to do this for every loop
        ReDim AARxNPV(AA_NPolys, MxStandRx)
        'First, find the value of the aspatial flows
        For jaa As Integer = 1 To AA_NPolys
            Do Until AA_polyid(jaa) > 0 'control for blank aa information
                jaa = jaa + 1
                If jaa > AA_NPolys Then Exit For
            Loop
            'If jaa = 3331 Then Stop
            'set parameters at the AA level
            ReDim MapLayerColor(NMapLayer)
            For jmap As Integer = 1 To NMapLayer
                MapLayerColor(jmap) = AA_MapLayerColor(jaa, jmap)
            Next jmap

            kmloc = AA_mloc(jaa)
            AAArea = AA_area(jaa)
            'InteriorISpaceAreaRatio = DualPlanAAPoly(jaa).ISpaceSelf / AAArea
            'kaa = jaa
            NRx = AA_NPres(jaa) 'AA(jaa).PresArray.Length - 1
            kmloc = AA_mloc(jaa)
            AAArea = AA_area(jaa)
            InteriorISpaceAreaRatio = DualPlanAAPoly_ISpaceSelf(jaa) / AAArea

            For jrx As Integer = 1 To NRx 'jpresAA = AA(KAA).firstpres To AA(KAA).lastpres
                krx = AA_PresArray(jaa, jrx) 'krx used to access line in prescription database
                kcoversite = AApres_Coversite(krx, ktic)
                kage = AApres_Age(krx, ktic)
                kind = AApres_ISpaceInd(krx)
                kspat = AApres_SpaceType(krx)


                'jpresAA = AA(jaa).PresArray(j)
                AANPV = 0
                'AANSNPV = 0
                '  first add values of all market flows

                For jflow As Integer = AApres_FirstFlow(krx) To AApres_FirstFlow(krx + 1) - 1
                    ktic = AAflow_tic(jflow)
                    kcoversite = AApres_Coversite(krx, ktic)
                    kage = AApres_Age(krx, ktic)
                    'DEBUG: Control for long rotations
                    If ktic > MxTic Then Exit For
                    kmtype = AAflow_type(jflow)
                    EntAdjFactor = 1
                    For jEnt As Integer = 1 To EntryAdjNEquation
                        For jEntDef As Integer = 1 To EntryAdjNDef(jEnt)
                            If kcoversite >= EntryAdjkCoverSiteBeg(jEnt, jEntDef) And kcoversite <= EntryAdjkCoverSiteEnd(jEnt, jEntDef) And _
                               kage >= EntryAdjkagebeg(jEnt, jEntDef) And kage <= EntryAdjkageend(jEnt, jEntDef) And _
                               AAflow_type(jflow) >= MTypeEntryCostBeg(jEnt, jEntDef) And AAflow_type(jflow) <= MTypeEntryCostEnd(jEnt, jEntDef) Then
                                'If AAflow_type(jflow) = EntryAdjMType(jEnt) Then
                                'have to figure out the area-based factor
                                'If jEnt = 2 Then Stop
                                EntAdjFactor = AdjFactor(jEnt, AAArea)
                                Exit For
                            End If
                        Next jEntDef
                    Next
                    AANPV = AANPV + AAflow_quan(jflow) * MtypePr(kmtype, kmloc, ktic) * EntAdjFactor
                Next jflow
                'AANSNPV = AANPV

                ' Then add values associated with biological conditions and spatial conditions of each map layer
                ktic = Math.Max(AApres_RgTic(krx) + AApres_RgRotLen(krx), NTic) 'the regen rx is summed through the full rotation and a land val is added after that point
                If ktic > MxTic Then ktic = MxTic 'DEBUG: Control for long rotations
                For jtic As Integer = 1 To ktic 'the regen Rx will begin tally before the end of the planning horizon, but then adjust everything to NTIC +1
                    'For jtic As Integer = 1 To NTic 'the regen Rx will begin tally before the end of the planning horizon, but then adjust everything to NTIC +1
                    'first condition type prices
                    For jmap As Integer = 1 To NMapLayer
                        kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
                        AANPV = AANPV + CondTypePr(AApres_Coversite(krx, jtic), AApres_Age(krx, jtic), kMapArea, jtic)
                        'AANSNPV = AANSNPV + CondTypePr(AApres_Coversite(krx, jtic), AApres_Age(krx, jtic), kMapArea, jtic)

                    Next jmap
                    'then spatial type prices for the 1-way influence zone
                    If bNeedSpatialIterations = True Then
                        AANPV = AANPV + SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * InteriorISpaceAreaRatio
                    End If
                    'AANSNPV = AANSNPV + SpatTypePr(kspat, jtic) * SpatialSeries(kind).Flows(jtic) * InteriorISpaceAreaRatio

                Next jtic


                '-----------------------------------------
                'Add info. for future rotations...only for Rx other than grow-only
                'DEBUG: - ADDED THE RGROTLEN+RGTIC < MXTIC FOR LONG ROTATIONS - IF YOU CAN'T GET IT IN, IT'S ESSENTIALLY A GROW-ONLY
                If AApres_RgRotLen(krx) > 0 And AApres_RgTic(krx) + AApres_RgRotLen(krx) < MxTic Then
                    ktic = AApres_RgTic(krx) + AApres_RgRotLen(krx)
                    kcoversite = AApres_Coversite(krx, ktic)

                    If MktLandValue_Calculated(kaaset, kmloc, krx) = 0 Then CalcMktLandVal(kmloc, krx)
                    AANPV = AANPV + MktLandValue_LandVal(kaaset, kmloc, krx) * CondDisc(ktic - NTic - 1)
                    'AANSNPV = AANSNPV + MktLandValue_LandVal(kaaset, kmloc, krx) * CondDisc(ktic - NTic - 1)

                    'add in value for condition type flows
                    For jmap As Integer = 1 To NMapLayer
                        kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
                        If CondLandValue_Calculated(kaaset, kMapArea, krx) = 0 Then CalcCondLandVal(kMapArea, krx)
                        'discount the appropriate land value by the number of periods beyond NTic + 1 the regen occurs
                        'add flows for condition types
                        AANPV = AANPV + CondLandValue_LandVal(kaaset, kMapArea, krx) * CondDisc(ktic - NTic - 1)
                        'AANSNPV = AANSNPV + CondLandValue_LandVal(kaaset, kMapArea, krx) * CondDisc(ktic - NTic - 1)

                    Next

                    'add in any value for spatial flows - of the interior of the stand
                    If bNeedSpatialIterations = True Then
                        If SpatLandValue_Calculated(kaaset, kspat, krx) = 0 Then CalcSpatLandVal(krx, kind, kspat)
                        AANPV = AANPV + SpatLandValue_LandVal(kaaset, kspat, krx) * CondDisc(ktic - NTic - 1) * InteriorISpaceAreaRatio
                        'AANSNPV = AANSNPV + SpatLandValue_LandVal(kaaset, kspat, krx) * CondDisc(ktic - NTic - 1) * InteriorISpaceAreaRatio
                    End If
                End If
                AARxNPV(jaa, jrx) = AANPV
            Next jrx
        Next jaa
    End Sub
    Private Function FileExtension(ByVal strFile As String) As String
        FileExtension = ""
        Dim a As String
        Dim j As Integer
        For j = 0 To strFile.Length
            a = strFile.Substring(strFile.Length - 1 - j, 1)
            If a <> "." Then
                FileExtension = a & FileExtension
            Else
                Exit For
            End If
        Next

    End Function
    Private Function DetermineBaseIFName(ByVal filename As String) As String
        Dim dummy As String
        dummy = FName(filename)
        DetermineBaseIFName = ""
        If dummy.Contains("NewIn") Then
            DetermineBaseIFName = dummy.Substring(0, dummy.Length - 5)
        End If
    End Function

    Public Function FName(ByVal FullName As String) As String
        Dim i As Integer
        Dim j As Integer
        Dim dummy As String
        dummy = FullName
        i = 0
        j = 0
        Do
            i = i + 1
            If dummy.Substring(Len(dummy) - i, 1) = "." Then j = i
            If dummy.Substring(Len(dummy) - i, 1) = "\" Or i = Len(dummy) Then Exit Do
        Loop
        If j = 0 Then
            FName = ""
        ElseIf i = Len(dummy) Then 'just file and extension
            FName = dummy.Substring((Len(dummy) - i), (i - (j)))
        Else 'full path and extension
            FName = dummy.Substring((Len(dummy) - i + 1), (i - (j + 1)))
        End If
    End Function
    Public Function DirName(ByVal FullName As String) As String
        'returns name of the directory 
        Dim i As Integer
        Dim dummy As String
        dummy = FullName
        i = 0
        Do
            i = i + 1
            If dummy.Substring(Len(dummy) - i, 1) = "\" Then Exit Do
        Loop
        If i >= Len(dummy) Then
            DirName = ""
        Else
            DirName = dummy.Substring(0, (Len(dummy) - i)) & "\"
        End If
    End Function
    Public Function AdjFactor(ByVal kEntryAdj As Integer, ByVal Area As Double) As Double
        AdjFactor = 1 '(default)
        Dim lastbreak As Integer = 0
        Dim Slope As Double
        For kbreak As Integer = 1 To EntryAdjNPoint(kEntryAdj)
            If Area > EntryAdjAcres(kEntryAdj, kbreak) Then lastbreak = kbreak
        Next
        'control for low or high values
        If lastbreak = 0 Then 'below the first value gets the factor of the first value identified
            AdjFactor = EntryAdjFactor(kEntryAdj, 1)
            Exit Function
        ElseIf lastbreak = EntryAdjNPoint(kEntryAdj) Then 'above the last value gets the factor of the last value identified
            AdjFactor = EntryAdjFactor(kEntryAdj, EntryAdjNPoint(kEntryAdj))
            Exit Function
        Else
            'for everything else, adjust the factors the appropriate level along the curve
            Slope = (EntryAdjFactor(kEntryAdj, lastbreak + 1) - EntryAdjFactor(kEntryAdj, lastbreak)) / (EntryAdjAcres(kEntryAdj, lastbreak + 1) - EntryAdjAcres(kEntryAdj, lastbreak))
            AdjFactor = EntryAdjFactor(kEntryAdj, lastbreak) + ((Area - EntryAdjAcres(kEntryAdj, lastbreak)) * Slope)
        End If

    End Function


End Module