Imports VB = Microsoft.VisualBasic
Public Class clDualSpace
    '#Region "Declarations"
    '    Dim AA() As AAdata
    '    Dim AAflow() As FlowData
    '    Dim AApres() As AApresData

    '    '----------------------------------------
    '    '***  DPspace dim statements
    '    Dim ArriveNode(10000000) As Integer
    '    Dim A(10000000) As Object

    '    Dim Model1PresData() As Model1Pres
    '    Dim Model1PolyData() As Model1Poly



    '    Const MAXLEVEL As Short = 12

    '    Dim Stand() As StandData
    '    Dim Adj() As AdjData

    '    Dim BEGNODEVAL() As Single
    '    Dim counter() As Integer
    '    'Dim CutTics()
    '    Dim Discend() As Single 'discount factor if discounted from the last year in each of the period
    '    Dim Discmid() As Single
    '    Dim SpaceMult() As Object


    '    Dim Edgefloat(,) As Single
    '    Dim Edgeprice(,,) As Single
    '    'Dim EdgeTypeAlt()
    '    Dim EdgeVal(,,) As Single
    '    Dim EndID() As Object

    '    Dim Model1PresFileList() As String
    '    Dim Model1PolyFileList() As String
    '    Dim NpresSubFor() As Integer
    '    Dim NPolySubFor() As Integer

    '    Dim SubDiv As Integer 'subdivision sequence id

    '    Dim TotalBegNodeStage As Integer 'total number of begining nodes for the current stage
    '    Dim TotalArcsPerStand As Short 'total number of arcs per nod for the current stage
    '    Dim TotalIspacesPerArc As Short 'total number of influence zones per arc for the current stage

    '    Dim TotalSpaceValRowsThisPass As Integer 'total number of space value evaluations for the current pass
    '    Dim TotalSpaceValRefThisPass As Integer 'total number of reference to the space value evaluations for the current pass
    '    Dim TotalSpaceValRowsThissubdiv As Integer
    '    Dim TotalSpaceValRefThissubdiv As Integer



    '    'Dim FirstAltStage&()
    '    Dim FirstAltStand() As Integer
    '    Dim FirstSpaceval() As Integer
    '    Dim IBufferSeriesAlt() As Object
    '    Dim IDBeg() As Object
    '    Dim IDStand() As Object
    '    Dim IDStage() As Object
    '    Dim IEdgeSeriesAlt() As Object
    '    Dim IspaceSeriesAlt() As Object
    '    'Dim ISpaceTypeForStand()
    '    Dim KBestAlt() As Object
    '    Dim EdgeSched(,) As Object
    '    Dim MaxNPVUnc() As Single
    '    Dim MaxVal() As Single
    '    Dim NaltStage() As Object
    '    Dim NaltStand() As Object
    '    Dim NaltBegLevel() As Object
    '    Dim NaltEndLevel() As Object
    '    Dim NEndLevel() As Object
    '    'UPGRADE_NOTE: NPV was upgraded to NPV_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    '    Dim NPV_Renamed() As Single
    '    Dim NPVnonSpatial() As Single
    '    Dim NStageFile() As Object
    '    Dim PriorStageIDforLevel(,) As Object
    '    Dim PassToSched() As Object

    '    Dim SortedIzone() As Integer
    '    Dim SpaceFloat() As Single
    '    Dim SpacePrice(,) As Single
    '    Dim SpaceTypeAlt(,) As Object
    '    Dim SpaceBufferSeriesSched() As Object

    '    Dim SpaceISpaceSeriesSched() As Object
    '    'Dim SpaceSched()
    '    Dim SpaceType(,) As Object
    '    Dim NodeSpaceType(,) As Object
    '    Dim Spacekaltid() As Object
    '    Dim SpaceEarlierStage() As Object
    '    Dim SpaceCoef(,) As Integer
    '    Dim SpaceNaltEarlierStage() As Object
    '    Dim SpaceCoefEarlierStage() As Integer
    '    Dim SpaceStandInZoneEarlierStage() As Object
    '    Dim SpaceReduce() As Integer
    '    Dim Spaceid() As Integer
    '    Dim Spaceval() As Single
    '    Dim TicSeriesYesNo(,) As Object
    '    Dim SumSpace(,) As Single
    '    Dim StageToDrop() As Object
    '    Dim StoreBase() As Integer
    '    Dim SumEdge(,,) As Integer
    '    Dim SubSpaceType() As Object
    '    Dim SubSpaceArea() As Single



    '    ' Arrays added for GetSubForData
    '    Dim IzoneEachStageFN() As String
    '    Dim DPFormFN() As String
    '    Dim SubIzoneDataFN() As String
    '    Dim IzoneDataFN() As String
    '    Dim IzoneListFN() As String
    '    Dim SortedIzoneFN() As String
    '    Dim StandDataFN() As String
    '    Dim StandAdjDataFN() As String


    '    Dim NPassSubFor() As Integer
    '    Dim NIzoneSubFor() As Integer
    '    Dim NSubIzoneSubFor() As Integer
    '    Dim NIzoneAllPassesSubFor() As Integer

    '    Dim IzoneEachStage() As PassIzone
    '    Dim DPFormSubFor() As DPformData
    '    Dim SubIZData() As SubIZoneData
    '    Dim IZdata() As IZoneData
    '    Dim IzoneListPerStage() As Integer



    '    Dim NStandSubFor() As Short
    '    Dim NAdjSubFor() As Integer

    '    Dim Discrate As Single
    '    Dim EdgePriceConversion As Single
    '    Dim File95 As String
    '    Public FileOut As String
    '    Dim INPUTBSAVEFN As String

    '    Dim MaxALTperStand As Short
    '    Dim MaxSpaceIndex As Integer
    '    Dim MaxStage As Short
    '    Dim MaxStandPerSpace As Short
    '    Dim MaxTotalalt As Integer

    '    Dim NBegLevel As Short
    '    Dim Nedgetype As Short
    '    'Dim Niteration
    '    Dim Nperiod As Short
    '    Dim NSched As Short
    '    Dim NStand As Short
    '    Dim NAdj As Integer
    '    Dim Nspacetype As Short
    '    Dim NTicSeriesYesNo As Short
    '    Dim NspaceThisStage As Short

    '    Dim TIMESTART As Single
    '    Dim TotalAdj As Integer
    '    Dim TotalAlt As Integer
    '    Dim TotalArea As Double
    '    Dim TotalISpaceVal As Double


    '    Dim AAPresFN() As String

    '    'Stats added for comparing runs hh  & Wei 6/19/03

    '    Dim TotalNodesThisPass As Integer
    '    Dim TotalNodesThisSubdiv As Integer
    '    Dim AreaSchedThisPass As Single
    '    Dim AreaThisPass As Single



    '    'END DP DECLARATIONS
    '    '------------------------------------------------------

    '    ' **** types for DPspace *********
    '    ' ********************************
    '    Structure IZoneData
    '        Dim FirstSubIZone As Integer
    '        Dim NStand As Short
    '    End Structure

    '    Structure SubIZoneData
    '        Dim ParentStand As Short
    '        Dim Area As Single
    '    End Structure

    '    Structure StandData
    '        Dim Area As Single
    '        Dim SpaceType As Short
    '        'BufferTic0 As Byte
    '        'SpaceTic0 As Byte
    '        'EdgeTic0 As Byte
    '        Dim FirstAdjStand As Integer
    '        Dim NAdj As Short
    '    End Structure

    '    Structure AdjData
    '        Dim StandAdj As Short
    '        Dim EdgeLength As Short
    '    End Structure
    '    'Types added for Binary read for GetSubForData

    '    Structure DPformData
    '        Dim NStand As Short
    '        Dim NStandSchedule As Short
    '        Dim StandForStage() As Short
    '        Dim StandsScheduled() As Short
    '    End Structure

    '    Structure StageIzone
    '        Dim NIzone As Short
    '        Dim FirstIzone As Integer
    '    End Structure

    '    Structure PassIzone
    '        Dim Stage() As StageIzone
    '    End Structure

    '    ' types from Trimmer code
    '    Structure Model1Pres
    '        Dim NPVAspat As Double
    '        Dim NPVallMx As Double
    '        Dim NPVallMn As Double
    '        Dim ISpaceSeries As Short 'is this the "string" series of on/off for each period?
    '        Dim BufferSeries As Short
    '        Dim EdgeSeries As Short
    '        Dim presAA As Integer
    '        Dim jreg As Short
    '        Dim npresrg As Byte
    '        Dim pres1rg As Integer
    '        Dim pres2rg As Integer
    '        Dim pres3rg As Integer
    '    End Structure


    '    Structure Model1Poly
    '        Dim polyid As Integer
    '        Dim ISpace0 As Byte
    '        Dim IBuffer0 As Byte
    '        Dim IEdge0 As Byte
    '        Dim firstpres As Integer
    '        Dim npres As Short
    '    End Structure

    '    ' **** End of types for DPspace ****
    '    '***********************************
    '#End Region

    '    Public Sub DPSpaceIterate()
    '        Dim filetoOpen As String
    '        Dim INKEY As String
    '        Dim OptimalPathVal As Double
    '        Dim TotalLossSpatial As Double
    '        Dim PolySched As String
    '        Dim jtype As Short
    '        Dim jedge2 As Short
    '        Dim jedge1 As Short
    '        Dim jper As Short
    '        Dim IterFind As Short
    '        IterTimeToStop = 0
    '        HowMany = 0
    '        If IterStop > 0 Then IterTimeToStop = 1
    '        MainForm.Txtmore.Text = Str(IterationNumber + IterHowmanyMore)

    '        '###########BELOW IS ITERATE FROM DPSPACE...SHOULD BE MODIFIED FOR INCLUSION HERE...
    '        Do Until IterTimeToStop > 0
    '            'DoEvents
    '            MainForm.Label1.Text = "Scheduling DP Iteration"
    '            IterationNumber = IterationNumber + 1
    '            'HowMany& = HowMany& + 1
    '            'MainForm.Txtmore.Text = Str$(IterationNumber& + IterHowmanyMore& - HowMany&)
    '            MainForm.TxtIter.Text = Str(IterationNumber)
    '            MainForm.TxtPolyDone.Text = "0"
    '            MainForm.Refresh()

    '            HowMany = HowMany + 1
    '            ksave = ((IterationNumber - 1) Mod NIterToSave) + 1
    '            ksaveBase = ((IterationBase - 1) Mod NIterToSave) + 1
    '            If IterMethod = "Auto" Then
    '                IterFind = 0
    '                Do Until IterFind > 0
    '                    If kiterFloat < NFloat Then
    '                        IterType(ksave) = "Float"
    '                        kiterFloat = kiterFloat + 1
    '                    ElseIf kiterSmooth < NSmooth Then
    '                        IterType(ksave) = "Smooth"
    '                        kiterSmooth = kiterSmooth + 1
    '                    ElseIf kiterShape < NShape Then
    '                        IterType(ksave) = "Shape"
    '                        kiterShape = kiterShape + 1
    '                        kiterSmooth = 0
    '                    Else
    '                        kiterFloat = 0
    '                        kiterSmooth = 0
    '                        kiterShape = 0
    '                    End If
    '                    IterFind = kiterFloat + kiterShape + kiterSmooth
    '                Loop
    '            Else
    '                IterType(ksave) = IterMethod
    '            End If


    '            '###############This stuff...is it used in DP run?

    '            'If IterType(ksave) <> "Manual" And IterType(ksave) <> "Input" Then
    '            '    Call FloatShapeSmooth()
    '            '    Call DualityCheck()
    '            '    If IterNPrAdjust = 0 Then
    '            '        kiterFloat = NFloat
    '            '        kiterSmooth = 1
    '            '        IterType(ksave) = "Smooth"
    '            '        Call FloatShapeSmooth()
    '            '    End If
    '            'End If

    '            '###########WHY?
    '            'Call SetTypePrices()

    '            ' set Spatial Shadow prices in discounted terms
    '            For jper = 1 To Nperiod
    '                For jedge1 = 0 To Nedgetype
    '                    For jedge2 = 0 To Nedgetype
    '                        Edgeprice(jedge1, jedge2, jper) = Edgeprice(jedge1, jedge2, jper) * Discend(jper)
    '                    Next jedge2
    '                Next jedge1
    '                For jtype = 1 To Nspacetype
    '                    SpacePrice(jtype, jper) = SpacePrice(jtype, jper) * Discend(jper)
    '                Next jtype
    '            Next jper


    '            'RunStatus.Text2.Text = Str$(Niteration)
    '            RunStatus.Text2.Text = "1"
    '            'For jiteration = 1 To Niteration
    '            'RunStatus.Text1.Text = Str$(jiteration)
    '            RunStatus.Text1.Text = "1"

    '            'PolySched$ = FileOut$ + "polySched" + Str$(jiteration) + ".csv"
    '            PolySched = FileOut & "polySched.csv"
    '            FileOpen(86, PolySched, OpenMode.Output)

    '            Write(86, "Subforest")
    '            Write(86, "Model_id2")
    '            Write(86, "Area")
    '            Write(86, "ISpace0")
    '            Write(86, "Buffer0")
    '            Write(86, "Edge0")
    '            Write(86, "ISpaceSeries")
    '            Write(86, "BufferSeries")
    '            Write(86, "EdgeSeries")
    '            Write(86, "kaltModel1")
    '            Write(86, "NPVSCHED!")
    '            WriteLine(86, "LOSS!")
    '            FileClose(86) 'check this!


    '            ReDim SumSpace(Nspacetype, Nperiod)
    '            ReDim SumEdge(Nedgetype, Nedgetype, Nperiod)
    '            TotalArea = 0
    '            TotalLossSpatial = 0
    '            OptimalPathVal = 0

    '            RunStatus.Text4.Text = Str(NSubFor)


    '            'Call SCHEDULE
    '            TIMESTART = VB.Timer()
    '            Call SpatSchedule(IterationNumber)
    '            'Call SpatSchedule

    '            '###########WHY DO WE NEED TO DO THESE HERE??
    '            Call SumVol() 'public module subroutine
    '            Call Deviations() 'public module subroutine
    '            IterMethod = "Auto"
    '            IterationBase = IterationNumber
    '            If HowMany >= IterHowmanyMore Then IterTimeToStop = 1
    '            If INKEY = "8" Then IterTimeToStop = 8
    '            'option yet to stop if all constraints are satisfied

    '            '##########WHY THIS PART? WHY NOT JUST TAKE THE SCHEDULE AND RUN?
    '            HowmanytoRewrite = HowmanytoRewrite + 1
    '            If HowmanytoRewrite >= NiterToRewrite Then
    '                HowmanytoRewrite = 0
    '                filetoOpen = FnBaseforOutputFiles & "NewIn." & LTrim(Str(IterationNumber))
    '                Call WriteNewInput(filetoOpen) 'public module subroutine
    '            End If

    '            ' Adjust spatial prices for another float iteration
    '            For jper = 1 To Nperiod
    '                For jedge1 = 0 To Nedgetype
    '                    For jedge2 = 0 To Nedgetype
    '                        Edgeprice(jedge1, jedge2, jper) = Edgeprice(jedge1, jedge2, jper) + Edgefloat(1, 1) * Discend(jper)
    '                    Next jedge2
    '                Next jedge1
    '            Next jper

    '            For jtype = 1 To Nspacetype
    '                For jper = 1 To Nperiod
    '                    SpacePrice(jtype, jper) = SpacePrice(jtype, jper) + SpaceFloat(jtype) * Discend(jper)
    '                Next jper
    '            Next jtype

    '        Loop
    '    End Sub

    '    Private Sub SpatSchedule(ByRef numIteration As Integer)
    '        Dim TIMESTOP As Single
    '        Dim Spaceflow As String
    '        Dim maxtspace As Integer
    '        Dim Eprice As Single
    '        Dim Edgeflow As String
    '        Dim SubArea As Double
    '        Dim fnpolyBdp As String
    '        Dim jtime As Short
    '        Dim kTicRg As Short
    '        Dim ktime As Short
    '        Dim jitem As Integer
    '        Dim krg As Short
    '        Dim RegenCost As Single
    '        Dim kRgbioNext As Short
    '        Dim kMapArea As Short
    '        Dim ktic As Short
    '        Dim kmtype As Short
    '        Dim jflow As Integer
    '        Dim kregtic As Short
    '        Dim jtic As Short
    '        Dim EntryCostFactor As Single
    '        Dim AAArea As Single
    '        Dim kmloc As Short
    '        Dim jmap As Short
    '        Dim Kaa As Integer
    '        Dim kpresBestRg As Short
    '        Dim kpres As Integer
    '        Dim LOSS As Single
    '        Dim MxNPVUnc As Single
    '        Dim NPVsched As Single
    '        Dim kpresRG As Object
    '        Dim TotalLossSpatialStart As Double
    '        Dim TotalLossSpatial As Double
    '        Dim OptimalPathValStart As Double
    '        Dim OptimalPathVal As Double
    '        Dim TotalAreaStart As Double
    '        Dim jtype As Short
    '        Dim TotalISpaceValStart As Double
    '        Dim SPaceTypeThisStand As Short
    '        Dim jj As Short
    '        Dim ThisSubSpaceArea As Single
    '        Dim kIspaceSeries As Short
    '        Dim kbufferseries As Short
    '        Dim kaltstand As Short
    '        Dim kstandID As Short
    '        Dim KSubIzone As Integer
    '        Dim nstandThisSpace As Short
    '        Dim whichIzone As Integer
    '        Dim jzone As Integer
    '        Dim jEdgeType2 As Short
    '        Dim jedgetype1 As Short
    '        Dim SubTIMESTOP As Single
    '        Dim kstore As Integer
    '        Dim idnodeonoptpath As Integer
    '        Dim MAXPASSNODE As Integer
    '        Dim PRIORSTAGEID As Short
    '        Dim KEND As Short
    '        Dim jlevelalt As Short
    '        Dim kaltadj As Integer
    '        Dim kstageadj As Short
    '        Dim kstagejlevel As Short
    '        Dim KLEVEL As Short
    '        Dim PATHVAL As Single
    '        Dim jaltpernode As Short
    '        Dim jnode As Integer
    '        Dim kstandalt As Short
    '        Dim jspace As Short
    '        Dim KENDLEVEL As Short
    '        Dim idendnode As Integer
    '        Dim JLEV12 As Short
    '        Dim JLEV11 As Short
    '        Dim JLEV10 As Short
    '        Dim JLEV9 As Short
    '        Dim JLEV8 As Short
    '        Dim JLEV7 As Short
    '        Dim JLEV6 As Short
    '        Dim JLEV5 As Short
    '        Dim JLEV4 As Short
    '        Dim JLEV3 As Short
    '        Dim JLEV2 As Short
    '        Dim JLEV1 As Short
    '        Dim idbegnode As Integer
    '        Dim NODESLINKED As Short
    '        Dim NALTPERNODE As Short
    '        Dim JEND As Integer
    '        Dim jjj As Integer
    '        Dim PUTFN As String
    '        Dim EXT As String
    '        Dim HOWBIG As Integer
    '        Dim countersum As Integer
    '        Dim jlevel As Short
    '        Dim NendLEVELthisSTAGE As Short
    '        Dim nnodeend As Integer
    '        Dim MAXnnodeend As Integer
    '        Dim JFRONT As Short
    '        Dim EdgetypeAdj As Short
    '        Dim Edgetype As Short
    '        Dim SumEdgeVal As Single
    '        Dim jadj As Short
    '        Dim kadj As Integer
    '        Dim kstandadj As Short
    '        Dim ADJPOINTER As Integer
    '        Dim kstage As Short
    '        Dim jalt As Integer
    '        Dim jstage As Short
    '        Dim jstand As Short
    '        Dim nstage As Short
    '        Dim KBASE As Integer
    '        Dim KSOLUTIONFILE As Short
    '        Dim KSTAGEINARRAY As Short
    '        Dim jper As Short
    '        Dim kalt As Integer
    '        Dim jjstand As Short
    '        Dim kpass As Short
    '        Dim StandsScheduled As Short
    '        Dim dumnum As Short
    '        Dim KSTANDTOSCHED As Short
    '        Dim j As Short
    '        Dim jpass As Short
    '        Dim SubTIMEstart As Single
    '        Dim StandInThisSpace As Object
    '        Dim jsubfor As Short
    '        Dim fnpoly96 As String
    '        Dim fnpoly As String
    '        Dim leftuse As Short
    '        Dim RunStats As String
    '        Dim iterNumber As Short
    '        Dim relabel As Integer
    '        Dim kaatotal As Integer
    '        'Public Sub SpatSchedule(iterNumber)

    '        ReDim CondTypeFlow(Ncoversite, MxAgeClass, NMapArea, MxTic)
    '        'ReDim SpatTypeFlow(Ncoversite, MxAgeClass, NMapArea, MxTic)
    '        ReDim MTypeFlow(NMType, NMLocation, MxTic)
    '        ReDim EntryCostTicThisAA(MxTic)


    '        '  Evaluate existing stands one at a time
    '        kaatotal = 0
    '        relabel = 0
    '        SubDiv = 1
    '        ' Loop through the set of input data.
    '        ' Precription/Polygon Data is stored in blocks of AA's with three files for each block

    '        RunStatus.Show()

    '        RunStats = FileOut & "RunStats" & Str(iterNumber) & ".csv"
    '        FileOpen(77, RunStats, OpenMode.Output)
    '        'Write #77, "SubDiv", "TotalNodesThisPass&", "TotalNodesThisSubdiv&", "TotalSpaceValRowsThisPass&", "TotalSpaceValRowsThissubdiv&", _
    '        '"TotalSpaceValRefThisPass&", "TotalSpaceValRefThissubdiv&", "MAXnnodeend&", "AreaSchedThisPass!", "AreaThisPass!", _
    '        '"100 * AreaSchedThisPass! / AreaThisPass!", "Timer1", "Timer2", "Timer3", "Timer4"
    '        WriteLine(77, "SubDiv", "TotalNodesThisPass&", "TotalNodesThisSubdiv&", "TotalSpaceValRowsThisPass&", "TotalSpaceValRowsThissubdiv&", "TotalSpaceValRefThisPass&", "TotalSpaceValRefThissubdiv&", "MAXnnodeend&", "AreaSchedThisPass!", "AreaThisPass!", "100 * AreaSchedThisPass! / AreaThisPass!", "Timer")



    '        leftuse = InStr(AAFN(1), ".")


    '        'If leftuse > 0 Then
    '        '  fnpoly$ = Left(AAFN$(1), leftuse - 2) & ".fdp"
    '        'Else
    '        '  fnpoly$ = AAFN$(1) & ".fdp"
    '        'End If

    '        '07/14, wei
    '        fnpoly = DpSpaceLinkBaseNameOut & LastDPfileID & ".fdp"

    '        FileOpen(66, fnpoly, OpenMode.Output)
    '        WriteLine(66, "nsubfor")
    '        WriteLine(66, NSubFor)

    '        fnpoly96 = DpSpaceLinkBaseNameOut & LastDPfileID & "subfor.csv"
    '        FileOpen(96, fnpoly96, OpenMode.Output)

    '        Write(96, "subforest")
    '        Write(96, "Time start")
    '        Write(96, "Time stop")
    '        Write(96, "Total Area")
    '        Write(96, "Max Aspatial NPV")
    '        Write(96, "Sched Aspatial NPV")
    '        Write(96, "Sched Aspatial Loss")
    '        Write(96, "Sched Spatial Value")
    '        Write(96, "Sched Total Value")
    '        Write(96, "  ")
    '        Write(96, "Max Aspatial NPV/acre")
    '        Write(96, "Sched Aspatial NPV/acre")
    '        Write(96, "Sched Aspatial Loss/acre")
    '        Write(96, "Sched Spatial Value/acre")
    '        WriteLine(96, "Sched Total Value/acre")



    '        'UPGRADE_WARNING: Lower bound of array NPV_Renamed was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '        'UPGRADE_NOTE: NPV was upgraded to NPV_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    '        ReDim NPV_Renamed(TotalAlt)
    '        For jsubfor = 1 To NSubFor 'ebh: go through each DualPlan subforest...
    '            SubTIMEstart = VB.Timer()

    '            RunStatus.Text3.Text = Str(jsubfor)
    '            RunStatus.Refresh()

    '            '###############EBH: SHOULD ONLY NEED ECON INFO. WHY LOAD ALL THIS STUFF???
    '            '######### I THINK THIS SHOULD BE MODIFIED SO THAT YOU ONLY READ AN AA FILE AND A RX FILE WITH ECON VALUES...
    '            ReDim AAflow(AASetNFlow(jsubfor))
    '            'UPGRADE_WARNING: Array AApres may need to have individual elements initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="B97B714D-9338-48AC-B03F-345B617E2B02"'
    '            ReDim AApres(AASetNPres(jsubfor))
    '            'UPGRADE_WARNING: Array AA may need to have individual elements initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="B97B714D-9338-48AC-B03F-345B617E2B02"'
    '            ReDim AA(AASetNAA(jsubfor))

    '            FileOpen(4, AAFlowFN(jsubfor), OpenMode.Binary, OpenAccess.Read)
    '            'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '            FileGet(4, AAflow)
    '            FileClose(4)

    '            FileOpen(4, AAPresFN(jsubfor), OpenMode.Binary, OpenAccess.Read)
    '            'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '            FileGet(4, AApres)
    '            FileClose(4)

    '            FileOpen(4, AAFN(jsubfor), OpenMode.Binary, OpenAccess.Read)
    '            'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '            FileGet(4, AA)
    '            FileClose(4)


    '            Call GetSubForData(jsubfor)

    '            ' PRINT #9, "Pass", "Stage", "# of End Nodes"

    '            ReDim PassToSched(NStand)

    '            'Open FileAlternativeSubFor$(jsubfor) For Input As #1
    '            Call ReadAlternativeFile(jsubfor)
    '            'Close #1

    '            ' with data prep we can now set pass to schedule for all stands up front
    '            'ebh: this appears to define which window STANDS are scheduled
    '            For jpass = 1 To NPassSubFor(jsubfor)
    '                For j = 1 To DPFormSubFor(jpass).NStandSchedule
    '                    KSTANDTOSCHED = DPFormSubFor(jpass).StandsScheduled(j)
    '                    PassToSched(KSTANDTOSCHED) = jpass
    '                Next j
    '            Next jpass

    '            '' This was used to flag that pass to schedule is some later pass.  It was reset when it
    '            '' appeared in the sequential output file from dpform
    '            'For jstand = 1 To NStand
    '            'PassToSched(jstand) = 9999
    '            'Next jstand
    '            ' old files used without preprocessing
    '            'Open FileDPformulationSubFor$(jSubFor) For Input As #55
    '            'Open FileSpaceEachStageSubFor$(jSubFor) For Input As #35

    '            ReDim counter(MAXLEVEL) 'MAXLEVEL IS A CONSTANT WITH A VALUE OF 12
    '            ReDim EdgeVal(MaxALTperStand, MAXLEVEL, MaxALTperStand)
    '            ReDim EndID(MAXLEVEL)
    '            ReDim PriorStageIDforLevel(MaxStage, MAXLEVEL)
    '            'ReDim FirstAltStage&(maxstage)
    '            'ReDim FirstAltStand&(NStand)
    '            ReDim IDBeg(MAXLEVEL + 1)
    '            ReDim IDStand(MaxStage)
    '            ReDim IDStage(NStand)
    '            ReDim KBestAlt(NStand)
    '            ReDim EdgeSched(NStand, Nperiod) 'ebh: edge might be the narrow strip that is scheduled and subsequently removed
    '            ReDim NaltStage(MaxStage)
    '            ReDim NaltBegLevel(MAXLEVEL)
    '            ReDim NaltEndLevel(MAXLEVEL)
    '            ReDim NEndLevel(MaxStage)
    '            ReDim NStageFile(MaxStage)
    '            ReDim StageToDrop(MaxStage)
    '            ReDim StoreBase(MaxStage)
    '            'ReDim SpaceSched(nstand, 0 To Nperiod)
    '            ReDim SpaceBufferSeriesSched(NStand)
    '            ReDim SpaceISpaceSeriesSched(NStand)
    '            'ReDim ISpaceTypeForStand(NStand)
    '            ' SpaceType no longer needs 0 to Nperiod?
    '            ReDim NodeSpaceType(MaxStandPerSpace, Nperiod)
    '            ReDim SpaceType(MaxStandPerSpace, Nperiod)

    '            ' THE ARRAYS DIMENSIONED BELOW WILL BE REDIMENSIONED THROUGHOUT
    '            dumnum = 11
    '            'ebh - 1/30/08 - are these the two big vectors that are used to describe starting condition set of nodes and ending condition set of nodes?
    '            'ebh: no...I think the two big vectors are A and ArriveNode
    '            ReDim BEGNODEVAL(dumnum)
    '            ReDim MaxVal(dumnum)


    '            'UPGRADE_WARNING: Couldn't resolve default property of object IDBeg(MAXLEVEL + 1). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '            IDBeg(MAXLEVEL + 1) = 1
    '            StandsScheduled = 0
    '            kpass = 0 'ebh: passes may represent scheduling each window
    '            RunStatus.Text5.Text = "0"
    '            RunStatus.Text6.Text = Str(NStand - DPFormSubFor(1).NStandSchedule)
    '            RunStatus.Text7.Text = Str(kpass)
    '            RunStatus.Refresh()
    '            Do Until StandsScheduled = NStand 'ebh: from notes 1/29/08 - know the end of a subdivision when you get to a point where all schedules are accepted
    '                'ebh: do ends about 2770
    '                'Timer1 = Timer
    '                kpass = kpass + 1 'ebh: passes may represent each window
    '                If kpass = 1 Then 'ebh: about 2255 and 2770
    '                    For jjstand = 1 To NStand
    '                        If PassToSched(jjstand) = 1 Then 'ebh: which window the stand gets scheduled in 
    '                            kalt = FirstAltStand(jjstand) 'ebh: kalt refers to alternative (Rx) in a list for the forest - not a new list for each stand
    '                            For jper = 1 To Nperiod
    '                                EdgeSched(jjstand, jper) = TicSeriesYesNo(IEdgeSeriesAlt(kalt), jper)
    '                            Next jper
    '                            KBestAlt(jjstand) = 1 'ebh: why assume first ALT is the best?
    '                            'hmh 9/23/2004
    '                            'SpaceBufferSeriesSched(jjstand) = IspaceSeriesAlt(kalt&)
    '                            'SpaceISpaceSeriesSched(jjstand) = IBufferSeriesAlt(kalt&)
    '                            SpaceISpaceSeriesSched(jjstand) = IspaceSeriesAlt(kalt)
    '                            SpaceBufferSeriesSched(jjstand) = IBufferSeriesAlt(kalt)
    '                        End If
    '                    Next jjstand
    '                    NSched = DPFormSubFor(1).NStandSchedule
    '                    StandsScheduled = StandsScheduled + NSched
    '                Else

    '                    'For JPASS = 1 To NPASS
    '                    'KIDINROW = 1
    '                    ' KIDROW = 0
    '                    'Print #9,
    '                    'Print #9, "States per stage -- Pass #", jpass
    '                    'Print #9, "         1     2     3     4     5     6     7     8     9     0"
    '                    'Print #9, KIDROW;
    '                    KSTAGEINARRAY = 0
    '                    KSOLUTIONFILE = 0
    '                    KBASE = 0

    '                    'Line Input #55, dummy$
    '                    'Input #55, nstage
    '                    nstage = DPFormSubFor(kpass).NStand 'ebh: number of stages (stands) in this window/subforest determined by the DPForm program

    '                    'ebh: could probably use a redim statement to speed this
    '                    For jstand = 1 To NStand
    '                        IDStage(jstand) = 0
    '                    Next jstand

    '                    'Line Input #55, dummy$
    '                    For jstage = 1 To nstage 'ebh: nstage: # stands in window
    '                        StageToDrop(jstage) = jstage 'ebh: set stage to drop a stand initially at the stage the stand comes in. This is adjusted below (with the adjacency considerations)
    '                        'Input #55, IDStand(jstage)
    '                        IDStand(jstage) = DPFormSubFor(kpass).StandForStage(jstage) 'ebh: stages are defined by the stand to bring in at that stage
    '                        IDStage(IDStand(jstage)) = jstage 'ebh: ID of the stage indexed by the stand
    '                    Next jstage
    '                    'Line Input #55, dummy$
    '                    'Line Input #55, dummy$
    '                    'Line Input #55, dummy$
    '                    'Line Input #55, dummy$
    '                    'Line Input #55, dummy$
    '                    'Input #55, NSCHED

    '                    NSched = DPFormSubFor(kpass).NStandSchedule
    '                    'ebh: appears to count all scheduled before they actually are...shouldn't matter in the do test
    '                    StandsScheduled = StandsScheduled + NSched

    '                    'Line Input #55, dummy$
    '                    'For j = 1 To NSched
    '                    'Input #55, KSTANDTOSCHED
    '                    'KSTANDTOSCHED = DPFormSubFor(kpass).StandsScheduled(j)
    '                    'PassToSched(KSTANDTOSCHED) = kpass
    '                    'Next j

    '                    'Later, each pass NPV!(alt) will be adjusted to account for impacts
    '                    'of schedules accepted for prior passes.  Need to reset NPV!() to nonspatial
    '                    'value of alternative at the start of each pass
    '                    For jalt = 1 To TotalAlt 'ebh: I think TotalAlt is the cumulative number of Rx across all stands - in this subforest
    '                        NPV_Renamed(jalt) = NPVnonSpatial(jalt)
    '                    Next jalt

    '                    '  IDENTIFY Stage To Drop each Stand For the Current Pass

    '                    If kpass = 1 Then
    '                        For jstand = 1 To NStand
    '                            kstage = IDStage(jstand)
    '                            If kstage > 0 Then 'ebh: stand is included in this window
    '                                NaltStage(kstage) = NaltStand(jstand) 'ebh: number of Rx with this stand (?)
    '                            End If
    '                        Next jstand

    '                    Else
    '                        For jstand = 1 To NStand
    '                            kstage = IDStage(jstand)
    '                            If kstage > 0 Then
    '                                NaltStage(kstage) = NaltStand(jstand)

    '                                ADJPOINTER = Stand(jstand).FirstAdjStand 'ebh: is this a stand number/index/unique identifier?
    '                                For j = 1 To Stand(jstand).NAdj 'ebh: is this max of 12?
    '                                    kstandadj = Adj(ADJPOINTER).StandAdj
    '                                    ADJPOINTER = ADJPOINTER + 1
    '                                    'ebh: initially, stagetodrop is set to the stand's stage...
    '                                    'ebh: does this then reset it to drop only after all adjacent stands are scheduled?
    '                                    If StageToDrop(kstage) < IDStage(kstandadj) Then StageToDrop(kstage) = IDStage(kstandadj)
    '                                Next j
    '                            End If
    '                        Next jstand

    '                        ' adjust Value of alternatives to recognize
    '                        ' potential edge impacts from stands already scheduled

    '                        For jstand = 1 To NStand
    '                            If PassToSched(jstand) < kpass Then 'ebh: stand has already been scheduled
    '                                kadj = Stand(jstand).FirstAdjStand 'ebh: kadj seems to be used in the same way as ADJPOINTER above
    '                                For jadj = 1 To Stand(jstand).NAdj
    '                                    kstandadj = Adj(kadj).StandAdj
    '                                    kstage = IDStage(kstandadj) 'ebh: stage ID of the adjacent stand
    '                                    If kstage > 0 Then 'ebh: what would it mean if it = 0?
    '                                        'kalt& = FirstAltStage&(kstage)
    '                                        kalt = FirstAltStand(IDStand(kstage))
    '                                        For jalt = 1 To NaltStage(kstage)
    '                                            SumEdgeVal = 0
    '                                            For jper = 1 To Nperiod
    '                                                'Edgetype = EdgeTypeAlt(jper, kalt&)
    '                                                Edgetype = TicSeriesYesNo(IEdgeSeriesAlt(kalt), jper)
    '                                                EdgetypeAdj = EdgeSched(jstand, jper)
    '                                                SumEdgeVal = SumEdgeVal + Edgeprice(Edgetype, EdgetypeAdj, jper)
    '                                            Next jper
    '                                            'ebh: looks like NPV gets readjusted to reflect benefit or penalty (sumedgeval) from adjacent stands
    '                                            NPV_Renamed(kalt) = NPV_Renamed(kalt) + SumEdgeVal * Adj(kadj).EdgeLength
    '                                            kalt = kalt + 1
    '                                        Next jalt
    '                                    End If
    '                                    kadj = kadj + 1
    '                                Next jadj
    '                            End If
    '                        Next jstand
    '                    End If



    '                    ' Identify state variables needed at the end of each stage.
    '                    'ebh: so, the stands that need to be carried into the next stage?
    '                    For jstage = 1 To nstage
    '                        NEndLevel(jstage) = 0
    '                    Next jstage
    '                    For jstage = 1 To nstage
    '                        For JFRONT = jstage To StageToDrop(jstage) - 1 'ebh: from the stage the stand comes in to 1 before it drops out
    '                            NEndLevel(JFRONT) = NEndLevel(JFRONT) + 1
    '                            PriorStageIDforLevel(JFRONT, NEndLevel(JFRONT)) = jstage
    '                        Next JFRONT
    '                    Next jstage

    '                    'ebh: this appears to be reseting/default setting
    '                    BEGNODEVAL(1) = 0
    '                    NBegLevel = 0
    '                    EndID(1) = MAXLEVEL + 1
    '                    For j = 1 To MAXLEVEL
    '                        NaltBegLevel(j) = 1
    '                    Next j
    '                    MAXnnodeend = 1


    '                    'Timer2 = Timer


    '                    ' Start the solution process stage by stage
    '                    For jstage = 1 To nstage
    '                        'Print "STAGE ="; JSTAGE;
    '                        'Print "  # OF ENDING NODES = "; nnodeend&
    '                        If nnodeend > MAXnnodeend Then MAXnnodeend = nnodeend

    '                        'hmh & wei 6/19/2003
    '                        TotalNodesThisPass = TotalNodesThisPass + nnodeend

    '                        kstage = jstage
    '                        NendLEVELthisSTAGE = NEndLevel(jstage)

    '                        '      DETERMINE # OF POSSIBLE NODES AT THE END OF THIS STAGE(nnodeend&) AND
    '                        '      SET THE DIMENSIONS FOR EACH STATE DISCRIPTER

    '                        nnodeend = 1

    '                        For jlevel = 1 To NendLEVELthisSTAGE
    '                            NaltEndLevel(jlevel) = NaltStage(PriorStageIDforLevel(kstage, jlevel))
    '                            nnodeend = nnodeend * NaltEndLevel(jlevel) 'ebh: here's where the number of nodes is calculated and where the Rx per poly can explode
    '                        Next jlevel
    '                        'ebh: this just accounts for stages with fewer than the MAXLEVEL number of stands
    '                        For jlevel = NendLEVELthisSTAGE + 1 To MAXLEVEL
    '                            NaltEndLevel(jlevel) = 1
    '                        Next jlevel

    '                        counter(1) = nnodeend / NaltEndLevel(1) 'ebh: don't know what counter is
    '                        For jlevel = 2 To NendLEVELthisSTAGE
    '                            counter(jlevel) = counter(jlevel - 1) / NaltEndLevel(jlevel)
    '                        Next jlevel
    '                        countersum = 0
    '                        For jlevel = 1 To NendLEVELthisSTAGE
    '                            countersum = countersum + counter(jlevel)
    '                        Next jlevel

    '                        ' PRINT #9, KPASS, KSTAGE, nnodeend&
    '                        'If KIDINROW > 10 Then
    '                        'KIDINROW = KIDINROW - 10
    '                        ' KIDROW = KIDROW + 10
    '                        'Print #9,
    '                        'Print #9, KIDROW;
    '                        'End If
    '                        'Print #9, nnodeend&;
    '                        'KIDINROW = KIDINROW + 1
    '                        ReDim MaxVal(nnodeend)

    '                        ' STORE SOLUTIONS SO FAR IF A() AND Arrivenode&() WILL BE TOO LARGE
    '                        'ebh: so far we've only counted the size of the array, not done any calculations for the stage

    '                        HOWBIG = KBASE
    '                        HOWBIG = HOWBIG + nnodeend
    '                        If HOWBIG > 10000000 Then
    '                            'If HOWBIG& > 200000 Then
    '                            If KBASE = 0 Then
    '                                'Print "EXCEEDED AVAILABLE STORAGE AT STAGE; "; kstage
    '                                'Print "NUMBER OF NODES = "; nnodeend&
    '                                Stop
    '                            Else
    '                                KSOLUTIONFILE = KSOLUTIONFILE + 1
    '                                NStageFile(KSOLUTIONFILE) = KSTAGEINARRAY
    '                                EXT = LTrim(Str(KSOLUTIONFILE))
    '                                PUTFN = INPUTBSAVEFN & "A" & "." & EXT
    '                                FileOpen(22, PUTFN, OpenMode.Binary)
    '                                'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '                                FilePut(22, A) 'ebh: assume at least one node has been defined and dump those values
    '                                FileClose(22)
    '                                PUTFN = INPUTBSAVEFN & "B" & "." & EXT
    '                                FileOpen(22, PUTFN, OpenMode.Binary)
    '                                'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '                                FilePut(22, ArriveNode) 'ebh: assume at least one node has been defined and dump those values
    '                                FileClose(22)
    '                                KBASE = 0
    '                                KSTAGEINARRAY = 0
    '                                For jjj = 1 To 10000000
    '                                    'For jjj& = 1 To 200000
    '                                    A(jjj) = 0
    '                                    ArriveNode(jjj) = 0
    '                                Next jjj


    '                            End If
    '                        End If
    '                        KSTAGEINARRAY = KSTAGEINARRAY + 1
    '                        StoreBase(kstage) = KBASE
    '                        HOWBIG = KBASE

    '                        '  INITIALIZE STATS FOR ENDOFSTAGE NODES
    '                        For JEND = 1 To nnodeend
    '                            MaxVal(JEND) = -9999999
    '                        Next JEND
    '                        kstage = jstage 'ebh: is this statement redundant? - also appears just inside "for jstage = ..." (above)
    '                        If StageToDrop(kstage) = kstage Then 'ebh: there are no adjacencies?
    '                            NALTPERNODE = NaltStage(kstage)
    '                            NODESLINKED = 1
    '                        Else
    '                            NALTPERNODE = 1
    '                            NODESLINKED = NaltStage(kstage)
    '                        End If

    '                        'What does this return? Where/how is it used?
    '                        Call ISpaceForStage(kpass, kstage) 'ebh: seems to get values of interior space for all possible alts of this stage while accounting for previously scheduled stands

    '                        'If kpass > 1 Then Call ISpaceForStage(kpass, kstage)

    '                        'ebh: loop through all alternatives (Rx) of each level (stand in the stage) to define the nodes 
    '                        'ebh: could this be taken care of by a recursive subroutine?
    '                        idbegnode = 0
    '                        For JLEV1 = 1 To NaltBegLevel(1)
    '                            IDBeg(1) = JLEV1
    '                            For JLEV2 = 1 To NaltBegLevel(2)
    '                                IDBeg(2) = JLEV2
    '                                For JLEV3 = 1 To NaltBegLevel(3)
    '                                    IDBeg(3) = JLEV3
    '                                    For JLEV4 = 1 To NaltBegLevel(4)
    '                                        IDBeg(4) = JLEV4
    '                                        For JLEV5 = 1 To NaltBegLevel(5)
    '                                            IDBeg(5) = JLEV5
    '                                            For JLEV6 = 1 To NaltBegLevel(6)
    '                                                IDBeg(6) = JLEV6
    '                                                For JLEV7 = 1 To NaltBegLevel(7)
    '                                                    IDBeg(7) = JLEV7
    '                                                    For JLEV8 = 1 To NaltBegLevel(8)
    '                                                        IDBeg(8) = JLEV8
    '                                                        For JLEV9 = 1 To NaltBegLevel(9)
    '                                                            IDBeg(9) = JLEV9
    '                                                            For JLEV10 = 1 To NaltBegLevel(10)
    '                                                                IDBeg(10) = JLEV10
    '                                                                For JLEV11 = 1 To NaltBegLevel(11)
    '                                                                    IDBeg(11) = JLEV11
    '                                                                    For JLEV12 = 1 To NaltBegLevel(12) 'ebh: some higher levels may not have any stands, and default at value 1 to 1
    '                                                                        IDBeg(12) = JLEV12

    '                                                                        idbegnode = idbegnode + 1

    '                                                                        'If BEGNODEVAL!(idbegnode&) > -0.001 Then
    '                                                                        If BEGNODEVAL(idbegnode) > -88888 Then
    '                                                                            ' FIND ID OF NEXT ENDNODE

    '                                                                            idendnode = 1
    '                                                                            For jlevel = 1 To NendLEVELthisSTAGE
    '                                                                                KENDLEVEL = IDBeg(EndID(jlevel))
    '                                                                                '  idendnode& = idendnode& + (KENDLEVEL - 1) *Counter (JLEVEL)
    '                                                                                '  countersum& USED TO SIMPLIFY ABOVE EXPRESSION
    '                                                                                idendnode = idendnode + KENDLEVEL * counter(jlevel)
    '                                                                            Next jlevel
    '                                                                            idendnode = idendnode - countersum


    '                                                                            ' identify interior space factors for this beginning node
    '                                                                            For jspace = 1 To NspaceThisStage
    '                                                                                ' changes 11/25
    '                                                                                'Spaceid&(jspace) = -SpaceReduce&(jspace)
    '                                                                                'For jlevel = 1 To NBegLevel
    '                                                                                ' Spaceid&(jspace) = Spaceid&(jspace) + SpaceCoef&(jspace, IDBeg(jlevel))
    '                                                                                'Next jlevel

    '                                                                                'change 9/08/2003 hmh
    '                                                                                'Spaceid&(jspace) = 1 - SpaceReduce&(jspace)
    '                                                                                Spaceid(jspace) = -SpaceReduce(jspace)
    '                                                                                For jlevel = 1 To NBegLevel
    '                                                                                    Spaceid(jspace) = Spaceid(jspace) + SpaceCoef(jspace, jlevel) * IDBeg(jlevel)
    '                                                                                Next jlevel

    '                                                                                Spaceid(jspace) = Spaceid(jspace) + FirstSpaceval(jspace) - 1
    '                                                                            Next jspace


    '                                                                            ' EVALUATE ALL OPTIONS OUT OF CURRENT BEGINNING NODE
    '                                                                            'kalt& = FirstAltStage&(kstage)
    '                                                                            kalt = FirstAltStand(IDStand(kstage))

    '                                                                            kstandalt = 1
    '                                                                            For jnode = 1 To NODESLINKED
    '                                                                                For jaltpernode = 1 To NALTPERNODE
    '                                                                                    PATHVAL = NPV_Renamed(kalt) + BEGNODEVAL(idbegnode)
    '                                                                                    ' 11/24/97 kstandalt = kstandalt + 1

    '                                                                                    ' adjust value for edge considerations
    '                                                                                    For jlevel = 1 To NBegLevel
    '                                                                                        PATHVAL = PATHVAL + EdgeVal(kstandalt, jlevel, IDBeg(jlevel))
    '                                                                                    Next jlevel

    '                                                                                    ' adjust value for interior space consideration
    '                                                                                    For jspace = 1 To NspaceThisStage
    '                                                                                        PATHVAL = PATHVAL + Spaceval(Spaceid(jspace) + kstandalt)
    '                                                                                    Next jspace
    '                                                                                    'ebh: appears that node with the largest value is flagged
    '                                                                                    'and the altrenative is saved (A) along with the node index (ArriveNode - (?))
    '                                                                                    If PATHVAL > MaxVal(idendnode) Then
    '                                                                                        MaxVal(idendnode) = PATHVAL
    '                                                                                        'A(KBASE& + idendnode&) = kalt& - FirstAltStage&(kstage) + 1
    '                                                                                        A(KBASE + idendnode) = kalt - FirstAltStand(IDStand(kstage)) + 1
    '                                                                                        ArriveNode(KBASE + idendnode) = idbegnode
    '                                                                                    End If
    '                                                                                    kalt = kalt + 1
    '                                                                                    kstandalt = kstandalt + 1
    '                                                                                Next jaltpernode
    '                                                                                idendnode = idendnode + 1
    '                                                                            Next jnode
    '                                                                        End If

    '                                                                    Next JLEV12
    '                                                                Next JLEV11
    '                                                            Next JLEV10
    '                                                        Next JLEV9
    '                                                    Next JLEV8
    '                                                Next JLEV7
    '                                            Next JLEV6
    '                                        Next JLEV5
    '                                    Next JLEV4
    '                                Next JLEV3
    '                            Next JLEV2
    '                        Next JLEV1


    '                        'hmh 08/25/03
    '                        TotalBegNodeStage = idbegnode
    '                        TotalSpaceValRefThisPass = TotalSpaceValRefThisPass + TotalBegNodeStage * TotalArcsPerStand * TotalIspacesPerArc

    '                        ' IF NOT LAST STAGE, INITIALIZE VALUES FOR THE NEXT STAGE
    '                        If kstage < nstage Then
    '                            KBASE = KBASE + nnodeend
    '                            NBegLevel = NendLEVELthisSTAGE
    '                            ReDim BEGNODEVAL(nnodeend)
    '                            For jnode = 1 To nnodeend
    '                                BEGNODEVAL(jnode) = MaxVal(jnode)
    '                            Next jnode

    '                            ' SET Edge Values FOR NEXT STAGE for interactions
    '                            ' with earlier stages in pass.
    '                            ' Note: edge values for interacrtions with stands of
    '                            ' stands scheduled in earlier passes will be
    '                            ' recognized when setting NPV values for each alternative
    '                            ' at the start of the pass

    '                            For jlevel = 1 To NendLEVELthisSTAGE
    '                                KLEVEL = jlevel
    '                                kstagejlevel = PriorStageIDforLevel(kstage, KLEVEL)

    '                                '  NEED TO DETERMINE if stand STAGEJLEVEL is
    '                                '  an adjacent stand (stand KSTAGEADJ)
    '                                kadj = Stand(IDStand(kstage + 1)).FirstAdjStand
    '                                For jadj = 1 To Stand(IDStand(kstage + 1)).NAdj
    '                                    kstageadj = IDStage(Adj(kadj).StandAdj)
    '                                    If kstageadj = kstagejlevel Then
    '                                        Exit For
    '                                    End If
    '                                    kadj = kadj + 1
    '                                Next jadj

    '                                If kstageadj = kstagejlevel Then
    '                                    'kalt& = FirstAltStage&(kstage + 1)
    '                                    kalt = FirstAltStand(IDStand(kstage + 1))
    '                                    For jalt = 1 To NaltStage(kstage + 1)
    '                                        'kaltadj& = FirstAltStage&(kstageadj)
    '                                        kaltadj = FirstAltStand(IDStand(kstageadj))
    '                                        For jlevelalt = 1 To NaltStage(kstagejlevel)
    '                                            SumEdgeVal = 0
    '                                            For jper = 1 To Nperiod
    '                                                'Edgetype = EdgeTypeAlt(jper, kalt&)
    '                                                Edgetype = TicSeriesYesNo(IEdgeSeriesAlt(kalt), jper)
    '                                                'EdgetypeAdj = EdgeTypeAlt(jper, kaltadj&)
    '                                                EdgetypeAdj = TicSeriesYesNo(IEdgeSeriesAlt(kaltadj), jper)
    '                                                SumEdgeVal = SumEdgeVal + Edgeprice(Edgetype, EdgetypeAdj, jper)
    '                                            Next jper
    '                                            EdgeVal(jalt, KLEVEL, jlevelalt) = SumEdgeVal * Adj(kadj).EdgeLength
    '                                            kaltadj = kaltadj + 1
    '                                        Next jlevelalt
    '                                        kalt = kalt + 1
    '                                    Next jalt
    '                                Else
    '                                    For jalt = 1 To NaltStage(kstage + 1)
    '                                        For jlevelalt = 1 To NaltStage(kstagejlevel)
    '                                            EdgeVal(jalt, KLEVEL, jlevelalt) = 0.0!
    '                                        Next jlevelalt
    '                                    Next jalt
    '                                End If
    '                            Next jlevel

    '                            ' SET ENDID() FOR NEXT STAGE
    '                            KEND = 1
    '                            For jlevel = 1 To NendLEVELthisSTAGE
    '                                PRIORSTAGEID = PriorStageIDforLevel(kstage, jlevel)
    '                                If PRIORSTAGEID = PriorStageIDforLevel(kstage + 1, KEND) Then
    '                                    EndID(KEND) = jlevel
    '                                    KEND = KEND + 1
    '                                End If
    '                            Next jlevel
    '                            If KEND <= NEndLevel(kstage + 1) Then
    '                                EndID(KEND) = MAXLEVEL + 1
    '                                KEND = KEND + 1
    '                            End If
    '                            For j = 1 To MAXLEVEL
    '                                NaltBegLevel(j) = NaltEndLevel(j)
    '                            Next j
    '                        End If
    '                    Next jstage

    '                    'Timer3 = Timer

    '                    If MAXnnodeend > MAXPASSNODE Then MAXPASSNODE = MAXnnodeend

    '                    '  TRACE OPTIMAL SOLUTION FOR THIS PASS AND SET SCHEDULE FOR STANDS
    '                    '  SCHEDULED IN THIS PASS
    '                    idnodeonoptpath = 1
    '                    kstage = nstage

    '                    '06/19/03
    '                    AreaSchedThisPass = 0
    '                    AreaThisPass = 0

    '                    'ebh: this appears to be the backwards recursion where the best path is found...
    '                    Do Until kstage < 1
    '                        If KSTAGEINARRAY = 0 Then
    '                            ' READ NEXT PUT FILE
    '                            EXT = LTrim(Str(KSOLUTIONFILE))
    '                            PUTFN = INPUTBSAVEFN & "A" & "." & EXT
    '                            FileOpen(22, PUTFN, OpenMode.Binary)
    '                            'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '                            FileGet(22, A)
    '                            FileClose(22)
    '                            PUTFN = INPUTBSAVEFN & "B" & "." & EXT
    '                            FileOpen(22, PUTFN, OpenMode.Binary)
    '                            'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '                            FileGet(22, ArriveNode)
    '                            FileClose(22)
    '                            'UPGRADE_WARNING: Couldn't resolve default property of object NStageFile(KSOLUTIONFILE). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                            KSTAGEINARRAY = NStageFile(KSOLUTIONFILE)
    '                            KSOLUTIONFILE = KSOLUTIONFILE - 1
    '                        End If

    '                        kstore = StoreBase(kstage) + idnodeonoptpath

    '                        '06/19/03
    '                        AreaThisPass = AreaThisPass + Stand(IDStand(kstage)).Area

    '                        If PassToSched(IDStand(kstage)) = kpass Then

    '                            '6/19/2003
    '                            AreaSchedThisPass = AreaSchedThisPass + Stand(IDStand(kstage)).Area

    '                            KBestAlt(IDStand(kstage)) = A(kstore) 'ebh: the best alt for the stand scheduled at this stage...
    '                            'kalt& = FirstAltStage&(kstage) + A(kstore&) - 1
    '                            kalt = FirstAltStand(IDStand(kstage)) + A(kstore) - 1

    '                            'hmh 9/23/2004
    '                            'SpaceBufferSeriesSched(IDStand(kstage)) = IspaceSeriesAlt(kalt&)
    '                            'SpaceISpaceSeriesSched(IDStand(kstage)) = IBufferSeriesAlt(kalt&)

    '                            SpaceBufferSeriesSched(IDStand(kstage)) = IBufferSeriesAlt(kalt)
    '                            SpaceISpaceSeriesSched(IDStand(kstage)) = IspaceSeriesAlt(kalt)



    '                            'SpaceSched(IDStand(kstage), 0) = SpaceTypeAlt(0, kalt&)
    '                            For jper = 1 To Nperiod
    '                                'SpaceSched(IDStand(kstage), jper) = SpaceTypeAlt(jper, kalt&)
    '                                'EdgeSched(IDStand(kstage), jper) = EdgeTypeAlt(jper, kalt&)
    '                                EdgeSched(IDStand(kstage), jper) = TicSeriesYesNo(IEdgeSeriesAlt(kalt), jper)
    '                            Next jper
    '                        End If
    '                        idnodeonoptpath = ArriveNode(kstore)
    '                        KSTAGEINARRAY = KSTAGEINARRAY - 1
    '                        kstage = kstage - 1
    '                    Loop
    '                    RunStatus.Text5.Text = Str(StandsScheduled - DPFormSubFor(1).NStandSchedule)
    '                    RunStatus.Text7.Text = Str(kpass)
    '                    RunStatus.Refresh()


    '                    '6/19/2003
    '                    'TotalNodesAllPasses& = TotalNodesAllPasses& + TotalNodesThisPass&

    '                    TotalNodesThisSubdiv = TotalNodesThisSubdiv + TotalNodesThisPass
    '                    TotalSpaceValRowsThissubdiv = TotalSpaceValRowsThissubdiv + TotalSpaceValRowsThisPass
    '                    TotalSpaceValRefThissubdiv = TotalSpaceValRefThissubdiv + TotalSpaceValRefThisPass

    '                    'Write #77, SubDiv, TotalNodesThisPass&, TotalNodesThisSubdiv&, TotalSpaceValRowsThisPass&, TotalSpaceValRowsThissubdiv&, _
    '                    'TotalSpaceValRefThisPass&, TotalSpaceValRefThissubdiv&, MAXnnodeend&, AreaSchedThisPass!, AreaThisPass!, _
    '                    '100 * AreaSchedThisPass! / AreaThisPass!, Timer1, Timer2, Timer3, Timer
    '                    WriteLine(77, SubDiv, TotalNodesThisPass, TotalNodesThisSubdiv, TotalSpaceValRowsThisPass, TotalSpaceValRowsThissubdiv, TotalSpaceValRefThisPass, TotalSpaceValRefThissubdiv, MAXnnodeend, AreaSchedThisPass, AreaThisPass, 100 * AreaSchedThisPass / AreaThisPass, VB.Timer())



    '                    TotalNodesThisPass = 0
    '                    TotalSpaceValRowsThisPass = 0 'total number of space value evaluations for the current pass
    '                    TotalSpaceValRefThisPass = 0 'total number of reference to the space value evaluations for the current pass

    '                    If DPFormSubFor(kpass).NStandSchedule = DPFormSubFor(kpass).NStand Then
    '                        TotalNodesThisSubdiv = 0
    '                        TotalSpaceValRowsThissubdiv = 0
    '                        TotalSpaceValRefThissubdiv = 0
    '                        SubDiv = SubDiv + 1
    '                    End If

    '                End If 'ebh: seems to be the end of the "is this the first pass" if statement
    '            Loop 'ebh: seems to end the loop through all moving windows 

    '            SubTIMESTOP = VB.Timer()



    '            'Finished all passes for this SubFor
    '            'Close #35
    '            FileClose(55)
    '            ' Add Edge Production Levels for this SubFor
    '            For jstand = 1 To NStand
    '                kadj = Stand(jstand).FirstAdjStand
    '                For jadj = 1 To Stand(jstand).NAdj
    '                    kstandadj = Adj(kadj).StandAdj
    '                    If jstand < kstandadj Then
    '                        For jper = 1 To Nperiod
    '                            jedgetype1 = EdgeSched(jstand, jper)
    '                            jEdgeType2 = EdgeSched(kstandadj, jper)
    '                            SumEdge(jedgetype1, jEdgeType2, jper) = SumEdge(jedgetype1, jEdgeType2, jper) + Adj(kadj).EdgeLength
    '                        Next jper
    '                        'jedgetype1 = Stand(jstand).EdgeTic0
    '                        'jedgetype2 = Stand(kstandadj).EdgeTic0
    '                        jedgetype1 = Model1PolyData(jstand).IEdge0
    '                        jEdgeType2 = Model1PolyData(kstandadj).IEdge0
    '                        SumEdge(jedgetype1, jEdgeType2, 0) = SumEdge(jedgetype1, jEdgeType2, 0) + Adj(kadj).EdgeLength


    '                    End If
    '                    kadj = kadj + 1
    '                Next jadj
    '            Next jstand


    '            ' Sum Interior Space Production
    '            ReDim StandInThisSpace(MaxStandPerSpace)
    '            ReDim SubSpaceArea(MaxStandPerSpace)

    '            For jzone = 1 To NIzoneSubFor(jsubfor)
    '                whichIzone = jzone
    '                nstandThisSpace = IZdata(whichIzone).NStand
    '                KSubIzone = IZdata(whichIzone).FirstSubIZone
    '                For jstand = 1 To nstandThisSpace
    '                    StandInThisSpace(jstand) = SubIZData(KSubIzone).ParentStand
    '                    SubSpaceArea(jstand) = SubIZData(KSubIzone).Area
    '                    KSubIzone = KSubIzone + 1
    '                Next jstand

    '                ' adjust for concept of quality buffer -- subspaces of influence zones
    '                ' sum over all subspaces for this influence zone
    '                For jstand = 1 To nstandThisSpace
    '                    ReDim SubSpaceType(Nperiod)
    '                    kstandID = StandInThisSpace(jstand)
    '                    kaltstand = KBestAlt(kstandID)
    '                    kalt = FirstAltStand(kstandID) + kaltstand - 1
    '                    kbufferseries = SpaceBufferSeriesSched(kstandID)
    '                    kIspaceSeries = SpaceISpaceSeriesSched(kstandID)
    '                    ThisSubSpaceArea = SubSpaceArea(jstand)
    '                    ' first set condition of this subspace to interior space condition of the parent stand
    '                    For jper = 1 To Nperiod
    '                        SubSpaceType(jper) = TicSeriesYesNo(kIspaceSeries, jper)
    '                    Next jper
    '                    'SubSpaceType(0) = Stand(jstand).SpaceTic0
    '                    SubSpaceType(0) = Model1PolyData(kstandID).ISpace0
    '                    ' then adjust for buffer quality of all neighboring stands
    '                    For jj = 1 To jstand - 1
    '                        kbufferseries = SpaceBufferSeriesSched(StandInThisSpace(jj))
    '                        For jper = 1 To Nperiod
    '                            SubSpaceType(jper) = SubSpaceType(jper) * TicSeriesYesNo(kbufferseries, jper)
    '                        Next jper
    '                        'SubSpaceType(0) = SubSpaceType(0) * Stand(StandInThisSpace(jj)).BufferTic0
    '                        SubSpaceType(0) = SubSpaceType(0) * Model1PolyData(StandInThisSpace(jj)).IBuffer0
    '                    Next jj

    '                    For jj = jstand + 1 To nstandThisSpace
    '                        kbufferseries = SpaceBufferSeriesSched(StandInThisSpace(jj))
    '                        For jper = 1 To Nperiod
    '                            SubSpaceType(jper) = SubSpaceType(jper) * TicSeriesYesNo(kbufferseries, jper)
    '                        Next jper
    '                        'SubSpaceType(0) = SubSpaceType(0) * Stand(StandInThisSpace(jj)).BufferTic0
    '                        SubSpaceType(0) = SubSpaceType(0) * Model1PolyData(StandInThisSpace(jj)).IBuffer0
    '                    Next jj

    '                    ' then add area to forest-wide totals
    '                    SPaceTypeThisStand = Stand(kstandID).SpaceType
    '                    For jper = 0 To Nperiod
    '                        SumSpace(SPaceTypeThisStand, jper) = SumSpace(SPaceTypeThisStand, jper) + SubSpaceType(jper) * ThisSubSpaceArea
    '                    Next jper
    '                Next jstand

    '            Next jzone

    '            ' hmh 9/15/2003 added "start" values to write NPV values by subforest
    '            TotalISpaceValStart = TotalISpaceVal

    '            TotalISpaceVal = 0
    '            For jtype = 1 To Nspacetype
    '                For jper = 1 To Nperiod
    '                    TotalISpaceVal = TotalISpaceVal + SpacePrice(jtype, jper) * SumSpace(jtype, jper)
    '                Next jper
    '            Next jtype

    '            'Close #36

    '            ' calculate loss in nonspatial max NPV from spatial solution
    '            ' And add nonspatial flows to forest-wide totals


    '            ' hmh 9/15/2003 added "start" values to write NPV values by subforest
    '            TotalAreaStart = TotalArea
    '            OptimalPathValStart = OptimalPathVal
    '            TotalLossSpatialStart = TotalLossSpatial

    '            For jstand = 1 To NStand
    '                TotalArea = TotalArea + Stand(jstand).Area
    '                kalt = FirstAltStand(jstand) + KBestAlt(jstand) - 1
    '                NPVsched = NPVnonSpatial(kalt)
    '                MxNPVUnc = MaxNPVUnc(jstand)
    '                LOSS = MxNPVUnc - NPVsched
    '                TotalLossSpatial = TotalLossSpatial + LOSS
    '                OptimalPathVal = OptimalPathVal + NPVsched

    '                Write(86, jsubfor)
    '                Write(86, System.Math.Abs(AA(jstand).polyid))
    '                Write(86, AA(jstand).area)
    '                Write(86, Model1PolyData(jstand).ISpace0)
    '                Write(86, Model1PolyData(jstand).IBuffer0)
    '                Write(86, Model1PolyData(jstand).IEdge0)
    '                Write(86, Model1PresData(kalt).ISpaceSeries)
    '                Write(86, Model1PresData(kalt).BufferSeries)
    '                Write(86, Model1PresData(kalt).EdgeSeries)
    '                Write(86, kalt)
    '                Write(86, NPVsched)
    '                WriteLine(86, LOSS)


    '                ' 5/23/03 new data type for dpspace output to dualplan

    '                'Type Model1PresOut
    '                ' polyid As Long
    '                'NPVAspat As Double
    '                'NPVallMx As Double
    '                'NPVallMn As Double
    '                'ISpaceSeries As Integer
    '                'BufferSeries As Integer
    '                'EdgeSeries As Integer
    '                'presAA As Long
    '                'jreg As Integer
    '                'npresrg As Byte
    '                'pres1rg As Long
    '                'pres2rg As Long
    '                'pres3rg As Long
    '                'End Type

    '                'The Model1PolyOut is the optimal spatial schedule...and is dumped in a file to be read by 
    '                'DualPlan...
    '                Model1PolyOut(jstand).polyid = System.Math.Abs(AA(jstand).polyid)
    '                Model1PolyOut(jstand).NPVAspat = NPVsched
    '                Model1PolyOut(jstand).NPVallMx = Model1PresData(kalt).NPVallMx
    '                Model1PolyOut(jstand).NPVallMn = Model1PresData(kalt).NPVallMn
    '                Model1PolyOut(jstand).NPVAspatMx = MaxNPVUnc(jstand)
    '                Model1PolyOut(jstand).ISpaceSeries = Model1PresData(kalt).ISpaceSeries
    '                Model1PolyOut(jstand).BufferSeries = Model1PresData(kalt).BufferSeries
    '                Model1PolyOut(jstand).EdgeSeries = Model1PresData(kalt).EdgeSeries
    '                Model1PolyOut(jstand).presAA = Model1PresData(kalt).presAA
    '                Model1PolyOut(jstand).jreg = Model1PresData(kalt).jreg
    '                Model1PolyOut(jstand).npresrg = Model1PresData(kalt).npresrg
    '                Model1PolyOut(jstand).pres1rg = Model1PresData(kalt).pres1rg
    '                Model1PolyOut(jstand).pres2rg = Model1PresData(kalt).pres2rg
    '                Model1PolyOut(jstand).pres3rg = Model1PresData(kalt).pres3rg


    '                'added 4/2002 -- add nonspatial flows to forestwide flows
    '                kpres = Model1PresData(kalt).presAA
    '                kpresBestRg = Model1PresData(kalt).jreg
    '                ReDim kpresRG(3)
    '                kpresRG(1) = Model1PresData(kalt).pres1rg
    '                kpresRG(2) = Model1PresData(kalt).pres2rg
    '                kpresRG(3) = Model1PresData(kalt).pres3rg


    '                If relabel > 4999 Then
    '                    MainForm.TxtPolyDone.Text = Str(kaatotal)
    '                    MainForm.TxtPolyDone.Refresh()
    '                    relabel = 0
    '                End If
    '                kaatotal = kaatotal + 1
    '                relabel = relabel + 1
    '                Kaa = jstand
    '                ReDim MapLayerColor(NMapLayer)
    '                For jmap = 1 To NMapLayer
    '                    MapLayerColor(jmap) = AA(Kaa).MapLayerColor(jmap)
    '                Next jmap
    '                kmloc = AA(Kaa).mloc
    '                AAArea = AA(Kaa).area
    '                'set the fixed entry cost for this AA on a per land unit basis
    '                'ReDim EntryCostThisPoly!(MxTic)
    '                EntryCostFactor = 1.0# / AAArea
    '                For jtic = 1 To MxTic
    '                    EntryCostTicThisAA(jtic) = EntryCostTic(kmloc, jtic) * EntryCostFactor
    '                Next jtic

    '                ' Add info for forest-wide totals
    '                kregtic = AApres(kpres).RgTic

    '                '  Add Market flows for first rotation
    '                For jflow = AApres(kpres).FirstFlow To AApres(kpres + 1).FirstFlow - 1
    '                    kmtype = AAflow(jflow).type
    '                    ktic = AAflow(jflow).tic
    '                    MTypeFlow(kmtype, kmloc, ktic) = MTypeFlow(kmtype, kmloc, ktic) + AAArea * AAflow(jflow).quan
    '                Next jflow

    '                ' Add Info for biological conditions for each map for first rotation
    '                For jmap = 1 To NMapLayer
    '                    kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
    '                    For jtic = 0 To kregtic - 1
    '                        'CondTypeFlow#(kcoversiteAA, AAage + jtic, kMapArea, jtic) = CondTypeFlow#(kcoversiteAA, AAage + jtic, kMapArea, jtic) + AAArea!
    '                        CondTypeFlow(AApres(kpres).Coversite(jtic), AApres(kpres).Age(jtic), kMapArea, jtic) = CondTypeFlow(AApres(kpres).Coversite(jtic), AApres(kpres).Age(jtic), kMapArea, jtic) + AAArea
    '                    Next jtic
    '                Next jmap

    '                ' add area to forest-wide tally for regen info
    '                kRgbioNext = AApres(kpres).RgBioType(kpresBestRg)

    '                ' tally the cost for any site conversion
    '                RegenCost = AApres(kpres).RgCost(kpresBestRg) * AAArea
    '                MTypeFlow(MTypeSiteConvCost, kmloc, kregtic) = MTypeFlow(MTypeSiteConvCost, kmloc, kregtic) + RegenCost

    '                ' tally the fixed cost for the stand entry -- not per acre
    '                MTypeFlow(MTypeEntryCost, kmloc, kregtic) = MTypeFlow(MTypeEntryCost, kmloc, kregtic) + EntryCost


    '                ' sum flows from Future Rotations

    '                krg = 1
    '                Do Until kregtic > NTic
    '                    'UPGRADE_WARNING: Couldn't resolve default property of object kpresRG(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                    kpres = kpresRG(krg)

    '                    ' Add market flows
    '                    For jitem = RgPres(kpres).FirstFlow To RgPres(kpres).LastFlow
    '                        ktime = kregtic + RgFlow(jitem).tic
    '                        kmtype = RgFlow(jitem).type
    '                        MTypeFlow(kmtype, kmloc, ktime) = MTypeFlow(kmtype, kmloc, ktime) + RgFlow(jitem).quan * AAArea
    '                    Next jitem

    '                    ' Add age info for each map
    '                    For jmap = 1 To NMapLayer
    '                        kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
    '                        kTicRg = 0
    '                        For jtime = kregtic To kregtic + RgPres(kpres).Rotlength - 1
    '                            kTicRg = kTicRg + 1
    '                            'CondTypeFlow#(kRgBioType, kticrg, kMapArea, jtime) = CondTypeFlow#(kRgBioType, kticrg, kMapArea, jtime) + AAArea!
    '                            CondTypeFlow(RgPres(kpres).Coversite(kTicRg), RgPres(kpres).Age(kTicRg), kMapArea, jtime) = CondTypeFlow(RgPres(kpres).Coversite(kTicRg), RgPres(kpres).Age(kTicRg), kMapArea, jtime) + AAArea
    '                        Next jtime
    '                    Next jmap
    '                    kregtic = kregtic + RgPres(kpres).Rotlength

    '                    ' tally the fixed cost for the stand entry at end of rotation -- not per acre
    '                    MTypeFlow(MTypeEntryCost, kmloc, kregtic) = MTypeFlow(MTypeEntryCost, kmloc, kregtic) + EntryCost

    '                    krg = krg + 1
    '                Loop
    '            Next jstand


    '            leftuse = InStr(AAFN(jsubfor), ".")

    '            'If leftuse > 0 Then
    '            '  fnpoly$ = Left(AAFN$(jsubfor), leftuse) & ".bdp"
    '            'Else
    '            '  fnpoly$ = AAFN$(jsubfor) & ".bdp"
    '            'End If

    '            '07/14, wei
    '            'ebh: so, we're dumping the model1 prescription info....
    '            fnpolyBdp = DpSpaceLinkBaseNameOut & LastDPfileID & "SF" & jsubfor & ".bdp"
    '            FileOpen(2, fnpolyBdp, OpenMode.Binary)
    '            FilePut(2, Model1PolyOut)
    '            FileClose(2)

    '            WriteLine(66, fnpolyBdp)
    '            WriteLine(66, NPolySubFor(jsubfor))



    '            ' hmh 9/15/2003 added "start" values to write NPV values by subforest

    '            Write(96, jsubfor)
    '            SubArea = TotalArea - TotalAreaStart
    '            Write(96, SubTIMEstart)
    '            Write(96, SubTIMESTOP)
    '            Write(96, SubArea)
    '            Write(96, (TotalLossSpatial - TotalLossSpatialStart) + (OptimalPathVal - OptimalPathValStart))
    '            Write(96, OptimalPathVal - OptimalPathValStart)
    '            Write(96, TotalLossSpatial - TotalLossSpatialStart)
    '            Write(96, TotalISpaceVal - TotalISpaceValStart)
    '            Write(96, (TotalISpaceVal - TotalISpaceValStart) + (OptimalPathVal - OptimalPathValStart))
    '            Write(96, " ")
    '            Write(96, ((TotalLossSpatial - TotalLossSpatialStart) + (OptimalPathVal - OptimalPathValStart)) / SubArea)
    '            Write(96, (OptimalPathVal - OptimalPathValStart) / SubArea)
    '            Write(96, (TotalLossSpatial - TotalLossSpatialStart) / SubArea)
    '            Write(96, (TotalISpaceVal - TotalISpaceValStart) / SubArea)
    '            WriteLine(96, ((TotalISpaceVal - TotalISpaceValStart) + (OptimalPathVal - OptimalPathValStart)) / SubArea)

    '        Next jsubfor
    '        FileClose(66)







    '        ' write spatial summary to output
    '        'Edgeflow$ = FileOut$ + "EdgeFlow" + Str$(iterNumber) + ".csv"
    '        Edgeflow = FileOut & "EdgeFlow.csv"
    '        FileOpen(76, Edgeflow, OpenMode.Output)
    '        WriteLine(76, "EdgeType1,", "EdgeType2,", "jper,", "EdgePrice!,", "Flow,")




    '        For jedgetype1 = 0 To Nedgetype
    '            For jEdgeType2 = 0 To jedgetype1
    '                Print(9, "Summary for EdgeType Combination" & jedgetype1)
    '                PrintLine(9, " and " & jEdgeType2)
    '                PrintLine(9, "Period,     Shadow Price,     Amount of Edge,")
    '                For jper = 0 To Nperiod
    '                    Eprice = 0
    '                    If jper > 0 Then Eprice = Edgeprice(jedgetype1, jEdgeType2, jper) / EdgePriceConversion
    '                    If jedgetype1 <> jEdgeType2 Then
    '                        PrintLine(9, jper, ",", TAB(15), "," & Eprice & ",", TAB(30), "," & SumEdge(jedgetype1, jEdgeType2, jper) + SumEdge(jEdgeType2, jedgetype1, jper) & ",")
    '                        WriteLine(76, jedgetype1, jEdgeType2, jper, Eprice, SumEdge(jedgetype1, jEdgeType2, jper) + SumEdge(jEdgeType2, jedgetype1, jper))
    '                    Else
    '                        PrintLine(9, jper, ",", TAB(15), "," & Eprice & ",", TAB(30), "," & SumEdge(jedgetype1, jEdgeType2, jper) & ",")
    '                        WriteLine(76, jedgetype1, jEdgeType2, jper, Eprice, SumEdge(jedgetype1, jEdgeType2, jper))
    '                    End If
    '                Next jper
    '                PrintLine(9)
    '            Next jEdgeType2
    '        Next jedgetype1
    '        FileClose(76)
    '        If maxtspace > MaxSpaceIndex Then
    '            'Print "Error detected when summing Interior Space production"
    '            'Print "MaxSpaceIndex& was set too low"
    '            'Print " It needs to be at least "; maxtspace&; ""
    '            Stop
    '        ElseIf MaxSpaceIndex - maxtspace > 10000 Then
    '            'Print "MaxSpaceIndex& is currently set at "; MaxSpaceIndex&
    '            'Print "It could be reduced to "; maxtspace&
    '            'Print
    '        End If

    '        'Spaceflow$ = FileOut$ + "SpaceFlow" + Str$(iterNumber) + ".csv"
    '        Spaceflow = FileOut & "SpaceFlow.csv"
    '        FileOpen(76, Spaceflow, OpenMode.Output)
    '        WriteLine(76, "jtype", "jper", "SpacePrice!", "Flow")
    '        For jtype = 1 To Nspacetype
    '            PrintLine(9, "SpaceType", "," & "period", "," & "Price", "," & "Production" & ",")
    '            PrintLine(9, jtype, "," & "0", "," & "-----", "," & SumSpace(jtype, 0) & ",")
    '            WriteLine(76, jtype, "0", "-----", SumSpace(jtype, 0))
    '            For jper = 1 To Nperiod
    '                PrintLine(9, jtype, "," & jper, "," & SpacePrice(jtype, jper), "," & SumSpace(jtype, jper) & ",")
    '                WriteLine(76, jtype, jper, SpacePrice(jtype, jper), SumSpace(jtype, jper))
    '            Next jper
    '            PrintLine(9)
    '        Next jtype
    '        FileClose(76)


    '        'Print "Results for Iteration # "; jiteration
    '        'Print #9, "Results for Iteration # "; iterNumber

    '        'Print "MAXIMUM STATES ALL PASSES = "; MAXPASSNODE&
    '        ' PRINT "VALUE OF THE OPTIMAL PATH = "; OptimalPathVal#
    '        'Print "Max NonSpatial NPV! = ";
    '        'Print OptimalPathVal# + TotalLossSpatial#
    '        'Print "NonSpatial NPV of Solution = ";
    '        'Print OptimalPathVal#
    '        'Print "        Loss in NonSpatial NPV = ";
    '        'Print TotalLossSpatial#
    '        'Print "        Loss in NonSpatial NPV per unit area = ";
    '        'Print TotalLossSpatial# / TotalArea#

    '        PrintLine(9, "Maximum STATES all Passes = " & MAXPASSNODE)
    '        PrintLine(9, "Total area = " & TotalArea)
    '        PrintLine(9)
    '        Print(9, "       Max NonSpatial NPV! = ")
    '        PrintLine(9, OptimalPathVal + TotalLossSpatial)
    '        Print(9, "NonSpatial NPV of Solution = ")
    '        PrintLine(9, OptimalPathVal)
    '        Print(9, "    Loss in NonSpatial NPV = ")
    '        PrintLine(9, TotalLossSpatial)
    '        PrintLine(9)
    '        PrintLine(9, "Results in per unit area terms")
    '        Print(9, "  Total Area = ")
    '        PrintLine(9, TotalArea)
    '        Print(9, "Total NPV! of solution = ")
    '        PrintLine(9, (TotalISpaceVal + OptimalPathVal) / TotalArea)
    '        Print(9, "       ISpace NPV! of solution = ")
    '        PrintLine(9, TotalISpaceVal / TotalArea)
    '        Print(9, "       Max NonSpatial NPV! = ")
    '        PrintLine(9, (OptimalPathVal + TotalLossSpatial) / TotalArea)
    '        Print(9, "NonSpatial NPV of Solution = ")
    '        PrintLine(9, OptimalPathVal / TotalArea)
    '        Print(9, "    Loss in NonSpatial NPV = ")
    '        PrintLine(9, TotalLossSpatial / TotalArea)

    '        TIMESTOP = VB.Timer()
    '        PrintLine(9, "TIME STARTED  =" & TIMESTART)
    '        PrintLine(9, "TIME FINISHED =" & TIMESTOP)

    '        Write(96, "Total")
    '        Write(96, TIMESTART)
    '        Write(96, TIMESTOP)
    '        Write(96, TotalArea)
    '        Write(96, TotalLossSpatial + OptimalPathVal)
    '        Write(96, OptimalPathVal)
    '        Write(96, TotalLossSpatial)
    '        Write(96, TotalISpaceVal)
    '        Write(96, TotalISpaceVal + OptimalPathVal)
    '        Write(96, " ")
    '        Write(96, (TotalLossSpatial + OptimalPathVal) / TotalArea)
    '        Write(96, OptimalPathVal / TotalArea)
    '        Write(96, TotalLossSpatial / TotalArea)
    '        Write(96, TotalISpaceVal / TotalArea)
    '        WriteLine(96, (TotalISpaceVal + OptimalPathVal) / TotalArea)

    '        FileClose(96)
    '        FileClose(77)




    '        FileClose(9) 'wei 07/15
    '        End 'wei 07/15

    '        MainForm.TxtPolyDone.Text = Str(kaatotal)
    '        MainForm.Label1.Text = "Finished this iteration"
    '        MainForm.Label1.Refresh()


    '    End Sub


    '    Private Sub ISpaceForStage(ByRef kpass As Short, ByRef kstage As Short)
    '        Dim maxtspace As Integer
    '        Dim SpvalStand As Single
    '        Dim SPaceTypeThisStand As Short
    '        Dim Spval As Single
    '        Dim jalt As Short
    '        Dim kval As Integer
    '        Dim kstandInZone As Short
    '        Dim kalt As Integer
    '        Dim lastAlt As Short
    '        Dim jestage As Short
    '        Dim spaceindex As Integer
    '        Dim jper As Short
    '        Dim jj As Short
    '        Dim kIspaceSeries As Short
    '        Dim kbufferseries As Short
    '        Dim nearlierpass As Short
    '        Dim nearlierstage As Short
    '        Dim Spacestate As Integer
    '        Dim kstagejlevel As Short
    '        Dim jlevel As Short
    '        Dim jstand As Short
    '        Dim KSubIzone As Integer
    '        Dim nstandThisSpace As Short
    '        Dim whichIzone As Integer
    '        Dim StandInThisSpace As Object
    '        Dim IDBegLevelSpace As Object
    '        Dim jspace As Short
    '        Dim KIzoneThisStage As Integer
    '        Dim tspace As Integer
    '        ' start of interior SPACE block
    '        'Line Input #35, dummy$
    '        'Input #35, NspaceThisStage
    '        NspaceThisStage = IzoneEachStage(kpass).Stage(kstage).NIzone

    '        TotalIspacesPerArc = NspaceThisStage 'total number of influence zones per arc for the current stage
    '        TotalArcsPerStand = NaltStage(kstage) 'total number of arcs per node for the current stage

    '        If NspaceThisStage > 0 Then
    '            'ReDim FirstSpaceval&(NspaceThisStage), Spaceval!(MaxSpaceIndex&, NaltStage(kstage))

    '            ' redim can be deleted
    '            'ReDim Spaceval!(MaxSpaceIndex&)

    '            ReDim FirstSpaceval(NspaceThisStage)
    '            ReDim SpaceCoef(NspaceThisStage, NBegLevel + 1)
    '            ReDim SpaceReduce(NspaceThisStage)
    '            ReDim Spaceid(NspaceThisStage)
    '        End If
    '        tspace = 0
    '        KIzoneThisStage = IzoneEachStage(kpass).Stage(kstage).FirstIzone

    '        For jspace = 1 To NspaceThisStage 'ebh: for each interior space...
    '            'Input #35, nstandThisSpace
    '            whichIzone = SortedIzone(KIzoneThisStage)
    '            nstandThisSpace = IZdata(whichIzone).NStand



    '            FirstSpaceval(jspace) = tspace + 1
    '            ReDim IDBegLevelSpace(nstandThisSpace)
    '            ReDim StandInThisSpace(nstandThisSpace)
    '            ReDim SubSpaceArea(nstandThisSpace)
    '            ReDim Spacekaltid(nstandThisSpace)
    '            ReDim SpaceEarlierStage(nstandThisSpace)
    '            ReDim SpaceNaltEarlierStage(nstandThisSpace)
    '            ReDim SpaceCoefEarlierStage(nstandThisSpace)
    '            ReDim SpaceStandInZoneEarlierStage(nstandThisSpace)

    '            KSubIzone = IZdata(whichIzone).FirstSubIZone
    '            For jstand = 1 To nstandThisSpace 'ebh: ...and for each stand within the interior space

    '                'Input #35, StandInThisSpace(jstand), SubSpaceArea!(jstand)
    '                StandInThisSpace(jstand) = SubIZData(KSubIzone).ParentStand
    '                SubSpaceArea(jstand) = SubIZData(KSubIzone).Area


    '                IDBegLevelSpace(jstand) = 0
    '                For jlevel = 1 To NBegLevel 'ebh: the dimension of the stage...# of stands in each node
    '                    ' PriorstageIdforLevel() is based on end-of-stage levels
    '                    ' end-of-stage levels for kstage-1 = beg-of-stage levels kstage
    '                    kstagejlevel = PriorStageIDforLevel(kstage - 1, jlevel)
    '                    If IDStand(kstagejlevel) = StandInThisSpace(jstand) Then 'ebh: if the stand in the stage is the same as the stand in the interior space
    '                        SpaceCoef(jspace, jlevel) = 1
    '                        IDBegLevelSpace(jstand) = jlevel
    '                        Exit For
    '                    End If
    '                Next jlevel
    '                KSubIzone = KSubIzone + 1
    '            Next jstand

    '            '  Set coefficients for identifying condition of the
    '            '  stands in interior space set jspace


    '            'Spacestate& = 1
    '            Spacestate = NaltStage(kstage) 'ebh: 1 spacestate for each stand prescription (?)

    '            SpaceReduce(jspace) = 0
    '            For jlevel = 1 To NBegLevel
    '                If SpaceCoef(jspace, jlevel) >= 1 Then
    '                    SpaceCoef(jspace, jlevel) = Spacestate
    '                    SpaceReduce(jspace) = SpaceReduce(jspace) + Spacestate
    '                    Spacestate = Spacestate * NaltBegLevel(jlevel)
    '                Else
    '                    SpaceCoef(jspace, jlevel) = 0
    '                End If
    '            Next jlevel

    '            '  Determine interior space value for each possible
    '            '  condition for influence area jspace
    '            nearlierstage = 0
    '            nearlierpass = 0

    '            '  First consider alternatives selected for stands
    '            '  in this space set that were scheduled in an earlier pass.
    '            '  Also determine how many stands were scheduled this
    '            '  pass and how many were scheduled in an earlier pass.

    '            For jstand = 1 To nstandThisSpace
    '                If PassToSched(StandInThisSpace(jstand)) < kpass Then 'ebh: it was already scheduled?
    '                    kbufferseries = SpaceBufferSeriesSched(StandInThisSpace(jstand)) 'ebh: definition of quality buffer array this stand has ?
    '                    kIspaceSeries = SpaceISpaceSeriesSched(StandInThisSpace(jstand))

    '                    If nearlierpass > 0 Then
    '                        'For jper = 1 To Nperiod
    '                        'If SpaceType(jper) <> SpaceSched(standinthisspace(jstand), jper) Then SpaceType(jper) = 0
    '                        'Next jper

    '                        ' adjust for quality buffer feature
    '                        For jj = 1 To jstand - 1
    '                            For jper = 1 To Nperiod
    '                                SpaceType(jj, jper) = SpaceType(jj, jper) * TicSeriesYesNo(kbufferseries, jper)
    '                            Next jper
    '                        Next jj
    '                        For jper = 1 To Nperiod
    '                            SpaceType(jstand, jper) = SpaceType(jstand, jper) * TicSeriesYesNo(kIspaceSeries, jper)
    '                        Next jper
    '                        For jj = jstand + 1 To nstandThisSpace
    '                            For jper = 1 To Nperiod
    '                                SpaceType(jj, jper) = SpaceType(jj, jper) * TicSeriesYesNo(kbufferseries, jper)
    '                            Next jper
    '                        Next jj

    '                    Else 'nearlierpass =0
    '                        'For jper = 1 To Nperiod
    '                        'SpaceType(jper) = SpaceSched(standinthisspace(jstand), jper)
    '                        'Next jper

    '                        For jj = 1 To jstand - 1
    '                            For jper = 1 To Nperiod
    '                                SpaceType(jj, jper) = TicSeriesYesNo(kbufferseries, jper)
    '                            Next jper
    '                        Next jj
    '                        For jper = 1 To Nperiod
    '                            SpaceType(jstand, jper) = TicSeriesYesNo(kIspaceSeries, jper)
    '                        Next jper
    '                        For jj = jstand + 1 To nstandThisSpace
    '                            For jper = 1 To Nperiod
    '                                SpaceType(jj, jper) = TicSeriesYesNo(kbufferseries, jper)
    '                            Next jper
    '                        Next jj

    '                    End If
    '                    nearlierpass = nearlierpass + 1
    '                ElseIf StandInThisSpace(jstand) <> IDStand(kstage) Then
    '                    ' store info on stands in earlier stage of this pass
    '                    ' check for SpaceType() will be done later
    '                    nearlierstage = nearlierstage + 1
    '                    SpaceEarlierStage(nearlierstage) = IDStage(StandInThisSpace(jstand))
    '                    SpaceNaltEarlierStage(nearlierstage) = NaltStage(IDStage(StandInThisSpace(jstand)))
    '                    SpaceCoefEarlierStage(nearlierstage) = SpaceCoef(jspace, IDBegLevelSpace(jstand))

    '                    ' add SpaceStandInZoneEarlierStage 10/2002
    '                    SpaceStandInZoneEarlierStage(nearlierstage) = jstand

    '                End If
    '            Next jstand

    '            '  Spacetype() stores condition for stands already
    '            '  scheduled. Checks involving earlier stages (stands)
    '            '  in current pass will use it as a starting point.
    '            '  Because multiple checks are involved, Spacetype()is
    '            '  retained and Nodespacetype()is used for each specific
    '            '  check.

    '            spaceindex = 1
    '            If nearlierstage > 0 Then

    '                '  First set alternative counter/identifier to 1
    '                '  for each stand in kspace that is represented in
    '                '  an earlier stage of this pass
    '                For jestage = 1 To nearlierstage
    '                    Spacekaltid(jestage) = 1
    '                Next jestage

    '                ' check to see that MaxSpaceIndex& was set large enough
    '                'spaceindex& = 1
    '                ' 9/6/2003 hmh
    '                spaceindex = NaltStage(kstage)
    '                For jestage = 1 To nearlierstage
    '                    lastAlt = SpaceNaltEarlierStage(jestage)
    '                    spaceindex = spaceindex + SpaceCoefEarlierStage(jestage) * (lastAlt - 1)
    '                Next jestage
    '                If tspace + spaceindex > MaxSpaceIndex Then
    '                    'Print "Error!! MaxSpaceIndex& was set too low"
    '                    'Print " It needs to be at least "; tspace& + spaceindex&;
    '                    Stop
    '                End If

    '                '  Calculate Spaceval!() for each possible set of
    '                '  spatial conditions for this space jspace in
    '                '  the current DP window.  This space involves all
    '                '  combinations of alternative types for stands in
    '                '  jspace and also in an earlier stage of this
    '                '  window

    '                Do Until Spacekaltid(nearlierstage) > SpaceNaltEarlierStage(nearlierstage)
    '                    If nearlierpass > 0 Then
    '                        '  If space involves stands scheduled in prior
    '                        '  passes, set spatial conditions to result
    '                        '  from examination of stands scheduled in prior passes.

    '                        'For jper = 1 To Nperiod
    '                        'NodeSpaceType(jper) = SpaceType(jper)
    '                        'Next jper

    '                        For jj = 1 To nstandThisSpace
    '                            For jper = 1 To Nperiod
    '                                NodeSpaceType(jj, jper) = SpaceType(jj, jper)
    '                            Next jper
    '                        Next jj

    '                        ' adjust Nodespacetype() to account for earlier stages
    '                        For jestage = 1 To nearlierstage
    '                            'kalt& = FirstAltStage&(SpaceEarlierStage(jestage)) + Spacekaltid(jestage) - 1
    '                            kalt = FirstAltStand(IDStand(SpaceEarlierStage(jestage))) + Spacekaltid(jestage) - 1

    '                            'For jper = 1 To Nperiod
    '                            'If NodeSpaceType(jper) <> SpaceTypeAlt(jper, kalt&) Then NodeSpaceType(jper) = 0
    '                            'Next jper
    '                            'NodeSpaceType expanded to recognize separate areas within each Inf Zone

    '                            For jj = 1 To jestage - 1
    '                                kstandInZone = SpaceStandInZoneEarlierStage(jj)
    '                                For jper = 1 To Nperiod
    '                                    NodeSpaceType(kstandInZone, jper) = NodeSpaceType(kstandInZone, jper) * TicSeriesYesNo(IBufferSeriesAlt(kalt), jper)
    '                                Next jper
    '                            Next jj

    '                            ' Must pass stricter influence zone test for area within the stand if it is to produce ISPACE
    '                            kstandInZone = SpaceStandInZoneEarlierStage(jestage)
    '                            For jper = 1 To Nperiod
    '                                NodeSpaceType(kstandInZone, jper) = NodeSpaceType(kstandInZone, jper) * TicSeriesYesNo(IspaceSeriesAlt(kalt), jper)
    '                            Next jper


    '                            For jj = jestage + 1 To nearlierstage
    '                                kstandInZone = SpaceStandInZoneEarlierStage(jj)
    '                                For jper = 1 To Nperiod
    '                                    NodeSpaceType(kstandInZone, jper) = NodeSpaceType(kstandInZone, jper) * TicSeriesYesNo(IBufferSeriesAlt(kalt), jper)
    '                                Next jper
    '                            Next jj
    '                        Next jestage
    '                    Else
    '                        'nearlierpass = 0 so set Nodespacetype() to conditions of earliest stage
    '                        'kalt& = FirstAltStage&(SpaceEarlierStage(1)) + Spacekaltid(1) - 1
    '                        kalt = FirstAltStand(IDStand(SpaceEarlierStage(1))) + Spacekaltid(1) - 1
    '                        kstandInZone = SpaceStandInZoneEarlierStage(1)

    '                        'For jper = 1 To Nperiod
    '                        'NodeSpaceType(jper) = SpaceTypeAlt(jper, kalt&)
    '                        'Next jper

    '                        ' For the earliest stage, set initial conditions for each influence zone subarea based on this stand
    '                        For jstand = 1 To kstandInZone - 1
    '                            For jper = 1 To Nperiod
    '                                NodeSpaceType(jstand, jper) = TicSeriesYesNo(IBufferSeriesAlt(kalt), jper)
    '                            Next jper
    '                        Next jstand

    '                        For jper = 1 To Nperiod
    '                            NodeSpaceType(kstandInZone, jper) = TicSeriesYesNo(IspaceSeriesAlt(kalt), jper)
    '                        Next jper

    '                        For jstand = kstandInZone + 1 To nstandThisSpace
    '                            For jper = 1 To Nperiod
    '                                NodeSpaceType(jstand, jper) = TicSeriesYesNo(IBufferSeriesAlt(kalt), jper)
    '                            Next jper
    '                        Next jstand

    '                        ' Now update to consider condition of other earlier stages
    '                        For jestage = 2 To nearlierstage
    '                            'kalt& = FirstAltStage&(SpaceEarlierStage(jestage)) + Spacekaltid(jestage) - 1
    '                            kalt = FirstAltStand(IDStand(SpaceEarlierStage(jestage))) + Spacekaltid(jestage) - 1

    '                            'For jper = 1 To Nperiod
    '                            'If NodeSpaceType(jper) <> SpaceTypeAlt(jper, kalt&) Then NodeSpaceType(jper) = 0
    '                            'Next jper

    '                            kstandInZone = SpaceStandInZoneEarlierStage(jestage)
    '                            For jstand = 1 To kstandInZone - 1
    '                                For jper = 1 To Nperiod
    '                                    NodeSpaceType(jstand, jper) = NodeSpaceType(jstand, jper) * TicSeriesYesNo(IBufferSeriesAlt(kalt), jper)
    '                                Next jper
    '                            Next jstand
    '                            For jper = 1 To Nperiod
    '                                NodeSpaceType(kstandInZone, jper) = NodeSpaceType(kstandInZone, jper) * TicSeriesYesNo(IspaceSeriesAlt(kalt), jper)
    '                            Next jper
    '                            For jstand = kstandInZone + 1 To nstandThisSpace
    '                                For jper = 1 To Nperiod
    '                                    NodeSpaceType(jstand, jper) = NodeSpaceType(jstand, jper) * TicSeriesYesNo(IBufferSeriesAlt(kalt), jper)
    '                                Next jper
    '                            Next jstand
    '                        Next jestage
    '                    End If


    '                    ' 9/08/2003 hmh change because of switch in spaceval! from two dim to 1 dim
    '                    spaceindex = 0
    '                    'spaceindex& = 1

    '                    For jestage = 1 To nearlierstage
    '                        spaceindex = spaceindex + SpaceCoefEarlierStage(jestage) * (Spacekaltid(jestage) - 1)
    '                    Next jestage
    '                    kval = FirstSpaceval(jspace) + spaceindex - 1
    '                    'kalt& = FirstAltStage&(kstage)
    '                    kalt = FirstAltStand(IDStand(kstage))
    '                    'For jalt = 1 To NaltStage(kstage)
    '                    'Spval! = 0
    '                    'For jper = 1 To Nperiod
    '                    'sptype = NodeSpaceType(jper)
    '                    'If sptype <> SpaceTypeAlt(jper, kalt&) Then sptype = 0
    '                    'Spval! = Spval! + SpacePrice!(sptype, jper)
    '                    'Next jper
    '                    'kval& = FirstSpaceval&(jspace) + spaceindex& - 1
    '                    'Spaceval!(kval&, jalt) = Spval! * SpaceArea!(jstand)
    '                    'kalt& = kalt& + 1
    '                    'Next jalt

    '                    For jalt = 1 To NaltStage(kstage)
    '                        Spval = 0
    '                        For jstand = 1 To nstandThisSpace
    '                            SPaceTypeThisStand = Stand(StandInThisSpace(jstand)).SpaceType
    '                            SpvalStand = 0
    '                            If IDStand(kstage) <> StandInThisSpace(jstand) Then
    '                                For jper = 1 To Nperiod
    '                                    SpvalStand = SpvalStand + NodeSpaceType(jstand, jper) * TicSeriesYesNo(IBufferSeriesAlt(kalt), jper) * SpacePrice(SPaceTypeThisStand, jper)
    '                                Next jper
    '                            Else
    '                                For jper = 1 To Nperiod
    '                                    SpvalStand = SpvalStand + NodeSpaceType(jstand, jper) * TicSeriesYesNo(IspaceSeriesAlt(kalt), jper) * SpacePrice(SPaceTypeThisStand, jper)
    '                                Next jper
    '                            End If
    '                            Spval = Spval + SpvalStand * SubSpaceArea(jstand)
    '                        Next jstand
    '                        Spaceval(kval + jalt) = Spval
    '                        kalt = kalt + 1
    '                    Next jalt

    '                    ' hmh 9/08/2003  set spaceindex& to total number of rows added
    '                    spaceindex = spaceindex + NaltStage(kstage)

    '                    ' Update set parameters for next alternative combo
    '                    ' This increments Spacekaltid(nearlierstage)
    '                    ' until finally all combos have been addressed
    '                    Spacekaltid(1) = Spacekaltid(1) + 1
    '                    For jestage = 1 To nearlierstage - 1
    '                        If Spacekaltid(jestage) > SpaceNaltEarlierStage(jestage) Then
    '                            Spacekaltid(jestage) = 1
    '                            Spacekaltid(jestage + 1) = Spacekaltid(jestage + 1) + 1
    '                        End If
    '                    Next jestage
    '                Loop


    '            ElseIf nearlierpass > 0 Then
    '                'nealierstage = 0 so this is the case where all other stands are in earlier pass
    '                'kalt& = FirstAltStage&(kstage)
    '                kalt = FirstAltStand(IDStand(kstage))

    '                '9/08/2003 change to one dimension  HMH
    '                'kval& = FirstSpaceval&(jspace)
    '                kval = FirstSpaceval(jspace) - 1

    '                'For jalt = 1 To NaltStage(kstage)
    '                'Spval! = 0
    '                'For jper = 1 To Nperiod
    '                'sptype = SpaceType(jper)
    '                'If sptype <> SpaceTypeAlt(jper, kalt&) Then sptype = 0
    '                'Spval! = Spval! + SpacePrice!(sptype, jper)
    '                'Next jper
    '                'kval& = FirstSpaceval&(jspace)
    '                'Spaceval!(kval&, jalt) = Spval! * SpaceArea!(jstand)
    '                'kalt& = kalt& + 1
    '                'Next jalt

    '                For jalt = 1 To NaltStage(kstage)
    '                    Spval = 0
    '                    For jstand = 1 To nstandThisSpace
    '                        SPaceTypeThisStand = Stand(StandInThisSpace(jstand)).SpaceType
    '                        SpvalStand = 0
    '                        If IDStand(kstage) <> StandInThisSpace(jstand) Then
    '                            For jper = 1 To Nperiod
    '                                SpvalStand = SpvalStand + SpaceType(jstand, jper) * TicSeriesYesNo(IBufferSeriesAlt(kalt), jper) * SpacePrice(SPaceTypeThisStand, jper)
    '                            Next jper
    '                        Else
    '                            For jper = 1 To Nperiod
    '                                SpvalStand = SpvalStand + SpaceType(jstand, jper) * TicSeriesYesNo(IspaceSeriesAlt(kalt), jper) * SpacePrice(SPaceTypeThisStand, jper)
    '                            Next jper
    '                        End If
    '                        Spval = Spval + SpvalStand * SubSpaceArea(jstand)
    '                    Next jstand
    '                    Spaceval(kval + jalt) = Spval
    '                    kalt = kalt + 1
    '                Next jalt
    '                ' 9/09/2003 wei & hmh
    '                spaceindex = NaltStage(kstage)
    '            Else
    '                ' this is the case where the space has only one stand
    '                'kalt& = FirstAltStage&(kstage)
    '                kalt = FirstAltStand(IDStand(kstage))

    '                '9/08/2003 change to one dimension  HMH
    '                'kval& = FirstSpaceval&(jspace)
    '                kval = FirstSpaceval(jspace) - 1


    '                SPaceTypeThisStand = Stand(IDStand(kstage)).SpaceType
    '                'For jalt = 1 To NaltStage(kstage)
    '                'Spval! = 0
    '                'For jper = 1 To Nperiod
    '                'sptype = SpaceTypeAlt(jper, kalt&)
    '                'Spval! = Spval! + SpacePrice!(sptype, jper)
    '                'Next jper
    '                'kval& = FirstSpaceval&(jspace)
    '                'Spaceval!(kval&, jalt) = Spval! * SpaceArea!(jstand)
    '                'kalt& = kalt& + 1
    '                'Next jalt

    '                For jalt = 1 To NaltStage(kstage)
    '                    SpvalStand = 0
    '                    For jper = 1 To Nperiod
    '                        SpvalStand = SpvalStand + TicSeriesYesNo(IspaceSeriesAlt(kalt), jper) * SpacePrice(SPaceTypeThisStand, jper)
    '                    Next jper
    '                    Spaceval(kval + jalt) = SpvalStand * SubSpaceArea(1)
    '                    kalt = kalt + 1
    '                Next jalt
    '                ' 9/09/2003 hmh & wei
    '                spaceindex = NaltStage(kstage)
    '            End If
    '            tspace = tspace + spaceindex
    '            KIzoneThisStage = KIzoneThisStage + 1
    '        Next jspace

    '        TotalSpaceValRowsThisPass = TotalSpaceValRowsThisPass + tspace 'total number of space value evaluations for the current pass

    '        If tspace > maxtspace Then maxtspace = tspace

    '        ' end of interior SPACE block

    '    End Sub
    '    Private Sub GetSubForData(ByRef ksub As Short)
    '        '############EBH: WHY DO YOU NEED ALL THIS 'STUFF'? WHERE IS IT USED?
    '        '############This should be updated to get around the 'get' language
    '        'I THINK IT'S TO SUM THE INTERIOR SPACE PRODUCTION OF EACH NODE...

    '        'EBH: THIS READS THE OUTPUT OF THE SPACE DATA PREP PROGRAM

    '        Dim FileBin As String

    '        ' 1) Info for Each Influence Zone and its subcomponents
    '        '          SubIzoneData(NSubIZoneSubFor&) As SubIzoneData
    '        '          IzoneData(NIzoneSubFor&) As IzoneData
    '        FileBin = SubIzoneDataFN(ksub)
    '        ReDim SubIZData(NSubIzoneSubFor(ksub))
    '        FileOpen(2, FileBin, OpenMode.Binary, OpenAccess.Read)
    '        FileGet(2, SubIZData)
    '        FileClose(2)

    '        FileBin = IzoneDataFN(ksub)
    '        ReDim IZdata(NIzoneSubFor(ksub))
    '        FileOpen(2, FileBin, OpenMode.Binary, OpenAccess.Read)
    '        FileGet(2, IZdata)
    '        FileClose(2)

    '        ' 2) Window and Stand Sequencing for each DP Window
    '        '          DPFormSubFor(NPass) As DPformData
    '        FileBin = DPFormFN(ksub)
    '        'UPGRADE_WARNING: Couldn't resolve default property of object NPassSubFor(ksub). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '        ReDim DPFormSubFor(NPassSubFor(ksub))
    '        FileOpen(2, FileBin, OpenMode.Binary, OpenAccess.Read)
    '        FileGet(2, DPFormSubFor)
    '        FileClose(2)

    '        ' 3) Izone Sequencing for adding to DP
    '        '          IzoneEachStage(NPass) As PassIzone
    '        '          SortedIzone&(NIzoneSubFor&) AS Long

    '        '          change 1/27/2003  HMH
    '        '          IzoneListAllPasses&() AS Long
    '        FileBin = IzoneEachStageFN(ksub)
    '        'UPGRADE_WARNING: Couldn't resolve default property of object NPassSubFor(ksub). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '        ReDim IzoneEachStage(NPassSubFor(ksub))
    '        FileOpen(2, FileBin, OpenMode.Binary, OpenAccess.Read)
    '        FileGet(2, IzoneEachStage)
    '        FileClose(2)

    '        FileBin = SortedIzoneFN(ksub)
    '        ' changes 1/27 2003
    '        'ReDim SortedIzone&(NIzoneSubFor&(ksub))
    '        ReDim SortedIzone(NIzoneAllPassesSubFor(ksub))
    '        FileOpen(2, FileBin, OpenMode.Binary, OpenAccess.Read)
    '        FileGet(2, SortedIzone)
    '        FileClose(2)

    '        ' 4) Stand Data which includes link to list of adjacent stands
    '        FileBin = StandDataFN(ksub)
    '        NStand = NStandSubFor(ksub)
    '        ReDim Stand(NStand)
    '        FileOpen(2, FileBin, OpenMode.Binary, OpenAccess.Read)
    '        FileGet(2, Stand)
    '        FileClose(2)

    '        ' 5) SubFor list of adjacent stands and associated edge lengths
    '        FileBin = StandAdjDataFN(ksub)
    '        NAdj = NAdjSubFor(ksub)
    '        ReDim Adj(NAdj)
    '        FileOpen(2, FileBin, OpenMode.Binary, OpenAccess.Read)
    '        FileGet(2, Adj)
    '        FileClose(2)

    '    End Sub
    '    Private Sub ReadAlternativeFile(ByRef kSubFor As Short)
    '        'ebh: I think this is to get the prescription data in the old format...each stand has it's own data rather than
    '        '     a data bank
    '        Dim jalt As Integer
    '        Dim klast As Integer
    '        Dim kfirst As Integer
    '        Dim kNalt As Short
    '        Dim MxNPVUnc As Single
    '        Dim jstand As Short
    '        ReDim NaltStand(NStand)
    '        ReDim NPVnonSpatial(NpresSubFor(kSubFor))
    '        ReDim IspaceSeriesAlt(NpresSubFor(kSubFor))
    '        ReDim IBufferSeriesAlt(NpresSubFor(kSubFor))
    '        ReDim IEdgeSeriesAlt(NpresSubFor(kSubFor))
    '        ReDim SpaceTypeAlt(Nperiod, NpresSubFor(kSubFor))
    '        ReDim MaxNPVUnc(NStand)

    '        ReDim Model1PresData(NpresSubFor(kSubFor))
    '        ReDim Model1PolyData(NPolySubFor(kSubFor))
    '        ReDim FirstAltStand(NStand)


    '        FileOpen(4, Model1PresFileList(kSubFor), OpenMode.Binary, OpenAccess.Read)
    '        'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '        FileGet(4, Model1PresData)
    '        FileClose(4)
    '        FileOpen(4, Model1PolyFileList(kSubFor), OpenMode.Binary, OpenAccess.Read)
    '        'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '        FileGet(4, Model1PolyData)
    '        FileClose(4)

    '        ReDim Model1PolyOut(NPolySubFor(kSubFor))

    '        For jstand = 1 To NStand
    '            MxNPVUnc = -9999
    '            kNalt = Model1PolyData(jstand).npres
    '            'UPGRADE_WARNING: Couldn't resolve default property of object NaltStand(jstand). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '            NaltStand(jstand) = kNalt
    '            kfirst = Model1PolyData(jstand).firstpres
    '            FirstAltStand(jstand) = kfirst
    '            klast = kfirst + kNalt - 1
    '            For jalt = kfirst To klast
    '                NPVnonSpatial(jalt) = Model1PresData(jalt).NPVAspat
    '                'UPGRADE_WARNING: Couldn't resolve default property of object IspaceSeriesAlt(jalt). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                IspaceSeriesAlt(jalt) = Model1PresData(jalt).ISpaceSeries
    '                'UPGRADE_WARNING: Couldn't resolve default property of object IBufferSeriesAlt(jalt). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                IBufferSeriesAlt(jalt) = Model1PresData(jalt).BufferSeries
    '                'UPGRADE_WARNING: Couldn't resolve default property of object IEdgeSeriesAlt(jalt). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                IEdgeSeriesAlt(jalt) = Model1PresData(jalt).EdgeSeries
    '                If MxNPVUnc < NPVnonSpatial(jalt) Then
    '                    MxNPVUnc = NPVnonSpatial(jalt)
    '                    MaxNPVUnc(jstand) = MxNPVUnc
    '                End If


    '                'Determine IEdgeSeriesAlt (kalt&)
    '                'ReDim kpresRg&(3)
    '                'kpresRg&(1) = Model1PresData(jalt&).pres1rg
    '                'kpresRg&(2) = Model1PresData(jalt&).pres2rg
    '                'kpresRg&(3) = Model1PresData(jalt&).pres3rg

    '                'kpresAA& = Model1PresData(jalt&).presAA

    '                'kregtic = AApres(kpresAA&).RgTic
    '                'krg = 1
    '                'ESeries = 0
    '                'Do Until kregtic > NTic
    '                'ESeries = ESeries + SpaceMult(kregtic)
    '                'kpres& = kpresRg&(krg)
    '                'kregtic = kregtic + RgPres(kpres&).Rotlength
    '                'krg = krg + 1
    '                'Loop
    '                'IEdgeSeriesAlt(jalt&) = ESeries

    '            Next jalt
    '        Next jstand
    '        TotalAlt = NpresSubFor(kSubFor)
    '    End Sub
    '    Private Sub ReadDualSpaceInput()
    '        Dim discfactor As Single
    '        Dim kdone As String
    '        Dim kper As Short
    '        Dim jseries As Short
    '        Dim jdum As Short
    '        Dim jsubfor As Short
    '        Dim dummy5 As String
    '        Dim jsub As Short
    '        Dim PrepFileList As String
    '        Dim Mainout As String
    '        Dim kdumbSptype As Short
    '        Dim jspace As Short
    '        Dim Dummy8 As String
    '        Dim dummy4 As String
    '        Dim jper As Short
    '        Dim jedge2 As Short
    '        Dim jedge1 As Short
    '        Dim Dummy7 As String
    '        Dim dummy3 As String
    '        Dim dummy2 As String
    '        Dim dummy1 As String
    '        Dim kNEcolArea As Short
    '        Dim kNcoversiteLocal As Short
    '        Dim kNCover As Short
    '        Dim kNspacetype As Short
    '        Dim kSpaceLayer As Short
    '        Dim kNSubunit As Short
    '        Dim Dummy As String

    '        ' use this file to get NspaceType
    '        FileOpen(59, DefineIspaceFile, OpenMode.Input)
    '        Dummy = LineInput(59)
    '        Input(59, kNSubunit)
    '        Input(59, kSpaceLayer)
    '        Input(59, kNspacetype)
    '        Input(59, kNCover)
    '        Input(59, kNcoversiteLocal)
    '        Input(59, kNEcolArea)
    '        Nspacetype = kNspacetype
    '        FileClose(59)


    '        'Main Dualplan input file to use ###########shouldn't have to do this...go through trimmer each time!!
    '        ''################ Call ReadDualPlanInput(DualplanLinkLastMainIn, AA, AApres, AAflow)

    '        File95 = DpSpaceLinkMainIn 'wei
    '        FileOpen(95, File95, OpenMode.Input)

    '        dummy1 = LineInput(95)
    '        Input(95, INPUTBSAVEFN)
    '        'Line Input #95, dummy4$
    '        'Line Input #95, presfilelist$
    '        dummy2 = LineInput(95)
    '        'Input #95, Nedgetype, Nspacetype, MaxSpaceIndex&, EdgePriceConversion!
    '        Input(95, Nedgetype)
    '        Input(95, MaxSpaceIndex)
    '        Input(95, EdgePriceConversion)

    '        ReDim Spaceval(MaxSpaceIndex)


    '        dummy3 = LineInput(95)
    '        'Input #95, Discrate!, Nperiod, Niteration
    '        Input(95, Discrate)
    '        Input(95, Nperiod)
    '        ReDim Edgeprice(Nedgetype, Nedgetype, Nperiod)
    '        ReDim Edgefloat(Nedgetype, Nedgetype)
    '        ReDim SpacePrice(Nspacetype, Nperiod)
    '        ReDim SpaceFloat(Nspacetype)
    '        Dummy7 = LineInput(95)
    '        Input(95, MaxStage)
    '        Input(95, MaxTotalalt)
    '        Input(95, MaxALTperStand)
    '        Input(95, MaxStandPerSpace)

    '        ' Read info for Edgeprice!() and Edgefloat!()
    '        dummy1 = LineInput(95)
    '        For jedge1 = 0 To Nedgetype
    '            For jedge2 = jedge1 To Nedgetype
    '                dummy2 = LineInput(95)
    '                dummy3 = LineInput(95)
    '                For jper = 1 To Nperiod
    '                    Input(95, Edgeprice(jedge1, jedge2, jper))
    '                    Edgeprice(jedge1, jedge2, jper) = Edgeprice(jedge1, jedge2, jper) * EdgePriceConversion
    '                Next jper
    '                dummy4 = LineInput(95)
    '                Input(95, Edgefloat(jedge1, jedge2))
    '                Edgefloat(jedge1, jedge2) = Edgefloat(jedge1, jedge2) * EdgePriceConversion
    '            Next jedge2
    '        Next jedge1
    '        ' Edgeprice!() array is symmetric, set side not read
    '        For jedge1 = 0 To Nedgetype
    '            For jedge2 = 0 To jedge1 - 1
    '                For jper = 1 To Nperiod
    '                    Edgeprice(jedge1, jedge2, jper) = Edgeprice(jedge2, jedge1, jper)
    '                Next jper
    '                Edgefloat(jedge1, jedge2) = Edgefloat(jedge2, jedge1)
    '            Next jedge2
    '        Next jedge1



    '        '############FILE CONTAINS ISPACE PRICES...THIS CAN NOW BE INCLUDED IN THE SPACESET PORTION OF THE DUALPLAN INPUT
    '        FileOpen(5, FileIspacePrices, OpenMode.Input)
    '        Dummy7 = LineInput(5)
    '        Dummy8 = LineInput(5)
    '        For jspace = 1 To Nspacetype
    '            Input(5, kdumbSptype)
    '            For jper = 1 To Nperiod
    '                Input(5, SpacePrice(jspace, jper))
    '            Next jper
    '        Next jspace
    '        FileClose(5)




    '        Mainout = FileOut & "summary.csv"
    '        FileOpen(9, Mainout, OpenMode.Output)
    '        PrintLine(9, "Run input from file: " & File95)
    '        FileClose(95)




    '        'Read File summarizing location of data from SpaceDataPrepProgram
    '        PrepFileList = DataPrepBaseNameOut & "FileList.txt"

    '        FileOpen(85, PrepFileList, OpenMode.Input)
    '        dummy1 = LineInput(85)
    '        dummy2 = LineInput(85)
    '        dummy3 = LineInput(85)
    '        'Write #16, "name of input file used for SpaceDatePrep"
    '        'Write #16, File85$
    '        'Write #16, "NSubFor"
    '        'Write #16, NSubFor
    '        Input(85, NSubFor)

    '        ReDim SubIzoneDataFN(NSubFor)
    '        ReDim IzoneDataFN(NSubFor)
    '        ReDim DPFormFN(NSubFor)
    '        ReDim IzoneEachStageFN(NSubFor)
    '        ReDim SortedIzoneFN(NSubFor)
    '        ReDim StandDataFN(NSubFor)
    '        ReDim StandAdjDataFN(NSubFor)


    '        ReDim NPassSubFor(NSubFor)
    '        ReDim NIzoneSubFor(NSubFor)
    '        ReDim NSubIzoneSubFor(NSubFor)
    '        ReDim NAdjSubFor(NSubFor)
    '        ReDim NStandSubFor(NSubFor)
    '        ReDim NIzoneAllPassesSubFor(NSubFor)


    '        For jsub = 1 To NSubFor
    '            dummy4 = LineInput(85)
    '            'Write #16, "Data for SubFor "; jSubFor
    '            Input(85, SubIzoneDataFN(jsub))
    '            Input(85, IzoneDataFN(jsub))
    '            Input(85, DPFormFN(jsub))
    '            Input(85, IzoneEachStageFN(jsub))
    '            Input(85, SortedIzoneFN(jsub))
    '            Input(85, StandDataFN(jsub))
    '            Input(85, StandAdjDataFN(jsub))
    '            dummy5 = LineInput(85)
    '            'Input #85, NPassSubFor(jsub), NIzoneSubFor&(jsub), _
    '            'NSubIzoneSubFor&(jsub), NAdjSubFor&(jsub), NStandSubFor(jsub)

    '            'change 1/27/2003
    '            Input(85, NPassSubFor(jsub))
    '            Input(85, NIzoneSubFor(jsub))
    '            Input(85, NSubIzoneSubFor(jsub))
    '            Input(85, NAdjSubFor(jsub))
    '            Input(85, NStandSubFor(jsub))
    '            Input(85, NIzoneAllPassesSubFor(jsub))
    '        Next jsub
    '        FileClose(85)

    '        'ebh: read file describing output from the trimmer program
    '        FileOpen(5, TrimmerFileList, OpenMode.Input)
    '        ReDim Model1PresFileList(NSubFor)
    '        ReDim Model1PolyFileList(NSubFor)
    '        ReDim NpresSubFor(NSubFor)
    '        ReDim NPolySubFor(NSubFor)

    '        'ReDim FileAlternativeSubFor$(NSubFor)



    '        dummy1 = LineInput(5)
    '        For jsubfor = 1 To NSubFor
    '            Input(5, jdum)
    '            Input(5, NPolySubFor(jsubfor))
    '            Input(5, NpresSubFor(jsubfor))
    '            Input(5, Model1PolyFileList(jsubfor))
    '            Input(5, Model1PresFileList(jsubfor))
    '        Next jsubfor



    '        FileClose(5)


    '        ' 11/2001: replaced this with code to use TicSeriesYesNo() timings
    '        'Open alttypefile$ For Input As #15
    '        'Line Input #15, dummy$
    '        'Input #15, nalttype
    '        'ReDim CutTics(nalttype, Nperiod)
    '        'For jalt = 1 To nalttype
    '        'Input #15, kalttype
    '        'For jper = 1 To Nperiod
    '        'Input #15, CutTics(jalt, jper)
    '        'Next jper
    '        'Next jalt
    '        'Close #15



    '        ' Set base 2 multipliers for identifying iedge series
    '        ReDim SpaceMult(Nperiod)
    '        'UPGRADE_WARNING: Couldn't resolve default property of object SpaceMult(Nperiod). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '        SpaceMult(Nperiod) = 1
    '        For jper = Nperiod - 1 To 1 Step -1
    '            'UPGRADE_WARNING: Couldn't resolve default property of object SpaceMult(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '            'UPGRADE_WARNING: Couldn't resolve default property of object SpaceMult(jper). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '            SpaceMult(jper) = SpaceMult(jper + 1) * 2
    '        Next jper
    '        'UPGRADE_WARNING: Couldn't resolve default property of object SpaceMult(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '        NTicSeriesYesNo = 2 * SpaceMult(1)

    '        'NTicSeriesYesNo = 2 ^ Nperiod

    '        'UPGRADE_WARNING: Lower bound of array TicSeriesYesNo was changed from 0,1 to 0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '        ReDim TicSeriesYesNo(NTicSeriesYesNo - 1, Nperiod)
    '        For jseries = 1 To NTicSeriesYesNo - 1
    '            For jper = 1 To Nperiod
    '                'UPGRADE_WARNING: Couldn't resolve default property of object TicSeriesYesNo(jseries - 1, jper). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                'UPGRADE_WARNING: Couldn't resolve default property of object TicSeriesYesNo(jseries, jper). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                TicSeriesYesNo(jseries, jper) = TicSeriesYesNo(jseries - 1, jper)
    '            Next jper

    '            kper = Nperiod
    '            kdone = ""
    '            'UPGRADE_WARNING: Couldn't resolve default property of object TicSeriesYesNo(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '            'UPGRADE_WARNING: Couldn't resolve default property of object TicSeriesYesNo(jseries, Nperiod). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '            TicSeriesYesNo(jseries, Nperiod) = TicSeriesYesNo(jseries, Nperiod) + 1
    '            Do Until kdone = "done"
    '                'UPGRADE_WARNING: Couldn't resolve default property of object TicSeriesYesNo(jseries, kper). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                If TicSeriesYesNo(jseries, kper) > 1 Then
    '                    'UPGRADE_WARNING: Couldn't resolve default property of object TicSeriesYesNo(jseries, kper). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                    TicSeriesYesNo(jseries, kper) = 0
    '                    'UPGRADE_WARNING: Couldn't resolve default property of object TicSeriesYesNo(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                    'UPGRADE_WARNING: Couldn't resolve default property of object TicSeriesYesNo(jseries, kper - 1). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                    TicSeriesYesNo(jseries, kper - 1) = TicSeriesYesNo(jseries, kper - 1) + 1
    '                    kper = kper - 1
    '                Else
    '                    kdone = "done"
    '                End If
    '            Loop
    '        Next jseries

    '        ReDim Discend(Nperiod)
    '        ReDim Discmid(Nperiod)
    '        discfactor = 1 + Discrate
    '        Discend(1) = 1 / ((discfactor) ^ 0.5)
    '        Discmid(1) = 1
    '        For jper = 2 To Nperiod
    '            Discend(jper) = Discend(jper - 1) / discfactor
    '            Discmid(jper) = Discmid(jper - 1) / discfactor
    '        Next jper

    '    End Sub
    '    'Private Sub WriteAAPres()
    '    '    'THIS SUBROUTINE SHOULD BE USED TO OUTPUT THE POLYGON SCHEDULE OF THE DP SOLUTION
    '    ' THIS SHOULD ALSO REPORT THE AMOUNT OF ISPACE IN EACH OF THE SPATIAL CONDITION SETS...TO BE USED
    '    ' BY DUALPLAN FOR PRICE ADJUSTMENT


    '    '    Dim intConvert As Integer
    '    '    Dim TotalConvert As Single
    '    '    Dim jfrom As Short
    '    '    Dim jto As Short
    '    '    Dim jrot As Short
    '    '    Dim zero As Short
    '    '    Dim NRgRotation As Short
    '    '    Dim kRgbioNext As Short
    '    '    Dim jlayer As Short
    '    '    Dim ktab As Short
    '    '    Dim kTicAA As Short
    '    '    Dim kTicRg As Short
    '    '    Dim kregTic As Short
    '    '    Dim kpres As Integer
    '    '    Dim regval As Single
    '    '    Dim TVAL As Single
    '    '    Dim ktime As Short
    '    '    Dim FirstTic As Short
    '    '    Dim SEV As Single
    '    '    Dim jage As Short
    '    '    Dim ktic As Short
    '    '    Dim kmtype As Short
    '    '    Dim kage As Short
    '    '    Dim jitem As Integer
    '    '    Dim NetPV As Single
    '    '    Dim rotlen As Short
    '    '    Dim kpresRG As Integer
    '    '    Dim jpresRG As Integer
    '    '    Dim kRgBioType As Short
    '    '    Dim jreg As Short
    '    '    Dim PresRegTic As Short
    '    '    Dim kMapArea As Short
    '    '    Dim jflow As Integer
    '    '    Dim AANPV As Single
    '    '    Dim PreShadPriceNPV As Single
    '    '    Dim mxPreShadPriceNPV As Single
    '    '    Dim jpresAA As Integer
    '    '    Dim jtic As Short
    '    '    Dim EntryCostFactor As Single
    '    '    Dim BestRgOption As Short
    '    '    Dim BestAApres As Integer
    '    '    Dim mxNPV As Single
    '    '    Dim AAArea As Single
    '    '    Dim kmloc As Short
    '    '    Dim jmap As Short
    '    '    Dim KAA As Integer
    '    '    Dim jaa As Integer
    '    '    Dim jaaset As Short
    '    '    Dim relabel As Integer
    '    '    Dim kaatotal As Integer
    '    '    Dim kdpspaceAA As Integer
    '    '    Dim kbestdpspace As Integer
    '    '    Dim j As Integer


    '    '    MainForm.Label1.Text = "Writing AA Output File"""
    '    '    MainForm.TxtPolyDone.Text = "0"
    '    '    MainForm.Refresh()


    '    '    Call SetTypePrices()
    '    '    'Write #26, "file#    polyid    area   MapColor(1 to nMap)         BAadj,    mxnpv    HarvType  pres#  ConvOpt#  rgtic       3 Sets Regen: (Rotlen  Prestype  Pres#)              Ageclass(tic0 to Ntic)               Coversite(tic0 to Ntic)"
    '    '    'Write #26, "file#,   polyid,   area,  MC1, MC2, MC3, MC4, MC5, MC6,BAadj,    mxNPV,   HarvType, pres#, ConvOpt#, rgtic,   Rot1, Ptype1, P#1,  Rot2, Ptype2, P#2,  Rot3, Ptype3, P#3,      A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10,      C0, C1, C2, C3, C4, C5, C6, C7, C8, C9, C10"
    '    '    Write(26, "file#", "polyid", "area", "MC1", "MC2", "MC3", "MC4", "MC5", "MC6", "BAadj", "mxNPV", "HarvType", "pres#", "ConvOpt#", "rgtic", "Rot1", "Ptype1", "Pres#1", "Rot2", "Ptype2", "Pres#2", "Rot3", "Ptype3", "Pres#3")
    '    '    For jtic = 1 To NTic
    '    '        Write(26, ", A" & jtic)
    '    '    Next
    '    '    For jtic = 1 To NTic
    '    '        Write(26, ", C" & jtic)
    '    '    Next
    '    '    WriteLine(26)
    '    '    '"A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "C0", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "C10")

    '    '    'ReDim CondTypeFlow#(Ncoversite, MxAgeClass, NMapArea, 0 To MxTic)
    '    '    'ReDim MTypeFlow#(NMType, NMLocation, MxTic)

    '    '    Dim PolyAge() As Object
    '    '    Dim PolyCoversite() As Object

    '    '    Dim ConversionPoly(,) As Integer
    '    '    Dim ConversionArea(,) As Single


    '    '    '  Evaluate existing stands one at a time

    '    '    'UPGRADE_WARNING: Lower bound of array ConversionPoly was changed from 0,0 to 0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '    '    ReDim ConversionPoly(Ncoversite, Ncoversite)
    '    '    'UPGRADE_WARNING: Lower bound of array ConversionArea was changed from 0,0 to 0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '    '    ReDim ConversionArea(Ncoversite, Ncoversite)

    '    '    'UPGRADE_WARNING: Lower bound of array EntryCostTicThisAA was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '    '    ReDim EntryCostTicThisAA(MxTic)
    '    '    'UPGRADE_WARNING: Lower bound of array LandVal was changed from 1,1 to 0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '    '    ReDim LandVal(NRgBioType, MxTic)

    '    '    kaatotal = 0
    '    '    relabel = 0



    '    '    ReDim CondTypeFlow(Ncoversite, MxAgeClass, NMapArea, MxTic)
    '    '    ReDim MTypeFlow(NMType, NMLocation, MxTic)

    '    '    '  Evaluate existing stands one at a time

    '    '    kaatotal = 0
    '    '    relabel = 0

    '    '    ' Loop through the set of input data.
    '    '    ' Data is stored in blocks of AA's with multiple files for each block

    '    '    For jaaset = 1 To AAnDataSet

    '    '        ' ''#####polyidDpspace = AAdpSpace(1).polyid
    '    '        kdpspaceAA = 1

    '    '        'Open "debug.txt" For Output As #99
    '    '        For jaa = 1 To AASetNAA(kaaset)
    '    '            ' ''#####polyidDualplan = AA(jaa).polyid

    '    '            If relabel > 4999 Then
    '    '                MainForm.TxtPolyDone.Text = Str(kaatotal)
    '    '                MainForm.TxtPolyDone.Refresh()
    '    '                relabel = 0
    '    '            End If

    '    '            kaatotal = kaatotal + 1
    '    '            relabel = relabel + 1
    '    '            KAA = jaa
    '    '            ReDim MapLayerColor(NMapLayer)
    '    '            For jmap = 1 To NMapLayer
    '    '                'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(jMap). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '                MapLayerColor(jmap) = AA(jaa).MapLayerColor(jmap)
    '    '            Next jmap

    '    '            kmloc = AA(jaa).mloc
    '    '            AAArea = AA(jaa).area

    '    '            mxNPV = -99999
    '    '            mxPreShadPriceNPV = mxNPV
    '    '            BestAApres = -9


    '    '            For j = 1 To AA(KAA).PresArray.Length - 1 'jpresAA = AA(KAA).firstpres To AA(KAA).lastpres
    '    '                jpresAA = AA(jaa).PresArray(j)

    '    '                AANPV = 0
    '    '                PreShadPriceNPV = AANPV
    '    '                '  first add values of all market flows

    '    '                For jflow = AApres(jpresAA).FirstFlow To AApres(jpresAA + 1).FirstFlow - 1
    '    '                    AANPV = AANPV + AAflow(jflow).quan * MtypePr(AAflow(jflow).type, kmloc, AAflow(jflow).tic)
    '    '                Next jflow
    '    '                PreShadPriceNPV = AANPV
    '    '                ' Then add values associated with biological conditions of each map layer
    '    '                For jmap = 1 To NMapLayer
    '    '                    'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '                    'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '                    kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
    '    '                    For jtic = 1 To NTic '#####NTic + 1
    '    '                        AANPV = AANPV + CondTypePr(AApres(jpresAA).Coversite(jtic), AApres(jpresAA).Age(jtic), kMapArea, jtic)
    '    '                    Next jtic
    '    '                Next jmap


    '    '                If AANPV > mxNPV Then
    '    '                    mxNPV = AANPV
    '    '                    mxPreShadPriceNPV = PreShadPriceNPV
    '    '                    BestAApres = jpresAA
    '    '                End If

    '    '            Next j


    '    '            ''kbestdpspace = 0

    '    '            ' '' kw why two blocks now (above and below) -- just determine best alt (kpres&) and sum -- stored same now!
    '    '            ''If kbestdpspace = 0 Then

    '    '            ''    ' Add info for forest-wide totals
    '    '            ''    kpres = BestAApres

    '    '            ''    '  Add Market flows for first rotation

    '    '            ''    For jflow = AApres(kpres).FirstFlow To AApres(kpres + 1).FirstFlow - 1
    '    '            ''        kmtype = AAflow(jflow).type
    '    '            ''        ktic = AAflow(jflow).tic
    '    '            ''        MTypeFlow(kmtype, kmloc, ktic) = MTypeFlow(kmtype, kmloc, ktic) + AAArea * AAflow(jflow).quan
    '    '            ''    Next jflow

    '    '            ''    ' Add Info for biological conditions for each map for first rotation
    '    '            ''    For jmap = 1 To NMapLayer
    '    '            ''        'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ''        'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jMap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ''        kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
    '    '            ''        For jtic = 0 To NTic
    '    '            ''            CondTypeFlow(AApres(kpres).Coversite(jtic), AApres(kpres).Age(jtic), kMapArea, jtic) = CondTypeFlow(AApres(kpres).Coversite(jtic), AApres(kpres).Age(jtic), kMapArea, jtic) + AAArea
    '    '            ''        Next jtic
    '    '            ''    Next jmap


    '    '            ''    ' sum flows from Future Rotations

    '    '            ''End If
    '    '            ' ''Next jaa



























    '    '            '' '' Loop through the set of input data.
    '    '            '' '' Data is stored in blocks of AA's with three files for each block
    '    '            ' ''For jaaset = 1 To AAnDataSet


    '    '            ' ''    'kaaset = jaaset
    '    '            ' ''    ' load the three AA tables for this set

    '    '            ' ''    'ReDim AAflow(AASetNFlow&(kaaset)) As FlowData
    '    '            ' ''    'ReDim AApres(AASetNPres&(kaaset)) As AApresData
    '    '            ' ''    'ReDim AA(AASetNAA&(kaaset)) As AAdata

    '    '            ' ''    'Open AAFlowFN$(kaaset) For Binary Access Read As #4
    '    '            ' ''    'Get #4, , AAflow()
    '    '            ' ''    'Close #4

    '    '            ' ''    'Open AAPresFN$(kaaset) For Binary Access Read As #4
    '    '            ' ''    'Get #4, , AApres()
    '    '            ' ''    'Close #4

    '    '            ' ''    'Open AAFN$(kaaset) For Binary Access Read As #4
    '    '            ' ''    'Get #4, , AA()
    '    '            ' ''    'Close #4

    '    '            ' ''    For jaa = 1 To AASetNAA(kaaset)

    '    '            ' ''        If relabel > 999 Then
    '    '            ' ''            MainForm.TxtPolyDone.Text = Str(kaatotal)
    '    '            ' ''            MainForm.TxtPolyDone.Refresh()
    '    '            ' ''            relabel = 0
    '    '            ' ''        End If


    '    '            ' ''        kaatotal = kaatotal + 1
    '    '            ' ''        relabel = relabel + 1
    '    '            ' ''        KAA = jaa
    '    '            ' ''        'UPGRADE_WARNING: Lower bound of array MapLayerColor was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '    '            ' ''        ReDim MapLayerColor(NMapLayer)
    '    '            ' ''        For jmap = 1 To NMapLayer
    '    '            ' ''            'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(jmap). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ' ''            MapLayerColor(jmap) = AA(jaa).MapLayerColor(jmap)
    '    '            ' ''        Next jmap

    '    '            ' ''        kmloc = AA(jaa).mloc
    '    '            ' ''        AAArea = AA(jaa).area
    '    '            ' ''        'AAage = AA(jaa&).Ageclass
    '    '            ' ''        'kcoversiteAA = AA(jaa&).Coversite

    '    '            ' ''        mxNPV = -99999
    '    '            ' ''        BestAApres = -9
    '    '            ' ''        BestRgOption = -9

    '    '            ' ''        ' move LandVal! Redim outside polygon loop  2/2002
    '    '            ' ''        'ReDim LandVal!(NRgBioType, MxTic)
    '    '            ' ''        'UPGRADE_WARNING: Lower bound of array LandOpt was changed from 1,1 to 0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '    '            ' ''        ReDim LandOpt(NRgBioType, MxTic)

    '    '            ' ''        'set the fixed entry cost for this AA on a per land unit basis
    '    '            ' ''        'ReDim EntryCostThisPoly!(MxTic)
    '    '            ' ''        EntryCostFactor = 1.0# / AAArea
    '    '            ' ''        For jtic = 1 To MxTic
    '    '            ' ''            EntryCostTicThisAA(jtic) = EntryCostTic(kmloc, jtic) * EntryCostFactor
    '    '            ' ''        Next jtic





    '    '            ' ''        For jpresAA = AA(KAA).firstpres To AA(KAA).lastpres

    '    '            ' ''            AANPV = 0
    '    '            ' ''            '  first add values of all market flows

    '    '            ' ''            'change to eliminate aapres().lastflow
    '    '            ' ''            'For jflow& = AApres(jpresAA&).FirstFlow To AApres(jpresAA&).LastFlow
    '    '            ' ''            For jflow = AApres(jpresAA).FirstFlow To AApres(jpresAA + 1).FirstFlow - 1
    '    '            ' ''                AANPV = AANPV + AAflow(jflow).quan * MtypePr(AAflow(jflow).type, kmloc, AAflow(jflow).tic)
    '    '            ' ''            Next jflow

    '    '            ' ''            ' Then add values associated with biological conditions of each map layer
    '    '            ' ''            For jmap = 1 To NMapLayer
    '    '            ' ''                'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ' ''                'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jmap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ' ''                kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
    '    '            ' ''                For jtic = 1 To AApres(jpresAA).RgTic - 1
    '    '            ' ''                    AANPV = AANPV + CondTypePr(AApres(jpresAA).Coversite(jtic), AApres(jpresAA).Age(jtic), kMapArea, jtic)
    '    '            ' ''                Next jtic
    '    '            ' ''            Next jmap



    '    '            ' ''            PresRegTic = AApres(jpresAA).RgTic

    '    '            ' ''            'Add cost of entering AA at end of rotation
    '    '            ' ''            AANPV = AANPV + EntryCostTicThisAA(PresRegTic)


    '    '            ' ''            ' then consider regen options

    '    '            ' ''            For jreg = 1 To AApres(jpresAA).NRgAlt
    '    '            ' ''                kRgBioType = AApres(jpresAA).RgBioType(jreg)
    '    '            ' ''                ' if this bareland type has not been considered for this AA, then
    '    '            ' ''                ' analyze it.

    '    '            ' ''                'If kRgBioType = 13 Then Stop

    '    '            ' ''                If LandOpt(kRgBioType, NTic1) = 0 Then
    '    '            ' ''                    '   initialize best option info assuming one option specified must
    '    '            ' ''                    '   be selected, i.e., no assumed "do nothing"
    '    '            ' ''                    For jtic = 1 To NTic1
    '    '            ' ''                        LandVal(kRgBioType, jtic) = -9999
    '    '            ' ''                    Next jtic

    '    '            ' ''                    '   Determine SEVs (LandVal!())FOR THIS kRgBioType for periods beyond the end
    '    '            ' ''                    '   of the planning horizon.

    '    '            ' ''                    For jpresRG = RgBioType(kRgBioType).firstpres To RgBioType(kRgBioType).lastpres
    '    '            ' ''                        kpresRG = jpresRG
    '    '            ' ''                        rotlen = RgPres(kpresRG).Rotlength

    '    '            ' ''                        NetPV = 0
    '    '            ' ''                        ' Add NPV from market returns
    '    '            ' ''                        For jitem = RgPres(kpresRG).FirstFlow To RgPres(kpresRG).LastFlow
    '    '            ' ''                            kage = RgFlow(jitem).tic
    '    '            ' ''                            kmtype = RgFlow(jitem).type
    '    '            ' ''                            ktic = NTic1 + kage
    '    '            ' ''                            NetPV = NetPV + MtypePr(kmtype, kmloc, ktic) * RgFlow(jitem).quan
    '    '            ' ''                        Next jitem

    '    '            ' ''                        ' Add NPV from conditions for all map layers
    '    '            ' ''                        For jmap = 1 To NMapLayer
    '    '            ' ''                            ktic = NTic + 1
    '    '            ' ''                            'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ' ''                            'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jmap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ' ''                            kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
    '    '            ' ''                            For jage = 1 To rotlen
    '    '            ' ''                                'NetPV! = NetPV! + CondTypePr!(kRgBioType, jage, kMapArea, ktic)
    '    '            ' ''                                NetPV = NetPV + CondTypePr(RgPres(kpresRG).Coversite(jage), RgPres(kpresRG).Age(jage), kMapArea, ktic)
    '    '            ' ''                                ktic = ktic + 1
    '    '            ' ''                            Next jage
    '    '            ' ''                        Next jmap


    '    '            ' ''                        'Add cost of entering AA at end of rotation
    '    '            ' ''                        NetPV = NetPV + EntryCostTicThisAA(NTic1 + rotlen)


    '    '            ' ''                        SEV = NetPV * SEVFactor(rotlen)
    '    '            ' ''                        If SEV > LandVal(kRgBioType, NTic1) Then
    '    '            ' ''                            LandVal(kRgBioType, NTic1) = SEV
    '    '            ' ''                            LandOpt(kRgBioType, NTic1) = jpresRG
    '    '            ' ''                        End If
    '    '            ' ''                    Next jpresRG

    '    '            ' ''                    ' Set LandVal!() for periods beyond NTIC1

    '    '            ' ''                    For jtic = NTic1 + 1 To MxTic
    '    '            ' ''                        LandVal(kRgBioType, jtic) = LandVal(kRgBioType, jtic - 1) * MDisc(2)
    '    '            ' ''                    Next jtic

    '    '            ' ''                    ' DETERMINE LandVal!() FOR EACH Possible TIC STARTING WITH LAST PERIOD

    '    '            ' ''                    FirstTic = AA(KAA).FirstRgTic
    '    '            ' ''                    For jtic = NTic To FirstTic Step -1
    '    '            ' ''                        For jpresRG = RgBioType(kRgBioType).firstpres To RgBioType(kRgBioType).lastpres
    '    '            ' ''                            kpresRG = jpresRG
    '    '            ' ''                            rotlen = RgPres(kpresRG).Rotlength

    '    '            ' ''                            NetPV = 0

    '    '            ' ''                            ' add values for market flows
    '    '            ' ''                            For jitem = RgPres(kpresRG).FirstFlow To RgPres(kpresRG).LastFlow
    '    '            ' ''                                ktime = jtic + RgFlow(jitem).tic
    '    '            ' ''                                kmtype = RgFlow(jitem).type
    '    '            ' ''                                NetPV = NetPV + MtypePr(kmtype, kmloc, ktime) * RgFlow(jitem).quan
    '    '            ' ''                            Next jitem

    '    '            ' ''                            '  Add value for conditions for all map layers
    '    '            ' ''                            For jmap = 1 To NMapLayer
    '    '            ' ''                                'UPGRADE_WARNING: Couldn't resolve default property of object MapLayerColor(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ' ''                                'UPGRADE_WARNING: Couldn't resolve default property of object MapAreaID(jmap, MapLayerColor()). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ' ''                                kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
    '    '            ' ''                                ktic = jtic

    '    '            ' ''                                For jage = 1 To rotlen
    '    '            ' ''                                    'NetPV! = NetPV! + CondTypePr!(kRgBioType, jage, kMapArea, ktic)
    '    '            ' ''                                    NetPV = NetPV + CondTypePr(RgPres(kpresRG).Coversite(jage), RgPres(kpresRG).Age(jage), kMapArea, ktic)
    '    '            ' ''                                    ktic = ktic + 1
    '    '            ' ''                                Next jage
    '    '            ' ''                            Next jmap

    '    '            ' ''                            'Add cost of entering AA at end of rotation
    '    '            ' ''                            NetPV = NetPV + EntryCostTicThisAA(jtic + rotlen)


    '    '            ' ''                            TVAL = NetPV + LandVal(kRgBioType, jtic + rotlen)
    '    '            ' ''                            If TVAL > LandVal(kRgBioType, jtic) Then
    '    '            ' ''                                LandVal(kRgBioType, jtic) = TVAL
    '    '            ' ''                                LandOpt(kRgBioType, jtic) = kpresRG
    '    '            ' ''                            End If
    '    '            ' ''                        Next jpresRG
    '    '            ' ''                    Next jtic
    '    '            ' ''                End If

    '    '            ' ''                regval = AApres(jpresAA).RgCost(jreg) * MtypePr(MTypeSiteConvCost, kmloc, PresRegTic) + LandVal(kRgBioType, PresRegTic)
    '    '            ' ''                If AANPV + regval > mxNPV Then
    '    '            ' ''                    mxNPV = AANPV + regval
    '    '            ' ''                    BestAApres = jpresAA
    '    '            ' ''                    BestRgOption = jreg
    '    '            ' ''                End If
    '    '            ' ''            Next jreg
    '    '            ' ''        Next jpresAA


    '    '            ' ''        ' Add info for forest-wide totals
    '    '            ' ''        kpres = BestAApres
    '    '            ' ''        kregTic = AApres(kpres).RgTic

    '    '            ' ''        'UPGRADE_WARNING: Lower bound of array PolyAge was changed from 0 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '    '            ' ''        ReDim PolyAge(MxTic)
    '    '            ' ''        'UPGRADE_WARNING: Lower bound of array PolyCoversite was changed from 0 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '    '            ' ''        ReDim PolyCoversite(MxTic)


    '    '            ' ''        kTicRg = AApres(kpres).RgTic
    '    '            ' ''        If kTicAA > NTic1 Then kTicAA = NTic1
    '    '            ' ''        For jtic = 0 To kTicRg - 1
    '    '            ' ''            'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ' ''            PolyCoversite(jtic) = AApres(kpres).Coversite(jtic)
    '    '            ' ''            'UPGRADE_WARNING: Couldn't resolve default property of object PolyAge(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ' ''            PolyAge(jtic) = AApres(kpres).Age(jtic)
    '    '            ' ''        Next jtic

    '    '            ' ''        'write #26, "file#, polyid, area, MapColor(1 to nMap)";
    '    '            ' ''        'write #26, "mxnpv, aaprestype filepres# ConvOption# rgtic";
    '    '            ' ''        'write #26, "3 sets Regen:(Rotlength, Prestype, Pres#)";
    '    '            ' ''        'write #26, "Ageclass(tic0 to ntic), Coversite(tic0 to ntic)"

    '    '            ' write polygon info

    '    '            kpres = BestAApres

    '    '            Write(26, kaaset)
    '    '            Write(26, TAB(9), AA(jaa).polyid)
    '    '            Write(26, TAB(21), AA(jaa).area)
    '    '            ktab = 28
    '    '            For jlayer = 1 To 6
    '    '                Write(26, TAB(ktab), AA(jaa).MapLayerColor(jlayer))
    '    '                ktab = ktab + 5
    '    '            Next jlayer
    '    '            Write(26, TAB(ktab + 2), AApres(kpres).BAadjust)
    '    '            Write(26, TAB(ktab + 8), mxNPV)
    '    '            Write(26, mxPreShadPriceNPV)
    '    '            Write(26, AApres(kpres).PresType)
    '    '            Write(26, kpres)
    '    '            Write(26, BestRgOption)
    '    '            Write(26, AApres(kpres).RgTic)
    '    '            ktab = ktab + 58

    '    '            'UPGRADE_WARNING: Lower bound of array PolyAge was changed from 0 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '    '            ReDim PolyAge(MxTic)
    '    '            'UPGRADE_WARNING: Lower bound of array PolyCoversite was changed from 0 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    '    '            ReDim PolyCoversite(MxTic)


    '    '            For jtic = 0 To NTic 'kregTic - 1
    '    '                'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '                PolyCoversite(jtic) = AApres(kpres).Coversite(jtic)
    '    '                'UPGRADE_WARNING: Couldn't resolve default property of object PolyAge(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '                PolyAge(jtic) = AApres(kpres).Age(jtic)
    '    '            Next jtic


    '    '            '' '' set krgbiotype after AA regen
    '    '            ' ''kRgbioNext = AApres(kpres).RgBioType(BestRgOption)
    '    '            ' ''NRgRotation = 0
    '    '            ' ''Do Until kregTic > NTic
    '    '            ' ''    kpres = LandOpt(kRgbioNext, kregTic)
    '    '            ' ''    Write(26, TAB(ktab), RgPres(kpres).Rotlength)
    '    '            ' ''    Write(26, TAB(ktab + 4), RgPres(kpres).PresType)
    '    '            ' ''    Write(26, TAB(ktab + 8), kpres)
    '    '            ' ''    ktab = ktab + 19
    '    '            ' ''    NRgRotation = NRgRotation + 1
    '    '            ' ''    ' Add age/coversite info for this regen rotation

    '    '            ' ''    kTicRg = 0
    '    '            ' ''    For jtic = kregTic To kregTic + RgPres(kpres).Rotlength - 1
    '    '            ' ''        kTicRg = kTicRg + 1
    '    '            ' ''        'UPGRADE_WARNING: Couldn't resolve default property of object PolyAge(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ' ''        PolyAge(jtic) = RgPres(kpres).Age(kTicRg)
    '    '            ' ''        'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(jtic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ' ''        PolyCoversite(jtic) = RgPres(kpres).Coversite(kTicRg)
    '    '            ' ''    Next jtic
    '    '            ' ''    kregTic = kregTic + RgPres(kpres).Rotlength
    '    '            ' ''Loop

    '    '            ' add conversion info for forestwide summary

    '    '            'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(NTic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ConversionArea(PolyCoversite(0), PolyCoversite(NTic)) = ConversionArea(PolyCoversite(0), PolyCoversite(NTic)) + AA(jaa).area
    '    '            'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(NTic). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(0). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            'UPGRADE_WARNING: Couldn't resolve default property of object PolyCoversite(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    '            ConversionPoly(PolyCoversite(0), PolyCoversite(NTic)) = ConversionPoly(PolyCoversite(0), PolyCoversite(NTic)) + 1

    '    '            ' fixed format on output so fill regen fields
    '    '            zero = 0
    '    '            For jrot = NRgRotation + 1 To 3
    '    '                Write(26, zero)
    '    '                Write(26, zero)
    '    '                Write(26, zero)
    '    '                ktab = ktab + 17
    '    '            Next jrot
    '    '            'kTicRg = 0
    '    '            For jtic = 0 To NTic
    '    '                'kTicRg = kTicRg + 1
    '    '                Write(26, PolyAge(jtic))
    '    '                ktab = ktab + 4
    '    '            Next jtic
    '    '            ktab = ktab + 5
    '    '            'kTicRg = 0
    '    '            For jtic = 0 To NTic
    '    '                'kTicRg = kTicRg + 1
    '    '                Write(26, PolyCoversite(jtic))
    '    '                ktab = ktab + 5
    '    '            Next jtic
    '    '            WriteLine(26)
    '    '            'GoTo 366
    '    '        Next jaa


    '    '        If jaaset < AAnDataSet Then

    '    '            kaaset = kaaset + kaasetdirection

    '    '            ReDim AAflow(AASetNFlow(kaaset))
    '    '            'UPGRADE_WARNING: Array AApres may need to have individual elements initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="B97B714D-9338-48AC-B03F-345B617E2B02"'
    '    '            ReDim AApres(AASetNPres(kaaset))
    '    '            'UPGRADE_WARNING: Array AA may need to have individual elements initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="B97B714D-9338-48AC-B03F-345B617E2B02"'
    '    '            ReDim AA(AASetNAA(kaaset))

    '    '            FileOpen(4, AAFlowFN(kaaset), OpenMode.Binary, OpenAccess.Read)
    '    '            'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '    '            FileGet(4, AAflow)
    '    '            FileClose(4)

    '    '            '8/18/08  - need to upgrade this get info.
    '    '            ''FileOpen(4, AAPresFN(kaaset), OpenMode.Binary, OpenAccess.Read)
    '    '            'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '    '            FileGet(4, AApres)
    '    '            FileClose(4)

    '    '            FileOpen(4, AAFN(kaaset), OpenMode.Binary, OpenAccess.Read)
    '    '            'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
    '    '            FileGet(4, AA)
    '    '            FileClose(4)

    '    '        End If
    '    '    Next jaaset
    '    '    kaasetdirection = kaasetdirection * (-1)

    '    '    FileClose(26)

    '    '    '366: ktab = 35
    '    '    ktab = 35
    '    '    Write(36, "Coversite", TAB(ktab))

    '    '    For jto = 0 To Ncoversite
    '    '        ktab = ktab + 6
    '    '        Write(36, jto, TAB(ktab))
    '    '    Next jto
    '    '    WriteLine(36, "total")
    '    '    For jfrom = 1 To Ncoversite
    '    '        ktab = 35
    '    '        Write(36, coversiteLabel(jfrom), TAB(ktab))
    '    '        TotalConvert = 0
    '    '        For jto = 0 To Ncoversite
    '    '            ktab = ktab + 6
    '    '            TotalConvert = TotalConvert + ConversionPoly(jfrom, jto)
    '    '            Write(36, ConversionPoly(jfrom, jto), TAB(ktab))
    '    '        Next jto
    '    '        WriteLine(36, TotalConvert)
    '    '    Next jfrom
    '    '    WriteLine(36)


    '    '    ktab = 35
    '    '    Write(36, "Coversite", TAB(ktab))

    '    '    For jto = 0 To Ncoversite
    '    '        ktab = ktab + 6
    '    '        Write(36, jto, TAB(ktab))
    '    '    Next jto
    '    '    WriteLine(36, "total")
    '    '    For jfrom = 1 To Ncoversite
    '    '        ktab = 35
    '    '        Write(36, coversiteLabel(jfrom), TAB(ktab))
    '    '        TotalConvert = 0
    '    '        For jto = 0 To Ncoversite
    '    '            ktab = ktab + 6
    '    '            intConvert = ConversionArea(jfrom, jto)
    '    '            TotalConvert = TotalConvert + intConvert
    '    '            Write(36, intConvert, TAB(ktab))
    '    '        Next jto
    '    '        WriteLine(36, TotalConvert)
    '    '    Next jfrom


    '    '    FileClose(36)

    '    '    MainForm.TxtPolyDone.Text = Str(kaatotal)
    '    '    MainForm.Label1.Text = "Finished writing AApoly file"
    '    '    MainForm.Label1.Refresh()
    '    'End Sub

End Class


