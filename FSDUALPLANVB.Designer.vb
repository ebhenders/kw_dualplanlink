<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class MainForm
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents OK As System.Windows.Forms.Button
	Public WithEvents ComboBoxIteration As System.Windows.Forms.ComboBox
	Public WithEvents TxtPolyDone As System.Windows.Forms.TextBox
	Public WithEvents Txtmore As System.Windows.Forms.TextBox
	Public WithEvents TxtIter As System.Windows.Forms.TextBox
	Public CommonDialog1Open As System.Windows.Forms.OpenFileDialog
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents LblPolyDone As System.Windows.Forms.Label
	Public WithEvents Lblmore As System.Windows.Forms.Label
	Public WithEvents Lbliter As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents mnuWriteIn As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuWriteForestwide As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuWritePolySched As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuQuit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnufile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuEditPr As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuAdjParameters As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuWriteSpread As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuReadSpread As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuedit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuCondSets As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuCondTypes As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuMSets As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuMtypes As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuResults As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MnuRunContinue As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuRunFloat As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuRunSmooth As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuRunShape As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuRgLand As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MnuRun As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.OK = New System.Windows.Forms.Button
        Me.ComboBoxIteration = New System.Windows.Forms.ComboBox
        Me.TxtPolyDone = New System.Windows.Forms.TextBox
        Me.Txtmore = New System.Windows.Forms.TextBox
        Me.TxtIter = New System.Windows.Forms.TextBox
        Me.CommonDialog1Open = New System.Windows.Forms.OpenFileDialog
        Me.Label2 = New System.Windows.Forms.Label
        Me.LblPolyDone = New System.Windows.Forms.Label
        Me.Lblmore = New System.Windows.Forms.Label
        Me.Lbliter = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.MainMenu1 = New System.Windows.Forms.MenuStrip
        Me.mnufile = New System.Windows.Forms.ToolStripMenuItem
        Me.LoadMainInputFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuWriteIn = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuWriteForestwide = New System.Windows.Forms.ToolStripMenuItem
        Me.WritePolygonSpatialFlowsToFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuWritePolySched = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuQuit = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuedit = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEditPr = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAdjParameters = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuWriteSpread = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuReadSpread = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuResults = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuCondSets = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuCondTypes = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuMSets = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuMtypes = New System.Windows.Forms.ToolStripMenuItem
        Me.MnuRun = New System.Windows.Forms.ToolStripMenuItem
        Me.MnuRunContinue = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRunFloat = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRunSmooth = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRunShape = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRgLand = New System.Windows.Forms.ToolStripMenuItem
        Me.CreateGridForTrimmerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MainMenu1.SuspendLayout()
        Me.SuspendLayout()
        '
        'OK
        '
        Me.OK.BackColor = System.Drawing.SystemColors.Control
        Me.OK.Cursor = System.Windows.Forms.Cursors.Default
        Me.OK.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.OK.Location = New System.Drawing.Point(261, 329)
        Me.OK.Name = "OK"
        Me.OK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.OK.Size = New System.Drawing.Size(49, 33)
        Me.OK.TabIndex = 9
        Me.OK.Text = "OK"
        Me.OK.UseVisualStyleBackColor = False
        '
        'ComboBoxIteration
        '
        Me.ComboBoxIteration.BackColor = System.Drawing.SystemColors.Window
        Me.ComboBoxIteration.Cursor = System.Windows.Forms.Cursors.Default
        Me.ComboBoxIteration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxIteration.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxIteration.ForeColor = System.Drawing.SystemColors.WindowText
        Me.ComboBoxIteration.Location = New System.Drawing.Point(109, 329)
        Me.ComboBoxIteration.Name = "ComboBoxIteration"
        Me.ComboBoxIteration.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ComboBoxIteration.Size = New System.Drawing.Size(137, 22)
        Me.ComboBoxIteration.TabIndex = 8
        '
        'TxtPolyDone
        '
        Me.TxtPolyDone.AcceptsReturn = True
        Me.TxtPolyDone.BackColor = System.Drawing.SystemColors.Window
        Me.TxtPolyDone.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TxtPolyDone.Font = New System.Drawing.Font("Arial", 13.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtPolyDone.ForeColor = System.Drawing.SystemColors.WindowText
        Me.TxtPolyDone.Location = New System.Drawing.Point(296, 105)
        Me.TxtPolyDone.MaxLength = 0
        Me.TxtPolyDone.Name = "TxtPolyDone"
        Me.TxtPolyDone.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TxtPolyDone.Size = New System.Drawing.Size(97, 28)
        Me.TxtPolyDone.TabIndex = 5
        '
        'Txtmore
        '
        Me.Txtmore.AcceptsReturn = True
        Me.Txtmore.BackColor = System.Drawing.SystemColors.Window
        Me.Txtmore.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Txtmore.Font = New System.Drawing.Font("Arial", 13.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txtmore.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Txtmore.Location = New System.Drawing.Point(280, 193)
        Me.Txtmore.MaxLength = 0
        Me.Txtmore.Name = "Txtmore"
        Me.Txtmore.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Txtmore.Size = New System.Drawing.Size(73, 28)
        Me.Txtmore.TabIndex = 4
        '
        'TxtIter
        '
        Me.TxtIter.AcceptsReturn = True
        Me.TxtIter.BackColor = System.Drawing.SystemColors.Window
        Me.TxtIter.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TxtIter.Font = New System.Drawing.Font("Arial", 13.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtIter.ForeColor = System.Drawing.SystemColors.WindowText
        Me.TxtIter.Location = New System.Drawing.Point(216, 41)
        Me.TxtIter.MaxLength = 0
        Me.TxtIter.Name = "TxtIter"
        Me.TxtIter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TxtIter.Size = New System.Drawing.Size(89, 28)
        Me.TxtIter.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.Font = New System.Drawing.Font("Arial", 13.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(104, 285)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(81, 41)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Status:"
        '
        'LblPolyDone
        '
        Me.LblPolyDone.BackColor = System.Drawing.SystemColors.Control
        Me.LblPolyDone.Cursor = System.Windows.Forms.Cursors.Default
        Me.LblPolyDone.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPolyDone.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LblPolyDone.Location = New System.Drawing.Point(104, 105)
        Me.LblPolyDone.Name = "LblPolyDone"
        Me.LblPolyDone.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LblPolyDone.Size = New System.Drawing.Size(177, 49)
        Me.LblPolyDone.TabIndex = 6
        Me.LblPolyDone.Text = "Polygons completed this iteration"
        '
        'Lblmore
        '
        Me.Lblmore.BackColor = System.Drawing.SystemColors.Control
        Me.Lblmore.Cursor = System.Windows.Forms.Cursors.Default
        Me.Lblmore.Font = New System.Drawing.Font("Arial", 13.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lblmore.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Lblmore.Location = New System.Drawing.Point(80, 201)
        Me.Lblmore.Name = "Lblmore"
        Me.Lblmore.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Lblmore.Size = New System.Drawing.Size(201, 25)
        Me.Lblmore.TabIndex = 2
        Me.Lblmore.Text = "Will stop after iteration #"
        '
        'Lbliter
        '
        Me.Lbliter.BackColor = System.Drawing.SystemColors.Control
        Me.Lbliter.Cursor = System.Windows.Forms.Cursors.Default
        Me.Lbliter.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbliter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Lbliter.Location = New System.Drawing.Point(104, 49)
        Me.Lbliter.Name = "Lbliter"
        Me.Lbliter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Lbliter.Size = New System.Drawing.Size(105, 25)
        Me.Lbliter.TabIndex = 1
        Me.Lbliter.Text = "Iteration #"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Font = New System.Drawing.Font("Arial", 13.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(192, 285)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(489, 41)
        Me.Label1.TabIndex = 0
        '
        'MainMenu1
        '
        Me.MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnufile, Me.mnuedit, Me.mnuResults, Me.MnuRun})
        Me.MainMenu1.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu1.Name = "MainMenu1"
        Me.MainMenu1.Size = New System.Drawing.Size(540, 24)
        Me.MainMenu1.TabIndex = 10
        '
        'mnufile
        '
        Me.mnufile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LoadMainInputFileToolStripMenuItem, Me.mnuWriteIn, Me.mnuWriteForestwide, Me.WritePolygonSpatialFlowsToFileToolStripMenuItem, Me.mnuWritePolySched, Me.mnuQuit})
        Me.mnufile.Name = "mnufile"
        Me.mnufile.Size = New System.Drawing.Size(35, 20)
        Me.mnufile.Text = "&File"
        '
        'LoadMainInputFileToolStripMenuItem
        '
        Me.LoadMainInputFileToolStripMenuItem.Name = "LoadMainInputFileToolStripMenuItem"
        Me.LoadMainInputFileToolStripMenuItem.Size = New System.Drawing.Size(251, 22)
        Me.LoadMainInputFileToolStripMenuItem.Text = "Load Main Input File"
        '
        'mnuWriteIn
        '
        Me.mnuWriteIn.Name = "mnuWriteIn"
        Me.mnuWriteIn.Size = New System.Drawing.Size(251, 22)
        Me.mnuWriteIn.Text = "Write New &InputFile"
        '
        'mnuWriteForestwide
        '
        Me.mnuWriteForestwide.Name = "mnuWriteForestwide"
        Me.mnuWriteForestwide.Size = New System.Drawing.Size(251, 22)
        Me.mnuWriteForestwide.Text = "Write &Forestwide Results To File"
        '
        'WritePolygonSpatialFlowsToFileToolStripMenuItem
        '
        Me.WritePolygonSpatialFlowsToFileToolStripMenuItem.Name = "WritePolygonSpatialFlowsToFileToolStripMenuItem"
        Me.WritePolygonSpatialFlowsToFileToolStripMenuItem.Size = New System.Drawing.Size(251, 22)
        Me.WritePolygonSpatialFlowsToFileToolStripMenuItem.Text = "Write Polygon Spatial Flows to File"
        '
        'mnuWritePolySched
        '
        Me.mnuWritePolySched.Name = "mnuWritePolySched"
        Me.mnuWritePolySched.Size = New System.Drawing.Size(251, 22)
        Me.mnuWritePolySched.Text = "Write All &Polygon Schedules To File"
        '
        'mnuQuit
        '
        Me.mnuQuit.Name = "mnuQuit"
        Me.mnuQuit.Size = New System.Drawing.Size(251, 22)
        Me.mnuQuit.Text = "&Quit"
        '
        'mnuedit
        '
        Me.mnuedit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEditPr, Me.mnuAdjParameters, Me.mnuWriteSpread, Me.mnuReadSpread})
        Me.mnuedit.Name = "mnuedit"
        Me.mnuedit.Size = New System.Drawing.Size(37, 20)
        Me.mnuedit.Text = "&Edit"
        '
        'mnuEditPr
        '
        Me.mnuEditPr.Name = "mnuEditPr"
        Me.mnuEditPr.Size = New System.Drawing.Size(277, 22)
        Me.mnuEditPr.Text = "Manually Adjust Shadow Prices"
        '
        'mnuAdjParameters
        '
        Me.mnuAdjParameters.Name = "mnuAdjParameters"
        Me.mnuAdjParameters.Size = New System.Drawing.Size(277, 22)
        Me.mnuAdjParameters.Text = "&Adjustment Parameters for Shad. Prices"
        '
        'mnuWriteSpread
        '
        Me.mnuWriteSpread.Name = "mnuWriteSpread"
        Me.mnuWriteSpread.Size = New System.Drawing.Size(277, 22)
        Me.mnuWriteSpread.Text = "Write Spreadsheet For Input Edit"
        '
        'mnuReadSpread
        '
        Me.mnuReadSpread.Name = "mnuReadSpread"
        Me.mnuReadSpread.Size = New System.Drawing.Size(277, 22)
        Me.mnuReadSpread.Text = "Read SpreadSheet as Input"
        '
        'mnuResults
        '
        Me.mnuResults.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCondSets, Me.mnuCondTypes, Me.mnuMSets, Me.mnuMtypes})
        Me.mnuResults.Name = "mnuResults"
        Me.mnuResults.Size = New System.Drawing.Size(57, 20)
        Me.mnuResults.Text = " &Results"
        '
        'mnuCondSets
        '
        Me.mnuCondSets.Name = "mnuCondSets"
        Me.mnuCondSets.Size = New System.Drawing.Size(162, 22)
        Me.mnuCondSets.Text = "&ConditionSets"
        '
        'mnuCondTypes
        '
        Me.mnuCondTypes.Name = "mnuCondTypes"
        Me.mnuCondTypes.Size = New System.Drawing.Size(162, 22)
        Me.mnuCondTypes.Text = "Condition &Types"
        '
        'mnuMSets
        '
        Me.mnuMSets.Name = "mnuMSets"
        Me.mnuMSets.Size = New System.Drawing.Size(162, 22)
        Me.mnuMSets.Text = "&Market Sets"
        '
        'mnuMtypes
        '
        Me.mnuMtypes.Name = "mnuMtypes"
        Me.mnuMtypes.Size = New System.Drawing.Size(162, 22)
        Me.mnuMtypes.Text = "Mar&ket Types"
        '
        'MnuRun
        '
        Me.MnuRun.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuRunContinue, Me.mnuRunFloat, Me.mnuRunSmooth, Me.mnuRunShape, Me.mnuRgLand, Me.CreateGridForTrimmerToolStripMenuItem})
        Me.MnuRun.Name = "MnuRun"
        Me.MnuRun.Size = New System.Drawing.Size(38, 20)
        Me.MnuRun.Text = "&Run"
        '
        'MnuRunContinue
        '
        Me.MnuRunContinue.Name = "MnuRunContinue"
        Me.MnuRunContinue.Size = New System.Drawing.Size(251, 22)
        Me.MnuRunContinue.Text = "Schedule using automated Process"
        '
        'mnuRunFloat
        '
        Me.mnuRunFloat.Name = "mnuRunFloat"
        Me.mnuRunFloat.Size = New System.Drawing.Size(251, 22)
        Me.mnuRunFloat.Text = "A Float Iteration"
        '
        'mnuRunSmooth
        '
        Me.mnuRunSmooth.Name = "mnuRunSmooth"
        Me.mnuRunSmooth.Size = New System.Drawing.Size(251, 22)
        Me.mnuRunSmooth.Text = "A Smooth Iteration"
        '
        'mnuRunShape
        '
        Me.mnuRunShape.Name = "mnuRunShape"
        Me.mnuRunShape.Size = New System.Drawing.Size(251, 22)
        Me.mnuRunShape.Text = "A Shape Iteration"
        '
        'mnuRgLand
        '
        Me.mnuRgLand.Name = "mnuRgLand"
        Me.mnuRgLand.Size = New System.Drawing.Size(251, 22)
        Me.mnuRgLand.Text = "Estimate ""Bare"" Land Values "
        '
        'CreateGridForTrimmerToolStripMenuItem
        '
        Me.CreateGridForTrimmerToolStripMenuItem.Name = "CreateGridForTrimmerToolStripMenuItem"
        Me.CreateGridForTrimmerToolStripMenuItem.Size = New System.Drawing.Size(251, 22)
        Me.CreateGridForTrimmerToolStripMenuItem.Text = "Create Grid For Trimmer"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(540, 377)
        Me.Controls.Add(Me.OK)
        Me.Controls.Add(Me.ComboBoxIteration)
        Me.Controls.Add(Me.TxtPolyDone)
        Me.Controls.Add(Me.Txtmore)
        Me.Controls.Add(Me.TxtIter)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.LblPolyDone)
        Me.Controls.Add(Me.Lblmore)
        Me.Controls.Add(Me.Lbliter)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MainMenu1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Location = New System.Drawing.Point(11, 51)
        Me.Name = "MainForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "DualPlan"
        Me.MainMenu1.ResumeLayout(False)
        Me.MainMenu1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LoadMainInputFileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WritePolygonSpatialFlowsToFileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CreateGridForTrimmerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
#End Region 
End Class