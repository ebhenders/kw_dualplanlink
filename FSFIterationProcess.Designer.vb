<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class IterSteps
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents tbDlPlItersNoTrim As System.Windows.Forms.TextBox
    Public WithEvents txtHalfLife As System.Windows.Forms.TextBox
    Public WithEvents txtrewrite As System.Windows.Forms.TextBox
    Public WithEvents txtNFloat As System.Windows.Forms.TextBox
    Public WithEvents cmdItrPrcApply As System.Windows.Forms.Button
    Public WithEvents cmdItrPrcCancel As System.Windows.Forms.Button
    Public WithEvents txthowmanymore As System.Windows.Forms.TextBox
    Public WithEvents txtNShape As System.Windows.Forms.TextBox
    Public WithEvents txtNSmooth As System.Windows.Forms.TextBox
    Public WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents Label2 As System.Windows.Forms.Label
    Public WithEvents Lblrewrite As System.Windows.Forms.Label
    Public WithEvents lblHowManyIterations As System.Windows.Forms.Label
    Public WithEvents lblNShape As System.Windows.Forms.Label
    Public WithEvents lblNsmooth As System.Windows.Forms.Label
    Public WithEvents lblNFloat As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tbFlopConMaxMult = New System.Windows.Forms.TextBox
        Me.tbMaxBreaks = New System.Windows.Forms.TextBox
        Me.tbBackFlopFactor = New System.Windows.Forms.TextBox
        Me.tbDlPlItersNoTrim = New System.Windows.Forms.TextBox
        Me.txtHalfLife = New System.Windows.Forms.TextBox
        Me.txtrewrite = New System.Windows.Forms.TextBox
        Me.txtNFloat = New System.Windows.Forms.TextBox
        Me.cmdItrPrcApply = New System.Windows.Forms.Button
        Me.cmdItrPrcCancel = New System.Windows.Forms.Button
        Me.txthowmanymore = New System.Windows.Forms.TextBox
        Me.txtNShape = New System.Windows.Forms.TextBox
        Me.txtNSmooth = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Lblrewrite = New System.Windows.Forms.Label
        Me.lblHowManyIterations = New System.Windows.Forms.Label
        Me.lblNShape = New System.Windows.Forms.Label
        Me.lblNsmooth = New System.Windows.Forms.Label
        Me.lblNFloat = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.tbDPSpaceAfterDlPlIters = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.tb1optStop = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.tbTrimIzoneControls = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.tbTrimTol = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.tbMxPresPerPoly = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.tbMxPresUnchosen = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.tbNumDPCycles = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.tbMaxPresChosen = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.tbDeviationWriteFile = New System.Windows.Forms.TextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.cbKeepBestNS = New System.Windows.Forms.CheckBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.cbUseMSetViolations = New System.Windows.Forms.CheckBox
        Me.cbUseCSetViolations = New System.Windows.Forms.CheckBox
        Me.cbUseSSetViolations = New System.Windows.Forms.CheckBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cbUseSSetExcesses = New System.Windows.Forms.CheckBox
        Me.cbUseCSetExcesses = New System.Windows.Forms.CheckBox
        Me.cbUseMSetExcesses = New System.Windows.Forms.CheckBox
        Me.rbClosestVal = New System.Windows.Forms.RadioButton
        Me.rbHighestVal = New System.Windows.Forms.RadioButton
        Me.Label15 = New System.Windows.Forms.Label
        Me.tbCycleHalfLife = New System.Windows.Forms.TextBox
        Me.tbSchedLockCycles = New System.Windows.Forms.TextBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.tbDirections = New System.Windows.Forms.TextBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.tbFootprintPct = New System.Windows.Forms.TextBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.tbUnschedLockCycles = New System.Windows.Forms.TextBox
        Me.tbIterationNum = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.btnSchedShadPrice = New System.Windows.Forms.Button
        Me.cbLockExactSDSol = New System.Windows.Forms.CheckBox
        Me.rbOnlyFirstDPRx = New System.Windows.Forms.RadioButton
        Me.cbReportByOrigSD = New System.Windows.Forms.CheckBox
        Me.cbReportByPredefSD = New System.Windows.Forms.CheckBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.tbWindowSizeFactor = New System.Windows.Forms.TextBox
        Me.tbWindowMaxSize = New System.Windows.Forms.TextBox
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label26 = New System.Windows.Forms.Label
        Me.cbOneOptAfterDP = New System.Windows.Forms.CheckBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.cbRandomOneOpt = New System.Windows.Forms.CheckBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.tbTrimSpeed = New System.Windows.Forms.TextBox
        Me.Label27 = New System.Windows.Forms.Label
        Me.cbGridBuilder = New System.Windows.Forms.CheckBox
        Me.tbDollarPerAcreTol = New System.Windows.Forms.TextBox
        Me.Label28 = New System.Windows.Forms.Label
        Me.Button3 = New System.Windows.Forms.Button
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label30 = New System.Windows.Forms.Label
        Me.cbSmoothSideSFlopControl = New System.Windows.Forms.CheckBox
        Me.cbSmoothBackSFlopControl = New System.Windows.Forms.CheckBox
        Me.tbItersBetweenTrims = New System.Windows.Forms.TextBox
        Me.Label31 = New System.Windows.Forms.Label
        Me.rbAbsDev = New System.Windows.Forms.RadioButton
        Me.rbPctDev = New System.Windows.Forms.RadioButton
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbFlopConMaxMult
        '
        Me.tbFlopConMaxMult.AcceptsReturn = True
        Me.tbFlopConMaxMult.BackColor = System.Drawing.SystemColors.Window
        Me.tbFlopConMaxMult.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbFlopConMaxMult.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFlopConMaxMult.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbFlopConMaxMult.Location = New System.Drawing.Point(288, 204)
        Me.tbFlopConMaxMult.MaxLength = 0
        Me.tbFlopConMaxMult.Name = "tbFlopConMaxMult"
        Me.tbFlopConMaxMult.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbFlopConMaxMult.Size = New System.Drawing.Size(41, 22)
        Me.tbFlopConMaxMult.TabIndex = 106
        Me.tbFlopConMaxMult.Text = "5"
        Me.ToolTip1.SetToolTip(Me.tbFlopConMaxMult, "Max adjustment factor considered")
        '
        'tbMaxBreaks
        '
        Me.tbMaxBreaks.AcceptsReturn = True
        Me.tbMaxBreaks.BackColor = System.Drawing.SystemColors.Window
        Me.tbMaxBreaks.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbMaxBreaks.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMaxBreaks.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbMaxBreaks.Location = New System.Drawing.Point(241, 217)
        Me.tbMaxBreaks.MaxLength = 0
        Me.tbMaxBreaks.Name = "tbMaxBreaks"
        Me.tbMaxBreaks.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbMaxBreaks.Size = New System.Drawing.Size(41, 22)
        Me.tbMaxBreaks.TabIndex = 105
        Me.tbMaxBreaks.Text = "40"
        Me.ToolTip1.SetToolTip(Me.tbMaxBreaks, "Total Number of Class Breaks")
        '
        'tbBackFlopFactor
        '
        Me.tbBackFlopFactor.AcceptsReturn = True
        Me.tbBackFlopFactor.BackColor = System.Drawing.SystemColors.Window
        Me.tbBackFlopFactor.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbBackFlopFactor.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbBackFlopFactor.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbBackFlopFactor.Location = New System.Drawing.Point(241, 193)
        Me.tbBackFlopFactor.MaxLength = 0
        Me.tbBackFlopFactor.Name = "tbBackFlopFactor"
        Me.tbBackFlopFactor.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbBackFlopFactor.Size = New System.Drawing.Size(41, 22)
        Me.tbBackFlopFactor.TabIndex = 102
        Me.tbBackFlopFactor.Text = ".9"
        Me.ToolTip1.SetToolTip(Me.tbBackFlopFactor, "Reduces adjustment for many flops, increases without flops")
        '
        'tbDlPlItersNoTrim
        '
        Me.tbDlPlItersNoTrim.AcceptsReturn = True
        Me.tbDlPlItersNoTrim.BackColor = System.Drawing.SystemColors.Window
        Me.tbDlPlItersNoTrim.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbDlPlItersNoTrim.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDlPlItersNoTrim.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbDlPlItersNoTrim.Location = New System.Drawing.Point(325, 321)
        Me.tbDlPlItersNoTrim.MaxLength = 0
        Me.tbDlPlItersNoTrim.Name = "tbDlPlItersNoTrim"
        Me.tbDlPlItersNoTrim.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbDlPlItersNoTrim.Size = New System.Drawing.Size(41, 20)
        Me.tbDlPlItersNoTrim.TabIndex = 15
        Me.tbDlPlItersNoTrim.Text = "0"
        '
        'txtHalfLife
        '
        Me.txtHalfLife.AcceptsReturn = True
        Me.txtHalfLife.BackColor = System.Drawing.SystemColors.Window
        Me.txtHalfLife.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHalfLife.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHalfLife.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtHalfLife.Location = New System.Drawing.Point(296, 269)
        Me.txtHalfLife.MaxLength = 0
        Me.txtHalfLife.Name = "txtHalfLife"
        Me.txtHalfLife.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHalfLife.Size = New System.Drawing.Size(41, 22)
        Me.txtHalfLife.TabIndex = 14
        '
        'txtrewrite
        '
        Me.txtrewrite.AcceptsReturn = True
        Me.txtrewrite.BackColor = System.Drawing.SystemColors.Window
        Me.txtrewrite.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtrewrite.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtrewrite.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtrewrite.Location = New System.Drawing.Point(296, 245)
        Me.txtrewrite.MaxLength = 0
        Me.txtrewrite.Name = "txtrewrite"
        Me.txtrewrite.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtrewrite.Size = New System.Drawing.Size(41, 22)
        Me.txtrewrite.TabIndex = 10
        '
        'txtNFloat
        '
        Me.txtNFloat.AcceptsReturn = True
        Me.txtNFloat.BackColor = System.Drawing.SystemColors.Window
        Me.txtNFloat.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNFloat.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNFloat.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNFloat.Location = New System.Drawing.Point(291, 101)
        Me.txtNFloat.MaxLength = 0
        Me.txtNFloat.Name = "txtNFloat"
        Me.txtNFloat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNFloat.Size = New System.Drawing.Size(41, 22)
        Me.txtNFloat.TabIndex = 9
        '
        'cmdItrPrcApply
        '
        Me.cmdItrPrcApply.BackColor = System.Drawing.SystemColors.Control
        Me.cmdItrPrcApply.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdItrPrcApply.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdItrPrcApply.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdItrPrcApply.Location = New System.Drawing.Point(255, 621)
        Me.cmdItrPrcApply.Name = "cmdItrPrcApply"
        Me.cmdItrPrcApply.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdItrPrcApply.Size = New System.Drawing.Size(81, 40)
        Me.cmdItrPrcApply.TabIndex = 8
        Me.cmdItrPrcApply.Text = "&Start Run"
        Me.cmdItrPrcApply.UseVisualStyleBackColor = False
        '
        'cmdItrPrcCancel
        '
        Me.cmdItrPrcCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdItrPrcCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdItrPrcCancel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdItrPrcCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdItrPrcCancel.Location = New System.Drawing.Point(346, 621)
        Me.cmdItrPrcCancel.Name = "cmdItrPrcCancel"
        Me.cmdItrPrcCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdItrPrcCancel.Size = New System.Drawing.Size(81, 40)
        Me.cmdItrPrcCancel.TabIndex = 7
        Me.cmdItrPrcCancel.Text = "&Cancel"
        Me.cmdItrPrcCancel.UseVisualStyleBackColor = False
        '
        'txthowmanymore
        '
        Me.txthowmanymore.AcceptsReturn = True
        Me.txthowmanymore.BackColor = System.Drawing.SystemColors.Window
        Me.txthowmanymore.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txthowmanymore.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txthowmanymore.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txthowmanymore.Location = New System.Drawing.Point(315, 11)
        Me.txthowmanymore.MaxLength = 0
        Me.txthowmanymore.Name = "txthowmanymore"
        Me.txthowmanymore.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txthowmanymore.Size = New System.Drawing.Size(60, 22)
        Me.txthowmanymore.TabIndex = 6
        Me.txthowmanymore.Text = "2"
        '
        'txtNShape
        '
        Me.txtNShape.AcceptsReturn = True
        Me.txtNShape.BackColor = System.Drawing.SystemColors.Window
        Me.txtNShape.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNShape.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNShape.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNShape.Location = New System.Drawing.Point(291, 125)
        Me.txtNShape.MaxLength = 0
        Me.txtNShape.Name = "txtNShape"
        Me.txtNShape.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNShape.Size = New System.Drawing.Size(41, 22)
        Me.txtNShape.TabIndex = 5
        Me.txtNShape.Text = "2"
        '
        'txtNSmooth
        '
        Me.txtNSmooth.AcceptsReturn = True
        Me.txtNSmooth.BackColor = System.Drawing.SystemColors.Window
        Me.txtNSmooth.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNSmooth.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNSmooth.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNSmooth.Location = New System.Drawing.Point(291, 149)
        Me.txtNSmooth.MaxLength = 0
        Me.txtNSmooth.Name = "txtNSmooth"
        Me.txtNSmooth.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNSmooth.Size = New System.Drawing.Size(41, 22)
        Me.txtNSmooth.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(56, 321)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(263, 17)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "DualPlan 1-opt Move iterations per set"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(8, 269)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(281, 34)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Half-life of Adjustment Parameters (# Iterations)            Value of 0 indicates" & _
            " no decay"
        '
        'Lblrewrite
        '
        Me.Lblrewrite.BackColor = System.Drawing.SystemColors.Control
        Me.Lblrewrite.Cursor = System.Windows.Forms.Cursors.Default
        Me.Lblrewrite.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lblrewrite.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Lblrewrite.Location = New System.Drawing.Point(6, 250)
        Me.Lblrewrite.Name = "Lblrewrite"
        Me.Lblrewrite.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Lblrewrite.Size = New System.Drawing.Size(281, 17)
        Me.Lblrewrite.TabIndex = 11
        Me.Lblrewrite.Text = "Number of iterations before writing new input file"
        '
        'lblHowManyIterations
        '
        Me.lblHowManyIterations.BackColor = System.Drawing.SystemColors.Control
        Me.lblHowManyIterations.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHowManyIterations.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHowManyIterations.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHowManyIterations.Location = New System.Drawing.Point(12, 12)
        Me.lblHowManyIterations.Name = "lblHowManyIterations"
        Me.lblHowManyIterations.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHowManyIterations.Size = New System.Drawing.Size(304, 18)
        Me.lblHowManyIterations.TabIndex = 3
        Me.lblHowManyIterations.Text = "How many iterations before stopping"
        '
        'lblNShape
        '
        Me.lblNShape.BackColor = System.Drawing.SystemColors.Control
        Me.lblNShape.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNShape.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNShape.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNShape.Location = New System.Drawing.Point(3, 129)
        Me.lblNShape.Name = "lblNShape"
        Me.lblNShape.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNShape.Size = New System.Drawing.Size(281, 18)
        Me.lblNShape.TabIndex = 2
        Me.lblNShape.Text = "Number of Shape iterations per iteration set"
        '
        'lblNsmooth
        '
        Me.lblNsmooth.BackColor = System.Drawing.SystemColors.Control
        Me.lblNsmooth.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNsmooth.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNsmooth.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNsmooth.Location = New System.Drawing.Point(3, 153)
        Me.lblNsmooth.Name = "lblNsmooth"
        Me.lblNsmooth.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNsmooth.Size = New System.Drawing.Size(291, 18)
        Me.lblNsmooth.TabIndex = 1
        Me.lblNsmooth.Text = "Number of Smooth iterations after each other iteration type"
        '
        'lblNFloat
        '
        Me.lblNFloat.BackColor = System.Drawing.SystemColors.Control
        Me.lblNFloat.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNFloat.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNFloat.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNFloat.Location = New System.Drawing.Point(3, 105)
        Me.lblNFloat.Name = "lblNFloat"
        Me.lblNFloat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNFloat.Size = New System.Drawing.Size(291, 18)
        Me.lblNFloat.TabIndex = 0
        Me.lblNFloat.Text = "Number of Float iterations per iteration set"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label6.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(55, 400)
        Me.Label6.Name = "Label6"
        Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label6.Size = New System.Drawing.Size(244, 37)
        Me.Label6.TabIndex = 23
        Me.Label6.Text = "Number of DPSpace Iterations after DualPlan iteration sets"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Cooper Black", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 78)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(228, 19)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "Price Adjustment Controls"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Cooper Black", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(108, 302)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(176, 19)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Scheduling Controls"
        '
        'tbDPSpaceAfterDlPlIters
        '
        Me.tbDPSpaceAfterDlPlIters.AcceptsReturn = True
        Me.tbDPSpaceAfterDlPlIters.BackColor = System.Drawing.SystemColors.Window
        Me.tbDPSpaceAfterDlPlIters.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbDPSpaceAfterDlPlIters.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDPSpaceAfterDlPlIters.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbDPSpaceAfterDlPlIters.Location = New System.Drawing.Point(322, 400)
        Me.tbDPSpaceAfterDlPlIters.MaxLength = 0
        Me.tbDPSpaceAfterDlPlIters.Name = "tbDPSpaceAfterDlPlIters"
        Me.tbDPSpaceAfterDlPlIters.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbDPSpaceAfterDlPlIters.Size = New System.Drawing.Size(41, 20)
        Me.tbDPSpaceAfterDlPlIters.TabIndex = 26
        Me.tbDPSpaceAfterDlPlIters.Text = "1"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.Control
        Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label7.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(101, 338)
        Me.Label7.Name = "Label7"
        Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label7.Size = New System.Drawing.Size(186, 31)
        Me.Label7.TabIndex = 27
        Me.Label7.Text = "1-opt Stop percentage (<1) [reschedules relative to # of stands]"
        '
        'tb1optStop
        '
        Me.tb1optStop.AcceptsReturn = True
        Me.tb1optStop.BackColor = System.Drawing.SystemColors.Window
        Me.tb1optStop.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tb1optStop.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb1optStop.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tb1optStop.Location = New System.Drawing.Point(287, 347)
        Me.tb1optStop.MaxLength = 0
        Me.tb1optStop.Name = "tb1optStop"
        Me.tb1optStop.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tb1optStop.Size = New System.Drawing.Size(41, 20)
        Me.tb1optStop.TabIndex = 28
        Me.tb1optStop.Text = ".002"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Cooper Black", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(12, 478)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(221, 19)
        Me.Label8.TabIndex = 31
        Me.Label8.Text = "Trimmer Controls For DP"
        '
        'tbTrimIzoneControls
        '
        Me.tbTrimIzoneControls.AcceptsReturn = True
        Me.tbTrimIzoneControls.BackColor = System.Drawing.SystemColors.Window
        Me.tbTrimIzoneControls.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbTrimIzoneControls.Enabled = False
        Me.tbTrimIzoneControls.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbTrimIzoneControls.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbTrimIzoneControls.Location = New System.Drawing.Point(280, 503)
        Me.tbTrimIzoneControls.MaxLength = 0
        Me.tbTrimIzoneControls.Name = "tbTrimIzoneControls"
        Me.tbTrimIzoneControls.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbTrimIzoneControls.Size = New System.Drawing.Size(99, 20)
        Me.tbTrimIzoneControls.TabIndex = 29
        Me.tbTrimIzoneControls.Text = "0"
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.Control
        Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label9.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(11, 503)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label9.Size = New System.Drawing.Size(272, 31)
        Me.Label9.TabIndex = 30
        Me.Label9.Text = "Trim IZones with More than ""X"" Prescription combos: (0 means don't trim)"
        '
        'tbTrimTol
        '
        Me.tbTrimTol.AcceptsReturn = True
        Me.tbTrimTol.BackColor = System.Drawing.SystemColors.Window
        Me.tbTrimTol.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbTrimTol.Enabled = False
        Me.tbTrimTol.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbTrimTol.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbTrimTol.Location = New System.Drawing.Point(280, 537)
        Me.tbTrimTol.MaxLength = 0
        Me.tbTrimTol.Name = "tbTrimTol"
        Me.tbTrimTol.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbTrimTol.Size = New System.Drawing.Size(33, 20)
        Me.tbTrimTol.TabIndex = 32
        Me.tbTrimTol.Text = ".00"
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.SystemColors.Control
        Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label10.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label10.Location = New System.Drawing.Point(11, 537)
        Me.Label10.Name = "Label10"
        Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label10.Size = New System.Drawing.Size(272, 31)
        Me.Label10.TabIndex = 33
        Me.Label10.Text = "Trim prescriptions that aren't at least this percent (<1) higher than NPVLbnd:"
        '
        'tbMxPresPerPoly
        '
        Me.tbMxPresPerPoly.AcceptsReturn = True
        Me.tbMxPresPerPoly.BackColor = System.Drawing.SystemColors.Window
        Me.tbMxPresPerPoly.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbMxPresPerPoly.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbMxPresPerPoly.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMxPresPerPoly.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbMxPresPerPoly.Location = New System.Drawing.Point(254, 568)
        Me.tbMxPresPerPoly.MaxLength = 0
        Me.tbMxPresPerPoly.Name = "tbMxPresPerPoly"
        Me.tbMxPresPerPoly.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbMxPresPerPoly.Size = New System.Drawing.Size(33, 20)
        Me.tbMxPresPerPoly.TabIndex = 34
        Me.tbMxPresPerPoly.Text = "10"
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.Control
        Me.Label11.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label11.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label11.Location = New System.Drawing.Point(11, 568)
        Me.Label11.Name = "Label11"
        Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label11.Size = New System.Drawing.Size(272, 20)
        Me.Label11.TabIndex = 35
        Me.Label11.Text = "Max Prescriptions Per Poly (0 is unconstrained)"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Cooper Black", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(435, 212)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(250, 19)
        Me.Label12.TabIndex = 36
        Me.Label12.Text = "DP Cycle Controls (Optional)"
        '
        'tbMxPresUnchosen
        '
        Me.tbMxPresUnchosen.AcceptsReturn = True
        Me.tbMxPresUnchosen.BackColor = System.Drawing.SystemColors.Window
        Me.tbMxPresUnchosen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbMxPresUnchosen.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbMxPresUnchosen.Enabled = False
        Me.tbMxPresUnchosen.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMxPresUnchosen.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbMxPresUnchosen.Location = New System.Drawing.Point(683, 411)
        Me.tbMxPresUnchosen.MaxLength = 0
        Me.tbMxPresUnchosen.Name = "tbMxPresUnchosen"
        Me.tbMxPresUnchosen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbMxPresUnchosen.Size = New System.Drawing.Size(33, 20)
        Me.tbMxPresUnchosen.TabIndex = 40
        Me.tbMxPresUnchosen.Text = "5"
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.SystemColors.Control
        Me.Label13.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label13.Enabled = False
        Me.Label13.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label13.Location = New System.Drawing.Point(461, 411)
        Me.Label13.Name = "Label13"
        Me.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label13.Size = New System.Drawing.Size(205, 32)
        Me.Label13.TabIndex = 41
        Me.Label13.Text = "Max Number of Rx Per Unscheduled Poly (0 is ALL are trimmed)"
        '
        'tbNumDPCycles
        '
        Me.tbNumDPCycles.AcceptsReturn = True
        Me.tbNumDPCycles.BackColor = System.Drawing.SystemColors.Window
        Me.tbNumDPCycles.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbNumDPCycles.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNumDPCycles.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbNumDPCycles.Location = New System.Drawing.Point(683, 280)
        Me.tbNumDPCycles.MaxLength = 0
        Me.tbNumDPCycles.Name = "tbNumDPCycles"
        Me.tbNumDPCycles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbNumDPCycles.Size = New System.Drawing.Size(33, 20)
        Me.tbNumDPCycles.TabIndex = 38
        Me.tbNumDPCycles.Text = "0"
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.SystemColors.Control
        Me.Label14.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label14.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label14.Location = New System.Drawing.Point(461, 280)
        Me.Label14.Name = "Label14"
        Me.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label14.Size = New System.Drawing.Size(123, 20)
        Me.Label14.TabIndex = 39
        Me.Label14.Text = "Number of DP Cycles"
        '
        'tbMaxPresChosen
        '
        Me.tbMaxPresChosen.AcceptsReturn = True
        Me.tbMaxPresChosen.BackColor = System.Drawing.SystemColors.Window
        Me.tbMaxPresChosen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbMaxPresChosen.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbMaxPresChosen.Enabled = False
        Me.tbMaxPresChosen.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMaxPresChosen.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.tbMaxPresChosen.Location = New System.Drawing.Point(683, 304)
        Me.tbMaxPresChosen.MaxLength = 0
        Me.tbMaxPresChosen.Name = "tbMaxPresChosen"
        Me.tbMaxPresChosen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbMaxPresChosen.Size = New System.Drawing.Size(33, 20)
        Me.tbMaxPresChosen.TabIndex = 46
        Me.tbMaxPresChosen.Text = "2"
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.SystemColors.Control
        Me.Label16.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label16.Enabled = False
        Me.Label16.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label16.Location = New System.Drawing.Point(461, 306)
        Me.Label16.Name = "Label16"
        Me.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label16.Size = New System.Drawing.Size(218, 18)
        Me.Label16.TabIndex = 47
        Me.Label16.Text = "Max Number of Rx Per Poly in Solution"
        '
        'tbDeviationWriteFile
        '
        Me.tbDeviationWriteFile.AcceptsReturn = True
        Me.tbDeviationWriteFile.BackColor = System.Drawing.SystemColors.Window
        Me.tbDeviationWriteFile.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbDeviationWriteFile.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDeviationWriteFile.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbDeviationWriteFile.Location = New System.Drawing.Point(24, 47)
        Me.tbDeviationWriteFile.MaxLength = 0
        Me.tbDeviationWriteFile.Name = "tbDeviationWriteFile"
        Me.tbDeviationWriteFile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbDeviationWriteFile.Size = New System.Drawing.Size(68, 22)
        Me.tbDeviationWriteFile.TabIndex = 48
        Me.tbDeviationWriteFile.Text = "0"
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.SystemColors.Control
        Me.Label17.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label17.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label17.Location = New System.Drawing.Point(3, 18)
        Me.Label17.Name = "Label17"
        Me.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label17.Size = New System.Drawing.Size(276, 17)
        Me.Label17.TabIndex = 49
        Me.Label17.Text = "Deviation threshold for writing new input files:"
        '
        'cbKeepBestNS
        '
        Me.cbKeepBestNS.AutoSize = True
        Me.cbKeepBestNS.Checked = True
        Me.cbKeepBestNS.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbKeepBestNS.Enabled = False
        Me.cbKeepBestNS.Location = New System.Drawing.Point(525, 321)
        Me.cbKeepBestNS.Name = "cbKeepBestNS"
        Me.cbKeepBestNS.Size = New System.Drawing.Size(141, 18)
        Me.cbKeepBestNS.TabIndex = 50
        Me.cbKeepBestNS.Text = "Include best Non-spatial"
        Me.cbKeepBestNS.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.SystemColors.Control
        Me.Label18.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label18.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label18.Location = New System.Drawing.Point(17, 89)
        Me.Label18.Name = "Label18"
        Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label18.Size = New System.Drawing.Size(242, 17)
        Me.Label18.TabIndex = 51
        Me.Label18.Text = "Includes:"
        '
        'cbUseMSetViolations
        '
        Me.cbUseMSetViolations.AutoSize = True
        Me.cbUseMSetViolations.Location = New System.Drawing.Point(38, 109)
        Me.cbUseMSetViolations.Name = "cbUseMSetViolations"
        Me.cbUseMSetViolations.Size = New System.Drawing.Size(100, 18)
        Me.cbUseMSetViolations.TabIndex = 52
        Me.cbUseMSetViolations.Text = "MSet Violations"
        Me.cbUseMSetViolations.UseVisualStyleBackColor = True
        '
        'cbUseCSetViolations
        '
        Me.cbUseCSetViolations.AutoSize = True
        Me.cbUseCSetViolations.Checked = True
        Me.cbUseCSetViolations.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbUseCSetViolations.Location = New System.Drawing.Point(38, 133)
        Me.cbUseCSetViolations.Name = "cbUseCSetViolations"
        Me.cbUseCSetViolations.Size = New System.Drawing.Size(99, 18)
        Me.cbUseCSetViolations.TabIndex = 53
        Me.cbUseCSetViolations.Text = "CSet Violations"
        Me.cbUseCSetViolations.UseVisualStyleBackColor = True
        '
        'cbUseSSetViolations
        '
        Me.cbUseSSetViolations.AutoSize = True
        Me.cbUseSSetViolations.Location = New System.Drawing.Point(38, 157)
        Me.cbUseSSetViolations.Name = "cbUseSSetViolations"
        Me.cbUseSSetViolations.Size = New System.Drawing.Size(99, 18)
        Me.cbUseSSetViolations.TabIndex = 54
        Me.cbUseSSetViolations.Text = "SSet Violations"
        Me.cbUseSSetViolations.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbPctDev)
        Me.GroupBox1.Controls.Add(Me.rbAbsDev)
        Me.GroupBox1.Controls.Add(Me.cbUseSSetExcesses)
        Me.GroupBox1.Controls.Add(Me.cbUseCSetExcesses)
        Me.GroupBox1.Controls.Add(Me.cbUseMSetExcesses)
        Me.GroupBox1.Controls.Add(Me.cbUseSSetViolations)
        Me.GroupBox1.Controls.Add(Me.cbUseCSetViolations)
        Me.GroupBox1.Controls.Add(Me.cbUseMSetViolations)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.tbDeviationWriteFile)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Location = New System.Drawing.Point(440, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(292, 185)
        Me.GroupBox1.TabIndex = 55
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Deviation Based Output Controls"
        '
        'cbUseSSetExcesses
        '
        Me.cbUseSSetExcesses.AutoSize = True
        Me.cbUseSSetExcesses.Location = New System.Drawing.Point(145, 157)
        Me.cbUseSSetExcesses.Name = "cbUseSSetExcesses"
        Me.cbUseSSetExcesses.Size = New System.Drawing.Size(100, 18)
        Me.cbUseSSetExcesses.TabIndex = 57
        Me.cbUseSSetExcesses.Text = "SSet Excesses"
        Me.cbUseSSetExcesses.UseVisualStyleBackColor = True
        '
        'cbUseCSetExcesses
        '
        Me.cbUseCSetExcesses.AutoSize = True
        Me.cbUseCSetExcesses.Checked = True
        Me.cbUseCSetExcesses.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbUseCSetExcesses.Location = New System.Drawing.Point(145, 133)
        Me.cbUseCSetExcesses.Name = "cbUseCSetExcesses"
        Me.cbUseCSetExcesses.Size = New System.Drawing.Size(100, 18)
        Me.cbUseCSetExcesses.TabIndex = 56
        Me.cbUseCSetExcesses.Text = "CSet Excesses"
        Me.cbUseCSetExcesses.UseVisualStyleBackColor = True
        '
        'cbUseMSetExcesses
        '
        Me.cbUseMSetExcesses.AutoSize = True
        Me.cbUseMSetExcesses.Location = New System.Drawing.Point(144, 109)
        Me.cbUseMSetExcesses.Name = "cbUseMSetExcesses"
        Me.cbUseMSetExcesses.Size = New System.Drawing.Size(101, 18)
        Me.cbUseMSetExcesses.TabIndex = 55
        Me.cbUseMSetExcesses.Text = "MSet Excesses"
        Me.cbUseMSetExcesses.UseVisualStyleBackColor = True
        '
        'rbClosestVal
        '
        Me.rbClosestVal.AutoSize = True
        Me.rbClosestVal.Location = New System.Drawing.Point(639, 340)
        Me.rbClosestVal.Name = "rbClosestVal"
        Me.rbClosestVal.Size = New System.Drawing.Size(92, 18)
        Me.rbClosestVal.TabIndex = 57
        Me.rbClosestVal.Text = "Closest Value"
        Me.rbClosestVal.UseVisualStyleBackColor = True
        '
        'rbHighestVal
        '
        Me.rbHighestVal.AutoSize = True
        Me.rbHighestVal.Location = New System.Drawing.Point(540, 340)
        Me.rbHighestVal.Name = "rbHighestVal"
        Me.rbHighestVal.Size = New System.Drawing.Size(92, 18)
        Me.rbHighestVal.TabIndex = 56
        Me.rbHighestVal.Text = "Highest Value"
        Me.rbHighestVal.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.SystemColors.Control
        Me.Label15.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label15.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label15.Location = New System.Drawing.Point(475, 361)
        Me.Label15.Name = "Label15"
        Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label15.Size = New System.Drawing.Size(241, 17)
        Me.Label15.TabIndex = 59
        Me.Label15.Text = "Half-life of Max Rx  (# Cycles) (0 is no decay)"
        '
        'tbCycleHalfLife
        '
        Me.tbCycleHalfLife.AcceptsReturn = True
        Me.tbCycleHalfLife.BackColor = System.Drawing.SystemColors.Window
        Me.tbCycleHalfLife.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbCycleHalfLife.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCycleHalfLife.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbCycleHalfLife.Location = New System.Drawing.Point(725, 353)
        Me.tbCycleHalfLife.MaxLength = 0
        Me.tbCycleHalfLife.Name = "tbCycleHalfLife"
        Me.tbCycleHalfLife.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbCycleHalfLife.Size = New System.Drawing.Size(33, 20)
        Me.tbCycleHalfLife.TabIndex = 60
        Me.tbCycleHalfLife.Text = "0"
        '
        'tbSchedLockCycles
        '
        Me.tbSchedLockCycles.AcceptsReturn = True
        Me.tbSchedLockCycles.BackColor = System.Drawing.SystemColors.Window
        Me.tbSchedLockCycles.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbSchedLockCycles.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSchedLockCycles.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbSchedLockCycles.Location = New System.Drawing.Point(725, 379)
        Me.tbSchedLockCycles.MaxLength = 0
        Me.tbSchedLockCycles.Name = "tbSchedLockCycles"
        Me.tbSchedLockCycles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbSchedLockCycles.Size = New System.Drawing.Size(33, 20)
        Me.tbSchedLockCycles.TabIndex = 62
        Me.tbSchedLockCycles.Text = "0"
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.SystemColors.Control
        Me.Label19.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label19.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label19.Location = New System.Drawing.Point(475, 382)
        Me.Label19.Name = "Label19"
        Me.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label19.Size = New System.Drawing.Size(244, 17)
        Me.Label19.TabIndex = 61
        Me.Label19.Text = "Lock Rx after X Cycles no change (0 is no lock)"
        '
        'tbDirections
        '
        Me.tbDirections.AcceptsReturn = True
        Me.tbDirections.BackColor = System.Drawing.SystemColors.Window
        Me.tbDirections.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbDirections.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDirections.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbDirections.Location = New System.Drawing.Point(486, 496)
        Me.tbDirections.MaxLength = 0
        Me.tbDirections.Name = "tbDirections"
        Me.tbDirections.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbDirections.Size = New System.Drawing.Size(193, 20)
        Me.tbDirections.TabIndex = 64
        Me.tbDirections.Text = "2,3,4,5,6,7,8,1"
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.Control
        Me.Label20.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label20.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label20.Location = New System.Drawing.Point(461, 463)
        Me.Label20.Name = "Label20"
        Me.Label20.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label20.Size = New System.Drawing.Size(270, 30)
        Me.Label20.TabIndex = 65
        Me.Label20.Text = "DP Directional Search Series (comma-separated, 0 is default in file)"
        '
        'tbFootprintPct
        '
        Me.tbFootprintPct.AcceptsReturn = True
        Me.tbFootprintPct.BackColor = System.Drawing.SystemColors.Window
        Me.tbFootprintPct.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbFootprintPct.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbFootprintPct.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbFootprintPct.Location = New System.Drawing.Point(250, 598)
        Me.tbFootprintPct.MaxLength = 0
        Me.tbFootprintPct.Name = "tbFootprintPct"
        Me.tbFootprintPct.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbFootprintPct.Size = New System.Drawing.Size(33, 20)
        Me.tbFootprintPct.TabIndex = 70
        Me.tbFootprintPct.Text = "1"
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.SystemColors.Control
        Me.Label23.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label23.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label23.Location = New System.Drawing.Point(43, 598)
        Me.Label23.Name = "Label23"
        Me.Label23.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label23.Size = New System.Drawing.Size(190, 20)
        Me.Label23.TabIndex = 71
        Me.Label23.Text = "Percent (1=100%) value of footprint"
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.SystemColors.Control
        Me.Label24.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label24.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label24.Location = New System.Drawing.Point(472, 443)
        Me.Label24.Name = "Label24"
        Me.Label24.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label24.Size = New System.Drawing.Size(244, 17)
        Me.Label24.TabIndex = 72
        Me.Label24.Text = "Lock Rx after X Cycles no change (0 is no lock)"
        '
        'tbUnschedLockCycles
        '
        Me.tbUnschedLockCycles.AcceptsReturn = True
        Me.tbUnschedLockCycles.BackColor = System.Drawing.SystemColors.Window
        Me.tbUnschedLockCycles.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbUnschedLockCycles.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbUnschedLockCycles.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbUnschedLockCycles.Location = New System.Drawing.Point(725, 440)
        Me.tbUnschedLockCycles.MaxLength = 0
        Me.tbUnschedLockCycles.Name = "tbUnschedLockCycles"
        Me.tbUnschedLockCycles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbUnschedLockCycles.Size = New System.Drawing.Size(33, 20)
        Me.tbUnschedLockCycles.TabIndex = 73
        Me.tbUnschedLockCycles.Text = "0"
        '
        'tbIterationNum
        '
        Me.tbIterationNum.AcceptsReturn = True
        Me.tbIterationNum.BackColor = System.Drawing.SystemColors.Window
        Me.tbIterationNum.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbIterationNum.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbIterationNum.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbIterationNum.Location = New System.Drawing.Point(224, 40)
        Me.tbIterationNum.MaxLength = 0
        Me.tbIterationNum.Name = "tbIterationNum"
        Me.tbIterationNum.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbIterationNum.Size = New System.Drawing.Size(60, 22)
        Me.tbIterationNum.TabIndex = 75
        Me.tbIterationNum.Text = "1"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(11, 44)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(225, 18)
        Me.Label4.TabIndex = 74
        Me.Label4.Text = "Starting Iteration Number"
        '
        'btnSchedShadPrice
        '
        Me.btnSchedShadPrice.Location = New System.Drawing.Point(304, 43)
        Me.btnSchedShadPrice.Name = "btnSchedShadPrice"
        Me.btnSchedShadPrice.Size = New System.Drawing.Size(119, 38)
        Me.btnSchedShadPrice.TabIndex = 76
        Me.btnSchedShadPrice.Text = "Load Schedule and Shadow Price Files"
        Me.btnSchedShadPrice.UseVisualStyleBackColor = True
        '
        'cbLockExactSDSol
        '
        Me.cbLockExactSDSol.AutoSize = True
        Me.cbLockExactSDSol.Checked = True
        Me.cbLockExactSDSol.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbLockExactSDSol.Location = New System.Drawing.Point(440, 234)
        Me.cbLockExactSDSol.Name = "cbLockExactSDSol"
        Me.cbLockExactSDSol.Size = New System.Drawing.Size(184, 18)
        Me.cbLockExactSDSol.TabIndex = 81
        Me.cbLockExactSDSol.Text = "Lock Exact Subdivision Solutions"
        Me.cbLockExactSDSol.UseVisualStyleBackColor = True
        '
        'rbOnlyFirstDPRx
        '
        Me.rbOnlyFirstDPRx.AutoSize = True
        Me.rbOnlyFirstDPRx.Checked = True
        Me.rbOnlyFirstDPRx.Location = New System.Drawing.Point(442, 340)
        Me.rbOnlyFirstDPRx.Name = "rbOnlyFirstDPRx"
        Me.rbOnlyFirstDPRx.Size = New System.Drawing.Size(97, 18)
        Me.rbOnlyFirstDPRx.TabIndex = 83
        Me.rbOnlyFirstDPRx.TabStop = True
        Me.rbOnlyFirstDPRx.Text = "Only 1st DP Rx"
        Me.rbOnlyFirstDPRx.UseVisualStyleBackColor = True
        '
        'cbReportByOrigSD
        '
        Me.cbReportByOrigSD.AutoSize = True
        Me.cbReportByOrigSD.Checked = True
        Me.cbReportByOrigSD.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbReportByOrigSD.Location = New System.Drawing.Point(440, 258)
        Me.cbReportByOrigSD.Name = "cbReportByOrigSD"
        Me.cbReportByOrigSD.Size = New System.Drawing.Size(154, 18)
        Me.cbReportByOrigSD.TabIndex = 82
        Me.cbReportByOrigSD.Text = "Report by Original Subdivs"
        Me.cbReportByOrigSD.UseVisualStyleBackColor = True
        '
        'cbReportByPredefSD
        '
        Me.cbReportByPredefSD.AutoSize = True
        Me.cbReportByPredefSD.Location = New System.Drawing.Point(588, 258)
        Me.cbReportByPredefSD.Name = "cbReportByPredefSD"
        Me.cbReportByPredefSD.Size = New System.Drawing.Size(170, 18)
        Me.cbReportByPredefSD.TabIndex = 84
        Me.cbReportByPredefSD.Text = "Report by Predefined Subdivs"
        Me.cbReportByPredefSD.UseVisualStyleBackColor = True
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.SystemColors.Control
        Me.Label21.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label21.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label21.Location = New System.Drawing.Point(457, 519)
        Me.Label21.Name = "Label21"
        Me.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label21.Size = New System.Drawing.Size(270, 14)
        Me.Label21.TabIndex = 85
        Me.Label21.Text = "Window Size Expansion Factor and Max Size"
        '
        'tbWindowSizeFactor
        '
        Me.tbWindowSizeFactor.AcceptsReturn = True
        Me.tbWindowSizeFactor.BackColor = System.Drawing.SystemColors.Window
        Me.tbWindowSizeFactor.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbWindowSizeFactor.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbWindowSizeFactor.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbWindowSizeFactor.Location = New System.Drawing.Point(460, 536)
        Me.tbWindowSizeFactor.MaxLength = 0
        Me.tbWindowSizeFactor.Name = "tbWindowSizeFactor"
        Me.tbWindowSizeFactor.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbWindowSizeFactor.Size = New System.Drawing.Size(79, 20)
        Me.tbWindowSizeFactor.TabIndex = 86
        Me.tbWindowSizeFactor.Text = "0"
        '
        'tbWindowMaxSize
        '
        Me.tbWindowMaxSize.AcceptsReturn = True
        Me.tbWindowMaxSize.BackColor = System.Drawing.SystemColors.Window
        Me.tbWindowMaxSize.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbWindowMaxSize.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbWindowMaxSize.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbWindowMaxSize.Location = New System.Drawing.Point(551, 536)
        Me.tbWindowMaxSize.MaxLength = 0
        Me.tbWindowMaxSize.Name = "tbWindowMaxSize"
        Me.tbWindowMaxSize.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbWindowMaxSize.Size = New System.Drawing.Size(181, 20)
        Me.tbWindowMaxSize.TabIndex = 87
        Me.tbWindowMaxSize.Text = "30000000"
        '
        'Label25
        '
        Me.Label25.BackColor = System.Drawing.SystemColors.Control
        Me.Label25.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label25.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label25.Location = New System.Drawing.Point(451, 559)
        Me.Label25.Name = "Label25"
        Me.Label25.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label25.Size = New System.Drawing.Size(91, 33)
        Me.Label25.TabIndex = 88
        Me.Label25.Text = "Factor <10 or States to add"
        '
        'Label26
        '
        Me.Label26.BackColor = System.Drawing.SystemColors.Control
        Me.Label26.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label26.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label26.Location = New System.Drawing.Point(548, 559)
        Me.Label26.Name = "Label26"
        Me.Label26.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label26.Size = New System.Drawing.Size(183, 12)
        Me.Label26.TabIndex = 89
        Me.Label26.Text = "Max Size"
        '
        'cbOneOptAfterDP
        '
        Me.cbOneOptAfterDP.AutoSize = True
        Me.cbOneOptAfterDP.Location = New System.Drawing.Point(134, 425)
        Me.cbOneOptAfterDP.Name = "cbOneOptAfterDP"
        Me.cbOneOptAfterDP.Size = New System.Drawing.Size(153, 18)
        Me.cbOneOptAfterDP.TabIndex = 90
        Me.cbOneOptAfterDP.Text = "Do 1-opt After DP/Cycles?"
        Me.cbOneOptAfterDP.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(614, 560)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(102, 39)
        Me.Button1.TabIndex = 91
        Me.Button1.Text = "Load File With Defs"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'cbRandomOneOpt
        '
        Me.cbRandomOneOpt.AutoSize = True
        Me.cbRandomOneOpt.Location = New System.Drawing.Point(133, 372)
        Me.cbRandomOneOpt.Name = "cbRandomOneOpt"
        Me.cbRandomOneOpt.Size = New System.Drawing.Size(154, 18)
        Me.cbRandomOneOpt.TabIndex = 92
        Me.cbRandomOneOpt.Text = "Randomize 1-opt Iterations"
        Me.cbRandomOneOpt.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(89, 621)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(102, 39)
        Me.Button2.TabIndex = 94
        Me.Button2.Text = "File for Trim and Cycle Controls"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'tbTrimSpeed
        '
        Me.tbTrimSpeed.AcceptsReturn = True
        Me.tbTrimSpeed.BackColor = System.Drawing.SystemColors.Window
        Me.tbTrimSpeed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbTrimSpeed.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbTrimSpeed.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbTrimSpeed.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbTrimSpeed.Location = New System.Drawing.Point(333, 538)
        Me.tbTrimSpeed.MaxLength = 0
        Me.tbTrimSpeed.Name = "tbTrimSpeed"
        Me.tbTrimSpeed.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbTrimSpeed.Size = New System.Drawing.Size(33, 20)
        Me.tbTrimSpeed.TabIndex = 95
        Me.tbTrimSpeed.Text = "1"
        '
        'Label27
        '
        Me.Label27.BackColor = System.Drawing.SystemColors.Control
        Me.Label27.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label27.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label27.Location = New System.Drawing.Point(322, 526)
        Me.Label27.Name = "Label27"
        Me.Label27.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label27.Size = New System.Drawing.Size(66, 15)
        Me.Label27.TabIndex = 96
        Me.Label27.Text = "Trim Speed"
        '
        'cbGridBuilder
        '
        Me.cbGridBuilder.AutoSize = True
        Me.cbGridBuilder.Location = New System.Drawing.Point(112, 581)
        Me.cbGridBuilder.Name = "cbGridBuilder"
        Me.cbGridBuilder.Size = New System.Drawing.Size(140, 18)
        Me.cbGridBuilder.TabIndex = 97
        Me.cbGridBuilder.Text = "Use Grids to Build Rxs?"
        Me.cbGridBuilder.UseVisualStyleBackColor = True
        '
        'tbDollarPerAcreTol
        '
        Me.tbDollarPerAcreTol.AcceptsReturn = True
        Me.tbDollarPerAcreTol.BackColor = System.Drawing.SystemColors.Window
        Me.tbDollarPerAcreTol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbDollarPerAcreTol.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbDollarPerAcreTol.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDollarPerAcreTol.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbDollarPerAcreTol.Location = New System.Drawing.Point(394, 568)
        Me.tbDollarPerAcreTol.MaxLength = 0
        Me.tbDollarPerAcreTol.Name = "tbDollarPerAcreTol"
        Me.tbDollarPerAcreTol.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbDollarPerAcreTol.Size = New System.Drawing.Size(33, 20)
        Me.tbDollarPerAcreTol.TabIndex = 98
        Me.tbDollarPerAcreTol.Text = "5"
        '
        'Label28
        '
        Me.Label28.BackColor = System.Drawing.SystemColors.Control
        Me.Label28.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label28.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label28.Location = New System.Drawing.Point(379, 537)
        Me.Label28.Name = "Label28"
        Me.Label28.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label28.Size = New System.Drawing.Size(66, 31)
        Me.Label28.TabIndex = 99
        Me.Label28.Text = "$/Ac For Grid"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(337, 197)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(56, 38)
        Me.Button3.TabIndex = 107
        Me.Button3.Text = "Show Curve"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label29
        '
        Me.Label29.BackColor = System.Drawing.SystemColors.Control
        Me.Label29.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label29.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label29.Location = New System.Drawing.Point(287, 176)
        Me.Label29.Name = "Label29"
        Me.Label29.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label29.Size = New System.Drawing.Size(53, 14)
        Me.Label29.TabIndex = 104
        Me.Label29.Text = "Max Mult:"
        '
        'Label30
        '
        Me.Label30.BackColor = System.Drawing.SystemColors.Control
        Me.Label30.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label30.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label30.Location = New System.Drawing.Point(238, 176)
        Me.Label30.Name = "Label30"
        Me.Label30.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label30.Size = New System.Drawing.Size(50, 14)
        Me.Label30.TabIndex = 100
        Me.Label30.Text = "Factor:"
        '
        'cbSmoothSideSFlopControl
        '
        Me.cbSmoothSideSFlopControl.AutoSize = True
        Me.cbSmoothSideSFlopControl.Location = New System.Drawing.Point(14, 217)
        Me.cbSmoothSideSFlopControl.Name = "cbSmoothSideSFlopControl"
        Me.cbSmoothSideSFlopControl.Size = New System.Drawing.Size(217, 18)
        Me.cbSmoothSideSFlopControl.TabIndex = 103
        Me.cbSmoothSideSFlopControl.Text = "Use Smooth "" Sideways S"" Flop Control"
        Me.cbSmoothSideSFlopControl.UseVisualStyleBackColor = True
        '
        'cbSmoothBackSFlopControl
        '
        Me.cbSmoothBackSFlopControl.AutoSize = True
        Me.cbSmoothBackSFlopControl.Location = New System.Drawing.Point(14, 193)
        Me.cbSmoothBackSFlopControl.Name = "cbSmoothBackSFlopControl"
        Me.cbSmoothBackSFlopControl.Size = New System.Drawing.Size(221, 18)
        Me.cbSmoothBackSFlopControl.TabIndex = 101
        Me.cbSmoothBackSFlopControl.Text = "Use Smooth ""Backwards S"" Flop Control"
        Me.cbSmoothBackSFlopControl.UseVisualStyleBackColor = True
        '
        'tbItersBetweenTrims
        '
        Me.tbItersBetweenTrims.AcceptsReturn = True
        Me.tbItersBetweenTrims.BackColor = System.Drawing.SystemColors.Window
        Me.tbItersBetweenTrims.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbItersBetweenTrims.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbItersBetweenTrims.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbItersBetweenTrims.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tbItersBetweenTrims.Location = New System.Drawing.Point(315, 579)
        Me.tbItersBetweenTrims.MaxLength = 0
        Me.tbItersBetweenTrims.Name = "tbItersBetweenTrims"
        Me.tbItersBetweenTrims.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tbItersBetweenTrims.Size = New System.Drawing.Size(33, 20)
        Me.tbItersBetweenTrims.TabIndex = 108
        Me.tbItersBetweenTrims.Text = "1"
        '
        'Label31
        '
        Me.Label31.BackColor = System.Drawing.SystemColors.Control
        Me.Label31.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label31.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label31.Location = New System.Drawing.Point(296, 561)
        Me.Label31.Name = "Label31"
        Me.Label31.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label31.Size = New System.Drawing.Size(89, 15)
        Me.Label31.TabIndex = 109
        Me.Label31.Text = "Iters Btwn Trims"
        '
        'rbAbsDev
        '
        Me.rbAbsDev.AutoSize = True
        Me.rbAbsDev.Location = New System.Drawing.Point(103, 38)
        Me.rbAbsDev.Name = "rbAbsDev"
        Me.rbAbsDev.Size = New System.Drawing.Size(121, 18)
        Me.rbAbsDev.TabIndex = 58
        Me.rbAbsDev.Text = "Absolute Deviations"
        Me.rbAbsDev.UseVisualStyleBackColor = True
        '
        'rbPctDev
        '
        Me.rbPctDev.AutoSize = True
        Me.rbPctDev.Checked = True
        Me.rbPctDev.Location = New System.Drawing.Point(103, 62)
        Me.rbPctDev.Name = "rbPctDev"
        Me.rbPctDev.Size = New System.Drawing.Size(182, 18)
        Me.rbPctDev.TabIndex = 59
        Me.rbPctDev.TabStop = True
        Me.rbPctDev.Text = "Percent of Primal Deviations (<1)"
        Me.rbPctDev.UseVisualStyleBackColor = True
        '
        'IterSteps
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(761, 674)
        Me.Controls.Add(Me.tbItersBetweenTrims)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.tbFlopConMaxMult)
        Me.Controls.Add(Me.tbMaxBreaks)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.cbSmoothSideSFlopControl)
        Me.Controls.Add(Me.tbBackFlopFactor)
        Me.Controls.Add(Me.cbSmoothBackSFlopControl)
        Me.Controls.Add(Me.tbDollarPerAcreTol)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.tbFootprintPct)
        Me.Controls.Add(Me.cbGridBuilder)
        Me.Controls.Add(Me.tbTrimSpeed)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.cbRandomOneOpt)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.cbOneOptAfterDP)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.tbWindowMaxSize)
        Me.Controls.Add(Me.tbWindowSizeFactor)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.cbReportByPredefSD)
        Me.Controls.Add(Me.rbOnlyFirstDPRx)
        Me.Controls.Add(Me.cbReportByOrigSD)
        Me.Controls.Add(Me.cbLockExactSDSol)
        Me.Controls.Add(Me.btnSchedShadPrice)
        Me.Controls.Add(Me.tbIterationNum)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tbUnschedLockCycles)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.tbDirections)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.tbMxPresUnchosen)
        Me.Controls.Add(Me.tbSchedLockCycles)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.tbCycleHalfLife)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.rbClosestVal)
        Me.Controls.Add(Me.rbHighestVal)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cbKeepBestNS)
        Me.Controls.Add(Me.tbMaxPresChosen)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.tbNumDPCycles)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.tbMxPresPerPoly)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.tbTrimTol)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.tbTrimIzoneControls)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.tb1optStop)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.tbDPSpaceAfterDlPlIters)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tbDlPlItersNoTrim)
        Me.Controls.Add(Me.txtHalfLife)
        Me.Controls.Add(Me.txtrewrite)
        Me.Controls.Add(Me.txtNFloat)
        Me.Controls.Add(Me.cmdItrPrcApply)
        Me.Controls.Add(Me.cmdItrPrcCancel)
        Me.Controls.Add(Me.txthowmanymore)
        Me.Controls.Add(Me.txtNShape)
        Me.Controls.Add(Me.txtNSmooth)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Lblrewrite)
        Me.Controls.Add(Me.lblHowManyIterations)
        Me.Controls.Add(Me.lblNShape)
        Me.Controls.Add(Me.lblNsmooth)
        Me.Controls.Add(Me.lblNFloat)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.WindowText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "IterSteps"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.Text = "Run Procedure"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Public WithEvents tbDPSpaceAfterDlPlIters As System.Windows.Forms.TextBox
    Public WithEvents Label7 As System.Windows.Forms.Label
    Public WithEvents tb1optStop As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Public WithEvents tbTrimIzoneControls As System.Windows.Forms.TextBox
    Public WithEvents Label9 As System.Windows.Forms.Label
    Public WithEvents tbTrimTol As System.Windows.Forms.TextBox
    Public WithEvents Label10 As System.Windows.Forms.Label
    Public WithEvents tbMxPresPerPoly As System.Windows.Forms.TextBox
    Public WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Public WithEvents tbMxPresUnchosen As System.Windows.Forms.TextBox
    Public WithEvents Label13 As System.Windows.Forms.Label
    Public WithEvents tbNumDPCycles As System.Windows.Forms.TextBox
    Public WithEvents Label14 As System.Windows.Forms.Label
    Public WithEvents tbMaxPresChosen As System.Windows.Forms.TextBox
    Public WithEvents Label16 As System.Windows.Forms.Label
    Public WithEvents tbDeviationWriteFile As System.Windows.Forms.TextBox
    Public WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents cbKeepBestNS As System.Windows.Forms.CheckBox
    Public WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents cbUseMSetViolations As System.Windows.Forms.CheckBox
    Friend WithEvents cbUseCSetViolations As System.Windows.Forms.CheckBox
    Friend WithEvents cbUseSSetViolations As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbClosestVal As System.Windows.Forms.RadioButton
    Friend WithEvents rbHighestVal As System.Windows.Forms.RadioButton
    Public WithEvents Label15 As System.Windows.Forms.Label
    Public WithEvents tbCycleHalfLife As System.Windows.Forms.TextBox
    Public WithEvents tbSchedLockCycles As System.Windows.Forms.TextBox
    Public WithEvents Label19 As System.Windows.Forms.Label
    Public WithEvents tbDirections As System.Windows.Forms.TextBox
    Public WithEvents Label20 As System.Windows.Forms.Label
    Public WithEvents tbFootprintPct As System.Windows.Forms.TextBox
    Public WithEvents Label23 As System.Windows.Forms.Label
    Public WithEvents Label24 As System.Windows.Forms.Label
    Public WithEvents tbUnschedLockCycles As System.Windows.Forms.TextBox
    Friend WithEvents cbUseSSetExcesses As System.Windows.Forms.CheckBox
    Friend WithEvents cbUseCSetExcesses As System.Windows.Forms.CheckBox
    Friend WithEvents cbUseMSetExcesses As System.Windows.Forms.CheckBox
    Public WithEvents tbIterationNum As System.Windows.Forms.TextBox
    Public WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnSchedShadPrice As System.Windows.Forms.Button
    Friend WithEvents cbLockExactSDSol As System.Windows.Forms.CheckBox
    Friend WithEvents rbOnlyFirstDPRx As System.Windows.Forms.RadioButton
    Friend WithEvents cbReportByOrigSD As System.Windows.Forms.CheckBox
    Friend WithEvents cbReportByPredefSD As System.Windows.Forms.CheckBox
    Public WithEvents Label21 As System.Windows.Forms.Label
    Public WithEvents tbWindowSizeFactor As System.Windows.Forms.TextBox
    Public WithEvents tbWindowMaxSize As System.Windows.Forms.TextBox
    Public WithEvents Label25 As System.Windows.Forms.Label
    Public WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents cbOneOptAfterDP As System.Windows.Forms.CheckBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents cbRandomOneOpt As System.Windows.Forms.CheckBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Public WithEvents tbTrimSpeed As System.Windows.Forms.TextBox
    Public WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents cbGridBuilder As System.Windows.Forms.CheckBox
    Public WithEvents tbDollarPerAcreTol As System.Windows.Forms.TextBox
    Public WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Public WithEvents tbFlopConMaxMult As System.Windows.Forms.TextBox
    Public WithEvents tbMaxBreaks As System.Windows.Forms.TextBox
    Public WithEvents Label29 As System.Windows.Forms.Label
    Public WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents cbSmoothSideSFlopControl As System.Windows.Forms.CheckBox
    Public WithEvents tbBackFlopFactor As System.Windows.Forms.TextBox
    Friend WithEvents cbSmoothBackSFlopControl As System.Windows.Forms.CheckBox
    Public WithEvents tbItersBetweenTrims As System.Windows.Forms.TextBox
    Public WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents rbPctDev As System.Windows.Forms.RadioButton
    Friend WithEvents rbAbsDev As System.Windows.Forms.RadioButton
#End Region
End Class