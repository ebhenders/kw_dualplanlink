Public Class clTrimmer1

    'The trimmer will read price, AA, and Rx information either from an input file, or from a Dual-plan
    'processed solution (with adjusted prices)
    'The trimmer will then determine the values (including an infinite series beyond the planning horizon),
    ' assume optimistic spatial alignment, and choose suite of best spatial solutions PLUS the best
    ' non-spatial solutions to send on to DPForm OR DPSpace

    'outputs will include the per-poly prescriptions, associated non-spatial NPV, and Space, Buffer, and Edge
    ' series pointer indices for each Rx. These are all inputs for DPSpace.

#Region "Declarations"


    Dim DualPlanFileList As String 'aa and rx index file
    Dim CurrentDualPlanPricesFile As String 'current dual plan input file; mining the shadow price info.
    Dim ISpaceDefFile As String 'file with interior space and quality buffer definitions
    Dim IZonesFile As String
    Dim GlobalTrim As Integer

    'Stand-level prescriptions available.
    'Dim StandRx() As RxInfo 'by stand

    'Hexagon/stand information
    'Dim HexPolys() As PolyInfo

    'Dim NStand As Integer
    Dim StandIDByPolyID() As Integer 'displays a poly's stand ID...indexed by its polyID
    Dim PolyIndByPolyID() As Integer 'put in polyID and output hex ID for polygon array
    Dim StandMaxPatch(,,) As Single  'maximum patch size a stand can be a part of - by stand,spacetype, period (eventually should be by space type as well)
    Dim StandMaxID() As Long 'when reassigning stand IDs to the hex, keep track of the largest reassigned ID associated with any stand
    Dim MaxPatchPer() As Single 'the maximum patch that the prescription can be a part of by period
    Dim Patches(,) As PatchInfo 'dim by spacetype,NTIC
    Dim MaxPatchID() As Integer 'by NTIC


    'for economic prescription trimming
    Dim CurrRxValue As Single 'value of the current prescription being evaluated
    Dim MaxRxValue As Single 'the maximum value of the suite of prescriptions being evaluated
    'Dim KeepRx As Integer '0/1 whether to keep or not
    Dim RxSpatialValue As Integer '0/1 whether or not the Rx can even have spatial value from neighboring stands

    'for trimming based on spatial information
    Dim DPSpaceStandID_LUT() As Double 'stores the stand id sent to DPSpace
    Dim NSpatialPolys As Integer = 0 'number of polygons with spatial prescriptions that have made it through the trimmer
    Dim AASpacePotential(,,) As Byte 'by AA, SpaceType, Tic
    Dim KeepZoneSpace(,) As Byte 'whether to keep the zone for printing or not - dimensioned by the number of SpaceTypes and IZones

    'for tracking prescriptions
    Dim OrderedPres(,) As Integer 'by stand, prescription - ordered by value largest to smallest
    'Dim TrimmedEcon(,) As Integer 'tracks prescriptions trimmed by the economic trimmer - by stand, prescription
    'Dim TrimmedSpatialDom(,) As Integer 'by stand, prescription - tracks number of prescriptions trimmed by spatial dominance
    Dim TrimmedNumber(,) As Integer 'by stand, prescription - based on whether it's been trimmed based on a max number of treatments per polygon
    Dim RxIn(,) As Byte 'by stand, prescription

    Dim SpatialAlignValue(,) As Single 'Double 'by stand, prescription


    Structure PatchInfo
        Dim PatchID() As Single 'patch ID
        Dim Area() As Single 'DIM BY patch ID
        Dim Patch() As StandList 'dim by patch ID
    End Structure
    Structure StandList
        Dim Stands() As Integer
    End Structure

#End Region

    Public Sub RunTrimmer(ByVal ksubfor As Integer, ByVal WriteDPSpace As Boolean)
        'implied cycle number = 0

        Dim trimiter As Integer = 0
        Dim ktrialset As Integer
        Dim kbreakset As Integer
        ReDim RxIn(AA_NPolys, MxStandRx)
        ReDim NPValMxSpat(AA_NPolys, MxStandRx)
        ReDim MaxNSRx(AA_NPolys)
        Dim BestSpatRx() As Long 'DUMMY chosen Rx if it's a kw rx, otherwise the one chosen that best aligns with fellow IZone stands
        ReDim BestSpatRx(AA_NPolys) 'dummy

        Dim NRx As Integer
        'Dim mrx As Integer 'the index of the last chosen prescription
        Dim krx As Integer
        Dim kind As Integer
        'Dim mind As Integer 'spatial index of the last chosen prescription
        Dim MaxInRx As Integer 'tally the highest number of prescriptions that make it through the trimmer for a stand
        Dim RxInThisStand As Integer
        Dim RxToTrimThisIter As Integer
        Dim TrimReset As Integer = 0
        Dim MxPresPerPoly As Integer = 999999


        'meant to subforest information - assume kaa is set!

        'refresh the polygon information...

        'now get the economic info. for all Dual Plan prescriptions (similar to the SCHEDULE routine, just don't pick yet!)
        DualPlanRxEcon(RxIn, MaxNSRx)

        'figure the MxPresPerPoly
        ktrialset = DetermineKTrialSet(0)
        kbreakset = TrialBreakSet(ktrialset)
        For jbreak As Integer = 1 To TrialBreakSetNBreaks(kbreakset)
            If TrialNSched(kbreakset, jbreak) < MxPresPerPoly And TrialNSched(kbreakset, jbreak) > 0 Then MxPresPerPoly = TrialNSched(kbreakset, jbreak)
            If TrialNUnsched(kbreakset, jbreak) < MxPresPerPoly And TrialNUnsched(kbreakset, jbreak) > 0 Then MxPresPerPoly = TrialNUnsched(kbreakset, jbreak)
        Next

        'back and forth between TrimEcon and SpatialDominance
        GlobalTrim = 1
        Do Until GlobalTrim = 0
            trimiter = trimiter + 1
            If trimiter = 1 Then 'go through each trim routine at least once
                GlobalTrim = 0
                TrimEcon(RxIn, NPValMxSpat, TrimTolerance, FootprintPct)
                If GlobalTrim > 0 Then TrimSpatialDom(RxIn)
            Else
                'if your last trimspatialdom trimmed anything, re-evaluate with TrimEcon
                If GlobalTrim > 0 Then
                    GlobalTrim = 0
                    TrimEcon(RxIn, NPValMxSpat, TrimTolerance, FootprintPct)
                End If

                'if your last TrimEcon trimmed anything, re-check spatial dominance
                If GlobalTrim > 0 Then
                    GlobalTrim = 0
                    TrimSpatialDom(RxIn)
                End If

            End If
        Loop

        'write out the appropriate files - for Dual plan iterations
        If WriteDPSpace = False Then WriteDlPlTrimInfo(DualPlanSubForests(ksubfor).LatestTrimDataFile)

        If WriteDPSpace = True Then
            trimiter = 0
            GlobalTrim = 0 'don't do another trim iteration if you're not trimming based on max # per poly
            If MxPresPerPoly > 0 Then
                MaxInRx = 0
                For jaa As Integer = 1 To AA_NPolys
                    RxInThisStand = 0
                    NRx = AA_NPres(jaa)

                    For jrx As Integer = 1 To NRx
                        krx = AA_PresArray(jaa, jrx) 'krx used to access line in prescription database
                        kind = AApres_ISpaceInd(krx)
                        'If iRxIn(jaa, jrx) = 1 And SpatialSeries(kind).NNonZero > 0 Then    'If TrimPoly(jaa).RxIn(jrx) = 1 Then
                        If SpatialSeries(kind).PersistentSpatial = True Then RxInThisStand = RxInThisStand + RxIn(jaa, jrx) 'don't include non-spatial
                    Next
                    If RxInThisStand > MaxInRx Then MaxInRx = RxInThisStand
                Next jaa

                RxToTrimThisIter = MaxInRx - TrimSpeed
                If MaxInRx > MxPresPerPoly And RxToTrimThisIter < MxPresPerPoly Then RxToTrimThisIter = MxPresPerPoly
                Do Until RxToTrimThisIter < MxPresPerPoly
                    TrimMaxRxPerPoly2(NPValMxSpat, BestSpatRx, RxIn, RxToTrimThisIter, True, TrimTolerance, 1, 0, ktrialset)
                    If GlobalTrim > TrimReset Then
                        GlobalTrim = 0
                        TrimEcon(RxIn, NPValMxSpat, 1, FootprintPct) 'account for scheduled polygon Rx's that were just trimmed!
                        If GlobalTrim > 0 Then TrimSpatialDom(RxIn)
                        GlobalTrim = 0
                    End If
                    RxToTrimThisIter = RxToTrimThisIter - TrimSpeed
                    If RxToTrimThisIter + TrimSpeed > MxPresPerPoly And RxToTrimThisIter < MxPresPerPoly Then RxToTrimThisIter = MxPresPerPoly
                Loop
                'make sure you clean up
                If GlobalTrim > 0 Then
                    GlobalTrim = 0
                    TrimEcon(RxIn, NPValMxSpat, 1, FootprintPct) 'account for scheduled polygon Rx's that were just trimmed!
                    If GlobalTrim > 0 Then TrimSpatialDom(RxIn)
                End If
            End If

            'finally, make sure that you include the best non-spatial
            For jaa As Integer = 1 To AA_NPolys
                RxIn(jaa, MaxNSRx(jaa)) = 1 'best non-spatial
            Next

            WriteDPSpaceTrimInfo(ksubfor, RxIn, True)
            WriteTrimmedIZoneAndHexInfo(ksubfor)
        End If


    End Sub

    Public Sub CycleTrimmer(ByVal ksubfor As Integer, ByVal CycleNum As Integer)
        'trimmer takes current spatial solution as "1 option stands" and allows anything not in (spatial) solution to have
        ' a full suite of choices
        Dim jaa As Integer
        Dim NRx As Integer
        Dim jrx As Integer
        Dim mrx As Integer 'the index of the last chosen prescription
        Dim mind As Integer 'spatial index of the last chosen prescription
        Dim krx As Integer
        Dim kind As Integer
        Dim ktrialset As Integer
        Dim kbreakset As Integer
        Dim kbreak As Integer
        Dim trimiter As Integer
        Dim TrimReset As Integer = 0
        Dim bSpatialScheduledStand() As Boolean
        Dim CycleRxIn(,) As Byte
        Dim CycleMxSpat(,) As Single
        Dim MaxInRx As Integer 'tally the highest number of prescriptions that make it through the trimmer for a stand
        Dim RxInThisStand As Integer
        'Dim RxToTrimThisIter As Integer
        'Dim SRxToTrimThisIter As Integer 'scheduled trims this iteration
        'Dim URxToTrimThisIter As Integer 'unscheduled trims this iteration
        Dim RxToKeepThisIter As Integer 'number of prescriptions to keep this iteration
        Dim CompareSRxTrim As Integer
        Dim CompareURxTrim As Integer
        Dim MxPresPerPoly As Integer = 999999
        Dim UMxPresPerPoly As Integer = 999999
        Dim SMxPresPerPoly As Integer = 999999


        Dim BestSpatRx() As Long 'chosen Rx if it's a kw rx, otherwise the one chosen that best aligns with fellow IZone stands
        Dim LockedRx() As Long 'if you elect to lock a prescription if it doesn't change after X cycles
        Dim bLockedAA() As Boolean
        Dim basecompare As Integer
        Dim bLock As Boolean

        Dim PreTrim As Boolean 'whether you are paring down based on first DP solution trim or not

        'check to see if you are keying to the first DP prescription trim


        ktrialset = DetermineKTrialSet(CycleNum)
        kbreakset = TrialBreakSet(ktrialset)
        For jbreak As Integer = 1 To TrialBreakSetNBreaks(kbreakset)
            If TrialNSched(kbreakset, jbreak) < MxPresPerPoly And TrialNSched(kbreakset, jbreak) > 0 Then MxPresPerPoly = TrialNSched(kbreakset, jbreak)
            If TrialNSched(kbreakset, jbreak) < SMxPresPerPoly And TrialNSched(kbreakset, jbreak) > 0 Then SMxPresPerPoly = TrialNSched(kbreakset, jbreak)

            If TrialNUnsched(kbreakset, jbreak) < MxPresPerPoly And TrialNUnsched(kbreakset, jbreak) > 0 Then MxPresPerPoly = TrialNUnsched(kbreakset, jbreak)
            If TrialNUnsched(kbreakset, jbreak) < UMxPresPerPoly And TrialNUnsched(kbreakset, jbreak) > 0 Then UMxPresPerPoly = TrialNUnsched(kbreakset, jbreak)

        Next

        If TrialTrimType(ktrialset) = "First" Then ' If bOnlyFirstDPRx = True Then
            If CycleNum = 1 Then
                ReDim EconRxIn(AA_NPolys, MxStandRx) 'what was trimmed based just on econ - a "pure" trim
                ReDim NPValMxSpat(AA_NPolys, MxStandRx)
                ReDim MaxNSRx(AA_NPolys)
                DualPlanRxEcon(EconRxIn, MaxNSRx)
            End If
            Call FirstCycleTrimmer(ksubfor, CycleNum, ktrialset)
            Exit Sub
        End If

        'if you are using the closest value and trimming to fewer than the max number of prescriptions in the first iteration,
        ' you can save some time here....
        If TrialTrimType(ktrialset) = "Closest" Then 'If bClosestValue = True And iMaxPresScheduled > 0 And iMaxPresUnscheduled > 0 And iMaxPresUnscheduled <= MxPresPerPoly Then
            If CycleNum = 1 Then
                ReDim EconRxIn(AA_NPolys, MxStandRx) 'what was trimmed based just on econ - a "pure" trim
                ReDim NPValMxSpat(AA_NPolys, MxStandRx)
                ReDim MaxNSRx(AA_NPolys)
                DualPlanRxEcon(EconRxIn, MaxNSRx)
                'TrimEcon2(DualPlanAAPoly, EconRxIn, NPValMxSpat, 1, FootprintPct)
            End If
            Call ClosestValueTrimmer(ksubfor, CycleNum, ktrialset)
            Exit Sub
        End If



        ReDim RxIn(AA_NPolys, MxStandRx) ', SolutionRxIn(AA_NPolys, MxStandRx)
        ReDim CycleRxIn(AA_NPolys, MxStandRx)
        ReDim CycleMxSpat(AA_NPolys, MxStandRx)

        ReDim SpatialAlignValue(AA_NPolys, MxStandRx)
        ReDim KeepZoneSpace(NSpaceType, NZoneSubFor)
        ReDim bSpatialScheduledStand(AA_NPolys)
        ReDim bLockedAA(AA_NPolys)

        'First, get the default economic/"True" trim values...
        If CycleNum = 1 Then 'don't have to do with every cycle
            'now get the economic info. for all Dual Plan prescriptions (similar to the SCHEDULE routine, just don't pick yet!)
            ReDim EconRxIn(AA_NPolys, MxStandRx) 'what was trimmed based just on econ - a "pure" trim
            ReDim NPValMxSpat(AA_NPolys, MxStandRx)
            ReDim MaxNSRx(AA_NPolys)
            DualPlanRxEcon(EconRxIn, MaxNSRx)
            trimiter = 0
            GlobalTrim = 1
            Do Until GlobalTrim = 0
                trimiter = trimiter + 1
                If trimiter = 1 Then 'go through each trim routine at least once
                    GlobalTrim = 0
                    TrimEcon(EconRxIn, NPValMxSpat, 1, FootprintPct) 'use a 1 trimtolerance to not artificially trim treatments
                    If GlobalTrim > 0 Then TrimSpatialDom(EconRxIn)
                Else
                    'if your last trimspatialdom trimmed anything, re-evaluate with TrimEcon
                    If GlobalTrim > 0 Then
                        GlobalTrim = 0
                        TrimEcon(EconRxIn, NPValMxSpat, 1, FootprintPct) 'use a 1 trimtolerance to not artificially trim treatments
                    End If
                    'if your last TrimEcon trimmed anything, re-check spatial dominance
                    If GlobalTrim > 0 Then
                        GlobalTrim = 0
                        TrimSpatialDom(EconRxIn)
                    End If
                End If
            Loop
        End If


        'OLD: shortcut - if both cycle trimmer parameters are <= first DP max # prescriptions, some cycle scheduling has already been pared down!
        PreTrim = False
        'If iMaxPresScheduled > 0 And iMaxPresUnscheduled > 0 And iMaxPresScheduled <= MxPresPerPoly _
        '      And iMaxPresUnscheduled <= MxPresPerPoly Then
        '    LoadRxLUT(FnDualPlanOutBaseA & "_RxLUTFirst." & CStr(ksubfor), CycleRxIn)
        '    PreTrim = True
        'End If


        'First, figure the best KW Rx per stand and default to include the best non-spatial. Also populate relevant arrays
        ReDim BestSpatRx(AA_NPolys), LockedRx(AA_NPolys)
        MaxInRx = 0

        For jaa = 1 To AA_NPolys

            'ktrialset = DetermineKTrialSet(CycleNum)
            'kbreakset = TrialBreakSet(ktrialset)
            For jbreak As Integer = 1 To TrialBreakSetNBreaks(kbreakset)
                If AA_area(jaa) >= TrialStandMaxSize(kbreakset, jbreak) And AA_area(jaa) < TrialStandMaxSize(kbreakset, jbreak) Then
                    kbreak = jbreak
                    Exit For
                End If
            Next

            RxInThisStand = 0
            bSpatialScheduledStand(jaa) = False
            bLockedAA(jaa) = False 'default
            NRx = AA_NPres(jaa)
            If AA_polyid(jaa) > 0 Then
                mrx = PolySolRx_ChosenRxInd(jaa) 'the DualPlan input file prescription index
                mind = AApres_ISpaceInd(mrx)
                'If SpatialSeries(mind).NNonZero > 0 Then 'chosen solution is a spatial solution
                If SpatialSeries(mind).PersistentSpatial = True Then 'chosen solution is a spatial solution
                    BestSpatRx(jaa) = mrx
                    bSpatialScheduledStand(jaa) = True
                End If
            End If

            'does this Spatially Scheduled stand have a locked prescription?
            bLock = False 'default
            If kSchedLockCycles > 0 And bSpatialScheduledStand(jaa) = True And CycleNum >= kSchedLockCycles - 1 Then
                basecompare = SchedPolySolution(jaa, 1) 'doesn't matter the index since they are all the same - use 1 as default
                bLock = True
                For jlock As Integer = 1 To kSchedLockCycles
                    If basecompare <> SchedPolySolution(jaa, jlock) Then
                        bLock = False
                        Exit For
                    End If
                Next
                If bLock = True Then LockedRx(jaa) = basecompare
                bLockedAA(jaa) = bLock
            End If
            'does this non-Spatially Scheduled stand have a locked prescription?
            bLock = False 'default
            If kUnschedLockCycles > 0 And bSpatialScheduledStand(jaa) = False And CycleNum >= kUnschedLockCycles - 1 Then
                basecompare = UnschedPolySolution(jaa, 1) 'doesn't matter the index since they are all the same - use 1 as default
                bLock = True
                For jlock As Integer = 1 To kUnschedLockCycles
                    If basecompare <> UnschedPolySolution(jaa, jlock) Then
                        bLock = False
                        Exit For
                    End If
                Next
                If bLock = True Then LockedRx(jaa) = basecompare
                bLockedAA(jaa) = bLock
            End If
            'If iMaxPresScheduled = 1 Then basecompare = BestSpatRx(jaa) 'do you want to lock at this prescription anyway?

            'was this subdivision scheduled in one pass? Do you want to lock those stands? 
            '       - check to see if the AA is part of a subdivision and that subdivision was solved in 1 pass or less 
            '            (less if all stands in the pass have exactly 1 solution)
            If bLockExactSDSol = True And AASubDiv(DPSpace_PolyInd(jaa)) > 0 And bSubForSubDivSinglePass(ksubfor, AASubDiv(DPSpace_PolyInd(jaa))) = True Then
                bLockedAA(jaa) = True
                bgLockedAASD(kaaset, jaa) = True
                basecompare = PolySolRx_ChosenRxInd(jaa)
                gLockedRxSD(kaaset, jaa) = basecompare
                LockedRx(jaa) = basecompare
            End If
            'FLAG - because you require a positive identifier on the window count of the subdivision, you miss those that were scheduled last pass - need to have a "global" already schedule? Do we have it already?
            If bLockExactSDSol = True Then
                If bgLockedAASD(kaaset, jaa) = True Then
                    bLockedAA(jaa) = True
                    basecompare = gLockedRxSD(kaaset, jaa)
                    LockedRx(jaa) = basecompare
                End If
            End If
            If bLockedAA(jaa) = True Then basecompare = LockedRx(jaa)
            If TrialNSched(kbreakset, kbreak) = 1 Then basecompare = BestSpatRx(jaa)

            For jrx = 1 To NRx
                If PreTrim = False Then CycleRxIn(jaa, jrx) = EconRxIn(jaa, jrx)
                CycleMxSpat(jaa, jrx) = NPValMxSpat(jaa, jrx) 'refresh with every cycle

                If bLockedAA(jaa) = True Or (TrialNSched(kbreakset, kbreak) = 1 And bSpatialScheduledStand(jaa) = True) Then 'filter them out right away!
                    If basecompare <> AA_PresArray(jaa, jrx) Then
                        CycleRxIn(jaa, jrx) = 0
                        'ScheduledRxIn(jaa, jrx) = 0
                    End If
                End If
                krx = AA_PresArray(jaa, jrx) 'krx used to access line in prescription database
                kind = AApres_ISpaceInd(krx)
                If SpatialSeries(kind).PersistentSpatial = True Then RxInThisStand = RxInThisStand + CycleRxIn(jaa, jrx) 'don't include non-spatial
                'RxInThisStand = RxInThisStand + CycleRxIn(jaa, jrx) '
            Next
            If RxInThisStand > MaxInRx Then MaxInRx = RxInThisStand
        Next jaa


        'account for prescriptions just trimmed with max cycles before harvest...
        If (CycleNum >= kSchedLockCycles - 1 And kSchedLockCycles > 0) Or (CycleNum >= kUnschedLockCycles - 1 And kUnschedLockCycles > 0) Then
            GlobalTrim = 0
            TrimEcon(CycleRxIn, CycleMxSpat, 1, FootprintPct) 'account for scheduled polygon Rx's that were just trimmed!
            If GlobalTrim > 0 Then TrimSpatialDom(CycleRxIn)
            GlobalTrim = 0
        End If

        'now, pare down to the max number of Rx per poly and see if this takes out any economic options

        If MxPresPerPoly > 0 Then 'scheduled stands get pared down to a max # Rx per poly
            RxToKeepThisIter = MaxInRx - -TrimSpeed
            If MaxInRx > MxPresPerPoly And RxToKeepThisIter < MxPresPerPoly Then RxToKeepThisIter = MxPresPerPoly
            'CompareSRxTrim = iMaxPresScheduled 'default
            'If iMaxPresScheduled <= 1 Then CompareSRxTrim = MaxInRx '1 = locked prescription which should already be filtered, 0 means unconstrained
            'CompareURxTrim = iMaxPresUnscheduled
            'If iMaxPresUnscheduled = 0 Then CompareURxTrim = MaxInRx

            'SRxToTrimThisIter = MaxInRx - 1
            'URxToTrimThisIter = MaxInRx - 1

            Do Until RxToKeepThisIter < MxPresPerPoly
                TrimMaxRxPerPoly2(CycleMxSpat, BestSpatRx, CycleRxIn, RxToKeepThisIter, KeepBestNS, 1, 1, CycleNum, ktrialset)
                If GlobalTrim > TrimReset Then
                    GlobalTrim = 0
                    TrimEcon(CycleRxIn, CycleMxSpat, 1, FootprintPct) 'account for scheduled polygon Rx's that were just trimmed!
                    If GlobalTrim > 0 Then TrimSpatialDom(CycleRxIn)
                    GlobalTrim = 0
                End If
                RxToKeepThisIter = RxToKeepThisIter - TrimSpeed
                If RxToKeepThisIter + TrimSpeed > MxPresPerPoly And RxToKeepThisIter < MxPresPerPoly Then RxToKeepThisIter = MxPresPerPoly

                'If iMaxPresScheduled >= 1 And SRxToTrimThisIter > iMaxPresScheduled Then SRxToTrimThisIter = SRxToTrimThisIter - 1
                'If iMaxPresUnscheduled > 0 And URxToTrimThisIter > iMaxPresUnscheduled Then URxToTrimThisIter = URxToTrimThisIter - 1
            Loop
            'make sure you clean up
            If GlobalTrim > 0 Then
                GlobalTrim = 0
                TrimEcon(CycleRxIn, CycleMxSpat, 1, FootprintPct) 'account for scheduled polygon Rx's that were just trimmed!
                If GlobalTrim > 0 Then TrimSpatialDom(CycleRxIn)
            End If
        End If


        'make sure at least the last chosen prescription and best non-spatial are included...
        For jaa = 1 To AA_NPolys
            NRx = AA_NPres(jaa)
            'filter out all other prescriptions that have not been chosen
            'If BestSpatRx(jaa) > 0 Then 'this prescription was chosen
            For jrx = 1 To NRx
                RxIn(jaa, jrx) = CycleRxIn(jaa, jrx)
                krx = AA_PresArray(jaa, jrx)
                If krx = BestSpatRx(jaa) And bSpatialScheduledStand(jaa) = True And bLockedAA(jaa) = False Then
                    RxIn(jaa, jrx) = 1 'chosen prescription
                    'If KeepBestNS = False Then Exit For
                End If
            Next
            If KeepBestNS = True And bLockedAA(jaa) = False Then RxIn(jaa, MaxNSRx(jaa)) = 1 'best non-spatial
            If bSpatialScheduledStand(jaa) = False Then RxIn(jaa, MaxNSRx(jaa)) = 1 'best non-spatial for unscheduled stand
            If KeepBestNS = False And bSpatialScheduledStand(jaa) = True Then RxIn(jaa, MaxNSRx(jaa)) = 0 'filter out best non-spatial if you don't want to keep it
        Next

        'Final Keepzonespace tally

        WriteDPSpaceTrimInfo(ksubfor, RxIn, False)
        WriteTrimmedIZoneAndHexInfo(ksubfor)

    End Sub

    Private Sub FirstCycleTrimmer(ByVal ksubfor As Integer, ByVal CycleNum As Integer, ByVal ktrialset As Integer)
        'Sub trims base on the first DP - only those Rx that made it through the trimmer for the first DP are allowed initially.
        'Then pare down to locked prescriptions, etc.

        'trimmer takes current spatial solution as "1 option stands" and allows anything not in (spatial) solution to have
        ' a full suite of choices

        Dim NRx As Integer
        Dim jrx As Integer
        Dim mrx As Integer 'the index of the last chosen prescription
        Dim mind As Integer 'spatial index of the last chosen prescription
        Dim krx As Integer
        Dim kbreakset As Integer
        Dim TrimReset As Integer = 0
        Dim bSpatialScheduledStand() As Boolean
        Dim CycleRxIn(,) As Byte
        Dim StandNPValMx(,) As Single 'here, a dummy value to satiate the Trim to # ... I think ...


        Dim BestSpatRx() As Long 'chosen Rx if it's a kw rx, otherwise the one chosen that best aligns with fellow IZone stands
        Dim LockedRx() As Long 'if you elect to lock a prescription if it doesn't change after X cycles
        Dim bLockedAA() As Boolean
        Dim basecompare As Integer
        Dim bLock As Boolean
        Dim MxPresPerPoly As Integer = 999999

        'check to see if you are keying to the first DP prescription trim

        ReDim RxIn(AA_NPolys, MxStandRx) ', SolutionRxIn(AA_NPolys, MxStandRx)
        ReDim CycleRxIn(AA_NPolys, MxStandRx)

        ReDim bSpatialScheduledStand(AA_NPolys)
        ReDim bLockedAA(AA_NPolys)
        ReDim StandNPValMx(AA_NPolys, MxStandRx)

        'figure the MxPresPerPoly
        kbreakset = TrialBreakSet(ktrialset)
        For jbreak As Integer = 1 To TrialBreakSetNBreaks(kbreakset)
            If TrialNSched(kbreakset, jbreak) < MxPresPerPoly And TrialNSched(kbreakset, jbreak) > 0 Then MxPresPerPoly = TrialNSched(kbreakset, jbreak)
            If TrialNUnsched(kbreakset, jbreak) < MxPresPerPoly And TrialNUnsched(kbreakset, jbreak) > 0 Then MxPresPerPoly = TrialNUnsched(kbreakset, jbreak)
        Next

        'All I have to do is keep ALL the Rx from the first trim, save those that have been locked due to the number of ScheduledLock and UnscheduledLock cycles
        '1. Load the first DP Trim information
        LoadRxLUT(FnDualPlanOutBaseA & "_RxLUTFirst." & CStr(ksubfor), CycleRxIn)

        '2. Pare down for locks - exact solutions, after so many moving windows, etc.
        'does this Spatially Scheduled stand have a locked prescription?
        ReDim BestSpatRx(AA_NPolys), LockedRx(AA_NPolys)
        'MaxInRx = 0
        For jaa As Integer = 1 To AA_NPolys
            'If jaa = 2075 Then Stop

            'RxInThisStand = 0
            bSpatialScheduledStand(jaa) = False
            bLockedAA(jaa) = False 'default
            NRx = AA_NPres(jaa)
            If AA_polyid(jaa) > 0 Then
                mrx = PolySolRx_ChosenRxInd(jaa) 'the DualPlan input file prescription index
                mind = AApres_ISpaceInd(mrx)
                'If SpatialSeries(mind).NNonZero > 0 Then 'chosen solution is a spatial solution
                If SpatialSeries(mind).PersistentSpatial = True Then 'chosen solution is a spatial solution
                    BestSpatRx(jaa) = mrx
                    bSpatialScheduledStand(jaa) = True
                End If
            End If


            bLock = False 'default
            If kSchedLockCycles > 0 And bSpatialScheduledStand(jaa) = True And CycleNum >= kSchedLockCycles - 1 Then
                basecompare = SchedPolySolution(jaa, 1) 'doesn't matter the index since they are all the same - use 1 as default
                bLock = True
                For jlock As Integer = 1 To kSchedLockCycles
                    If basecompare <> SchedPolySolution(jaa, jlock) Then
                        bLock = False
                        Exit For
                    End If
                Next
                If bLock = True Then LockedRx(jaa) = basecompare
                bLockedAA(jaa) = bLock
            End If
            'does this non-Spatially Scheduled stand have a locked prescription?
            bLock = False 'default
            If kUnschedLockCycles > 0 And bSpatialScheduledStand(jaa) = False And CycleNum >= kUnschedLockCycles - 1 Then
                basecompare = UnschedPolySolution(jaa, 1) 'doesn't matter the index since they are all the same - use 1 as default
                bLock = True
                For jlock As Integer = 1 To kUnschedLockCycles
                    If basecompare <> UnschedPolySolution(jaa, jlock) Then
                        bLock = False
                        Exit For
                    End If
                Next
                If bLock = True Then LockedRx(jaa) = basecompare
                bLockedAA(jaa) = bLock
            End If

            'was this subdivision scheduled in one pass? Do you want to lock those stands? 
            '       - check to see if the AA is part of a subdivision and that subdivision was solved in 1 pass or less 
            '            (less if all stands in the pass have exactly 1 solution)
            If bLockExactSDSol = True And AASubDiv(DPSpace_PolyInd(jaa)) > 0 And bSubForSubDivSinglePass(ksubfor, AASubDiv(DPSpace_PolyInd(jaa))) = True Then
                bLockedAA(jaa) = True
                bgLockedAASD(kaaset, jaa) = True
                basecompare = PolySolRx_ChosenRxInd(jaa)
                gLockedRxSD(kaaset, jaa) = basecompare
                LockedRx(jaa) = basecompare
            End If
            'FLAG - because you require a positive identifier on the window count of the subdivision, you miss those that were scheduled last pass - need to have a "global" already schedule? Do we have it already?
            If bLockExactSDSol = True Then
                If bgLockedAASD(kaaset, jaa) = True Then
                    bLockedAA(jaa) = True
                    basecompare = gLockedRxSD(kaaset, jaa)
                    LockedRx(jaa) = basecompare
                End If
            End If

            If bLockedAA(jaa) = True Then 'Or (iMaxPresScheduled = 1 And bSpatialScheduledStand(jaa) = True) Then 'filter them out right away!
                basecompare = LockedRx(jaa)
                For jrx = 1 To NRx
                    If basecompare <> AA_PresArray(jaa, jrx) Then
                        CycleRxIn(jaa, jrx) = 0
                    Else
                        CycleRxIn(jaa, jrx) = 1
                    End If
                    StandNPValMx(jaa, jrx) = DualPlanAAPoly_NPVSelf(jaa, jrx)
                Next
            Else
                For jrx = 1 To NRx
                    StandNPValMx(jaa, jrx) = DualPlanAAPoly_NPVSelf(jaa, jrx)
                Next
            End If
        Next jaa

        '3. Check to see if the BreakSet definition calls for harder trimming than the First set
        TrimMaxRxPerPoly2(StandNPValMx, BestSpatRx, CycleRxIn, MxPresPerPoly, KeepBestNS, 1, 1, CycleNum, ktrialset)


        '4. Write out the new files!
        'make sure at least the last chosen prescription and best non-spatial are included...
        For jaa As Integer = 1 To AA_NPolys
            'If jaa = 1538 Then Stop
            NRx = AA_NPres(jaa)
            'filter out all other prescriptions that have not been chosen
            'If BestSpatRx(jaa) > 0 Then 'this prescription was chosen
            For jrx = 1 To NRx
                RxIn(jaa, jrx) = CycleRxIn(jaa, jrx)
                krx = AA_PresArray(jaa, jrx)
                If krx = BestSpatRx(jaa) And bSpatialScheduledStand(jaa) = True And bLockedAA(jaa) = False Then
                    RxIn(jaa, jrx) = 1 'chosen prescription
                    'If KeepBestNS = False Then Exit For
                End If
                'see if it's ever been scheduled and if so, make sure it's included.
                If bLockedAA(jaa) = False And RxIn(jaa, jrx) = 0 Then
                    'see if it's ever been scheduled and if so, make sure it's included.
                    If bLockedAA(jaa) = False And RxIn(jaa, jrx) = 0 Then
                        For jlock As Integer = 0 To NumDPCycles
                            If krx = AllSchedPolySolution(jaa, jlock) Then RxIn(jaa, jrx) = 1
                            If krx = AllUnschedPolySolution(jaa, jlock) Then RxIn(jaa, jrx) = 1 'note: should really only be the best non-spatial...
                        Next
                    End If
                End If
            Next
            If KeepBestNS = True And bLockedAA(jaa) = False Then RxIn(jaa, MaxNSRx(jaa)) = 1 'best non-spatial
            If bSpatialScheduledStand(jaa) = False Then RxIn(jaa, MaxNSRx(jaa)) = 1 'best non-spatial for unscheduled stand
            If KeepBestNS = False And bSpatialScheduledStand(jaa) = True Then RxIn(jaa, MaxNSRx(jaa)) = 0 'filter out best non-spatial if you don't want to keep it
        Next

        '------------------------------------------------------
        ''DEBUG - pare down to just subforest 1
        'Dim keeppoly() As Byte
        'ReDim keeppoly(AA_NPolys)
        'For jaa As Integer = 1 To AA_NPolys
        '    keeppoly(jaa) = 0
        '    If AASubDiv(DPSpace_PolyInd(jaa)) = 1 Then keeppoly(jaa) = 1
        'Next
        'FilterStandsForDebug(RxIn, keeppoly)
        'ReDim keeppoly(AA_NPolys)
        'keeppoly(110) = 1
        'keeppoly(436) = 1
        'keeppoly(455) = 1
        'FilterStandsForDebug2(RxIn, keeppoly)
        '------------------------------------------------------

        FinalKeepZoneSpaceTally(RxIn)

        WriteDPSpaceTrimInfo(ksubfor, RxIn, False)
        WriteTrimmedIZoneAndHexInfo(ksubfor)



    End Sub

    Private Sub ClosestValueTrimmer(ByVal ksubfor As Integer, ByVal CycleNum As Integer, ByVal ktrialset As Integer)
        'Sub trims base on the first DP - only those Rx that made it through the trimmer for the first DP are allowed initially.
        'Then pare down to locked prescriptions, etc.

        'trimmer takes current spatial solution as "1 option stands" and allows anything not in (spatial) solution to have
        ' a full suite of choices

        Dim NRx As Integer
        Dim jrx As Integer
        Dim mrx As Integer 'the index of the last chosen prescription
        Dim mind As Integer 'spatial index of the last chosen prescription
        Dim krx As Integer
        Dim kbreakset As Integer
        Dim TrimReset As Integer = 0
        Dim bSpatialScheduledStand() As Boolean
        Dim CycleRxIn(,) As Byte 'here used as a dummy to laod the unscheduled stands
        Dim Cycle2RxIn(,) As Byte 'here used to take CycleRxIn and add in all schedules for scheduled stands - so you can pare down the closest...
        Dim StandNPValMx(,) As Single
        'Dim CycleMxSpat(,) As Single


        Dim BestSpatRx() As Long 'chosen Rx if it's a kw rx, otherwise the one chosen that best aligns with fellow IZone stands
        Dim LockedRx() As Long 'if you elect to lock a prescription if it doesn't change after X cycles
        Dim bLockedAA() As Boolean
        Dim basecompare As Integer
        Dim bLock As Boolean
        Dim MxPresPerPoly As Integer = 999999

        'check to see if you are keying to the first DP prescription trim

        ReDim RxIn(AA_NPolys, MxStandRx) ', SolutionRxIn(AA_NPolys, MxStandRx)
        ReDim CycleRxIn(AA_NPolys, MxStandRx)
        ReDim Cycle2RxIn(AA_NPolys, MxStandRx)
        ReDim StandNPValMx(AA_NPolys, MxStandRx)

        ReDim bSpatialScheduledStand(AA_NPolys)
        ReDim bLockedAA(AA_NPolys)
        'ReDim CycleMxSpat(AA_NPolys, MxStandRx)


        'figure the MxPresPerPoly
        kbreakset = TrialBreakSet(ktrialset)
        For jbreak As Integer = 1 To TrialBreakSetNBreaks(kbreakset)
            If TrialNSched(kbreakset, jbreak) < MxPresPerPoly And TrialNSched(kbreakset, jbreak) > 0 Then MxPresPerPoly = TrialNSched(kbreakset, jbreak)
            If TrialNUnsched(kbreakset, jbreak) < MxPresPerPoly And TrialNUnsched(kbreakset, jbreak) > 0 Then MxPresPerPoly = TrialNUnsched(kbreakset, jbreak)
        Next

        'Start with the Rx from the first trim for unscheduled - if scheduled, don't trim...
        '1. Load the first DP Trim information
        LoadRxLUT(FnDualPlanOutBaseA & "_RxLUTFirst." & CStr(ksubfor), CycleRxIn)

        '2. Pare down for locks - exact solutions, after so many moving windows, etc.
        'does this Spatially Scheduled stand have a locked prescription?
        ReDim BestSpatRx(AA_NPolys), LockedRx(AA_NPolys)
        'MaxInRx = 0
        For jaa As Integer = 1 To AA_NPolys
            'If jaa = 3565 Then Stop

            'RxInThisStand = 0
            bSpatialScheduledStand(jaa) = False
            bLockedAA(jaa) = False 'default
            NRx = AA_NPres(jaa)
            If AA_polyid(jaa) > 0 Then
                mrx = PolySolRx_ChosenRxInd(jaa) 'the DualPlan input file prescription index
                mind = AApres_ISpaceInd(mrx)
                'If SpatialSeries(mind).NNonZero > 0 Then 'chosen solution is a spatial solution
                If SpatialSeries(mind).PersistentSpatial = True Then 'chosen solution is a spatial solution
                    BestSpatRx(jaa) = mrx
                    bSpatialScheduledStand(jaa) = True
                    'for a spatially scheduled stand, don't trim any prescriptions
                    For jrx = 1 To AA_NPres(jaa)
                        krx = AA_PresArray(jaa, jrx)
                        mind = AApres_ISpaceInd(krx)
                        If SpatialSeries(mind).PersistentSpatial = True Then Cycle2RxIn(jaa, jrx) = 1
                        StandNPValMx(jaa, jrx) = DualPlanAAPoly_NPVSelf(jaa, jrx)
                    Next jrx
                    'catch the best non-spatial
                    'If KeepBestNS = True And bLockedAA(jaa) = False Then Cycle2RxIn(jaa, MaxNSRx(jaa)) = 1 'best non-spatial
                Else 'you're dealing with an unscheduled stand - keep what you figured with the first trim
                    For jrx = 1 To AA_NPres(jaa)
                        Cycle2RxIn(jaa, jrx) = CycleRxIn(jaa, jrx)
                        StandNPValMx(jaa, jrx) = DualPlanAAPoly_NPVSelf(jaa, jrx)
                    Next
                End If
            End If


            bLock = False 'default
            If kSchedLockCycles > 0 And bSpatialScheduledStand(jaa) = True And CycleNum >= kSchedLockCycles - 1 Then
                basecompare = SchedPolySolution(jaa, 1) 'doesn't matter the index since they are all the same - use 1 as default
                bLock = True
                For jlock As Integer = 1 To kSchedLockCycles
                    If basecompare <> SchedPolySolution(jaa, jlock) Then
                        bLock = False
                        Exit For
                    End If
                Next
                If bLock = True Then LockedRx(jaa) = basecompare
                bLockedAA(jaa) = bLock
            End If
            'does this non-Spatially Scheduled stand have a locked prescription?
            bLock = False 'default
            If kUnschedLockCycles > 0 And bSpatialScheduledStand(jaa) = False And CycleNum >= kUnschedLockCycles - 1 Then
                basecompare = UnschedPolySolution(jaa, 1) 'doesn't matter the index since they are all the same - use 1 as default
                bLock = True
                For jlock As Integer = 1 To kUnschedLockCycles
                    If basecompare <> UnschedPolySolution(jaa, jlock) Then
                        bLock = False
                        Exit For
                    End If
                Next
                If bLock = True Then LockedRx(jaa) = basecompare
                bLockedAA(jaa) = bLock
            End If

            'was this subdivision scheduled in one pass? Do you want to lock those stands? 
            '       - check to see if the AA is part of a subdivision and that subdivision was solved in 1 pass or less 
            '            (less if all stands in the pass have exactly 1 solution)
            If bLockExactSDSol = True And AASubDiv(DPSpace_PolyInd(jaa)) > 0 And bSubForSubDivSinglePass(ksubfor, AASubDiv(DPSpace_PolyInd(jaa))) = True Then
                bLockedAA(jaa) = True
                bgLockedAASD(kaaset, jaa) = True
                basecompare = PolySolRx_ChosenRxInd(jaa)
                gLockedRxSD(kaaset, jaa) = basecompare
                LockedRx(jaa) = basecompare
            End If
            'FLAG - because you require a positive identifier on the window count of the subdivision, you miss those that were scheduled last pass - need to have a "global" already schedule? Do we have it already?
            If bLockExactSDSol = True Then
                If bgLockedAASD(kaaset, jaa) = True Then
                    bLockedAA(jaa) = True
                    basecompare = gLockedRxSD(kaaset, jaa)
                    LockedRx(jaa) = basecompare
                End If
            End If

            If bLockedAA(jaa) = True Then basecompare = LockedRx(jaa)

            If bLockedAA(jaa) = True Then 'Or (iMaxPresScheduled = 1 And bSpatialScheduledStand(jaa) = True) Then 'filter them out right away!
                For jrx = 1 To NRx
                    If basecompare <> AA_PresArray(jaa, jrx) Then
                        'CycleRxIn(jaa, jrx) = 0
                        Cycle2RxIn(jaa, jrx) = 0
                    Else
                        'CycleRxIn(jaa, jrx) = 1
                        Cycle2RxIn(jaa, jrx) = 1
                    End If
                Next
            End If

        Next jaa

        '3. Pare down the schedules to the desired max number
        'for now, keep all unscheduled poly options identified in the first trim and pare down, with brute force, to the max number in the Scheduled polys realm

        '' These are if you are using the neighbors to calculate the closest
        ''TrimEcon2(DualPlanAAPoly, Cycle2RxIn, NPValMxSpat, 1, FootprintPct)
        ''TrimMaxRxPerPoly2(DualPlanAAPoly, NPValMxSpat, BestSpatRx, Cycle2RxIn, iMaxPresScheduled, iMaxPresUnscheduled, KeepBestNS, bHighestValue, 1, 1)

        TrimMaxRxPerPoly2(StandNPValMx, BestSpatRx, Cycle2RxIn, MxPresPerPoly, KeepBestNS, 1, 1, CycleNum, ktrialset)


        '4. Write out the new files!
        'make sure at least the last chosen prescription and best non-spatial are included...
        For jaa As Integer = 1 To AA_NPolys
            'If jaa = 1538 Then Stop
            NRx = AA_NPres(jaa)
            'filter out all other prescriptions that have not been chosen
            'If BestSpatRx(jaa) > 0 Then 'this prescription was chosen
            For jrx = 1 To NRx
                RxIn(jaa, jrx) = Cycle2RxIn(jaa, jrx)
                krx = AA_PresArray(jaa, jrx)
                If krx = BestSpatRx(jaa) And bSpatialScheduledStand(jaa) = True And bLockedAA(jaa) = False Then
                    RxIn(jaa, jrx) = 1 'chosen prescription
                    'If KeepBestNS = False Then Exit For
                End If
                'see if it's ever been scheduled and if so, make sure it's included.
                If bLockedAA(jaa) = False And RxIn(jaa, jrx) = 0 Then
                    For jlock As Integer = 0 To NumDPCycles
                        If krx = AllSchedPolySolution(jaa, jlock) Then RxIn(jaa, jrx) = 1
                        If krx = AllUnschedPolySolution(jaa, jlock) Then RxIn(jaa, jrx) = 1 'note: should really only be the best non-spatial...
                    Next
                End If
            Next
            If KeepBestNS = True And bLockedAA(jaa) = False Then RxIn(jaa, MaxNSRx(jaa)) = 1 'best non-spatial
            If bSpatialScheduledStand(jaa) = False Then RxIn(jaa, MaxNSRx(jaa)) = 1 'best non-spatial for unscheduled stand
            If KeepBestNS = False And bSpatialScheduledStand(jaa) = True Then RxIn(jaa, MaxNSRx(jaa)) = 0 'filter out best non-spatial if you don't want to keep it
        Next


        FinalKeepZoneSpaceTally(RxIn)

        WriteDPSpaceTrimInfo(ksubfor, RxIn, False)
        WriteTrimmedIZoneAndHexInfo(ksubfor)



    End Sub

    Public Sub UseLastIterRxTrimmer(ByVal ksubfor As Integer)


        'looks up the prescriptions passed to DP last iteration, re-writes trimmer files based on updated price estimates
        ReDim RxIn(AA_NPolys, MxStandRx)
        LoadRxLUT(FnDualPlanOutBaseA & "_RxLUTFirst." & CStr(ksubfor), RxIn)
        FinalKeepZoneSpaceTally(RxIn)
        WriteDPSpaceTrimInfo(ksubfor, RxIn, False)
        WriteTrimmedIZoneAndHexInfo(ksubfor)

    End Sub

    Private Sub DualPlanRxEcon(ByRef iRxIn(,) As Byte, ByRef MxNSRx() As Integer) ', ByVal FirstTryPerIter As Boolean) ', ByRef DlPlM1Pres() As Model1Pres)


        'all this does is filter out the non-spatial prescriptions except for the best non-spatial
        'does NOT account for spatial interactions of neighboring stands and prescriptions
        'also should identify the maximum non-ispace solution for inclusion in the DP
        Dim jaa As Integer
        Dim jrx As Integer
        Dim mrx As Integer 'the non-ispace rx with the maximum NPV
        Dim krx As Integer
        Dim ktic As Integer
        Dim kcoversite As Integer
        Dim kage As Integer
        Dim kspat As Integer
        Dim kind As Integer
        Dim NRx As Integer
        Dim MaxNSNPV As Double 'max non-spatial npv
        Dim NPVLbnd As Double
        Dim AAArea As Double 'the area of the AA
        Dim InteriorISpaceAreaRatio As Double
        Dim LastExTic As Integer = 0
        Dim FirstRgTic As Integer = 0
        Dim LastRgTic As Integer = 0


        For jaa = 1 To AA_NPolys
            'refresh the parameters
            'If jaa = 512 Then Stop
            NPVLbnd = 0
            If AA_polyid(jaa) > 0 Then
                'If AA_polyid(jaa) = 5107 Then Stop
                AAArea = AA_area(jaa)
                InteriorISpaceAreaRatio = DualPlanAAPoly_ISpaceSelf(jaa) / AAArea

                NRx = AA_NPres(jaa)
                MaxNSNPV = -9999999999

                mrx = 0
                For jrx = 1 To NRx
                    krx = AA_PresArray(jaa, jrx)
                    kind = AApres_ISpaceInd(krx)
                    kspat = AApres_SpaceType(krx)
                    kcoversite = AApres_Coversite(krx, ktic)
                    kage = AApres_Age(krx, ktic)

                    'DEFAULT TO INCLUDE THE RX
                    iRxIn(jaa, jrx) = 1

                    'want to only keep in the ASPATIAL Rx with the greatest npv...so...
                    If SpatialSeries(kind).PersistentSpatial = False Then
                        iRxIn(jaa, jrx) = 0
                    End If
                    If SpatialSeries(kind).PersistentSpatial = False And DualPlanAAPoly_NPVSelf(jaa, jrx) > MaxNSNPV Then
                        mrx = jrx
                        MaxNSNPV = DualPlanAAPoly_NPVSelf(jaa, jrx)
                    End If

                    If jrx = 1 Then
                        NPVLbnd = DualPlanAAPoly_NPVSelf(jaa, jrx)
                    Else
                        If DualPlanAAPoly_NPVSelf(jaa, jrx) > NPVLbnd Then NPVLbnd = DualPlanAAPoly_NPVSelf(jaa, jrx)
                    End If

                Next jrx
                DualPlanAAPoly_NPValLbnd(jaa) = NPVLbnd
                'keep the non-spatial rx in the solution IF it is larger than NPVLbnd. mrx flags this as you are going through
                If DualPlanAAPoly_NPVSelf(jaa, mrx) >= NPVLbnd - 0.0001 Then
                    iRxIn(jaa, mrx) = 1
                    MxNSRx(jaa) = mrx
                End If

            End If
        Next

    End Sub

    Private Sub TrimEcon(ByRef iRxIn(,) As Byte, ByRef RxValueWithSpace(,) As Single, ByVal TrimTol As Double, ByVal ISpaceRatioVal As Single)
        'looks at the total ISpace potential of the stands in an influence
        ' zone and compares that to the current prescription of the stand being evaluated. Also tracks
        ' influence zones where the alignment potential is 0, thus eliminating the zone from need for 
        ' consideration

        'sub trims out spatial max prescriptions that are less than the lowest non-spatial NPV value
        ' so...even if the stand got credit for ALL of the interior space it could create + outside its borders, it couldn't compete 
        ' with the best "NPVLbnd" - 'spatial minimum' Rx, then trim it out...

        'uses the AA, AAPres, AAFlow as well as the Spatial, Market, and Condition set shadow price data
        'to calculate the NPV values for all DualPlan model 1 prescriptions...

        ' TrimTol used to control prescriptions that aren't at least this much higher than the best NPV without considering spatial interaction
        ' ISpaceRatioVal used to control how much "footprint" value the prescription gets when ranking...

        Dim jaa As Integer
        'Dim kaa As Integer 'the index of the anchor stand
        Dim zstand As Integer
        Dim jrx As Integer
        Dim NRx As Integer
        Dim NTrim As Integer = 1

        Dim krx As Integer
        Dim kind As Integer
        Dim kspat As Integer

        Dim jmap As Integer

        Dim AAArea As Double 'the area of the AA
        Dim zoneID As Integer

        Dim MaxTicAlign As Integer 'the maximum number of periods stuff can be in ispace and align...by spacetype

        Dim ValAdd As Double 'value to add to the prescription
        Dim ZoneArea As Double 'total area in the influence zone
        Dim TicMult As Integer
        Dim KeepRx As Integer 'whether to keep (1) or trim (0) the rx

        'GlobalTrim = 0

        'find the value of the 'footprint'
        'loop through the relevant izones, and assume that the suite of prescriptions that create the maximum 
        'value are chosen are the ones picked by the solver. What's the value of those prescriptions?
        'Is it more than the Lbnd value for the AA? If not, then trim it!

        'continue looping through all AAs and prescriptions until none are trimmed. After all, once you trim
        ' one, it may affect the value of another stand in the influence zone...

        ReDim AASpacePotential(AA_NPolys, NSpaceType, MxTic)
        ReDim KeepZoneSpace(NSpaceType, NZoneSubFor)
        'ReDim TrimmedEcon(AA_NPolys, MxStandRx)
        'default influence zone "keep" as 1
        For jspace As Integer = 1 To NSpaceType
            For jiz As Integer = 1 To NZoneSubFor
                KeepZoneSpace(jspace, jiz) = 1
            Next
        Next
        For jaa = 1 To AA_NPolys

            'now tally all periods of possible spatial contribution per stand. If any RxIn contributes a spatial value
            ' in any period, it gets set to a 1.
            For jspace As Integer = 1 To NSpaceType
                If TallySpacePotential(AASpacePotential, iRxIn, jaa, jaa, jspace) = 0 Then 'trim out the zone for this space type
                    For jiz As Integer = 1 To DualPlanAAPoly_NIZones(jaa)
                        zoneID = DualPlanAAPoly_IZones(jaa, jiz)
                        KeepZoneSpace(jspace, zoneID) = 0 'Assumes all polys in the zone must have at least one period of space production
                    Next jiz
                End If
            Next
        Next jaa


        Do Until NTrim = 0
            NTrim = 0
            For jaa = 1 To AA_NPolys
                AAArea = AA_area(jaa)
                If AA_polyid(jaa) > 0 Then
                    'kaa = jaa
                    NRx = AA_NPres(jaa) 'AA(jaa).PresArray.Length - 1
                    AAArea = AA_area(jaa)
                    DualPlanAAPoly_NPVallMx(jaa) = DualPlanAAPoly_NPValLbnd(jaa) 'default
                    ReDim MapLayerColor(NMapLayer)
                    For jmap = 1 To NMapLayer
                        MapLayerColor(jmap) = AA_MapLayerColor(jaa, jmap)
                    Next jmap

                    'now go through and for each prescription of the stand, compare potential ISpace to the Zone's potential
                    For jrx = 1 To NRx
                        KeepRx = 0
                        krx = AA_PresArray(jaa, jrx) 'krx used to access line in prescription database
                        kind = AApres_ISpaceInd(krx)
                        kspat = AApres_SpaceType(krx)

                        MaxTicAlign = SpatialSeries(kind).NNonZero

                        'has the rx been trimmed (want the non-trimmed) and is it a <potential> spatial rx?
                        If iRxIn(jaa, jrx) = 1 And MaxTicAlign > 0 Then 'If TrimPoly(kaa).RxIn(jrx) = 1 And MaxTicAlign > 0 Then ' FIGURE THE MAX VALUE FOR EACH RX And DlPlM1Poly(kaa).NPVSelf(jrx) < DlPlM1Poly(kaa).NPValLbnd Then 'only need to evaluate the Rx if it's less than the LBnd
                            'up the feasible prescriptions of the other stands in all influence zones around the 
                            'current polygon

                            ''#########REQUIRED...IF YOU HAVE GOTTEN TO THIS POINT, NEED TO CHECK TO SEE IF YOU NEED TO trim OR NOT
                            ValAdd = 0
                            For jiz As Integer = 1 To DualPlanAAPoly_NIZones(jaa) 'ON INPUT, THE 1-WAY INTERACTIONS ARE FILTERED OUT
                                zoneID = DualPlanAAPoly_IZones(jaa, jiz)
                                'If KeepRx = 1 Then Exit For
                                ZoneArea = 0
                                If KeepZoneSpace(kspat, zoneID) > 0 Then
                                    For jstand As Integer = 1 To Zonedim(zoneID) 'get whether the izone lines up for this period
                                        ZoneArea = ZoneArea + ZoneAAlistAreaS(zoneID, jstand)
                                    Next jstand
                                    'If MaxTicAlign > 0 Then 'only need to check this spatial def if there are values

                                    For jtic As Integer = SpatialSeries(kind).First1Tic To SpatialSeries(kind).Last1tic 'COULD ITERATE FROM FIRST TIC FLOW WITH A '1' TO THE LAST
                                        TicMult = 1
                                        For jstand As Integer = 1 To Zonedim(zoneID) 'get whether the izone lines up for this period
                                            zstand = ZoneAAlist(zoneID, jstand)
                                            TicMult = TicMult * AASpacePotential(zstand, kspat, jtic)
                                            'TicMult = TicMult * SpatialSeries(kkind).Flows(jtic) 'DlPlM1Poly(zstand).ISpaceSeries(MaxMatchRx(jinf), jspat, jtic)
                                        Next jstand
                                        'now for this particular prescription!
                                        TicMult = TicMult * SpatialSeries(kind).Flows(jtic)
                                        ValAdd = ValAdd + TicMult * ISpaceRatioVal * (SpatTypePr(kspat, jtic) * (ZoneArea / AAArea)) 'divide by AAArea to translate this into a per-acre value

                                    Next jtic
                                    'End If
                                End If
                            Next jiz

                            'Tally the value of this prescription considering spatial interactions
                            RxValueWithSpace(jaa, jrx) = DualPlanAAPoly_NPVSelf(jaa, jrx) + ValAdd 'TrimPoly(kaa).NPValMxSpat(jrx) = TrimPoly(kaa).NPVSelf(jrx) + ValAdd

                            ''compare to npvaspatial because you have 1 way influence zones that come into account here - except make sure you keep at least the prescription that wins when it's a spatial prescription
                            If DualPlanAAPoly_NPVSelf(jaa, jrx) + ValAdd >= DualPlanAAPoly_NPValLbnd(jaa) * TrimTol Or DualPlanAAPoly_NPVSelf(jaa, jrx) = DualPlanAAPoly_NPValLbnd(jaa) Then
                                KeepRx = 1
                            End If

                            'if after you go through all this and you decide not to keep the Rx, then trim it
                            'only trim out spatial value prescriptions, though - still keep the best non-spatial
                            'If KeepRx = 0 And iRxIn(jaa, jrx) = 1 And SpatialSeries(kind).NNonZero > 0 Then 'If KeepRx = 0 And TrimPoly(kaa).RxIn(jrx) = 1 And SpatialSeries(kind).NNonZero > 0 Then 'DlPlM1Poly(kaa).RxISpace(jrx) = 1 Then
                            If KeepRx = 0 And iRxIn(jaa, jrx) = 1 And SpatialSeries(kind).PersistentSpatial = True Then
                                iRxIn(jaa, jrx) = 0
                                NTrim = NTrim + 1
                                GlobalTrim = GlobalTrim + 1

                                'refigure the potential of this stand in light of the trimmed prescription
                                If TallySpacePotential(AASpacePotential, iRxIn, jaa, jaa, kspat) = 0 Then 'trim out the zone for this space type AND reset AASpacePotential modified to trimmed prescription
                                    For jiz As Integer = 1 To DualPlanAAPoly_NIZones(jaa)
                                        zoneID = DualPlanAAPoly_IZones(jaa, jiz)
                                        KeepZoneSpace(kspat, zoneID) = 0
                                    Next jiz
                                End If
                            End If

                            'FIGURE THE NPVVALMAX
                            'If TrimPoly(kaa).NPValMxSpat(jrx) > TrimPoly(kaa).NPVallMx Then TrimPoly(kaa).NPVallMx = TrimPoly(kaa).NPValMxSpat(jrx)
                            If RxValueWithSpace(jaa, jrx) > DualPlanAAPoly_NPVallMx(jaa) Then DualPlanAAPoly_NPVallMx(jaa) = RxValueWithSpace(jaa, jrx)
                        End If 'has not been trimmed
                    Next jrx

                    'calculate the ratio of spatial max to spatial min (for this prescription(?)...to use in the aspatial schedule routine
                    'Looking to compute EstISpacePct or EstISpacePct()
                    If DualPlanAAPoly_NPVallMx(jaa) <> 0 Then
                        DualPlanAAPoly_EstISpacePct(jaa) = 1 - Math.Min(1, Math.Abs(DualPlanAAPoly_NPValLbnd(jaa) / DualPlanAAPoly_NPVallMx(jaa)))
                    Else
                        DualPlanAAPoly_EstISpacePct(jaa) = 0
                    End If

                End If 'polyID > 0
            Next jaa
        Loop

    End Sub

    Private Sub FilterStandsForDebug(ByRef cRxIn(,) As Byte, ByVal KeepAA() As Byte)
        For jaa As Integer = 1 To AA_NPolys
            If KeepAA(jaa) = 0 Then
                'filter out all prescriptions if the AA is not in
                For jrx As Integer = 1 To AA_NPres(jaa)
                    cRxIn(jaa, jrx) = 0
                Next
            End If
        Next
    End Sub
    Private Sub FilterStandsForDebug2(ByRef cRxIn(,) As Byte, ByVal KeepAA() As Byte)
        Dim krx As Integer
        For jaa As Integer = 1 To AA_NPolys
            If KeepAA(jaa) <> 1 Then
                'filter out all prescriptions if the AA is not in
                For jrx As Integer = 1 To AA_NPres(jaa)
                    krx = AA_PresArray(jaa, jrx)
                    If krx <> PolySolRx_ChosenRxInd(jaa) Then cRxIn(jaa, jrx) = 0
                Next
            End If
        Next
    End Sub

    Private Sub FinalKeepZoneSpaceTally(ByVal iRxIn(,) As Byte)

        Dim kspat As Integer
        Dim zoneID As Integer

        ReDim AASpacePotential(AA_NPolys, NSpaceType, MxTic)
        ReDim KeepZoneSpace(NSpaceType, NZoneSubFor)
        'ReDim TrimmedEcon(AA_NPolys, MxStandRx)
        'default influence zone "keep" as 1
        For jspace As Integer = 1 To NSpaceType
            For jiz As Integer = 1 To NZoneSubFor
                KeepZoneSpace(jspace, jiz) = 1
            Next
        Next

        For jspace As Integer = 1 To NSpaceType
            kspat = jspace
            For jaa As Integer = 1 To AA_NPolys
                'refigure the potential of this stand in light of the trimmed prescription
                If TallySpacePotential(AASpacePotential, iRxIn, jaa, jaa, kspat) = 0 Then 'trim out the zone for this space type AND reset AASpacePotential modified to trimmed prescription
                    For jiz As Integer = 1 To DualPlanAAPoly_NIZones(jaa)
                        zoneID = DualPlanAAPoly_IZones(jaa, jiz)
                        KeepZoneSpace(kspat, zoneID) = 0
                    Next jiz
                End If
            Next jaa
        Next jspace

    End Sub


    Private Sub TrimSpatialDom(ByRef iRxIn(,) As Byte)
        'For each prescription of each stand that has not been trimmed...
        '1. compare to each other non-trimmed prescription
        '2. If you find a spatially equal or superior prescription, then
        '3. Trim current prescription if the PNV is lower than that other prescription
        ' Subroutine uses NPVSelf (which includes the value of the interior of the stand)
        ' and only modifies the RxIn(jrx) flag...
        Dim jaa As Integer
        Dim kaa As Integer 'the index of the anchor stand
        Dim SpatialTicMatch As Boolean 'whether you get same or greater spatial benefit from the evaluated prescription
        Dim jrx As Integer
        Dim jjrx As Integer
        Dim NRx As Integer
        Dim NTrim As Integer = 1

        Dim krx As Integer
        Dim kind As Integer
        Dim kkrx As Integer
        Dim kkind As Integer
        Dim kspat As Integer
        Dim kkspat As Integer

        Dim MaxTicAlign As Integer 'the maximum number of periods stuff can be in ispace and align...by spacetype
        Dim MaxMatchTicAlign As Integer 'the influenced stand number of tics that align wihth ispace value
        'ReDim TrimmedSpatialDom(AA_NPolys, MxStandRx)

        'GlobalTrim = 0

        For jaa = 1 To AA_NPolys
            'AAArea = AA_area(jaa)
            If AA_polyid(jaa) > 0 Then
                kaa = jaa
                NRx = AA_NPres(jaa) 'AA(jaa).PresArray.Length - 1
                For jrx = 1 To NRx
                    'KeepRx = 0
                    If iRxIn(jaa, jrx) = 1 Then 'If TrimPoly(jaa).RxIn(jrx) = 1 Then
                        krx = AA_PresArray(kaa, jrx) 'krx used to access line in prescription database
                        kind = AApres_ISpaceInd(krx)
                        kspat = AApres_SpaceType(krx)

                        MaxTicAlign = SpatialSeries(kind).NNonZero
                        For jjrx = 1 To NRx
                            kkrx = AA_PresArray(kaa, jjrx) 'krx used to access line in prescription database
                            kkind = AApres_ISpaceInd(kkrx)
                            kkspat = AApres_SpaceType(kkrx)
                            MaxMatchTicAlign = SpatialSeries(kkind).NNonZero
                            SpatialTicMatch = True
                            'potential better prescription is still in, 
                            ' has the same space type, 
                            ' has a higher NPV
                            ' and has at least as many periods of spatial value as the current prescription,
                            ' and you're dealing with a spatial prescription...

                            'If jrx <> jjrx And TrimPoly(kaa).RxIn(jjrx) = 1 And kspat = kkspat _
                            '   And MaxMatchTicAlign >= MaxTicAlign And _
                            '   TrimPoly(kaa).NPVSelf(jjrx) >= TrimPoly(kaa).NPVSelf(jrx) And MaxTicAlign > 0 Then
                            If jrx <> jjrx And iRxIn(kaa, jjrx) = 1 And kspat = kkspat _
                               And MaxMatchTicAlign >= MaxTicAlign And _
                               DualPlanAAPoly_NPVSelf(kaa, jjrx) >= DualPlanAAPoly_NPVSelf(kaa, jrx) And MaxTicAlign > 0 Then
                                For jtic As Integer = SpatialSeries(kind).First1Tic To SpatialSeries(kind).Last1tic
                                    'if the parent Rx has a spatial flow and the potential dominant does not, then there is no spatial dominance
                                    If SpatialSeries(kind).Flows(jtic) = 1 And SpatialSeries(kkind).Flows(jtic) = 0 Then
                                        SpatialTicMatch = False
                                        Exit For
                                    End If
                                Next jtic
                                'you found a prescription with higher PNV and at least as good of a spatial flow
                                If SpatialTicMatch = True Then
                                    'TrimPoly(kaa).RxIn(jrx) = 0
                                    iRxIn(kaa, jrx) = 0
                                    GlobalTrim = GlobalTrim + 1
                                    Exit For 'don't have to look for another spatially dominant series since we're trimmed!
                                End If

                            End If
                        Next jjrx

                    End If
                Next jrx
            End If 'polyID > 0
        Next jaa
    End Sub

    Private Sub TrimMaxRxPerPoly2(ByRef PolyRxVal(,) As Single, ByVal ChosenRx() As Long, ByRef iRxIn(,) As Byte, ByVal GlobalMaxPres As Integer, ByVal BestNonSpatial As Boolean, ByVal TrimTol As Double, ByVal ISpaceRatioVal As Single, ByVal CycleNum As Integer, ByVal ktrialset As Integer)
        'Limits the maximum number of prescriptions for any poly - chosen as prescriptions closest in value to the chosen one.
        ' Clusters the selection around the chosen Rx value - above and below. In the case of needing to choose an even number,
        ' err on choosing one more higher (if feasible)

        Dim jaa As Integer
        Dim kaa As Integer 'the index of the anchor stand
        Dim jrx As Integer
        Dim irx As Integer
        Dim NRxKeep As Integer 'number of values/prescriptions to keep
        Dim NRx As Integer
        Dim NTrim As Integer = 1
        Dim PolyMaxNumPres As Integer


        Dim krx As Integer
        Dim kind As Integer
        Dim kbreakset As Integer
        Dim kbreak As Integer

        Dim OrderedArray() As Double
        Dim bChosenPoly As Boolean 'is the polygon in the solution?
        Dim CompareVal As Double 'if you have a polygon with a chosen prescription
        Dim CompareIndex As Integer 'index of the chosen prescription in the ordered array
        Dim FirstKeepIndex As Integer
        Dim LastKeepIndex As Integer
        Dim ValsToKeep() As Single
        Dim NKept As Integer 'number of prescriptions for this polygon not yet trimmed
        Dim ValueCompare As Double 'the value of the prescription in the Nth place of an ordered array - where N is the max # of pres to keep
        Dim pottrim As Integer
        Dim MaxNumPres As Integer

        'Dim TallyTrims As Integer 'how many prescriptions have been trimmed out

        'GlobalTrim = 0
        MaxNumPres = GlobalMaxPres

        For jaa = 1 To AA_NPolys
            'AAArea = AA_area(jaa)
            bChosenPoly = False 'default
            If ChosenRx(jaa) > 0 Then bChosenPoly = True
            If AA_polyid(jaa) > 0 Then
                kaa = jaa
                NRx = AA_NPres(jaa) 'AA(jaa).PresArray.Length - 1
                ReDim OrderedArray(NRx)
                NKept = 0
                OrderedArray(0) = -999999999 'really low value
                CompareVal = -999999999 'really low value - default
                For jrx = 1 To NRx
                    'KeepRx = 0
                    krx = AA_PresArray(kaa, jrx) 'krx used to access line in prescription database
                    If bChosenPoly = True And krx = ChosenRx(jaa) Then CompareVal = PolyRxVal(jaa, jrx)
                    kind = AApres_ISpaceInd(krx)
                    OrderedArray(jrx) = -999999999 'really low value
                    'If iRxIn(jaa, jrx) = 1 And SpatialSeries(kind).NNonZero > 0 Then    'If TrimPoly(jaa).RxIn(jrx) = 1 Then
                    If iRxIn(jaa, jrx) = 1 And SpatialSeries(kind).PersistentSpatial = True Then
                        OrderedArray(jrx) = PolyRxVal(jaa, jrx) 'TrimPoly(kaa).NPValMxSpat(jrx) 'include the maximum possible value from aligning with the neighbors
                        NKept = NKept + 1
                    End If
                Next jrx


                'find max pres for this polygon based on its area
                kbreakset = TrialBreakSet(ktrialset)
                For jbreak As Integer = 1 To TrialBreakSetNBreaks(kbreakset)
                    If AA_area(jaa) >= TrialStandMinSize(kbreakset, jbreak) And AA_area(jaa) < TrialStandMaxSize(kbreakset, jbreak) Then
                        kbreak = jbreak
                        Exit For
                    End If
                Next
                PolyMaxNumPres = NRx
                If bChosenPoly = False And TrialNUnsched(kbreakset, kbreak) > 0 Then PolyMaxNumPres = TrialNUnsched(kbreakset, kbreak)
                If bChosenPoly = True And TrialNSched(kbreakset, kbreak) > 0 Then PolyMaxNumPres = TrialNSched(kbreakset, kbreak)


                If (NKept > MaxNumPres And NKept > PolyMaxNumPres) Or TrialTrimType(ktrialset) = "First" Or TrialTrimType(ktrialset) = "Closest" Then 'bHighest = False Then
                    If (NKept > MaxNumPres) And (ChosenRx(jaa) <= 0 Or TrialTrimType(ktrialset) = "Highest") Then 'bHighest = True) Then
                        Array.Sort(OrderedArray)
                        ValueCompare = OrderedArray(OrderedArray.Length - MaxNumPres) 'largest values are at the end - find the lowest value to keep
                        For jrx = 1 To NRx
                            krx = AA_PresArray(kaa, jrx) 'krx used to access line in prescription database
                            kind = AApres_ISpaceInd(krx)
                            pottrim = iRxIn(jaa, jrx) 'TrimPoly(jaa).RxIn(jrx) 'first cut is current condition
                            If iRxIn(jaa, jrx) = 1 And PolyRxVal(jaa, jrx) < ValueCompare Then pottrim = 0 'If TrimPoly(jaa).RxIn(jrx) = 1 And TrimPoly(kaa).NPValMxSpat(jrx) < ValueCompare Then pottrim = 0
                            'If iRxIn(jaa, jrx) = 1 And SpatialSeries(kind).NNonZero = 0 And BestNonSpatial = True Then pottrim = 1 'if you have non-spatial option then keep it
                            If iRxIn(jaa, jrx) = 1 And SpatialSeries(kind).PersistentSpatial = False And BestNonSpatial = True Then pottrim = 1 'if you have non-spatial option then keep it
                            'now trim to the top N
                            If iRxIn(jaa, jrx) = 1 And pottrim = 0 Then GlobalTrim = GlobalTrim + 1
                            iRxIn(jaa, jrx) = pottrim

                        Next
                        'you are searching for the closest values to the chosen prescription
                    ElseIf bChosenPoly = True And (TrialTrimType(ktrialset) = "Closest" Or TrialTrimType(ktrialset) = "First") And NKept > PolyMaxNumPres Then 'bHighest = False Then
                        Array.Sort(OrderedArray)
                        Array.Reverse(OrderedArray)
                        ValueCompare = CompareVal
                        ReDim ValsToKeep(NRx)
                        For jrx = 0 To NRx - 1 '0-based OrderedArray
                            If CompareVal = OrderedArray(jrx) Then
                                CompareIndex = jrx
                                Exit For
                            End If
                        Next
                        'find min and max ordered array indices to keep
                        If CompareIndex - (PolyMaxNumPres / 2) < 0 Then 'you are skewed to having chosen the most positive prescription
                            FirstKeepIndex = 0
                            LastKeepIndex = PolyMaxNumPres - 1 'adjusted to 0 starting index of OrderedArray
                        ElseIf CompareIndex + (PolyMaxNumPres / 2) > NRx - 1 Then
                            LastKeepIndex = NRx - 1 'adjusted to 0 starting index of OrderedArray
                            FirstKeepIndex = LastKeepIndex - PolyMaxNumPres + 1
                        Else
                            If PolyMaxNumPres Mod 2 <> 0 Then 'Math.Round(MaxNumPres / 2) - (MaxNumPres / 2) <> 0 Then 'odd number
                                FirstKeepIndex = CompareIndex - (PolyMaxNumPres - 1) / 2 'EXAMPLE: compareindex = 5, MaxnumPres = 5, keep indices 3-7
                            Else 'even number
                                FirstKeepIndex = CompareIndex - PolyMaxNumPres / 2 'EXAMPLE: compareindex = 5, MaxNumPres = 6, keep indices 2-7
                            End If
                            LastKeepIndex = FirstKeepIndex + (PolyMaxNumPres - 1)
                        End If

                        'populate the values to keep
                        irx = 0
                        NRxKeep = 0
                        For jrx = FirstKeepIndex To LastKeepIndex
                            irx = irx + 1
                            NRxKeep = NRxKeep + 1
                            ValsToKeep(irx) = OrderedArray(jrx)
                        Next

                        'find the appropriate prescriptions
                        For jrx = 1 To NRx
                            krx = AA_PresArray(kaa, jrx) 'krx used to access line in prescription database
                            kind = AApres_ISpaceInd(krx)
                            pottrim = 0
                            For irx = 1 To NRxKeep
                                If ValsToKeep(irx) = PolyRxVal(jaa, jrx) Then
                                    pottrim = 1
                                    Exit For
                                End If
                            Next
                            If iRxIn(jaa, jrx) = 1 And pottrim = 0 Then GlobalTrim = GlobalTrim + 1
                            iRxIn(jaa, jrx) = pottrim
                        Next
                    End If
                    'assume you have trimmed polygons out from here
                    'UpdateRxValsFromLatestTrim(TrimPoly, jaa, iRxIn, PolyRxVal, TrimTol, ISpaceRatioVal)
                End If 'if NKept > MaxRxPerPolly

            End If 'polyID > 0
        Next jaa
    End Sub


    Private Sub LoadRxLUT(ByVal RxLUT As String, ByRef cRxIn(,) As Byte)

        Dim dummy As String
        Dim arr() As String
        Dim kaa As Integer
        Dim krx As Integer

        'figure out which solution was chosen per polygon
        Dim fnum2 As Integer = FreeFile()
        FileOpen(fnum2, RxLUT, OpenMode.Input)
        dummy = LineInput(fnum2)
        Do Until EOF(fnum2)
            dummy = Trim(LineInput(fnum2))
            If dummy.Length < 1 Then Exit Do
            arr = Split(dummy, ",")
            kaa = arr(0)
            krx = arr(3)

            For jrx As Integer = 1 To AA_NPres(kaa)
                If krx = AA_PresArray(kaa, jrx) Then
                    cRxIn(kaa, jrx) = 1
                    Exit For 'only one match per line
                End If
            Next
        Loop
        FileClose(fnum2)

    End Sub

    Private Sub WriteDlPlTrimInfo(ByVal outfile As String)
        'sub spits out the per poly TrimPolyData.EstISpacePct information for use in dualplan aspatial iterations
        Dim Fnum As Integer = FreeFile()

        FileOpen(Fnum, outfile, OpenMode.Output)

        For j As Integer = 1 To AA_NPolys
            PrintLine(Fnum, DualPlanAAPoly_polyid(j) & " " & DualPlanAAPoly_EstISpacePct(j))
        Next
        FileClose(Fnum)
    End Sub

    Private Sub WriteDPSpaceTrimInfo(ByVal ksubfor As Integer, ByVal iRxIn(,) As Byte, ByVal bFirstDP As Boolean)

        Dim SpatialRxCount As Integer = 0
        Dim PolySpatialRxCount As Integer = 0 'how many 
        Dim NPolyRx As Integer = 0
        Dim CountSpatialPolys As Integer = 0 'used to formulate new StandID
        Dim Mxkind As Integer
        Dim parea As Double
        Dim krx As Integer
        Dim kind As Integer
        Dim SpatialRxPerAA() As Integer
        Dim pDP As Integer 'signals whether you are printing to the DP Input files

        'HOW MANY PRESCRIPTIONS ARE PRINTED OUT IN THIS SUBFOREST?
        ReDim SpatialRxPerAA(AA_NPolys)
        ReDim DPSpaceStandID_LUT(AA_NPolys) 'indexed by the dualplan stand id
        For jaa As Integer = 1 To AA_NPolys
            DualPlanAAPoly_PolyRxSpace(jaa) = 0 'default
            NPolyRx = 0
            For jrx As Integer = 1 To AA_NPres(jaa) 'AA(jaa).PresArray.Length - 1
                krx = AA_PresArray(jaa, jrx) 'krx used to access line in prescription database
                kind = AApres_ISpaceInd(krx)
                If iRxIn(jaa, jrx) = 1 Then 'DualPlanAAPoly(jaa).RxIn(jrx) = 1 Then 'RxTally = RxTally + 1 'And DualPlanModel1Poly(jaa).RxISpace(jrx) = 1 Then RxTally = RxTally + 1
                    'if there is a prescription that's kept that makes ISpace then flag the poly as needing to be passed to DPSpace
                    If SpatialSeries(kind).NNonZero > 0 Then DualPlanAAPoly_PolyRxSpace(jaa) = 1
                    'RxCount = RxCount + 1
                    NPolyRx = NPolyRx + 1
                End If
            Next jrx
            'control for 'blank' aa's with no Rx identified...in this output file, it'll at least get 1 Rx
            'If NPolyRx = 0 Then RxCount = RxCount + 1
            SpatialRxPerAA(jaa) = NPolyRx * DualPlanAAPoly_PolyRxSpace(jaa) 'totals the spatial and non-spatial prescriptions for any poly that has at least 1 spatial Rx that is "in"
            SpatialRxCount = SpatialRxCount + SpatialRxPerAA(jaa)
            NSpatialPolys = NSpatialPolys + DualPlanAAPoly_PolyRxSpace(jaa) '1 used to flag wheter it produces ISpace, can also be used to tally total stands capable of producing ISpace
        Next jaa

        'SPATIAL INDEX BY RX DATA FILE

        FileOpen(86, FnTrimmeroutBaseA & "_Iter" & IterationNumber & "_PresDataSpat_sf" & CStr(ksubfor) & ".csv", OpenMode.Output)
        PrintLine(86, "NpresSubFor")
        PrintLine(86, SpatialRxCount)
        PrintLine(86, "ISpatSeriesAlt, IBufferSeriesAlt, IEdgeSeriesAlt")

        'RX DATA FILE 

        FileOpen(36, FnDualPlanOutBaseA & "_Iter" & IterationNumber & "_PresDataNPV_sf" & CStr(ksubfor) & ".csv", OpenMode.Output)
        PrintLine(36, "NpresSubFor")
        PrintLine(36, SpatialRxCount)

        'POLY DATA FILE

        FileOpen(26, FnTrimmeroutBaseA & "_Iter" & IterationNumber & "_PolyData_sf" & CStr(ksubfor) & ".csv", OpenMode.Output)
        PrintLine(26, "NstandSubfor")
        PrintLine(26, NSpatialPolys)
        PrintLine(26, "NAltStand, StandSpaceType, StandArea")

        FileOpen(27, BaseNameAAnaltAAareaInfilesA & "_sf" & ksubfor & ".csv", OpenMode.Output)
        PrintLine(27, "kaa, NAlt, Area!")

        'PRESCRIPTION NUMBER LOOK UP TABLE
        FileOpen(11, FnDualPlanOutBaseA & "_RxLUT." & CStr(ksubfor), OpenMode.Output)
        PrintLine(11, "StandID, DPSpaceStandID, StandRx, RxNumber")


        'DEBUG TRIMMER DUMP
        FileOpen(43, FnTrimmeroutBaseA & "_Iter" & IterationNumber & "_TrimmerDump_sf" & CStr(ksubfor) & ".csv", OpenMode.Output)
        PrintLine(43, "jaa, AA_area, jrx, pDP, DP_StandID, NPolyRx, krx, NPVSelf(jrx)")


        Dim spatprint As Integer
        'RxCount = 0
        CountSpatialPolys = 0
        For jaa As Integer = 1 To AA_NPolys

            Mxkind = 0
            NPolyRx = 0
            pDP = DualPlanAAPoly_PolyRxSpace(jaa)
            CountSpatialPolys = CountSpatialPolys + DualPlanAAPoly_PolyRxSpace(jaa)
            DPSpaceStandID_LUT(jaa) = 0
            If DualPlanAAPoly_PolyRxSpace(jaa) = 1 Then
                DPSpaceStandID_LUT(jaa) = CountSpatialPolys
                'PrintLine(12, jaa & "," & DPSpaceStandID_LUT(jaa))
            End If

            For jrx As Integer = 1 To AA_NPres(jaa) 'AA(jaa).PresArray.Length - 1
                If iRxIn(jaa, jrx) = 1 Then 'DualPlanAAPoly(jaa).RxIn(jrx) = 1 Then 'RxTally = RxTally + 1 'And DualPlanModel1Poly(jaa).RxISpace(jrx) = 1 Then RxTally = RxTally + 1
                    'RxCount = RxCount + 1
                    NPolyRx = NPolyRx + 1
                    krx = AA_PresArray(jaa, jrx)
                    kind = AApres_SpaceType(krx)
                    If kind > Mxkind Then Mxkind = kind
                    spatprint = 0
                    If pDP Then
                        PrintLine(36, DualPlanAAPoly_NPVSelf(jaa, jrx)) 'this includes the value of the 1-way influence zones, as calculated in the DualPlanRxEcon Sub
                        PrintLine(86, AApres_ISpaceInd(krx) & "," & AApres_QBuffInd(krx) & "," & AApres_EdgeInd(krx))
                    End If
                    PrintLine(11, jaa & "," & DPSpaceStandID_LUT(jaa) & "," & NPolyRx & "," & krx)
                    PrintLine(43, jaa & "," & AA_area(jaa) & "," & jrx & "," & pDP & "," & DPSpaceStandID_LUT(jaa) & "," & NPolyRx & "," & krx & "," & DualPlanAAPoly_NPVSelf(jaa, jrx))
                End If
            Next

            If NPolyRx = 0 Then
                NPolyRx = 1
                PrintLine(11, jaa & ",0,1,0")
            End If
            'count the number of untrimmed prescriptions for this polygon
            DualPlanAAPoly_NRxIn(jaa) = NPolyRx

            parea = AA_area(jaa)
            If parea = 0 Then parea = 999 'Dummy for now #############
            If pDP Then
                PrintLine(26, NPolyRx & "," & Mxkind & "," & parea)
                PrintLine(27, DPSpaceStandID_LUT(jaa) & "," & NPolyRx & "," & parea)
            End If
        Next

        FileClose(26)
        FileClose(27)
        FileClose(36)
        FileClose(86)
        FileClose(11)
        'dump/debug file
        FileClose(43)

        'IF FIRST DP AND USING FIRST DP TRIMS FOR CYCLES, THEN WRITE OUT A LUT
        If bFirstDP = True Then _
            FileCopy(FnDualPlanOutBaseA & "_RxLUT." & CStr(ksubfor), FnDualPlanOutBaseA & "_RxLUTFirst." & CStr(ksubfor))

    End Sub
    Private Sub WriteTrimmedIZoneAndHexInfo(ByVal ksubfor As Integer)
        'This subroutine filters out hexagons that are not in a spatial stand
        'also filters out influence zones that cannot produce core based on their available prescriptions

        'subroutine uses DualPlanSubForests().HexStandsFile and DualPlanSubforests().IZonesFile
        ' Also uses DPSpaceStandID_LUT() to filter hexagons and stands

        Dim NHex As Integer
        'Dim SubForNHex As Long = 0
        Dim NSpatialHex As Long 'count the number of hexagons to print out
        Dim minx As Double
        Dim maxx As Double
        Dim miny As Double
        Dim maxy As Double
        Dim MxDistanceBetweenAdjHexCenters As Double
        Dim HexPolys_PolyID() As Integer
        Dim HexPolys_StandID() As Integer
        Dim HexPolys_xVal() As Double
        Dim HexPolys_yVal() As Double
        Dim HexPolys_AdjPolyInd(,) As Integer
        Dim dummy As String
        Dim dummy1 As String
        Dim DPHexID() As Long 'reassigned hexagon ID of hexagon going into DPSpace
        Dim f11 As Integer
        Dim f22 As Integer

        'define the field numbers
        Dim HexIDField As Integer = 0
        Dim StandIDField As Integer = 1
        Dim XField As Integer = 2
        Dim YField As Integer = 3
        Dim StartAdjField As Integer = 4

        Dim tind As Integer = 0
        f11 = FreeFile()
        FileOpen(f11, DualPlanSubForests(ksubfor).HexStandsFile, OpenMode.Input)
        dummy = LineInput(f11)
        dummy = LineInput(f11)
        Atts = Split(dummy, ",")
        NHex = Atts(0)
        MxDistanceBetweenAdjHexCenters = Atts(6)

        ReDim HexPolys_PolyID(NHex)
        ReDim HexPolys_StandID(NHex)
        ReDim HexPolys_xVal(NHex)
        ReDim HexPolys_yVal(NHex)
        ReDim HexPolys_AdjPolyInd(NHex, 5)
        ReDim DPHexID(NHex)
        dummy = LineInput(f11)

        Do Until EOF(f11)
            dummy = Trim(LineInput(f11))
            If Len(dummy) > 0 Then
                Atts = Split(dummy, ",")
                'test to see if this hexagon is in a defined stand
                If DPSpaceStandID_LUT(Atts(StandIDField)) > 0 Then 'means the stand was sent to dpspace
                    tind = tind + 1
                    DPHexID(Atts(HexIDField)) = tind
                    HexPolys_PolyID(tind) = tind 'Atts(HexIDField)
                    HexPolys_StandID(tind) = DPSpaceStandID_LUT(Atts(StandIDField)) 'the mapped stand id
                    HexPolys_xVal(tind) = Atts(XField)
                    HexPolys_yVal(tind) = Atts(YField)
                    'Poly indexes of adjacent polygons...
                    For j As Integer = 0 To 5
                        HexPolys_AdjPolyInd(tind, j) = Atts(j + StartAdjField) 'this will load the original hex id - have to print out the mapped hex id
                    Next

                    'determine min and max of that which is left in the forest
                    If tind = 1 Then
                        minx = HexPolys_xVal(tind)
                        maxx = HexPolys_xVal(tind)
                        miny = HexPolys_yVal(tind)
                        maxy = HexPolys_yVal(tind)
                    Else
                        If HexPolys_xVal(tind) < minx Then minx = HexPolys_xVal(tind)
                        If HexPolys_xVal(tind) > maxx Then maxx = HexPolys_xVal(tind)
                        If HexPolys_yVal(tind) < miny Then miny = HexPolys_yVal(tind)
                        If HexPolys_yVal(tind) > maxy Then maxy = HexPolys_yVal(tind)
                    End If
                End If
            End If
        Loop
        NSpatialHex = tind 'tally how many hexagons made it through
        FileClose(f11)

        f22 = FreeFile()
        FileOpen(f22, BaseNameHexDataOutfilesA & "_Hex_sf" & ksubfor & ".csv", OpenMode.Output)
        PrintLine(f22, "SubForNHex,SubForNAA,MnXlocS,MxXlocS,MnYlocS,MxYlocS,MxDistanceBetweenAdjHexCenters")
        PrintLine(f22, NSpatialHex & "," & NSpatialPolys & "," & minx & "," & maxx & "," & miny & "," & maxy & "," & MxDistanceBetweenAdjHexCenters)
        PrintLine(f22, "jHex,HexSubForAA,HexXloc,HexYloc,AdjHx(0),AdjHx(1),AdjHx(2),AdjHx(3),AdjHx(4),AdjHx(5)")
        For jpoly As Integer = 1 To NSpatialHex

            dummy1 = ""
            For jside As Integer = 0 To 5
                dummy1 = dummy1 & "," & DPHexID(HexPolys_AdjPolyInd(jpoly, jside))
            Next

            PrintLine(f22, HexPolys_PolyID(jpoly) & "," & HexPolys_StandID(jpoly) & "," & HexPolys_xVal(jpoly) & "," & _
                     HexPolys_yVal(jpoly) & dummy1)

        Next
        FileClose(f22)

        'Now process the influence zone file
        'ASSUMES THAT THE INFLUENCE ZONE FILE HAS ALREADY BEEN LOADED

        Dim NSpatialZones As Integer = 0 'counts how many influence zones made it through the trimmer
        Dim KeepZone() As Byte 'whether to keep the zone for printing or not - dimensioned by the number of IZones
        Dim dumstring As String
        Dim pZoneCount As Integer = 0 'counting the izones you are printing
        Dim TotalZoneRxCombos As Long 'prescription dimension of the influence zone
        Dim kaa As Integer

        ReDim KeepZone(NZoneSubFor)

        For jzone As Integer = 1 To NZoneSubFor
            KeepZone(jzone) = 0
            'default is initially set to whether any space types keep the zone, determined in TrimEcon2
            For jspace As Integer = 1 To NSpaceType
                If KeepZoneSpace(jspace, jzone) > 0 Then
                    KeepZone(jzone) = 1
                    Exit For
                End If
            Next

            If Zonedim(jzone) > 1 And KeepZone(jzone) = 1 Then 'filter out 1-way influence zones. Their value is already included in the stand's NPV anyway
                'go through the stands in the zone -if any one is NOT passed to the DP, the zone should be omitted
                'now check for whether the total number of prescription combos in the zone is greater than the defined threshhold
                TotalZoneRxCombos = 1 'reset
                For jdim As Integer = 1 To Zonedim(jzone)
                    kaa = ZoneAAlist(jzone, jdim)
                    If DPSpaceStandID_LUT(kaa) = 0 Then KeepZone(jzone) = 0

                    'TEST FOR TOTAL NUMBER OF PRESCRIPTIONS TO BE GREATER THAN A FILTER
                    TotalZoneRxCombos = TotalZoneRxCombos * DualPlanAAPoly_NRxIn(kaa)
                    If TotalZoneRxCombos > ZoneRxFilter And ZoneRxFilter > 0 Then
                        KeepZone(jzone) = 0
                        Exit For
                    End If
                Next
                If KeepZone(jzone) = 1 Then NSpatialZones = NSpatialZones + 1
            Else
                KeepZone(jzone) = 0
            End If
        Next jzone

        f22 = FreeFile()
        FileOpen(f22, BaseNameHexDataOutfilesA & "_Zones_sf" & ksubfor & ".csv", OpenMode.Output) 'for DPForm format
        PrintLine(f22, "NZoneSubFor, MxZoneDimInList")
        PrintLine(f22, NSpatialZones & "," & MXZoneDim)
        Dim dprint As String = ""
        dprint = "Zone_#,Dimension"
        For jzone As Integer = 1 To MXZoneDim
            dprint = dprint & ",AA" & jzone
        Next
        For jzone As Integer = 1 To MXZoneDim
            dprint = dprint & ",AA" & jzone & "area"
        Next
        PrintLine(f22, dprint)

        pZoneCount = 0
        For jzone As Integer = 1 To NZoneSubFor
            If KeepZone(jzone) = 1 Then
                pZoneCount = pZoneCount + 1
                dumstring = ""

                'dpspace stand IDs
                For jdim As Integer = 1 To MXZoneDim
                    dumstring = dumstring & "," & DPSpaceStandID_LUT(ZoneAAlist(jzone, jdim))
                Next jdim

                'areas in each stand within the izone
                For jdim As Integer = 1 To MXZoneDim
                    dumstring = dumstring & "," & ZoneAAlistAreaS(jzone, jdim)
                Next jdim

                PrintLine(f22, pZoneCount & "," & Zonedim(jzone) & dumstring)
            End If
        Next
        FileClose(f22)


    End Sub

    Private Function DetermineUnqInteger(ByVal test As Integer, ByVal Array() As Integer) As Boolean
        DetermineUnqInteger = True
        For j As Integer = 1 To Array.Length - 1
            If test = Array(j) Then
                DetermineUnqInteger = False
                Exit Function
            End If
        Next
    End Function
    Private Function TallySpacePotential(ByRef btAASpacePotential(,,) As Byte, ByVal iRxIn(,) As Byte, ByVal kaa As Integer, ByVal SpatPotIndex As Integer, ByVal kspacetype As Integer) As Integer '(ByVal trimpoly() As TrimPolyData, ByVal kaa As Integer, ByVal kspacetype As Integer) As Integer
        'populates the periods of when the non-trimmed prescriptions of a stand can produce ISpace
        'also returns the number of periods of potential ISpace creation
        Dim NRx As Integer
        Dim krx As Integer
        Dim kind As Integer
        Dim kspat As Integer
        Dim MaxTicAlign As Integer
        Dim TicValue() As Integer
        ReDim TicValue(MxTic)
        For jtic As Integer = 0 To MxTic
            TicValue(jtic) = 0
        Next

        TallySpacePotential = 0

        NRx = AA_NPres(kaa)
        For jrx As Integer = 1 To NRx
            krx = AA_PresArray(kaa, jrx) 'krx used to access line in prescription database
            kind = AApres_ISpaceInd(krx)
            kspat = AApres_SpaceType(krx)
            MaxTicAlign = SpatialSeries(kind).NNonZero

            'has the rx been trimmed (want the non-trimmed) and is it a <potential> spatial rx?
            'If trimpoly(kaa).RxIn(jrx) = 1 And MaxTicAlign > 0 And kspat = kspacetype Then ' FIGURE THE MAX VALUE FOR EACH RX And DlPlM1Poly(kaa).NPVSelf(jrx) < DlPlM1Poly(kaa).NPValLbnd Then 'only need to evaluate the Rx if it's less than the LBnd
            If iRxIn(kaa, jrx) = 1 And MaxTicAlign > 0 And kspat = kspacetype Then
                For jtic As Integer = SpatialSeries(kind).First1Tic To SpatialSeries(kind).Last1tic
                    TicValue(jtic) = TicValue(jtic) + SpatialSeries(kind).Flows(jtic)
                Next
            End If
        Next jrx

        'now set the values
        For jtic As Integer = 1 To MxTic
            btAASpacePotential(SpatPotIndex, kspacetype, jtic) = 0
            If TicValue(jtic) > 0 Then
                btAASpacePotential(SpatPotIndex, kspacetype, jtic) = 1 'by AA, SpaceType, Tic
                TallySpacePotential = TallySpacePotential + 1
            End If
        Next

    End Function



    Private Function DetermineKTrialSet(ByVal CycleNum As Integer) As Integer
        DetermineKTrialSet = 0
        For ktrial As Integer = 1 To NTrialSets
            If CycleNum >= TrialSetBegTrial(ktrial) And CycleNum <= TrialSetEndTrial(ktrial) Then
                DetermineKTrialSet = ktrial
                Exit Function
            End If
        Next
    End Function

End Class


