Option Strict Off
Option Explicit On
Friend Class MType
	Inherits System.Windows.Forms.Form
    Dim MtypePrBase(,,) As Single
    Dim MtypeUnits(,,) As Single
	Dim whichMType1 As Short
	Dim whichMType2 As Short
	Dim whichMLoc1 As Object
	Dim whichMLoc2 As Object
	
	
	Private Sub MType_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Dim jx As Object
		Dim UnitQuan As Single
		Dim UnitPrice As Single
		Dim jdef As Object
		Dim jloc As Object
		Dim jtype As Object
		Dim jtic As Object
		Dim jmloc As Object
		Dim jmtype As Object
		'UPGRADE_WARNING: Lower bound of array MtypePrBase was changed from 1,1,1 to 0,0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		ReDim MtypePrBase(NMType, NMLocation, NTic1)
		'UPGRADE_WARNING: Lower bound of array MtypeUnits was changed from 1,1,1 to 0,0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		ReDim MtypeUnits(NMType, NMLocation, NTic1)
		
		'initialize to 1 to prevent divion by zero for any type not used
		For jmtype = 1 To NMType
			For jmloc = 1 To NMLocation
				For jtic = 1 To NTic1
					'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					'UPGRADE_WARNING: Couldn't resolve default property of object jmloc. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					'UPGRADE_WARNING: Couldn't resolve default property of object jmtype. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					MtypeUnits(jmtype, jmloc, jtic) = 1
				Next jtic
			Next jmloc
		Next jmtype
		
		
		
		
		whichMType1 = 1
		whichMType2 = 2
		'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc1. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		whichMLoc1 = 1
		'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc2. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		whichMLoc2 = 1
		
		Text1.Text = MTypeLabel(whichMType1)
		'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc1. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Text2.Text = MLocationLabel(whichMLoc1)
		Text3.Text = MTypeLabel(whichMType2)
		'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc2. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Text4.Text = MLocationLabel(whichMLoc2)
        Text5.Text = CStr(MTypePrDef_Nunits(whichMType1))
        Text6.Text = CStr(MTypePrDef_Nunits(whichMType2))
		
		For jtype = 1 To NMType
			'UPGRADE_WARNING: Couldn't resolve default property of object jtype. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			List1.Items.Add(MTypeLabel(jtype))
			'UPGRADE_WARNING: Couldn't resolve default property of object jtype. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			List3.Items.Add(MTypeLabel(jtype))
		Next jtype
		For jloc = 1 To NMLocation
			'UPGRADE_WARNING: Couldn't resolve default property of object jloc. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			List2.Items.Add(MLocationLabel(jloc))
			'UPGRADE_WARNING: Couldn't resolve default property of object jloc. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			List4.Items.Add(MLocationLabel(jloc))
		Next jloc
		
		MSFlexGrid1.Rows = NTic1 + 2
		MSFlexGrid2.Rows = NTic1 + 2
		
		MSFlexGrid1.Cols = 5
		MSFlexGrid2.Cols = 5
		
		'MSFlexGrid1.TextMatrix(0, 0) = "period"
		'MSFlexGrid1.TextMatrix(0, 1) = "base price"
		'MSFlexGrid1.TextMatrix(0, 2) = "shadow price"
		'MSFlexGrid1.TextMatrix(0, 3) = "quantity this Mlocation"
		'MSFlexGrid1.TextMatrix(0, 4) = "quantity all  Mlocations"
		'MSFlexGrid2.TextMatrix(0, 0) = "period"
		'MSFlexGrid2.TextMatrix(0, 1) = "base price"
		'MSFlexGrid2.TextMatrix(0, 2) = "shadow price"
		'MSFlexGrid2.TextMatrix(0, 3) = "quantity this Mlocation"
		'MSFlexGrid2.TextMatrix(0, 4) = "quantity all  Mlocations"
		MSFlexGrid1.FormatString = "^period |>Base Price |>shadow price |>flow/yr this Mloc|>flow/yr all Mloc"
		MSFlexGrid2.FormatString = "^period |>Base Price |>shadow price |>flow/yr this Mloc|>flow/yr all Mloc"
		
		For jdef = 1 To MTypeNDef
			'UPGRADE_WARNING: Couldn't resolve default property of object jdef. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            UnitPrice = MTypePrDef_price(jdef)
			'UPGRADE_WARNING: Couldn't resolve default property of object jdef. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            UnitQuan = MTypePrDef_Nunits(jdef)
			'UPGRADE_WARNING: Couldn't resolve default property of object jdef. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For jtic = MTypePrDef_TicBeg(jdef) To MTypePrDef_TicEnd(jdef)
                'UnitPrice! = MTypePrDef(jdef).price / MTypePrDef(jdef).Nunits * MDisc!(jtic)
                'UPGRADE_WARNING: Couldn't resolve default property of object jdef. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                For jmtype = MTypePrDef_TypeBeg(jdef) To MTypePrDef_TypeEnd(jdef)
                    'UPGRADE_WARNING: Couldn't resolve default property of object jdef. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    For jmloc = MTypePrDef_MLocBeg(jdef) To MTypePrDef_MLocEnd(jdef)
                        'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object jmloc. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object jmtype. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        MtypePrBase(jmtype, jmloc, jtic) = UnitPrice
                        'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object jmloc. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object jmtype. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        MtypeUnits(jmtype, jmloc, jtic) = UnitQuan
                    Next jmloc
                Next jmtype
            Next jtic
		Next jdef
		
		For jx = 1 To NTic1
			'UPGRADE_WARNING: Couldn't resolve default property of object jx. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			MSFlexGrid1.set_TextMatrix(jx, 0, jx)
			'UPGRADE_WARNING: Couldn't resolve default property of object jx. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			MSFlexGrid2.set_TextMatrix(jx, 0, jx)
		Next jx
		
		
		Call resetgrid1()
		Call resetgrid2()
	End Sub
	
	
	Private Sub resetgrid1()
		Dim jloc As Object
		Dim totlocType1 As Double
		Dim jtic As Object
		For jtic = 1 To NTic
			totlocType1 = 0
			'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc1. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			MSFlexGrid1.set_TextMatrix(jtic, 1, MtypePrBase(whichMType1, whichMLoc1, jtic))
			'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc1. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			MSFlexGrid1.set_TextMatrix(jtic, 2, VB6.Format(MtypePr(whichMType1, whichMLoc1, jtic) / MDisc(jtic) * MtypeUnits(whichMType1, whichMLoc1, jtic), "#####.000"))
			
			For jloc = 1 To NMLocation
				'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				'UPGRADE_WARNING: Couldn't resolve default property of object jloc. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				totlocType1 = totlocType1 + MTypeFlow(whichMType1, jloc, jtic)
			Next jloc
			'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc1. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			MSFlexGrid1.set_TextMatrix(jtic, 3, VB6.Format(MTypeFlow(whichMType1, whichMLoc1, jtic) / (NYearPerTic * MtypeUnits(whichMType1, whichMLoc1, jtic)), "#######0"))
			'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc1. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			MSFlexGrid1.set_TextMatrix(jtic, 4, VB6.Format(totlocType1 / (NYearPerTic * MtypeUnits(whichMType1, whichMLoc1, jtic)), "##########0"))
		Next jtic
		'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc1. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MSFlexGrid1.set_TextMatrix(NTic1, 2, VB6.Format(MtypePr(whichMType1, whichMLoc1, NTic1) / MDisc(NTic1) * MtypeUnits(whichMType1, whichMLoc1, NTic), "######.000"))
	End Sub
	
	Private Sub resetgrid2()
		Dim jloc As Object
		Dim totlocType2 As Double
		Dim jtic As Object
		For jtic = 1 To NTic
			totlocType2 = 0
			'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc2. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			MSFlexGrid2.set_TextMatrix(jtic, 1, MtypePrBase(whichMType2, whichMLoc2, jtic))
			'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc2. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			MSFlexGrid2.set_TextMatrix(jtic, 2, VB6.Format(MtypePr(whichMType2, whichMLoc2, jtic) / MDisc(jtic) * MtypeUnits(whichMType2, whichMLoc2, jtic), "#####.000"))
			For jloc = 1 To NMLocation
				'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				'UPGRADE_WARNING: Couldn't resolve default property of object jloc. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				totlocType2 = totlocType2 + MTypeFlow(whichMType2, jloc, jtic)
			Next jloc
			'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc2. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			MSFlexGrid2.set_TextMatrix(jtic, 3, VB6.Format(MTypeFlow(whichMType2, whichMLoc2, jtic) / (NYearPerTic * MtypeUnits(whichMType2, whichMLoc2, jtic)), "#######0"))
			'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc2. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			MSFlexGrid2.set_TextMatrix(jtic, 4, VB6.Format(totlocType2 / (NYearPerTic * MtypeUnits(whichMType2, whichMLoc2, jtic)), "##########0"))
		Next jtic
		'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc2. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MSFlexGrid2.set_TextMatrix(NTic1, 2, VB6.Format(MtypePr(whichMType2, whichMLoc2, NTic1) / MDisc(NTic1) * MtypeUnits(whichMType2, whichMLoc2, NTic), "######.000"))
		
	End Sub
	
	
	'UPGRADE_WARNING: Event List1.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub List1_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List1.SelectedIndexChanged
		whichMType1 = List1.SelectedIndex + 1
		Text1.Text = MTypeLabel(whichMType1)
		'MtypeUnits!(jmtype, jmloc, jtic)
		'Text5.Text = CStr(MTypePrDef(whichMType1).Nunits)
		Text5.Text = CStr(MtypeUnits(whichMType1, 1, 1))
		Call resetgrid1()
	End Sub
	
	'UPGRADE_WARNING: Event List2.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub List2_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List2.SelectedIndexChanged
		'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc1. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		whichMLoc1 = List2.SelectedIndex + 1
		'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc1. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Text2.Text = MLocationLabel(whichMLoc1)
		Call resetgrid1()
	End Sub
	
	'UPGRADE_WARNING: Event List3.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub List3_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List3.SelectedIndexChanged
		whichMType2 = List3.SelectedIndex + 1
		Text3.Text = MTypeLabel(whichMType2)
		'Text6.Text = CStr(MTypePrDef(whichMType2).Nunits)
		Text6.Text = CStr(MtypeUnits(whichMType2, 1, 1))
		Call resetgrid2()
	End Sub
	
	'UPGRADE_WARNING: Event List4.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub List4_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List4.SelectedIndexChanged
		'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc2. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		whichMLoc2 = List4.SelectedIndex + 1
		'UPGRADE_WARNING: Couldn't resolve default property of object whichMLoc2. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Text4.Text = MLocationLabel(whichMLoc2)
		Call resetgrid2()
	End Sub
End Class