Option Strict Off
Option Explicit On
Friend Class ConditionSet
    Inherits System.Windows.Forms.Form
    'UPGRADE_NOTE: DefInt A-Z statement was removed. Variables were explicitly declared as type Short. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="92AFD3E3-440D-4D49-A8BF-580D74A8C9F2"'


    Dim whichCondSet As Short
    Dim whatToShow As Short
    Dim whichCondSet2 As Short
    Dim WhatToShow2 As Short
    'UPGRADE_WARNING: Lower bound of array whatToShowLabel was changed from 0 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    Dim whatToShowLabel(4) As String
    'UPGRADE_WARNING: Lower bound of array ConstTypeLabel was changed from 0 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    Dim ConstTypeLabel(7) As String


    Private Sub ConditionSet_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim jshow As Short
        Dim jCondSet As Short
        whatToShowLabel(0) = "Deviation from Target(%)"
        whatToShowLabel(1) = "Flow Quantity "
        whatToShowLabel(2) = "Target"
        whatToShowLabel(3) = "Shadow Price ($/Unit"
        whatToShowLabel(4) = "Change in Shadow Price ($/Unit"
        ConstTypeLabel(0) = "Unconstrained"
        ConstTypeLabel(1) = "Target Levels"
        ConstTypeLabel(2) = "Demand = f(price)"
        ConstTypeLabel(3) = "Lower Bounds"
        ConstTypeLabel(4) = "Upper Bounds"
        ConstTypeLabel(5) = "Upper and Lower Bounds"
        ConstTypeLabel(6) = "% Changes, All Periods"
        ConstTypeLabel(7) = "% Changes, except period 1"

        whichCondSet = 1
        whatToShow = 1
        whichCondSet2 = 1
        WhatToShow2 = 3
        Text1.Text = CondSet_Label(whichCondSet)
        'UPGRADE_WARNING: Couldn't resolve default property of object CondSetConstraintOption(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Text2.Text = ConstTypeLabel(CondSet_ConstraintOption(whichCondSet))
        Text3.Text = whatToShowLabel(whatToShow)
        Text4.Text = CondSet_Label(whichCondSet2)
        'UPGRADE_WARNING: Couldn't resolve default property of object CondSetConstraintOption(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Text5.Text = ConstTypeLabel(CondSet_ConstraintOption(whichCondSet2))
        Text6.Text = whatToShowLabel(WhatToShow2)
        For jCondSet = 1 To NCondSet
            List1.Items.Add(CondSet_Label(whichCondSet))
            List3.Items.Add(CondSet_Label(whichCondSet))
            whichCondSet = whichCondSet + 1
        Next jCondSet
        For jshow = 0 To 4
            List2.Items.Add(whatToShowLabel(jshow))
            List4.Items.Add(whatToShowLabel(jshow))
        Next jshow
        whichCondSet = 1
        MSFlexGrid1.Rows = NTic1 + 2
        MSFlexGrid2.Rows = NTic1 + 2
        MSFlexGrid1.set_TextMatrix(0, 0, "Period")
        MSFlexGrid2.set_TextMatrix(0, 0, "Period")
        Call resetgrid1()
        Call resetgrid2()
    End Sub


    Private Sub resetgrid1()
        Dim PrChange As Single
        Dim kposprior As Short
        Dim kprior As Short
        Dim Lower As Single
        Dim Upper As Single
        Dim jtic As Short
        Dim kposition As Short
        Dim jiter As Short
        Dim NiterToShow As Short
        Dim kiter As Short
        kiter = IterationNumber
        If kiter = 0 Then kiter = 1
        NiterToShow = kiter
        If NiterToShow > NIterToSave Then NiterToShow = NIterToSave
        MSFlexGrid1.Cols = NiterToShow + 1
        If whatToShow = 0 Then
            For jiter = 1 To NiterToShow
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                'MSFlexGrid1.TextMatrix(0, jiter) = Str$(kiter) + " " + IterType$(kposition)
                MSFlexGrid1.set_TextMatrix(0, jiter, "Iter " & Str(kiter))
                MSFlexGrid1.set_TextMatrix(1, 0, "0")
                MSFlexGrid1.set_TextMatrix(1, 1, "*****")
                For jtic = 1 To NTic
                    MSFlexGrid1.set_TextMatrix(jtic + 1, 0, jtic)
                    MSFlexGrid1.set_TextMatrix(jtic + 1, jiter, VB6.Format(CondSet_Dev(whichCondSet, kposition, jtic), "####.00"))
                Next jtic
                kiter = kiter - 1
            Next jiter
        ElseIf whatToShow = 1 Then
            For jiter = 1 To NiterToShow
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                'MSFlexGrid1.TextMatrix(0, jiter) = Str$(kiter) + " " + IterType$(kposition)
                MSFlexGrid1.set_TextMatrix(0, jiter, "Iter " & Str(kiter))
                For jtic = 0 To NTic
                    MSFlexGrid1.set_TextMatrix(jtic + 1, 0, jtic)
                    MSFlexGrid1.set_TextMatrix(jtic + 1, jiter, VB6.Format(CondSet_Flow(whichCondSet, kposition, jtic), "######0"))
                Next jtic
                kiter = kiter - 1
            Next jiter
        ElseIf whatToShow = 2 Then
            kposition = ((kiter - 1) Mod NIterToSave) + 1
            Select Case CondSet_ConstraintOption(whichCondSet)
                Case 0, 1, 2, 3, 4
                    MSFlexGrid1.Cols = 2
                    'MSFlexGrid1.TextMatrix(0, 1) = "Target/10000"
                    MSFlexGrid1.set_TextMatrix(0, 1, "Target")
                    MSFlexGrid1.set_TextMatrix(1, 0, "0")
                    MSFlexGrid1.set_TextMatrix(1, 1, "****")
                    For jtic = 1 To NTic
                        MSFlexGrid1.set_TextMatrix(jtic + 1, 0, jtic)
                        'MSFlexGrid1.TextMatrix(jtic, 1) = Format$(CondSetTarget!(whichCondSet, jtic) / 10000, "######0")
                        MSFlexGrid1.set_TextMatrix(jtic + 1, 1, VB6.Format(CondSet_Target(whichCondSet, jtic), "######0"))
                    Next jtic
                Case 5
                    MSFlexGrid1.Cols = 3
                    MSFlexGrid1.set_TextMatrix(1, 0, "0")
                    MSFlexGrid1.set_TextMatrix(1, 1, "****")
                    MSFlexGrid1.set_TextMatrix(1, 2, "****")
                    'MSFlexGrid1.TextMatrix(0, 1) = "Lower Bound/10000"
                    'MSFlexGrid1.TextMatrix(0, 2) = "Upper Bound/10000"
                    MSFlexGrid1.set_TextMatrix(0, 1, "Lower Bound")
                    MSFlexGrid1.set_TextMatrix(0, 2, "Upper Bound")
                    For jtic = 1 To NTic
                        MSFlexGrid1.set_TextMatrix(jtic + 1, 0, jtic)
                        'MSFlexGrid1.TextMatrix(jtic, 1) = Format$(CondSetFlowMn!(whichCondSet) / 10000, "######0")
                        'MSFlexGrid1.TextMatrix(jtic, 2) = Format$(CondSetFlowMx!(whichCondSet) / 10000, "######0")
                        MSFlexGrid1.set_TextMatrix(jtic + 1, 1, VB6.Format(CondSet_FlowMn1(whichCondSet), "######0"))
                        MSFlexGrid1.set_TextMatrix(jtic + 1, 2, VB6.Format(CondSet_FlowMx1(whichCondSet), "######0"))


                    Next jtic
                Case 6, 7
                    MSFlexGrid1.Cols = 3
                    MSFlexGrid1.set_TextMatrix(1, 0, "0")
                    MSFlexGrid1.set_TextMatrix(1, 1, "****")
                    MSFlexGrid1.set_TextMatrix(1, 2, "****")
                    MSFlexGrid1.set_TextMatrix(2, 0, "1")

                    'MSFlexGrid1.TextMatrix(0, 1) = "Lower Bound/10000"
                    'MSFlexGrid1.TextMatrix(0, 2) = "Upper Bound/10000"
                    MSFlexGrid1.set_TextMatrix(0, 1, "Lower Bound")
                    MSFlexGrid1.set_TextMatrix(0, 2, "Upper Bound")
                    For jtic = 2 To NTic
                        Upper = CondSet_Flow(whichCondSet, kposition, jtic - 1) * (1 + CondSet_DevMx(whichCondSet))
                        Lower = CondSet_Flow(whichCondSet, kposition, jtic - 1) * (1 - CondSet_DevMn(whichCondSet))
                        MSFlexGrid1.set_TextMatrix(jtic + 1, 0, jtic)
                        'MSFlexGrid1.TextMatrix(jtic, 1) = Format$(Lower! / 10000, "######0")
                        'MSFlexGrid1.TextMatrix(jtic, 2) = Format$(Upper! / 10000, "######0")
                        MSFlexGrid1.set_TextMatrix(jtic + 1, 1, VB6.Format(Lower, "######0"))
                        MSFlexGrid1.set_TextMatrix(jtic + 1, 2, VB6.Format(Upper, "######0"))
                    Next jtic
                    Upper = CondSet_BaseQuan(whichCondSet) * (1 + CondSet_DevMx(whichCondSet))
                    Lower = CondSet_BaseQuan(whichCondSet) * (1 - CondSet_DevMn(whichCondSet))
                    'MSFlexGrid1.TextMatrix(1, 1) = Format$(Lower! / 10000, "######0")
                    'MSFlexGrid1.TextMatrix(1, 2) = Format$(Upper! / 10000, "######0")
                    MSFlexGrid1.set_TextMatrix(2, 1, VB6.Format(Lower, "######0"))
                    MSFlexGrid1.set_TextMatrix(2, 2, VB6.Format(Upper, "######0"))
                    'UPGRADE_WARNING: Couldn't resolve default property of object CondSetConstraintOption(whichCondSet). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If CondSet_ConstraintOption(whichCondSet) = 7 Then
                        MSFlexGrid1.set_TextMatrix(2, 1, "Unbounded")
                        MSFlexGrid1.set_TextMatrix(2, 2, "Unbounded")
                    End If
            End Select
        ElseIf whatToShow = 3 Then
            For jiter = 1 To NiterToShow
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                'MSFlexGrid1.TextMatrix(0, jiter) = Str$(kiter) + " " + IterType$(kposition)
                MSFlexGrid1.set_TextMatrix(0, jiter, "Iter " & Str(kiter))
                MSFlexGrid1.set_TextMatrix(1, 0, "0")
                MSFlexGrid1.set_TextMatrix(1, jiter, "***")
                For jtic = 1 To NTic
                    MSFlexGrid1.set_TextMatrix(jtic + 1, 0, jtic)
                    MSFlexGrid1.set_TextMatrix(jtic + 1, jiter, VB6.Format(CondSet_Pr(whichCondSet, kposition, jtic), "##.00"))
                Next jtic
                kiter = kiter - 1
            Next jiter
        ElseIf whatToShow = 4 Then
            MSFlexGrid1.Cols = NiterToShow
            For jiter = 1 To NiterToShow - 1
                kprior = kiter - 1
                If kprior = 0 Then kprior = 1
                kposprior = ((kprior - 1) Mod NIterToSave) + 1
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                'MSFlexGrid1.TextMatrix(0, jiter) = Str$(kiter) + " " + IterType$(kposition)
                MSFlexGrid1.set_TextMatrix(0, jiter, "Iter " & Str(kiter))
                For jtic = 1 To NTic
                    MSFlexGrid1.set_TextMatrix(jtic + 1, 0, jtic)
                    PrChange = CondSet_Pr(whichCondSet, kposition, jtic) - CondSet_Pr(whichCondSet, kposprior, jtic)
                    MSFlexGrid1.set_TextMatrix(jtic + 1, jiter, VB6.Format(PrChange, "####.00"))
                Next jtic
                kiter = kiter - 1
            Next jiter
        End If


    End Sub

    Private Sub resetgrid2()
        Dim PrChange As Single
        Dim kposprior As Short
        Dim kprior As Short
        Dim Lower As Single
        Dim Upper As Single
        Dim jtic As Short
        Dim kposition As Short
        Dim jiter As Short
        Dim NiterToShow As Short
        Dim kiter As Short
        kiter = IterationNumber
        If kiter = 0 Then kiter = 1
        NiterToShow = kiter
        If NiterToShow > NIterToSave Then NiterToShow = NIterToSave
        MSFlexGrid2.Cols = NiterToShow + 1
        If WhatToShow2 = 0 Then
            MSFlexGrid2.set_TextMatrix(1, 0, "0")
            MSFlexGrid2.set_TextMatrix(1, 1, "*****")
            For jiter = 1 To NiterToShow
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                MSFlexGrid2.set_TextMatrix(0, jiter, Str(kiter) & " " & AdjIterType(kposition))
                For jtic = 0 To NTic
                    MSFlexGrid2.set_TextMatrix(jtic + 1, 0, jtic)
                    MSFlexGrid2.set_TextMatrix(jtic + 1, jiter, VB6.Format(CondSet_Dev(whichCondSet2, kposition, jtic), "####.00"))
                Next jtic
                kiter = kiter - 1
            Next jiter
        ElseIf WhatToShow2 = 1 Then
            For jiter = 1 To NiterToShow
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                'MSFlexGrid2.TextMatrix(0, jiter) = Str$(kiter) + " " + IterType$(kposition)
                MSFlexGrid2.set_TextMatrix(0, jiter, "Iter " & Str(kiter))
                For jtic = 0 To NTic
                    MSFlexGrid2.set_TextMatrix(jtic + 1, 0, jtic)
                    MSFlexGrid2.set_TextMatrix(jtic + 1, jiter, VB6.Format(CondSet_Flow(whichCondSet2, kposition, jtic), "######0"))
                Next jtic
                kiter = kiter - 1
            Next jiter
        ElseIf WhatToShow2 = 2 Then
            kposition = ((kiter - 1) Mod NIterToSave) + 1
            Select Case CondSet_ConstraintOption(whichCondSet)
                Case 0, 1, 2, 3, 4
                    MSFlexGrid2.Cols = 2
                    MSFlexGrid2.set_TextMatrix(1, 0, "0")
                    MSFlexGrid2.set_TextMatrix(1, 1, "****")
                    'MSFlexGrid2.TextMatrix(0, 1) = "Target/10000"
                    MSFlexGrid2.set_TextMatrix(0, 1, "Target")
                    For jtic = 1 To NTic
                        MSFlexGrid2.set_TextMatrix(jtic + 1, 0, jtic)
                        'MSFlexGrid2.TextMatrix(jtic, 1) = Format$(CondSetTarget!(whichCondSet2, jtic) / 10000, "######0")
                        MSFlexGrid2.set_TextMatrix(jtic + 1, 1, VB6.Format(CondSet_Target(whichCondSet2, jtic), "######0"))
                    Next jtic
                Case 5
                    MSFlexGrid2.Cols = 3
                    'MSFlexGrid2.TextMatrix(0, 1) = "Lower Bound/10000"
                    'MSFlexGrid2.TextMatrix(0, 2) = "Upper Bound/10000"
                    MSFlexGrid2.set_TextMatrix(0, 1, "Lower Bound")
                    MSFlexGrid2.set_TextMatrix(0, 2, "Upper Bound")
                    MSFlexGrid2.set_TextMatrix(1, 0, "0")
                    MSFlexGrid2.set_TextMatrix(1, 1, "****")
                    MSFlexGrid2.set_TextMatrix(1, 2, "****")



                    For jtic = 1 To NTic
                        MSFlexGrid2.set_TextMatrix(jtic + 1, 0, jtic)
                        'MSFlexGrid2.TextMatrix(jtic, 1) = Format$(CondSetFlowMn!(whichCondSet2) / 10000, "######0")
                        'MSFlexGrid2.TextMatrix(jtic, 2) = Format$(CondSetFlowMx!(whichCondSet2) / 10000, "######0")
                        MSFlexGrid2.set_TextMatrix(jtic + 1, 1, VB6.Format(CondSet_FlowMn1(whichCondSet2), "######0"))
                        MSFlexGrid2.set_TextMatrix(jtic + 1, 2, VB6.Format(CondSet_FlowMx1(whichCondSet2), "######0"))
                    Next jtic
                Case 6, 7
                    MSFlexGrid2.Cols = 3
                    'MSFlexGrid2.TextMatrix(0, 1) = "Lower Bound/10000"
                    'MSFlexGrid2.TextMatrix(0, 2) = "Upper Bound/10000"
                    MSFlexGrid2.set_TextMatrix(0, 1, "Lower Bound")
                    MSFlexGrid2.set_TextMatrix(0, 2, "Upper Bound")
                    MSFlexGrid2.set_TextMatrix(1, 0, "0")
                    MSFlexGrid2.set_TextMatrix(1, 1, "****")
                    MSFlexGrid2.set_TextMatrix(1, 2, "****")
                    MSFlexGrid2.set_TextMatrix(2, 0, "1")


                    For jtic = 2 To NTic
                        Upper = CondSet_Flow(whichCondSet2, kposition, jtic - 1) * (1 + CondSet_DevMx(whichCondSet2))
                        Lower = CondSet_Flow(whichCondSet2, kposition, jtic - 1) * (1 - CondSet_DevMn(whichCondSet2))
                        MSFlexGrid2.set_TextMatrix(jtic + 1, 0, jtic)
                        'MSFlexGrid2.TextMatrix(jtic, 1) = Format$(Lower! / 10000, "######0")
                        'MSFlexGrid2.TextMatrix(jtic, 2) = Format$(Upper! / 10000, "######0")
                        MSFlexGrid2.set_TextMatrix(jtic + 1, 1, VB6.Format(Lower, "######0"))
                        MSFlexGrid2.set_TextMatrix(jtic + 1, 2, VB6.Format(Upper, "######0"))
                    Next jtic
                    Upper = CondSet_BaseQuan(whichCondSet2) * (1 + CondSet_DevMx(whichCondSet2))
                    Lower = CondSet_BaseQuan(whichCondSet2) * (1 - CondSet_DevMn(whichCondSet2))

                    'MSFlexGrid2.TextMatrix(1, 1) = Format$(Lower! / 10000, "######0")
                    'MSFlexGrid2.TextMatrix(1, 2) = Format$(Upper! / 10000, "######0")
                    MSFlexGrid2.set_TextMatrix(2, 1, VB6.Format(Lower, "######0"))
                    MSFlexGrid2.set_TextMatrix(2, 2, VB6.Format(Upper, "######0"))
                    'UPGRADE_WARNING: Couldn't resolve default property of object CondSetConstraintOption(whichCondSet2). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If CondSet_ConstraintOption(whichCondSet) = 7 Then
                        MSFlexGrid2.set_TextMatrix(2, 1, "Unbounded")
                        MSFlexGrid2.set_TextMatrix(2, 2, "Unbounded")
                    End If
            End Select
        ElseIf WhatToShow2 = 3 Then
            MSFlexGrid2.set_TextMatrix(1, 0, "0")
            MSFlexGrid2.set_TextMatrix(1, jiter, "***")
            For jiter = 1 To NiterToShow
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                'MSFlexGrid2.TextMatrix(0, jiter) = Str$(kiter) + " " + IterType$(kposition)
                MSFlexGrid2.set_TextMatrix(0, jiter, "Iter " & Str(kiter))
                For jtic = 1 To NTic
                    MSFlexGrid2.set_TextMatrix(jtic + 1, 0, jtic)
                    MSFlexGrid2.set_TextMatrix(jtic + 1, jiter, VB6.Format(CondSet_Pr(whichCondSet2, kposition, jtic), "##.00"))
                Next jtic
                kiter = kiter - 1
            Next jiter
        ElseIf WhatToShow2 = 4 Then
            MSFlexGrid2.Cols = NiterToShow
            For jiter = 1 To NiterToShow - 1
                kprior = kiter - 1
                If kprior = 0 Then kprior = 1
                kposprior = ((kprior - 1) Mod NIterToSave) + 1
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                'MSFlexGrid2.TextMatrix(0, jiter) = Str$(kiter) + " " + IterType$(kposition)
                MSFlexGrid2.set_TextMatrix(0, jiter, "Iter " & Str(kiter))
                For jtic = 1 To NTic
                    MSFlexGrid2.set_TextMatrix(jtic + 1, 0, jtic)
                    PrChange = CondSet_Pr(whichCondSet2, kposition, jtic) - CondSet_Pr(whichCondSet2, kposprior, jtic)
                    MSFlexGrid2.set_TextMatrix(jtic + 1, jiter, VB6.Format(PrChange, "####.00"))
                Next jtic
                kiter = kiter - 1
            Next jiter
        End If


    End Sub



    'UPGRADE_WARNING: Event List1.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub List1_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List1.SelectedIndexChanged
        whichCondSet = List1.SelectedIndex + 1
        Text1.Text = CondSet_Label(whichCondSet)
        'UPGRADE_WARNING: Couldn't resolve default property of object CondSetConstraintOption(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Text2.Text = ConstTypeLabel(CondSet_ConstraintOption(whichCondSet))
        Call resetgrid1()
    End Sub

    'UPGRADE_WARNING: Event List2.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub List2_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List2.SelectedIndexChanged
        whatToShow = List2.SelectedIndex
        Text3.Text = whatToShowLabel(whatToShow)
        Call resetgrid1()
    End Sub

    'UPGRADE_WARNING: Event List3.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub List3_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List3.SelectedIndexChanged
        whichCondSet2 = List3.SelectedIndex + 1
        Text4.Text = CondSet_Label(whichCondSet2)
        'UPGRADE_WARNING: Couldn't resolve default property of object CondSetConstraintOption(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Text5.Text = ConstTypeLabel(CondSet_ConstraintOption(whichCondSet2))
        Call resetgrid2()

    End Sub

    'UPGRADE_WARNING: Event List4.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub List4_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List4.SelectedIndexChanged
        WhatToShow2 = List4.SelectedIndex
        Text6.Text = whatToShowLabel(WhatToShow2)
        Call resetgrid2()

    End Sub
End Class