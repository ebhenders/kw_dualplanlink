<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class ShadPrCoef
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdcan3 As System.Windows.Forms.Button
	Public WithEvents cmdsave3 As System.Windows.Forms.Button
	Public WithEvents cmdcan2 As System.Windows.Forms.Button
	Public WithEvents cmdsave2 As System.Windows.Forms.Button
	Public WithEvents cmdcan1 As System.Windows.Forms.Button
	Public WithEvents cmdSave1 As System.Windows.Forms.Button
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents MSFlexGrid3 As AxMSFlexGridLib.AxMSFlexGrid
	Public WithEvents MSFlexGrid2 As AxMSFlexGridLib.AxMSFlexGrid
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents MSFlexGrid1 As AxMSFlexGridLib.AxMSFlexGrid
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ShadPrCoef))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdcan3 = New System.Windows.Forms.Button
        Me.cmdsave3 = New System.Windows.Forms.Button
        Me.cmdcan2 = New System.Windows.Forms.Button
        Me.cmdsave2 = New System.Windows.Forms.Button
        Me.cmdcan1 = New System.Windows.Forms.Button
        Me.cmdSave1 = New System.Windows.Forms.Button
        Me.Text4 = New System.Windows.Forms.TextBox
        Me.Text3 = New System.Windows.Forms.TextBox
        Me.MSFlexGrid3 = New AxMSFlexGridLib.AxMSFlexGrid
        Me.MSFlexGrid2 = New AxMSFlexGridLib.AxMSFlexGrid
        Me.Text2 = New System.Windows.Forms.TextBox
        Me.MSFlexGrid1 = New AxMSFlexGridLib.AxMSFlexGrid
        CType(Me.MSFlexGrid3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MSFlexGrid2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MSFlexGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdcan3
        '
        Me.cmdcan3.BackColor = System.Drawing.SystemColors.Control
        Me.cmdcan3.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdcan3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdcan3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdcan3.Location = New System.Drawing.Point(400, 672)
        Me.cmdcan3.Name = "cmdcan3"
        Me.cmdcan3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdcan3.Size = New System.Drawing.Size(89, 25)
        Me.cmdcan3.TabIndex = 11
        Me.cmdcan3.Text = "Cancel"
        Me.cmdcan3.UseVisualStyleBackColor = False
        '
        'cmdsave3
        '
        Me.cmdsave3.BackColor = System.Drawing.SystemColors.Control
        Me.cmdsave3.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdsave3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdsave3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdsave3.Location = New System.Drawing.Point(296, 672)
        Me.cmdsave3.Name = "cmdsave3"
        Me.cmdsave3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdsave3.Size = New System.Drawing.Size(81, 25)
        Me.cmdsave3.TabIndex = 10
        Me.cmdsave3.Text = "Save Links"
        Me.cmdsave3.UseVisualStyleBackColor = False
        '
        'cmdcan2
        '
        Me.cmdcan2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdcan2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdcan2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdcan2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdcan2.Location = New System.Drawing.Point(168, 280)
        Me.cmdcan2.Name = "cmdcan2"
        Me.cmdcan2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdcan2.Size = New System.Drawing.Size(89, 25)
        Me.cmdcan2.TabIndex = 9
        Me.cmdcan2.Text = "Cancel"
        Me.cmdcan2.UseVisualStyleBackColor = False
        '
        'cmdsave2
        '
        Me.cmdsave2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdsave2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdsave2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdsave2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdsave2.Location = New System.Drawing.Point(48, 280)
        Me.cmdsave2.Name = "cmdsave2"
        Me.cmdsave2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdsave2.Size = New System.Drawing.Size(89, 25)
        Me.cmdsave2.TabIndex = 8
        Me.cmdsave2.Text = "Save Links"
        Me.cmdsave2.UseVisualStyleBackColor = False
        '
        'cmdcan1
        '
        Me.cmdcan1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdcan1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdcan1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdcan1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdcan1.Location = New System.Drawing.Point(816, 384)
        Me.cmdcan1.Name = "cmdcan1"
        Me.cmdcan1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdcan1.Size = New System.Drawing.Size(129, 41)
        Me.cmdcan1.TabIndex = 7
        Me.cmdcan1.Text = "Cancel"
        Me.cmdcan1.UseVisualStyleBackColor = False
        '
        'cmdSave1
        '
        Me.cmdSave1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSave1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSave1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSave1.Location = New System.Drawing.Point(648, 384)
        Me.cmdSave1.Name = "cmdSave1"
        Me.cmdSave1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSave1.Size = New System.Drawing.Size(121, 41)
        Me.cmdSave1.TabIndex = 6
        Me.cmdSave1.Text = "Save coefficients"
        Me.cmdSave1.UseVisualStyleBackColor = False
        '
        'Text4
        '
        Me.Text4.AcceptsReturn = True
        Me.Text4.BackColor = System.Drawing.SystemColors.Window
        Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text4.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text4.Location = New System.Drawing.Point(48, 384)
        Me.Text4.MaxLength = 0
        Me.Text4.Name = "Text4"
        Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text4.Size = New System.Drawing.Size(169, 19)
        Me.Text4.TabIndex = 5
        Me.Text4.Text = "Text4"
        '
        'Text3
        '
        Me.Text3.AcceptsReturn = True
        Me.Text3.BackColor = System.Drawing.SystemColors.Window
        Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text3.Location = New System.Drawing.Point(80, 8)
        Me.Text3.MaxLength = 0
        Me.Text3.Name = "Text3"
        Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text3.Size = New System.Drawing.Size(153, 19)
        Me.Text3.TabIndex = 4
        Me.Text3.Text = "Text3"
        '
        'MSFlexGrid3
        '
        Me.MSFlexGrid3.Location = New System.Drawing.Point(24, 416)
        Me.MSFlexGrid3.Name = "MSFlexGrid3"
        Me.MSFlexGrid3.OcxState = CType(resources.GetObject("MSFlexGrid3.OcxState"), System.Windows.Forms.AxHost.State)
        Me.MSFlexGrid3.Size = New System.Drawing.Size(473, 241)
        Me.MSFlexGrid3.TabIndex = 3
        '
        'MSFlexGrid2
        '
        Me.MSFlexGrid2.Location = New System.Drawing.Point(40, 40)
        Me.MSFlexGrid2.Name = "MSFlexGrid2"
        Me.MSFlexGrid2.OcxState = CType(resources.GetObject("MSFlexGrid2.OcxState"), System.Windows.Forms.AxHost.State)
        Me.MSFlexGrid2.Size = New System.Drawing.Size(233, 225)
        Me.MSFlexGrid2.TabIndex = 2
        '
        'Text2
        '
        Me.Text2.AcceptsReturn = True
        Me.Text2.BackColor = System.Drawing.SystemColors.Window
        Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text2.Location = New System.Drawing.Point(592, 8)
        Me.Text2.MaxLength = 0
        Me.Text2.Name = "Text2"
        Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text2.Size = New System.Drawing.Size(97, 25)
        Me.Text2.TabIndex = 1
        Me.Text2.Text = "Text2"
        '
        'MSFlexGrid1
        '
        Me.MSFlexGrid1.Location = New System.Drawing.Point(312, 48)
        Me.MSFlexGrid1.Name = "MSFlexGrid1"
        Me.MSFlexGrid1.OcxState = CType(resources.GetObject("MSFlexGrid1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.MSFlexGrid1.Size = New System.Drawing.Size(673, 321)
        Me.MSFlexGrid1.TabIndex = 0
        '
        'ShadPrCoef
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(998, 729)
        Me.Controls.Add(Me.cmdcan3)
        Me.Controls.Add(Me.cmdsave3)
        Me.Controls.Add(Me.cmdcan2)
        Me.Controls.Add(Me.cmdsave2)
        Me.Controls.Add(Me.cmdcan1)
        Me.Controls.Add(Me.cmdSave1)
        Me.Controls.Add(Me.Text4)
        Me.Controls.Add(Me.Text3)
        Me.Controls.Add(Me.MSFlexGrid3)
        Me.Controls.Add(Me.MSFlexGrid2)
        Me.Controls.Add(Me.Text2)
        Me.Controls.Add(Me.MSFlexGrid1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "ShadPrCoef"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Edit Shadow Price Adjustment Coefficients"
        CType(Me.MSFlexGrid3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MSFlexGrid2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MSFlexGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class