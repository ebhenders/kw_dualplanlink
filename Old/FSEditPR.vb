Option Strict Off
Option Explicit On
Friend Class EditPR
	Inherits System.Windows.Forms.Form
	Dim NeedToChange1 As Short
    Dim NeedToChange2 As Short
    Dim NeedToChange3 As Short
	Private Sub EditPR_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Dim jset As Object
		Dim jtic As Object
		NeedToChange1 = 0
        NeedToChange2 = 0
        NeedToChange3 = 0
		Text1.Visible = False
        Text2.Visible = False
        Text3.Visible = False
		MSFlexGrid1.Rows = NTic1
		MSFlexGrid1.Cols = NMSet + 1
		
		MSFlexGrid2.Rows = NTic1
        MSFlexGrid2.Cols = NCondSet + 1

        MSFlexGrid3.Rows = NTic1
        MSFlexGrid3.Cols = NSpatSet + 1
		
		'MSFlexGrid1.FormatString = "<Period                    |   < Break #|^   Float Break Pt(%)|< Float Adj|^ Shape Break Pt(%) |< Shape Adj |>   Smooth Break Pt(%) |< Smooth Adj|"
		For jtic = 1 To NTic
			'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			MSFlexGrid1.set_TextMatrix(jtic, 0, jtic)
			'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            MSFlexGrid2.set_TextMatrix(jtic, 0, jtic)
            MSFlexGrid3.set_TextMatrix(jtic, 0, jtic)
		Next jtic
		
		knext = ((IterationNumber) Mod NIterToSave) + 1
		'If IterMethod$ = "Manual" Or IterMethod$ = "Input" Then
		'   knextbase = knext
		'Else
		' knextbase = ((IterationNumber& - 1) Mod NIterToSave) + 1
		'End If
		
		For jset = 1 To NMSet
			'UPGRADE_WARNING: Couldn't resolve default property of object jset. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            MSFlexGrid1.set_TextMatrix(0, jset, MSet_Label(jset))
		Next jset
		
		
		For jset = 1 To NCondSet
			'UPGRADE_WARNING: Couldn't resolve default property of object jset. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            MSFlexGrid2.set_TextMatrix(0, jset, CondSet_Label(jset))
        Next jset

        For jset = 1 To NSpatSet
            'UPGRADE_WARNING: Couldn't resolve default property of object jset. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            MSFlexGrid3.set_TextMatrix(0, jset, SpatSet_Label(jset))
        Next jset
		
		Call resetgrids()
		
	End Sub
	
	Private Sub resetgrids()
		Dim jtic As Object
		Dim jset As Object
		
		If IterMethod = "Manual" Or IterMethod = "Input" Then
			knextbase = knext
		Else
			knextbase = ((IterationNumber - 1) Mod NIterToSave) + 1
		End If
		
		For jset = 1 To NMSet
			For jtic = 1 To NTic
				'UPGRADE_WARNING: Couldn't resolve default property of object jset. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                MSFlexGrid1.set_TextMatrix(jtic, jset, MSet_Pr(jset, knextbase, jtic))
            Next jtic
        Next jset

        For jset = 1 To NCondSet
            For jtic = 1 To NTic
                'UPGRADE_WARNING: Couldn't resolve default property of object jset. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                MSFlexGrid2.set_TextMatrix(jtic, jset, CondSet_Pr(jset, knextbase, jtic))
            Next jtic
        Next jset

        For jset = 1 To NSpatSet
            For jtic = 1 To NTic
                'UPGRADE_WARNING: Couldn't resolve default property of object jset. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object jtic. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                MSFlexGrid3.set_TextMatrix(jtic, jset, SpatSet_Pr(jset, knextbase, jtic))
            Next jtic
        Next jset


    End Sub

    Private Sub MSFlexGrid1_EnterCell(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MSFlexGrid1.EnterCell
        'passing cell's contents to textbox
        NeedToChange1 = 1
        'Text1.Text = MSFlexGrid1.Text
        If MSFlexGrid1.Row = 0 Or MSFlexGrid1.Col = 0 Or MSFlexGrid1.MouseRow = 0 Or MSFlexGrid1.MouseCol = 0 Then
            NeedToChange1 = 0
            Text1.Visible = False
            Exit Sub
        End If
        MSFlexGrid1.Row = MSFlexGrid1.MouseRow
        MSFlexGrid1.Col = MSFlexGrid1.MouseCol
        Text1.Text = ""
        'place textbox over current cell
        Text1.Visible = CBool("false")
        Text1.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(MSFlexGrid1.Top) + MSFlexGrid1.CellTop)
        Text1.Left = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(MSFlexGrid1.Left) + MSFlexGrid1.CellLeft)
        Text1.Width = VB6.TwipsToPixelsX(MSFlexGrid1.CellWidth)
        Text1.Height = VB6.TwipsToPixelsY(MSFlexGrid1.CellHeight)
        Text1.Visible = True
        Text1.Focus()
    End Sub
    Private Sub MSFlexGrid1_LeaveCell(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MSFlexGrid1.LeaveCell
        If NeedToChange1 > 0 Then MSFlexGrid1.Text = Text1.Text
        Text1.Visible = False
        NeedToChange1 = 0
    End Sub
    Private Sub MSFlexGrid2_EnterCell(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MSFlexGrid2.EnterCell
        'passing cell's contents to textbox
        NeedToChange2 = 1
        'Text2.Text = MSFlexGrid2.Text
        If MSFlexGrid2.Row = 0 Or MSFlexGrid2.Col = 0 Or MSFlexGrid2.MouseRow = 0 Or MSFlexGrid2.MouseCol = 0 Then
            NeedToChange2 = 0
            Text2.Visible = False
            Exit Sub
        End If
        MSFlexGrid2.Row = MSFlexGrid2.MouseRow
        MSFlexGrid2.Col = MSFlexGrid2.MouseCol
        Text2.Text = ""
        'place textbox over current cell
        Text2.Visible = CBool("false")
        Text2.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(MSFlexGrid2.Top) + MSFlexGrid2.CellTop)
        Text2.Left = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(MSFlexGrid2.Left) + MSFlexGrid2.CellLeft)
        Text2.Width = VB6.TwipsToPixelsX(MSFlexGrid2.CellWidth)
        Text2.Height = VB6.TwipsToPixelsY(MSFlexGrid2.CellHeight)
        Text2.Visible = True
        Text2.Focus()
    End Sub
    Private Sub MSFlexGrid2_LeaveCell(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MSFlexGrid2.LeaveCell
        If NeedToChange2 > 0 Then MSFlexGrid2.Text = Text2.Text
        Text2.Visible = False
        NeedToChange2 = 0
    End Sub
    Private Sub MSFlexGrid3_EnterCell(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MSFlexGrid3.EnterCell
        'passing cell's contents to textbox
        NeedToChange3 = 1
        'Text2.Text = MSFlexGrid2.Text
        If MSFlexGrid3.Row = 0 Or MSFlexGrid3.Col = 0 Then 'Or MSFlexGrid3.MouseRow = 0 Or MSFlexGrid3.MouseCol = 0 Then
            NeedToChange3 = 0
            Text3.Visible = False
            Exit Sub
        End If
        'MSFlexGrid3.Row = MSFlexGrid3.MouseRow
        'MSFlexGrid3.Col = MSFlexGrid3.MouseCol
        Text3.Text = ""
        'place textbox over current cell
        Text3.Visible = CBool("false")
        Text3.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(MSFlexGrid3.Top) + MSFlexGrid3.CellTop)
        Text3.Left = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(MSFlexGrid3.Left) + MSFlexGrid3.CellLeft)
        Text3.Width = VB6.TwipsToPixelsX(MSFlexGrid3.CellWidth)
        Text3.Height = VB6.TwipsToPixelsY(MSFlexGrid3.CellHeight)
        Text3.Visible = True
        Text3.Focus()
    End Sub
    Private Sub MSFlexGrid3_LeaveCell(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MSFlexGrid3.LeaveCell
        If NeedToChange3 > 0 Then MSFlexGrid3.Text = Text3.Text
        Text3.Visible = False
        NeedToChange3 = 0
    End Sub

    Private Sub SaveAll_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SaveAll.Click
        Dim jtic As Object
        Dim jset As Object
        MSFlexGrid1.Row = 0
        MSFlexGrid2.Row = 0
        MSFlexGrid3.Row = 0

        For jset = 1 To NMSet
            For jtic = 1 To NTic
                MSet_Pr(jset, knext, jtic) = Val(MSFlexGrid1.get_TextMatrix(jtic, jset))
            Next jtic
        Next jset

        For jset = 1 To NCondSet
            For jtic = 1 To NTic
                CondSet_Pr(jset, knext, jtic) = Val(MSFlexGrid2.get_TextMatrix(jtic, jset))
            Next jtic
        Next jset

        For jset = 1 To NSpatSet
            For jtic = 1 To NTic
                SpatSet_Pr(jset, knext, jtic) = Val(MSFlexGrid3.get_TextMatrix(jtic, jset))
            Next jtic
        Next jset

        IterMethod = "Manual"
        Call resetgrids()
    End Sub
	Private Sub CancelAll_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CancelAll.Click
		MSFlexGrid1.Row = 0
        MSFlexGrid2.Row = 0
        MSFlexGrid3.Row = 0
		Call resetgrids()
	End Sub


End Class