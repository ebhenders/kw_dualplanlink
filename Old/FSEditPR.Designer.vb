<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class EditPR
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents MSFlexGrid2 As AxMSFlexGridLib.AxMSFlexGrid
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents CancelAll As System.Windows.Forms.Button
	Public WithEvents SaveAll As System.Windows.Forms.Button
    Public WithEvents MSFlexGrid1 As AxMSFlexGridLib.AxMSFlexGrid
    'Public WithEvents Text3 As System.Windows.Forms.TextBox
    Public WithEvents MSFlexGrid3 As AxMSFlexGridLib.AxMSFlexGrid
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EditPR))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Text2 = New System.Windows.Forms.TextBox
        Me.MSFlexGrid2 = New AxMSFlexGridLib.AxMSFlexGrid
        Me.Text1 = New System.Windows.Forms.TextBox
        Me.CancelAll = New System.Windows.Forms.Button
        Me.SaveAll = New System.Windows.Forms.Button
        Me.MSFlexGrid1 = New AxMSFlexGridLib.AxMSFlexGrid
        Me.MSFlexGrid3 = New AxMSFlexGridLib.AxMSFlexGrid
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Text3 = New System.Windows.Forms.TextBox
        CType(Me.MSFlexGrid2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MSFlexGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MSFlexGrid3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Text2
        '
        Me.Text2.AcceptsReturn = True
        Me.Text2.BackColor = System.Drawing.SystemColors.Window
        Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text2.Location = New System.Drawing.Point(424, 560)
        Me.Text2.MaxLength = 0
        Me.Text2.Name = "Text2"
        Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text2.Size = New System.Drawing.Size(377, 20)
        Me.Text2.TabIndex = 6
        Me.Text2.Text = "Text2"
        '
        'MSFlexGrid2
        '
        Me.MSFlexGrid2.Location = New System.Drawing.Point(367, 64)
        Me.MSFlexGrid2.Name = "MSFlexGrid2"
        Me.MSFlexGrid2.OcxState = CType(resources.GetObject("MSFlexGrid2.OcxState"), System.Windows.Forms.AxHost.State)
        Me.MSFlexGrid2.Size = New System.Drawing.Size(369, 329)
        Me.MSFlexGrid2.TabIndex = 5
        '
        'Text1
        '
        Me.Text1.AcceptsReturn = True
        Me.Text1.BackColor = System.Drawing.SystemColors.Window
        Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text1.Location = New System.Drawing.Point(64, 552)
        Me.Text1.MaxLength = 0
        Me.Text1.Name = "Text1"
        Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text1.Size = New System.Drawing.Size(305, 20)
        Me.Text1.TabIndex = 4
        Me.Text1.Text = "Text1"
        '
        'CancelAll
        '
        Me.CancelAll.BackColor = System.Drawing.SystemColors.Control
        Me.CancelAll.Cursor = System.Windows.Forms.Cursors.Default
        Me.CancelAll.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CancelAll.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CancelAll.Location = New System.Drawing.Point(408, 448)
        Me.CancelAll.Name = "CancelAll"
        Me.CancelAll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CancelAll.Size = New System.Drawing.Size(137, 89)
        Me.CancelAll.TabIndex = 2
        Me.CancelAll.Text = " Cancel Changes"
        Me.CancelAll.UseVisualStyleBackColor = False
        '
        'SaveAll
        '
        Me.SaveAll.BackColor = System.Drawing.SystemColors.Control
        Me.SaveAll.Cursor = System.Windows.Forms.Cursors.Default
        Me.SaveAll.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveAll.ForeColor = System.Drawing.SystemColors.ControlText
        Me.SaveAll.Location = New System.Drawing.Point(240, 448)
        Me.SaveAll.Name = "SaveAll"
        Me.SaveAll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SaveAll.Size = New System.Drawing.Size(129, 89)
        Me.SaveAll.TabIndex = 1
        Me.SaveAll.Text = "Save Changes"
        Me.SaveAll.UseVisualStyleBackColor = False
        '
        'MSFlexGrid1
        '
        Me.MSFlexGrid1.Location = New System.Drawing.Point(8, 64)
        Me.MSFlexGrid1.Name = "MSFlexGrid1"
        Me.MSFlexGrid1.OcxState = CType(resources.GetObject("MSFlexGrid1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.MSFlexGrid1.Size = New System.Drawing.Size(353, 329)
        Me.MSFlexGrid1.TabIndex = 0
        '
        'MSFlexGrid3
        '
        Me.MSFlexGrid3.Location = New System.Drawing.Point(742, 64)
        Me.MSFlexGrid3.Name = "MSFlexGrid3"
        Me.MSFlexGrid3.OcxState = CType(resources.GetObject("MSFlexGrid3.OcxState"), System.Windows.Forms.AxHost.State)
        Me.MSFlexGrid3.Size = New System.Drawing.Size(353, 329)
        Me.MSFlexGrid3.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.Font = New System.Drawing.Font("Arial", 13.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(397, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(289, 25)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Condition Set Shadow Prices"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Font = New System.Drawing.Font("Arial", 13.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(102, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(222, 21)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Market Set Shadow Prices"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Font = New System.Drawing.Font("Arial", 13.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(757, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(289, 25)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Spatial Set Shadow Prices"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Text3
        '
        Me.Text3.AcceptsReturn = True
        Me.Text3.BackColor = System.Drawing.SystemColors.Window
        Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text3.Location = New System.Drawing.Point(573, 534)
        Me.Text3.MaxLength = 0
        Me.Text3.Name = "Text3"
        Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text3.Size = New System.Drawing.Size(377, 20)
        Me.Text3.TabIndex = 9
        Me.Text3.Text = "TextBox1"
        '
        'EditPR
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1224, 662)
        Me.Controls.Add(Me.Text3)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Text2)
        Me.Controls.Add(Me.MSFlexGrid2)
        Me.Controls.Add(Me.Text1)
        Me.Controls.Add(Me.CancelAll)
        Me.Controls.Add(Me.SaveAll)
        Me.Controls.Add(Me.MSFlexGrid1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MSFlexGrid3)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "EditPR"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Edit Shadow Prices"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.MSFlexGrid2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MSFlexGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MSFlexGrid3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents Text3 As System.Windows.Forms.TextBox
#End Region 
End Class