<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class ConditionSet
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents MSFlexGrid2 As AxMSFlexGridLib.AxMSFlexGrid
    Public WithEvents MSFlexGrid1 As AxMSFlexGridLib.AxMSFlexGrid
    Public WithEvents Text6 As System.Windows.Forms.TextBox
    Public WithEvents Text5 As System.Windows.Forms.TextBox
    Public WithEvents Text4 As System.Windows.Forms.TextBox
    Public WithEvents Text3 As System.Windows.Forms.TextBox
    Public WithEvents Text2 As System.Windows.Forms.TextBox
    Public WithEvents List4 As System.Windows.Forms.ListBox
    Public WithEvents List3 As System.Windows.Forms.ListBox
    Public WithEvents List2 As System.Windows.Forms.ListBox
    Public WithEvents Text1 As System.Windows.Forms.TextBox
    Public WithEvents List1 As System.Windows.Forms.ListBox
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(ConditionSet))
        Me.components = New System.ComponentModel.Container()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
        Me.MSFlexGrid2 = New AxMSFlexGridLib.AxMSFlexGrid
        Me.MSFlexGrid1 = New AxMSFlexGridLib.AxMSFlexGrid
        Me.Text6 = New System.Windows.Forms.TextBox
        Me.Text5 = New System.Windows.Forms.TextBox
        Me.Text4 = New System.Windows.Forms.TextBox
        Me.Text3 = New System.Windows.Forms.TextBox
        Me.Text2 = New System.Windows.Forms.TextBox
        Me.List4 = New System.Windows.Forms.ListBox
        Me.List3 = New System.Windows.Forms.ListBox
        Me.List2 = New System.Windows.Forms.ListBox
        Me.Text1 = New System.Windows.Forms.TextBox
        Me.List1 = New System.Windows.Forms.ListBox
        Me.SuspendLayout()
        Me.ToolTip1.Active = True
        CType(Me.MSFlexGrid2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MSFlexGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Text = "CondSet"
        Me.ClientSize = New System.Drawing.Size(312, 213)
        Me.Location = New System.Drawing.Point(4, 23)
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.ControlBox = True
        Me.Enabled = True
        Me.KeyPreview = False
        Me.MaximizeBox = True
        Me.MinimizeBox = True
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = True
        Me.HelpButton = False
        Me.Name = "CondSet"
        MSFlexGrid2.OcxState = CType(resources.GetObject("MSFlexGrid2.OcxState"), System.Windows.Forms.AxHost.State)
        Me.MSFlexGrid2.Size = New System.Drawing.Size(465, 273)
        Me.MSFlexGrid2.Location = New System.Drawing.Point(504, 136)
        Me.MSFlexGrid2.TabIndex = 11
        Me.MSFlexGrid2.Name = "MSFlexGrid2"
        MSFlexGrid1.OcxState = CType(resources.GetObject("MSFlexGrid1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.MSFlexGrid1.Size = New System.Drawing.Size(433, 273)
        Me.MSFlexGrid1.Location = New System.Drawing.Point(24, 136)
        Me.MSFlexGrid1.TabIndex = 10
        Me.MSFlexGrid1.Name = "MSFlexGrid1"
        Me.Text6.AutoSize = False
        Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Text6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text6.Size = New System.Drawing.Size(409, 25)
        Me.Text6.Location = New System.Drawing.Point(544, 88)
        Me.Text6.TabIndex = 9
        Me.Text6.Text = "Text6"
        Me.Text6.AcceptsReturn = True
        Me.Text6.BackColor = System.Drawing.SystemColors.Window
        Me.Text6.CausesValidation = True
        Me.Text6.Enabled = True
        Me.Text6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text6.HideSelection = True
        Me.Text6.ReadOnly = False
        Me.Text6.Maxlength = 0
        Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text6.MultiLine = False
        Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Text6.TabStop = True
        Me.Text6.Visible = True
        Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Text6.Name = "Text6"
        Me.Text5.AutoSize = False
        Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Text5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text5.Size = New System.Drawing.Size(409, 25)
        Me.Text5.Location = New System.Drawing.Point(544, 40)
        Me.Text5.TabIndex = 8
        Me.Text5.Text = "Text5"
        Me.Text5.AcceptsReturn = True
        Me.Text5.BackColor = System.Drawing.SystemColors.Window
        Me.Text5.CausesValidation = True
        Me.Text5.Enabled = True
        Me.Text5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text5.HideSelection = True
        Me.Text5.ReadOnly = False
        Me.Text5.Maxlength = 0
        Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text5.MultiLine = False
        Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Text5.TabStop = True
        Me.Text5.Visible = True
        Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Text5.Name = "Text5"
        Me.Text4.AutoSize = False
        Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Text4.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text4.Size = New System.Drawing.Size(409, 25)
        Me.Text4.Location = New System.Drawing.Point(544, 0)
        Me.Text4.TabIndex = 7
        Me.Text4.Text = "Text4"
        Me.Text4.AcceptsReturn = True
        Me.Text4.BackColor = System.Drawing.SystemColors.Window
        Me.Text4.CausesValidation = True
        Me.Text4.Enabled = True
        Me.Text4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text4.HideSelection = True
        Me.Text4.ReadOnly = False
        Me.Text4.Maxlength = 0
        Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text4.MultiLine = False
        Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Text4.TabStop = True
        Me.Text4.Visible = True
        Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Text4.Name = "Text4"
        Me.Text3.AutoSize = False
        Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Text3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text3.Size = New System.Drawing.Size(377, 25)
        Me.Text3.Location = New System.Drawing.Point(40, 88)
        Me.Text3.TabIndex = 6
        Me.Text3.Text = "Text3"
        Me.Text3.AcceptsReturn = True
        Me.Text3.BackColor = System.Drawing.SystemColors.Window
        Me.Text3.CausesValidation = True
        Me.Text3.Enabled = True
        Me.Text3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text3.HideSelection = True
        Me.Text3.ReadOnly = False
        Me.Text3.Maxlength = 0
        Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text3.MultiLine = False
        Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Text3.TabStop = True
        Me.Text3.Visible = True
        Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Text3.Name = "Text3"
        Me.Text2.AutoSize = False
        Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Text2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text2.Size = New System.Drawing.Size(377, 25)
        Me.Text2.Location = New System.Drawing.Point(40, 40)
        Me.Text2.TabIndex = 5
        Me.Text2.Text = "Text2"
        Me.Text2.AcceptsReturn = True
        Me.Text2.BackColor = System.Drawing.SystemColors.Window
        Me.Text2.CausesValidation = True
        Me.Text2.Enabled = True
        Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text2.HideSelection = True
        Me.Text2.ReadOnly = False
        Me.Text2.Maxlength = 0
        Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text2.MultiLine = False
        Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Text2.TabStop = True
        Me.Text2.Visible = True
        Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Text2.Name = "Text2"
        Me.List4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.List4.Size = New System.Drawing.Size(465, 87)
        Me.List4.Location = New System.Drawing.Point(504, 632)
        Me.List4.TabIndex = 4
        Me.List4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.List4.BackColor = System.Drawing.SystemColors.Window
        Me.List4.CausesValidation = True
        Me.List4.Enabled = True
        Me.List4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.List4.IntegralHeight = True
        Me.List4.Cursor = System.Windows.Forms.Cursors.Default
        Me.List4.SelectionMode = System.Windows.Forms.SelectionMode.One
        Me.List4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.List4.Sorted = False
        Me.List4.TabStop = True
        Me.List4.Visible = True
        Me.List4.MultiColumn = False
        Me.List4.Name = "List4"
        Me.List3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.List3.Size = New System.Drawing.Size(465, 215)
        Me.List3.Location = New System.Drawing.Point(504, 416)
        Me.List3.TabIndex = 3
        Me.List3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.List3.BackColor = System.Drawing.SystemColors.Window
        Me.List3.CausesValidation = True
        Me.List3.Enabled = True
        Me.List3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.List3.IntegralHeight = True
        Me.List3.Cursor = System.Windows.Forms.Cursors.Default
        Me.List3.SelectionMode = System.Windows.Forms.SelectionMode.One
        Me.List3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.List3.Sorted = False
        Me.List3.TabStop = True
        Me.List3.Visible = True
        Me.List3.MultiColumn = False
        Me.List3.Name = "List3"
        Me.List2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.List2.Size = New System.Drawing.Size(441, 87)
        Me.List2.Location = New System.Drawing.Point(20, 632)
        Me.List2.TabIndex = 2
        Me.List2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.List2.BackColor = System.Drawing.SystemColors.Window
        Me.List2.CausesValidation = True
        Me.List2.Enabled = True
        Me.List2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.List2.IntegralHeight = True
        Me.List2.Cursor = System.Windows.Forms.Cursors.Default
        Me.List2.SelectionMode = System.Windows.Forms.SelectionMode.One
        Me.List2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.List2.Sorted = False
        Me.List2.TabStop = True
        Me.List2.Visible = True
        Me.List2.MultiColumn = False
        Me.List2.Name = "List2"
        Me.Text1.AutoSize = False
        Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Text1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Text1.Size = New System.Drawing.Size(377, 25)
        Me.Text1.Location = New System.Drawing.Point(40, 0)
        Me.Text1.TabIndex = 1
        Me.Text1.Text = "Text1"
        Me.Text1.AcceptsReturn = True
        Me.Text1.BackColor = System.Drawing.SystemColors.Window
        Me.Text1.CausesValidation = True
        Me.Text1.Enabled = True
        Me.Text1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Text1.HideSelection = True
        Me.Text1.ReadOnly = False
        Me.Text1.Maxlength = 0
        Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Text1.MultiLine = False
        Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Text1.TabStop = True
        Me.Text1.Visible = True
        Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Text1.Name = "Text1"
        Me.List1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.List1.Size = New System.Drawing.Size(433, 215)
        Me.List1.Location = New System.Drawing.Point(24, 416)
        Me.List1.TabIndex = 0
        Me.List1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.List1.BackColor = System.Drawing.SystemColors.Window
        Me.List1.CausesValidation = True
        Me.List1.Enabled = True
        Me.List1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.List1.IntegralHeight = True
        Me.List1.Cursor = System.Windows.Forms.Cursors.Default
        Me.List1.SelectionMode = System.Windows.Forms.SelectionMode.One
        Me.List1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.List1.Sorted = False
        Me.List1.TabStop = True
        Me.List1.Visible = True
        Me.List1.MultiColumn = False
        Me.List1.Name = "List1"
        Me.Controls.Add(MSFlexGrid2)
        Me.Controls.Add(MSFlexGrid1)
        Me.Controls.Add(Text6)
        Me.Controls.Add(Text5)
        Me.Controls.Add(Text4)
        Me.Controls.Add(Text3)
        Me.Controls.Add(Text2)
        Me.Controls.Add(List4)
        Me.Controls.Add(List3)
        Me.Controls.Add(List2)
        Me.Controls.Add(Text1)
        Me.Controls.Add(List1)
        CType(Me.MSFlexGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MSFlexGrid2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()
    End Sub
#End Region
End Class