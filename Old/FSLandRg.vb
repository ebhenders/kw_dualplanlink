Option Strict Off
Option Explicit On
Friend Class LandRg
	Inherits System.Windows.Forms.Form
	'UPGRADE_NOTE: DefInt A-Z statement was removed. Variables were explicitly declared as type Short. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="92AFD3E3-440D-4D49-A8BF-580D74A8C9F2"'
	Dim whichrgbiotype1 As Short
	Dim whichrgbiotype2 As Short
	Dim whichMlocation As Short
	Dim whichMSet As Short
	
	Dim WhatToShow1 As Short
	Dim WhatToShow2 As Short
	'UPGRADE_WARNING: Lower bound of array whatToShowLabel was changed from 0 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
	Dim whatToShowLabel(2) As String
	
	
	Private Sub LandRg_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Dim jtic As Short
		Dim jbiotype As Short
		Dim jshow As Short
		Dim jland As Short
		whatToShowLabel(0) = "Bare Land Value"
		whatToShowLabel(1) = "Optimal Pres #"
		whichMlocation = 1
		whichrgbiotype1 = 1
		WhatToShow1 = 0
		whichrgbiotype2 = 1
		WhatToShow2 = 1
		
		Text2.Text = whatToShowLabel(WhatToShow1)
		Text4.Text = whatToShowLabel(WhatToShow2)
		
		
		For jland = 1 To NRgBioType
			List1.Items.Add(Str(jland) & " " & RgBioTypeLabel(jland))
			List3.Items.Add(Str(jland) & " " & RgBioTypeLabel(jland))
		Next jland
		For jshow = 0 To 1
			List2.Items.Add(whatToShowLabel(jshow))
			List4.Items.Add(whatToShowLabel(jshow))
		Next jshow
		MSFlexGrid1.Cols = NRgBioType + 2
		MSFlexGrid2.Cols = NRgBioType + 2
		MSFlexGrid1.Rows = MxTic + 4
		MSFlexGrid2.Rows = MxTic + 4
		
		
		
		For jbiotype = 1 To NRgBioType
			MSFlexGrid1.set_TextMatrix(0, jbiotype, "RgLand" & Str(jbiotype))
			MSFlexGrid2.set_TextMatrix(0, jbiotype, "RgLand" & Str(jbiotype))
		Next jbiotype
		
		For jtic = 1 To MxTic
			MSFlexGrid1.set_TextMatrix(jtic, 0, "Tic " & Str(jtic))
			MSFlexGrid2.set_TextMatrix(jtic, 0, "Tic " & Str(jtic))
		Next jtic
		
		'MSFlexGrid1.TextMatrix(0, 1) = "Bare Land Type"
		'MSFlexGrid2.TextMatrix(0, 1) = "Bare Land Type"
		
        'Call SetTypePrices() '############SHOULD EVENTUALLY BE CLEANE UP!
		Call FindSEV()
		Call resetgrid1()
		Call resetgrid2()
		
		
		
	End Sub
	Private Sub resetgrid1()
		Dim whichpres As Integer
		Dim jtic As Short
		Dim krow As Short
		Dim jbiotype As Short
		Dim kcol As Short
		
		kcol = 1
		For jbiotype = 1 To NRgBioType
			'MSFlexGrid1.TextMatrix(1, kcol) = whichrgbiotype1
			' MSFlexGrid1.TextMatrix(1, kcol) = jbiotype
			krow = 1
			If WhatToShow1 = 0 Then
				For jtic = 1 To MxTic
					MSFlexGrid1.set_TextMatrix(krow, kcol, VB6.Format(LandVal(jbiotype, jtic), "#####.00"))
					krow = krow + 1
				Next jtic
			Else
				For jtic = 1 To NTic1
					whichpres = LandOpt(jbiotype, jtic)
					MSFlexGrid1.set_TextMatrix(krow, kcol, VB6.Format(whichpres, "####0"))
					krow = krow + 1
				Next jtic
				For jtic = NTic1 To MxTic
					MSFlexGrid1.set_TextMatrix(krow, kcol, "***")
					krow = krow + 1
				Next jtic
			End If
			kcol = kcol + 1
		Next jbiotype
	End Sub
	Private Sub resetgrid2()
		Dim whichpres As Integer
		Dim jtic As Short
		Dim krow As Short
		Dim jbiotype As Short
		Dim kcol As Short
		
		kcol = 1
		For jbiotype = 1 To NRgBioType
			krow = 1
			If WhatToShow2 = 0 Then
				For jtic = 1 To MxTic
					MSFlexGrid2.set_TextMatrix(krow, kcol, VB6.Format(LandVal(jbiotype, jtic), "#####.00"))
					krow = krow + 1
				Next jtic
			Else
				For jtic = 1 To NTic1
					whichpres = LandOpt(jbiotype, jtic)
					MSFlexGrid2.set_TextMatrix(krow, kcol, VB6.Format(whichpres, "####0"))
					krow = krow + 1
				Next jtic
				
				For jtic = NTic1 To MxTic
					MSFlexGrid2.set_TextMatrix(krow, kcol, "***")
					krow = krow + 1
				Next jtic
			End If
			kcol = kcol + 1
		Next jbiotype
	End Sub
	
	
	
	
	'UPGRADE_WARNING: Event List2.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub List2_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List2.SelectedIndexChanged
		WhatToShow1 = List2.SelectedIndex
		Text2.Text = whatToShowLabel(WhatToShow1)
		Call resetgrid1()
	End Sub
	
	
	
	'UPGRADE_WARNING: Event List4.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub List4_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List4.SelectedIndexChanged
		WhatToShow2 = List4.SelectedIndex
		Text4.Text = whatToShowLabel(WhatToShow2)
		Call resetgrid2()
	End Sub
	
	Private Sub FindSEV()
		Dim TVAL As Single
		Dim ktime As Short
		Dim jtic As Short
		Dim SEV As Single
		Dim ktic As Short
		Dim kmtype As Short
		Dim kage As Short
		Dim jitem As Integer
		Dim NetPV As Single
		Dim rotlen As Short
		Dim kpresRG As Integer
		Dim jpresRG As Integer
		Dim kRgBioType As Short
		Dim jbiotype As Short
		'UPGRADE_WARNING: Lower bound of array LandVal was changed from 1,1 to 0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		ReDim LandVal(NRgBioType, MxTic)
		'UPGRADE_WARNING: Lower bound of array LandOpt was changed from 1,1 to 0,0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		ReDim LandOpt(NRgBioType, NTic1)
		
		whichMlocation = 1
		For jbiotype = 1 To NRgBioType
			kRgBioType = jbiotype
			LandVal(kRgBioType, NTic1) = -9999
			For jpresRG = RgBioType(kRgBioType).firstpres To RgBioType(kRgBioType).lastpres
				kpresRG = jpresRG
                rotlen = RgPres_Rotlength(kpresRG)
				NetPV = 0
				' Add NPV from market returns
                For jitem = RgPres_FirstFlow(kpresRG) To RgPres_LastFlow(kpresRG)
                    kage = Rgflow_tic(jitem)
                    kmtype = Rgflow_type(jitem)
                    ktic = NTic1 + kage
                    NetPV = NetPV + MtypePr(kmtype, whichMlocation, ktic) * Rgflow_quan(jitem)
                Next jitem
				
				' Add NPV from conditions for all map layers
				'For jmap = 1 To NMapLayer
				'   ktic = NTic + 1
				'   kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
				'   For jage = 1 To rotlen
				'      NetPV! = NetPV! + CondTypePr!(kRgBioType, jage, kMapArea, ktic)
				'      NetPV! = NetPV! + CondTypePr!(RgPres(kpresRG&).Coversite(jage), RgPres(kpresRG&).Age(jage), kMapArea, ktic)
				'      ktic = ktic + 1
				'   Next jage
				'Next jmap
				
				SEV = NetPV * SEVFactor(rotlen)
				If SEV > LandVal(kRgBioType, NTic1) Then
					LandVal(kRgBioType, NTic1) = SEV
					LandOpt(kRgBioType, NTic1) = jpresRG
				End If
			Next jpresRG
			
			' Set LandVal!() for periods beyond NTIC1
			
			For jtic = NTic1 + 1 To MxTic
				LandVal(kRgBioType, jtic) = LandVal(kRgBioType, jtic - 1) * MDisc(2)
			Next jtic
			
			' DETERMINE LandVal!() FOR EACH Possible TIC STARTING WITH LAST PERIOD
			
			For jtic = NTic To 1 Step -1
				For jpresRG = RgBioType(kRgBioType).firstpres To RgBioType(kRgBioType).lastpres
					kpresRG = jpresRG
                    rotlen = RgPres_Rotlength(kpresRG)
					NetPV = 0
					' add values for market flows
                    For jitem = RgPres_FirstFlow(kpresRG) To RgPres_LastFlow(kpresRG)
                        ktime = jtic + Rgflow_tic(jitem)
                        kmtype = Rgflow_type(jitem)
                        NetPV = NetPV + MtypePr(kmtype, whichMlocation, ktime) * Rgflow_quan(jitem)
                    Next jitem
					
					'  Add value for conditions for all map layers
					'For jmap = 1 To NMapLayer
					'kMapArea = MapAreaID(jmap, MapLayerColor(jmap))
					'ktic = jtic
					'For jage = 1 To rotlen
					'  NetPV! = NetPV! + CondTypePr!(RgPres(kpresRG&).Coversite(jage), RgPres(kpresRG&).Age(jage), kMapArea, ktic)
					'  ktic = ktic + 1
					'Next jage
					'Next jmap
					
					TVAL = NetPV + LandVal(kRgBioType, jtic + rotlen)
					If TVAL > LandVal(kRgBioType, jtic) Then
						LandVal(kRgBioType, jtic) = TVAL
						LandOpt(kRgBioType, jtic) = kpresRG
					End If
				Next jpresRG
			Next jtic
			
			
		Next jbiotype
	End Sub
End Class