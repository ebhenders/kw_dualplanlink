<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class LandRg
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents List4 As System.Windows.Forms.ListBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents List3 As System.Windows.Forms.ListBox
	Public WithEvents MSFlexGrid2 As AxMSFlexGridLib.AxMSFlexGrid
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents List2 As System.Windows.Forms.ListBox
	Public WithEvents List1 As System.Windows.Forms.ListBox
	Public WithEvents MSFlexGrid1 As AxMSFlexGridLib.AxMSFlexGrid
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(LandRg))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.List4 = New System.Windows.Forms.ListBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.List3 = New System.Windows.Forms.ListBox
		Me.MSFlexGrid2 = New AxMSFlexGridLib.AxMSFlexGrid
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.List2 = New System.Windows.Forms.ListBox
		Me.List1 = New System.Windows.Forms.ListBox
		Me.MSFlexGrid1 = New AxMSFlexGridLib.AxMSFlexGrid
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.MSFlexGrid2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.MSFlexGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Text = "Regen Info"
		Me.ClientSize = New System.Drawing.Size(585, 447)
		Me.Location = New System.Drawing.Point(4, 23)
		Me.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.Name = "LandRg"
		Me.List4.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.List4.Size = New System.Drawing.Size(377, 47)
		Me.List4.Location = New System.Drawing.Point(640, 896)
		Me.List4.TabIndex = 7
		Me.List4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.List4.BackColor = System.Drawing.SystemColors.Window
		Me.List4.CausesValidation = True
		Me.List4.Enabled = True
		Me.List4.ForeColor = System.Drawing.SystemColors.WindowText
		Me.List4.IntegralHeight = True
		Me.List4.Cursor = System.Windows.Forms.Cursors.Default
		Me.List4.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.List4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.List4.Sorted = False
		Me.List4.TabStop = True
		Me.List4.Visible = True
		Me.List4.MultiColumn = False
		Me.List4.Name = "List4"
		Me.Text4.AutoSize = False
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
		Me.Text4.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Text4.Size = New System.Drawing.Size(361, 33)
		Me.Text4.Location = New System.Drawing.Point(648, 88)
		Me.Text4.TabIndex = 6
		Me.Text4.Text = "Text4"
		Me.Text4.AcceptsReturn = True
		Me.Text4.BackColor = System.Drawing.SystemColors.Window
		Me.Text4.CausesValidation = True
		Me.Text4.Enabled = True
		Me.Text4.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text4.Name = "Text4"
		Me.List3.Size = New System.Drawing.Size(497, 279)
		Me.List3.Location = New System.Drawing.Point(584, 520)
		Me.List3.TabIndex = 5
		Me.List3.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.List3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.List3.BackColor = System.Drawing.SystemColors.Window
		Me.List3.CausesValidation = True
		Me.List3.Enabled = True
		Me.List3.ForeColor = System.Drawing.SystemColors.WindowText
		Me.List3.IntegralHeight = True
		Me.List3.Cursor = System.Windows.Forms.Cursors.Default
		Me.List3.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.List3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.List3.Sorted = False
		Me.List3.TabStop = True
		Me.List3.Visible = True
		Me.List3.MultiColumn = False
		Me.List3.Name = "List3"
		MSFlexGrid2.OcxState = CType(resources.GetObject("MSFlexGrid2.OcxState"), System.Windows.Forms.AxHost.State)
		Me.MSFlexGrid2.Size = New System.Drawing.Size(497, 329)
		Me.MSFlexGrid2.Location = New System.Drawing.Point(584, 160)
		Me.MSFlexGrid2.TabIndex = 4
		Me.MSFlexGrid2.Name = "MSFlexGrid2"
		Me.Text2.AutoSize = False
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
		Me.Text2.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Text2.Size = New System.Drawing.Size(377, 33)
		Me.Text2.Location = New System.Drawing.Point(80, 88)
		Me.Text2.TabIndex = 3
		Me.Text2.Text = "Text2"
		Me.Text2.AcceptsReturn = True
		Me.Text2.BackColor = System.Drawing.SystemColors.Window
		Me.Text2.CausesValidation = True
		Me.Text2.Enabled = True
		Me.Text2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Text2.Name = "Text2"
		Me.List2.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.List2.Size = New System.Drawing.Size(385, 47)
		Me.List2.Location = New System.Drawing.Point(72, 896)
		Me.List2.TabIndex = 2
		Me.List2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.List2.BackColor = System.Drawing.SystemColors.Window
		Me.List2.CausesValidation = True
		Me.List2.Enabled = True
		Me.List2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.List2.IntegralHeight = True
		Me.List2.Cursor = System.Windows.Forms.Cursors.Default
		Me.List2.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.List2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.List2.Sorted = False
		Me.List2.TabStop = True
		Me.List2.Visible = True
		Me.List2.MultiColumn = False
		Me.List2.Name = "List2"
		Me.List1.Size = New System.Drawing.Size(521, 279)
		Me.List1.Location = New System.Drawing.Point(8, 520)
		Me.List1.TabIndex = 1
		Me.List1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.List1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.List1.BackColor = System.Drawing.SystemColors.Window
		Me.List1.CausesValidation = True
		Me.List1.Enabled = True
		Me.List1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.List1.IntegralHeight = True
		Me.List1.Cursor = System.Windows.Forms.Cursors.Default
		Me.List1.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.List1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.List1.Sorted = False
		Me.List1.TabStop = True
		Me.List1.Visible = True
		Me.List1.MultiColumn = False
		Me.List1.Name = "List1"
		MSFlexGrid1.OcxState = CType(resources.GetObject("MSFlexGrid1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.MSFlexGrid1.Size = New System.Drawing.Size(521, 329)
		Me.MSFlexGrid1.Location = New System.Drawing.Point(8, 160)
		Me.MSFlexGrid1.TabIndex = 0
		Me.MSFlexGrid1.Name = "MSFlexGrid1"
		Me.Controls.Add(List4)
		Me.Controls.Add(Text4)
		Me.Controls.Add(List3)
		Me.Controls.Add(MSFlexGrid2)
		Me.Controls.Add(Text2)
		Me.Controls.Add(List2)
		Me.Controls.Add(List1)
		Me.Controls.Add(MSFlexGrid1)
		CType(Me.MSFlexGrid1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.MSFlexGrid2, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class