Option Strict Off
Option Explicit On
Friend Class ShadPrCoef
	Inherits System.Windows.Forms.Form
	'UPGRADE_NOTE: DefInt A-Z statement was removed. Variables were explicitly declared as type Short. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="92AFD3E3-440D-4D49-A8BF-580D74A8C9F2"'
	Dim NeedToChange1 As Short
	Dim NeedToChange2 As Short
	Dim NeedToChange3 As Short
	Dim WhatToShow1 As String
	Private Sub cmdcan1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdcan1.Click
		MSFlexGrid1.Row = 0
		MSFlexGrid1.Col = 0
		Call resetgrid1()
	End Sub
	
	Private Sub cmdSave1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSave1.Click
		Dim CellString As String
		Dim CellValue As Short
		Dim jbreak As Short
		Dim Mnbreak As Short
		Dim jset As Short
		Dim krow As Short
		MSFlexGrid1.Row = 0
		MSFlexGrid1.Col = 0
		krow = 0
		For jset = 1 To PriceAdjNSet
			Mnbreak = 0
			'UPGRADE_WARNING: Couldn't resolve default property of object FloatNBreak(jset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			For jbreak = 1 To FloatNBreak(jset)
				CellValue = Val(MSFlexGrid1.get_TextMatrix(krow + jbreak, 2))
				If CellValue > Mnbreak Then
					Mnbreak = CellValue
				Else
					CellValue = Mnbreak
				End If
				'UPGRADE_WARNING: Couldn't resolve default property of object FloatPercentDev(jset, jbreak). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				FloatPercentDev(jset, jbreak) = CellValue
				CellString = MSFlexGrid1.get_TextMatrix(krow + jbreak, 3)
				FloatPriceAdj(jset, jbreak) = Val(CellString)
			Next jbreak
			
			Mnbreak = 0
			'UPGRADE_WARNING: Couldn't resolve default property of object ShapeNBreak(jset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			For jbreak = 1 To ShapeNBreak(jset)
				CellValue = Val(MSFlexGrid1.get_TextMatrix(krow + jbreak, 4))
				If CellValue > Mnbreak Then
					Mnbreak = CellValue
				Else
					CellValue = Mnbreak
				End If
				'UPGRADE_WARNING: Couldn't resolve default property of object ShapePercentDev(jset, jbreak). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				ShapePercentDev(jset, jbreak) = CellValue
				CellString = MSFlexGrid1.get_TextMatrix(krow + jbreak, 5)
				ShapePriceAdj(jset, jbreak) = Val(CellString)
			Next jbreak
			
			Mnbreak = 0
			'UPGRADE_WARNING: Couldn't resolve default property of object SmoothNBreak(jset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			For jbreak = 1 To SmoothNBreak(jset)
				CellValue = Val(MSFlexGrid1.get_TextMatrix(krow + jbreak, 6))
				If CellValue > Mnbreak Then
					Mnbreak = CellValue
				Else
					CellValue = Mnbreak
				End If
				'UPGRADE_WARNING: Couldn't resolve default property of object SmoothPercentDev(jset, jbreak). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				SmoothPercentDev(jset, jbreak) = CellValue
				CellString = MSFlexGrid1.get_TextMatrix(krow + jbreak, 7)
				SmoothPriceAdj(jset, jbreak) = Val(CellString)
			Next jbreak
			krow = krow + PriceAdjMxBreakPoint
		Next jset
		MSFlexGrid1.Row = 0
		MSFlexGrid1.Col = 0
	End Sub
	
	
	
	Private Sub ShadPrCoef_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Dim jset As Short
		Text2.Visible = False
		Text3.Visible = False
		Text4.Visible = False
		MSFlexGrid1.Rows = (PriceAdjMxBreakPoint) * PriceAdjNSet + 1
		MSFlexGrid1.Cols = 8
		MSFlexGrid1.FormatString = "<Adj Set  |   < Break #|^   Float Break Pt(%)|< Float Adj|^    Shape Break Pt(%) |< Shape Adj |>   Smooth Break Pt(%) |< Smooth Adj|"
		Call resetgrid1()
		
		
		MSFlexGrid2.Cols = 2
		MSFlexGrid2.Rows = NMSet + 1
		MSFlexGrid2.FormatString = "<Market Set      |^Shadow Price Adjustment Set"
		Text3.Visible = False
		
		
		MSFlexGrid3.Cols = 2
		MSFlexGrid3.Rows = NCondSet + 1
		MSFlexGrid3.FormatString = "<Condition Set   |^Shadow Price Adjustment Set"
		Text4.Visible = False
		
		For jset = 1 To NMSet
            MSFlexGrid2.set_TextMatrix(jset, 0, MSet_Label(jset))
            'UPGRADE_WARNING: Couldn't resolve default property of object MsetPrAdjSet(jset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            MSFlexGrid2.set_TextMatrix(jset, 1, MSet_PrAdjSet(jset))
        Next jset
        For jset = 1 To NCondSet
            MSFlexGrid3.set_TextMatrix(jset, 0, CondSet_Label(jset))
            'UPGRADE_WARNING: Couldn't resolve default property of object CondsetPrAdjSet(jset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            MSFlexGrid3.set_TextMatrix(jset, 1, CondSet_PrAdjSet(jset))
        Next jset
    End Sub

    Private Sub resetgrid1()
        Dim jbreak As Short
        Dim jset As Short
        Dim krow As Short
        Dim jy As Short
        Dim jx As Short
        For jx = 1 To MSFlexGrid1.Rows - 1
            For jy = 0 To MSFlexGrid1.Cols - 1
                MSFlexGrid1.set_TextMatrix(jx, jy, "***")
            Next jy
        Next jx
        krow = 0
        For jset = 1 To PriceAdjNSet
            'UPGRADE_WARNING: Couldn't resolve default property of object FloatNBreak(jset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For jbreak = 1 To FloatNBreak(jset)
                MSFlexGrid1.set_TextMatrix(krow + jbreak, 0, jset)
                MSFlexGrid1.set_TextMatrix(krow + jbreak, 1, jbreak)
                'UPGRADE_WARNING: Couldn't resolve default property of object FloatPercentDev(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                MSFlexGrid1.set_TextMatrix(krow + jbreak, 2, VB6.Format(FloatPercentDev(jset, jbreak), "####0"))
                MSFlexGrid1.set_TextMatrix(krow + jbreak, 3, VB6.Format(FloatPriceAdj(jset, jbreak), "###.000"))
            Next jbreak
            'UPGRADE_WARNING: Couldn't resolve default property of object ShapeNBreak(jset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For jbreak = 1 To ShapeNBreak(jset)
                MSFlexGrid1.set_TextMatrix(krow + jbreak, 0, jset)
                MSFlexGrid1.set_TextMatrix(krow + jbreak, 1, jbreak)
                'UPGRADE_WARNING: Couldn't resolve default property of object ShapePercentDev(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                MSFlexGrid1.set_TextMatrix(krow + jbreak, 4, VB6.Format(ShapePercentDev(jset, jbreak), "####0"))
                MSFlexGrid1.set_TextMatrix(krow + jbreak, 5, VB6.Format(ShapePriceAdj(jset, jbreak), "###.000"))
            Next jbreak
            'UPGRADE_WARNING: Couldn't resolve default property of object SmoothNBreak(jset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For jbreak = 1 To SmoothNBreak(jset)
                MSFlexGrid1.set_TextMatrix(krow + jbreak, 0, jset)
                MSFlexGrid1.set_TextMatrix(krow + jbreak, 1, jbreak)
                'UPGRADE_WARNING: Couldn't resolve default property of object SmoothPercentDev(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                MSFlexGrid1.set_TextMatrix(krow + jbreak, 6, VB6.Format(SmoothPercentDev(jset, jbreak), "####0"))
                MSFlexGrid1.set_TextMatrix(krow + jbreak, 7, VB6.Format(SmoothPriceAdj(jset, jbreak), "###.000"))
            Next jbreak
            krow = krow + PriceAdjMxBreakPoint
        Next jset
        MSFlexGrid1.Row = 0
        MSFlexGrid1.Col = 0

    End Sub

    Private Sub MSFlexGrid1_EnterCell(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MSFlexGrid1.EnterCell
        'passing cell's contents to textbox
        NeedToChange1 = 1
        Text2.Text = MSFlexGrid1.Text
        If MSFlexGrid1.MouseRow = 0 Then
            NeedToChange1 = 0
            Text2.Visible = False
            Exit Sub
        End If

        MSFlexGrid1.Row = MSFlexGrid1.MouseRow
        MSFlexGrid1.Col = MSFlexGrid1.MouseCol
        ' clear contents of current cell"
        'Text2.Text = ""
        'place textbox over current cell
        Text2.Visible = CBool("false")
        Text2.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(MSFlexGrid1.Top) + MSFlexGrid1.CellTop)
        Text2.Left = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(MSFlexGrid1.Left) + MSFlexGrid1.CellLeft)
        Text2.Width = VB6.TwipsToPixelsX(MSFlexGrid1.CellWidth)
        Text2.Height = VB6.TwipsToPixelsY(MSFlexGrid1.CellHeight)
        'passing cell's contents to textbox
        'Text2.Text = MSFlexGrid1.Text
        'move focus to textbox
        Text2.Visible = True
        Text2.Focus()
    End Sub

    Private Sub MSFlexGrid1_LeaveCell(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MSFlexGrid1.LeaveCell
        If NeedToChange1 > 0 Then
            MSFlexGrid1.Text = Text2.Text
            Text2.Visible = False
        End If
    End Sub
    Private Sub resetgrid2()
        Dim jset As Short
        For jset = 1 To NMSet
            MSFlexGrid2.set_TextMatrix(jset, 0, MSet_Label(jset))
            'UPGRADE_WARNING: Couldn't resolve default property of object MsetPrAdjSet(jset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            MSFlexGrid2.set_TextMatrix(jset, 1, MSet_PrAdjSet(jset))
        Next jset
        MSFlexGrid2.Row = 0
        MSFlexGrid2.Col = 0

    End Sub

    Private Sub MSFlexGrid2_EnterCell(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MSFlexGrid2.EnterCell
        NeedToChange2 = 1
        Text3.Text = MSFlexGrid2.Text
        If MSFlexGrid2.MouseRow = 0 Then
            NeedToChange2 = 0
            Text3.Visible = False
            Exit Sub
        End If
        MSFlexGrid2.Row = MSFlexGrid2.MouseRow
        MSFlexGrid2.Col = MSFlexGrid2.MouseCol
        Text3.Visible = CBool("false")
        Text3.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(MSFlexGrid2.Top) + MSFlexGrid2.CellTop)
        Text3.Left = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(MSFlexGrid2.Left) + MSFlexGrid2.CellLeft)
        Text3.Width = VB6.TwipsToPixelsX(MSFlexGrid2.CellWidth)
        Text3.Height = VB6.TwipsToPixelsY(MSFlexGrid2.CellHeight)
        Text3.Visible = True
        Text3.Focus()

    End Sub
    Private Sub MSFlexGrid2_LeaveCell(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MSFlexGrid2.LeaveCell
        If NeedToChange2 > 0 Then MSFlexGrid2.Text = Text3.Text
        Text3.Visible = False
    End Sub

    Private Sub cmdcan2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdcan2.Click
        MSFlexGrid2.Row = 0
        MSFlexGrid2.Col = 0

        Call resetgrid2()
    End Sub

    Private Sub cmdsave2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdsave2.Click
        Dim jset As Short
        MSFlexGrid2.Row = 0
        MSFlexGrid2.Col = 0


        For jset = 1 To NMSet
            'UPGRADE_WARNING: Couldn't resolve default property of object MsetPrAdjSet(jset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            MSet_PrAdjSet(jset) = Val(MSFlexGrid2.get_TextMatrix(jset, 1))
        Next jset
    End Sub






    Private Sub MSFlexGrid3_EnterCell(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MSFlexGrid3.EnterCell
        NeedToChange3 = 1
        Text4.Text = MSFlexGrid3.Text
        If MSFlexGrid3.MouseRow = 0 Then
            NeedToChange2 = 0
            Text4.Visible = False
            Exit Sub
        End If
        MSFlexGrid3.Row = MSFlexGrid3.MouseRow
        MSFlexGrid3.Col = MSFlexGrid3.MouseCol
        Text4.Visible = CBool("false")
        Text4.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(MSFlexGrid3.Top) + MSFlexGrid3.CellTop)
        Text4.Left = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(MSFlexGrid3.Left) + MSFlexGrid3.CellLeft)
        Text4.Width = VB6.TwipsToPixelsX(MSFlexGrid3.CellWidth)
        Text4.Height = VB6.TwipsToPixelsY(MSFlexGrid3.CellHeight)
        Text4.Visible = True
        Text4.Focus()

    End Sub

    Private Sub MSFlexGrid3_LeaveCell(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MSFlexGrid3.LeaveCell
        If NeedToChange3 > 0 Then MSFlexGrid3.Text = Text4.Text
        Text4.Visible = False
    End Sub

    Private Sub cmdcan3_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdcan3.Click
        MSFlexGrid3.Row = 0
        MSFlexGrid3.Col = 0
        Call ReSetGrid3()
    End Sub

    Private Sub cmdsave3_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdsave3.Click
        Dim jset As Short
        MSFlexGrid3.Row = 0
        MSFlexGrid3.Col = 0


        For jset = 1 To NCondSet
            'UPGRADE_WARNING: Couldn't resolve default property of object CondsetPrAdjSet(jset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            CondSet_PrAdjSet(jset) = Val(MSFlexGrid3.get_TextMatrix(jset, 1))
        Next jset
    End Sub
	
	Private Sub ReSetGrid3()
		Dim jset As Short
		For jset = 1 To NCondSet
            MSFlexGrid3.set_TextMatrix(jset, 0, CondSet_Label(jset))
            'UPGRADE_WARNING: Couldn't resolve default property of object CondsetPrAdjSet(jset). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            MSFlexGrid3.set_TextMatrix(jset, 1, CondSet_PrAdjSet(jset))
		Next jset
		MSFlexGrid3.Row = 0
		MSFlexGrid3.Col = 0
		
	End Sub
End Class