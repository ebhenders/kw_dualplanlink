Option Strict Off
Option Explicit On
Friend Class CondType
	Inherits System.Windows.Forms.Form
	'UPGRADE_NOTE: DefInt A-Z statement was removed. Variables were explicitly declared as type Short. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="92AFD3E3-440D-4D49-A8BF-580D74A8C9F2"'
	Dim whichcoversite1 As Short
	Dim whichMapArea1 As Short
	Dim whichcoversite2 As Short
	Dim whichMapArea2 As Short
	Dim WhatToShow1 As String
	Dim WhatToShow2 As String
	
	Private Sub CondType_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Dim jage As Short
		Dim jtic As Short
		Dim jArea As Short
		Dim jcs As Short
		
		WhatToShow1 = "Area"
		WhatToShow2 = "Area"
		whichcoversite1 = 1
		whichMapArea1 = 1
		whichcoversite2 = 1
		whichMapArea2 = 1
		Text1.Text = coversiteLabel(whichcoversite1)
		Text2.Text = MapAreaLabel(whichMapArea1)
		Text3.Text = coversiteLabel(whichcoversite2)
		Text4.Text = MapAreaLabel(whichMapArea2)
		Text5.Text = "Area in Each Age Class by Period"
		Text6.Text = "Area in Each Age Class by Period"
		
		List5.Items.Add("Area in Each Age Class")
		List6.Items.Add("Area in Each Age Class")
		List5.Items.Add("Shadow Price for Each Age Class")
		List6.Items.Add("Shadow Price for Each Age Class")
		
		For jcs = 1 To Ncoversite
			List1.Items.Add(coversiteLabel(jcs))
			List3.Items.Add(coversiteLabel(jcs))
		Next jcs
		For jArea = 1 To NMapArea
			List2.Items.Add(MapAreaLabel(jArea))
			List4.Items.Add(MapAreaLabel(jArea))
		Next jArea
		MSFlexGrid1.Rows = MxAgeClass + 2
		MSFlexGrid2.Rows = MxAgeClass + 2
		MSFlexGrid1.Cols = NTic + 2
		MSFlexGrid2.Cols = NTic + 2
		
		For jtic = 1 To NTic + 1
			MSFlexGrid1.set_TextMatrix(0, jtic, "Period " & Str(jtic - 1))
			MSFlexGrid2.set_TextMatrix(0, jtic, "Period " & Str(jtic - 1))
		Next jtic
		For jage = 1 To MxAgeClass
			MSFlexGrid1.set_TextMatrix(jage, 0, jage)
			MSFlexGrid2.set_TextMatrix(jage, 0, jage)
		Next jage
		MSFlexGrid1.set_TextMatrix(0, 0, "Age")
		MSFlexGrid2.set_TextMatrix(0, 0, "Age")
		Call resetgrid1()
		Call resetgrid2()
	End Sub
	
	
	Private Sub resetgrid1()
		Dim AreaSum As Single
		Dim jtic As Short
		Dim jage As Short
		
		If WhatToShow1 = "prices" Then
			For jage = 1 To MxAgeClass + 1
				MSFlexGrid1.set_TextMatrix(jage, 1, " ")
			Next jage
			For jtic = 1 To NTic
				
				For jage = 1 To MxAgeClass
					MSFlexGrid1.set_TextMatrix(jage, jtic + 1, VB6.Format(CondTypePr(whichcoversite1, jage, whichMapArea1, jtic), "#####.000"))
				Next jage
				MSFlexGrid1.set_TextMatrix(MxAgeClass + 1, jtic, " ")
			Next jtic
			MSFlexGrid1.set_TextMatrix(MxAgeClass + 1, 0, "")
		Else
			For jtic = 0 To NTic
				AreaSum = 0
				For jage = 1 To MxAgeClass
					AreaSum = AreaSum + CondTypeFlow(whichcoversite1, jage, whichMapArea1, jtic)
					MSFlexGrid1.set_TextMatrix(jage, jtic + 1, VB6.Format(CondTypeFlow(whichcoversite1, jage, whichMapArea1, jtic), "#######0"))
				Next jage
				MSFlexGrid1.set_TextMatrix(MxAgeClass + 1, jtic + 1, AreaSum)
			Next jtic
			MSFlexGrid1.set_TextMatrix(MxAgeClass + 1, 0, "Total")
		End If
		
	End Sub
	
	Private Sub resetgrid2()
		Dim AreaSum As Single
		Dim jtic As Short
		Dim jage As Short
		
		
		If WhatToShow2 = "prices" Then
			For jage = 1 To MxAgeClass + 1
				MSFlexGrid2.set_TextMatrix(jage, 1, " ")
			Next jage
			
			
			For jtic = 1 To NTic
				For jage = 1 To MxAgeClass
					MSFlexGrid2.set_TextMatrix(jage, jtic + 1, VB6.Format(CondTypePr(whichcoversite2, jage, whichMapArea2, jtic), "#####.000"))
				Next jage
				MSFlexGrid1.set_TextMatrix(MxAgeClass + 1, jtic, "")
			Next jtic
			MSFlexGrid1.set_TextMatrix(MxAgeClass + 1, 0, "")
		Else
			For jtic = 0 To NTic
				AreaSum = 0
				For jage = 1 To MxAgeClass
					AreaSum = AreaSum + CondTypeFlow(whichcoversite2, jage, whichMapArea2, jtic)
					MSFlexGrid2.set_TextMatrix(jage, jtic + 1, VB6.Format(CondTypeFlow(whichcoversite2, jage, whichMapArea2, jtic), "#######0"))
				Next jage
				MSFlexGrid2.set_TextMatrix(MxAgeClass + 1, jtic + 1, AreaSum)
			Next jtic
			MSFlexGrid2.set_TextMatrix(MxAgeClass + 1, 0, "total")
		End If
	End Sub
	
	
	
	'UPGRADE_WARNING: Event List1.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub List1_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List1.SelectedIndexChanged
		whichcoversite1 = List1.SelectedIndex + 1
		Text1.Text = coversiteLabel(whichcoversite1)
		Call resetgrid1()
	End Sub
	
	'UPGRADE_WARNING: Event List2.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub List2_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List2.SelectedIndexChanged
		whichMapArea1 = List2.SelectedIndex + 1
		Text2.Text = MapAreaLabel(whichMapArea1)
		Call resetgrid1()
	End Sub
	
	'UPGRADE_WARNING: Event List3.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub List3_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List3.SelectedIndexChanged
		whichcoversite2 = List3.SelectedIndex + 1
		Text3.Text = coversiteLabel(whichcoversite2)
		Call resetgrid2()
	End Sub
	
	'UPGRADE_WARNING: Event List4.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub List4_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List4.SelectedIndexChanged
		whichMapArea2 = List4.SelectedIndex + 1
		Text4.Text = MapAreaLabel(whichMapArea2)
		Call resetgrid2()
	End Sub
	
	'UPGRADE_WARNING: Event List5.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub List5_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List5.SelectedIndexChanged
		If List5.SelectedIndex = 1 Then
			WhatToShow1 = "prices"
			Text5.Text = "Shadow Price for Each Age Class"
		Else
			WhatToShow1 = "areass"
			Text5.Text = "Area in Each Age Class"
		End If
		Call resetgrid1()
	End Sub
	
	'UPGRADE_WARNING: Event List6.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub List6_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List6.SelectedIndexChanged
		If List6.SelectedIndex = 1 Then
			WhatToShow2 = "prices"
			Text6.Text = "Shadow Price for Each Age Class"
		Else
			WhatToShow2 = "areas"
			Text6.Text = "Area in Each Age Class"
		End If
		Call resetgrid2()
	End Sub
End Class