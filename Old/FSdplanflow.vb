Option Strict Off
Option Explicit On
Friend Class MKTset
    Inherits System.Windows.Forms.Form
    'UPGRADE_NOTE: DefInt A-Z statement was removed. Variables were explicitly declared as type Short. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="92AFD3E3-440D-4D49-A8BF-580D74A8C9F2"'


    Dim whichMSet As Short
    Dim whatToShow As Short
    Dim whichMSet2 As Short
    Dim WhatToShow2 As Short
    'UPGRADE_WARNING: Lower bound of array whatToShowLabel was changed from 0 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    Dim whatToShowLabel(4) As String
    'UPGRADE_WARNING: Lower bound of array ConstTypeLabel was changed from 0 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
    Dim ConstTypeLabel(7) As String


    Private Sub MKTset_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim jshow As Short
        Dim jmset As Short
        whatToShowLabel(0) = "Deviation from Target(%)"
        'whatToShowLabel$(1) = "Flow Quantity 10,000/period"
        'whatToShowLabel$(2) = "Target 10,000/period"
        whatToShowLabel(1) = "Flow Quantity period"
        whatToShowLabel(2) = "Target period"
        whatToShowLabel(3) = "Shadow Price ($/Unit"
        whatToShowLabel(4) = "Change in Shadow Price ($/Unit"

        ConstTypeLabel(0) = "Unconstrained"
        ConstTypeLabel(1) = "Target Levels"
        ConstTypeLabel(2) = "Demand = f(price)"
        ConstTypeLabel(3) = "Lower bounds"
        ConstTypeLabel(4) = "Upper bounds"
        ConstTypeLabel(5) = "Lower and Upper Bounds"
        ConstTypeLabel(6) = "% Changes, All Periods"
        ConstTypeLabel(7) = "% Changes, except period 1"


        whichMSet = 1
        whatToShow = 1
        whichMSet2 = 1
        WhatToShow2 = 3
        Text1.Text = MSet_Label(whichMSet)
        'UPGRADE_WARNING: Couldn't resolve default property of object MSetConstraintOption(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Text2.Text = ConstTypeLabel(MSet_ConstraintOption(whichMSet))
        Text3.Text = whatToShowLabel(whatToShow)
        Text4.Text = MSet_Label(whichMSet2)
        'UPGRADE_WARNING: Couldn't resolve default property of object MSetConstraintOption(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Text5.Text = ConstTypeLabel(MSet_ConstraintOption(whichMSet2))
        Text6.Text = whatToShowLabel(WhatToShow2)
        For jmset = 1 To NMSet
            List1.Items.Add(MSet_Label(whichMSet))
            List3.Items.Add(MSet_Label(whichMSet))
            whichMSet = whichMSet + 1
        Next jmset
        For jshow = 0 To 4
            List2.Items.Add(whatToShowLabel(jshow))
            List4.Items.Add(whatToShowLabel(jshow))
        Next jshow
        whichMSet = 1
        MSFlexGrid1.Rows = NTic1
        MSFlexGrid2.Rows = NTic1
        MSFlexGrid1.set_TextMatrix(0, 0, "Period")
        MSFlexGrid2.set_TextMatrix(0, 0, "Period")
        Call resetgrid1()
        Call resetgrid2()

    End Sub


    Private Sub resetgrid1()
        Dim PrChange As Single
        Dim kposprior As Short
        Dim kprior As Short
        Dim Lower As Single
        Dim Upper As Single
        Dim jtic As Short
        Dim kposition As Short
        Dim jiter As Short
        Dim NiterToShow As Short
        Dim kiter As Short
        kiter = IterationNumber
        If kiter = 0 Then kiter = 1
        NiterToShow = kiter
        If NiterToShow > NIterToSave Then NiterToShow = NIterToSave

        MSFlexGrid1.Cols = NiterToShow + 1
        If whatToShow = 0 Then
            For jiter = 1 To NiterToShow
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                MSFlexGrid1.set_TextMatrix(0, jiter, Str(kiter) & " " & AdjIterType(kposition))
                For jtic = 1 To NTic
                    MSFlexGrid1.set_TextMatrix(jtic, 0, jtic)
                    MSFlexGrid1.set_TextMatrix(jtic, jiter, VB6.Format(MSet_Dev(whichMSet, kposition, jtic), "####.00"))
                Next jtic
                kiter = kiter - 1
            Next jiter
        ElseIf whatToShow = 1 Then
            For jiter = 1 To NiterToShow
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                MSFlexGrid1.set_TextMatrix(0, jiter, Str(kiter) & " " & AdjIterType(kposition))
                For jtic = 1 To NTic
                    MSFlexGrid1.set_TextMatrix(jtic, 0, jtic)
                    MSFlexGrid1.set_TextMatrix(jtic, jiter, VB6.Format(MSet_Flow(whichMSet, kposition, jtic) / NYearPerTic, "######0"))
                    'MSFlexGrid1.TextMatrix(jtic, Jiter) = Format$(MSetFlow#(kposition, whichMSet, jtic) / 10000, "######0")
                Next jtic
                kiter = kiter - 1
            Next jiter
        ElseIf whatToShow = 2 Then
            kposition = ((kiter - 1) Mod NIterToSave) + 1
            Select Case MSet_ConstraintOption(whichMSet)
                Case 0, 1, 2, 3, 4
                    MSFlexGrid1.Cols = 2
                    'MSFlexGrid1.TextMatrix(0, 1) = "Target/10000"
                    MSFlexGrid1.set_TextMatrix(0, 1, "Target")
                    For jtic = 1 To NTic
                        MSFlexGrid1.set_TextMatrix(jtic, 0, jtic)
                        'MSFlexGrid1.TextMatrix(jtic, 1) = Format$(MSetTarget!(whichMSet, jtic) / 10000, "######0")
                        MSFlexGrid1.set_TextMatrix(jtic, 1, VB6.Format(MSet_Target(whichMSet, jtic) / NYearPerTic, "######0"))
                    Next jtic
                Case 5
                    MSFlexGrid1.Cols = 3
                    'MSFlexGrid1.TextMatrix(0, 1) = "Lower Bound/10000"
                    'MSFlexGrid1.TextMatrix(0, 2) = "Upper Bound/10000"
                    MSFlexGrid1.set_TextMatrix(0, 1, "Lower Bound")
                    MSFlexGrid1.set_TextMatrix(0, 2, "Upper Bound")

                    For jtic = 1 To NTic
                        MSFlexGrid1.set_TextMatrix(jtic, 0, jtic)
                        'MSFlexGrid1.TextMatrix(jtic, 1) = Format$(MSetFlowMn!(whichMSet) / 10000, "######0")
                        'MSFlexGrid1.TextMatrix(jtic, 2) = Format$(MSetFlowMx!(whichMSet) / 10000, "######0")
                        MSFlexGrid1.set_TextMatrix(jtic, 1, VB6.Format(MSet_FlowMn1(whichMSet) / NYearPerTic, "######0"))
                        MSFlexGrid1.set_TextMatrix(jtic, 2, VB6.Format(MSet_FlowMx1(whichMSet) / NYearPerTic, "######0"))
                    Next jtic
                Case 6, 7
                    MSFlexGrid1.Cols = 3
                    'MSFlexGrid1.TextMatrix(0, 1) = "Lower Bound/10000"
                    'MSFlexGrid1.TextMatrix(0, 2) = "Upper Bound/10000"
                    MSFlexGrid1.set_TextMatrix(0, 1, "Lower Bound")
                    MSFlexGrid1.set_TextMatrix(0, 2, "Upper Bound")

                    For jtic = 2 To NTic
                        Upper = MSet_Flow(whichMSet, kposition, jtic - 1) * (1 + MSet_DevMx(whichMSet))
                        Lower = MSet_Flow(whichMSet, kposition, jtic - 1) * (1 - MSet_DevMn(whichMSet))
                        MSFlexGrid1.set_TextMatrix(jtic, 0, jtic)
                        'MSFlexGrid1.TextMatrix(jtic, 1) = Format$(Lower! / 10000, "######0")
                        'MSFlexGrid1.TextMatrix(jtic, 2) = Format$(Upper! / 10000, "######0")
                        MSFlexGrid1.set_TextMatrix(jtic, 1, VB6.Format(Lower / NYearPerTic, "######0"))
                        MSFlexGrid1.set_TextMatrix(jtic, 2, VB6.Format(Upper / NYearPerTic, "######0"))
                    Next jtic
                    Upper = MSet_BaseQuan(whichMSet * (1 + MSet_DevMx(whichMSet)))
                    Lower = MSet_BaseQuan(whichMSet * (1 - MSet_DevMn(whichMSet)))
                    'MSFlexGrid1.TextMatrix(1, 1) = Format$(Lower! / 10000, "######0")
                    'MSFlexGrid1.TextMatrix(1, 2) = Format$(Upper! / 10000, "######0")
                    MSFlexGrid1.set_TextMatrix(1, 1, VB6.Format(Lower / NYearPerTic, "######0"))
                    MSFlexGrid1.set_TextMatrix(1, 2, VB6.Format(Upper / NYearPerTic, "######0"))
                    'UPGRADE_WARNING: Couldn't resolve default property of object MSetConstraintOption(whichMSet). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If MSet_ConstraintOption(whichMSet) = 7 Then
                        MSFlexGrid1.set_TextMatrix(1, 1, "Unbounded")
                        MSFlexGrid1.set_TextMatrix(1, 2, "Unbounded")
                    End If
            End Select
        ElseIf whatToShow = 3 Then
            For jiter = 1 To NiterToShow
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                MSFlexGrid1.set_TextMatrix(0, jiter, Str(kiter) & " " & AdjIterType(kposition))
                For jtic = 1 To NTic
                    MSFlexGrid1.set_TextMatrix(jtic, 0, jtic)
                    MSFlexGrid1.set_TextMatrix(jtic, jiter, VB6.Format(MSet_Pr(whichMSet, kposition, jtic), "##.00"))
                Next jtic
                kiter = kiter - 1
            Next jiter
        ElseIf whatToShow = 4 Then
            MSFlexGrid1.Cols = NiterToShow
            For jiter = 1 To NiterToShow - 1
                kprior = kiter - 1
                If kprior = 0 Then kprior = 1
                kposprior = ((kprior - 1) Mod NIterToSave) + 1
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                MSFlexGrid1.set_TextMatrix(0, jiter, Str(kiter) & " " & AdjIterType(kposition))
                For jtic = 1 To NTic
                    MSFlexGrid1.set_TextMatrix(jtic, 0, jtic)
                    PrChange = MSet_Pr(whichMSet, kposition, jtic) - MSet_Pr(whichMSet, kposprior, jtic)
                    MSFlexGrid1.set_TextMatrix(jtic, jiter, VB6.Format(PrChange, "####.0000"))
                Next jtic
                kiter = kiter - 1
            Next jiter
        End If

    End Sub

    Private Sub resetgrid2()
        Dim PrChange As Single
        Dim kposprior As Short
        Dim kprior As Short
        Dim Lower As Single
        Dim Upper As Single
        Dim jtic As Short
        Dim kposition As Short
        Dim jiter As Short
        Dim NiterToShow As Short
        Dim kiter As Short
        kiter = IterationNumber
        If kiter = 0 Then kiter = 1
        NiterToShow = kiter
        If NiterToShow > NIterToSave Then NiterToShow = NIterToSave
        MSFlexGrid2.Cols = NiterToShow + 1

        If WhatToShow2 = 0 Then
            For jiter = 1 To NiterToShow
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                MSFlexGrid2.set_TextMatrix(0, jiter, Str(kiter) & " " & AdjIterType(kposition))
                For jtic = 1 To NTic
                    MSFlexGrid2.set_TextMatrix(jtic, 0, jtic)
                    MSFlexGrid2.set_TextMatrix(jtic, jiter, VB6.Format(MSet_Dev(whichMSet2, kposition, jtic), "####.00"))
                Next jtic
                kiter = kiter - 1
            Next jiter
        ElseIf WhatToShow2 = 1 Then
            For jiter = 1 To NiterToShow
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                MSFlexGrid2.set_TextMatrix(0, jiter, Str(kiter) & " " & AdjIterType(kposition))
                For jtic = 1 To NTic
                    MSFlexGrid2.set_TextMatrix(jtic, 0, jtic)
                    MSFlexGrid2.set_TextMatrix(jtic, jiter, VB6.Format(MSet_flow(whichMSet2, kposition, jtic) / NYearPerTic, "######0"))
                    'MSFlexGrid2.TextMatrix(jtic, Jiter) = Format$(MSetFlow#(kposition, whichMSet2, jtic) / 10000, "######0")
                Next jtic
                kiter = kiter - 1
            Next jiter
        ElseIf WhatToShow2 = 2 Then
            kposition = ((kiter - 1) Mod NIterToSave) + 1
            Select Case MSet_ConstraintOption(whichMSet)
                Case 0, 1, 2, 3, 4
                    MSFlexGrid2.Cols = 2
                    'MSFlexGrid2.TextMatrix(0, 1) = "Target/10000"
                    MSFlexGrid2.set_TextMatrix(0, 1, "Target")
                    For jtic = 1 To NTic
                        MSFlexGrid2.set_TextMatrix(jtic, 0, jtic)
                        'MSFlexGrid2.TextMatrix(jtic, 1) = Format$(MSetTarget!(whichMSet2, jtic) / 10000, "######0")
                        MSFlexGrid2.set_TextMatrix(jtic, 1, VB6.Format(MSet_Target(whichMSet2, jtic) / NYearPerTic, "######0"))
                    Next jtic
                Case 5
                    MSFlexGrid2.Cols = 3
                    'MSFlexGrid2.TextMatrix(0, 1) = "Lower Bound/10000"
                    'MSFlexGrid2.TextMatrix(0, 2) = "Upper Bound/10000"
                    MSFlexGrid2.set_TextMatrix(0, 1, "Lower Bound")
                    MSFlexGrid2.set_TextMatrix(0, 2, "Upper Bound")
                    For jtic = 1 To NTic
                        MSFlexGrid2.set_TextMatrix(jtic, 0, jtic)
                        'MSFlexGrid2.TextMatrix(jtic, 1) = Format$(MSetFlowMn!(whichMSet2) / 10000, "######0")
                        'MSFlexGrid2.TextMatrix(jtic, 2) = Format$(MSetFlowMx!(whichMSet2) / 10000, "######0")
                        MSFlexGrid2.set_TextMatrix(jtic, 1, VB6.Format(MSet_FlowMn1(whichMSet2) / NYearPerTic, "######0"))
                        MSFlexGrid2.set_TextMatrix(jtic, 2, VB6.Format(MSet_FlowMx1(whichMSet2) / NYearPerTic, "######0"))


                    Next jtic
                Case 6, 7
                    MSFlexGrid2.Cols = 3
                    'MSFlexGrid2.TextMatrix(0, 1) = "Lower Bound/10000"
                    'MSFlexGrid2.TextMatrix(0, 2) = "Upper Bound/10000"
                    MSFlexGrid2.set_TextMatrix(0, 1, "Lower Bound")
                    MSFlexGrid2.set_TextMatrix(0, 2, "Upper Bound")
                    For jtic = 2 To NTic
                        Upper = MSet_Flow(whichMSet2, kposition, jtic - 1) * (1 + MSet_DevMx(whichMSet2))
                        Lower = MSet_Flow(whichMSet2, kposition, jtic - 1) * (1 - MSet_DevMn(whichMSet2))
                        MSFlexGrid2.set_TextMatrix(jtic, 0, jtic)
                        'MSFlexGrid2.TextMatrix(jtic, 1) = Format$(Lower! / 10000, "######0")
                        'MSFlexGrid2.TextMatrix(jtic, 2) = Format$(Upper! / 10000, "######0")
                        MSFlexGrid2.set_TextMatrix(jtic, 1, VB6.Format(Lower / NYearPerTic, "######0"))
                        MSFlexGrid2.set_TextMatrix(jtic, 2, VB6.Format(Upper / NYearPerTic, "######0"))

                    Next jtic
                    Upper = MSet_BaseQuan(whichMSet2) * (1 + MSet_DevMx(whichMSet2))
                    Lower = MSet_BaseQuan(whichMSet2) * (1 - MSet_DevMn(whichMSet2))
                    'MSFlexGrid2.TextMatrix(1, 1) = Format$(Lower! / 10000, "######0")
                    'MSFlexGrid2.TextMatrix(1, 2) = Format$(Upper! / 10000, "######0")
                    MSFlexGrid2.set_TextMatrix(1, 1, VB6.Format(Lower / NYearPerTic, "######0"))
                    MSFlexGrid2.set_TextMatrix(1, 2, VB6.Format(Upper / NYearPerTic, "######0"))

                    'UPGRADE_WARNING: Couldn't resolve default property of object MSetConstraintOption(whichMSet). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If MSet_ConstraintOption(whichMSet) = 7 Then
                        MSFlexGrid2.set_TextMatrix(1, 1, "Unbounded")
                        MSFlexGrid2.set_TextMatrix(1, 2, "Unbounded")
                    End If
            End Select
        ElseIf WhatToShow2 = 3 Then
            For jiter = 1 To NiterToShow
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                MSFlexGrid2.set_TextMatrix(0, jiter, Str(kiter) & " " & AdjIterType(kposition))
                For jtic = 1 To NTic
                    MSFlexGrid2.set_TextMatrix(jtic, 0, jtic)
                    MSFlexGrid2.set_TextMatrix(jtic, jiter, VB6.Format(MSet_Pr(whichMSet2, kposition, jtic), "##.00"))
                Next jtic
                kiter = kiter - 1
            Next jiter

        ElseIf WhatToShow2 = 4 Then
            MSFlexGrid2.Cols = NiterToShow
            For jiter = 1 To NiterToShow - 1
                kprior = kiter - 1
                If kprior = 0 Then kprior = 1
                kposprior = ((kprior - 1) Mod NIterToSave) + 1
                kposition = ((kiter - 1) Mod NIterToSave) + 1
                MSFlexGrid2.set_TextMatrix(0, jiter, Str(kiter) & " " & AdjIterType(kposition))
                For jtic = 1 To NTic
                    MSFlexGrid2.set_TextMatrix(jtic, 0, jtic)
                    PrChange = MSet_Pr(whichMSet2, kposition, jtic) - MSet_Pr(whichMSet2, kposprior, jtic)
                    MSFlexGrid2.set_TextMatrix(jtic, jiter, VB6.Format(PrChange, "####.0000"))
                Next jtic
                kiter = kiter - 1
            Next jiter
        End If


    End Sub



    'UPGRADE_WARNING: Event List1.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub List1_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List1.SelectedIndexChanged
        whichMSet = List1.SelectedIndex + 1
        Text1.Text = MSet_Label(whichMSet)
        'UPGRADE_WARNING: Couldn't resolve default property of object MSetConstraintOption(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Text2.Text = ConstTypeLabel(MSet_ConstraintOption(whichMSet))
        Call resetgrid1()
    End Sub

    'UPGRADE_WARNING: Event List2.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub List2_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List2.SelectedIndexChanged
        whatToShow = List2.SelectedIndex
        Text3.Text = whatToShowLabel(whatToShow)
        Call resetgrid1()
    End Sub

    'UPGRADE_WARNING: Event List3.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub List3_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List3.SelectedIndexChanged
        whichMSet2 = List3.SelectedIndex + 1
        Text4.Text = MSet_Label(whichMSet2)
        'UPGRADE_WARNING: Couldn't resolve default property of object MSetConstraintOption(). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Text5.Text = ConstTypeLabel(MSet_ConstraintOption(whichMSet2))
        Call resetgrid2()

    End Sub

    'UPGRADE_WARNING: Event List4.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub List4_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles List4.SelectedIndexChanged
        WhatToShow2 = List4.SelectedIndex
        Text6.Text = whatToShowLabel(WhatToShow2)
        Call resetgrid2()

    End Sub
End Class